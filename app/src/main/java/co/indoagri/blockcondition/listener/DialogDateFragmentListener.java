package co.indoagri.blockcondition.listener;

import java.util.Date;

public interface DialogDateFragmentListener {
	public void onDateFragmentOK(Date date, int id);
}
