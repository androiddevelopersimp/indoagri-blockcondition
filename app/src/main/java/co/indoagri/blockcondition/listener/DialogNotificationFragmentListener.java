package co.indoagri.blockcondition.listener;

public interface DialogNotificationFragmentListener {
	public void onFragmentOK(boolean is_finish);
}
