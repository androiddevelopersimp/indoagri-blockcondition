package co.indoagri.blockcondition.listener;

public interface DialogTimeFragmentListener {
	public void onTimeFragmentOK(int hour, int minute, int second, int id);
}
