package co.indoagri.blockcondition.listener;

public interface DialogConfirmationListener {
	public void onConfirmOK(Object object, int id);
	public void onConfirmCancel(Object object, int id);
}
