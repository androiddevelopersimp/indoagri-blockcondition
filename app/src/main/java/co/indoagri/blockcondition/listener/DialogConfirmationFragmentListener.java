package co.indoagri.blockcondition.listener;

public interface DialogConfirmationFragmentListener {
	public void onConfirmFragmentOK(int id);
}
