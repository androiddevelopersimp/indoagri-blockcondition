package co.indoagri.blockcondition.listener;

public interface DialogNotificationListener {
	public void onOK(boolean is_finish);
}
