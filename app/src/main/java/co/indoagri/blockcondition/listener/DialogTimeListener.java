package co.indoagri.blockcondition.listener;

public interface DialogTimeListener {
    public void onTimeOK(int hour, int minute, int second, int id);
}
