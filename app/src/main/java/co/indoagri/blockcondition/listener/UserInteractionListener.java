package co.indoagri.blockcondition.listener;

public interface UserInteractionListener {
    void onUserInteraction();
}