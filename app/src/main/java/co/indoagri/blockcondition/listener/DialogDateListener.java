package co.indoagri.blockcondition.listener;

import java.util.Date;

public interface DialogDateListener {
	public void onDateOK(Date date, int id);
}
