package co.indoagri.blockcondition.listener;

public interface OutputListener {
	public void onOutputChange();
}
