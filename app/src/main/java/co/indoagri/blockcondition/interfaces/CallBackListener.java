package co.indoagri.blockcondition.interfaces;

public interface CallBackListener {
    void  onCallBack(int typeCode); // pass any parameter in your onCallBack which you want to return
}