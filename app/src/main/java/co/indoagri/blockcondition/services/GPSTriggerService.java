package co.indoagri.blockcondition.services;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import java.util.Date;
import java.util.List;

import co.indoagri.blockcondition.activity.RKHActivity;
import co.indoagri.blockcondition.activity.RKHJOBActivity;
import co.indoagri.blockcondition.activity.RKHMainGangActivity;
import co.indoagri.blockcondition.activity.RKHPekerjaActivity;

public class GPSTriggerService extends Service implements LocationListener {
	 
    private Location location = null;
    protected LocationManager locMgr;
    
    public static RKHJOBActivity rkhjobActivity;
	public static RKHPekerjaActivity rkhPekerjaActivity;
	public static RKHActivity rkhActivity;
	public static RKHMainGangActivity rkhMainGangActivity;

    private ActivityManager activityMgr;
  
    public void initGPS() {    	
        try {              	
            locMgr = (LocationManager) getSystemService(LOCATION_SERVICE);

            if (locMgr.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				try {
					locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

					if (locMgr != null) {
						Location new_location = locMgr.getLastKnownLocation(LocationManager.GPS_PROVIDER);

						if (new_location != null) {
							Date date_curr = new Date();

							long time_diff = date_curr.getTime() - new_location.getTime();

							if (time_diff <= (5 * 60 * 1000) && new_location.getAccuracy() < 100) {
								doWorkWithNewLocation(new_location);
							}
						}
					}
				}catch (SecurityException e){

				}
            }
            
        } catch (Exception e) {
            e.printStackTrace(); 
        }
    }
    

	public void stopGps(){
		if(locMgr != null){
			try {
				locMgr.removeUpdates(GPSTriggerService.this);
			}catch (SecurityException e){

			}
		}
	}
    
    @Override
    public void onLocationChanged(Location new_location) {
    	doWorkWithNewLocation(new_location);
    }  
 
    @Override
    public void onProviderDisabled(String provider) {
    	Log.d("TAG", provider + " disbaled");
    }
 
    @Override
    public void onProviderEnabled(String provider) {
    	Log.d("TAG", provider + " enabled");
    }
 
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}
 
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
    
	@Override
	public void onCreate() {
		super.onCreate();
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		activityMgr = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		initGPS();
		
//		Toast.makeText(getApplicationContext(), "jalan", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		activityMgr = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		initGPS();
		return Service.START_STICKY;
	}


	@Override
	public void onDestroy() {
		stopGps();
		super.onDestroy();
	}
	
	private void doWorkWithNewLocation(Location newLocation){
		if(isBetterLocation(location, newLocation)){
			location = newLocation;
		}
		  
		if(activityMgr != null){
			List<ActivityManager.RunningTaskInfo> taskInfo = activityMgr.getRunningTasks(1);
			
			if(taskInfo.get(0).topActivity.getClassName().toString().equals(RKHJOBActivity.class.getCanonicalName())){
				if(rkhjobActivity != null){
					rkhjobActivity.updateGpsKoordinat(location);
				}
			}else if(taskInfo.get(0).topActivity.getClassName().toString().equals(RKHPekerjaActivity.class.getCanonicalName())){
				if(rkhPekerjaActivity != null){
					rkhPekerjaActivity.updateGpsKoordinat(location);
				}
			}else if(taskInfo.get(0).topActivity.getClassName().toString().equals(RKHActivity.class.getCanonicalName())){
				if(rkhActivity != null){
					rkhActivity.updateGpsKoordinat(location);
				}
			}else if(taskInfo.get(0).topActivity.getClassName().toString().equals(RKHMainGangActivity.class.getCanonicalName())){
				if(rkhMainGangActivity != null){
					rkhMainGangActivity.updateGpsKoordinat(location);
				}
			}
		}
	}
	
	
	private boolean isBetterLocation(Location old_location, Location new_location){
		if(old_location == null){
			return true;
		}
		
		boolean is_newer = new_location.getTime() > old_location.getTime();
		
		boolean is_more_accurate = new_location.getAccuracy() < old_location.getAccuracy();
		
		if(is_more_accurate && is_newer){
			return true;
		}else if(is_more_accurate && !is_newer){
			long time_diff = new_location.getTime() - old_location.getTime();
			
			if(time_diff > 1000){
				return true;
			}
		}
		
		return false;
	}
  
}