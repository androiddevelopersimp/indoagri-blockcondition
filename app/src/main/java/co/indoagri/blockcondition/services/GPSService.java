package co.indoagri.blockcondition.services;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;

import java.util.Date;

public class GPSService extends Service implements LocationListener {
	private Context context;

	boolean isGPSEnabled = false;
	boolean isNetworkEnabled = false;
	boolean canGetLocation = false;

	Location location;
	double latitude;
	double longitude;

	protected LocationManager loc_mgr;

	public GPSService(Context context) {
		this.context = context;
		location = null;
		getLocation();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
	}

	public Location getLocation() {
		try {
			loc_mgr = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
			if (ActivityCompat.checkSelfPermission(this.context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
				ActivityCompat.requestPermissions((Activity) this.context, new String[]{
						android.Manifest.permission.ACCESS_FINE_LOCATION
				}, 10);
				loc_mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
			}

			isGPSEnabled = loc_mgr.isProviderEnabled(LocationManager.GPS_PROVIDER);
			isNetworkEnabled = loc_mgr.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			
			
			if(isGPSEnabled){
				canGetLocation = true;
				
				if(loc_mgr != null){
					Location newLocation = loc_mgr.getLastKnownLocation(LocationManager.GPS_PROVIDER);
					
					if(newLocation != null){
                		Date dateCurr = new Date();
                		
                		long timeDiff = dateCurr.getTime() - newLocation.getTime();
                		
//                		Log.d("tag", "cur:" + new DateLocal(new Date(date_curr.getTime())).getDateString(DateLocal.FORMAT_FILE));
//                		Log.d("tag", "new:" + new DateLocal(new Date(new_location.getTime())).getDateString(DateLocal.FORMAT_FILE));
//                		Log.d("tag", "X:" + time_diff);
                		
                		if(timeDiff <= (5 * 60 * 1000) && newLocation.getAccuracy() < 100){
                			doWorkWithNewLocation(newLocation);
                		}
					}
				}
			}else{
				canGetLocation = false;
			}
            
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return location;
	}
	
	public boolean canGetLocation(){
		return canGetLocation;
	}
	
	public void stopUsingGPS(){
		if(loc_mgr != null){
			loc_mgr.removeUpdates(GPSService.this);
		}
	}
	
	public double getLatitude(){
		return latitude;
	}
	
	public double getLongitude(){
		return longitude;
	}
	
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onLocationChanged(Location location) {
		doWorkWithNewLocation(location);
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	
	private void doWorkWithNewLocation(Location new_location){
		if(isBetterLocation(location, new_location)){
			location = new_location;
			
			if(location != null){
				latitude = location.getLatitude();
				longitude = location.getLongitude();
			}
		}
	}
	
	
	private boolean isBetterLocation(Location old_location, Location new_location){
		if(old_location == null){
			return true;
		}
		
		boolean is_newer = new_location.getTime() > old_location.getTime();
		
		boolean is_more_accurate = new_location.getAccuracy() < old_location.getAccuracy();
		
		if(is_more_accurate && is_newer){
			return true;
		}else if(is_more_accurate && !is_newer){
			long time_diff = new_location.getTime() - old_location.getTime();
			
			if(time_diff > 1000){
				return true;
			}
		}
		
		return false;
	}
}
