package co.indoagri.blockcondition.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import co.indoagri.blockcondition.model.Bluetooth;
import co.indoagri.blockcondition.model.Data.BJR;
import co.indoagri.blockcondition.model.Data.BLKPLT;
import co.indoagri.blockcondition.model.Data.BLKSBC;
import co.indoagri.blockcondition.model.Data.BLKSBCDetail;
import co.indoagri.blockcondition.model.Data.BLKSUGC;
import co.indoagri.blockcondition.model.Data.BlockHdrc;
import co.indoagri.blockcondition.model.Data.DayOff;
import co.indoagri.blockcondition.model.Data.DeviceAlias;
import co.indoagri.blockcondition.model.Data.MasterDownload;
import co.indoagri.blockcondition.model.Data.Penalty;
import co.indoagri.blockcondition.model.Data.RunningAccount;
import co.indoagri.blockcondition.model.Data.SKB;
import co.indoagri.blockcondition.model.Data.SPBSDestination;
import co.indoagri.blockcondition.model.Data.SPBSRunningNumber;
import co.indoagri.blockcondition.model.Data.SPTARunningNumber;
import co.indoagri.blockcondition.model.Data.Vendor;
import co.indoagri.blockcondition.model.Data.VersionDB;
import co.indoagri.blockcondition.model.Data.tblM_AccountingPeriod;
import co.indoagri.blockcondition.model.Data.tblM_BlockConditionScore;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.model.Data.tblT_BlockConditionScore;
import co.indoagri.blockcondition.model.LogModel;
import co.indoagri.blockcondition.model.RKH.RKH_ACTIVITY_TYPE;
import co.indoagri.blockcondition.model.RKH.RKH_HEADER;
import co.indoagri.blockcondition.model.RKH.RKH_ITEM;
import co.indoagri.blockcondition.model.Users.AbsentType;
import co.indoagri.blockcondition.model.Users.DivisionAssistant;
import co.indoagri.blockcondition.model.Users.Employee;
import co.indoagri.blockcondition.model.Users.GroupHead;
import co.indoagri.blockcondition.model.Users.UserApp;
import co.indoagri.blockcondition.model.Users.UserLogin;
import co.indoagri.blockcondition.routines.Constants;

public class DatabaseHelper extends SQLiteOpenHelper{

    public final static int dbVersion = Constants.db_version;
    public final static String dbName = Constants.db_name;

    public DatabaseHelper(Context context){
        super(context, dbName, null, dbVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase sqliteDatabase) {
        String query;
        query = "create table " + MasterDownload.TABLE_NAME +
                "(" +
                MasterDownload.XML_NAME + " text not null, " +
                MasterDownload.XML_FILENAME + " text, " +
                MasterDownload.XML_SYNC_DATE + " integer, " +
                MasterDownload.XML_STATUS + " integer, " +
                "primary key (" +
                MasterDownload.XML_NAME +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + LogModel.TABLE_NAME +
                "(" +
                LogModel.XML_ID + " integer, " +
                LogModel.XML_USERCODE+ " text, " +
                LogModel.XML_DESCRIPTION+ " text, " +
                LogModel.XML_CREATEDDATE + " text, " +
                LogModel.XML_REFERENCE + " text " +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + Employee.TABLE_NAME +
                "(" +
                Employee.XML_COMPANY_CODE + " text, " +
                Employee.XML_ESTATE + " text, " +
                Employee.XML_FISCAL_YEAR + " integer, " +
                Employee.XML_FISCAL_PERIOD + " integer, " +
                Employee.XML_NIK + " text, " +
                Employee.XML_NAME + " text, " +
                Employee.XML_TERM_DATE + " text, " +
                Employee.XML_DIVISION + " text, " +
                Employee.XML_ROLE_ID + " text, " +
                Employee.XML_JOB_POS + " text, " +
                Employee.XML_GANG + " text, " +
                Employee.XML_COST_CENTER + " text, " +
                Employee.XML_EMP_TYPE + " text, " +
                Employee.XML_VALID_FROM + " text, " +
                Employee.XML_HARVESTER_CODE + " text, " +
                "primary key (" +
                Employee.XML_COMPANY_CODE + ", " +
                Employee.XML_ESTATE + ", " +
                Employee.XML_FISCAL_YEAR + ", " +
                Employee.XML_FISCAL_PERIOD + ", " +
                Employee.XML_NIK +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + AbsentType.TABLE_NAME +
                "(" +
                AbsentType.XML_COMPANY_CODE + " text not null, " +
                AbsentType.XML_ABSENT_TYPE + " text not null, " +
                AbsentType.XML_DESCRIPTION + " text, " +
                AbsentType.XML_HKRLLO + " double, " +
                AbsentType.XML_HKRLHI + " double, " +
                AbsentType.XML_HKPYLO + " double, " +
                AbsentType.XML_HKPYHI + " double, " +
                AbsentType.XML_HKVLLO + " double, " +
                AbsentType.XML_HKVLHI + " double, " +
                AbsentType.XML_HKMEIN + " double, " +
                AbsentType.XML_AGROUP + " double, " +
                "primary key (" +
                AbsentType.XML_COMPANY_CODE + ", " +
                AbsentType.XML_ABSENT_TYPE +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + UserApp.TABLE_NAME +
                "(" +
                UserApp.XML_NIK + " text not null, " +
                UserApp.XML_USERNAME + " text not null, " +
                UserApp.XML_PASSWORD + " text not null, " +
                UserApp.XML_VALID_TO + " integer not null, " +
                UserApp.XML_CREATED_DATE + " integer, " +
                UserApp.XML_CREATED_BY + " text, " +
                "primary key (" +
                UserApp.XML_NIK + ", " +
                UserApp.XML_USERNAME +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);
        query = "create table " + DayOff.TABLE_NAME +
                "(" +
                DayOff.XML_ESTATE + " text not null, " +
                DayOff.XML_DATE + " text not null, " +
                DayOff.XML_DAY_OFF_TYPE + " text not null, " +
                DayOff.XML_DESCRIPTION + " text, " +
                "primary key (" +
                DayOff.XML_ESTATE + ", " +
                DayOff.XML_DATE +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);
        query = "create table " + DivisionAssistant.TABLE_NAME +
                "(" +
                DivisionAssistant.XML_ESTATE + " text not null, " +
                DivisionAssistant.XML_DIVISION + " text not null, " +
                DivisionAssistant.XML_SPRAS + " text, " +
                DivisionAssistant.XML_DESCRIPTION + " text, " +
                DivisionAssistant.XML_ASSISTANT + " text, " +
                DivisionAssistant.XML_DISTANCE_TO_MILL + " text, " +
                DivisionAssistant.XML_UOM + " text, " +
                DivisionAssistant.XML_ASSISTANT_NAME + " text, " +
                DivisionAssistant.XML_LIFNR + " text, " +
                "primary key (" +
                DivisionAssistant.XML_ESTATE + ", " +
                DivisionAssistant.XML_DIVISION +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + UserLogin.TABLE_NAME +
                "(" +
                UserLogin.XML_ID + " integer not null, " +
                UserLogin.XML_COMPANY_CODE + " text not null, " +
                UserLogin.XML_ESTATE + " text not null, " +
                UserLogin.XML_FISCAL_YEAR + " integer, " +
                UserLogin.XML_FISCAL_PERIOD + " integer, " +
                UserLogin.XML_NIK + " text not null, " +
                UserLogin.XML_NAME + " text not null, " +
                UserLogin.XML_TERM_DATE + " text, " +
                UserLogin.XML_DIVISION + " text not null, " +
                UserLogin.XML_ROLE_ID + " text, " +
                UserLogin.XML_JOB_POS + " text not null, " +
                UserLogin.XML_GANG + " text not null, " +
                UserLogin.XML_COST_CENTER + " text, " +
                UserLogin.XML_EMP_TYPE + " text, " +
                UserLogin.XML_VALID_FROM + " text, " +
                UserLogin.XML_HARVESTER_CODE + " text, " +
                "primary key (" +
                UserLogin.XML_ID +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + GroupHead.TABLE_NAME +
                "(" +
                GroupHead.XML_COMPANY_CODE + " text not null, " +
                GroupHead.XML_ESTATE + " text not null, " +
                GroupHead.XML_LIFNR + " text not null, " +
                GroupHead.XML_INITIAL + " text not null, " +
                GroupHead.XML_VALID_FROM + " text, " +
                GroupHead.XML_VALID_TO + " text, " +
                GroupHead.XML_NAME + " text not null, " +
                GroupHead.XML_STATUS + " integer, " +
                "primary key (" +
                GroupHead.XML_COMPANY_CODE + ", " +
                GroupHead.XML_ESTATE + ", " +
                GroupHead.XML_LIFNR + ", " +
                GroupHead.XML_INITIAL + ", " +
                GroupHead.XML_VALID_FROM +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BJR.TABLE_NAME +
                "(" +
                BJR.XML_COMPANY_CODE + " text, " +
                BJR.XML_ESTATE + " text, " +
                BJR.XMl_EFF_DATE + " text, " +
                BJR.XML_BLOCK + " text, " +
                BJR.XML_BJR + " double, " +
                "primary key (" +
                BJR.XML_COMPANY_CODE + ", " +
                BJR.XML_ESTATE + ", " +
                BJR.XMl_EFF_DATE + ", " +
                BJR.XML_BLOCK +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BlockHdrc.TABLE_NAME +
                "(" +
                BlockHdrc.XML_COMPANY_CODE + " text, " +
                BlockHdrc.XML_ESTATE + " text, " +
                BlockHdrc.XML_BLOCK + " text, " +
                BlockHdrc.XML_VALID_FROM + " text, " +
                BlockHdrc.XML_VALID_TO + " text, " +
                BlockHdrc.XML_DIVISION + " text, " +
                BlockHdrc.XML_TYPE + " text, " +
                BlockHdrc.XML_STATUS + " text, " +
                BlockHdrc.XML_PROJECT_DEFINITION + " text, " +
                BlockHdrc.XML_OWNER + " text, " +
                BlockHdrc.XML_TGL_TANAM + " text, " +
                BlockHdrc.XML_MANDT + " text, " +
                "primary key (" +
                BlockHdrc.XML_COMPANY_CODE + ", " +
                BlockHdrc.XML_ESTATE + ", " +
                BlockHdrc.XML_BLOCK + ", " +
                BlockHdrc.XML_VALID_FROM + ", " +
                BlockHdrc.XML_DIVISION + ", " +
                BlockHdrc.XML_MANDT +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BLKPLT.TABLE_NAME +
                "(" +
                BLKPLT.XML_COMPANY_CODE + " text, " +
                BLKPLT.XML_ESTATE + " text, " +
                BLKPLT.XML_BLOCK + " text, " +
                BLKPLT.XML_VALID_FROM + " text, " +
                BLKPLT.XML_VALID_TO + " text, " +
                BLKPLT.XML_CROP_TYPE + " text, " +
                BLKPLT.XML_PREVIOUS_CROP + " text, " +
                BLKPLT.XML_FINISH_DATE + " text, " +
                BLKPLT.XML_REFERENCE + " text, " +
                BLKPLT.XML_JARAK_TANAM + " text, " +
                BLKPLT.XML_HARVESTING_DATE + " text, " +
                BLKPLT.XML_HARVESTED + " double, " +
                BLKPLT.XML_PLAN_DATE + " text, " +
                BLKPLT.XML_TOPOGRAPHY + " text, " +
                BLKPLT.XML_SOIL_TYPE + " text, " +
                BLKPLT.XML_SOIL_CATEGORY + " text, " +
                BLKPLT.XML_PROD_TREES + " double, " +
                "primary key (" +
                BLKPLT.XML_COMPANY_CODE + ", " +
                BLKPLT.XML_ESTATE + ", " +
                BLKPLT.XML_BLOCK + ", " +
                BLKPLT.XML_VALID_FROM + ", " +
                BLKPLT.XML_CROP_TYPE +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + Penalty.TABLE_NAME +
                "(" +
                Penalty.XML_PENALTY_CODE + " text not null, " +
                Penalty.XML_PENALTY_DESC + " text, " +
                Penalty.XML_UOM + " text, " +
                Penalty.XMl_CROP_TYPE + " text, " +
                Penalty.XML_IS_LOADING + " integer, " +
                Penalty.XML_IS_ANCAK + " integer, " +
                "primary key (" +
                Penalty.XML_PENALTY_CODE +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + SKB.TABLE_NAME +
                "(" +
                SKB.XML_COMPANY_CODE + " text, " +
                SKB.XML_ESTATE + " text, " +
                SKB.XML_BLOCK + " text, " +
                SKB.XML_BARIS_SKB + " integer, " +
                SKB.XML_VALID_FROM + " text, " +
                SKB.XML_VALID_TO + " text,  " +
                SKB.XML_BARIS_BLOCK + " integer, " +
                SKB.XML_JUMLAH_POKOK + " double, " +
                SKB.XML_POKOK_MATI + " double, " +
                SKB.XML_TANGGAL_TANAM + " text, " +
                SKB.XML_LINE_SKB + " integer, " +
                "primary key (" +
                SKB.XML_COMPANY_CODE + ", " +
                SKB.XML_ESTATE + ", " +
                SKB.XML_BLOCK + ", " +
                SKB.XML_BARIS_SKB + ", " +
                SKB.XML_VALID_FROM + ", " +
                SKB.XML_BARIS_BLOCK + ", " +
                SKB.XML_LINE_SKB +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BLKSBC.TABLE_NAME +
                "(" +
                BLKSBC.XML_COMPANY_CODE + " text not null, " +
                BLKSBC.XML_ESTATE + " text not null, " +
                BLKSBC.XML_BLOCK + " text not null, " +
                BLKSBC.XML_VALID_FROM + " text not null, " +
                BLKSBC.XML_BASIS_NORMAL + " double, " +
                BLKSBC.XML_BASIS_FRIDAY + " double, " +
                BLKSBC.XML_BASIS_HOLIDAY + " double, " +
                BLKSBC.XML_PREMI_NORMAL + " double, " +
                BLKSBC.XML_PREMI_FRIDAY + " double, " +
                BLKSBC.XML_PREMI_HOLIDAY + " double, " +
                BLKSBC.XML_PRIORITY + " text, " +
                "primary key (" +
                BLKSBC.XML_COMPANY_CODE + ", " +
                BLKSBC.XML_ESTATE + ", " +
                BLKSBC.XML_BLOCK + ", " +
                BLKSBC.XML_VALID_FROM +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BLKSBCDetail.TABLE_NAME +
                "(" +
                BLKSBCDetail.XML_COMPANY_CODE + " text not null, " +
                BLKSBCDetail.XML_ESTATE + " text not null, " +
                BLKSBCDetail.XML_BLOCK + " text not null, " +
                BLKSBCDetail.XML_VALID_FROM + " text not null, " +
                BLKSBCDetail.XML_MIN_VAL + " double, " +
                BLKSBCDetail.XML_MAX_VAL + " double, " +
                BLKSBCDetail.XML_OVER_BASIC_RATE + " double, "+
                "primary key (" +
                BLKSBCDetail.XML_COMPANY_CODE + ", " +
                BLKSBCDetail.XML_ESTATE + ", " +
                BLKSBCDetail.XML_BLOCK + ", " +
                BLKSBCDetail.XML_VALID_FROM +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + SPBSRunningNumber.TABLE_NAME +
                "(" +
                SPBSRunningNumber.XML_ID + " integer not null, " +
                SPBSRunningNumber.XML_ESTATE + " text not null, " +
                SPBSRunningNumber.XML_DIVISION + " text not null, " +
                SPBSRunningNumber.XML_YEAR + " text not null, " +
                SPBSRunningNumber.XML_MONTH + " text not null, " +
                SPBSRunningNumber.XML_IMEI + " text not null, " +
                SPBSRunningNumber.XML_RUNNING_NUMBER + " integer default (1), " +
                SPBSRunningNumber.XML_DEVICE_ALIAS + " text, " +
                "primary key (" +
                SPBSRunningNumber.XML_ID + ", " +
                SPBSRunningNumber.XML_ESTATE + ", " +
                SPBSRunningNumber.XML_DIVISION + ", " +
                SPBSRunningNumber.XML_YEAR + ", " +
                SPBSRunningNumber.XML_MONTH + ", " +
                SPBSRunningNumber.XML_IMEI +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + RunningAccount.TABLE_NAME +
                "(" +
                RunningAccount.XML_COMPANY_CODE + " text not null, " +
                RunningAccount.XML_ESTATE + " text not null, " +
                RunningAccount.XML_RUNNING_ACCOUNT + " text not null, " +
                RunningAccount.XML_LICENSE_PLATE + " text, " +
                RunningAccount.XML_LIFNR + " text, " +
                RunningAccount.XML_OWNERSHIPFLAG + " text, " +
                "primary key (" +
                RunningAccount.XML_COMPANY_CODE + ", " +
                RunningAccount.XML_ESTATE + ", " +
                RunningAccount.XML_RUNNING_ACCOUNT +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + SPTARunningNumber.TABLE_NAME +
                "(" +
                SPTARunningNumber.XML_ID + " integer not null, " +
                SPTARunningNumber.XML_ESTATE + " text not null, " +
                SPTARunningNumber.XML_DIVISION + " text not null, " +
                SPTARunningNumber.XML_YEAR + " text not null, " +
                SPTARunningNumber.XML_MONTH + " text not null, " +
                SPTARunningNumber.XML_IMEI + " text not null, " +
                SPTARunningNumber.XML_RUNNING_NUMBER + " integer default (1), " +
                SPTARunningNumber.XML_DEVICE_ALIAS + " text, " +
                "primary key (" +
                SPTARunningNumber.XML_ID + ", " +
                SPTARunningNumber.XML_ESTATE + ", " +
                SPTARunningNumber.XML_DIVISION + ", " +
                SPTARunningNumber.XML_YEAR + ", " +
                SPTARunningNumber.XML_MONTH + ", " +
                SPTARunningNumber.XML_IMEI +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + Vendor.TABLE_NAME +
                "(" +
                Vendor.XML_ESTATE + " text not null, " +
                Vendor.XML_LIFNR + " text not null, " +
                Vendor.XML_NAME + " text, " +
                "primary key (" +
                Vendor.XML_ESTATE + ", " +
                Vendor.XML_LIFNR +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BLKSUGC.TABLE_NAME +
                "(" +
                BLKSUGC.XML_COMPANY_CODE + " text not null, " +
                BLKSUGC.XML_ESTATE + " text not null, " +
                BLKSUGC.XML_BLOCK + " text not null, " +
                BLKSUGC.XML_VALID_FROM + " text not null, " +
                BLKSUGC.XML_VALID_TO + " text, " +
                BLKSUGC.XML_PHASE + " text, " +
                BLKSUGC.XML_DISTANCE + " double, " +
                BLKSUGC.XML_SUB_DIVISION + " text, " +
                "primary key (" +
                BLKSUGC.XML_COMPANY_CODE + ", " +
                BLKSUGC.XML_ESTATE + ", " +
                BLKSUGC.XML_BLOCK + ", " +
                BLKSUGC.XML_VALID_FROM +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);
        query = "create table " + SPBSDestination.TABLE_NAME +
                "(" +
                SPBSDestination.XML_MANDT + " text not null, " +
                SPBSDestination.XML_ESTATE + " text not null, " +
                SPBSDestination.XML_DEST_TYPE + " text not null, " +
                SPBSDestination.XML_DEST_ID + " text not null, " +
                SPBSDestination.XML_DEST_DESC + " text, " +
                SPBSDestination.XML_ACTIVE + " integer, " +
                "primary key (" +
                SPBSDestination.XML_MANDT + ", " +
                SPBSDestination.XML_ESTATE + ", " +
                SPBSDestination.XML_DEST_TYPE + ", " +
                SPBSDestination.XML_DEST_ID +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);
        query = "create table " + Bluetooth.TABLE_NAME +
                "(" +
                Bluetooth.XML_NAME + " text, " +
                Bluetooth.XML_ADDRESS + " text not null, " +
                Bluetooth.XML_IS_PAIRED + " integer default (0), " +
                Bluetooth.XML_IS_SELECTED + " integer default (0)," +
                "primary key (" +
                Bluetooth.XML_ADDRESS +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);
        query = "create table " + DeviceAlias.TABLE_NAME +
                "(" +
                DeviceAlias.XML_DEVICE_ALIAS + " text not null " +
                ")";
        sqliteDatabase.execSQL(query);
        query = "create table " + tblM_AccountingPeriod.TABLE_NAME +
                "(" +
                tblM_AccountingPeriod.XML_Estate + " text not null, " +
                tblM_AccountingPeriod.XML_ZYear+ " text, " +
                tblM_AccountingPeriod.XML_Period + " text, " +
                tblM_AccountingPeriod.XML_ClosingDate + " text not null, " +
                tblM_AccountingPeriod.XML_Status+ " text, " +
                tblM_AccountingPeriod.XML_Active+ " text, " +
                tblM_AccountingPeriod.XML_CreatedBy+ " text, " +
                tblM_AccountingPeriod.XML_CreatedDate+ " text, " +
                tblM_AccountingPeriod.XML_ModifiedBy+ " text, " +
                tblM_AccountingPeriod.XML_ModifiedDate+ " text, " +
                "primary key (" +
                tblM_AccountingPeriod.XML_Estate + ", " +
                tblM_AccountingPeriod.XML_ZYear+ ", " +
                tblM_AccountingPeriod.XML_Period+ "" +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);
        query = "create table " + tblM_BlockConditionScore.TABLE_NAME +
                "(" +
                tblM_BlockConditionScore.XML_ScoreType + " text not null, " +
                tblM_BlockConditionScore.XML_ValidFrom+ " text, " +
                tblM_BlockConditionScore.XML_ValidTo+ " text, " +
                tblM_BlockConditionScore.XML_Color + " text, " +
                tblM_BlockConditionScore.XML_Interval1 + " decimal(10,2) default 0, " +
                tblM_BlockConditionScore.XML_Interval2+ " decimal(10,2) default 0, " +
                tblM_BlockConditionScore.XML_Score1+ " decimal(10,2) default 0, " +
                tblM_BlockConditionScore.XML_Score2+ " decimal(10,2) default 0, " +
                tblM_BlockConditionScore.XML_Score3+ " decimal(10,2) default 0, " +
                "primary key (" +
                tblM_BlockConditionScore.XML_ScoreType+ ", " +
                tblM_BlockConditionScore.XML_ValidFrom+ ", " +
                tblM_BlockConditionScore.XML_Color+ "" +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);
        query = "create table " + tblT_BlockCondition.TABLE_NAME +
                "(" +
                tblT_BlockCondition.XML_CompanyCode  + " text not null, " +
                tblT_BlockCondition.XML_Estate+ " text default null, " +
                tblT_BlockCondition.XML_ZYear + " text default null, " +
                tblT_BlockCondition.XML_Period + " text default null, " +
                tblT_BlockCondition.XML_ZDate+ " integer, " +
                tblT_BlockCondition.XML_TransLevel+ " integer, " +
                tblT_BlockCondition.XML_Block+ " text default null, " +
                tblT_BlockCondition.XML_SKB+ " text default null, " +
                tblT_BlockCondition.XML_BlockRow+ " text default null, " +
                tblT_BlockCondition.XML_CensusPoint+ " text default null, " +
                tblT_BlockCondition.XML_PokokLabel+ " text default null, " +
                tblT_BlockCondition.XML_PokokSide+ " text default null, " +
                tblT_BlockCondition.XML_PokokCondition+ " text default null, " +
                tblT_BlockCondition.XML_Jalan+ " text default null, " +
                tblT_BlockCondition.XML_Jembatan+ " text default null, " +
                tblT_BlockCondition.XML_Parit+ " text default null, " +
                tblT_BlockCondition.XML_TitiPanen+" text default null, " +
                tblT_BlockCondition.XML_TitiRintis+ " text default null, " +
                tblT_BlockCondition.XML_Tikus+ " text default null, " +
                tblT_BlockCondition.XML_Pencurian+ " text default null, " +
                tblT_BlockCondition.XML_BW+ " text default null, " +
                tblT_BlockCondition.XML_TPHBersih+ " text default null, " +
                tblT_BlockCondition.XML_TPHBersih2+ " text default null, " +
                tblT_BlockCondition.XML_TPH+ " text default null, " +
                tblT_BlockCondition.XML_TPH2+ " text default null, " +
                tblT_BlockCondition.XML_Piringan+ " text default null, " +
                tblT_BlockCondition.XML_PasarPanen+ " text default null, " +
                tblT_BlockCondition.XML_PasarRintis+ " text default null, " +
                tblT_BlockCondition.XML_TunasPokok+ " text default null, " +
                tblT_BlockCondition.XML_Gawangan+ " text default null, " +
                tblT_BlockCondition.XML_Drainase+ " text default null, " +
                tblT_BlockCondition.XML_Ganoderma+ " text default null, " +
                tblT_BlockCondition.XML_Rayap+ " text default null, " +
                tblT_BlockCondition.XML_Orcytes+ " text default null, " +
                tblT_BlockCondition.XML_Sanitasi+ " text default null, " +
                tblT_BlockCondition.XML_Kacangan+ " text default null, " +
                tblT_BlockCondition.XML_CreatedDateTime+ " text default null, " +
                tblT_BlockCondition.XML_CreatedBy+ " text default null, " +
                tblT_BlockCondition.XML_ModifiedDateTime+ " text default null, " +
                tblT_BlockCondition.XML_ModifiedBy+ " text default null," +
                tblT_BlockCondition.XML_Flag+ " text default null," +
                tblT_BlockCondition.XML_DIVISI+ " text default null," +
                tblT_BlockCondition.XML_Remark+ " text default null" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + VersionDB.TABLE_NAME +
                "(" +
                VersionDB.XML_DBVersion+ " text not null " +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + tblT_BlockConditionScore.TABLE_NAME +
                "(" +
                tblT_BlockConditionScore.XML_CompanyCode  + " text not null, " +
                tblT_BlockConditionScore.XML_Estate+ " text not null, " +
                tblT_BlockConditionScore.XML_ZYear + " text not null, " +
                tblT_BlockConditionScore.XML_Period + " text not null, " +
                tblT_BlockConditionScore.XML_Block+ " text not null, " +
                tblT_BlockConditionScore.XML_TransLevel+ " integer not null, " +
                tblT_BlockConditionScore.XML_Color+ " text not null, " +
                tblT_BlockConditionScore.XML_Jalan+ " text, " +
                tblT_BlockConditionScore.XML_Jembatan+ " text, " +
                tblT_BlockConditionScore.XML_Parit+ " text, " +
                tblT_BlockConditionScore.XML_TitiPanen+" text, " +
                tblT_BlockConditionScore.XML_TitiRintis+ " text, " +
                tblT_BlockConditionScore.XML_Tikus+ " text, " +
                tblT_BlockConditionScore.XML_Pencurian+ " text, " +
                tblT_BlockConditionScore.XML_BW+ " text, " +
                tblT_BlockConditionScore.XML_TPHBersih+ " text, " +
                tblT_BlockConditionScore.XML_TPHBersih2+ " text, " +
                tblT_BlockConditionScore.XML_TPH+ " text, " +
                tblT_BlockConditionScore.XML_TPH2+ " text, " +
                tblT_BlockConditionScore.XML_Piringan+ " text, " +
                tblT_BlockConditionScore.XML_PasarPanen+ " text, " +
                tblT_BlockConditionScore.XML_PasarRintis+ " text, " +
                tblT_BlockConditionScore.XML_TunasPokok+ " text, " +
                tblT_BlockConditionScore.XML_Gawangan+ " text, " +
                tblT_BlockConditionScore.XML_Drainase+ " text, " +
                tblT_BlockConditionScore.XML_Ganoderma+ " text, " +
                tblT_BlockConditionScore.XML_Rayap+ " text, " +
                tblT_BlockConditionScore.XML_Orcytes+ " text, " +
                tblT_BlockConditionScore.XML_Sanitasi+ " text, " +
                tblT_BlockConditionScore.XML_Kacangan+ " text, " +
                tblT_BlockConditionScore.XML_CreatedDateTime+ " integer, " +
                tblT_BlockConditionScore.XML_CreatedBy+ " text, " +
                tblT_BlockConditionScore.XML_ModifiedDateTime+ " integer, " +
                tblT_BlockConditionScore.XML_ModifiedBy+ " text " +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + RKH_ACTIVITY_TYPE.TABLE_NAME +
                "(" +
                RKH_ACTIVITY_TYPE.XML_ACTTYPE  + " text , " +
                RKH_ACTIVITY_TYPE.XML_NAME+ " text  " +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + RKH_HEADER.TABLE_NAME +
                "(" +
                RKH_HEADER.XML_IMEI  + " text , " +
                RKH_HEADER.XML_COMPANY_CODE  + " text , " +
                RKH_HEADER.XML_ESTATE  + " text , " +
                RKH_HEADER.XML_RKH_DATE  + " text , " +
                RKH_HEADER.XML_DIVISION  + " text , " +
                RKH_HEADER.XML_GANG  + " text , " +
                RKH_HEADER.XML_NIK_FOREMAN  + " text , " +
                RKH_HEADER.XML_FOREMAN  + " text , " +
                RKH_HEADER.XML_NIK_CLERK  + " text , " +
                RKH_HEADER.XML_CLERK  + " text , " +
                RKH_HEADER.XML_GPS_KOORDINAT  + " text , " +
                RKH_HEADER.XML_STATUS  + " integer , " +
                RKH_HEADER.XML_CREATED_DATE  + " integer , " +
                RKH_HEADER.XML_CREATED_BY  + " text , " +
                RKH_HEADER.XML_MODIFIED_DATE  + " integer , " +
                RKH_HEADER.XML_MODIFIED_BY  + " text , " +
                RKH_HEADER.XML_RKH_ID  + " text, " +
                "primary key (" +
                RKH_HEADER.XML_RKH_ID + ", " +
                RKH_HEADER.XML_IMEI +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + RKH_ITEM.TABLE_NAME +
                "(" +
                RKH_ITEM.XML_RKH_ID  + " text , " +
                RKH_ITEM.XML_LINE  + " text , " +
                RKH_ITEM.XML_ACTIVITY  + " text , " +
                RKH_ITEM.XML_BLOCK  + " text , " +
                RKH_ITEM.XML_TARGET_OUTPUT  + " text , " +
                RKH_ITEM.XML_SKU  + " text , " +
                RKH_ITEM.XML_PHL  + " text , " +
                RKH_ITEM.XML_TARGET_HASIL  + " text , " +
                RKH_ITEM.XML_TARGET_JANJANG  + " text , " +
                RKH_ITEM.XML_BJR  + " text , " +
                RKH_ITEM.XML_GPS_KOORDINAT  + " text , " +
                RKH_ITEM.XML_STATUS  + " integer , " +
                RKH_ITEM.XML_CREATED_DATE  + " integer , " +
                RKH_ITEM.XML_CREATED_BY  + " text , " +
                RKH_ITEM.XML_MODIFIED_DATE  + " integer , " +
                RKH_ITEM.XML_MODIFIED_BY  + " text  " +
                ")";
        sqliteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqliteDatabase, int versionOld, int versionNew) {
        String query;

        try{
            switch(versionOld){
                case 1:
                    sqliteDatabase.execSQL("alter table " + tblT_BlockCondition.TABLE_NAME + " add column " + tblT_BlockCondition.XML_DIVISI+ " text");
                    query = "create table " + VersionDB.TABLE_NAME +
                            "(" +
                            VersionDB.XML_DBVersion+ " text not null " +
                            ")";
                    sqliteDatabase.execSQL(query);
                case 2:
                    sqliteDatabase.execSQL("alter table " + tblT_BlockCondition.TABLE_NAME + " add column " + tblT_BlockCondition.XML_Remark+ " text");
                    query = "create table " + VersionDB.TABLE_NAME +
                            "(" +
                            VersionDB.XML_DBVersion+ " text not null " +
                            ")";
                    sqliteDatabase.execSQL(query);
                case 153:
                    query = "create table " + RKH_ACTIVITY_TYPE.TABLE_NAME +
                            "(" +
                            RKH_ACTIVITY_TYPE.XML_ACTTYPE  + " text , " +
                            RKH_ACTIVITY_TYPE.XML_NAME+ " text  " +
                            ")";
                    sqliteDatabase.execSQL(query);

                    query = "create table " + RKH_HEADER.TABLE_NAME +
                            "(" +
                            RKH_HEADER.XML_IMEI  + " text , " +
                            RKH_HEADER.XML_COMPANY_CODE  + " text , " +
                            RKH_HEADER.XML_ESTATE  + " text , " +
                            RKH_HEADER.XML_RKH_DATE  + " text , " +
                            RKH_HEADER.XML_DIVISION  + " text , " +
                            RKH_HEADER.XML_GANG  + " text , " +
                            RKH_HEADER.XML_NIK_FOREMAN  + " text , " +
                            RKH_HEADER.XML_FOREMAN  + " text , " +
                            RKH_HEADER.XML_NIK_CLERK  + " text , " +
                            RKH_HEADER.XML_CLERK  + " text , " +
                            RKH_HEADER.XML_GPS_KOORDINAT  + " text , " +
                            RKH_HEADER.XML_STATUS  + " integer , " +
                            RKH_HEADER.XML_CREATED_DATE  + " integer , " +
                            RKH_HEADER.XML_CREATED_BY  + " text , " +
                            RKH_HEADER.XML_MODIFIED_DATE  + " integer , " +
                            RKH_HEADER.XML_MODIFIED_BY  + " text , " +
                            RKH_HEADER.XML_RKH_ID  + " text, " +
                            "primary key (" +
                            RKH_HEADER.XML_RKH_ID + ", " +
                            RKH_HEADER.XML_IMEI +
                            ")" +
                            ")";
                    sqliteDatabase.execSQL(query);

                    query = "create table " + RKH_ITEM.TABLE_NAME +
                            "(" +
                            RKH_ITEM.XML_RKH_ID  + " text , " +
                            RKH_ITEM.XML_LINE  + " text , " +
                            RKH_ITEM.XML_ACTIVITY  + " text , " +
                            RKH_ITEM.XML_BLOCK  + " text , " +
                            RKH_ITEM.XML_TARGET_OUTPUT  + " text , " +
                            RKH_ITEM.XML_SKU  + " text , " +
                            RKH_ITEM.XML_PHL  + " text , " +
                            RKH_ITEM.XML_TARGET_HASIL  + " text , " +
                            RKH_ITEM.XML_TARGET_JANJANG  + " text , " +
                            RKH_ITEM.XML_BJR  + " text , " +
                            RKH_ITEM.XML_GPS_KOORDINAT  + " text , " +
                            RKH_ITEM.XML_STATUS  + " integer , " +
                            RKH_ITEM.XML_CREATED_DATE  + " integer , " +
                            RKH_ITEM.XML_CREATED_BY  + " text , " +
                            RKH_ITEM.XML_MODIFIED_DATE  + " integer , " +
                            RKH_ITEM.XML_MODIFIED_BY  + " text  " +
                            ")";
                    sqliteDatabase.execSQL(query);
            }

        }catch(SQLiteException e){
            e.printStackTrace();
        }
    }
}
