package co.indoagri.blockcondition.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import android.util.Log;

import co.indoagri.blockcondition.model.Bluetooth;
import co.indoagri.blockcondition.model.Data.AncakPanenHeader;
import co.indoagri.blockcondition.model.Data.AncakPanenQuality;
import co.indoagri.blockcondition.model.Data.BJR;
import co.indoagri.blockcondition.model.Data.BKMHeader;
import co.indoagri.blockcondition.model.Data.BKMLine;
import co.indoagri.blockcondition.model.Data.BKMOutput;
import co.indoagri.blockcondition.model.Data.BLKPLT;
import co.indoagri.blockcondition.model.Data.BLKSBC;
import co.indoagri.blockcondition.model.Data.BLKSBCDetail;
import co.indoagri.blockcondition.model.Data.BLKSUGC;
import co.indoagri.blockcondition.model.Data.BPNHeader;
import co.indoagri.blockcondition.model.Data.BPNQuality;
import co.indoagri.blockcondition.model.Data.BPNQuantity;
import co.indoagri.blockcondition.model.Data.BlockHdrc;
import co.indoagri.blockcondition.model.Data.BlockPlanning;
import co.indoagri.blockcondition.model.Data.DayOff;
import co.indoagri.blockcondition.model.Data.DeviceAlias;
import co.indoagri.blockcondition.model.Data.MasterDownload;
import co.indoagri.blockcondition.model.Data.Penalty;
import co.indoagri.blockcondition.model.Data.ResultDetailModel;
import co.indoagri.blockcondition.model.Data.ResultModel;
import co.indoagri.blockcondition.model.Data.RunningAccount;
import co.indoagri.blockcondition.model.Data.SKB;
import co.indoagri.blockcondition.model.Data.SPBSDestination;
import co.indoagri.blockcondition.model.Data.SPBSHeader;
import co.indoagri.blockcondition.model.Data.SPBSLine;
import co.indoagri.blockcondition.model.Data.SPBSLineSummary;
import co.indoagri.blockcondition.model.Data.SPBSRunningNumber;
import co.indoagri.blockcondition.model.Data.SPTA;
import co.indoagri.blockcondition.model.Data.SPTARunningNumber;
import co.indoagri.blockcondition.model.Data.TaksasiHeader;
import co.indoagri.blockcondition.model.Data.TaksasiLine;
import co.indoagri.blockcondition.model.Data.Vendor;
import co.indoagri.blockcondition.model.Data.tblM_AccountingPeriod;
import co.indoagri.blockcondition.model.Data.tblM_BlockConditionScore;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.model.Data.tblT_BlockConditionScore;
import co.indoagri.blockcondition.model.RKH.RKH_ACTIVITY_TYPE;
import co.indoagri.blockcondition.model.RKH.RKH_HEADER;
import co.indoagri.blockcondition.model.RKH.RKH_ITEM;
import co.indoagri.blockcondition.model.Users.AbsentType;
import co.indoagri.blockcondition.model.Users.DivisionAssistant;
import co.indoagri.blockcondition.model.Users.Employee;
import co.indoagri.blockcondition.model.Users.ForemanActive;
import co.indoagri.blockcondition.model.Users.GroupHead;
import co.indoagri.blockcondition.model.Users.UserApp;
import co.indoagri.blockcondition.model.Users.UserLogin;

import static co.indoagri.blockcondition.activity.BlockActivity.SESS_PERIOD;
import static co.indoagri.blockcondition.activity.BlockActivity.SESS_ZYEAR;

public class DatabaseHandler {
    private Context context;
    private SQLiteDatabase sqliteDatabase;
    private DatabaseHelper databaseHelper;
    public DatabaseHandler(Context context) {
        this.context = context;
        this.databaseHelper = new DatabaseHelper(context);
    }

    public void openTransaction() throws SQLException {
        try {
            sqliteDatabase = databaseHelper.getWritableDatabase();
            sqliteDatabase.beginTransaction();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public void closeTransaction() {
        try {
            if(sqliteDatabase.isOpen()){
                sqliteDatabase.endTransaction();
                databaseHelper.close();
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public void commitTransaction() throws SQLException {
        try {
            sqliteDatabase.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }


    /**
     * Close opened database.
     */


    public long setData(Object object) {
        String tableName = null;
        ContentValues values = new ContentValues();
        if(object.getClass().getName().equals(tblT_BlockConditionScore.class.getName())) {
            tblT_BlockConditionScore md = (tblT_BlockConditionScore) object;
            tableName = tblT_BlockConditionScore.TABLE_NAME;
            values.put(tblT_BlockConditionScore.XML_CompanyCode, md.getAcc_CompanyCode());
            values.put(tblT_BlockConditionScore.XML_ModifiedBy, md.getAcc_ModifiedBy());
            values.put(tblT_BlockConditionScore.XML_ModifiedDateTime, md.getAcc_ModifiedDateTime());
            values.put(tblT_BlockConditionScore.XML_CreatedBy, md.getAcc_CreatedBy());
            values.put(tblT_BlockConditionScore.XML_CreatedDateTime, md.getAcc_CreatedDateTime());
            values.put(tblT_BlockConditionScore.XML_Kacangan, md.getAcc_Kacangan());
            values.put(tblT_BlockConditionScore.XML_Sanitasi, md.getAcc_Sanitasi());
            values.put(tblT_BlockConditionScore.XML_Orcytes, md.getAcc_Orcytes());
            values.put(tblT_BlockConditionScore.XML_Rayap, md.getAcc_Rayap());
            values.put(tblT_BlockConditionScore.XML_Ganoderma, md.getAcc_Ganoderma());
            values.put(tblT_BlockConditionScore.XML_Drainase, md.getAcc_Drainase());
            values.put(tblT_BlockConditionScore.XML_Gawangan, md.getAcc_Gawangan());
            values.put(tblT_BlockConditionScore.XML_TunasPokok, md.getAcc_TunasPokok());
            values.put(tblT_BlockConditionScore.XML_PasarPanen, md.getAcc_PasarPanen());
            values.put(tblT_BlockConditionScore.XML_PasarRintis, md.getAcc_PasarRintis());
            values.put(tblT_BlockConditionScore.XML_Piringan, md.getAcc_Piringan());
            values.put(tblT_BlockConditionScore.XML_TPH, md.getAcc_TPH());
            values.put(tblT_BlockConditionScore.XML_TPHBersih, md.getAcc_TPHBersih());
            values.put(tblT_BlockConditionScore.XML_TPH2, md.getAcc_TPH2());
            values.put(tblT_BlockConditionScore.XML_TPHBersih2, md.getAcc_TPHBersih2());
            values.put(tblT_BlockConditionScore.XML_BW, md.getAcc_BW());
            values.put(tblT_BlockConditionScore.XML_Pencurian, md.getAcc_Pencurian());
            values.put(tblT_BlockConditionScore.XML_Tikus, md.getAcc_Tikus());
            values.put(tblT_BlockConditionScore.XML_TitiRintis, md.getAcc_TitiRintis());
            values.put(tblT_BlockConditionScore.XML_TitiPanen, md.getAcc_TitiPanen());
            values.put(tblT_BlockConditionScore.XML_Parit, md.getAcc_Parit());
            values.put(tblT_BlockConditionScore.XML_Jembatan, md.getAcc_Jembatan());
            values.put(tblT_BlockConditionScore.XML_Jalan, md.getAcc_Jalan());
            values.put(tblT_BlockConditionScore.XML_Block, md.getAcc_Block());
            values.put(tblT_BlockConditionScore.XML_TransLevel, md.getAcc_TransLevel());
            values.put(tblT_BlockConditionScore.XML_Period, md.getAcc_Period());
            values.put(tblT_BlockConditionScore.XML_ZYear, md.getAcc_ZYear());
            values.put(tblT_BlockConditionScore.XML_Estate, md.getAcc_Estate());
            values.put(tblT_BlockConditionScore.XML_Color, md.getAcc_Color());
        }else if (object.getClass().getName().equals(tblM_BlockConditionScore.class.getName())) {
            tblM_BlockConditionScore skb = (tblM_BlockConditionScore) object;
            tableName = tblM_BlockConditionScore.TABLE_NAME;

            values.put(tblM_BlockConditionScore.XML_Color, skb.getAcc_Color());
            values.put(tblM_BlockConditionScore.XML_Interval1, skb.getAcc_Interval1());
            values.put(tblM_BlockConditionScore.XML_Interval2, skb.getAcc_Interval2());
            values.put(tblM_BlockConditionScore.XML_Score1, skb.getAcc_Score1());
            values.put(tblM_BlockConditionScore.XML_Score2, skb.getAcc_Score2());
            values.put(tblM_BlockConditionScore.XML_Score3, skb.getAcc_Score3());
            values.put(tblM_BlockConditionScore.XML_ScoreType, skb.getAcc_ScoreType());
            values.put(tblM_BlockConditionScore.XML_ValidFrom, skb.getAcc_ValidFrom());
            values.put(tblM_BlockConditionScore.XML_ValidTo, skb.getAcc_ValidTo());
        }else if(object.getClass().getName().equals(tblT_BlockCondition.class.getName())) {
            tblT_BlockCondition md = (tblT_BlockCondition) object;
            tableName = tblT_BlockCondition.TABLE_NAME;

            values.put(tblT_BlockCondition.XML_CompanyCode, md.getAcc_CompanyCode());
            values.put(tblT_BlockCondition.XML_ModifiedBy, md.getAcc_ModifiedBy());
            values.put(tblT_BlockCondition.XML_ModifiedDateTime, md.getAcc_ModifiedDateTime());
            values.put(tblT_BlockCondition.XML_CreatedBy, md.getAcc_CreatedBy());
            values.put(tblT_BlockCondition.XML_CreatedDateTime, md.getAcc_CreatedDateTime());
            values.put(tblT_BlockCondition.XML_Kacangan, md.getAcc_Kacangan());
            values.put(tblT_BlockCondition.XML_Sanitasi, md.getAcc_Sanitasi());
            values.put(tblT_BlockCondition.XML_Orcytes, md.getAcc_Orcytes());
            values.put(tblT_BlockCondition.XML_Rayap, md.getAcc_Rayap());
            values.put(tblT_BlockCondition.XML_Ganoderma, md.getAcc_Ganoderma());
            values.put(tblT_BlockCondition.XML_Drainase, md.getAcc_Drainase());
            values.put(tblT_BlockCondition.XML_Gawangan, md.getAcc_Gawangan());
            values.put(tblT_BlockCondition.XML_TunasPokok, md.getAcc_TunasPokok());
            values.put(tblT_BlockCondition.XML_PasarPanen, md.getAcc_PasarPanen());
            values.put(tblT_BlockCondition.XML_PasarRintis, md.getAcc_PasarRintis());
            values.put(tblT_BlockCondition.XML_Piringan, md.getAcc_Piringan());
            values.put(tblT_BlockCondition.XML_TPH, md.getAcc_TPH());
            values.put(tblT_BlockCondition.XML_TPHBersih, md.getAcc_TPHBersih());
            values.put(tblT_BlockCondition.XML_TPH2, md.getAcc_TPH2());
            values.put(tblT_BlockCondition.XML_TPHBersih2, md.getAcc_TPHBersih2());
            values.put(tblT_BlockCondition.XML_BW, md.getAcc_BW());
            values.put(tblT_BlockCondition.XML_Pencurian, md.getAcc_Pencurian());
            values.put(tblT_BlockCondition.XML_Tikus, md.getAcc_Tikus());
            values.put(tblT_BlockCondition.XML_TitiRintis, md.getAcc_TitiRintis());
            values.put(tblT_BlockCondition.XML_TitiPanen, md.getAcc_TitiPanen());
            values.put(tblT_BlockCondition.XML_Parit, md.getAcc_Parit());
            values.put(tblT_BlockCondition.XML_Jembatan, md.getAcc_Jembatan());
            values.put(tblT_BlockCondition.XML_Jalan, md.getAcc_Jalan());
            values.put(tblT_BlockCondition.XML_PokokCondition, md.getAcc_PokokCondition());
            values.put(tblT_BlockCondition.XML_PokokSide, md.getAcc_PokokSide());
            values.put(tblT_BlockCondition.XML_PokokLabel, md.getAcc_PokokLabel());
            values.put(tblT_BlockCondition.XML_CensusPoint, md.getAcc_CensusPoint());
            values.put(tblT_BlockCondition.XML_BlockRow, md.getAcc_BlockRow());
            values.put(tblT_BlockCondition.XML_SKB, md.getAcc_SKB());
            values.put(tblT_BlockCondition.XML_Block, md.getAcc_Block());
            values.put(tblT_BlockCondition.XML_TransLevel, md.getAcc_TransLevel());
            values.put(tblT_BlockCondition.XML_ZDate, md.getAcc_ZDate());
            values.put(tblT_BlockCondition.XML_Period, md.getAcc_Period());
            values.put(tblT_BlockCondition.XML_ZYear, md.getAcc_ZYear());
            values.put(tblT_BlockCondition.XML_Estate, md.getAcc_Estate());
            values.put(tblT_BlockCondition.XML_Flag, md.getAcc_Flag());
            values.put(tblT_BlockCondition.XML_DIVISI, md.getAcc_DIVISI());
            values.put(tblT_BlockCondition.XML_Remark, md.getAcc_Remark());
        }else if (object.getClass().getName().equals(tblM_BlockConditionScore.class.getName())) {
            tblM_BlockConditionScore skb = (tblM_BlockConditionScore) object;
            tableName = tblM_BlockConditionScore.TABLE_NAME;

            values.put(tblM_BlockConditionScore.XML_Color, skb.getAcc_Color());
            values.put(tblM_BlockConditionScore.XML_Interval1, skb.getAcc_Interval1());
            values.put(tblM_BlockConditionScore.XML_Interval2, skb.getAcc_Interval2());
            values.put(tblM_BlockConditionScore.XML_Score1, skb.getAcc_Score1());
            values.put(tblM_BlockConditionScore.XML_Score2, skb.getAcc_Score2());
            values.put(tblM_BlockConditionScore.XML_Score3, skb.getAcc_Score3());
            values.put(tblM_BlockConditionScore.XML_ScoreType, skb.getAcc_ScoreType());
            values.put(tblM_BlockConditionScore.XML_ValidFrom, skb.getAcc_ValidFrom());
            values.put(tblM_BlockConditionScore.XML_ValidTo, skb.getAcc_ValidTo());
        }else if (object.getClass().getName().equals(tblM_AccountingPeriod.class.getName())) {
            tblM_AccountingPeriod skb = (tblM_AccountingPeriod) object;
            tableName = tblM_AccountingPeriod.TABLE_NAME;

            values.put(tblM_AccountingPeriod.XML_Estate, skb.getAcc_Estate());
            values.put(tblM_AccountingPeriod.XML_ZYear, skb.getAcc_ZYear());
            values.put(tblM_AccountingPeriod.XML_Period, skb.getAcc_Period());
            values.put(tblM_AccountingPeriod.XML_ClosingDate, skb.getAcc_ClosingDate());
            values.put(tblM_AccountingPeriod.XML_Status, skb.getAcc_Status());
            values.put(tblM_AccountingPeriod.XML_Active, skb.getAcc_Active());
            values.put(tblM_AccountingPeriod.XML_CreatedBy, skb.getAcc_CreatedBy());
            values.put(tblM_AccountingPeriod.XML_CreatedDate, skb.getAcc_CreatedDate());
            values.put(tblM_AccountingPeriod.XML_ModifiedBy, skb.getAcc_ModifiedBy());
            values.put(tblM_AccountingPeriod.XML_ModifiedDate, skb.getAcc_ModifiedDate());
        }else if(object.getClass().getName().equals(MasterDownload.class.getName())){
            MasterDownload md = (MasterDownload) object;
            tableName = MasterDownload.TABLE_NAME;

            values.put(MasterDownload.XML_NAME, md.getName());
            values.put(MasterDownload.XML_FILENAME, md.getFileName());
            values.put(MasterDownload.XML_SYNC_DATE, md.getSyncDate());
            values.put(MasterDownload.XML_STATUS, md.getStatus());
        }else if (object.getClass().getName().equals(SKB.class.getName())) {
            SKB skb = (SKB) object;
            tableName = SKB.TABLE_NAME;

            values.put(SKB.XML_COMPANY_CODE, skb.getCompanyCode());
            values.put(SKB.XML_ESTATE, skb.getEstate());
            values.put(SKB.XML_BLOCK, skb.getBlock());
            values.put(SKB.XML_BARIS_SKB, skb.getBarisSkb());
            values.put(SKB.XML_VALID_FROM, skb.getValidFrom());
            values.put(SKB.XML_VALID_TO, skb.getValidTo());
            values.put(SKB.XML_BARIS_BLOCK, skb.getBarisBlok());
            values.put(SKB.XML_JUMLAH_POKOK, skb.getJumlahPokok());
            values.put(SKB.XML_POKOK_MATI, skb.getPokokMati());
            values.put(SKB.XML_TANGGAL_TANAM, skb.getTanggalTanam());
            values.put(SKB.XML_LINE_SKB, skb.getLineSkb());
        } else if (object.getClass().getName()
                .equals(BlockHdrc.class.getName())) {
            BlockHdrc blockHdrc = (BlockHdrc) object;
            tableName = BlockHdrc.TABLE_NAME;

            values.put(BlockHdrc.XML_COMPANY_CODE, blockHdrc.getCompanyCode());
            values.put(BlockHdrc.XML_ESTATE, blockHdrc.getEstate());
            values.put(BlockHdrc.XML_BLOCK, blockHdrc.getBlock());
            values.put(BlockHdrc.XML_VALID_FROM, blockHdrc.getValidFrom());
            values.put(BlockHdrc.XML_VALID_TO, blockHdrc.getValidTo());
            values.put(BlockHdrc.XML_DIVISION, blockHdrc.getDivisi());
            values.put(BlockHdrc.XML_TYPE, blockHdrc.getType());
            values.put(BlockHdrc.XML_STATUS, blockHdrc.getStatus());
            values.put(BlockHdrc.XML_PROJECT_DEFINITION,
                    blockHdrc.getProjectDefinition());
            values.put(BlockHdrc.XML_OWNER, blockHdrc.getOwner());
            values.put(BlockHdrc.XML_TGL_TANAM, blockHdrc.getTglTanam());
            values.put(BlockHdrc.XML_MANDT, blockHdrc.getMandt());
        } else if (object.getClass().getName().equals(BLKSBC.class.getName())) {
            BLKSBC blksbc = (BLKSBC) object;
            tableName = BLKSBC.TABLE_NAME;

            values.put(BLKSBC.XML_COMPANY_CODE, blksbc.getCompanyCode());
            values.put(BLKSBC.XML_ESTATE, blksbc.getEstate());
            values.put(BLKSBC.XML_BLOCK, blksbc.getBlock());
            values.put(BLKSBC.XML_VALID_FROM, blksbc.getValidFrom());
            values.put(BLKSBC.XML_BASIS_NORMAL, blksbc.getBasisNormal());
            values.put(BLKSBC.XML_BASIS_FRIDAY, blksbc.getBasisFriday());
            values.put(BLKSBC.XML_BASIS_HOLIDAY, blksbc.getBasisHoliday());
            values.put(BLKSBC.XML_PREMI_NORMAL, blksbc.getPremiNormal());
            values.put(BLKSBC.XML_PREMI_FRIDAY, blksbc.getPremiFriday());
            values.put(BLKSBC.XML_PREMI_HOLIDAY, blksbc.getPremiHoliday());
        } else if (object.getClass().getName()
                .equals(BLKSBCDetail.class.getName())) {
            BLKSBCDetail blksbcDetail = (BLKSBCDetail) object;
            tableName = BLKSBCDetail.TABLE_NAME;

            values.put(BLKSBCDetail.XML_COMPANY_CODE,
                    blksbcDetail.getCompanyCode());
            values.put(BLKSBCDetail.XML_ESTATE, blksbcDetail.getEstate());
            values.put(BLKSBCDetail.XML_BLOCK, blksbcDetail.getBlock());
            values.put(BLKSBCDetail.XML_VALID_FROM, blksbcDetail.getValidFrom());
            values.put(BLKSBCDetail.XML_MIN_VAL, blksbcDetail.getMinimumValue());
            values.put(BLKSBCDetail.XML_MAX_VAL, blksbcDetail.getMaximumValue());
            values.put(BLKSBCDetail.XML_OVER_BASIC_RATE,
                    blksbcDetail.getOverBasicRate());
        } else if (object.getClass().getName().equals(BJR.class.getName())) {
            BJR bjr = (BJR) object;
            tableName = BJR.TABLE_NAME;

            values.put(BJR.XML_COMPANY_CODE, bjr.getCompanyCode());
            values.put(BJR.XML_ESTATE, bjr.getEstate());
            values.put(BJR.XMl_EFF_DATE, bjr.getEffDate());
            values.put(BJR.XML_BLOCK, bjr.getBlock());
            values.put(BJR.XML_BJR, bjr.getBjr());
        } else if (object.getClass().getName().equals(BLKPLT.class.getName())) {
            BLKPLT blkplt = (BLKPLT) object;
            tableName = BLKPLT.TABLE_NAME;

            values.put(BLKPLT.XML_COMPANY_CODE, blkplt.getCompanyCode());
            values.put(BLKPLT.XML_ESTATE, blkplt.getEstate());
            values.put(BLKPLT.XML_BLOCK, blkplt.getBlock());
            values.put(BLKPLT.XML_VALID_FROM, blkplt.getValidFrom());
            values.put(BLKPLT.XML_VALID_TO, blkplt.getValidTo());
            values.put(BLKPLT.XML_CROP_TYPE, blkplt.getCropType());
            values.put(BLKPLT.XML_PREVIOUS_CROP, blkplt.getPreviousCrop());
            values.put(BLKPLT.XML_FINISH_DATE, blkplt.getFinishDate());
            values.put(BLKPLT.XML_REFERENCE, blkplt.getReference());
            values.put(BLKPLT.XML_JARAK_TANAM, blkplt.getJarakTanam());
            values.put(BLKPLT.XML_HARVESTING_DATE, blkplt.getHarvestingDate());
            values.put(BLKPLT.XML_HARVESTED, blkplt.getHarvested());
            values.put(BLKPLT.XML_PLAN_DATE, blkplt.getPlanDate());
            values.put(BLKPLT.XML_TOPOGRAPHY, blkplt.getTopography());
            values.put(BLKPLT.XML_SOIL_TYPE, blkplt.getSoilType());
            values.put(BLKPLT.XML_SOIL_CATEGORY, blkplt.getSoilCategory());
            values.put(BLKPLT.XML_PROD_TREES, blkplt.getProdTrees());
        } else if (object.getClass().getName().equals(Employee.class.getName())) {
            Employee employee = (Employee) object;
            tableName = Employee.TABLE_NAME;

            values.put(Employee.XML_COMPANY_CODE, employee.getCompanyCode());
            values.put(Employee.XML_ESTATE, employee.getEstate());
            values.put(Employee.XML_FISCAL_YEAR, employee.getFiscalYear());
            values.put(Employee.XML_FISCAL_PERIOD, employee.getFiscalPeriod());
            values.put(Employee.XML_NIK, employee.getNik());
            values.put(Employee.XML_NAME, employee.getName());
            values.put(Employee.XML_TERM_DATE, employee.getTermDate());
            values.put(Employee.XML_DIVISION, employee.getDivision());
            values.put(Employee.XML_ROLE_ID, employee.getRoleId());
            values.put(Employee.XML_JOB_POS, employee.getJobPos());
            values.put(Employee.XML_GANG, employee.getGang());
            values.put(Employee.XML_COST_CENTER, employee.getCostCenter());
            values.put(Employee.XML_EMP_TYPE, employee.getEmpType());
            values.put(Employee.XML_VALID_FROM, employee.getValidFrom());
            values.put(Employee.XML_HARVESTER_CODE, employee.getHarvesterCode());
        } else if (object.getClass().getName().equals(UserApp.class.getName())) {
            UserApp userApp = (UserApp) object;
            tableName = UserApp.TABLE_NAME;

            values.put(UserApp.XML_NIK, userApp.getNik());
            values.put(UserApp.XML_USERNAME, userApp.getUsername());
            values.put(UserApp.XML_PASSWORD, userApp.getPassword());
            values.put(UserApp.XML_VALID_TO, userApp.getValidTo());
            values.put(UserApp.XML_CREATED_DATE, userApp.getCreatedDate());
            values.put(UserApp.XML_CREATED_BY, userApp.getCreatedBy());
        } else if (object.getClass().getName()
                .equals(AbsentType.class.getName())) {
            AbsentType absentType = (AbsentType) object;
            tableName = AbsentType.TABLE_NAME;

            values.put(AbsentType.XML_COMPANY_CODE, absentType.getCompanyCode());
            values.put(AbsentType.XML_ABSENT_TYPE, absentType.getAbsentType());
            values.put(AbsentType.XML_DESCRIPTION, absentType.getDescription());
            values.put(AbsentType.XML_HKRLLO, absentType.getHkrllo());
            values.put(AbsentType.XML_HKRLHI, absentType.getHkrlhi());
            values.put(AbsentType.XML_HKPYLO, absentType.getHkpylo());
            values.put(AbsentType.XML_HKPYHI, absentType.getHkpyhi());
            values.put(AbsentType.XML_HKVLLO, absentType.getHkvllo());
            values.put(AbsentType.XML_HKVLHI, absentType.getHkvlhi());
            values.put(AbsentType.XML_HKMEIN, absentType.getHkmein());
            values.put(AbsentType.XML_AGROUP, absentType.getAgroup());
        } else if (object.getClass().getName().equals(DayOff.class.getName())) {
            DayOff dayOff = (DayOff) object;
            tableName = DayOff.TABLE_NAME;

            values.put(DayOff.XML_ESTATE, dayOff.getEstate());
            values.put(DayOff.XML_DATE, dayOff.getDate());
            values.put(DayOff.XML_DAY_OFF_TYPE, dayOff.getDayOffType());
            values.put(DayOff.XML_DESCRIPTION, dayOff.getDescription());
        }else if(object.getClass().getName().equals(DivisionAssistant.class.getName())){
            DivisionAssistant assistantDivision = (DivisionAssistant) object;
            tableName = DivisionAssistant.TABLE_NAME;

            values.put(DivisionAssistant.XML_ESTATE, assistantDivision.getEstate());
            values.put(DivisionAssistant.XML_DIVISION, assistantDivision.getDivision());
            values.put(DivisionAssistant.XML_SPRAS, assistantDivision.getSpras());
            values.put(DivisionAssistant.XML_DESCRIPTION, assistantDivision.getDescription());
            values.put(DivisionAssistant.XML_ASSISTANT, assistantDivision.getAssistant());
            values.put(DivisionAssistant.XML_DISTANCE_TO_MILL, assistantDivision.getDistanceToMill());
            values.put(DivisionAssistant.XML_UOM, assistantDivision.getUom());
            values.put(DivisionAssistant.XML_ASSISTANT_NAME, assistantDivision.getAssistantName());
            values.put(DivisionAssistant.XML_LIFNR, assistantDivision.getLifnr());
        } else if (object.getClass().getName()
                .equals(ForemanActive.class.getName())) {
            ForemanActive foremanActive = (ForemanActive) object;
            tableName = ForemanActive.TABLE_NAME;

            values.put(ForemanActive.XML_COMPANY_CODE, foremanActive.getCompanyCode());
            values.put(ForemanActive.XML_ESTATE, foremanActive.getEstate());
            values.put(ForemanActive.XML_FISCAL_YEAR, foremanActive.getFiscalYear());
            values.put(ForemanActive.XML_FISCAL_PERIOD, foremanActive.getFiscalPeriod());
            values.put(ForemanActive.XML_NIK, foremanActive.getNik());
            values.put(ForemanActive.XML_NAME, foremanActive.getName());
            values.put(ForemanActive.XML_TERM_DATE, foremanActive.getTermDate());
            values.put(ForemanActive.XML_DIVISION, foremanActive.getDivision());
            values.put(ForemanActive.XML_ROLE_ID, foremanActive.getRoleId());
            values.put(ForemanActive.XML_JOB_POS, foremanActive.getJobPos());
            values.put(ForemanActive.XML_GANG, foremanActive.getGang());
            values.put(ForemanActive.XML_COST_CENTER, foremanActive.getCostCenter());
            values.put(ForemanActive.XML_EMP_TYPE, foremanActive.getEmpType());
            values.put(ForemanActive.XML_VALID_FROM, foremanActive.getValidFrom());
            values.put(ForemanActive.XML_HARVESTER_CODE, foremanActive.getHarvesterCode());
        } else if (object.getClass().getName()
                .equals(UserLogin.class.getName())) {
            UserLogin userLogin = (UserLogin) object;
            tableName = UserLogin.TABLE_NAME;

            values.put(UserLogin.XML_COMPANY_CODE, userLogin.getCompanyCode());
            values.put(UserLogin.XML_ESTATE, userLogin.getEstate());
            values.put(UserLogin.XML_FISCAL_YEAR, userLogin.getFiscalYear());
            values.put(UserLogin.XML_FISCAL_PERIOD, userLogin.getFiscalPeriod());
            values.put(UserLogin.XML_NIK, userLogin.getNik());
            values.put(UserLogin.XML_NAME, userLogin.getName());
            values.put(UserLogin.XML_TERM_DATE, userLogin.getTermDate());
            values.put(UserLogin.XML_DIVISION, userLogin.getDivision());
            values.put(UserLogin.XML_ROLE_ID, userLogin.getRoleId());
            values.put(UserLogin.XML_JOB_POS, userLogin.getJobPos());
            values.put(UserLogin.XML_GANG, userLogin.getGang());
            values.put(UserLogin.XML_COST_CENTER, userLogin.getCostCenter());
            values.put(UserLogin.XML_EMP_TYPE, userLogin.getEmpType());
            values.put(UserLogin.XML_VALID_FROM, userLogin.getValidFrom());
            values.put(UserLogin.XML_HARVESTER_CODE, userLogin.getHarvesterCode());
        } else if (object.getClass().getName()
                .equals(BPNHeader.class.getName())) {
            BPNHeader bpnHeader = (BPNHeader) object;
            tableName = BPNHeader.TABLE_NAME;

            values.put(BPNHeader.XML_BPN_ID, bpnHeader.getBpnId());
            values.put(BPNHeader.XML_IMEI, bpnHeader.getImei());
            values.put(BPNHeader.XML_COMPANY_CODE, bpnHeader.getCompanyCode());
            values.put(BPNHeader.XML_ESTATE, bpnHeader.getEstate());
            values.put(BPNHeader.XML_BPN_DATE, bpnHeader.getBpnDate());
            values.put(BPNHeader.XML_DIVISION, bpnHeader.getDivision());
            values.put(BPNHeader.XML_GANG, bpnHeader.getGang());
            values.put(BPNHeader.XML_LOCATION, bpnHeader.getLocation());
            values.put(BPNHeader.XML_TPH, bpnHeader.getTph());
            values.put(BPNHeader.XML_NIK_HARVESTER, bpnHeader.getNikHarvester());
            values.put(BPNHeader.XML_HARVESTER, bpnHeader.getHarvester());
            values.put(BPNHeader.XML_NIK_FOREMAN, bpnHeader.getNikForeman());
            values.put(BPNHeader.XML_FOREMAN, bpnHeader.getForeman());
            values.put(BPNHeader.XML_NIK_CLERK, bpnHeader.getNikClerk());
            values.put(BPNHeader.XML_CLERK, bpnHeader.getClerk());
            values.put(BPNHeader.XML_USE_GERDANG, bpnHeader.isUseGerdang());
            values.put(BPNHeader.XML_CROP, bpnHeader.getCrop());
            values.put(BPNHeader.XML_GPS_KOORDINAT, bpnHeader.getGpsKoordinat());
            values.put(BPNHeader.XML_PHOTO, bpnHeader.getPhoto());
            values.put(BPNHeader.XML_STATUS, bpnHeader.getStatus());
            values.put(BPNHeader.XML_SPBS_NUMBER, bpnHeader.getSpbsNumber());
            values.put(BPNHeader.XML_CREATED_DATE, bpnHeader.getCreatedDate());
            values.put(BPNHeader.XML_CREATED_BY, bpnHeader.getCreatedBy());
            values.put(BPNHeader.XML_MODIFIED_DATE, bpnHeader.getModifiedDate());
            values.put(BPNHeader.XML_MODIFIED_BY, bpnHeader.getModifiedBy());
        } else if (object.getClass().getName()
                .equals(BPNQuantity.class.getName())) {
            BPNQuantity bpnQuantity = (BPNQuantity) object;
            tableName = BPNQuantity.TABLE_NAME;

            values.put(BPNQuantity.XML_BPN_ID, bpnQuantity.getBpnId());
            values.put(BPNQuantity.XML_IMEI, bpnQuantity.getImei());
            values.put(BPNQuantity.XML_COMPANY_CODE,bpnQuantity.getCompanyCode());
            values.put(BPNQuantity.XML_ESTATE, bpnQuantity.getEstate());
            values.put(BPNQuantity.XML_BPN_DATE,bpnQuantity.getBpnDate());
            values.put(BPNQuantity.XML_DIVISION, bpnQuantity.getDivision());
            values.put(BPNQuantity.XML_GANG, bpnQuantity.getGang());
            values.put(BPNQuantity.XML_LOCATION, bpnQuantity.getLocation());
            values.put(BPNQuantity.XML_TPH, bpnQuantity.getTph());
            values.put(BPNQuantity.XML_NIK_HARVESTER,
                    bpnQuantity.getNikHarvester());
            values.put(BPNQuantity.XML_CROP,
                    bpnQuantity.getCrop());
            values.put(BPNQuantity.XML_ACHIEVEMENT_CODE,
                    bpnQuantity.getAchievementCode());
            values.put(BPNQuantity.XML_QUANTITY, bpnQuantity.getQuantity());
            values.put(BPNQuantity.XML_QUANTITY_REMAINING, bpnQuantity.getQuantityRemaining());
            values.put(BPNQuantity.XML_STATUS, bpnQuantity.getStatus());
            values.put(BPNQuantity.XML_CREATED_DATE,
                    bpnQuantity.getCreatedDate());
            values.put(BPNQuantity.XML_CREATED_BY, bpnQuantity.getCreatedBy());
            values.put(BPNQuantity.XML_MODIFIED_DATE,
                    bpnQuantity.getModifiedDate());
            values.put(BPNQuantity.XML_MODIFIED_BY, bpnQuantity.getModifiedBy());
        } else if (object.getClass().getName()
                .equals(BPNQuality.class.getName())) {
            BPNQuality bpnQuality = (BPNQuality) object;
            tableName = BPNQuality.TABLE_NAME;

            values.put(BPNQuality.XML_BPN_ID, bpnQuality.getBpnId());
            values.put(BPNQuality.XML_IMEI, bpnQuality.getImei());
            values.put(BPNQuality.XML_COMPANY_CODE, bpnQuality.getCompanyCode());
            values.put(BPNQuality.XML_ESTATE, bpnQuality.getEstate());
            values.put(BPNQuality.XML_BPN_DATE, bpnQuality.getBpnDate());
            values.put(BPNQuality.XML_DIVISION, bpnQuality.getDivision());
            values.put(BPNQuality.XML_GANG, bpnQuality.getGang());
            values.put(BPNQuality.XML_LOCATION, bpnQuality.getLocation());
            values.put(BPNQuality.XML_TPH, bpnQuality.getTph());
            values.put(BPNQuality.XML_NIK_HARVESTER,
                    bpnQuality.getNikHarvester());
            values.put(BPNQuality.XML_CROP,
                    bpnQuality.getCrop());
            values.put(BPNQuality.XML_ACHIEVEMENT_CODE, bpnQuality.getAchievementCode());
            values.put(BPNQuality.XML_QUALITY_CODE, bpnQuality.getQualityCode());
            values.put(BPNQuality.XML_QUANTITY, bpnQuality.getQuantity());
            values.put(BPNQuality.XML_STATUS, bpnQuality.getStatus());
            values.put(BPNQuality.XML_CREATED_DATE, bpnQuality.getCreatedDate());
            values.put(BPNQuality.XML_CREATED_BY, bpnQuality.getCreatedBy());
            values.put(BPNQuality.XML_MODIFIED_DATE,
                    bpnQuality.getModifiedDate());
            values.put(BPNQuality.XML_MODIFIED_BY, bpnQuality.getModifiedBy());
        } else if (object.getClass().getName()
                .equals(BKMHeader.class.getName())) {
            BKMHeader bkmHeader = (BKMHeader) object;
            tableName = BKMHeader.TABLE_NAME;

            values.put(BKMHeader.XML_IMEI, bkmHeader.getImei());
            values.put(BKMHeader.XML_COMPANY_CODE, bkmHeader.getCompanyCode());
            values.put(BKMHeader.XML_ESTATE, bkmHeader.getEstate());
            values.put(BKMHeader.XML_BKM_DATE, bkmHeader.getBkmDate());
            values.put(BKMHeader.XML_DIVISION, bkmHeader.getDivision());
            values.put(BKMHeader.XML_GANG, bkmHeader.getGang());
            values.put(BKMHeader.XML_NIK_FOREMAN, bkmHeader.getNikForeman());
            values.put(BKMHeader.XML_FOREMAN, bkmHeader.getForeman());
            values.put(BKMHeader.XML_NIK_CLERK, bkmHeader.getNikClerk());
            values.put(BKMHeader.XML_CLERK, bkmHeader.getClerk());
            values.put(BKMHeader.XML_GPS_KOORDINAT, bkmHeader.getGpsKoordinat());
            values.put(BKMHeader.XML_STATUS, bkmHeader.getStatus());
            values.put(BKMHeader.XML_CREATED_DATE, bkmHeader.getCreatedDate());
            values.put(BKMHeader.XML_CREATED_BY, bkmHeader.getCreatedBy());
            values.put(BKMHeader.XML_MODIFIED_DATE, bkmHeader.getModifiedDate());
            values.put(BKMHeader.XML_MODIFIED_BY, bkmHeader.getModifiedBy());
        } else if (object.getClass().getName().equals(BKMLine.class.getName())) {
            BKMLine bkmLine = (BKMLine) object;
            tableName = BKMLine.TABLE_NAME;

            values.put(BKMLine.XML_IMEI, bkmLine.getImei());
            values.put(BKMLine.XML_COMPANY_CODE, bkmLine.getCompanyCode());
            values.put(BKMLine.XML_ESTATE, bkmLine.getEstate());
            values.put(BKMLine.XML_BKM_DATE, bkmLine.getBkmDate());
            values.put(BKMLine.XML_DIVISION, bkmLine.getDivision());
            values.put(BKMLine.XML_GANG, bkmLine.getGang());
            values.put(BKMLine.XML_NIK, bkmLine.getNik());
            values.put(BKMLine.XML_NAME, bkmLine.getName());
            values.put(BKMLine.XML_ABSENT_TYPE, bkmLine.getAbsentType());
            values.put(BKMLine.XML_MANDAYS, bkmLine.getMandays());
            values.put(BKMLine.XML_UOM, bkmLine.getUom());
            values.put(BKMLine.XML_USE_GERDANG, bkmLine.getUseGerdang());
            values.put(BKMLine.XML_STATUS, bkmLine.getStatus());
            values.put(BKMLine.XML_CREATED_DATE, bkmLine.getCreatedDate());
            values.put(BKMLine.XML_CREATED_BY, bkmLine.getCreatedBy());
            values.put(BKMLine.XML_MODIFIED_DATE, bkmLine.getModifiedDate());
            values.put(BKMLine.XML_MODIFIED_BY, bkmLine.getModifiedBy());
        } else if (object.getClass().getName()
                .equals(BKMOutput.class.getName())) {
            BKMOutput bkmOutput = (BKMOutput) object;
            tableName = BKMOutput.TABLE_NAME;

            values.put(BKMOutput.XML_IMEI, bkmOutput.getImei());
            values.put(BKMOutput.XML_COMPANY_CODE, bkmOutput.getCompanyCode());
            values.put(BKMOutput.XML_ESTATE, bkmOutput.getEstate());
            values.put(BKMOutput.XML_BKM_DATE, bkmOutput.getBkmDate());
            values.put(BKMOutput.XML_DIVISION, bkmOutput.getDivision());
            values.put(BKMOutput.XML_GANG, bkmOutput.getGang());
            values.put(BKMOutput.XML_BLOCK, bkmOutput.getBlock());
            values.put(BKMOutput.XML_NIK, bkmOutput.getNik());
            values.put(BKMOutput.XML_NAME, bkmOutput.getName());
            values.put(BKMOutput.XML_OUTPUT, bkmOutput.getOutput());
            values.put(BKMOutput.XML_UOM, bkmOutput.getUom());
            values.put(BKMOutput.XML_STATUS, bkmOutput.getStatus());
            values.put(BKMOutput.XML_CREATED_DATE, bkmOutput.getCreatedDate());
            values.put(BKMOutput.XML_CREATED_BY, bkmOutput.getCreatedBy());
            values.put(BKMOutput.XML_MODIFIED_DATE, bkmOutput.getModifiedDate());
            values.put(BKMOutput.XML_MODIFIED_BY, bkmOutput.getModifiedBy());
        } else if (object.getClass().getName()
                .equals(SPBSHeader.class.getName())) {
            SPBSHeader spbsHeader = (SPBSHeader) object;
            tableName = SPBSHeader.TABLE_NAME;

            values.put(SPBSHeader.XML_IMEI, spbsHeader.getImei());
            values.put(SPBSHeader.XML_YEAR, spbsHeader.getYear());
            values.put(SPBSHeader.XML_COMPANY_CODE, spbsHeader.getCompanyCode());
            values.put(SPBSHeader.XML_ESTATE, spbsHeader.getEstate());
            values.put(SPBSHeader.XML_CROP, spbsHeader.getCrop());
            values.put(SPBSHeader.XML_SPBS_NUMBER, spbsHeader.getSpbsNumber());
            values.put(SPBSHeader.XML_SPBS_DATE, spbsHeader.getSpbsDate());
            values.put(SPBSHeader.XML_DEST_ID, spbsHeader.getDestId());
            values.put(SPBSHeader.XML_DEST_DESC, spbsHeader.getDestDesc());
            values.put(SPBSHeader.XML_DEST_TYPE, spbsHeader.getDestType());
            values.put(SPBSHeader.XML_DIVISION, spbsHeader.getDivision());
            values.put(SPBSHeader.XML_NIK_ASSISTANT,
                    spbsHeader.getNikAssistant());
            values.put(SPBSHeader.XML_ASSISTANT, spbsHeader.getAssistant());
            values.put(SPBSHeader.XML_NIK_CLERK, spbsHeader.getNikClerk());
            values.put(SPBSHeader.XML_CLERK, spbsHeader.getClerk());
            values.put(SPBSHeader.XML_NIK_DRIVER, spbsHeader.getNikDriver());
            values.put(SPBSHeader.XML_DRIVER, spbsHeader.getDriver());
            values.put(SPBSHeader.XML_NIK_KERNET, spbsHeader.getNikKernet());
            values.put(SPBSHeader.XML_KERNET, spbsHeader.getKernet());
            values.put(SPBSHeader.XML_LICENSE_PLATE, spbsHeader.getLicensePlate());
            values.put(SPBSHeader.XML_RUNNING_ACCOUNT, spbsHeader.getRunningAccount());
            values.put(SPBSHeader.XML_GPS_KOORDINAT,
                    spbsHeader.getGpsKoordinat());
            values.put(SPBSHeader.XML_IS_SAVE, spbsHeader.getIsSave());
            values.put(SPBSHeader.XML_STATUS, spbsHeader.getStatus());
            values.put(SPBSHeader.XML_CREATED_DATE, spbsHeader.getCreatedDate());
            values.put(SPBSHeader.XML_CREATED_BY, spbsHeader.getCreatedBy());
            values.put(SPBSHeader.XML_MODIFIED_DATE,
                    spbsHeader.getModifiedDate());
            values.put(SPBSHeader.XML_MODIFIED_BY, spbsHeader.getModifiedBy());
        } else if (object.getClass().getName().equals(SPBSLine.class.getName())) {
            SPBSLine spbsLine = (SPBSLine) object;
            tableName = SPBSLine.TABLE_NAME;

            values.put(SPBSLine.XML_ID, spbsLine.getId());
            values.put(SPBSLine.XML_IMEI, spbsLine.getImei());
            values.put(SPBSLine.XML_YEAR, spbsLine.getYear());
            values.put(SPBSLine.XML_COMPANY_CODE, spbsLine.getCompanyCode());
            values.put(SPBSLine.XML_ESTATE, spbsLine.getEstate());
            values.put(SPBSLine.XML_CROP, spbsLine.getCrop());
            values.put(SPBSLine.XML_SPBS_NUMBER, spbsLine.getSpbsNumber());
            values.put(SPBSLine.XML_SPBS_DATE, spbsLine.getSpbsDate());
            values.put(SPBSLine.XML_BLOCK, spbsLine.getBlock());
            values.put(SPBSLine.XML_TPH, spbsLine.getTph());
            values.put(SPBSLine.XML_BPN_DATE, spbsLine.getBpnDate());
            values.put(SPBSLine.XML_BPN_ID, spbsLine.getBpnId());
            values.put(SPBSLine.XML_SPBS_REF, spbsLine.getSpbsRef());
            values.put(SPBSLine.XML_SPBS_NEXT, spbsLine.getSpbsNext());
            values.put(SPBSLine.XML_ACHIEVEMENT_CODE, spbsLine.getAchievementCode());
            values.put(SPBSLine.XML_QUANTITY, spbsLine.getQuantity());
            values.put(SPBSLine.XML_QUANTITY_ANGKUT, spbsLine.getQuantityAngkut());
            values.put(SPBSLine.XML_QUANTITY_REMAINING, spbsLine.getQuantityRemaining());
            values.put(SPBSLine.XML_TOTAL_HARVESTER, spbsLine.getTotalHarvester());
            values.put(SPBSLine.XML_UOM, spbsLine.getUom());
            values.put(SPBSLine.XML_GPS_KOORDINAT, spbsLine.getGpsKoordinat());
            values.put(SPBSLine.XML_IS_SAVE, spbsLine.getIsSave());
            values.put(SPBSLine.XML_STATUS, spbsLine.getStatus());
            values.put(SPBSLine.XML_CREATED_DATE, spbsLine.getCreatedDate());
            values.put(SPBSLine.XML_CREATED_BY, spbsLine.getCreatedBy());
            values.put(SPBSLine.XML_MODIFIED_DATE, spbsLine.getModifiedDate());
            values.put(SPBSLine.XML_MODIFIED_BY, spbsLine.getModifiedBy());
        } else if (object.getClass().getName()
                .equals(SPBSRunningNumber.class.getName())) {
            SPBSRunningNumber spbsNumber = (SPBSRunningNumber) object;
            tableName = SPBSRunningNumber.TABLE_NAME;

            values.put(SPBSRunningNumber.XML_ID, spbsNumber.getId());
            values.put(SPBSRunningNumber.XML_ESTATE, spbsNumber.getEstate());
            values.put(SPBSRunningNumber.XML_DIVISION, spbsNumber.getDivision());
            values.put(SPBSRunningNumber.XML_YEAR, spbsNumber.getYear());
            values.put(SPBSRunningNumber.XML_MONTH, spbsNumber.getMonth());
            values.put(SPBSRunningNumber.XML_IMEI, spbsNumber.getImei());
            values.put(SPBSRunningNumber.XML_RUNNING_NUMBER, spbsNumber.getRunningNumber());
            values.put(SPBSRunningNumber.XML_DEVICE_ALIAS, spbsNumber.getDeviceAlias());
        }else if(object.getClass().getName().equals(Bluetooth.class.getName())){
            Bluetooth bluetooth = (Bluetooth) object;
            tableName = Bluetooth.TABLE_NAME;

            values.put(Bluetooth.XML_NAME, bluetooth.getName());
            values.put(Bluetooth.XML_ADDRESS, bluetooth.getAddress());
            values.put(Bluetooth.XML_IS_PAIRED, bluetooth.isPaired());
            values.put(Bluetooth.XML_IS_SELECTED, bluetooth.isSelected());
        }else if(object.getClass().getName().equals(RunningAccount.class.getName())){
            RunningAccount vehicle = (RunningAccount) object;
            tableName = RunningAccount.TABLE_NAME;

            values.put(RunningAccount.XML_COMPANY_CODE, vehicle.getCompanyCode());
            values.put(RunningAccount.XML_ESTATE, vehicle.getEstate());
            values.put(RunningAccount.XML_RUNNING_ACCOUNT, vehicle.getRunningAccount());
            values.put(RunningAccount.XML_LICENSE_PLATE, vehicle.getLicensePlate());
            values.put(RunningAccount.XML_LIFNR, vehicle.getLifnr());
            values.put(RunningAccount.XML_OWNERSHIPFLAG, vehicle.getOwnerShipFlag());
        }else if(object.getClass().getName().equals(TaksasiHeader.class.getName())){
            TaksasiHeader taksasiHeader = (TaksasiHeader) object;
            tableName = TaksasiHeader.TABLE_NAME;

            values.put(TaksasiHeader.XML_IMEI, taksasiHeader.getImei());
            values.put(TaksasiHeader.XML_COMPANY_CODE, taksasiHeader.getCompanyCode());
            values.put(TaksasiHeader.XML_ESTATE, taksasiHeader.getEstate());
            values.put(TaksasiHeader.XML_DIVISION, taksasiHeader.getDivision());
            values.put(TaksasiHeader.XML_TAKSASI_DATE, taksasiHeader.getTaksasiDate());
            values.put(TaksasiHeader.XML_BLOCK, taksasiHeader.getBlock());
            values.put(TaksasiHeader.XML_CROP, taksasiHeader.getCrop());
            values.put(TaksasiHeader.XML_NIK_FOREMAN, taksasiHeader.getNikForeman());
            values.put(TaksasiHeader.XML_FOREMAN, taksasiHeader.getForeman());
            values.put(TaksasiHeader.XML_PROD_TREES, taksasiHeader.getProdTrees());
            values.put(TaksasiHeader.XML_BJR, taksasiHeader.getBjr());
            values.put(TaksasiHeader.XML_GPS_KOORDINAT, taksasiHeader.getGpsKoordinat());
            values.put(TaksasiHeader.XML_IS_SAVE, taksasiHeader.getIsSave());
            values.put(TaksasiHeader.XML_STATUS, taksasiHeader.getStatus());
            values.put(TaksasiHeader.XML_CREATED_DATE, taksasiHeader.getCreatedDate());
            values.put(TaksasiHeader.XML_CREATED_BY, taksasiHeader.getCreatedBy());
            values.put(TaksasiHeader.XML_MODIFIED_DATE, taksasiHeader.getModifiedDate());
            values.put(TaksasiHeader.XML_MODIFIED_BY, taksasiHeader.getModifiedBy());
        }else if(object.getClass().getName().equals(TaksasiLine.class.getName())){
            TaksasiLine taksasiLine = (TaksasiLine) object;
            tableName = TaksasiLine.TABLE_NAME;

            values.put(TaksasiLine.XML_IMEI, taksasiLine.getImei());
            values.put(TaksasiLine.XML_COMPANY_CODE, taksasiLine.getCompanyCode());
            values.put(TaksasiLine.XML_ESTATE, taksasiLine.getEstate());
            values.put(TaksasiLine.XML_DIVISION, taksasiLine.getDivision());
            values.put(TaksasiLine.XML_TAKSASI_DATE, taksasiLine.getTaksasiDate());
            values.put(TaksasiLine.XML_BLOCK, taksasiLine.getBlock());
            values.put(TaksasiLine.XML_CROP, taksasiLine.getCrop());
            values.put(TaksasiLine.XML_BARIS_SKB, taksasiLine.getBarisSkb());
            values.put(TaksasiLine.XML_BARIS_BLOCK, taksasiLine.getBarisBlock());
            values.put(TaksasiLine.XML_LINE_SKB, taksasiLine.getLineSkb());
            values.put(TaksasiLine.XML_QTY_POKOK, taksasiLine.getQtyPokok());
            values.put(TaksasiLine.XML_QTY_JANJANG, taksasiLine.getQtyJanjang());
            values.put(TaksasiLine.XML_GPS_KOORDINAT, taksasiLine.getGpsKoordinat());
            values.put(TaksasiLine.XML_IS_SAVE, taksasiLine.getIsSave());
            values.put(TaksasiLine.XML_STATUS, taksasiLine.getStatus());
            values.put(TaksasiLine.XML_CREATED_DATE, taksasiLine.getCreatedDate());
            values.put(TaksasiLine.XML_CREATED_BY, taksasiLine.getCreatedBy());
            values.put(TaksasiLine.XML_MODIFIED_DATE, taksasiLine.getModifiedDate());
            values.put(TaksasiLine.XML_MODIFIED_BY, taksasiLine.getModifiedBy());
        }else if(object.getClass().getName().equals(Penalty.class.getName())){
            Penalty penalty = (Penalty) object;
            tableName = Penalty.TABLE_NAME;

            values.put(Penalty.XML_PENALTY_CODE, penalty.getPenaltyCode());
            values.put(Penalty.XML_PENALTY_DESC, penalty.getPenaltyDesc());
            values.put(Penalty.XML_UOM, penalty.getUom());
            values.put(Penalty.XMl_CROP_TYPE, penalty.getCropType());
            values.put(Penalty.XML_IS_LOADING, penalty.getIsLoading());
            values.put(Penalty.XML_IS_ANCAK,  penalty.getIsAncak());
        }else if(object.getClass().getName().equals(DeviceAlias.class.getName())){
            DeviceAlias deviceAlias = (DeviceAlias) object;
            tableName = DeviceAlias.TABLE_NAME;

            values.put(DeviceAlias.XML_DEVICE_ALIAS,  deviceAlias.getDeviceAlias());
        }else if(object.getClass().getName().equals(BlockPlanning.class.getName())){
            BlockPlanning blockPlanning = (BlockPlanning) object;
            tableName = BlockPlanning.TABLE_NAME;

            values.put(BlockPlanning.XML_BLOCK,  blockPlanning.getBlock());
            values.put(BlockPlanning.XML_CREATED_DATE,  blockPlanning.getCreatedDate());
            values.put(BlockPlanning.XML_CREATED_BY,  blockPlanning.getCreatedBy());
        }else if(object.getClass().getName().equals(AncakPanenHeader.class.getName())){
            AncakPanenHeader ancakPanenHeader = (AncakPanenHeader) object;
            tableName = AncakPanenHeader.TABLE_NAME;

            values.put(AncakPanenHeader.XML_ANCAK_PANEN_ID, ancakPanenHeader.getAncakPanenId());
            values.put(AncakPanenHeader.XML_IMEI, ancakPanenHeader.getImei());
            values.put(AncakPanenHeader.XML_COMPANY_CODE, ancakPanenHeader.getCompanyCode());
            values.put(AncakPanenHeader.XML_ESTATE, ancakPanenHeader.getEstate());
            values.put(AncakPanenHeader.XML_ANCAK_DATE, ancakPanenHeader.getAncakDate());
            values.put(AncakPanenHeader.XML_DIVISION, ancakPanenHeader.getDivision());
            values.put(AncakPanenHeader.XML_GANG, ancakPanenHeader.getGang());
            values.put(AncakPanenHeader.XML_LOCATION, ancakPanenHeader.getLocation());
            values.put(AncakPanenHeader.XML_TPH, ancakPanenHeader.getTph());
            values.put(AncakPanenHeader.XML_NIK_HARVESTER, ancakPanenHeader.getNikHarvester());
            values.put(AncakPanenHeader.XML_HARVESTER, ancakPanenHeader.getHarvester());
            values.put(AncakPanenHeader.XML_NIK_FOREMAN, ancakPanenHeader.getNikForeman());
            values.put(AncakPanenHeader.XML_FOREMAN, ancakPanenHeader.getForeman());
            values.put(AncakPanenHeader.XML_NIK_CLERK, ancakPanenHeader.getNikClerk());
            values.put(AncakPanenHeader.XML_CLERK, ancakPanenHeader.getClerk());
            values.put(AncakPanenHeader.XML_CROP, ancakPanenHeader.getCrop());
            values.put(AncakPanenHeader.XML_GPS_KOORDINAT, ancakPanenHeader.getGpsKoordinat());
            values.put(AncakPanenHeader.XML_STATUS, ancakPanenHeader.getStatus());
            values.put(AncakPanenHeader.XML_CREATED_DATE, ancakPanenHeader.getCreatedDate());
            values.put(AncakPanenHeader.XML_CREATED_BY, ancakPanenHeader.getCreatedBy());
            values.put(AncakPanenHeader.XML_MODIFIED_DATE, ancakPanenHeader.getModifiedDate());
            values.put(AncakPanenHeader.XML_MODIFIED_BY, ancakPanenHeader.getModifiedBy());
        }else if(object.getClass().getName().equals(AncakPanenQuality.class.getName())){
            AncakPanenQuality ancakPanenQuality = (AncakPanenQuality) object;
            tableName = AncakPanenQuality.TABLE_NAME;

            values.put(AncakPanenQuality.XML_ANCAK_PANEN_ID, ancakPanenQuality.getAncakPanenId());
            values.put(AncakPanenQuality.XML_IMEI, ancakPanenQuality.getImei());
            values.put(AncakPanenQuality.XML_COMPANY_CODE, ancakPanenQuality.getCompanyCode());
            values.put(AncakPanenQuality.XML_ESTATE, ancakPanenQuality.getEstate());
            values.put(AncakPanenQuality.XML_ANCAK_DATE, ancakPanenQuality.getAncakDate());
            values.put(AncakPanenQuality.XML_DIVISION, ancakPanenQuality.getDivision());
            values.put(AncakPanenQuality.XML_GANG, ancakPanenQuality.getGang());
            values.put(AncakPanenQuality.XML_LOCATION, ancakPanenQuality.getLocation());
            values.put(AncakPanenQuality.XML_TPH, ancakPanenQuality.getTph());
            values.put(AncakPanenQuality.XML_NIK_HARVESTER, ancakPanenQuality.getNikHarvester());
            values.put(AncakPanenQuality.XML_CROP,ancakPanenQuality.getCrop());
            values.put(AncakPanenQuality.XML_ACHIEVEMENT_CODE, ancakPanenQuality.getAchievementCode());
            values.put(AncakPanenQuality.XML_QUALITY_CODE, ancakPanenQuality.getQualityCode());
            values.put(AncakPanenQuality.XML_QUANTITY, ancakPanenQuality.getQuantity());
            values.put(AncakPanenQuality.XML_STATUS, ancakPanenQuality.getStatus());
            values.put(AncakPanenQuality.XML_CREATED_DATE, ancakPanenQuality.getCreatedDate());
            values.put(AncakPanenQuality.XML_CREATED_BY, ancakPanenQuality.getCreatedBy());
            values.put(AncakPanenQuality.XML_MODIFIED_DATE, ancakPanenQuality.getModifiedDate());
            values.put(AncakPanenQuality.XML_MODIFIED_BY, ancakPanenQuality.getModifiedBy());
        }else if(object.getClass().getName().equals(SPBSDestination.class.getName())){
            SPBSDestination spbsDestination = (SPBSDestination) object;
            tableName = SPBSDestination.TABLE_NAME;

            values.put(SPBSDestination.XML_MANDT, spbsDestination.getMandt());
            values.put(SPBSDestination.XML_ESTATE, spbsDestination.getEstate());
            values.put(SPBSDestination.XML_DEST_TYPE, spbsDestination.getDestType());
            values.put(SPBSDestination.XML_DEST_ID, spbsDestination.getDestId());
            values.put(SPBSDestination.XML_DEST_DESC, spbsDestination.getDestDesc());
            values.put(SPBSDestination.XML_ACTIVE, spbsDestination.getActive());
        }else if (object.getClass().getName().equals(SPTARunningNumber.class.getName())) {
            SPTARunningNumber sptaRunningNumber = (SPTARunningNumber) object;
            tableName = SPTARunningNumber.TABLE_NAME;

            values.put(SPBSRunningNumber.XML_ID, sptaRunningNumber.getId());
            values.put(SPBSRunningNumber.XML_ESTATE, sptaRunningNumber.getEstate());
            values.put(SPBSRunningNumber.XML_DIVISION, sptaRunningNumber.getDivision());
            values.put(SPBSRunningNumber.XML_YEAR, sptaRunningNumber.getYear());
            values.put(SPBSRunningNumber.XML_MONTH, sptaRunningNumber.getMonth());
            values.put(SPBSRunningNumber.XML_IMEI, sptaRunningNumber.getImei());
            values.put(SPBSRunningNumber.XML_RUNNING_NUMBER, sptaRunningNumber.getRunningNumber());
            values.put(SPBSRunningNumber.XML_DEVICE_ALIAS, sptaRunningNumber.getDeviceAlias());
        }else if(object.getClass().getName().equals(Vendor.class.getName())){
            Vendor vendor = (Vendor) object;
            tableName = Vendor.TABLE_NAME;

            values.put(Vendor.XML_ESTATE, vendor.getEstate());
            values.put(Vendor.XML_LIFNR, vendor.getLifnr());
            values.put(Vendor.XML_NAME, vendor.getName());
        }else if(object.getClass().getName().equals(BLKSUGC.class.getName())){
            BLKSUGC blksugc = (BLKSUGC) object;
            tableName = BLKSUGC.TABLE_NAME;

            values.put(BLKSUGC.XML_COMPANY_CODE, blksugc.getCompanyCode());
            values.put(BLKSUGC.XML_ESTATE, blksugc.getEstate());
            values.put(BLKSUGC.XML_BLOCK, blksugc.getBlock());
            values.put(BLKSUGC.XML_VALID_FROM, blksugc.getValidFrom());
            values.put(BLKSUGC.XML_VALID_TO, blksugc.getValidTo());
            values.put(BLKSUGC.XML_PHASE, blksugc.getPhase());
            values.put(BLKSUGC.XML_DISTANCE, blksugc.getDistance());
            values.put(BLKSUGC.XML_SUB_DIVISION, blksugc.getSubDivision());
        }else if(object.getClass().getName().equals(GroupHead.class.getName())){
            GroupHead groupHead = (GroupHead) object;
            tableName = GroupHead.TABLE_NAME;

            values.put(GroupHead.XML_COMPANY_CODE, groupHead.getCompanyCode());
            values.put(GroupHead.XML_ESTATE, groupHead.getEstate());
            values.put(GroupHead.XML_LIFNR, groupHead.getLifnr());
            values.put(GroupHead.XML_INITIAL, groupHead.getInitial());
            values.put(GroupHead.XML_VALID_FROM, groupHead.getValidFrom());
            values.put(GroupHead.XML_VALID_TO, groupHead.getValidTo());
            values.put(GroupHead.XML_NAME, groupHead.getName());
            values.put(GroupHead.XML_STATUS, groupHead.getStatus());
        }else if(object.getClass().getName().equals(SPTA.class.getName())){
            SPTA spta = (SPTA) object;
            tableName = SPTA.TABLE_NAME;

            values.put(SPTA.XML_ZYEAR, spta.getZyear());
            values.put(SPTA.XML_IMEI, spta.getImei());
            values.put(SPTA.XML_SPTA_NUM, spta.getSptaNum());
            values.put(SPTA.XML_COMPANY_CODE, spta.getCompanyCode());
            values.put(SPTA.XML_ESTATE, spta.getEstate());
            values.put(SPTA.XML_DIVISI, spta.getDivisi());
            values.put(SPTA.XML_SPTA_DATE, spta.getSptaDate());
            values.put(SPTA.XML_SUB_DIV, spta.getSubDiv());
            values.put(SPTA.XML_PETAK_ID, spta.getPetakId());
            values.put(SPTA.XML_VENDOR_ID, spta.getVendorId());
            values.put(SPTA.XML_NOPOL, spta.getNopol());
            values.put(SPTA.XML_LOGO, spta.getLogo());
            values.put(SPTA.XML_JARAK, spta.getJarak());
            values.put(SPTA.XML_RUN_ACC_1, spta.getRunAcc1());
            values.put(SPTA.XML_EMPL_ID_1, spta.getEmplId1());
            values.put(SPTA.XML_RUN_ACC_2, spta.getRunAcc2());
            values.put(SPTA.XML_EMPL_ID_2, spta.getEmplId2());
            values.put(SPTA.XML_CHOPPED_DATE, spta.getChoppedDate());
            values.put(SPTA.XML_CHOPPED_HOUR, spta.getChoppedHour());
            values.put(SPTA.XML_BURN_DATE, spta.getBurnDate());
            values.put(SPTA.XML_BURN_HOUR, spta.getBurnHour());
            values.put(SPTA.XML_LOAD_DATE, spta.getLoadDate());
            values.put(SPTA.XML_LOAD_HOUR, spta.getLoadHour());
            values.put(SPTA.XML_QUALITY, spta.getQuality());
            values.put(SPTA.XML_INSENTIVE_GULMA, spta.getInsentiveGulma());
            values.put(SPTA.XML_INSENTIVE_LANGSIR, spta.getInsentiveLangsir());
            values.put(SPTA.XML_INSENTIVE_ROBOH, spta.getInsentiveRoboh());
            values.put(SPTA.XML_CANE_TYPE, spta.getCaneType());
            values.put(SPTA.XML_COST_TEBANG, spta.getCostTebang());
            values.put(SPTA.XML_COST_MUAT, spta.getCostMuat());
            values.put(SPTA.XML_COST_ANGKUT, spta.getCostAngkut());
            values.put(SPTA.XML_PENALTY_TRASH, spta.getPenaltyTrash());
            values.put(SPTA.XML_GPS_KOORDINAT, spta.getGpsKoordinat());
            values.put(SPTA.XML_IS_SAVE, spta.getIsSave());
            values.put(SPTA.XML_STATUS, spta.getStatus());
            values.put(SPTA.XML_CREATED_DATE, spta.getCreatedDate());
            values.put(SPTA.XML_CREATED_BY, spta.getCreatedBy());
            values.put(SPTA.XML_MODIFIED_DATE, spta.getModifiedDate());
            values.put(SPTA.XML_MODIFIED_BY, spta.getModifiedBy());
        }else if(object.getClass().getName().equals(RKH_ACTIVITY_TYPE.class.getName())){
            RKH_ACTIVITY_TYPE rkh_activity_type = (RKH_ACTIVITY_TYPE) object;
            tableName = RKH_ACTIVITY_TYPE.TABLE_NAME;
            values.put(RKH_ACTIVITY_TYPE.XML_ACTTYPE, rkh_activity_type.getACTTYPE());
            values.put(RKH_ACTIVITY_TYPE.XML_NAME, rkh_activity_type.getNAME());
        }
        else if(object.getClass().getName().equals(RKH_HEADER.class.getName())){
            RKH_HEADER rkh_header = (RKH_HEADER) object;
            tableName = RKH_HEADER.TABLE_NAME;
            values.put(RKH_HEADER.XML_IMEI, rkh_header.getIMEI());
            values.put(RKH_HEADER.XML_COMPANY_CODE, rkh_header.getCOMPANY_CODE());
            values.put(RKH_HEADER.XML_ESTATE, rkh_header.getESTATE());
            values.put(RKH_HEADER.XML_RKH_DATE, rkh_header.getRKH_DATE());
            values.put(RKH_HEADER.XML_DIVISION, rkh_header.getDIVISION());
            values.put(RKH_HEADER.XML_GANG, rkh_header.getGANG());
            values.put(RKH_HEADER.XML_NIK_FOREMAN, rkh_header.getNIK_FOREMAN());
            values.put(RKH_HEADER.XML_FOREMAN, rkh_header.getFOREMAN());
            values.put(RKH_HEADER.XML_NIK_CLERK, rkh_header.getNIK_CLERK());
            values.put(RKH_HEADER.XML_CLERK, rkh_header.getCLERK());
            values.put(RKH_HEADER.XML_GPS_KOORDINAT, rkh_header.getGPS_KOORDINAT());
            values.put(RKH_HEADER.XML_STATUS, rkh_header.getSTATUS());
            values.put(RKH_HEADER.XML_CREATED_DATE, rkh_header.getCREATED_DATE());
            values.put(RKH_HEADER.XML_CREATED_BY, rkh_header.getCREATED_BY());
            values.put(RKH_HEADER.XML_MODIFIED_DATE, rkh_header.getMODIFIED_DATE());
            values.put(RKH_HEADER.XML_MODIFIED_BY, rkh_header.getMODIFIED_BY());
            values.put(RKH_HEADER.XML_RKH_ID, rkh_header.getRKH_ID());
        }
        else if(object.getClass().getName().equals(RKH_ITEM.class.getName())){
            RKH_ITEM rkh_item = (RKH_ITEM) object;
            tableName = RKH_ITEM.TABLE_NAME;
            values.put(RKH_ITEM.XML_RKH_ID, rkh_item.getRKH_ID());
            values.put(RKH_ITEM.XML_LINE, rkh_item.getLINE());
            values.put(RKH_ITEM.XML_ACTIVITY, rkh_item.getACTIVITY());
            values.put(RKH_ITEM.XML_BLOCK, rkh_item.getBLOCK());
            values.put(RKH_ITEM.XML_TARGET_OUTPUT, rkh_item.getTARGET_OUTPUT());
            values.put(RKH_ITEM.XML_TARGET_HASIL, rkh_item.getTARGET_HASIL());
            values.put(RKH_ITEM.XML_TARGET_JANJANG, rkh_item.getTARGET_JANJANG());
            values.put(RKH_ITEM.XML_SKU, rkh_item.getSKU());
            values.put(RKH_ITEM.XML_PHL, rkh_item.getPHL());
            values.put(RKH_ITEM.XML_BJR, rkh_item.getBJR());
            values.put(RKH_ITEM.XML_GPS_KOORDINAT, rkh_item.getGPS_KOORDINAT());
            values.put(RKH_ITEM.XML_STATUS, rkh_item.getSTATUS());
            values.put(RKH_ITEM.XML_MODIFIED_DATE, rkh_item.getMODIFIED_DATE());
            values.put(RKH_ITEM.XML_MODIFIED_BY, rkh_item.getMODIFIED_BY());
            values.put(RKH_ITEM.XML_CREATED_DATE, rkh_item.getCREATED_DATE());
            values.put(RKH_ITEM.XML_CREATED_BY, rkh_item.getCREATED_BY());
        }

        long rowId = 0;

        try{
            rowId = sqliteDatabase.insertOrThrow(tableName, null, values);
        }catch(Exception e){
            e.printStackTrace();
        }

        return rowId;
    }

    public List<Object> getListData(boolean distinct, String tableName,
                                    String[] columns, String whereClause, String[] whereArgs,
                                    String groupBy, String having, String orderBy, String limit) {

        Cursor cursor = null;
        List<Object> listObject = new ArrayList<Object>();

        try {
            cursor = sqliteDatabase.query(distinct, tableName, columns,
                    whereClause, whereArgs, groupBy, having, orderBy, limit);
            if (tableName.equals(tblT_BlockConditionScore.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        tblT_BlockConditionScore md = new tblT_BlockConditionScore();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_CompanyCode)) {
                                md.setAcc_CompanyCode(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Estate)) {
                                md.setAcc_Estate(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_ZYear)) {
                                md.setAcc_ZYear(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Period)) {
                                md.setAcc_Period(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TransLevel)) {
                                md.setAcc_TransLevel(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Block)) {
                                md.setAcc_Block(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Jalan)) {
                                md.setAcc_Jalan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Jembatan)) {
                                md.setAcc_Jembatan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Parit)) {
                                md.setAcc_Parit(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TitiPanen)) {
                                md.setAcc_TitiPanen(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TitiRintis)) {
                                md.setAcc_TitiRintis(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Tikus)) {
                                md.setAcc_Tikus(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Pencurian)) {
                                md.setAcc_Pencurian(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_BW)) {
                                md.setAcc_BW(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TPHBersih)) {
                                md.setAcc_TPHBersih(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TPH)) {
                                md.setAcc_TPH(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TPHBersih2)) {
                                md.setAcc_TPHBersih2(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TPH2)) {
                                md.setAcc_TPH2(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Piringan)) {
                                md.setAcc_Piringan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_PasarPanen)) {
                                md.setAcc_PasarPanen(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_PasarRintis)) {
                                md.setAcc_PasarRintis(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TunasPokok)) {
                                md.setAcc_TunasPokok(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Gawangan)) {
                                md.setAcc_Gawangan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Drainase)) {
                                md.setAcc_Drainase(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Ganoderma)) {
                                md.setAcc_Ganoderma(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Rayap)) {
                                md.setAcc_Rayap(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Orcytes)) {
                                md.setAcc_Orcytes(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Sanitasi)) {
                                md.setAcc_Sanitasi(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Kacangan)) {
                                md.setAcc_Kacangan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_CreatedDateTime)) {
                                md.setAcc_CreatedDateTime(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_CreatedBy)) {
                                md.setAcc_CreatedBy(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_ModifiedDateTime)) {
                                md.setAcc_ModifiedDateTime(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_ModifiedBy)) {
                                md.setAcc_ModifiedBy(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Color)) {
                                md.setAcc_Color(cursor.getString(i));
                            }


                        }

                        listObject.add(md);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(tblT_BlockCondition.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        tblT_BlockCondition md = new tblT_BlockCondition();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_CompanyCode)) {
                                md.setAcc_CompanyCode(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Estate)) {
                                md.setAcc_Estate(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_ZYear)) {
                                md.setAcc_ZYear(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Period)) {
                                md.setAcc_Period(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_ZDate)) {
                                md.setAcc_ZDate(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TransLevel)) {
                                md.setAcc_TransLevel(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Block)) {
                                md.setAcc_Block(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_SKB)) {
                                md.setAcc_SKB(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_BlockRow)) {
                                md.setAcc_BlockRow(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_CensusPoint)) {
                                md.setAcc_CensusPoint(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_PokokLabel)) {
                                md.setAcc_PokokLabel(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_PokokSide)) {
                                md.setAcc_PokokSide(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_PokokCondition)) {
                                md.setAcc_PokokCondition(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Jalan)) {
                                md.setAcc_Jalan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Jembatan)) {
                                md.setAcc_Jembatan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Parit)) {
                                md.setAcc_Parit(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TitiPanen)) {
                                md.setAcc_TitiPanen(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TitiRintis)) {
                                md.setAcc_TitiRintis(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Tikus)) {
                                md.setAcc_Tikus(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Pencurian)) {
                                md.setAcc_Pencurian(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_BW)) {
                                md.setAcc_BW(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TPHBersih)) {
                                md.setAcc_TPHBersih(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TPH)) {
                                md.setAcc_TPH(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TPHBersih2)) {
                                md.setAcc_TPHBersih2(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TPH2)) {
                                md.setAcc_TPH2(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Piringan)) {
                                md.setAcc_Piringan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_PasarPanen)) {
                                md.setAcc_PasarPanen(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_PasarRintis)) {
                                md.setAcc_PasarRintis(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TunasPokok)) {
                                md.setAcc_TunasPokok(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Gawangan)) {
                                md.setAcc_Gawangan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Drainase)) {
                                md.setAcc_Drainase(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Ganoderma)) {
                                md.setAcc_Ganoderma(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Rayap)) {
                                md.setAcc_Rayap(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Orcytes)) {
                                md.setAcc_Orcytes(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Sanitasi)) {
                                md.setAcc_Sanitasi(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Kacangan)) {
                                md.setAcc_Kacangan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_CreatedDateTime)) {
                                md.setAcc_CreatedDateTime(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_CreatedBy)) {
                                md.setAcc_CreatedBy(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_ModifiedDateTime)) {
                                md.setAcc_ModifiedDateTime(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_ModifiedBy)) {
                                md.setAcc_ModifiedBy(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Flag)) {
                                md.setAcc_Flag(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_DIVISI)) {
                                md.setAcc_DIVISI(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Remark)) {
                                md.setAcc_Remark(cursor.getString(i));
                            }


                        }

                        listObject.add(md);
                    } while (cursor.moveToNext());
                }
            }else if(tableName.equals(tblM_BlockConditionScore.TABLE_NAME)){
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        tblM_BlockConditionScore md = new tblM_BlockConditionScore();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_Color)){
                                md.setAcc_Color(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_Interval1)){
                                md.setAcc_Interval1(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_Interval2)){
                                md.setAcc_Interval2(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_Score1)){
                                md.setAcc_Score1(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_Score2)){
                                md.setAcc_Score2(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_Score3)){
                                md.setAcc_Score3(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_ScoreType)){
                                md.setAcc_ScoreType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_ValidFrom)){
                                md.setAcc_ValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_ValidTo)){
                                md.setAcc_ValidTo(cursor.getString(i));
                            }
                        }

                        listObject.add(md);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(tblM_AccountingPeriod.TABLE_NAME)){
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        tblM_AccountingPeriod md = new tblM_AccountingPeriod();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_Estate)){
                                md.setAcc_Estate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_ZYear)){
                                md.setAcc_ZYear(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_Period)){
                                md.setAcc_Period(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_ClosingDate)){
                                md.setAcc_ClosingDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_Status)){
                                md.setAcc_Status(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_Active)){
                                md.setAcc_Active(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_CreatedBy)){
                                md.setAcc_CreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_CreatedDate)){
                                md.setAcc_CreatedDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_ModifiedBy)){
                                md.setAcc_ModifiedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_ModifiedDate)){
                                md.setAcc_ModifiedDate(cursor.getString(i));
                            }
                        }

                        listObject.add(md);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(MasterDownload.TABLE_NAME)){
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        MasterDownload md = new MasterDownload();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(MasterDownload.XML_NAME)){
                                md.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(MasterDownload.XML_FILENAME)){
                                md.setFileName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(MasterDownload.XML_SYNC_DATE)){
                                md.setSyncDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(MasterDownload.XML_STATUS)){
                                md.setStatus(cursor.getInt(i));
                            }
                        }

                        listObject.add(md);
                    }while(cursor.moveToNext());
                }
            }else if (tableName.equals(SKB.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        SKB skb = new SKB();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_COMPANY_CODE)){
                                skb.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_ESTATE)){
                                skb.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_BLOCK)){
                                skb.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_BARIS_SKB)){
                                skb.setBarisSkb(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_VALID_FROM)){
                                skb.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_VALID_TO)){
                                skb.setValidTo(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_BARIS_BLOCK)){
                                skb.setBarisBlok(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_JUMLAH_POKOK)){
                                skb.setJumlahPokok(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_POKOK_MATI)){
                                skb.setPokokMati(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_TANGGAL_TANAM)){
                                skb.setTanggalTanam(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_LINE_SKB)){
                                skb.setLineSkb(cursor.getInt(i));
                            }
                        }

                        listObject.add(skb);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BlockHdrc.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        BlockHdrc blockHdrc = new BlockHdrc();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_COMPANY_CODE)){
                                blockHdrc.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_ESTATE)){
                                blockHdrc.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_BLOCK)){
                                blockHdrc.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_VALID_FROM)){
                                blockHdrc.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_VALID_TO)){
                                blockHdrc.setValidTo(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_DIVISION)){
                                blockHdrc.setDivisi(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_TYPE)){
                                blockHdrc.setType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_STATUS)){
                                blockHdrc.setStatus(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_PROJECT_DEFINITION)){
                                blockHdrc.setProjectDefinition(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_OWNER)){
                                blockHdrc.setOwner(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_TGL_TANAM)){
                                blockHdrc.setTglTanam(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_MANDT)){
                                blockHdrc.setMandt(cursor.getString(i));
                            }
                        }

                        listObject.add(blockHdrc);
                    }while(cursor.moveToNext());				}
            } else if (tableName.equals(BLKSBC.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BLKSBC blksbc = new BLKSBC();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_COMPANY_CODE)){
                                blksbc.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_ESTATE)){
                                blksbc.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_BLOCK)){
                                blksbc.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_VALID_FROM)){
                                blksbc.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_BASIS_NORMAL)){
                                blksbc.setBasisNormal(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_BASIS_FRIDAY)){
                                blksbc.setBasisFriday(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_BASIS_HOLIDAY)){
                                blksbc.setBasisHoliday(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_PREMI_NORMAL)){
                                blksbc.setPremiNormal(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_PREMI_FRIDAY)){
                                blksbc.setPremiFriday(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_PREMI_HOLIDAY)){
                                blksbc.setPremiHoliday(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_PRIORITY)){
                                blksbc.setPriority(cursor.getString(i));
                            }
                        }

                        listObject.add(blksbc);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BLKSBCDetail.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BLKSBCDetail blksbcDetail = new BLKSBCDetail();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBCDetail.XML_COMPANY_CODE)){
                                blksbcDetail.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBCDetail.XML_ESTATE)){
                                blksbcDetail.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBCDetail.XML_BLOCK)){
                                blksbcDetail.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBCDetail.XML_VALID_FROM)){
                                blksbcDetail.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBCDetail.XML_MIN_VAL)){
                                blksbcDetail.setMinimumValue(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBCDetail.XML_MAX_VAL)){
                                blksbcDetail.setMaximumValue(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBCDetail.XML_OVER_BASIC_RATE)){
                                blksbcDetail.setOverBasicRate(cursor.getDouble(i));
                            }
                        }

                        listObject.add(blksbcDetail);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BJR.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BJR bjr = new BJR();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BJR.XML_COMPANY_CODE)){
                                bjr.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BJR.XML_ESTATE)){
                                bjr.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BJR.XMl_EFF_DATE)){
                                bjr.setEffDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BJR.XML_BLOCK)){
                                bjr.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BJR.XML_BJR)){
                                bjr.setBjr(cursor.getDouble(i));
                            }
                        }

                        listObject.add(bjr);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BLKPLT.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BLKPLT blkplt = new BLKPLT();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_COMPANY_CODE)){
                                blkplt.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_ESTATE)){
                                blkplt.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_BLOCK)){
                                blkplt.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_VALID_FROM)){
                                blkplt.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_VALID_TO)){
                                blkplt.setValidTo(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_CROP_TYPE)){
                                blkplt.setCropType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_PREVIOUS_CROP)){
                                blkplt.setPreviousCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_FINISH_DATE)){
                                blkplt.setFinishDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_REFERENCE)){
                                blkplt.setReference(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_JARAK_TANAM)){
                                blkplt.setJarakTanam(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_HARVESTING_DATE)){
                                blkplt.setHarvestingDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_HARVESTED)){
                                blkplt.setHarvested(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_PLAN_DATE)){
                                blkplt.setPlanDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_TOPOGRAPHY)){
                                blkplt.setTopography(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_SOIL_TYPE)){
                                blkplt.setSoilType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_SOIL_CATEGORY)){
                                blkplt.setSoilCategory(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_PROD_TREES)){
                                blkplt.setProdTrees(cursor.getDouble(i));
                            }
                        }

                        listObject.add(blkplt);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(Employee.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        Employee employee = new Employee();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_COMPANY_CODE) ){
                                employee.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_ESTATE)){
                                employee.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_FISCAL_YEAR)){
                                employee.setFiscalYear(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_FISCAL_PERIOD)){
                                employee.setFiscalPeriod(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_NIK)){
                                employee.setNik(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_NAME)){
                                employee.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_TERM_DATE)){
                                employee.setTermDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_DIVISION)){
                                employee.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_ROLE_ID)){
                                employee.setRoleId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_JOB_POS)){
                                employee.setJobPos(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_GANG)){
                                employee.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_COST_CENTER)){
                                employee.setCostCenter(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_EMP_TYPE)){
                                employee.setEmpType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_VALID_FROM)){
                                employee.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_HARVESTER_CODE)){
                                employee.setHarvesterCode(cursor.getString(i));
                            }
                        }

                        listObject.add(employee);
                    }while (cursor.moveToNext());
                }
            } else if (tableName.equals(UserApp.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        UserApp userApp = new UserApp();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(UserApp.XML_NIK)){
                                userApp.setNik(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserApp.XML_USERNAME)){
                                userApp.setUsername(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserApp.XML_PASSWORD)){
                                userApp.setPassword(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserApp.XML_VALID_TO)){
                                userApp.setValidTo(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserApp.XML_CREATED_DATE)){
                                userApp.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserApp.XML_CREATED_BY)){
                                userApp.setCreatedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(userApp);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(AbsentType.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        AbsentType absentType = new AbsentType();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_COMPANY_CODE)){
                                absentType.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_ABSENT_TYPE)){
                                absentType.setAbsentType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_DESCRIPTION)){
                                absentType.setDescription(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_HKRLLO)){
                                absentType.setHkrllo(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_HKRLHI)){
                                absentType.setHkrlhi(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_HKPYLO)){
                                absentType.setHkpylo(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_HKPYHI)){
                                absentType.setHkpyhi(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_HKVLLO)){
                                absentType.setHkvllo(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_HKVLHI)){
                                absentType.setHkvlhi(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_HKMEIN)){
                                absentType.setHkmein(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_AGROUP)){
                                absentType.setAgroup(cursor.getString(i));
                            }
                        }

                        listObject.add(absentType);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(DayOff.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        DayOff dayOff = new DayOff();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(DayOff.XML_ESTATE)){
                                dayOff.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DayOff.XML_DATE)){
                                dayOff.setDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DayOff.XML_DAY_OFF_TYPE)){
                                dayOff.setDayOffType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DayOff.XML_DESCRIPTION)){
                                dayOff.setDescription(cursor.getString(i));
                            }
                        }

                        listObject.add(dayOff);
                    } while (cursor.moveToNext());
                }
            }else if (tableName.equals(DivisionAssistant.TABLE_NAME)){
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        DivisionAssistant assistantDivision = new DivisionAssistant();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_ESTATE)){
                                assistantDivision.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_DIVISION)){
                                assistantDivision.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_SPRAS)){
                                assistantDivision.setSpras(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_DESCRIPTION)){
                                assistantDivision.setDescription(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_ASSISTANT)){
                                assistantDivision.setAssistant(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_DISTANCE_TO_MILL)){
                                assistantDivision.setDistanceToMill(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_UOM)){
                                assistantDivision.setUom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_ASSISTANT_NAME)){
                                assistantDivision.setAssistantName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_LIFNR)) {
                                assistantDivision.setLifnr(cursor.getString(i));
                            }
                        }

                        listObject.add(assistantDivision);
                    }while(cursor.moveToNext());
                }
            } else if (tableName.equals(ForemanActive.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        ForemanActive foremanActive = new ForemanActive();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_COMPANY_CODE) ){
                                foremanActive.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_ESTATE)){
                                foremanActive.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_FISCAL_YEAR)){
                                foremanActive.setFiscalYear(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_FISCAL_PERIOD)){
                                foremanActive.setFiscalPeriod(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_NIK)){
                                foremanActive.setNik(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_NAME)){
                                foremanActive.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_TERM_DATE)){
                                foremanActive.setTermDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_DIVISION)){
                                foremanActive.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_ROLE_ID)){
                                foremanActive.setRoleId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_JOB_POS)){
                                foremanActive.setJobPos(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_GANG)){
                                foremanActive.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_COST_CENTER)){
                                foremanActive.setCostCenter(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_EMP_TYPE)){
                                foremanActive.setEmpType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_VALID_FROM)){
                                foremanActive.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_HARVESTER_CODE)){
                                foremanActive.setHarvesterCode(cursor.getString(i));
                            }
                        }

                        listObject.add(foremanActive);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(UserLogin.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        UserLogin userLogin = new UserLogin();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_COMPANY_CODE) ){
                                userLogin.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_ESTATE)){
                                userLogin.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_FISCAL_YEAR)){
                                userLogin.setFiscalYear(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_FISCAL_PERIOD)){
                                userLogin.setFiscalPeriod(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_NIK)){
                                userLogin.setNik(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_NAME)){
                                userLogin.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_TERM_DATE)){
                                userLogin.setTermDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_DIVISION)){
                                userLogin.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_ROLE_ID)){
                                userLogin.setRoleId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_JOB_POS)){
                                userLogin.setJobPos(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_GANG)){
                                userLogin.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_COST_CENTER)){
                                userLogin.setCostCenter(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_EMP_TYPE)){
                                userLogin.setEmpType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_VALID_FROM)){
                                userLogin.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_HARVESTER_CODE)){
                                userLogin.setHarvesterCode(cursor.getString(i));
                            }
                        }

                        listObject.add(userLogin);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BPNHeader.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        BPNHeader bpnHeader = new BPNHeader();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_BPN_ID)){
                                bpnHeader.setBpnId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_IMEI)){
                                bpnHeader.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_COMPANY_CODE)){
                                bpnHeader.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_ESTATE)){
                                bpnHeader.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_BPN_DATE)){
                                bpnHeader.setBpnDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_DIVISION)){
                                bpnHeader.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_GANG)){
                                bpnHeader.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_LOCATION)){
                                bpnHeader.setLocation(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_TPH)){
                                bpnHeader.setTph(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_NIK_HARVESTER)){
                                bpnHeader.setNikHarvester(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_HARVESTER)){
                                bpnHeader.setHarvester(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_NIK_FOREMAN)){
                                bpnHeader.setNikForeman(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_FOREMAN)){
                                bpnHeader.setForeman(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_NIK_CLERK)){
                                bpnHeader.setNikClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_CLERK)){
                                bpnHeader.setClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_USE_GERDANG)){
                                bpnHeader.setUseGerdang(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_CROP)){
                                bpnHeader.setCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_GPS_KOORDINAT)){
                                bpnHeader.setGpsKoordinat(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_PHOTO)){
                                bpnHeader.setPhoto(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_STATUS)){
                                bpnHeader.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_SPBS_NUMBER)){
                                bpnHeader.setSpbsNumber(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_CREATED_DATE)){
                                bpnHeader.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_CREATED_BY)){
                                bpnHeader.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_MODIFIED_DATE)){
                                bpnHeader.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_MODIFIED_BY)){
                                bpnHeader.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(bpnHeader);
                    }while (cursor.moveToNext());
                }
            } else if (tableName.equals(BPNQuantity.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BPNQuantity bpnQuantity = new BPNQuantity();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_BPN_ID)){
                                bpnQuantity.setBpnId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_IMEI)){
                                bpnQuantity.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_COMPANY_CODE)){
                                bpnQuantity.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_ESTATE)){
                                bpnQuantity.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_BPN_DATE)){
                                bpnQuantity.setBpnDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_DIVISION)){
                                bpnQuantity.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_GANG)){
                                bpnQuantity.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_LOCATION)){
                                bpnQuantity.setLocation(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_TPH)){
                                bpnQuantity.setTph(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_NIK_HARVESTER)){
                                bpnQuantity.setNikHarvester(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_CROP)){
                                bpnQuantity.setCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_ACHIEVEMENT_CODE)){
                                bpnQuantity.setAchievementCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_QUANTITY)){
                                bpnQuantity.setQuantity(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_QUANTITY_REMAINING)){
                                bpnQuantity.setQuantityRemaining(cursor.getDouble(i));
                            }
                            else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_STATUS)){
                                bpnQuantity.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_CREATED_DATE)){
                                bpnQuantity.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_CREATED_BY)){
                                bpnQuantity.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_MODIFIED_DATE)){
                                bpnQuantity.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_MODIFIED_BY)){
                                bpnQuantity.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(bpnQuantity);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BPNQuality.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BPNQuality bpnQuality = new BPNQuality();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_BPN_ID)){
                                bpnQuality.setBpnId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_IMEI)){
                                bpnQuality.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_COMPANY_CODE)){
                                bpnQuality.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_ESTATE)){
                                bpnQuality.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_BPN_DATE)){
                                bpnQuality.setBpnDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_DIVISION)){
                                bpnQuality.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_GANG)){
                                bpnQuality.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_LOCATION)){
                                bpnQuality.setLocation(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_TPH)){
                                bpnQuality.setTph(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_NIK_HARVESTER)){
                                bpnQuality.setNikHarvester(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_CROP)){
                                bpnQuality.setCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_ACHIEVEMENT_CODE)){
                                bpnQuality.setAchievementCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_QUALITY_CODE)){
                                bpnQuality.setQualityCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_QUANTITY)){
                                bpnQuality.setQuantity(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_STATUS)){
                                bpnQuality.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_CREATED_DATE)){
                                bpnQuality.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_CREATED_BY)){
                                bpnQuality.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_MODIFIED_DATE)){
                                bpnQuality.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_MODIFIED_BY)){
                                bpnQuality.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(bpnQuality);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BKMHeader.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BKMHeader bkmHeader = new BKMHeader();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_IMEI)){
                                bkmHeader.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_COMPANY_CODE)){
                                bkmHeader.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_ESTATE)){
                                bkmHeader.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_BKM_DATE)){
                                bkmHeader.setBkmDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_DIVISION)){
                                bkmHeader.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_GANG)){
                                bkmHeader.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_NIK_FOREMAN)){
                                bkmHeader.setNikForeman(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_FOREMAN)){
                                bkmHeader.setForeman(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_NIK_CLERK)){
                                bkmHeader.setNikClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_CLERK)){
                                bkmHeader.setClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_GPS_KOORDINAT)){
                                bkmHeader.setGpsKoordinat(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_STATUS)){
                                bkmHeader.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_CREATED_DATE)){
                                bkmHeader.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_CREATED_BY)){
                                bkmHeader.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_MODIFIED_DATE)){
                                bkmHeader.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_MODIFIED_BY)){
                                bkmHeader.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(bkmHeader);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BKMLine.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BKMLine bkmLine = new BKMLine();

                        for(int i = 0; i <  cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_IMEI)){
                                bkmLine.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_COMPANY_CODE)){
                                bkmLine.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_ESTATE)){
                                bkmLine.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_BKM_DATE)){
                                bkmLine.setBkmDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_DIVISION)){
                                bkmLine.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_GANG)){
                                bkmLine.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_NIK)){
                                bkmLine.setNik(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_NAME)){
                                bkmLine.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_ABSENT_TYPE)){
                                bkmLine.setAbsentType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_MANDAYS)){
                                bkmLine.setMandays(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_UOM)){
                                bkmLine.setUom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_USE_GERDANG)){
                                bkmLine.setUseGerdang(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_STATUS)){
                                bkmLine.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_CREATED_DATE)){
                                bkmLine.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_CREATED_BY)){
                                bkmLine.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_MODIFIED_DATE)){
                                bkmLine.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_MODIFIED_BY)){
                                bkmLine.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(bkmLine);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BKMOutput.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        BKMOutput bkmOutput = new BKMOutput();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_IMEI)){
                                bkmOutput.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_COMPANY_CODE)){
                                bkmOutput.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_ESTATE)){
                                bkmOutput.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_BKM_DATE)){
                                bkmOutput.setBkmDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_DIVISION)){
                                bkmOutput.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_GANG)){
                                bkmOutput.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_BLOCK)){
                                bkmOutput.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_NIK)){
                                bkmOutput.setNik(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_NAME)){
                                bkmOutput.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_OUTPUT)){
                                bkmOutput.setOutput(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_UOM)){
                                bkmOutput.setUom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_STATUS)){
                                bkmOutput.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_CREATED_DATE)){
                                bkmOutput.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_CREATED_BY)){
                                bkmOutput.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_MODIFIED_DATE)){
                                bkmOutput.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_MODIFIED_BY)){
                                bkmOutput.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(bkmOutput);
                    }while(cursor.moveToNext());
                }
            } else if (tableName.equals(SPBSHeader.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        SPBSHeader spbsHeader = new SPBSHeader();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_IMEI)){
                                spbsHeader.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_YEAR)){
                                spbsHeader.setYear(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_COMPANY_CODE)){
                                spbsHeader.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_ESTATE)){
                                spbsHeader.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_CROP)){
                                spbsHeader.setCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_SPBS_NUMBER)){
                                spbsHeader.setSpbsNumber(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_SPBS_DATE)){
                                spbsHeader.setSpbsDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_DEST_ID)){
                                spbsHeader.setDestId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_DEST_DESC)){
                                spbsHeader.setDestDesc(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_DEST_TYPE)){
                                spbsHeader.setDestType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_DIVISION)){
                                spbsHeader.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_NIK_ASSISTANT)){
                                spbsHeader.setNikAssistant(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_ASSISTANT)){
                                spbsHeader.setAssistant(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_NIK_CLERK)){
                                spbsHeader.setNikClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_CLERK)){
                                spbsHeader.setClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_NIK_DRIVER)){
                                spbsHeader.setNikDriver(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_DRIVER)){
                                spbsHeader.setDriver(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_NIK_KERNET)){
                                spbsHeader.setNikKernet(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_KERNET)){
                                spbsHeader.setKernet(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_LICENSE_PLATE)){
                                spbsHeader.setLicensePlate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_RUNNING_ACCOUNT)){
                                spbsHeader.setRunningAccount(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_GPS_KOORDINAT)){
                                spbsHeader.setGpsKoordinat(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_IS_SAVE)){
                                spbsHeader.setIsSave(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_STATUS)){
                                spbsHeader.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_CREATED_DATE)){
                                spbsHeader.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_CREATED_BY)){
                                spbsHeader.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_MODIFIED_DATE)){
                                spbsHeader.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_MODIFIED_BY)){
                                spbsHeader.setModifiedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_LIFNR)){
                                spbsHeader.setLifnr(cursor.getString(i));
                            }
                        }

                        listObject.add(spbsHeader);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(SPBSLine.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        SPBSLine spbsLine = new SPBSLine();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_ID)){
                                spbsLine.setId(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_IMEI)){
                                spbsLine.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_YEAR)){
                                spbsLine.setYear(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_COMPANY_CODE)){
                                spbsLine.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_ESTATE)){
                                spbsLine.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_CROP)){
                                spbsLine.setCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_SPBS_NUMBER)){
                                spbsLine.setSpbsNumber(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_SPBS_DATE)){
                                spbsLine.setSpbsDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_BLOCK)){
                                spbsLine.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_TPH)){
                                spbsLine.setTph(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_BPN_DATE)){
                                spbsLine.setBpnDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_ACHIEVEMENT_CODE)){
                                spbsLine.setAchievementCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_QUANTITY)){
                                spbsLine.setQuantity(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_QUANTITY_ANGKUT)){
                                spbsLine.setQuantityAngkut(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_QUANTITY_REMAINING)){
                                spbsLine.setQuantityRemaining(cursor.getDouble(i));
                            } else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_TOTAL_HARVESTER)){
                                spbsLine.setTotalHarvester(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_UOM)){
                                spbsLine.setUom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_GPS_KOORDINAT)){
                                spbsLine.setGpsKoordinat(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_IS_SAVE)){
                                spbsLine.setIsSave(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_STATUS)){
                                spbsLine.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_BPN_ID)){
                                spbsLine.setBpnId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_SPBS_REF)){
                                spbsLine.setSpbsRef(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_SPBS_NEXT)){
                                spbsLine.setSpbsNext(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_CREATED_DATE)){
                                spbsLine.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_CREATED_BY)){
                                spbsLine.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_MODIFIED_DATE)){
                                spbsLine.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_MODIFIED_BY)){
                                spbsLine.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(spbsLine);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(SPBSRunningNumber.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        SPBSRunningNumber spbsNumber = new SPBSRunningNumber();

                        for(int i = 0; i <cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_ID)){
                                spbsNumber.setId(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_ESTATE)){
                                spbsNumber.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_DIVISION)){
                                spbsNumber.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_YEAR)){
                                spbsNumber.setYear(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_MONTH)){
                                spbsNumber.setMonth(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_IMEI)){
                                spbsNumber.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_RUNNING_NUMBER)){
                                spbsNumber.setRunningNumber(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_DEVICE_ALIAS)){
                                spbsNumber.setDeviceAlias(cursor.getString(i));
                            }
                        }

                        listObject.add(spbsNumber);
                    }while(cursor.moveToNext());
                }
            } else if(tableName.equals(Bluetooth.TABLE_NAME)){
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        Bluetooth bluetooth = new Bluetooth();

                        for(int i = 0; i <cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(Bluetooth.XML_NAME)){
                                bluetooth.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Bluetooth.XML_ADDRESS)){
                                bluetooth.setAddress(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Bluetooth.XML_IS_PAIRED)){
                                if(cursor.getInt(i) == 1){
                                    bluetooth.setPaired(true);
                                }else{
                                    bluetooth.setPaired(false);
                                }
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Bluetooth.XML_IS_SELECTED)){
                                if(cursor.getInt(i) == 1){
                                    bluetooth.setSelected(true);
                                }else{
                                    bluetooth.setSelected(false);
                                }
                            }
                        }

                        listObject.add(bluetooth);
                    }while(cursor.moveToNext());
                }
            } else if(tableName.equals(RunningAccount.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        RunningAccount vehicle = new RunningAccount();

                        for(int i = 0; i <cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(RunningAccount.XML_COMPANY_CODE)){
                                vehicle.setCompanyCode(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(RunningAccount.XML_ESTATE)){
                                vehicle.setEstate(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(RunningAccount.XML_RUNNING_ACCOUNT)){
                                vehicle.setRunningAccount(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(RunningAccount.XML_LICENSE_PLATE)){
                                vehicle.setLicensePlate(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(RunningAccount.XML_LIFNR)){
                                vehicle.setLifnr(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(RunningAccount.XML_OWNERSHIPFLAG)){
                                vehicle.setOwnerShipFlag(cursor.getString(i));
                            }
                        }

                        listObject.add(vehicle);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(TaksasiHeader.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        TaksasiHeader taksasiHeader = new TaksasiHeader();

                        for(int i = 0; i <cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_IMEI)){
                                taksasiHeader.setImei(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_COMPANY_CODE)){
                                taksasiHeader.setCompanyCode(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_ESTATE)){
                                taksasiHeader.setEstate(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_DIVISION)){
                                taksasiHeader.setDivision(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_TAKSASI_DATE)){
                                taksasiHeader.setTaksasiDate(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_BLOCK)){
                                taksasiHeader.setBlock(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_CROP)){
                                taksasiHeader.setCrop(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_NIK_FOREMAN)){
                                taksasiHeader.setNikForeman(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_FOREMAN)){
                                taksasiHeader.setForeman(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_PROD_TREES)){
                                taksasiHeader.setProdTrees(cursor.getDouble(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_BJR)){
                                taksasiHeader.setBjr(cursor.getDouble(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_GPS_KOORDINAT)){
                                taksasiHeader.setGpsKoordinat(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_IS_SAVE)){
                                taksasiHeader.setIsSave(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_STATUS)){
                                taksasiHeader.setStatus(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_CREATED_DATE)){
                                taksasiHeader.setCreatedDate(cursor.getLong(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_CREATED_BY)){
                                taksasiHeader.setCreatedBy(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_MODIFIED_DATE)){
                                taksasiHeader.setModifiedDate(cursor.getLong(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_MODIFIED_BY)){
                                taksasiHeader.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(taksasiHeader);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(TaksasiLine.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        TaksasiLine taksasiLine = new TaksasiLine();

                        for(int i = 0; i <cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_IMEI)){
                                taksasiLine.setImei(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_COMPANY_CODE)){
                                taksasiLine.setCompanyCode(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_ESTATE)){
                                taksasiLine.setEstate(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_DIVISION)){
                                taksasiLine.setDivision(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_TAKSASI_DATE)){
                                taksasiLine.setTaksasiDate(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_BLOCK)){
                                taksasiLine.setBlock(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_CROP)){
                                taksasiLine.setCrop(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_BARIS_SKB)){
                                taksasiLine.setBarisSkb(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_BARIS_BLOCK)){
                                taksasiLine.setBarisBlock(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_LINE_SKB)){
                                taksasiLine.setLineSkb(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_QTY_POKOK)){
                                taksasiLine.setQtyPokok(cursor.getDouble(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_QTY_JANJANG)){
                                taksasiLine.setQtyJanjang(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_GPS_KOORDINAT)){
                                taksasiLine.setGpsKoordinat(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_IS_SAVE)){
                                taksasiLine.setIsSave(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_STATUS)){
                                taksasiLine.setStatus(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_CREATED_DATE)){
                                taksasiLine.setCreatedDate(cursor.getLong(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_CREATED_BY)){
                                taksasiLine.setCreatedBy(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_MODIFIED_DATE)){
                                taksasiLine.setModifiedDate(cursor.getLong(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_MODIFIED_BY)){
                                taksasiLine.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(taksasiLine);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(Penalty.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        Penalty penalty = new Penalty();

                        for(int i = 0; i <cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(Penalty.XML_PENALTY_CODE)){
                                penalty.setPenaltyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Penalty.XML_PENALTY_DESC)){
                                penalty.setPenaltyDesc(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Penalty.XML_UOM)){
                                penalty.setUom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Penalty.XMl_CROP_TYPE)){
                                penalty.setCropType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Penalty.XML_IS_LOADING)){
                                penalty.setIsLoading(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Penalty.XML_IS_ANCAK)){
                                penalty.setIsAncak(cursor.getInt(i));
                            }
                        }

                        listObject.add(penalty);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(DeviceAlias.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        DeviceAlias deviceAlias = new DeviceAlias();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(DeviceAlias.XML_DEVICE_ALIAS)){
                                deviceAlias.setDeviceAlias(cursor.getString(i));
                            }
                        }

                        listObject.add(deviceAlias);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(BlockPlanning.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        BlockPlanning blockPlanning = new BlockPlanning();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BlockPlanning.XML_BLOCK)){
                                blockPlanning.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockPlanning.XML_CREATED_DATE)){
                                blockPlanning.setCreatedDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockPlanning.XML_CREATED_BY)){
                                blockPlanning.setCreatedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(blockPlanning);
                    }while(cursor.moveToNext());
                }
            } else if (tableName.equals(AncakPanenHeader.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        AncakPanenHeader ancakPanenHeader = new AncakPanenHeader();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_ANCAK_PANEN_ID)){
                                ancakPanenHeader.setAncakPanenId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_IMEI)){
                                ancakPanenHeader.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_COMPANY_CODE)){
                                ancakPanenHeader.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_ESTATE)){
                                ancakPanenHeader.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_ANCAK_DATE)){
                                ancakPanenHeader.setAncakDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_DIVISION)){
                                ancakPanenHeader.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_GANG)){
                                ancakPanenHeader.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_LOCATION)){
                                ancakPanenHeader.setLocation(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_TPH)){
                                ancakPanenHeader.setTph(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_NIK_HARVESTER)){
                                ancakPanenHeader.setNikHarvester(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_HARVESTER)){
                                ancakPanenHeader.setHarvester(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_NIK_FOREMAN)){
                                ancakPanenHeader.setNikForeman(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_FOREMAN)){
                                ancakPanenHeader.setForeman(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_NIK_CLERK)){
                                ancakPanenHeader.setNikClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_CLERK)){
                                ancakPanenHeader.setClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_CROP)){
                                ancakPanenHeader.setCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_GPS_KOORDINAT)){
                                ancakPanenHeader.setGpsKoordinat(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_STATUS)){
                                ancakPanenHeader.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_CREATED_DATE)){
                                ancakPanenHeader.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_CREATED_BY)){
                                ancakPanenHeader.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_MODIFIED_DATE)){
                                ancakPanenHeader.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_MODIFIED_BY)){
                                ancakPanenHeader.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(ancakPanenHeader);
                    }while (cursor.moveToNext());
                }
            } else if (tableName.equals(AncakPanenQuality.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        AncakPanenQuality ancakPanenQuality = new AncakPanenQuality();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_ANCAK_PANEN_ID)){
                                ancakPanenQuality.setAncakPanenId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_IMEI)){
                                ancakPanenQuality.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_COMPANY_CODE)){
                                ancakPanenQuality.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_ESTATE)){
                                ancakPanenQuality.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_ANCAK_DATE)){
                                ancakPanenQuality.setAncakDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_DIVISION)){
                                ancakPanenQuality.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_GANG)){
                                ancakPanenQuality.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_LOCATION)){
                                ancakPanenQuality.setLocation(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_TPH)){
                                ancakPanenQuality.setTph(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_NIK_HARVESTER)){
                                ancakPanenQuality.setNikHarvester(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_CROP)){
                                ancakPanenQuality.setCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_ACHIEVEMENT_CODE)){
                                ancakPanenQuality.setAchievementCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_QUALITY_CODE)){
                                ancakPanenQuality.setQualityCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_QUANTITY)){
                                ancakPanenQuality.setQuantity(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_STATUS)){
                                ancakPanenQuality.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_CREATED_DATE)){
                                ancakPanenQuality.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_CREATED_BY)){
                                ancakPanenQuality.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_MODIFIED_DATE)){
                                ancakPanenQuality.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_MODIFIED_BY)){
                                ancakPanenQuality.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(ancakPanenQuality);
                    } while (cursor.moveToNext());
                }
            }else if(tableName.equals(SPBSDestination.TABLE_NAME)){
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        SPBSDestination spbsDestination = new SPBSDestination();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(SPBSDestination.XML_MANDT)){
                                spbsDestination.setMandt(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSDestination.XML_ESTATE)){
                                spbsDestination.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSDestination.XML_DEST_TYPE)){
                                spbsDestination.setDestType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSDestination.XML_DEST_ID)){
                                spbsDestination.setDestId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSDestination.XML_DEST_DESC)){
                                spbsDestination.setDestDesc(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSDestination.XML_ACTIVE)){
                                spbsDestination.setActive(cursor.getInt(i));
                            }
                        }

                        listObject.add(spbsDestination);
                    }while (cursor.moveToNext());
                }
            }else if (tableName.equals(SPTARunningNumber.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        SPTARunningNumber sptaRunningNumber = new SPTARunningNumber();

                        for(int i = 0; i <cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_ID)){
                                sptaRunningNumber.setId(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_ESTATE)){
                                sptaRunningNumber.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_DIVISION)){
                                sptaRunningNumber.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_YEAR)){
                                sptaRunningNumber.setYear(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_MONTH)){
                                sptaRunningNumber.setMonth(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_IMEI)){
                                sptaRunningNumber.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_RUNNING_NUMBER)){
                                sptaRunningNumber.setRunningNumber(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_DEVICE_ALIAS)){
                                sptaRunningNumber.setDeviceAlias(cursor.getString(i));
                            }
                        }

                        listObject.add(sptaRunningNumber);
                    }while(cursor.moveToNext());
                }
            }else if (tableName.equals(Vendor.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        Vendor vendor = new Vendor();

                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(Vendor.XML_ESTATE)){
                                vendor.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Vendor.XML_LIFNR)){
                                vendor.setLifnr(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Vendor.XML_NAME)){
                                vendor.setName(cursor.getString(i));
                            }
                        }

                        listObject.add(vendor);
                    }while (cursor.moveToNext());
                }
            }else if(tableName.equals(BLKSUGC.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        BLKSUGC blksugc = new BLKSUGC();

                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_COMPANY_CODE)){
                                blksugc.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_ESTATE)){
                                blksugc.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_BLOCK)){
                                blksugc.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_VALID_FROM)){
                                blksugc.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_VALID_TO)){
                                blksugc.setValidTo(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_PHASE)){
                                blksugc.setPhase(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_DISTANCE)){
                                blksugc.setDistance(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_SUB_DIVISION)){
                                blksugc.setSubDivision(cursor.getString(i));
                            }
                        }

                        listObject.add(blksugc);
                    }while (cursor.moveToNext());
                }
            }else if(tableName.equals(GroupHead.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        GroupHead groupHead = new GroupHead();

                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_COMPANY_CODE)){
                                groupHead.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_ESTATE)){
                                groupHead.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_LIFNR)){
                                groupHead.setLifnr(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_INITIAL)){
                                groupHead.setInitial(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_VALID_FROM)){
                                groupHead.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_VALID_TO)){
                                groupHead.setValidTo(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_NAME)){
                                groupHead.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_STATUS)){
                                groupHead.setStatus(cursor.getInt(i));
                            }
                        }

                        listObject.add(groupHead);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(SPTA.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        SPTA spta = new SPTA();

                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_ZYEAR)){
                                spta.setZyear(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_IMEI)){
                                spta.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_SPTA_NUM)){
                                spta.setSptaNum(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_COMPANY_CODE)){
                                spta.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_ESTATE)){
                                spta.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_DIVISI)){
                                spta.setDivisi(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_SPTA_DATE)){
                                spta.setSptaDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_SUB_DIV)){
                                spta.setSubDiv(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_PETAK_ID)){
                                spta.setPetakId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_VENDOR_ID)){
                                spta.setVendorId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_NOPOL)){
                                spta.setNopol(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_LOGO)){
                                spta.setLogo(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_JARAK)){
                                spta.setJarak(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_RUN_ACC_1)){
                                spta.setRunAcc1(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_EMPL_ID_1)){
                                spta.setEmplId1(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_RUN_ACC_2)){
                                spta.setRunAcc2(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_EMPL_ID_2)){
                                spta.setEmplId2(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_CHOPPED_DATE)){
                                spta.setChoppedDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_CHOPPED_HOUR)){
                                spta.setChoppedHour(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_BURN_DATE)){
                                spta.setBurnDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_BURN_HOUR)){
                                spta.setBurnHour(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_LOAD_DATE)){
                                spta.setLoadDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_LOAD_HOUR)){
                                spta.setLoadHour(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_QUALITY)){
                                spta.setQuality(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_INSENTIVE_GULMA)){
                                spta.setInsentiveGulma(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_INSENTIVE_LANGSIR)){
                                spta.setInsentiveLangsir(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_INSENTIVE_ROBOH)){
                                spta.setInsentiveRoboh(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_CANE_TYPE)){
                                spta.setCaneType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_COST_TEBANG)){
                                spta.setCostTebang(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_COST_MUAT)){
                                spta.setCostMuat(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_COST_ANGKUT)){
                                spta.setCostAngkut(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_PENALTY_TRASH)){
                                spta.setPenaltyTrash(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_GPS_KOORDINAT)){
                                spta.setGpsKoordinat(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_IS_SAVE)){
                                spta.setIsSave(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_STATUS)){
                                spta.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_CREATED_DATE)){
                                spta.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_CREATED_BY)){
                                spta.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_MODIFIED_DATE)){
                                spta.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_MODIFIED_BY)){
                                spta.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(spta);
                    }while (cursor.moveToNext());
                }
            }else if(tableName.equals(RKH_ACTIVITY_TYPE.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        RKH_ACTIVITY_TYPE rkh_activity_type = new RKH_ACTIVITY_TYPE();

                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(rkh_activity_type.XML_ACTTYPE)){
                                rkh_activity_type.setACTTYPE(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_activity_type.XML_NAME)){
                                rkh_activity_type.setNAME(cursor.getString(i));
                            }
                        }
                        listObject.add(rkh_activity_type);
                    }while (cursor.moveToNext());
                }
            }else if(tableName.equals(RKH_HEADER.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        RKH_HEADER rkh_header = new RKH_HEADER();

                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_IMEI)){
                                rkh_header.setIMEI(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_COMPANY_CODE)){
                                rkh_header.setCOMPANY_CODE(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_ESTATE)){
                                rkh_header.setESTATE(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_RKH_DATE)){
                                rkh_header.setRKH_DATE(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_DIVISION)){
                                rkh_header.setDIVISION(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_GANG)){
                                rkh_header.setGANG(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_NIK_FOREMAN)){
                                rkh_header.setNIK_FOREMAN(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_FOREMAN)){
                                rkh_header.setFOREMAN(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_NIK_CLERK)){
                                rkh_header.setNIK_CLERK(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_CLERK)){
                                rkh_header.setCLERK(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_GPS_KOORDINAT)){
                                rkh_header.setGPS_KOORDINAT(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_STATUS)){
                                rkh_header.setSTATUS(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_CREATED_DATE)){
                                rkh_header.setCREATED_DATE(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_CREATED_BY)){
                                rkh_header.setCREATED_BY(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_MODIFIED_DATE)){
                                rkh_header.setMODIFIED_DATE(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_MODIFIED_BY)){
                                rkh_header.setMODIFIED_BY(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_RKH_ID)){
                                rkh_header.setRKH_ID(cursor.getString(i));
                            }
                        }
                        listObject.add(rkh_header);
                    }while (cursor.moveToNext());
                }
            }else if(tableName.equals(RKH_ITEM.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        RKH_ITEM rkh_item = new RKH_ITEM();

                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_RKH_ID)){
                                rkh_item.setRKH_ID(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_LINE)){
                                rkh_item.setLINE(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_ACTIVITY)){
                                rkh_item.setACTIVITY(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_BLOCK)){
                                rkh_item.setBLOCK(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_TARGET_OUTPUT)){
                                rkh_item.setTARGET_OUTPUT(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_SKU)){
                                rkh_item.setSKU(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_PHL)){
                                rkh_item.setPHL(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_TARGET_HASIL)){
                                rkh_item.setTARGET_HASIL(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_TARGET_JANJANG)){
                                rkh_item.setTARGET_JANJANG(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_BJR)){
                                rkh_item.setBJR(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_GPS_KOORDINAT)){
                                rkh_item.setGPS_KOORDINAT(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_STATUS)){
                                rkh_item.setSTATUS(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_CREATED_DATE)){
                                rkh_item.setCREATED_DATE(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_CREATED_BY)){
                                rkh_item.setCREATED_BY(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_MODIFIED_DATE)){
                                rkh_item.setMODIFIED_DATE(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_MODIFIED_BY)){
                                rkh_item.setMODIFIED_BY(cursor.getString(i));
                            }
                        }
                        listObject.add(rkh_item);
                    }while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            cursor.close();
        }

        return listObject;
    }


    public List<Object> getListDataRawQuery(String sqldb_query, String tableName,String []a) {

        Cursor cursor = null;
        List<Object> listObject = new ArrayList<Object>();

        try {
            cursor = sqliteDatabase.rawQuery(sqldb_query,a);
            if (tableName.equals(tblT_BlockConditionScore.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        tblT_BlockConditionScore md = new tblT_BlockConditionScore();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_CompanyCode)) {
                                md.setAcc_CompanyCode(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Estate)) {
                                md.setAcc_Estate(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_ZYear)) {
                                md.setAcc_ZYear(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Period)) {
                                md.setAcc_Period(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TransLevel)) {
                                md.setAcc_TransLevel(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Block)) {
                                md.setAcc_Block(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Jalan)) {
                                md.setAcc_Jalan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Jembatan)) {
                                md.setAcc_Jembatan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Parit)) {
                                md.setAcc_Parit(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TitiPanen)) {
                                md.setAcc_TitiPanen(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TitiRintis)) {
                                md.setAcc_TitiRintis(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Tikus)) {
                                md.setAcc_Tikus(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Pencurian)) {
                                md.setAcc_Pencurian(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_BW)) {
                                md.setAcc_BW(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TPHBersih)) {
                                md.setAcc_TPHBersih(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TPH)) {
                                md.setAcc_TPH(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TPHBersih2)) {
                                    md.setAcc_TPHBersih2(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TPH2)) {
                                    md.setAcc_TPH2(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Piringan)) {
                                md.setAcc_Piringan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_PasarPanen)) {
                                md.setAcc_PasarPanen(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_PasarRintis)) {
                                md.setAcc_PasarRintis(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_TunasPokok)) {
                                md.setAcc_TunasPokok(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Gawangan)) {
                                md.setAcc_Gawangan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Drainase)) {
                                md.setAcc_Drainase(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Ganoderma)) {
                                md.setAcc_Ganoderma(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Rayap)) {
                                md.setAcc_Rayap(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Orcytes)) {
                                md.setAcc_Orcytes(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Sanitasi)) {
                                md.setAcc_Sanitasi(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Kacangan)) {
                                md.setAcc_Kacangan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_CreatedDateTime)) {
                                md.setAcc_CreatedDateTime(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_CreatedBy)) {
                                md.setAcc_CreatedBy(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_ModifiedDateTime)) {
                                md.setAcc_ModifiedDateTime(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_ModifiedBy)) {
                                md.setAcc_ModifiedBy(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockConditionScore.XML_Color)) {
                                md.setAcc_Color(cursor.getString(i));
                            }


                        }

                        listObject.add(md);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(tblT_BlockCondition.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        tblT_BlockCondition md = new tblT_BlockCondition();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_CompanyCode)) {
                                md.setAcc_CompanyCode(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Estate)) {
                                md.setAcc_Estate(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_ZYear)) {
                                md.setAcc_ZYear(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Period)) {
                                md.setAcc_Period(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_ZDate)) {
                                md.setAcc_ZDate(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TransLevel)) {
                                md.setAcc_TransLevel(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Block)) {
                                md.setAcc_Block(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_SKB)) {
                                md.setAcc_SKB(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_BlockRow)) {
                                md.setAcc_BlockRow(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_CensusPoint)) {
                                md.setAcc_CensusPoint(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_PokokLabel)) {
                                md.setAcc_PokokLabel(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_PokokSide)) {
                                md.setAcc_PokokSide(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_PokokCondition)) {
                                md.setAcc_PokokCondition(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Jalan)) {
                                md.setAcc_Jalan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Jembatan)) {
                                md.setAcc_Jembatan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Parit)) {
                                md.setAcc_Parit(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TitiPanen)) {
                                md.setAcc_TitiPanen(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TitiRintis)) {
                                md.setAcc_TitiRintis(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Tikus)) {
                                md.setAcc_Tikus(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Pencurian)) {
                                md.setAcc_Pencurian(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_BW)) {
                                md.setAcc_BW(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TPHBersih)) {
                                md.setAcc_TPHBersih(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TPH)) {
                                md.setAcc_TPH(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TPH2)) {
                                md.setAcc_TPH2(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TPHBersih2)) {
                                md.setAcc_TPHBersih2(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Piringan)) {
                                md.setAcc_Piringan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_PasarPanen)) {
                                md.setAcc_PasarPanen(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_PasarRintis)) {
                                md.setAcc_PasarRintis(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_TunasPokok)) {
                                md.setAcc_TunasPokok(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Gawangan)) {
                                md.setAcc_Gawangan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Drainase)) {
                                md.setAcc_Drainase(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Ganoderma)) {
                                md.setAcc_Ganoderma(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Rayap)) {
                                md.setAcc_Rayap(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Orcytes)) {
                                md.setAcc_Orcytes(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Sanitasi)) {
                                md.setAcc_Sanitasi(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Kacangan)) {
                                md.setAcc_Kacangan(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_CreatedDateTime)) {
                                md.setAcc_CreatedDateTime(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_CreatedBy)) {
                                md.setAcc_CreatedBy(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_ModifiedDateTime)) {
                                md.setAcc_ModifiedDateTime(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_ModifiedBy)) {
                                md.setAcc_ModifiedBy(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Flag)) {
                                md.setAcc_Flag(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_DIVISI)) {
                                md.setAcc_DIVISI(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(tblT_BlockCondition.XML_Remark)) {
                                md.setAcc_Remark(cursor.getString(i));
                            }



                        }

                        listObject.add(md);
                    } while (cursor.moveToNext());
                }
            }else if(tableName.equals(tblM_BlockConditionScore.TABLE_NAME)){
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        tblM_BlockConditionScore md = new tblM_BlockConditionScore();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_Color)){
                                md.setAcc_Color(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_Interval1)){
                                md.setAcc_Interval1(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_Interval2)){
                                md.setAcc_Interval2(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_Score1)){
                                md.setAcc_Score1(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_Score2)){
                                md.setAcc_Score2(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_Score3)){
                                md.setAcc_Score3(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_ScoreType)){
                                md.setAcc_ScoreType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_ValidFrom)){
                                md.setAcc_ValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_BlockConditionScore.XML_ValidTo)){
                                md.setAcc_ValidTo(cursor.getString(i));
                            }
                        }

                        listObject.add(md);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(tblM_AccountingPeriod.TABLE_NAME)){
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        tblM_AccountingPeriod md = new tblM_AccountingPeriod();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_Estate)){
                                md.setAcc_Estate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_ZYear)){
                                md.setAcc_ZYear(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_Period)){
                                md.setAcc_Period(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_ClosingDate)){
                                md.setAcc_ClosingDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_Status)){
                                md.setAcc_Status(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_Active)){
                                md.setAcc_Active(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_CreatedBy)){
                                md.setAcc_CreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_CreatedDate)){
                                md.setAcc_CreatedDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_ModifiedBy)){
                                md.setAcc_ModifiedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(tblM_AccountingPeriod.XML_ModifiedDate)){
                                md.setAcc_ModifiedDate(cursor.getString(i));
                            }
                        }

                        listObject.add(md);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(MasterDownload.TABLE_NAME)){
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        MasterDownload md = new MasterDownload();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(MasterDownload.XML_NAME)){
                                md.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(MasterDownload.XML_FILENAME)){
                                md.setFileName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(MasterDownload.XML_SYNC_DATE)){
                                md.setSyncDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(MasterDownload.XML_STATUS)){
                                md.setStatus(cursor.getInt(i));
                            }
                        }

                        listObject.add(md);
                    }while(cursor.moveToNext());
                }
            }else if (tableName.equals(SKB.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        SKB skb = new SKB();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_COMPANY_CODE)){
                                skb.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_ESTATE)){
                                skb.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_BLOCK)){
                                skb.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_BARIS_SKB)){
                                skb.setBarisSkb(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_VALID_FROM)){
                                skb.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_VALID_TO)){
                                skb.setValidTo(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_BARIS_BLOCK)){
                                skb.setBarisBlok(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_JUMLAH_POKOK)){
                                skb.setJumlahPokok(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_POKOK_MATI)){
                                skb.setPokokMati(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_TANGGAL_TANAM)){
                                skb.setTanggalTanam(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SKB.XML_LINE_SKB)){
                                skb.setLineSkb(cursor.getInt(i));
                            }
                        }

                        listObject.add(skb);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BlockHdrc.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        BlockHdrc blockHdrc = new BlockHdrc();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_COMPANY_CODE)){
                                blockHdrc.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_ESTATE)){
                                blockHdrc.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_BLOCK)){
                                blockHdrc.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_VALID_FROM)){
                                blockHdrc.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_VALID_TO)){
                                blockHdrc.setValidTo(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_DIVISION)){
                                blockHdrc.setDivisi(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_TYPE)){
                                blockHdrc.setType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_STATUS)){
                                blockHdrc.setStatus(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_PROJECT_DEFINITION)){
                                blockHdrc.setProjectDefinition(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_OWNER)){
                                blockHdrc.setOwner(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_TGL_TANAM)){
                                blockHdrc.setTglTanam(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockHdrc.XML_MANDT)){
                                blockHdrc.setMandt(cursor.getString(i));
                            }
                        }

                        listObject.add(blockHdrc);
                    }while(cursor.moveToNext());				}
            } else if (tableName.equals(BLKSBC.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BLKSBC blksbc = new BLKSBC();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_COMPANY_CODE)){
                                blksbc.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_ESTATE)){
                                blksbc.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_BLOCK)){
                                blksbc.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_VALID_FROM)){
                                blksbc.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_BASIS_NORMAL)){
                                blksbc.setBasisNormal(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_BASIS_FRIDAY)){
                                blksbc.setBasisFriday(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_BASIS_HOLIDAY)){
                                blksbc.setBasisHoliday(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_PREMI_NORMAL)){
                                blksbc.setPremiNormal(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_PREMI_FRIDAY)){
                                blksbc.setPremiFriday(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_PREMI_HOLIDAY)){
                                blksbc.setPremiHoliday(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBC.XML_PRIORITY)){
                                blksbc.setPriority(cursor.getString(i));
                            }
                        }

                        listObject.add(blksbc);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BLKSBCDetail.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BLKSBCDetail blksbcDetail = new BLKSBCDetail();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBCDetail.XML_COMPANY_CODE)){
                                blksbcDetail.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBCDetail.XML_ESTATE)){
                                blksbcDetail.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBCDetail.XML_BLOCK)){
                                blksbcDetail.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBCDetail.XML_VALID_FROM)){
                                blksbcDetail.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBCDetail.XML_MIN_VAL)){
                                blksbcDetail.setMinimumValue(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBCDetail.XML_MAX_VAL)){
                                blksbcDetail.setMaximumValue(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSBCDetail.XML_OVER_BASIC_RATE)){
                                blksbcDetail.setOverBasicRate(cursor.getDouble(i));
                            }
                        }

                        listObject.add(blksbcDetail);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BJR.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BJR bjr = new BJR();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BJR.XML_COMPANY_CODE)){
                                bjr.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BJR.XML_ESTATE)){
                                bjr.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BJR.XMl_EFF_DATE)){
                                bjr.setEffDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BJR.XML_BLOCK)){
                                bjr.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BJR.XML_BJR)){
                                bjr.setBjr(cursor.getDouble(i));
                            }
                        }

                        listObject.add(bjr);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BLKPLT.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BLKPLT blkplt = new BLKPLT();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_COMPANY_CODE)){
                                blkplt.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_ESTATE)){
                                blkplt.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_BLOCK)){
                                blkplt.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_VALID_FROM)){
                                blkplt.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_VALID_TO)){
                                blkplt.setValidTo(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_CROP_TYPE)){
                                blkplt.setCropType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_PREVIOUS_CROP)){
                                blkplt.setPreviousCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_FINISH_DATE)){
                                blkplt.setFinishDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_REFERENCE)){
                                blkplt.setReference(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_JARAK_TANAM)){
                                blkplt.setJarakTanam(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_HARVESTING_DATE)){
                                blkplt.setHarvestingDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_HARVESTED)){
                                blkplt.setHarvested(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_PLAN_DATE)){
                                blkplt.setPlanDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_TOPOGRAPHY)){
                                blkplt.setTopography(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_SOIL_TYPE)){
                                blkplt.setSoilType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_SOIL_CATEGORY)){
                                blkplt.setSoilCategory(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKPLT.XML_PROD_TREES)){
                                blkplt.setProdTrees(cursor.getDouble(i));
                            }
                        }

                        listObject.add(blkplt);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(Employee.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        Employee employee = new Employee();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_COMPANY_CODE) ){
                                employee.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_ESTATE)){
                                employee.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_FISCAL_YEAR)){
                                employee.setFiscalYear(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_FISCAL_PERIOD)){
                                employee.setFiscalPeriod(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_NIK)){
                                employee.setNik(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_NAME)){
                                employee.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_TERM_DATE)){
                                employee.setTermDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_DIVISION)){
                                employee.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_ROLE_ID)){
                                employee.setRoleId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_JOB_POS)){
                                employee.setJobPos(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_GANG)){
                                employee.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_COST_CENTER)){
                                employee.setCostCenter(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_EMP_TYPE)){
                                employee.setEmpType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_VALID_FROM)){
                                employee.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Employee.XML_HARVESTER_CODE)){
                                employee.setHarvesterCode(cursor.getString(i));
                            }
                        }

                        listObject.add(employee);
                    }while (cursor.moveToNext());
                }
            } else if (tableName.equals(UserApp.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        UserApp userApp = new UserApp();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(UserApp.XML_NIK)){
                                userApp.setNik(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserApp.XML_USERNAME)){
                                userApp.setUsername(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserApp.XML_PASSWORD)){
                                userApp.setPassword(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserApp.XML_VALID_TO)){
                                userApp.setValidTo(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserApp.XML_CREATED_DATE)){
                                userApp.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserApp.XML_CREATED_BY)){
                                userApp.setCreatedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(userApp);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(AbsentType.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        AbsentType absentType = new AbsentType();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_COMPANY_CODE)){
                                absentType.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_ABSENT_TYPE)){
                                absentType.setAbsentType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_DESCRIPTION)){
                                absentType.setDescription(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_HKRLLO)){
                                absentType.setHkrllo(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_HKRLHI)){
                                absentType.setHkrlhi(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_HKPYLO)){
                                absentType.setHkpylo(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_HKPYHI)){
                                absentType.setHkpyhi(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_HKVLLO)){
                                absentType.setHkvllo(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_HKVLHI)){
                                absentType.setHkvlhi(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_HKMEIN)){
                                absentType.setHkmein(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AbsentType.XML_AGROUP)){
                                absentType.setAgroup(cursor.getString(i));
                            }
                        }

                        listObject.add(absentType);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(DayOff.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        DayOff dayOff = new DayOff();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(DayOff.XML_ESTATE)){
                                dayOff.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DayOff.XML_DATE)){
                                dayOff.setDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DayOff.XML_DAY_OFF_TYPE)){
                                dayOff.setDayOffType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DayOff.XML_DESCRIPTION)){
                                dayOff.setDescription(cursor.getString(i));
                            }
                        }

                        listObject.add(dayOff);
                    } while (cursor.moveToNext());
                }
            }else if (tableName.equals(DivisionAssistant.TABLE_NAME)){
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        DivisionAssistant assistantDivision = new DivisionAssistant();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_ESTATE)){
                                assistantDivision.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_DIVISION)){
                                assistantDivision.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_SPRAS)){
                                assistantDivision.setSpras(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_DESCRIPTION)){
                                assistantDivision.setDescription(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_ASSISTANT)){
                                assistantDivision.setAssistant(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_DISTANCE_TO_MILL)){
                                assistantDivision.setDistanceToMill(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_UOM)){
                                assistantDivision.setUom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_ASSISTANT_NAME)){
                                assistantDivision.setAssistantName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(DivisionAssistant.XML_LIFNR)) {
                                assistantDivision.setLifnr(cursor.getString(i));
                            }
                        }

                        listObject.add(assistantDivision);
                    }while(cursor.moveToNext());
                }
            } else if (tableName.equals(ForemanActive.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        ForemanActive foremanActive = new ForemanActive();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_COMPANY_CODE) ){
                                foremanActive.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_ESTATE)){
                                foremanActive.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_FISCAL_YEAR)){
                                foremanActive.setFiscalYear(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_FISCAL_PERIOD)){
                                foremanActive.setFiscalPeriod(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_NIK)){
                                foremanActive.setNik(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_NAME)){
                                foremanActive.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_TERM_DATE)){
                                foremanActive.setTermDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_DIVISION)){
                                foremanActive.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_ROLE_ID)){
                                foremanActive.setRoleId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_JOB_POS)){
                                foremanActive.setJobPos(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_GANG)){
                                foremanActive.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_COST_CENTER)){
                                foremanActive.setCostCenter(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_EMP_TYPE)){
                                foremanActive.setEmpType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_VALID_FROM)){
                                foremanActive.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(ForemanActive.XML_HARVESTER_CODE)){
                                foremanActive.setHarvesterCode(cursor.getString(i));
                            }
                        }

                        listObject.add(foremanActive);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(UserLogin.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        UserLogin userLogin = new UserLogin();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_COMPANY_CODE) ){
                                userLogin.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_ESTATE)){
                                userLogin.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_FISCAL_YEAR)){
                                userLogin.setFiscalYear(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_FISCAL_PERIOD)){
                                userLogin.setFiscalPeriod(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_NIK)){
                                userLogin.setNik(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_NAME)){
                                userLogin.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_TERM_DATE)){
                                userLogin.setTermDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_DIVISION)){
                                userLogin.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_ROLE_ID)){
                                userLogin.setRoleId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_JOB_POS)){
                                userLogin.setJobPos(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_GANG)){
                                userLogin.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_COST_CENTER)){
                                userLogin.setCostCenter(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_EMP_TYPE)){
                                userLogin.setEmpType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_VALID_FROM)){
                                userLogin.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(UserLogin.XML_HARVESTER_CODE)){
                                userLogin.setHarvesterCode(cursor.getString(i));
                            }
                        }

                        listObject.add(userLogin);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BPNHeader.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        BPNHeader bpnHeader = new BPNHeader();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_BPN_ID)){
                                bpnHeader.setBpnId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_IMEI)){
                                bpnHeader.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_COMPANY_CODE)){
                                bpnHeader.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_ESTATE)){
                                bpnHeader.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_BPN_DATE)){
                                bpnHeader.setBpnDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_DIVISION)){
                                bpnHeader.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_GANG)){
                                bpnHeader.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_LOCATION)){
                                bpnHeader.setLocation(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_TPH)){
                                bpnHeader.setTph(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_NIK_HARVESTER)){
                                bpnHeader.setNikHarvester(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_HARVESTER)){
                                bpnHeader.setHarvester(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_NIK_FOREMAN)){
                                bpnHeader.setNikForeman(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_FOREMAN)){
                                bpnHeader.setForeman(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_NIK_CLERK)){
                                bpnHeader.setNikClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_CLERK)){
                                bpnHeader.setClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_USE_GERDANG)){
                                bpnHeader.setUseGerdang(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_CROP)){
                                bpnHeader.setCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_GPS_KOORDINAT)){
                                bpnHeader.setGpsKoordinat(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_PHOTO)){
                                bpnHeader.setPhoto(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_STATUS)){
                                bpnHeader.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_SPBS_NUMBER)){
                                bpnHeader.setSpbsNumber(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_CREATED_DATE)){
                                bpnHeader.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_CREATED_BY)){
                                bpnHeader.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_MODIFIED_DATE)){
                                bpnHeader.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNHeader.XML_MODIFIED_BY)){
                                bpnHeader.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(bpnHeader);
                    }while (cursor.moveToNext());
                }
            } else if (tableName.equals(BPNQuantity.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BPNQuantity bpnQuantity = new BPNQuantity();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_BPN_ID)){
                                bpnQuantity.setBpnId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_IMEI)){
                                bpnQuantity.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_COMPANY_CODE)){
                                bpnQuantity.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_ESTATE)){
                                bpnQuantity.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_BPN_DATE)){
                                bpnQuantity.setBpnDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_DIVISION)){
                                bpnQuantity.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_GANG)){
                                bpnQuantity.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_LOCATION)){
                                bpnQuantity.setLocation(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_TPH)){
                                bpnQuantity.setTph(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_NIK_HARVESTER)){
                                bpnQuantity.setNikHarvester(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_CROP)){
                                bpnQuantity.setCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_ACHIEVEMENT_CODE)){
                                bpnQuantity.setAchievementCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_QUANTITY)){
                                bpnQuantity.setQuantity(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_QUANTITY_REMAINING)){
                                bpnQuantity.setQuantityRemaining(cursor.getDouble(i));
                            }
                            else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_STATUS)){
                                bpnQuantity.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_CREATED_DATE)){
                                bpnQuantity.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_CREATED_BY)){
                                bpnQuantity.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_MODIFIED_DATE)){
                                bpnQuantity.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuantity.XML_MODIFIED_BY)){
                                bpnQuantity.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(bpnQuantity);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BPNQuality.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BPNQuality bpnQuality = new BPNQuality();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_BPN_ID)){
                                bpnQuality.setBpnId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_IMEI)){
                                bpnQuality.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_COMPANY_CODE)){
                                bpnQuality.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_ESTATE)){
                                bpnQuality.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_BPN_DATE)){
                                bpnQuality.setBpnDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_DIVISION)){
                                bpnQuality.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_GANG)){
                                bpnQuality.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_LOCATION)){
                                bpnQuality.setLocation(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_TPH)){
                                bpnQuality.setTph(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_NIK_HARVESTER)){
                                bpnQuality.setNikHarvester(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_CROP)){
                                bpnQuality.setCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_ACHIEVEMENT_CODE)){
                                bpnQuality.setAchievementCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_QUALITY_CODE)){
                                bpnQuality.setQualityCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_QUANTITY)){
                                bpnQuality.setQuantity(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_STATUS)){
                                bpnQuality.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_CREATED_DATE)){
                                bpnQuality.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_CREATED_BY)){
                                bpnQuality.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_MODIFIED_DATE)){
                                bpnQuality.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BPNQuality.XML_MODIFIED_BY)){
                                bpnQuality.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(bpnQuality);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BKMHeader.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BKMHeader bkmHeader = new BKMHeader();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_IMEI)){
                                bkmHeader.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_COMPANY_CODE)){
                                bkmHeader.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_ESTATE)){
                                bkmHeader.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_BKM_DATE)){
                                bkmHeader.setBkmDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_DIVISION)){
                                bkmHeader.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_GANG)){
                                bkmHeader.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_NIK_FOREMAN)){
                                bkmHeader.setNikForeman(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_FOREMAN)){
                                bkmHeader.setForeman(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_NIK_CLERK)){
                                bkmHeader.setNikClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_CLERK)){
                                bkmHeader.setClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_GPS_KOORDINAT)){
                                bkmHeader.setGpsKoordinat(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_STATUS)){
                                bkmHeader.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_CREATED_DATE)){
                                bkmHeader.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_CREATED_BY)){
                                bkmHeader.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_MODIFIED_DATE)){
                                bkmHeader.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMHeader.XML_MODIFIED_BY)){
                                bkmHeader.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(bkmHeader);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BKMLine.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        BKMLine bkmLine = new BKMLine();

                        for(int i = 0; i <  cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_IMEI)){
                                bkmLine.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_COMPANY_CODE)){
                                bkmLine.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_ESTATE)){
                                bkmLine.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_BKM_DATE)){
                                bkmLine.setBkmDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_DIVISION)){
                                bkmLine.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_GANG)){
                                bkmLine.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_NIK)){
                                bkmLine.setNik(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_NAME)){
                                bkmLine.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_ABSENT_TYPE)){
                                bkmLine.setAbsentType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_MANDAYS)){
                                bkmLine.setMandays(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_UOM)){
                                bkmLine.setUom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_USE_GERDANG)){
                                bkmLine.setUseGerdang(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_STATUS)){
                                bkmLine.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_CREATED_DATE)){
                                bkmLine.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_CREATED_BY)){
                                bkmLine.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_MODIFIED_DATE)){
                                bkmLine.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMLine.XML_MODIFIED_BY)){
                                bkmLine.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(bkmLine);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(BKMOutput.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        BKMOutput bkmOutput = new BKMOutput();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_IMEI)){
                                bkmOutput.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_COMPANY_CODE)){
                                bkmOutput.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_ESTATE)){
                                bkmOutput.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_BKM_DATE)){
                                bkmOutput.setBkmDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_DIVISION)){
                                bkmOutput.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_GANG)){
                                bkmOutput.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_BLOCK)){
                                bkmOutput.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_NIK)){
                                bkmOutput.setNik(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_NAME)){
                                bkmOutput.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_OUTPUT)){
                                bkmOutput.setOutput(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_UOM)){
                                bkmOutput.setUom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_STATUS)){
                                bkmOutput.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_CREATED_DATE)){
                                bkmOutput.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_CREATED_BY)){
                                bkmOutput.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_MODIFIED_DATE)){
                                bkmOutput.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BKMOutput.XML_MODIFIED_BY)){
                                bkmOutput.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(bkmOutput);
                    }while(cursor.moveToNext());
                }
            } else if (tableName.equals(SPBSHeader.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        SPBSHeader spbsHeader = new SPBSHeader();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_IMEI)){
                                spbsHeader.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_YEAR)){
                                spbsHeader.setYear(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_COMPANY_CODE)){
                                spbsHeader.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_ESTATE)){
                                spbsHeader.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_CROP)){
                                spbsHeader.setCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_SPBS_NUMBER)){
                                spbsHeader.setSpbsNumber(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_SPBS_DATE)){
                                spbsHeader.setSpbsDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_DEST_ID)){
                                spbsHeader.setDestId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_DEST_DESC)){
                                spbsHeader.setDestDesc(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_DEST_TYPE)){
                                spbsHeader.setDestType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_DIVISION)){
                                spbsHeader.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_NIK_ASSISTANT)){
                                spbsHeader.setNikAssistant(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_ASSISTANT)){
                                spbsHeader.setAssistant(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_NIK_CLERK)){
                                spbsHeader.setNikClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_CLERK)){
                                spbsHeader.setClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_NIK_DRIVER)){
                                spbsHeader.setNikDriver(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_DRIVER)){
                                spbsHeader.setDriver(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_NIK_KERNET)){
                                spbsHeader.setNikKernet(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_KERNET)){
                                spbsHeader.setKernet(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_LICENSE_PLATE)){
                                spbsHeader.setLicensePlate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_RUNNING_ACCOUNT)){
                                spbsHeader.setRunningAccount(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_GPS_KOORDINAT)){
                                spbsHeader.setGpsKoordinat(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_IS_SAVE)){
                                spbsHeader.setIsSave(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_STATUS)){
                                spbsHeader.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_CREATED_DATE)){
                                spbsHeader.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_CREATED_BY)){
                                spbsHeader.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_MODIFIED_DATE)){
                                spbsHeader.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_MODIFIED_BY)){
                                spbsHeader.setModifiedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSHeader.XML_LIFNR)){
                                spbsHeader.setLifnr(cursor.getString(i));
                            }
                        }

                        listObject.add(spbsHeader);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(SPBSLine.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        SPBSLine spbsLine = new SPBSLine();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_ID)){
                                spbsLine.setId(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_IMEI)){
                                spbsLine.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_YEAR)){
                                spbsLine.setYear(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_COMPANY_CODE)){
                                spbsLine.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_ESTATE)){
                                spbsLine.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_CROP)){
                                spbsLine.setCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_SPBS_NUMBER)){
                                spbsLine.setSpbsNumber(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_SPBS_DATE)){
                                spbsLine.setSpbsDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_BLOCK)){
                                spbsLine.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_TPH)){
                                spbsLine.setTph(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_BPN_DATE)){
                                spbsLine.setBpnDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_ACHIEVEMENT_CODE)){
                                spbsLine.setAchievementCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_QUANTITY)){
                                spbsLine.setQuantity(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_QUANTITY_ANGKUT)){
                                spbsLine.setQuantityAngkut(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_QUANTITY_REMAINING)){
                                spbsLine.setQuantityRemaining(cursor.getDouble(i));
                            } else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_TOTAL_HARVESTER)){
                                spbsLine.setTotalHarvester(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_UOM)){
                                spbsLine.setUom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_GPS_KOORDINAT)){
                                spbsLine.setGpsKoordinat(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_IS_SAVE)){
                                spbsLine.setIsSave(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_STATUS)){
                                spbsLine.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_BPN_ID)){
                                spbsLine.setBpnId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_SPBS_REF)){
                                spbsLine.setSpbsRef(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_SPBS_NEXT)){
                                spbsLine.setSpbsNext(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_CREATED_DATE)){
                                spbsLine.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_CREATED_BY)){
                                spbsLine.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_MODIFIED_DATE)){
                                spbsLine.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSLine.XML_MODIFIED_BY)){
                                spbsLine.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(spbsLine);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(SPBSRunningNumber.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        SPBSRunningNumber spbsNumber = new SPBSRunningNumber();

                        for(int i = 0; i <cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_ID)){
                                spbsNumber.setId(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_ESTATE)){
                                spbsNumber.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_DIVISION)){
                                spbsNumber.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_YEAR)){
                                spbsNumber.setYear(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_MONTH)){
                                spbsNumber.setMonth(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_IMEI)){
                                spbsNumber.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_RUNNING_NUMBER)){
                                spbsNumber.setRunningNumber(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_DEVICE_ALIAS)){
                                spbsNumber.setDeviceAlias(cursor.getString(i));
                            }
                        }

                        listObject.add(spbsNumber);
                    }while(cursor.moveToNext());
                }
            } else if(tableName.equals(Bluetooth.TABLE_NAME)){
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        Bluetooth bluetooth = new Bluetooth();

                        for(int i = 0; i <cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(Bluetooth.XML_NAME)){
                                bluetooth.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Bluetooth.XML_ADDRESS)){
                                bluetooth.setAddress(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Bluetooth.XML_IS_PAIRED)){
                                if(cursor.getInt(i) == 1){
                                    bluetooth.setPaired(true);
                                }else{
                                    bluetooth.setPaired(false);
                                }
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Bluetooth.XML_IS_SELECTED)){
                                if(cursor.getInt(i) == 1){
                                    bluetooth.setSelected(true);
                                }else{
                                    bluetooth.setSelected(false);
                                }
                            }
                        }

                        listObject.add(bluetooth);
                    }while(cursor.moveToNext());
                }
            } else if(tableName.equals(RunningAccount.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        RunningAccount vehicle = new RunningAccount();

                        for(int i = 0; i <cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(RunningAccount.XML_COMPANY_CODE)){
                                vehicle.setCompanyCode(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(RunningAccount.XML_ESTATE)){
                                vehicle.setEstate(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(RunningAccount.XML_RUNNING_ACCOUNT)){
                                vehicle.setRunningAccount(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(RunningAccount.XML_LICENSE_PLATE)){
                                vehicle.setLicensePlate(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(RunningAccount.XML_LIFNR)){
                                vehicle.setLifnr(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(RunningAccount.XML_OWNERSHIPFLAG)){
                                vehicle.setOwnerShipFlag(cursor.getString(i));
                            }
                        }

                        listObject.add(vehicle);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(TaksasiHeader.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        TaksasiHeader taksasiHeader = new TaksasiHeader();

                        for(int i = 0; i <cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_IMEI)){
                                taksasiHeader.setImei(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_COMPANY_CODE)){
                                taksasiHeader.setCompanyCode(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_ESTATE)){
                                taksasiHeader.setEstate(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_DIVISION)){
                                taksasiHeader.setDivision(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_TAKSASI_DATE)){
                                taksasiHeader.setTaksasiDate(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_BLOCK)){
                                taksasiHeader.setBlock(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_CROP)){
                                taksasiHeader.setCrop(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_NIK_FOREMAN)){
                                taksasiHeader.setNikForeman(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_FOREMAN)){
                                taksasiHeader.setForeman(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_PROD_TREES)){
                                taksasiHeader.setProdTrees(cursor.getDouble(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_BJR)){
                                taksasiHeader.setBjr(cursor.getDouble(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_GPS_KOORDINAT)){
                                taksasiHeader.setGpsKoordinat(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_IS_SAVE)){
                                taksasiHeader.setIsSave(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_STATUS)){
                                taksasiHeader.setStatus(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_CREATED_DATE)){
                                taksasiHeader.setCreatedDate(cursor.getLong(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_CREATED_BY)){
                                taksasiHeader.setCreatedBy(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_MODIFIED_DATE)){
                                taksasiHeader.setModifiedDate(cursor.getLong(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiHeader.XML_MODIFIED_BY)){
                                taksasiHeader.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(taksasiHeader);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(TaksasiLine.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        TaksasiLine taksasiLine = new TaksasiLine();

                        for(int i = 0; i <cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_IMEI)){
                                taksasiLine.setImei(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_COMPANY_CODE)){
                                taksasiLine.setCompanyCode(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_ESTATE)){
                                taksasiLine.setEstate(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_DIVISION)){
                                taksasiLine.setDivision(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_TAKSASI_DATE)){
                                taksasiLine.setTaksasiDate(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_BLOCK)){
                                taksasiLine.setBlock(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_CROP)){
                                taksasiLine.setCrop(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_BARIS_SKB)){
                                taksasiLine.setBarisSkb(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_BARIS_BLOCK)){
                                taksasiLine.setBarisBlock(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_LINE_SKB)){
                                taksasiLine.setLineSkb(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_QTY_POKOK)){
                                taksasiLine.setQtyPokok(cursor.getDouble(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_QTY_JANJANG)){
                                taksasiLine.setQtyJanjang(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_GPS_KOORDINAT)){
                                taksasiLine.setGpsKoordinat(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_IS_SAVE)){
                                taksasiLine.setIsSave(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_STATUS)){
                                taksasiLine.setStatus(cursor.getInt(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_CREATED_DATE)){
                                taksasiLine.setCreatedDate(cursor.getLong(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_CREATED_BY)){
                                taksasiLine.setCreatedBy(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_MODIFIED_DATE)){
                                taksasiLine.setModifiedDate(cursor.getLong(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TaksasiLine.XML_MODIFIED_BY)){
                                taksasiLine.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(taksasiLine);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(Penalty.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        Penalty penalty = new Penalty();

                        for(int i = 0; i <cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(Penalty.XML_PENALTY_CODE)){
                                penalty.setPenaltyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Penalty.XML_PENALTY_DESC)){
                                penalty.setPenaltyDesc(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Penalty.XML_UOM)){
                                penalty.setUom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Penalty.XMl_CROP_TYPE)){
                                penalty.setCropType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Penalty.XML_IS_LOADING)){
                                penalty.setIsLoading(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Penalty.XML_IS_ANCAK)){
                                penalty.setIsAncak(cursor.getInt(i));
                            }
                        }

                        listObject.add(penalty);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(DeviceAlias.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        DeviceAlias deviceAlias = new DeviceAlias();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(DeviceAlias.XML_DEVICE_ALIAS)){
                                deviceAlias.setDeviceAlias(cursor.getString(i));
                            }
                        }

                        listObject.add(deviceAlias);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(BlockPlanning.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        BlockPlanning blockPlanning = new BlockPlanning();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BlockPlanning.XML_BLOCK)){
                                blockPlanning.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockPlanning.XML_CREATED_DATE)){
                                blockPlanning.setCreatedDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BlockPlanning.XML_CREATED_BY)){
                                blockPlanning.setCreatedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(blockPlanning);
                    }while(cursor.moveToNext());
                }
            } else if (tableName.equals(AncakPanenHeader.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        AncakPanenHeader ancakPanenHeader = new AncakPanenHeader();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_ANCAK_PANEN_ID)){
                                ancakPanenHeader.setAncakPanenId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_IMEI)){
                                ancakPanenHeader.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_COMPANY_CODE)){
                                ancakPanenHeader.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_ESTATE)){
                                ancakPanenHeader.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_ANCAK_DATE)){
                                ancakPanenHeader.setAncakDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_DIVISION)){
                                ancakPanenHeader.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_GANG)){
                                ancakPanenHeader.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_LOCATION)){
                                ancakPanenHeader.setLocation(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_TPH)){
                                ancakPanenHeader.setTph(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_NIK_HARVESTER)){
                                ancakPanenHeader.setNikHarvester(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_HARVESTER)){
                                ancakPanenHeader.setHarvester(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_NIK_FOREMAN)){
                                ancakPanenHeader.setNikForeman(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_FOREMAN)){
                                ancakPanenHeader.setForeman(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_NIK_CLERK)){
                                ancakPanenHeader.setNikClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_CLERK)){
                                ancakPanenHeader.setClerk(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_CROP)){
                                ancakPanenHeader.setCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_GPS_KOORDINAT)){
                                ancakPanenHeader.setGpsKoordinat(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_STATUS)){
                                ancakPanenHeader.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_CREATED_DATE)){
                                ancakPanenHeader.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_CREATED_BY)){
                                ancakPanenHeader.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_MODIFIED_DATE)){
                                ancakPanenHeader.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenHeader.XML_MODIFIED_BY)){
                                ancakPanenHeader.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(ancakPanenHeader);
                    }while (cursor.moveToNext());
                }
            } else if (tableName.equals(AncakPanenQuality.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        AncakPanenQuality ancakPanenQuality = new AncakPanenQuality();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_ANCAK_PANEN_ID)){
                                ancakPanenQuality.setAncakPanenId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_IMEI)){
                                ancakPanenQuality.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_COMPANY_CODE)){
                                ancakPanenQuality.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_ESTATE)){
                                ancakPanenQuality.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_ANCAK_DATE)){
                                ancakPanenQuality.setAncakDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_DIVISION)){
                                ancakPanenQuality.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_GANG)){
                                ancakPanenQuality.setGang(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_LOCATION)){
                                ancakPanenQuality.setLocation(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_TPH)){
                                ancakPanenQuality.setTph(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_NIK_HARVESTER)){
                                ancakPanenQuality.setNikHarvester(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_CROP)){
                                ancakPanenQuality.setCrop(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_ACHIEVEMENT_CODE)){
                                ancakPanenQuality.setAchievementCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_QUALITY_CODE)){
                                ancakPanenQuality.setQualityCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_QUANTITY)){
                                ancakPanenQuality.setQuantity(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_STATUS)){
                                ancakPanenQuality.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_CREATED_DATE)){
                                ancakPanenQuality.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_CREATED_BY)){
                                ancakPanenQuality.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_MODIFIED_DATE)){
                                ancakPanenQuality.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(AncakPanenQuality.XML_MODIFIED_BY)){
                                ancakPanenQuality.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(ancakPanenQuality);
                    } while (cursor.moveToNext());
                }
            }else if(tableName.equals(SPBSDestination.TABLE_NAME)){
                if (cursor != null && cursor.moveToNext()) {
                    do {
                        SPBSDestination spbsDestination = new SPBSDestination();

                        for(int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(SPBSDestination.XML_MANDT)){
                                spbsDestination.setMandt(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSDestination.XML_ESTATE)){
                                spbsDestination.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSDestination.XML_DEST_TYPE)){
                                spbsDestination.setDestType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSDestination.XML_DEST_ID)){
                                spbsDestination.setDestId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSDestination.XML_DEST_DESC)){
                                spbsDestination.setDestDesc(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSDestination.XML_ACTIVE)){
                                spbsDestination.setActive(cursor.getInt(i));
                            }
                        }

                        listObject.add(spbsDestination);
                    }while (cursor.moveToNext());
                }
            }else if (tableName.equals(SPTARunningNumber.TABLE_NAME)) {
                if (cursor != null && cursor.moveToNext()) {
                    do{
                        SPTARunningNumber sptaRunningNumber = new SPTARunningNumber();

                        for(int i = 0; i <cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_ID)){
                                sptaRunningNumber.setId(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_ESTATE)){
                                sptaRunningNumber.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_DIVISION)){
                                sptaRunningNumber.setDivision(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_YEAR)){
                                sptaRunningNumber.setYear(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_MONTH)){
                                sptaRunningNumber.setMonth(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_IMEI)){
                                sptaRunningNumber.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_RUNNING_NUMBER)){
                                sptaRunningNumber.setRunningNumber(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPBSRunningNumber.XML_DEVICE_ALIAS)){
                                sptaRunningNumber.setDeviceAlias(cursor.getString(i));
                            }
                        }

                        listObject.add(sptaRunningNumber);
                    }while(cursor.moveToNext());
                }
            }else if (tableName.equals(Vendor.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        Vendor vendor = new Vendor();

                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(Vendor.XML_ESTATE)){
                                vendor.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Vendor.XML_LIFNR)){
                                vendor.setLifnr(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(Vendor.XML_NAME)){
                                vendor.setName(cursor.getString(i));
                            }
                        }

                        listObject.add(vendor);
                    }while (cursor.moveToNext());
                }
            }else if(tableName.equals(BLKSUGC.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        BLKSUGC blksugc = new BLKSUGC();

                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_COMPANY_CODE)){
                                blksugc.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_ESTATE)){
                                blksugc.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_BLOCK)){
                                blksugc.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_VALID_FROM)){
                                blksugc.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_VALID_TO)){
                                blksugc.setValidTo(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_PHASE)){
                                blksugc.setPhase(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_DISTANCE)){
                                blksugc.setDistance(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(BLKSUGC.XML_SUB_DIVISION)){
                                blksugc.setSubDivision(cursor.getString(i));
                            }
                        }

                        listObject.add(blksugc);
                    }while (cursor.moveToNext());
                }
            }else if(tableName.equals(GroupHead.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        GroupHead groupHead = new GroupHead();

                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_COMPANY_CODE)){
                                groupHead.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_ESTATE)){
                                groupHead.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_LIFNR)){
                                groupHead.setLifnr(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_INITIAL)){
                                groupHead.setInitial(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_VALID_FROM)){
                                groupHead.setValidFrom(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_VALID_TO)){
                                groupHead.setValidTo(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_NAME)){
                                groupHead.setName(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(GroupHead.XML_STATUS)){
                                groupHead.setStatus(cursor.getInt(i));
                            }
                        }

                        listObject.add(groupHead);
                    }while(cursor.moveToNext());
                }
            }else if(tableName.equals(SPTA.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        SPTA spta = new SPTA();

                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_ZYEAR)){
                                spta.setZyear(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_IMEI)){
                                spta.setImei(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_SPTA_NUM)){
                                spta.setSptaNum(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_COMPANY_CODE)){
                                spta.setCompanyCode(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_ESTATE)){
                                spta.setEstate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_DIVISI)){
                                spta.setDivisi(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_SPTA_DATE)){
                                spta.setSptaDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_SUB_DIV)){
                                spta.setSubDiv(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_PETAK_ID)){
                                spta.setPetakId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_VENDOR_ID)){
                                spta.setVendorId(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_NOPOL)){
                                spta.setNopol(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_LOGO)){
                                spta.setLogo(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_JARAK)){
                                spta.setJarak(cursor.getDouble(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_RUN_ACC_1)){
                                spta.setRunAcc1(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_EMPL_ID_1)){
                                spta.setEmplId1(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_RUN_ACC_2)){
                                spta.setRunAcc2(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_EMPL_ID_2)){
                                spta.setEmplId2(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_CHOPPED_DATE)){
                                spta.setChoppedDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_CHOPPED_HOUR)){
                                spta.setChoppedHour(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_BURN_DATE)){
                                spta.setBurnDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_BURN_HOUR)){
                                spta.setBurnHour(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_LOAD_DATE)){
                                spta.setLoadDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_LOAD_HOUR)){
                                spta.setLoadHour(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_QUALITY)){
                                spta.setQuality(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_INSENTIVE_GULMA)){
                                spta.setInsentiveGulma(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_INSENTIVE_LANGSIR)){
                                spta.setInsentiveLangsir(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_INSENTIVE_ROBOH)){
                                spta.setInsentiveRoboh(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_CANE_TYPE)){
                                spta.setCaneType(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_COST_TEBANG)){
                                spta.setCostTebang(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_COST_MUAT)){
                                spta.setCostMuat(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_COST_ANGKUT)){
                                spta.setCostAngkut(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_PENALTY_TRASH)){
                                spta.setPenaltyTrash(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_GPS_KOORDINAT)){
                                spta.setGpsKoordinat(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_IS_SAVE)){
                                spta.setIsSave(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_STATUS)){
                                spta.setStatus(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_CREATED_DATE)){
                                spta.setCreatedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_CREATED_BY)){
                                spta.setCreatedBy(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_MODIFIED_DATE)){
                                spta.setModifiedDate(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(SPTA.XML_MODIFIED_BY)){
                                spta.setModifiedBy(cursor.getString(i));
                            }
                        }

                        listObject.add(spta);
                    }while (cursor.moveToNext());
                }
            }else if(tableName.equals("RESULT")){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        ResultModel resultModel = new ResultModel();

                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase("BLOCK")){
                                resultModel.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase("Condition")){
                                resultModel.setCondition(cursor.getString(i));
                            }
                        }

                        listObject.add(resultModel);
                    }while (cursor.moveToNext());
                }
            }
            else if(tableName.equals("RESULTDETAIL")){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        ResultDetailModel resultModel = new ResultDetailModel();

                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase("Block")){
                                resultModel.setBlock(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase("Condition")){
                                resultModel.setCondition(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase("SKB")){
                                resultModel.setSKB(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase("BlockRow")){
                                resultModel.setBlockRow(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase("CensusPoint")){
                                resultModel.setCensusPoint(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase("PokokLabel")){
                                resultModel.setPokokLabel(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase("ZDate")){
                                resultModel.setZDate(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase("PokokCondition")){
                                resultModel.setPokokCondition(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase("PokokSide")){
                                resultModel.setPokokSide(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase("Flag")){
                                resultModel.setFlag(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase("PROD_TREES")){
                                resultModel.setPROD_TREES(cursor.getString(i));
                            }
                        }

                        listObject.add(resultModel);
                    }while (cursor.moveToNext());
                }
            }else if(tableName.equals(RKH_ACTIVITY_TYPE.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        RKH_ACTIVITY_TYPE rkh_activity_type = new RKH_ACTIVITY_TYPE();
                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(rkh_activity_type.XML_ACTTYPE)){
                                rkh_activity_type.setACTTYPE(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(RKH_ACTIVITY_TYPE.XML_NAME)){
                                rkh_activity_type.setNAME(cursor.getString(i));
                            }
                        }
                        listObject.add(rkh_activity_type);
                    }while (cursor.moveToNext());
                }
            }else if(tableName.equals(RKH_HEADER.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        RKH_HEADER rkh_header = new RKH_HEADER();

                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_IMEI)){
                                rkh_header.setIMEI(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_COMPANY_CODE)){
                                rkh_header.setCOMPANY_CODE(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_ESTATE)){
                                rkh_header.setESTATE(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_RKH_DATE)){
                                rkh_header.setRKH_DATE(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_DIVISION)){
                                rkh_header.setDIVISION(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_GANG)){
                                rkh_header.setGANG(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_NIK_FOREMAN)){
                                rkh_header.setNIK_FOREMAN(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_FOREMAN)){
                                rkh_header.setFOREMAN(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_NIK_CLERK)){
                                rkh_header.setNIK_CLERK(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_CLERK)){
                                rkh_header.setCLERK(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_GPS_KOORDINAT)){
                                rkh_header.setGPS_KOORDINAT(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_STATUS)){
                                rkh_header.setSTATUS(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_CREATED_DATE)){
                                rkh_header.setCREATED_DATE(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_CREATED_BY)){
                                rkh_header.setCREATED_BY(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_MODIFIED_DATE)){
                                rkh_header.setMODIFIED_DATE(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_MODIFIED_BY)){
                                rkh_header.setMODIFIED_BY(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_header.XML_RKH_ID)){
                                rkh_header.setRKH_ID(cursor.getString(i));
                            }
                        }
                        listObject.add(rkh_header);
                    }while (cursor.moveToNext());
                }
            }else if(tableName.equals(RKH_ITEM.TABLE_NAME)){
                if(cursor != null && cursor.moveToNext()){
                    do{
                        RKH_ITEM rkh_item = new RKH_ITEM();

                        for (int i = 0; i < cursor.getColumnCount(); i++){
                            if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_RKH_ID)){
                                rkh_item.setRKH_ID(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_LINE)){
                                rkh_item.setLINE(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_ACTIVITY)){
                                rkh_item.setACTIVITY(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_BLOCK)){
                                rkh_item.setBLOCK(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_TARGET_OUTPUT)){
                                rkh_item.setTARGET_OUTPUT(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_SKU)){
                                rkh_item.setSKU(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_PHL)){
                                rkh_item.setPHL(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_TARGET_HASIL)){
                                rkh_item.setTARGET_HASIL(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_TARGET_JANJANG)){
                                rkh_item.setTARGET_JANJANG(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_BJR)){
                                rkh_item.setBJR(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_GPS_KOORDINAT)){
                                rkh_item.setGPS_KOORDINAT(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_STATUS)){
                                rkh_item.setSTATUS(cursor.getInt(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_CREATED_DATE)){
                                rkh_item.setCREATED_DATE(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_CREATED_BY)){
                                rkh_item.setCREATED_BY(cursor.getString(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_MODIFIED_DATE)){
                                rkh_item.setMODIFIED_DATE(cursor.getLong(i));
                            }else if(cursor.getColumnName(i).equalsIgnoreCase(rkh_item.XML_MODIFIED_BY)){
                                rkh_item.setMODIFIED_BY(cursor.getString(i));
                            }
                        }
                        listObject.add(rkh_item);
                    }while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            cursor.close();
        }

        return listObject;
    }

    public Object getDataFirst(boolean distinct, String tableName,
                               String[] columns, String whereClause, String[] whereArgs,
                               String groupBy, String having, String orderBy, String limit) {

        List<Object> listObject = getListData(distinct, tableName, columns,
                whereClause, whereArgs, groupBy, having, orderBy, limit);

        if (listObject.size() > 0) {
            return listObject.get(0);
        } else {
            return null;
        }
    }
    public Object getDataFirstRaw(String sqldb_query, String tableName,String []a) {

        List<Object> listObject = getListDataRawQuery(sqldb_query,tableName,a);

        if (listObject.size() > 0) {
            return listObject.get(0);
        } else {
            return null;
        }
    }

    public int updateData(Object object, String whereClause, String[] whereArgs) {
        String tableName = null;
        ContentValues values = new ContentValues();
        if(object.getClass().getName().equals(tblT_BlockConditionScore.class.getName())) {
            tblT_BlockConditionScore md = (tblT_BlockConditionScore) object;
            tableName = tblT_BlockConditionScore.TABLE_NAME;
            values.put(tblT_BlockConditionScore.XML_CompanyCode, md.getAcc_CompanyCode());
            values.put(tblT_BlockConditionScore.XML_ModifiedBy, md.getAcc_ModifiedBy());
            values.put(tblT_BlockConditionScore.XML_ModifiedDateTime, md.getAcc_ModifiedDateTime());
            values.put(tblT_BlockConditionScore.XML_Kacangan, md.getAcc_Kacangan());
            values.put(tblT_BlockConditionScore.XML_Sanitasi, md.getAcc_Sanitasi());
            values.put(tblT_BlockConditionScore.XML_Orcytes, md.getAcc_Orcytes());
            values.put(tblT_BlockConditionScore.XML_Rayap, md.getAcc_Rayap());
            values.put(tblT_BlockConditionScore.XML_Ganoderma, md.getAcc_Ganoderma());
            values.put(tblT_BlockConditionScore.XML_Drainase, md.getAcc_Drainase());
            values.put(tblT_BlockConditionScore.XML_Gawangan, md.getAcc_Gawangan());
            values.put(tblT_BlockConditionScore.XML_TunasPokok, md.getAcc_TunasPokok());
            values.put(tblT_BlockConditionScore.XML_PasarPanen, md.getAcc_PasarPanen());
            values.put(tblT_BlockConditionScore.XML_PasarRintis, md.getAcc_PasarRintis());
            values.put(tblT_BlockConditionScore.XML_Piringan, md.getAcc_Piringan());
            values.put(tblT_BlockConditionScore.XML_TPH, md.getAcc_TPH());
            values.put(tblT_BlockConditionScore.XML_TPHBersih, md.getAcc_TPHBersih());
            values.put(tblT_BlockConditionScore.XML_TPH2, md.getAcc_TPH2());
            values.put(tblT_BlockConditionScore.XML_TPHBersih2, md.getAcc_TPHBersih2());
            values.put(tblT_BlockConditionScore.XML_BW, md.getAcc_BW());
            values.put(tblT_BlockConditionScore.XML_Pencurian, md.getAcc_Pencurian());
            values.put(tblT_BlockConditionScore.XML_Tikus, md.getAcc_Tikus());
            values.put(tblT_BlockConditionScore.XML_TitiRintis, md.getAcc_TitiRintis());
            values.put(tblT_BlockConditionScore.XML_TitiPanen, md.getAcc_TitiPanen());
            values.put(tblT_BlockConditionScore.XML_Parit, md.getAcc_Parit());
            values.put(tblT_BlockConditionScore.XML_Jembatan, md.getAcc_Jembatan());
            values.put(tblT_BlockConditionScore.XML_Jalan, md.getAcc_Jalan());
            values.put(tblT_BlockConditionScore.XML_Block, md.getAcc_Block());
            values.put(tblT_BlockConditionScore.XML_TransLevel, md.getAcc_TransLevel());
            values.put(tblT_BlockConditionScore.XML_Period, md.getAcc_Period());
            values.put(tblT_BlockConditionScore.XML_ZYear, md.getAcc_ZYear());
            values.put(tblT_BlockConditionScore.XML_Estate, md.getAcc_Estate());
            values.put(tblT_BlockConditionScore.XML_Color, md.getAcc_Color());
        }else if(object.getClass().getName().equals(tblT_BlockCondition.class.getName())) {
            tblT_BlockCondition md = (tblT_BlockCondition) object;
            tableName = tblT_BlockCondition.TABLE_NAME;
            values.put(tblT_BlockCondition.XML_CompanyCode, md.getAcc_CompanyCode());
            values.put(tblT_BlockCondition.XML_ModifiedBy, md.getAcc_ModifiedBy());
            values.put(tblT_BlockCondition.XML_ModifiedDateTime, md.getAcc_ModifiedDateTime());
            values.put(tblT_BlockCondition.XML_CreatedBy, md.getAcc_CreatedBy());
            values.put(tblT_BlockCondition.XML_CreatedDateTime, md.getAcc_CreatedDateTime());
            values.put(tblT_BlockCondition.XML_Kacangan, md.getAcc_Kacangan());
            values.put(tblT_BlockCondition.XML_Sanitasi, md.getAcc_Sanitasi());
            values.put(tblT_BlockCondition.XML_Orcytes, md.getAcc_Orcytes());
            values.put(tblT_BlockCondition.XML_Rayap, md.getAcc_Rayap());
            values.put(tblT_BlockCondition.XML_Ganoderma, md.getAcc_Ganoderma());
            values.put(tblT_BlockCondition.XML_Drainase, md.getAcc_Drainase());
            values.put(tblT_BlockCondition.XML_Gawangan, md.getAcc_Gawangan());
            values.put(tblT_BlockCondition.XML_TunasPokok, md.getAcc_TunasPokok());
            values.put(tblT_BlockCondition.XML_PasarPanen, md.getAcc_PasarPanen());
            values.put(tblT_BlockCondition.XML_PasarRintis, md.getAcc_PasarRintis());
            values.put(tblT_BlockCondition.XML_Piringan, md.getAcc_Piringan());
            values.put(tblT_BlockCondition.XML_TPH, md.getAcc_TPH());
            values.put(tblT_BlockCondition.XML_TPHBersih, md.getAcc_TPHBersih());
            values.put(tblT_BlockCondition.XML_TPH2, md.getAcc_TPH2());
            values.put(tblT_BlockCondition.XML_TPHBersih2, md.getAcc_TPHBersih2());
            values.put(tblT_BlockCondition.XML_BW, md.getAcc_BW());
            values.put(tblT_BlockCondition.XML_Pencurian, md.getAcc_Pencurian());
            values.put(tblT_BlockCondition.XML_Tikus, md.getAcc_Tikus());
            values.put(tblT_BlockCondition.XML_TitiRintis, md.getAcc_TitiRintis());
            values.put(tblT_BlockCondition.XML_TitiPanen, md.getAcc_TitiPanen());
            values.put(tblT_BlockCondition.XML_Parit, md.getAcc_Parit());
            values.put(tblT_BlockCondition.XML_Jembatan, md.getAcc_Jembatan());
            values.put(tblT_BlockCondition.XML_Jalan, md.getAcc_Jalan());
            values.put(tblT_BlockCondition.XML_PokokCondition, md.getAcc_PokokCondition());
            values.put(tblT_BlockCondition.XML_PokokSide, md.getAcc_PokokSide());
            values.put(tblT_BlockCondition.XML_PokokLabel, md.getAcc_PokokLabel());
            values.put(tblT_BlockCondition.XML_CensusPoint, md.getAcc_CensusPoint());
            values.put(tblT_BlockCondition.XML_BlockRow, md.getAcc_BlockRow());
            values.put(tblT_BlockCondition.XML_SKB, md.getAcc_SKB());
            values.put(tblT_BlockCondition.XML_Block, md.getAcc_Block());
            values.put(tblT_BlockCondition.XML_TransLevel, md.getAcc_TransLevel());
            values.put(tblT_BlockCondition.XML_ZDate, md.getAcc_ZDate());
            values.put(tblT_BlockCondition.XML_Period, md.getAcc_Period());
            values.put(tblT_BlockCondition.XML_ZYear, md.getAcc_ZYear());
            values.put(tblT_BlockCondition.XML_Estate, md.getAcc_Estate());
            values.put(tblT_BlockCondition.XML_Flag, md.getAcc_Flag());
            values.put(tblT_BlockCondition.XML_DIVISI, md.getAcc_DIVISI());
            values.put(tblT_BlockCondition.XML_Remark, md.getAcc_Remark());

        }else if (object.getClass().getName().equals(tblM_BlockConditionScore.class.getName())) {
            tblM_BlockConditionScore skb = (tblM_BlockConditionScore) object;
            tableName = tblM_BlockConditionScore.TABLE_NAME;

            values.put(tblM_BlockConditionScore.XML_Color, skb.getAcc_Color());
            values.put(tblM_BlockConditionScore.XML_Interval1, skb.getAcc_Interval1());
            values.put(tblM_BlockConditionScore.XML_Interval2, skb.getAcc_Interval2());
            values.put(tblM_BlockConditionScore.XML_Score1, skb.getAcc_Score1());
            values.put(tblM_BlockConditionScore.XML_Score2, skb.getAcc_Score2());
            values.put(tblM_BlockConditionScore.XML_Score3, skb.getAcc_Score3());
            values.put(tblM_BlockConditionScore.XML_ScoreType, skb.getAcc_ScoreType());
            values.put(tblM_BlockConditionScore.XML_ValidFrom, skb.getAcc_ValidFrom());
            values.put(tblM_BlockConditionScore.XML_ValidTo, skb.getAcc_ValidTo());
        }else if (object.getClass().getName().equals(tblM_AccountingPeriod.class.getName())) {
            tblM_AccountingPeriod skb = (tblM_AccountingPeriod) object;
            tableName = tblM_AccountingPeriod.TABLE_NAME;

            values.put(tblM_AccountingPeriod.XML_Estate, skb.getAcc_Estate());
            values.put(tblM_AccountingPeriod.XML_ZYear, skb.getAcc_ZYear());
            values.put(tblM_AccountingPeriod.XML_Period, skb.getAcc_Period());
            values.put(tblM_AccountingPeriod.XML_ClosingDate, skb.getAcc_ClosingDate());
            values.put(tblM_AccountingPeriod.XML_Status, skb.getAcc_Status());
            values.put(tblM_AccountingPeriod.XML_Active, skb.getAcc_Active());
            values.put(tblM_AccountingPeriod.XML_CreatedBy, skb.getAcc_CreatedBy());
            values.put(tblM_AccountingPeriod.XML_CreatedDate, skb.getAcc_CreatedDate());
            values.put(tblM_AccountingPeriod.XML_ModifiedBy, skb.getAcc_ModifiedBy());
            values.put(tblM_AccountingPeriod.XML_ModifiedDate, skb.getAcc_ModifiedDate());
        }else if(object.getClass().getName().equals(MasterDownload.class.getName())){
            MasterDownload md = (MasterDownload) object;
            tableName = MasterDownload.TABLE_NAME;

            values.put(MasterDownload.XML_NAME, md.getName());
            values.put(MasterDownload.XML_FILENAME, md.getFileName());
            values.put(MasterDownload.XML_SYNC_DATE, md.getSyncDate());
            values.put(MasterDownload.XML_STATUS, md.getStatus());
        }else if (object.getClass().getName().equals(BPNHeader.class.getName())) {
            BPNHeader bpnHeader = (BPNHeader) object;
            tableName = BPNHeader.TABLE_NAME;

            values.put(BPNHeader.XML_BPN_ID, bpnHeader.getBpnId());
            values.put(BPNHeader.XML_IMEI, bpnHeader.getImei());
            values.put(BPNHeader.XML_COMPANY_CODE, bpnHeader.getCompanyCode());
            values.put(BPNHeader.XML_ESTATE, bpnHeader.getEstate());
            values.put(BPNHeader.XML_BPN_DATE, bpnHeader.getBpnDate());
            values.put(BPNHeader.XML_DIVISION, bpnHeader.getDivision());
            values.put(BPNHeader.XML_GANG, bpnHeader.getGang());
            values.put(BPNHeader.XML_LOCATION, bpnHeader.getLocation());
            values.put(BPNHeader.XML_TPH, bpnHeader.getTph());
            values.put(BPNHeader.XML_NIK_HARVESTER, bpnHeader.getNikHarvester());
            values.put(BPNHeader.XML_HARVESTER, bpnHeader.getHarvester());
            values.put(BPNHeader.XML_NIK_FOREMAN, bpnHeader.getNikForeman());
            values.put(BPNHeader.XML_FOREMAN, bpnHeader.getForeman());
            values.put(BPNHeader.XML_NIK_CLERK, bpnHeader.getNikClerk());
            values.put(BPNHeader.XML_CLERK, bpnHeader.getClerk());
            values.put(BPNHeader.XML_USE_GERDANG, bpnHeader.isUseGerdang());
            values.put(BPNHeader.XML_CROP, bpnHeader.getCrop());
            values.put(BPNHeader.XML_GPS_KOORDINAT, bpnHeader.getGpsKoordinat());
            values.put(BPNHeader.XML_PHOTO, bpnHeader.getPhoto());
            values.put(BPNHeader.XML_STATUS, bpnHeader.getStatus());
            values.put(BPNHeader.XML_SPBS_NUMBER, bpnHeader.getSpbsNumber());
            values.put(BPNHeader.XML_MODIFIED_DATE, bpnHeader.getModifiedDate());
            values.put(BPNHeader.XML_MODIFIED_BY, bpnHeader.getModifiedBy());
        } else if (object.getClass().getName().equals(BPNQuantity.class.getName())) {
            BPNQuantity bpnQuantity = (BPNQuantity) object;
            tableName = BPNQuantity.TABLE_NAME;

            values.put(BPNQuantity.XML_BPN_ID, bpnQuantity.getBpnId());
            values.put(BPNQuantity.XML_IMEI, bpnQuantity.getImei());
            values.put(BPNQuantity.XML_COMPANY_CODE,bpnQuantity.getCompanyCode());
            values.put(BPNQuantity.XML_ESTATE, bpnQuantity.getEstate());
            values.put(BPNQuantity.XML_BPN_DATE,bpnQuantity.getBpnDate());
            values.put(BPNQuantity.XML_DIVISION, bpnQuantity.getDivision());
            values.put(BPNQuantity.XML_GANG, bpnQuantity.getGang());
            values.put(BPNQuantity.XML_LOCATION, bpnQuantity.getLocation());
            values.put(BPNQuantity.XML_TPH, bpnQuantity.getTph());
            values.put(BPNQuantity.XML_NIK_HARVESTER,bpnQuantity.getNikHarvester());
            values.put(BPNQuantity.XML_CROP,bpnQuantity.getCrop());
            values.put(BPNQuantity.XML_ACHIEVEMENT_CODE,bpnQuantity.getAchievementCode());
            values.put(BPNQuantity.XML_QUANTITY, bpnQuantity.getQuantity());
            values.put(BPNQuantity.XML_QUANTITY_REMAINING, bpnQuantity.getQuantityRemaining());
            values.put(BPNQuantity.XML_STATUS, bpnQuantity.getStatus());
            values.put(BPNQuantity.XML_MODIFIED_DATE,bpnQuantity.getModifiedDate());
            values.put(BPNQuantity.XML_MODIFIED_BY, bpnQuantity.getModifiedBy());
        } else if (object.getClass().getName().equals(BPNQuality.class.getName())) {
            BPNQuality bpnQuality = (BPNQuality) object;
            tableName = BPNQuality.TABLE_NAME;

            values.put(BPNQuality.XML_BPN_ID, bpnQuality.getBpnId());
            values.put(BPNQuality.XML_IMEI, bpnQuality.getImei());
            values.put(BPNQuality.XML_COMPANY_CODE, bpnQuality.getCompanyCode());
            values.put(BPNQuality.XML_ESTATE, bpnQuality.getEstate());
            values.put(BPNQuality.XML_BPN_DATE, bpnQuality.getBpnDate());
            values.put(BPNQuality.XML_DIVISION, bpnQuality.getDivision());
            values.put(BPNQuality.XML_GANG, bpnQuality.getGang());
            values.put(BPNQuality.XML_LOCATION, bpnQuality.getLocation());
            values.put(BPNQuality.XML_TPH, bpnQuality.getTph());
            values.put(BPNQuality.XML_NIK_HARVESTER, bpnQuality.getNikHarvester());
            values.put(BPNQuality.XML_CROP,bpnQuality.getCrop());
            values.put(BPNQuality.XML_ACHIEVEMENT_CODE, bpnQuality.getAchievementCode());
            values.put(BPNQuality.XML_QUALITY_CODE, bpnQuality.getQualityCode());
            values.put(BPNQuality.XML_QUANTITY, bpnQuality.getQuantity());
            values.put(BPNQuality.XML_STATUS, bpnQuality.getStatus());
            values.put(BPNQuality.XML_MODIFIED_DATE,bpnQuality.getModifiedDate());
            values.put(BPNQuality.XML_MODIFIED_BY, bpnQuality.getModifiedBy());
        } else if (object.getClass().getName().equals(BKMHeader.class.getName())) {
            BKMHeader bkmHeader = (BKMHeader) object;
            tableName = BKMHeader.TABLE_NAME;

            values.put(BKMHeader.XML_IMEI, bkmHeader.getImei());
            values.put(BKMHeader.XML_COMPANY_CODE, bkmHeader.getCompanyCode());
            values.put(BKMHeader.XML_ESTATE, bkmHeader.getEstate());
            values.put(BKMHeader.XML_BKM_DATE, bkmHeader.getBkmDate());
            values.put(BKMHeader.XML_DIVISION, bkmHeader.getDivision());
            values.put(BKMHeader.XML_GANG, bkmHeader.getGang());
            values.put(BKMHeader.XML_NIK_FOREMAN, bkmHeader.getNikForeman());
            values.put(BKMHeader.XML_FOREMAN, bkmHeader.getForeman());
            values.put(BKMHeader.XML_NIK_CLERK, bkmHeader.getNikClerk());
            values.put(BKMHeader.XML_CLERK, bkmHeader.getClerk());
            values.put(BKMHeader.XML_STATUS, bkmHeader.getStatus());
            values.put(BKMHeader.XML_MODIFIED_DATE, bkmHeader.getModifiedDate());
            values.put(BKMHeader.XML_MODIFIED_BY, bkmHeader.getModifiedBy());
        } else if (object.getClass().getName().equals(BKMLine.class.getName())) {
            BKMLine bkmLine = (BKMLine) object;
            tableName = BKMLine.TABLE_NAME;

            values.put(BKMLine.XML_IMEI, bkmLine.getImei());
            values.put(BKMLine.XML_COMPANY_CODE, bkmLine.getCompanyCode());
            values.put(BKMLine.XML_ESTATE, bkmLine.getEstate());
            values.put(BKMLine.XML_BKM_DATE, bkmLine.getBkmDate());
            values.put(BKMLine.XML_DIVISION, bkmLine.getDivision());
            values.put(BKMLine.XML_GANG, bkmLine.getGang());
            values.put(BKMLine.XML_NIK, bkmLine.getNik());
            values.put(BKMLine.XML_NAME, bkmLine.getName());
            values.put(BKMLine.XML_ABSENT_TYPE, bkmLine.getAbsentType());
            values.put(BKMLine.XML_MANDAYS, bkmLine.getMandays());
            values.put(BKMLine.XML_UOM, bkmLine.getUom());
            values.put(BKMLine.XML_USE_GERDANG, bkmLine.getUseGerdang());
            values.put(BKMLine.XML_STATUS, bkmLine.getStatus());
            values.put(BKMLine.XML_MODIFIED_DATE, bkmLine.getModifiedDate());
            values.put(BKMLine.XML_MODIFIED_BY, bkmLine.getModifiedBy());
        } else if (object.getClass().getName().equals(BKMOutput.class.getName())) {
            BKMOutput bkmOutput = (BKMOutput) object;
            tableName = BKMOutput.TABLE_NAME;

            values.put(BKMOutput.XML_IMEI, bkmOutput.getImei());
            values.put(BKMOutput.XML_COMPANY_CODE, bkmOutput.getCompanyCode());
            values.put(BKMOutput.XML_ESTATE, bkmOutput.getEstate());
            values.put(BKMOutput.XML_BKM_DATE, bkmOutput.getBkmDate());
            values.put(BKMOutput.XML_DIVISION, bkmOutput.getDivision());
            values.put(BKMOutput.XML_GANG, bkmOutput.getGang());
            values.put(BKMOutput.XML_BLOCK, bkmOutput.getBlock());
            values.put(BKMOutput.XML_NIK, bkmOutput.getNik());
            values.put(BKMOutput.XML_NAME, bkmOutput.getName());
            values.put(BKMOutput.XML_OUTPUT, bkmOutput.getOutput());
            values.put(BKMOutput.XML_UOM, bkmOutput.getUom());
            values.put(BKMOutput.XML_STATUS, bkmOutput.getStatus());
            values.put(BKMOutput.XML_MODIFIED_DATE, bkmOutput.getModifiedDate());
            values.put(BKMOutput.XML_MODIFIED_BY, bkmOutput.getModifiedBy());
        } else if (object.getClass().getName()
                .equals(SPBSHeader.class.getName())) {
            SPBSHeader spbsHeader = (SPBSHeader) object;
            tableName = SPBSHeader.TABLE_NAME;

            values.put(SPBSHeader.XML_IMEI, spbsHeader.getImei());
            values.put(SPBSHeader.XML_YEAR, spbsHeader.getYear());
            values.put(SPBSHeader.XML_COMPANY_CODE, spbsHeader.getCompanyCode());
            values.put(SPBSHeader.XML_ESTATE, spbsHeader.getEstate());
            values.put(SPBSHeader.XML_CROP, spbsHeader.getCrop());
            values.put(SPBSHeader.XML_SPBS_NUMBER, spbsHeader.getSpbsNumber());
            values.put(SPBSHeader.XML_SPBS_DATE, spbsHeader.getSpbsDate());
            values.put(SPBSHeader.XML_DEST_ID, spbsHeader.getDestId());
            values.put(SPBSHeader.XML_DEST_DESC, spbsHeader.getDestDesc());
            values.put(SPBSHeader.XML_DEST_TYPE, spbsHeader.getDestType());
            values.put(SPBSHeader.XML_DIVISION, spbsHeader.getDivision());
            values.put(SPBSHeader.XML_NIK_ASSISTANT,spbsHeader.getNikAssistant());
            values.put(SPBSHeader.XML_ASSISTANT, spbsHeader.getAssistant());
            values.put(SPBSHeader.XML_NIK_CLERK, spbsHeader.getNikClerk());
            values.put(SPBSHeader.XML_CLERK, spbsHeader.getClerk());
            values.put(SPBSHeader.XML_NIK_DRIVER, spbsHeader.getNikDriver());
            values.put(SPBSHeader.XML_DRIVER, spbsHeader.getDriver());
            values.put(SPBSHeader.XML_NIK_KERNET, spbsHeader.getNikKernet());
            values.put(SPBSHeader.XML_KERNET, spbsHeader.getKernet());
            values.put(SPBSHeader.XML_LICENSE_PLATE, spbsHeader.getLicensePlate());
            values.put(SPBSHeader.XML_RUNNING_ACCOUNT, spbsHeader.getRunningAccount());
            values.put(SPBSHeader.XML_GPS_KOORDINAT,spbsHeader.getGpsKoordinat());
            values.put(SPBSHeader.XML_IS_SAVE, spbsHeader.getIsSave());
            values.put(SPBSHeader.XML_STATUS, spbsHeader.getStatus());
//			values.put(SPBSHeader.XML_CREATED_DATE, spbsHeader.getCreatedDate());
//			values.put(SPBSHeader.XML_CREATED_BY, spbsHeader.getCreatedBy());
            values.put(SPBSHeader.XML_MODIFIED_DATE,spbsHeader.getModifiedDate());
            values.put(SPBSHeader.XML_MODIFIED_BY, spbsHeader.getModifiedBy());
            values.put(SPBSHeader.XML_LIFNR, spbsHeader.getLifnr());
        } else if (object.getClass().getName().equals(SPBSLine.class.getName())) {
            SPBSLine spbsLine = (SPBSLine) object;
            tableName = SPBSLine.TABLE_NAME;

            values.put(SPBSLine.XML_IMEI, spbsLine.getImei());
            values.put(SPBSLine.XML_YEAR, spbsLine.getYear());
            values.put(SPBSLine.XML_COMPANY_CODE, spbsLine.getCompanyCode());
            values.put(SPBSLine.XML_ESTATE, spbsLine.getEstate());
            values.put(SPBSLine.XML_CROP, spbsLine.getCrop());
            values.put(SPBSLine.XML_SPBS_NUMBER, spbsLine.getSpbsNumber());
            values.put(SPBSLine.XML_SPBS_DATE, spbsLine.getSpbsDate());
            values.put(SPBSLine.XML_BLOCK, spbsLine.getBlock());
            values.put(SPBSLine.XML_TPH, spbsLine.getTph());
            values.put(SPBSLine.XML_BPN_DATE, spbsLine.getBpnDate());
            values.put(SPBSLine.XML_ACHIEVEMENT_CODE, spbsLine.getAchievementCode());
            values.put(SPBSLine.XML_QUANTITY, spbsLine.getQuantity());
            values.put(SPBSLine.XML_QUANTITY_ANGKUT, spbsLine.getQuantityAngkut());
            values.put(SPBSLine.XML_QUANTITY_REMAINING, spbsLine.getQuantityRemaining());
            values.put(SPBSLine.XML_TOTAL_HARVESTER,spbsLine.getTotalHarvester());
            values.put(SPBSLine.XML_UOM, spbsLine.getUom());
            values.put(SPBSLine.XML_IS_SAVE, spbsLine.getIsSave());
            values.put(SPBSLine.XML_STATUS, spbsLine.getStatus());
            values.put(SPBSLine.XML_BPN_ID, spbsLine.getBpnId());
            values.put(SPBSLine.XML_SPBS_REF, spbsLine.getSpbsRef());
            values.put(SPBSLine.XML_SPBS_NEXT, spbsLine.getSpbsNext());
//			values.put(SPBSLine.XML_CREATED_DATE, spbsLine.getCreatedDate());
//			values.put(SPBSLine.XML_CREATED_BY, spbsLine.getCreatedBy());
            values.put(SPBSLine.XML_MODIFIED_DATE, spbsLine.getModifiedDate());
            values.put(SPBSLine.XML_MODIFIED_BY, spbsLine.getModifiedBy());
        } else if (object.getClass().getName().equals(SPBSRunningNumber.class.getName())) {
            SPBSRunningNumber spbsNumber = (SPBSRunningNumber) object;
            tableName = SPBSRunningNumber.TABLE_NAME;

            values.put(SPBSRunningNumber.XML_ID, spbsNumber.getId());
            values.put(SPBSRunningNumber.XML_ESTATE, spbsNumber.getEstate());
            values.put(SPBSRunningNumber.XML_DIVISION, spbsNumber.getDivision());
            values.put(SPBSRunningNumber.XML_YEAR, spbsNumber.getYear());
            values.put(SPBSRunningNumber.XML_MONTH, spbsNumber.getMonth());
            values.put(SPBSRunningNumber.XML_IMEI, spbsNumber.getImei());
            values.put(SPBSRunningNumber.XML_RUNNING_NUMBER, spbsNumber.getRunningNumber());
            values.put(SPBSRunningNumber.XML_DEVICE_ALIAS, spbsNumber.getDeviceAlias());
        }else if(object.getClass().getName().equals(TaksasiHeader.class.getName())){
            TaksasiHeader taksasiHeader = (TaksasiHeader) object;
            tableName = TaksasiHeader.TABLE_NAME;

            values.put(TaksasiHeader.XML_IMEI, taksasiHeader.getImei());
            values.put(TaksasiHeader.XML_COMPANY_CODE, taksasiHeader.getCompanyCode());
            values.put(TaksasiHeader.XML_ESTATE, taksasiHeader.getEstate());
            values.put(TaksasiHeader.XML_DIVISION, taksasiHeader.getDivision());
            values.put(TaksasiHeader.XML_TAKSASI_DATE, taksasiHeader.getTaksasiDate());
            values.put(TaksasiHeader.XML_BLOCK, taksasiHeader.getBlock());
            values.put(TaksasiHeader.XML_CROP, taksasiHeader.getCrop());
            values.put(TaksasiHeader.XML_NIK_FOREMAN, taksasiHeader.getNikForeman());
            values.put(TaksasiHeader.XML_FOREMAN, taksasiHeader.getForeman());
            values.put(TaksasiHeader.XML_PROD_TREES, taksasiHeader.getProdTrees());
            values.put(TaksasiHeader.XML_BJR, taksasiHeader.getBjr());
            values.put(TaksasiHeader.XML_GPS_KOORDINAT, taksasiHeader.getGpsKoordinat());
            values.put(TaksasiHeader.XML_IS_SAVE, taksasiHeader.getIsSave());
            values.put(TaksasiHeader.XML_STATUS, taksasiHeader.getStatus());
//			values.put(TaksasiHeader.XML_CREATED_DATE, taksasiHeader.getCreatedDate());
//			values.put(TaksasiHeader.XML_CREATED_BY, taksasiHeader.getCreatedBy());
            values.put(TaksasiHeader.XML_MODIFIED_DATE, taksasiHeader.getModifiedDate());
            values.put(TaksasiHeader.XML_MODIFIED_BY, taksasiHeader.getModifiedBy());
        }else if(object.getClass().getName().equals(TaksasiLine.class.getName())){
            TaksasiLine taksasiLine = (TaksasiLine) object;
            tableName = TaksasiLine.TABLE_NAME;

            values.put(TaksasiLine.XML_IMEI, taksasiLine.getImei());
            values.put(TaksasiLine.XML_COMPANY_CODE, taksasiLine.getCompanyCode());
            values.put(TaksasiLine.XML_ESTATE, taksasiLine.getEstate());
            values.put(TaksasiLine.XML_DIVISION, taksasiLine.getDivision());
            values.put(TaksasiLine.XML_TAKSASI_DATE, taksasiLine.getTaksasiDate());
            values.put(TaksasiLine.XML_BLOCK, taksasiLine.getBlock());
            values.put(TaksasiLine.XML_CROP, taksasiLine.getCrop());
            values.put(TaksasiLine.XML_BARIS_SKB, taksasiLine.getBarisSkb());
            values.put(TaksasiLine.XML_BARIS_BLOCK, taksasiLine.getBarisBlock());
            values.put(TaksasiLine.XML_LINE_SKB, taksasiLine.getLineSkb());
            values.put(TaksasiLine.XML_QTY_POKOK, taksasiLine.getQtyPokok());
            values.put(TaksasiLine.XML_QTY_JANJANG, taksasiLine.getQtyJanjang());
            values.put(TaksasiLine.XML_GPS_KOORDINAT, taksasiLine.getGpsKoordinat());
            values.put(TaksasiLine.XML_IS_SAVE, taksasiLine.getIsSave());
            values.put(TaksasiLine.XML_STATUS, taksasiLine.getStatus());
//			values.put(TaksasiLine.XML_CREATED_DATE, taksasiLine.getCreatedDate());
//			values.put(TaksasiLine.XML_CREATED_BY, taksasiLine.getCreatedBy());
            values.put(TaksasiLine.XML_MODIFIED_DATE, taksasiLine.getModifiedDate());
            values.put(TaksasiLine.XML_MODIFIED_BY, taksasiLine.getModifiedBy());
        }else if(object.getClass().getName().equals(AncakPanenHeader.class.getName())){
            AncakPanenHeader ancakPanenHeader = (AncakPanenHeader) object;
            tableName = AncakPanenHeader.TABLE_NAME;

            values.put(AncakPanenHeader.XML_ANCAK_PANEN_ID, ancakPanenHeader.getAncakPanenId());
            values.put(AncakPanenHeader.XML_IMEI, ancakPanenHeader.getImei());
            values.put(AncakPanenHeader.XML_COMPANY_CODE, ancakPanenHeader.getCompanyCode());
            values.put(AncakPanenHeader.XML_ESTATE, ancakPanenHeader.getEstate());
            values.put(AncakPanenHeader.XML_ANCAK_DATE, ancakPanenHeader.getAncakDate());
            values.put(AncakPanenHeader.XML_DIVISION, ancakPanenHeader.getDivision());
            values.put(AncakPanenHeader.XML_GANG, ancakPanenHeader.getGang());
            values.put(AncakPanenHeader.XML_LOCATION, ancakPanenHeader.getLocation());
            values.put(AncakPanenHeader.XML_TPH, ancakPanenHeader.getTph());
            values.put(AncakPanenHeader.XML_NIK_HARVESTER, ancakPanenHeader.getNikHarvester());
            values.put(AncakPanenHeader.XML_HARVESTER, ancakPanenHeader.getHarvester());
            values.put(AncakPanenHeader.XML_NIK_FOREMAN, ancakPanenHeader.getNikForeman());
            values.put(AncakPanenHeader.XML_FOREMAN, ancakPanenHeader.getForeman());
            values.put(AncakPanenHeader.XML_NIK_CLERK, ancakPanenHeader.getNikClerk());
            values.put(AncakPanenHeader.XML_CLERK, ancakPanenHeader.getClerk());
            values.put(AncakPanenHeader.XML_CROP, ancakPanenHeader.getCrop());
            values.put(AncakPanenHeader.XML_GPS_KOORDINAT, ancakPanenHeader.getGpsKoordinat());
            values.put(AncakPanenHeader.XML_STATUS, ancakPanenHeader.getStatus());
            values.put(AncakPanenHeader.XML_MODIFIED_DATE, ancakPanenHeader.getModifiedDate());
            values.put(AncakPanenHeader.XML_MODIFIED_BY, ancakPanenHeader.getModifiedBy());
        }else if(object.getClass().getName().equals(AncakPanenQuality.class.getName())){
            AncakPanenQuality ancakPanenQuality = (AncakPanenQuality) object;
            tableName = AncakPanenQuality.TABLE_NAME;

            values.put(AncakPanenQuality.XML_ANCAK_PANEN_ID, ancakPanenQuality.getAncakPanenId());
            values.put(AncakPanenQuality.XML_IMEI, ancakPanenQuality.getImei());
            values.put(AncakPanenQuality.XML_COMPANY_CODE, ancakPanenQuality.getCompanyCode());
            values.put(AncakPanenQuality.XML_ESTATE, ancakPanenQuality.getEstate());
            values.put(AncakPanenQuality.XML_ANCAK_DATE, ancakPanenQuality.getAncakDate());
            values.put(AncakPanenQuality.XML_DIVISION, ancakPanenQuality.getDivision());
            values.put(AncakPanenQuality.XML_GANG, ancakPanenQuality.getGang());
            values.put(AncakPanenQuality.XML_LOCATION, ancakPanenQuality.getLocation());
            values.put(AncakPanenQuality.XML_TPH, ancakPanenQuality.getTph());
            values.put(AncakPanenQuality.XML_NIK_HARVESTER, ancakPanenQuality.getNikHarvester());
            values.put(AncakPanenQuality.XML_CROP,ancakPanenQuality.getCrop());
            values.put(AncakPanenQuality.XML_ACHIEVEMENT_CODE, ancakPanenQuality.getAchievementCode());
            values.put(AncakPanenQuality.XML_QUALITY_CODE, ancakPanenQuality.getQualityCode());
            values.put(AncakPanenQuality.XML_QUANTITY, ancakPanenQuality.getQuantity());
            values.put(AncakPanenQuality.XML_STATUS, ancakPanenQuality.getStatus());
            values.put(AncakPanenQuality.XML_MODIFIED_DATE, ancakPanenQuality.getModifiedDate());
            values.put(AncakPanenQuality.XML_MODIFIED_BY, ancakPanenQuality.getModifiedBy());
        }else if (object.getClass().getName().equals(SPTARunningNumber.class.getName())) {
            SPTARunningNumber sptaRunningNumber = (SPTARunningNumber) object;
            tableName = SPTARunningNumber.TABLE_NAME;

            values.put(SPTARunningNumber.XML_ID, sptaRunningNumber.getId());
            values.put(SPTARunningNumber.XML_ESTATE, sptaRunningNumber.getEstate());
            values.put(SPTARunningNumber.XML_DIVISION, sptaRunningNumber.getDivision());
            values.put(SPTARunningNumber.XML_YEAR, sptaRunningNumber.getYear());
            values.put(SPTARunningNumber.XML_MONTH, sptaRunningNumber.getMonth());
            values.put(SPTARunningNumber.XML_IMEI, sptaRunningNumber.getImei());
            values.put(SPTARunningNumber.XML_RUNNING_NUMBER, sptaRunningNumber.getRunningNumber());
            values.put(SPTARunningNumber.XML_DEVICE_ALIAS, sptaRunningNumber.getDeviceAlias());
        }else if(object.getClass().getName().equals(SPTA.class.getName())){
            SPTA spta = (SPTA) object;
            tableName = SPTA.TABLE_NAME;

            values.put(SPTA.XML_ZYEAR, spta.getZyear());
            values.put(SPTA.XML_IMEI, spta.getImei());
            values.put(SPTA.XML_SPTA_NUM, spta.getSptaNum());
            values.put(SPTA.XML_COMPANY_CODE, spta.getCompanyCode());
            values.put(SPTA.XML_ESTATE, spta.getEstate());
            values.put(SPTA.XML_DIVISI, spta.getDivisi());
            values.put(SPTA.XML_SPTA_DATE, spta.getSptaDate());
            values.put(SPTA.XML_SUB_DIV, spta.getSubDiv());
            values.put(SPTA.XML_PETAK_ID, spta.getPetakId());
            values.put(SPTA.XML_VENDOR_ID, spta.getVendorId());
            values.put(SPTA.XML_NOPOL, spta.getNopol());
            values.put(SPTA.XML_LOGO, spta.getLogo());
            values.put(SPTA.XML_JARAK, spta.getJarak());
            values.put(SPTA.XML_RUN_ACC_1, spta.getRunAcc1());
            values.put(SPTA.XML_EMPL_ID_1, spta.getEmplId1());
            values.put(SPTA.XML_RUN_ACC_2, spta.getRunAcc2());
            values.put(SPTA.XML_EMPL_ID_2, spta.getEmplId2());
            values.put(SPTA.XML_CHOPPED_DATE, spta.getChoppedDate());
            values.put(SPTA.XML_CHOPPED_HOUR, spta.getChoppedHour());
            values.put(SPTA.XML_BURN_DATE, spta.getBurnDate());
            values.put(SPTA.XML_BURN_HOUR, spta.getBurnHour());
            values.put(SPTA.XML_LOAD_DATE, spta.getLoadDate());
            values.put(SPTA.XML_LOAD_HOUR, spta.getLoadHour());
            values.put(SPTA.XML_QUALITY, spta.getQuality());
            values.put(SPTA.XML_INSENTIVE_GULMA, spta.getInsentiveGulma());
            values.put(SPTA.XML_INSENTIVE_LANGSIR, spta.getInsentiveLangsir());
            values.put(SPTA.XML_INSENTIVE_ROBOH, spta.getInsentiveRoboh());
            values.put(SPTA.XML_CANE_TYPE, spta.getCaneType());
            values.put(SPTA.XML_COST_TEBANG, spta.getCostTebang());
            values.put(SPTA.XML_COST_MUAT, spta.getCostMuat());
            values.put(SPTA.XML_COST_ANGKUT, spta.getCostAngkut());
            values.put(SPTA.XML_PENALTY_TRASH, spta.getPenaltyTrash());
            values.put(SPTA.XML_GPS_KOORDINAT, spta.getGpsKoordinat());
            values.put(SPTA.XML_IS_SAVE, spta.getIsSave());
            values.put(SPTA.XML_STATUS, spta.getStatus());
            values.put(SPTA.XML_MODIFIED_DATE, spta.getModifiedDate());
            values.put(SPTA.XML_MODIFIED_BY, spta.getModifiedBy());
        }else if(object.getClass().getName().equals(RKH_ACTIVITY_TYPE.class.getName())){
            RKH_ACTIVITY_TYPE rkh_activity_type = (RKH_ACTIVITY_TYPE) object;
            tableName = RKH_ACTIVITY_TYPE.TABLE_NAME;
            values.put(RKH_ACTIVITY_TYPE.XML_ACTTYPE, rkh_activity_type.getACTTYPE());
            values.put(RKH_ACTIVITY_TYPE.XML_NAME, rkh_activity_type.getNAME());
        }
        else if(object.getClass().getName().equals(RKH_HEADER.class.getName())){
            RKH_HEADER rkh_header = (RKH_HEADER) object;
            tableName = RKH_HEADER.TABLE_NAME;
            values.put(RKH_HEADER.XML_IMEI, rkh_header.getIMEI());
            values.put(RKH_HEADER.XML_COMPANY_CODE, rkh_header.getCOMPANY_CODE());
            values.put(RKH_HEADER.XML_ESTATE, rkh_header.getESTATE());
            values.put(RKH_HEADER.XML_RKH_DATE, rkh_header.getRKH_DATE());
            values.put(RKH_HEADER.XML_DIVISION, rkh_header.getDIVISION());
            values.put(RKH_HEADER.XML_GANG, rkh_header.getGANG());
            values.put(RKH_HEADER.XML_NIK_FOREMAN, rkh_header.getNIK_FOREMAN());
            values.put(RKH_HEADER.XML_FOREMAN, rkh_header.getFOREMAN());
            values.put(RKH_HEADER.XML_NIK_CLERK, rkh_header.getNIK_CLERK());
            values.put(RKH_HEADER.XML_CLERK, rkh_header.getCLERK());
            values.put(RKH_HEADER.XML_GPS_KOORDINAT, rkh_header.getGPS_KOORDINAT());
            values.put(RKH_HEADER.XML_STATUS, rkh_header.getSTATUS());
            values.put(RKH_HEADER.XML_CREATED_DATE, rkh_header.getCREATED_DATE());
            values.put(RKH_HEADER.XML_CREATED_BY, rkh_header.getCREATED_BY());
            values.put(RKH_HEADER.XML_MODIFIED_DATE, rkh_header.getMODIFIED_DATE());
            values.put(RKH_HEADER.XML_MODIFIED_BY, rkh_header.getMODIFIED_BY());
            values.put(RKH_HEADER.XML_RKH_ID, rkh_header.getRKH_ID());
        }
        else if(object.getClass().getName().equals(RKH_ITEM.class.getName())){
            RKH_ITEM rkh_item = (RKH_ITEM) object;
            tableName = RKH_ITEM.TABLE_NAME;
            values.put(RKH_ITEM.XML_RKH_ID, rkh_item.getRKH_ID());
            values.put(RKH_ITEM.XML_LINE, rkh_item.getLINE());
            values.put(RKH_ITEM.XML_ACTIVITY, rkh_item.getACTIVITY());
            values.put(RKH_ITEM.XML_BLOCK, rkh_item.getBLOCK());
            values.put(RKH_ITEM.XML_TARGET_OUTPUT, rkh_item.getTARGET_OUTPUT());
            values.put(RKH_ITEM.XML_TARGET_HASIL, rkh_item.getTARGET_HASIL());
            values.put(RKH_ITEM.XML_TARGET_JANJANG, rkh_item.getTARGET_JANJANG());
            values.put(RKH_ITEM.XML_SKU, rkh_item.getSKU());
            values.put(RKH_ITEM.XML_PHL, rkh_item.getPHL());
            values.put(RKH_ITEM.XML_BJR, rkh_item.getBJR());
            values.put(RKH_ITEM.XML_GPS_KOORDINAT, rkh_item.getGPS_KOORDINAT());
            values.put(RKH_ITEM.XML_STATUS, rkh_item.getSTATUS());
            values.put(RKH_ITEM.XML_MODIFIED_DATE, rkh_item.getMODIFIED_DATE());
            values.put(RKH_ITEM.XML_MODIFIED_BY, rkh_item.getMODIFIED_BY());
            values.put(RKH_ITEM.XML_CREATED_DATE, rkh_item.getCREATED_DATE());
            values.put(RKH_ITEM.XML_CREATED_BY, rkh_item.getCREATED_BY());
        }

        int rowId = sqliteDatabase.update(tableName, values, whereClause, whereArgs);

        return rowId;
    }

    public int deleteData(String tableName, String whereClause, String[] whereArgs) {

        try {
            int rowId = sqliteDatabase.delete(tableName, whereClause, whereArgs);
            return rowId;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public List<SPBSLineSummary> getListTPH(String imei, String year, String companyCode, String estate, String spbsNumber, String spbsDate, String block, String crop){
        List<SPBSLineSummary> listSpbsLineSummary = new ArrayList<SPBSLineSummary>();

        Cursor cursor = sqliteDatabase.query(false, SPBSLine.TABLE_NAME, null,
                SPBSLine.XML_IMEI + "=?"  + " and " +
                        SPBSLine.XML_YEAR + "=?"+ " and " +
                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                        SPBSLine.XML_ESTATE + "=?" + " and " +
                        SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                        SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                        SPBSLine.XML_BLOCK + "=?" + " and " +
                        SPBSLine.XML_CROP + "=?" + " and " +
                        SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                new String [] {imei, year, companyCode, estate, spbsNumber, spbsDate, block, crop, BPNQuantity.JANJANG_CODE},
                null, null, SPBSLine.XML_TPH, null);

        if (cursor != null && cursor.moveToNext()) {
            int i = 1;
            do{
                int id = cursor.getInt(cursor.getColumnIndex(SPBSLine.XML_ID));
                String tph = cursor.getString(cursor.getColumnIndex(SPBSLine.XML_TPH));
                String tphOriginal = cursor.getString(cursor.getColumnIndex(SPBSLine.XML_TPH));
                String bpnId = cursor.getString(cursor.getColumnIndex(SPBSLine.XML_BPN_ID));
                String bpnDate = cursor.getString(cursor.getColumnIndex(SPBSLine.XML_BPN_DATE));
                String spbsRef = cursor.getString(cursor.getColumnIndex(SPBSLine.XML_SPBS_REF));
                String spbsNext = cursor.getString(cursor.getColumnIndex(SPBSLine.XML_SPBS_NEXT));
                String qtyJanjangCode = cursor.getString(cursor.getColumnIndex(SPBSLine.XML_ACHIEVEMENT_CODE));
                String TPH_TANGKAPAN = "88";


                if(!TextUtils.isEmpty(spbsRef))
                    tph = "L";

                //Fernando, New Load SPBS
                double qtyJanjang = 0;
                double qtyJanjangAngkut = cursor.getDouble(cursor.getColumnIndex(SPBSLine.XML_QUANTITY_ANGKUT));
                BPNQuantity bpnQuantity = (BPNQuantity) getDataFirst(false, BPNQuantity.TABLE_NAME, null,
                        BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                BPNQuantity.XML_COMPANY_CODE  + "=?" + " and " +
                                BPNQuantity.XML_ESTATE  + "=?" + " and " +
                                BPNQuantity.XML_LOCATION  + "=?" + " and " +
                                BPNQuantity.XML_TPH  + "=?" + " and " +
                                BPNQuantity.XML_CROP  + "=?" + " and " +
                                BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                        new String[]{bpnId, companyCode, estate, block, tph, crop, qtyJanjangCode},
                        null, null, null, null);

                String spbsNumberCurrent = spbsNumber.substring(0, 4);

                if(TextUtils.isEmpty(spbsRef)){
                    if(spbsNumberCurrent.equalsIgnoreCase(estate)) {
                        if(bpnQuantity !=null) {
                            double qtyJanjangAngkutCurrent = qtyJanjangAngkut;
                            List<Object> listSPBSLineCurrent = getListData(false, SPBSLine.TABLE_NAME, null,
                                    SPBSLine.XML_IMEI + "=?" + " and " +
                                            SPBSLine.XML_BPN_ID + "=?" + " and " +
                                            SPBSLine.XML_YEAR + "=?" + " and " +
                                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                            SPBSLine.XML_ESTATE + "=?" + " and " +
                                            SPBSLine.XML_CROP + "=?" + " and " +
                                            SPBSLine.XML_TPH + "=?" + " and " +
                                            SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                            SPBSLine.XML_BLOCK + "=?" + " and " +
                                            SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                            SPBSLine.XML_SPBS_NUMBER + "!=?" + " and " +
                                            SPBSLine.XML_SPBS_REF + "=?",
                                    new String[]{imei, bpnId, year, companyCode, estate, crop, tphOriginal, bpnQuantity.getBpnDate(), block, qtyJanjangCode, spbsNumber, ""},
                                    null, null, null, null);
                            for (int k = 0; k < listSPBSLineCurrent.size(); k++) {
                                SPBSLine spbsLineCurrent = (SPBSLine) listSPBSLineCurrent.get(k);
                                qtyJanjangAngkutCurrent = qtyJanjangAngkutCurrent + spbsLineCurrent.getQuantityAngkut();
                            }

                            double penalty = getPenalty(bpnId, companyCode, estate, bpnDate, bpnQuantity.getDivision(), bpnQuantity.getGang(), block, BPNQuantity.JANJANG_CODE);
                            qtyJanjang = ((bpnQuantity.getQuantity() - penalty) - qtyJanjangAngkutCurrent) + qtyJanjangAngkut;
                        }
                    }else{
                        if(bpnQuantity !=null) {
                            qtyJanjang = bpnQuantity.getQuantityRemaining();
                        }
                    }
                }
                else {
                    String number = spbsRef.split("_")[0];
                    SPBSLine spbslineLangsir = (SPBSLine) getDataFirst(false, SPBSLine.TABLE_NAME, null,
                            SPBSLine.XML_IMEI + "=?" + " and " +
                                    SPBSLine.XML_BPN_ID + "=?" + " and " +
                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                    SPBSLine.XML_CROP + "=?" + " and " +
                                    SPBSLine.XML_TPH + "=?" + " and " +
                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                    SPBSLine.XML_SPBS_NUMBER + "=?",
                            new String [] {imei, bpnId, year, companyCode, estate, crop, tphOriginal, block, qtyJanjangCode, number},
                            null, null, null, null);

                    double qtyLangsirRemaining = 0;
                    if(spbslineLangsir !=null){
                        if(!TextUtils.isEmpty(spbslineLangsir.getSpbsNext()) ||  !spbsNumberCurrent.equalsIgnoreCase(estate))
                            qtyLangsirRemaining = (double) (spbslineLangsir.getQuantityRemaining());
                    }

                    if(spbsNumberCurrent.equalsIgnoreCase(estate)){
                        qtyJanjang  = (double)(qtyLangsirRemaining + qtyJanjangAngkut);}
                    else{
                        qtyJanjang = qtyLangsirRemaining;
                    }
                }

//				String qtyLooseFruitCode = "02";
                double qtyLooseFruit = 0;
                double qtyLooseFruitAngkut = 0;
                String gpsKoordinat = cursor.getString(cursor.getColumnIndex(SPBSLine.XML_GPS_KOORDINAT));
                int isSave = cursor.getInt(cursor.getColumnIndex(SPBSLine.XML_IS_SAVE));


                SPBSLine spbsLineLooseFruit = (SPBSLine) getDataFirst(false, SPBSLine.TABLE_NAME, null,
                        SPBSLine.XML_ID + "=?" + " and " +
                                SPBSLine.XML_IMEI + "=?"  + " and " +
                                SPBSLine.XML_YEAR + "=?"+ " and " +
                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                SPBSLine.XML_CROP + "=?" + " and " +
                                SPBSLine.XML_TPH + "=?" + " and " +
                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                SPBSLine.XML_ACHIEVEMENT_CODE + " =?",
                        new String [] {cursor.getString(cursor.getColumnIndex(SPBSLine.XML_ID)),
                                cursor.getString(cursor.getColumnIndex(SPBSLine.XML_IMEI)),
                                cursor.getString(cursor.getColumnIndex(SPBSLine.XML_YEAR)),
                                cursor.getString(cursor.getColumnIndex(SPBSLine.XML_COMPANY_CODE)),
                                cursor.getString(cursor.getColumnIndex(SPBSLine.XML_ESTATE)),
                                cursor.getString(cursor.getColumnIndex(SPBSLine.XML_SPBS_NUMBER)),
                                cursor.getString(cursor.getColumnIndex(SPBSLine.XML_SPBS_DATE)),
                                cursor.getString(cursor.getColumnIndex(SPBSLine.XML_BLOCK)),
                                cursor.getString(cursor.getColumnIndex(SPBSLine.XML_CROP)),
                                cursor.getString(cursor.getColumnIndex(SPBSLine.XML_TPH)),
                                cursor.getString(cursor.getColumnIndex(SPBSLine.XML_BPN_DATE)),
                                BPNQuantity.LOOSE_FRUIT_CODE},
                        null, null, null, null);

                if(spbsLineLooseFruit != null){
                    qtyLooseFruit = spbsLineLooseFruit.getQuantity();
                    qtyLooseFruitAngkut = spbsLineLooseFruit.getQuantityAngkut();

                    if(qtyLooseFruit != qtyLooseFruitAngkut){

                        List<Object> listspbslineLooseFruitExist = getListData(false, SPBSLine.TABLE_NAME, null,
                                SPBSLine.XML_YEAR + "=?" + " and " +
                                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                        SPBSLine.XML_ESTATE + "=?" + " and " +
                                        SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                        SPBSLine.XML_BPN_ID + "=?" + " and " +
                                        SPBSLine.XML_CROP + "=?" + " and " +
                                        SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                new String[]{year, companyCode, estate, spbsDate, bpnId, crop, BPNQuantity.LOOSE_FRUIT_CODE},
                                null, null, null, null);

                        double qtyLooseFruitExist = 0;
                        for (int k = 0; k < listspbslineLooseFruitExist.size(); k++) {
                            SPBSLine spbsLineLooseFruitExist = (SPBSLine) listspbslineLooseFruitExist.get(k);
                            qtyLooseFruitExist = qtyLooseFruitExist + spbsLineLooseFruitExist.getQuantityAngkut();
                        }

                        qtyLooseFruit = qtyLooseFruit - qtyLooseFruitExist;

                    }
                }

                SPBSLineSummary spbsLineSummary = new SPBSLineSummary(id, tph, bpnId, bpnDate, spbsRef, spbsNext, qtyJanjang, qtyJanjangAngkut,
                        qtyLooseFruit, qtyLooseFruitAngkut, gpsKoordinat, isSave, tphOriginal);

                listSpbsLineSummary.add(spbsLineSummary);
                i++;
            }while(cursor.moveToNext());
        }

        cursor.close();
        return listSpbsLineSummary;
    }

    private double getPenalty(String bpnId, String companyCode, String estate, String bpnDate, String division, String gang, String block, String achievementCode){
        double qty = 0;

        List<Object> lstPenalty = getListData(false, Penalty.TABLE_NAME, null,
                Penalty.XML_IS_LOADING + "=?",
                new String [] {"0"},
                null, null, null, null);

        if(lstPenalty != null && lstPenalty.size() > 0){
            for(Object obj : lstPenalty){
                Penalty penalty = (Penalty) obj;

                if(penalty != null){

                    BPNQuality bpnQuality =  (BPNQuality) getDataFirst(false, BPNQuality.TABLE_NAME, null,
                            BPNQuality.XML_BPN_ID + "=?" + " and " +
                                    BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
                                    BPNQuality.XML_ESTATE + "=?" + " and " +
                                    BPNQuality.XML_BPN_DATE + "=?" + " and " +
                                    BPNQuality.XML_DIVISION + "=?" + " and " +
                                    BPNQuality.XML_GANG + "=?" + " and " +
                                    BPNQuality.XML_LOCATION + "=?" + " and " +
                                    BPNQuality.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                    BPNQuality.XML_QUALITY_CODE + "=?",
                            new String [] {bpnId, companyCode, estate, bpnDate, division, gang, block, achievementCode, penalty.getPenaltyCode()},
                            null, null, null, null);

                    if(bpnQuality != null)
                        qty = qty + bpnQuality.getQuantity();
                }
            }
        }

        return qty;
    }

    public int updateDataSQL(String Table, String sql, Object[] bindArgs) {

        int result = 0;
        try{
            String query = sql;
           sqliteDatabase.execSQL(query,bindArgs);
           result = 1;
        }
        catch(Exception e)
        {
            result = 0;
            e.printStackTrace();
        }

        return result;
    }

    public int ResetData(String sql) {

        int result = 0;
        try{
            String query = sql;
            sqliteDatabase.execSQL(query);
            result = 1;
        }
        catch(Exception e)
        {
            result = 0;
            e.printStackTrace();
        }

        return result;
    }

    public int RawQuery(String sql) {

        int result = 0;
        try{
            String query = sql;
            sqliteDatabase.execSQL(query);
            result = 1;
        }
        catch(Exception e)
        {
            result = 0;
            e.printStackTrace();
        }

        return result;
    }
    public int insertDataSQL(String tableName,ContentValues values) {

        int result = 0;
        try{
            /*sqliteDatabase.rawQuery(query,bindArgs);*/
            sqliteDatabase.insertOrThrow(tableName, null, values);
            result = 1;
        }
        catch(Exception e)
        {
            result = 0;
            e.printStackTrace();
        }

        return result;
    }

    public String[] SelectSKB(String block) {
        try {
            String arrData[] = null;
            String[] a = new String[4];
            a[0] = SESS_ZYEAR;
            a[1] = SESS_PERIOD;
            a[2] = block;
            a[3] = "2";
            String query = "SELECT DISTINCT  SKB  FROM  tblT_BlockCondition " +
                    "WHERE ZYear=? " +
                    "AND Period=? " +
                    "AND Block = ? " +
                    "AND TransLevel = ?";
            Cursor cursor = sqliteDatabase.rawQuery(query,a);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    arrData = new String[cursor.getCount()];
                    int i = 0;
                    do {
                        arrData[i] = cursor.getString(0);
                        i++;
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
            return arrData;
        } catch (Exception e) {
            return null;
        }
    }

    public int getTaskCount(String QueryString, String[] content) {
        String Query = QueryString;
        Cursor cursor =sqliteDatabase.rawQuery(Query,
                content);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }


}
