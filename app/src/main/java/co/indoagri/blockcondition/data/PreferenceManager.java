package co.indoagri.blockcondition.data;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PreferenceManager {

    protected Context mContext;

    // TODO: make these members private.
    protected SharedPreferences mSettings;
    protected Editor mEditor;

    public final static String SHARED_COUNTRY_CODE = "COUNTRY_CODE";
    public final static String SHARED_QR = "QR";
    public final static String POKOKL = "POKOKL";
    public final static String POKOKR = "POKOKR";
    public final static String BARISR = "BARISR";
    public final static String BARISL = "BARISL";
    public final static String VALUEPERIOD = "VALUEPERIOD";
    public final static String VALUEYEAR = "VALUEYEAR";
    public final static String VALUEDATE = "VALUEDATE";
    public final static String SEARCHSUMMARYBLOCK = "SEARCHSUMMARYBLOCK";
    public final static String SEARCHSUMMARYPERIODE = "SEARCHSUMMARYPERIOD";
    public final static String SEARCHSUMMARYYEAR = "SEARCHSUMMARYYEAR";
    public final static String SEARCHSUMMARYPHASE= "SEARCHSUMMARYPHASE";
    public final static String KONDISIPOKOKR = "KONDISIPOKOKR";
    public final static String KONDISIPOKOKL = "KONDISIPOKOKL";

    // RKH //
    public final static String RKH_INPUT_PEKERJA_1 = "RKH_INPUTPEKERJA_1";
    public final static String RKH_INPUT_PEKERJA_2 = "RKH_INPUTPEKERJA_2";
    public final static String RKH_INPUTGANG = "RKH_INPUTGANG";
    public final static String RKH_INPUTTYPE = "RKH_INPUTTYPE";
    public final static String RKH_INPUTBLOCK = "RKH_INPUTBLOK";
    public final static String RKH_INPUTRKHDATE = "RKH_INPUTRKHDATE";
    public PreferenceManager(Context ctx, String prefFileName) {
        mContext = ctx;
        mSettings = mContext.getSharedPreferences(prefFileName,
                Context.MODE_PRIVATE);
        mEditor = mSettings.edit();
    }

    public void setCountryCode(String countryCode){
        mEditor = mSettings.edit();
        mEditor.putString(SHARED_COUNTRY_CODE, countryCode);
        mEditor.commit();
    }

    public String getCountryCode(){
        return mSettings.getString(SHARED_COUNTRY_CODE, "");
    }
    public void setPokokL(int pokokL){
        mEditor = mSettings.edit();
        mEditor.putInt(POKOKL, pokokL);
        mEditor.commit();
    }

    public int getPokokL(){
        return mSettings.getInt(POKOKL, 0);
    }
    public void setBarisR(String barisR){
        mEditor = mSettings.edit();
        mEditor.putString(BARISR, barisR);
        mEditor.commit();
    }

    public String getBarisR(){
        return mSettings.getString(BARISR, "0");
    }
    public void setBarisL(String barisl){
        mEditor = mSettings.edit();
        mEditor.putString(BARISL, barisl);
        mEditor.commit();
    }
    public String getKondisipokokL(){
        return mSettings.getString(KONDISIPOKOKL, "");
    }
    public void setKondisipokokL(String kondisipokokL){
        mEditor = mSettings.edit();
        mEditor.putString(KONDISIPOKOKL, kondisipokokL);
        mEditor.commit();
    }
    public String getKondisipokokR(){
        return mSettings.getString(KONDISIPOKOKR, "");
    }
    public void setKondisipokokR(String kondisipokokR){
        mEditor = mSettings.edit();
        mEditor.putString(KONDISIPOKOKR, kondisipokokR);
        mEditor.commit();
    }

    public String getBarisL(){
        return mSettings.getString(BARISL, "0");
    }
    public String getPeriod(){
        return mSettings.getString(VALUEPERIOD, "0");
    }
    public void setPeriod(String barisl){
        mEditor = mSettings.edit();
        mEditor.putString(VALUEPERIOD, barisl);
        mEditor.commit();
    }

    public String getYear(){
        return mSettings.getString(VALUEYEAR, "0");
    }
    public void setYear(String barisl){
        mEditor = mSettings.edit();
        mEditor.putString(VALUEYEAR, barisl);
        mEditor.commit();
    }
    public String getDatein()
    {
        return mSettings.getString(VALUEDATE, "0");

    }

    public void setDatein(String datein){
        mEditor = mSettings.edit();
        mEditor.putString(VALUEDATE, datein);
        mEditor.commit();
    }


    public void setPokokR(int pokokR){
        mEditor = mSettings.edit();
        mEditor.putInt(POKOKR, pokokR);
        mEditor.commit();
    }

    public int getPokokR(){
        return mSettings.getInt(POKOKR, 0);
    }

    public void setQR(String qrType){
        mEditor = mSettings.edit();
        mEditor.putString(SHARED_QR, qrType);
        mEditor.commit();
    }

    public String getQR(){
        return mSettings.getString(SHARED_QR,"");
    }

    /***
     * Set a value for the key
     ****/
    // Set Value String Type
    public void setValue(String key, String value) {
        mEditor.putString(key, value);
        mEditor.commit();
    }

    /***
     * Set a value for the key
     ****/
    // Set Value Integer Type
    public void setValue(String key, int value) {
        mEditor.putInt(key, value);
        mEditor.commit();
    }

    /***
     * Set a value for the key
     ****/

   /* public void setValue(String key, double value) {
        setValue(key, Double.toString(value));
    }*/
    // Set Value Double Type
    public void setValue(String key, double value){
        mEditor.putLong(key,Double.doubleToLongBits(value));
        mEditor.commit();
    }

    /***
     * Set a value for the key
     ****/
    // Set Value Long Type
    public void setValue(String key, long value) {
        mEditor.putLong(key, value);
        mEditor.commit();
    }

    /****
     * Gets the value from the settings stored natively on the device.
     *
     * @param defaultValue Default value for the key, if one is not found.
     **/

    // Get Value String
    public String getValue(String key, String defaultValue) {
        return mSettings.getString(key, defaultValue);
    }

    // Get Value Integer
    public int getIntValue(String key, int defaultValue) {
        return mSettings.getInt(key, defaultValue);
    }

    // Get Value Long
    public long getLongValue(String key, long defaultValue) {
        return mSettings.getLong(key, defaultValue);
    }

    // Get Value Double
    public double getDoubleValue(String key, double defaultValue) {
        return Double.longBitsToDouble(mSettings.getLong(key,Double.doubleToLongBits(defaultValue)));
    }


    /****
     * Gets the value from the preferences stored natively on the device.
     *
     * @param defValue Default value for the key, if one is not found.
     **/
    // Get Value Boolean True or False
    public boolean getValue(String key, boolean defValue) {
        return mSettings.getBoolean(key, defValue);
    }

    // Set Value Boolean Type
    public void setValue(String key, boolean value) {
        mEditor.putBoolean(key, value);
        mEditor.commit();
    }

    /**
     * Clear all the preferences store in this {@link Editor}
     */
    // Clear All preference Store
    public boolean clear() {
        try {
            mEditor.clear().commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Removes preference entry for the given key.
     *
     * @param key
     */

    // Remove key in preference  MAnager
    public void removeValue(String key) {
        if (mEditor != null) {
            mEditor.remove(key).commit();
        }
    }


    public String getSSYear(){
        return mSettings.getString(SEARCHSUMMARYYEAR, null);
    }
    public void setSSYear(String fieldSS){
        mEditor = mSettings.edit();
        mEditor.putString(SEARCHSUMMARYYEAR, fieldSS);
        mEditor.commit();
    }
    public String getSSPeriode(){
        return mSettings.getString(SEARCHSUMMARYPERIODE, null);
    }
    public void setSSPeriode(String fieldSS){
        mEditor = mSettings.edit();
        mEditor.putString(SEARCHSUMMARYPERIODE, fieldSS);
        mEditor.commit();
    }
    public String getSSBlock(){
        return mSettings.getString(SEARCHSUMMARYBLOCK, null);
    }
    public void setSSBlock(String fieldSS){
        mEditor = mSettings.edit();
        mEditor.putString(SEARCHSUMMARYBLOCK, fieldSS);
        mEditor.commit();
    }
    public String getSSPhase(){
        return mSettings.getString(SEARCHSUMMARYPHASE, null);
    }
    public void setSSPhase(String fieldSS){
        mEditor = mSettings.edit();
        mEditor.putString(SEARCHSUMMARYPHASE, fieldSS);
        mEditor.commit();
    }

    public String getRkhInputPekerja1(){
        return mSettings.getString(RKH_INPUT_PEKERJA_1, null);
    }
    public void setRkhInputPekerja1(String fields){
        mEditor = mSettings.edit();
        mEditor.putString(RKH_INPUT_PEKERJA_1, fields);
        mEditor.commit();
    }

    public String getRkhInputPekerja2(){
        return mSettings.getString(RKH_INPUT_PEKERJA_2, null);
    }
    public void setRkhInputPekerja2(String fieldss){
        mEditor = mSettings.edit();
        mEditor.putString(RKH_INPUT_PEKERJA_2, fieldss);
        mEditor.commit();
    }

    public String getRkhInputgang(){
        return mSettings.getString(RKH_INPUTGANG, null);
    }
    public void setRkhInputgang(String fieldss){
        mEditor = mSettings.edit();
        mEditor.putString(RKH_INPUTGANG, fieldss);
        mEditor.commit();
    }

    public String getRkhInputtype(){
        return mSettings.getString(RKH_INPUTTYPE, null);
    }
    public void setRkhInputtype(String fieldss){
        mEditor = mSettings.edit();
        mEditor.putString(RKH_INPUTTYPE, fieldss);
        mEditor.commit();
    }
    public String getRkhInputblock(){
        return mSettings.getString(RKH_INPUTBLOCK, null);
    }
    public void setRkhInputblock(String fieldss){
        mEditor = mSettings.edit();
        mEditor.putString(RKH_INPUTBLOCK, fieldss);
        mEditor.commit();
    }

    public String getRkhInputrkhdate(){
        return mSettings.getString(RKH_INPUTRKHDATE, null);
    }
    public void setRkhInputrkhdate(String fieldss){
        mEditor = mSettings.edit();
        mEditor.putString(RKH_INPUTRKHDATE, fieldss);
        mEditor.commit();
    }

    //

    /*public void addFavorite(String LISTNAME,String prefname,Context context, ProductModel product) {
        List<ProductModel> saved = getLIST(LISTNAME,prefname,context);
        if (saved == null)
            saved = new ArrayList<ProductModel>();
        saved.add(product);
        saveLIST(LISTNAME,prefname,context, saved);
    }
    public void removeFavorite(String LISTNAME,String prefname,Context context, ProductModel product) {
        ArrayList<ProductModel> favorites = getLIST(LISTNAME,prefname,context);
        if (favorites != null) {
            favorites.remove(product);
            saveLIST(LISTNAME,prefname,context, favorites);
        }
    }*/

  /*  public void saveLIST(String LISTNAME,String prefname,Context context, List<ProductModel> objects) {
        SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(prefname,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(objects);

        editor.putString(LISTNAME, jsonFavorites);

        editor.commit();
    }

    public ArrayList<ProductModel> getLIST(String LISTNAME, String prefname, Context context) {
        SharedPreferences settings;
        List<ProductModel> objects;

        settings = context.getSharedPreferences(prefname,
                Context.MODE_PRIVATE);

        if (settings.contains(LISTNAME)) {
            String jsonFavorites = settings.getString(LISTNAME, null);
            Gson gson = new Gson();
            ProductModel[] favoriteItems = gson.fromJson(jsonFavorites,
                    ProductModel[].class);
            objects = Arrays.asList(favoriteItems);
            objects = new ArrayList<ProductModel>(objects);
        } else
            return null;

        return (ArrayList<ProductModel>) objects;
    }

    public void removePrefName(String LISTNAME,String prefname,Context context, ProductModel product) {
        ArrayList<ProductModel> objects = getLIST(LISTNAME,prefname,context);
        if (objects != null) {
            objects.remove(product);
            saveLIST(LISTNAME,prefname,context,objects);
        }
    }*/

}
