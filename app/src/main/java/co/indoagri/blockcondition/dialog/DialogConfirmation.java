package co.indoagri.blockcondition.dialog;


import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.listener.DialogConfirmationListener;

public class DialogConfirmation extends Dialog implements android.view.View.OnClickListener {
    private TextView txt_confirm_title;
    private TextView txt_confirm_message;
    private Button btn_confirm_cancel;
    private Button btn_confirm_ok;

    private Activity activity;
    private String title;
    private String message;
    private int id;
    private Object object;
    private DialogConfirmationListener callback;

    public DialogConfirmation(Activity activity, String title, String message, Object object, int id){
        super(activity);
        this.activity = activity;
        this.title = title;
        this.message = message;
        this.id = id;
        this.object = object;
        this.callback = (DialogConfirmationListener) activity;
    }

    public DialogConfirmation(Activity activity, String title, String message, Object object, DialogConfirmationListener callback){
        super(activity);

        this.activity = activity;
        this.title = title;
        this.message = message;
        this.object = object;
        this.callback = callback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.dialog_confirmation);

        txt_confirm_title = (TextView)findViewById(R.id.txt_confirm_title);
        txt_confirm_message = (TextView)findViewById(R.id.txt_confirm_message);
        btn_confirm_cancel = (Button)findViewById(R.id.btn_confirm_cancel);
        btn_confirm_ok = (Button)findViewById(R.id.btn_confirm_ok);

        txt_confirm_title.setText(title);
        txt_confirm_message.setText(message);

        btn_confirm_cancel.setOnClickListener(this);
        btn_confirm_ok.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_confirm_cancel:
                dismiss();
                callback.onConfirmCancel(object, id);
                break;
            case R.id.btn_confirm_ok:
                dismiss();
                callback.onConfirmOK(object, id);
                break;
            default:
                break;
        }
    }

    @Override
    public void cancel() {
        dismiss();
        super.cancel();
    }
}