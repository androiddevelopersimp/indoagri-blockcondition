package co.indoagri.blockcondition.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.listener.DialogTimeListener;
import co.indoagri.blockcondition.widget.TimePickerCustom;

public class DialogTime extends Dialog implements View.OnClickListener, TimePickerCustom.TimeWatcher {
    private TextView txt_time_title;
    private TimePickerCustom dtp_time_waktu;
    private Button btn_time_cancel;
    private Button btn_time_ok;
    private Activity activity;
    private String title;
    private int hour;
    private int minute;
    private int second;
    private int id;

    private DialogTimeListener callback;


    public DialogTime(Activity activity, String title, int hour, int minute, int second, int id){
        super(activity);
        this.activity = activity;
		this.title = title;
		this.hour = hour;
		this.minute = minute;
		this.second = second;
		this.id = id;
	}


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.dialog_time);

        txt_time_title = (TextView) findViewById(R.id.txt_time_title);
        dtp_time_waktu = (TimePickerCustom) findViewById(R.id.dtp_time_waktu);
        btn_time_cancel = (Button) findViewById(R.id.btn_time_cancel);
        btn_time_ok = (Button) findViewById(R.id.btn_time_ok);

        txt_time_title.setText(title);

        dtp_time_waktu.setTimeChangedListener(this);
        btn_time_cancel.setOnClickListener(this);
        btn_time_ok.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_time_cancel:
                dismiss();
                break;
            case R.id.btn_time_ok:
                try{
                    callback = (DialogTimeListener)activity;
//                    callback.onTimeOK(hour, minute, second, id);
                }catch(ClassCastException e){
                    e.printStackTrace();
                    throw e;
                }

                if(callback != null){
                    callback.onTimeOK(hour, minute, second, id);
                    dismiss();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void cancel() {
        dismiss();
        super.cancel();
    }
    @Override
    public void onTimeChanged(Calendar c) {
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        second = c.get(Calendar.SECOND);
    }
}