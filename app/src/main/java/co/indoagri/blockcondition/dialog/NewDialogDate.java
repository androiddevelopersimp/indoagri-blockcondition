package co.indoagri.blockcondition.dialog;


import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.listener.DialogDateListener;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.widget.NewDatePickerCustom;

public class NewDialogDate extends Dialog implements View.OnClickListener, NewDatePickerCustom.DateWatcher {
    private TextView txt_date_title;
    private NewDatePickerCustom dtp_date_tanggal;
    private Button btn_date_cancel;
    private Button btn_date_ok;

    private Activity activity;
    private String title;
    private Date date;
    private int id;
    private DialogDateListener callback;

    public NewDialogDate(Activity activity, String title, Date date, int id){
        super(activity);
        this.activity = activity;
        this.title = title;
        this.date = date;
        this.id = id;
        this.callback = (DialogDateListener) activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     /*   requestWindowFeature(Window.FEATURE_NO_TITLE);*/
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.new_dialog_date);

        txt_date_title = (TextView) findViewById(R.id.txt_date_title);
        dtp_date_tanggal = (NewDatePickerCustom) findViewById(R.id.dtp_date_tanggal);
        btn_date_cancel = (Button) findViewById(R.id.btn_date_cancel);
        btn_date_ok = (Button) findViewById(R.id.btn_date_ok);

        txt_date_title.setText(title);

        dtp_date_tanggal.setDateChangedListener(this);
        btn_date_cancel.setOnClickListener(this);
        btn_date_ok.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_date_cancel:
                dismiss();
                break;
            case R.id.btn_date_ok:
                dismiss();
                callback.onDateOK(date, id);
                break;
            default:
                break;
        }
    }

    @Override
    public void cancel() {
        dismiss();
        super.cancel();
    }

    //	@SuppressLint("SimpleDateFormat")
    @Override
    public void onDateChanged(Calendar c) {
        String tanggal;
        DateLocal date_local;

//		tanggal = String.valueOf(c.get(Calendar.DAY_OF_MONTH)) + "-" +
//				 String.valueOf(c.get(Calendar.MONTH) + 1) + "-" +
//				 String.valueOf(c.get(Calendar.YEAR));
        tanggal = String.valueOf(c.get(Calendar.YEAR)) + "-" + String.valueOf(c.get(Calendar.MONTH) + 1) + "-" + String.valueOf(c.get(Calendar.DAY_OF_MONTH));

        date_local = new DateLocal(tanggal, DateLocal.FORMAT_INPUT);

        Log.d("tag", tanggal);

        date = date_local.getDate();
    }
}