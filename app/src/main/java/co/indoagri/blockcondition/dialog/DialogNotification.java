package co.indoagri.blockcondition.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.listener.DialogNotificationListener;

public class DialogNotification extends Dialog implements android.view.View.OnClickListener {

    private TextView txt_notification_title;
    private TextView txt_notification_message;
    private Button btn_notification_ok;
    private Activity activity;
    private String title;
    private String message;
    private boolean is_finish;
    private DialogNotificationListener callback;

    public DialogNotification(Activity activity){
        super(activity);
    }

    public DialogNotification(Activity activity, String title, String message, boolean is_finish){
        super(activity);
        this.activity = activity;
        this.title = title;
        this.message = message;
        this.is_finish = is_finish;

        this.callback = (DialogNotificationListener) activity;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setMessage(String message){
        this.message = message;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.dialog_notification);

        txt_notification_title = (TextView)findViewById(R.id.txt_notification_title);
        txt_notification_message = (TextView)findViewById(R.id.txt_notification_message);
        btn_notification_ok = (Button)findViewById(R.id.btn_notification_ok);

        txt_notification_title.setText(this.title);
        txt_notification_message.setText(this.message);
        btn_notification_ok.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_notification_ok:
                dismiss();
                callback.onOK(is_finish);
                break;

            default:
                break;
        }
    }

    @Override
    public void cancel() {
        dismiss();
        super.cancel();
    }
}
