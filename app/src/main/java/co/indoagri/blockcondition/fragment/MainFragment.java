package co.indoagri.blockcondition.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.input.BOMInputStream;
import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.model.Data.SKB;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.model.Users.UserLogin;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.widget.ToastMessage;

import static co.indoagri.blockcondition.activity.MainActivity.SESS_HPERIOD;
import static co.indoagri.blockcondition.activity.MainActivity.SESS_HZYEAR;
import static co.indoagri.blockcondition.routines.Constants.PARAM1;


public class MainFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    DatabaseHandler database;
    private ProgressDialog progressDialog;
    View view,matureFrame1,matureFrame2,matureFrame3,viewLine1,viewLine2;
    View rootView;
    FragmentHomeListener fragmentHomeListener;
    Button set_setup,set_skb,set_entry,set_display,set_summary;
    TextView txtHome_Date,txtHome_TotalBlock,txtHome_TotalSurveyedBlock,
    txtHome_MatureNumberOfBlock,txtHome_MatureSurveyedBlock,txtHome_MatureNotSurveyedBlock,
            txtHome_IMMatureNumberOfBlock,txtHome_IMMatureSurveyedBlock,txtHome_IMMatureNotSurveyedBlock;

    TextView Total_Surveyed_1,Nama_Level_1,Total_Surveyed_2,Nama_Level_2,Total_Surveyed_3,Nama_Level_3;
    TextView Total_Surveyed_4,Nama_Level_4,Total_Surveyed_5,Nama_Level_5,Total_Surveyed_6,Nama_Level_6;
    TextView Total_Surveyed_7,Nama_Level_7,Total_Surveyed_8,Nama_Level_8,Total_Surveyed_9,Nama_Level_9,Total_Surveyed_10,Nama_Level_10;
    TextView Total_Surveyed_SKB_1,Nama_Level_SKB_1,Total_Surveyed_SKB_2,Nama_Level_SKB_2;
    TextView Total_Level_SKB_1,Total_Level_SKB_2;

    // POKOK //

    TextView Total_Surveyed_POKOK_1,Nama_Level_POKOK_1,Total_Surveyed_POKOK_2,Nama_Level_POKOK_2;
    TextView Total_Surveyed_POKOK_3,Nama_Level_POKOK_3,Total_Surveyed_POKOK_4,Nama_Level_POKOK_4;
    TextView Total_Surveyed_POKOK_5,Nama_Level_POKOK_5,Total_Surveyed_POKOK_6,Nama_Level_POKOK_6;
    TextView Total_Surveyed_POKOK_7,Nama_Level_POKOK_7,Total_Surveyed_POKOK_8,Nama_Level_POKOK_8;

    TextView Hijau_1_value,Hijau_2_value,Hijau_3_value,Hijau_4_value,Hijau_5_value,Hijau_6_value,Hijau_7_value,Hijau_8_value,Hijau_9_value,Hijau_10_value;
    TextView Kuning_1_value,Kuning_2_value,Kuning_3_value,Kuning_4_value,Kuning_5_value,Kuning_6_value,Kuning_7_value,Kuning_8_value,Kuning_9_value,Kuning_10_value;
    TextView Merah_1_value,Merah_2_value,Merah_3_value,Merah_4_value,Merah_5_value,Merah_6_value,Merah_7_value,Merah_8_value,Merah_9_value,Merah_10_value;

    TextView Hijau_1_skb_value,Hijau_2_skb_value;
    TextView Kuning_1_skb_value,Kuning_2_skb_value;
    TextView Merah_1_skb_value,Merah_2_skb_value;


    TextView Hijau_1_pokok_value,Hijau_2_pokok_value,Hijau_3_pokok_value,Hijau_4_pokok_value,Hijau_5_pokok_value,Hijau_6_pokok_value,Hijau_7_pokok_value,Hijau_8_pokok_value;
    TextView Kuning_1_pokok_value,Kuning_2_pokok_value,Kuning_3_pokok_value,Kuning_4_pokok_value,Kuning_5_pokok_value,Kuning_6_pokok_value,Kuning_7_pokok_value,Kuning_8_pokok_value;
    TextView Merah_1_pokok_value,Merah_2_pokok_value,Merah_3_pokok_value,Merah_4_pokok_value,Merah_5_pokok_value,Merah_6_pokok_value,Merah_7_pokok_value,Merah_8_pokok_value;

    TextView Total_Level_POKOK_1,Total_Level_POKOK_2,Total_Level_POKOK_3,Total_Level_POKOK_4,Total_Level_POKOK_5,Total_Level_POKOK_6,Total_Level_POKOK_7,Total_Level_POKOK_8;


    LinearLayout Hijau_1,Kuning_1,Merah_1,Hijau_2,Kuning_2,Merah_2;

    TextView textTabImmature, textTabMature;
    private DialogProgress dialogProgress;
    TableRow Row3;
    String SESS_PERIOD;
    String SESS_ZYEAR;
    String fase = "MATURE";
    String NumberBlockMature = "0";
    String NumberBlockImmature = "0";
    String TotalBlock, TotalSurveyBlock,SurveyeBlockMature,NotSurveyedBlockMature;
    String SurveyedBlockImmature,NotSurveyedBlockImmature;

    ProgressBar mprogressBar;
    LinearLayout PanelLoading;
    TextView txtLoadingCount;
    int StatusRun = 0;
    LinearLayout MenuBottom;
    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String Smonth;
        String Syear;
        String Sday;
        Calendar cal=Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        if(month<10){
        Smonth = "0"+String.valueOf(month);
        Syear = String.valueOf(year);
        Sday = String.valueOf(dayOfMonth);
        }
        else{
            Smonth = String.valueOf(month);
            Syear = String.valueOf(year);
            Sday = String.valueOf(dayOfMonth);
        }
        showSetDate(Syear,Smonth,Sday);
    }

    public void showSetDate(String year,String month,String day) {
        new PreferenceManager(getActivity(),Constants.shared_name).setPeriod(month);
        new PreferenceManager(getActivity(),Constants.shared_name).setYear(year);
        onStart();
    }
    public interface FragmentHomeListener{
        void onSendClickHomeFragment(String input, int intent, String kondisi);
    }

    @Nullable
    public static MainFragment newInstance(int Status) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putInt(PARAM1, Status);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        database = new DatabaseHandler(MainFragment.this.getContext());
        Bundle args = getArguments();
        if (args != null) {
            StatusRun =  getArguments().getInt(PARAM1);
        }else{
            StatusRun = 10;
        }
        rootView = inflater.inflate(R.layout.fragment_home, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() instanceof FragmentHomeListener)

            // LOADING //
            mprogressBar  = rootView.findViewById(R.id.progress_bar);
            txtLoadingCount = (TextView)rootView.findViewById(R.id.txtCount);
            PanelLoading = (LinearLayout)rootView.findViewById(R.id.loadingPanel);
            MenuBottom = (LinearLayout)rootView.findViewById(R.id.MenuBottom);
            MenuBottom.setVisibility(View.GONE);
            /// LOADING //
        fragmentHomeListener = (FragmentHomeListener) getActivity();
        textTabMature = (TextView)rootView.findViewById(R.id.matureblock);
        textTabImmature = (TextView)rootView.findViewById(R.id.immatureblock);
        Row3 = (TableRow)rootView.findViewById(R.id.row3);
        textTabMature.setOnClickListener(this);
        textTabImmature.setOnClickListener(this);
        txtHome_Date = (TextView)rootView.findViewById(R.id.txtHome_Date);
        txtHome_TotalBlock = (TextView)rootView.findViewById(R.id.txtHome_TotalBlock);
        txtHome_TotalSurveyedBlock = (TextView)rootView.findViewById(R.id.txtHome_TotalSurveyedBlock);
        txtHome_MatureNumberOfBlock = (TextView)rootView.findViewById(R.id.txtHome_MatureNumberOfBlock);
        txtHome_MatureSurveyedBlock = (TextView)rootView.findViewById(R.id.txtHome_MatureSurveyedBlock);
        txtHome_MatureNotSurveyedBlock = (TextView)rootView.findViewById(R.id.txtHome_MatureNotSurveyedBlock);
        txtHome_IMMatureNumberOfBlock = (TextView)rootView.findViewById(R.id.txtHome_IMMatureNumberOfBlock);
        txtHome_IMMatureSurveyedBlock = (TextView)rootView.findViewById(R.id.txtHome_IMMatureSurveyedBlock);
        txtHome_IMMatureNotSurveyedBlock = (TextView)rootView.findViewById(R.id.txtHome_IMMatureNotSurveyedBlock);
        set_entry= (Button)rootView.findViewById(R.id.set_entry);
        set_summary= (Button)rootView.findViewById(R.id.set_summary);
        set_setup= (Button)rootView.findViewById(R.id.set_setup);
        set_skb= (Button)rootView.findViewById(R.id.set_skb);
        set_display= (Button)rootView.findViewById(R.id.set_display);
        set_display.setOnClickListener(this);
        set_entry.setOnClickListener(this);
        set_skb.setOnClickListener(this);
        set_summary.setOnClickListener(this);
        set_setup.setOnClickListener(this);
        matureFrame1 = (View)rootView.findViewById(R.id.matureFrame1);
        viewLine1 = (View)rootView.findViewById(R.id.viewLine1);
        viewLine2 = (View)rootView.findViewById(R.id.viewLine2);
        matureFrame2 = (View)rootView.findViewById(R.id.matureFrame2);
        matureFrame3 = (View)rootView.findViewById(R.id.matureFrame3);

        Total_Surveyed_1 = (TextView)rootView.findViewById(R.id.Total_Surveyed_1);
        Nama_Level_1 = (TextView)rootView.findViewById(R.id.Nama_Level_1);
        Total_Surveyed_2 = (TextView)rootView.findViewById(R.id.Total_Surveyed_2);
        Nama_Level_2 = (TextView)rootView.findViewById(R.id.Nama_Level_2);
        Total_Surveyed_3 = (TextView)rootView.findViewById(R.id.Total_Surveyed_3);
        Nama_Level_3 = (TextView)rootView.findViewById(R.id.Nama_Level_3);
        Total_Surveyed_4 = (TextView)rootView.findViewById(R.id.Total_Surveyed_4);
        Nama_Level_4 = (TextView)rootView.findViewById(R.id.Nama_Level_4);
        Total_Surveyed_5 = (TextView)rootView.findViewById(R.id.Total_Surveyed_5);
        Nama_Level_5 = (TextView)rootView.findViewById(R.id.Nama_Level_5);
        Total_Surveyed_6 = (TextView)rootView.findViewById(R.id.Total_Surveyed_6);
        Nama_Level_6 = (TextView)rootView.findViewById(R.id.Nama_Level_6);
        Total_Surveyed_7 = (TextView)rootView.findViewById(R.id.Total_Surveyed_7);
        Nama_Level_7 = (TextView)rootView.findViewById(R.id.Nama_Level_7);
        Total_Surveyed_8 = (TextView)rootView.findViewById(R.id.Total_Surveyed_8);
        Nama_Level_8 = (TextView)rootView.findViewById(R.id.Nama_Level_8);
        Total_Surveyed_9 = (TextView)rootView.findViewById(R.id.Total_Surveyed_9);
        Nama_Level_9 = (TextView)rootView.findViewById(R.id.Nama_Level_9);
        Total_Surveyed_10 = (TextView)rootView.findViewById(R.id.Total_Surveyed_10);
        Nama_Level_10 = (TextView)rootView.findViewById(R.id.Nama_Level_10);
        Total_Surveyed_SKB_1 = (TextView)rootView.findViewById(R.id.Total_Surveyed_SKB_1);
        Nama_Level_SKB_1 = (TextView)rootView.findViewById(R.id.Nama_Level_SKB_1);
        Total_Surveyed_SKB_2 = (TextView)rootView.findViewById(R.id.Total_Surveyed_SKB_2);
        Nama_Level_SKB_2 = (TextView)rootView.findViewById(R.id.Nama_Level_SKB_2);

        Total_Surveyed_POKOK_1 = (TextView)rootView.findViewById(R.id.Total_Surveyed_POKOK_1);
        Nama_Level_POKOK_1 = (TextView)rootView.findViewById(R.id.Nama_Level_POKOK_1);
        Total_Surveyed_POKOK_2 = (TextView)rootView.findViewById(R.id.Total_Surveyed_POKOK_2);
        Nama_Level_POKOK_2 = (TextView)rootView.findViewById(R.id.Nama_Level_POKOK_2);
        Total_Surveyed_POKOK_3 = (TextView)rootView.findViewById(R.id.Total_Surveyed_POKOK_3);
        Nama_Level_POKOK_3 = (TextView)rootView.findViewById(R.id.Nama_Level_POKOK_3);
        Total_Surveyed_POKOK_4 = (TextView)rootView.findViewById(R.id.Total_Surveyed_POKOK_4);
        Nama_Level_POKOK_4 = (TextView)rootView.findViewById(R.id.Nama_Level_POKOK_4);
        Total_Surveyed_POKOK_5 = (TextView)rootView.findViewById(R.id.Total_Surveyed_POKOK_5);
        Nama_Level_POKOK_5 = (TextView)rootView.findViewById(R.id.Nama_Level_POKOK_5);
        Total_Surveyed_POKOK_6 = (TextView)rootView.findViewById(R.id.Total_Surveyed_POKOK_6);
        Nama_Level_POKOK_6 = (TextView)rootView.findViewById(R.id.Nama_Level_POKOK_6);
        Total_Surveyed_POKOK_7 = (TextView)rootView.findViewById(R.id.Total_Surveyed_POKOK_7);
        Nama_Level_POKOK_7 = (TextView)rootView.findViewById(R.id.Nama_Level_POKOK_7);
        Total_Surveyed_POKOK_8 = (TextView)rootView.findViewById(R.id.Total_Surveyed_POKOK_8);
        Nama_Level_POKOK_8 = (TextView)rootView.findViewById(R.id.Nama_Level_POKOK_8);
        Hijau_1_value = (TextView)rootView.findViewById(R.id.Hijau_1_value);
        Hijau_2_value = (TextView)rootView.findViewById(R.id.Hijau_2_value);
        Hijau_3_value = (TextView)rootView.findViewById(R.id.Hijau_3_value);
        Hijau_4_value = (TextView)rootView.findViewById(R.id.Hijau_4_value);
        Hijau_5_value = (TextView)rootView.findViewById(R.id.Hijau_5_value);
        Hijau_6_value = (TextView)rootView.findViewById(R.id.Hijau_6_value);
        Hijau_7_value = (TextView)rootView.findViewById(R.id.Hijau_7_value);
        Hijau_8_value = (TextView)rootView.findViewById(R.id.Hijau_8_value);
        Hijau_9_value = (TextView)rootView.findViewById(R.id.Hijau_9_value);
        Hijau_10_value = (TextView)rootView.findViewById(R.id.Hijau_10_value);

        Hijau_1_skb_value = (TextView)rootView.findViewById(R.id.Hijau_1_skb_value);
        Hijau_2_skb_value = (TextView)rootView.findViewById(R.id.Hijau_2_skb_value);

        Hijau_1_pokok_value = (TextView)rootView.findViewById(R.id.Hijau_1_pokok_value);
        Hijau_2_pokok_value = (TextView)rootView.findViewById(R.id.Hijau_2_pokok_value);
        Hijau_3_pokok_value = (TextView)rootView.findViewById(R.id.Hijau_3_pokok_value);
        Hijau_4_pokok_value = (TextView)rootView.findViewById(R.id.Hijau_4_pokok_value);
        Hijau_5_pokok_value = (TextView)rootView.findViewById(R.id.Hijau_5_pokok_value);
        Hijau_6_pokok_value = (TextView)rootView.findViewById(R.id.Hijau_6_pokok_value);
        Hijau_7_pokok_value = (TextView)rootView.findViewById(R.id.Hijau_7_pokok_value);
        Hijau_8_pokok_value = (TextView)rootView.findViewById(R.id.Hijau_8_pokok_value);

        Kuning_1_value = (TextView)rootView.findViewById(R.id.Kuning_1_value);
        Kuning_2_value = (TextView)rootView.findViewById(R.id.Kuning_2_value);
        Kuning_3_value= (TextView)rootView.findViewById(R.id.Kuning_3_value);
        Kuning_4_value = (TextView)rootView.findViewById(R.id.Kuning_4_value);
        Kuning_5_value = (TextView)rootView.findViewById(R.id.Kuning_5_value);
        Kuning_6_value = (TextView)rootView.findViewById(R.id.Kuning_6_value);
        Kuning_7_value = (TextView)rootView.findViewById(R.id.Kuning_7_value);
        Kuning_8_value = (TextView)rootView.findViewById(R.id.Kuning_8_value);
        Kuning_9_value = (TextView)rootView.findViewById(R.id.Kuning_9_value);
        Kuning_10_value = (TextView)rootView.findViewById(R.id.Kuning_10_value);

        Kuning_1_skb_value= (TextView)rootView.findViewById(R.id.Kuning_1_skb_value);
        Kuning_2_skb_value= (TextView)rootView.findViewById(R.id.Kuning_2_skb_value);


        Kuning_1_pokok_value= (TextView)rootView.findViewById(R.id.Kuning_1_pokok_value);
        Kuning_2_pokok_value= (TextView)rootView.findViewById(R.id.Kuning_2_pokok_value);
        Kuning_3_pokok_value= (TextView)rootView.findViewById(R.id.Kuning_3_pokok_value);
        Kuning_4_pokok_value= (TextView)rootView.findViewById(R.id.Kuning_4_pokok_value);
        Kuning_5_pokok_value= (TextView)rootView.findViewById(R.id.Kuning_5_pokok_value);
        Kuning_6_pokok_value= (TextView)rootView.findViewById(R.id.Kuning_6_pokok_value);
        Kuning_7_pokok_value= (TextView)rootView.findViewById(R.id.Kuning_7_pokok_value);
        Kuning_8_pokok_value= (TextView)rootView.findViewById(R.id.Kuning_8_pokok_value);

        Merah_1_value = (TextView)rootView.findViewById(R.id.Merah_1_value);
        Merah_2_value = (TextView)rootView.findViewById(R.id.Merah_2_value);
        Merah_3_value = (TextView)rootView.findViewById(R.id.Merah_3_value);
        Merah_4_value = (TextView)rootView.findViewById(R.id.Merah_4_value);
        Merah_5_value = (TextView)rootView.findViewById(R.id.Merah_5_value);
        Merah_6_value = (TextView)rootView.findViewById(R.id.Merah_6_value);
        Merah_7_value = (TextView)rootView.findViewById(R.id.Merah_7_value);
        Merah_8_value = (TextView)rootView.findViewById(R.id.Merah_8_value);
        Merah_9_value = (TextView)rootView.findViewById(R.id.Merah_9_value);
        Merah_10_value = (TextView)rootView.findViewById(R.id.Merah_10_value);

        Merah_1_pokok_value= (TextView)rootView.findViewById(R.id.Merah_1_pokok_value);
        Merah_2_pokok_value = (TextView)rootView.findViewById(R.id.Merah_2_pokok_value);
        Merah_3_pokok_value= (TextView)rootView.findViewById(R.id.Merah_3_pokok_value);
        Merah_4_pokok_value= (TextView)rootView.findViewById(R.id.Merah_4_pokok_value);
        Merah_5_pokok_value=(TextView)rootView.findViewById(R.id.Merah_5_pokok_value);
        Merah_6_pokok_value= (TextView)rootView.findViewById(R.id.Merah_6_pokok_value);
        Merah_7_pokok_value= (TextView)rootView.findViewById(R.id.Merah_7_pokok_value);
        Merah_8_pokok_value= (TextView)rootView.findViewById(R.id.Merah_8_pokok_value);

        Merah_1_skb_value = (TextView)rootView.findViewById(R.id.Merah_1_skb_value);
        Merah_2_skb_value = (TextView)rootView.findViewById(R.id.Merah_2_skb_value);

        Total_Level_SKB_1 = (TextView)rootView.findViewById(R.id.Total_Level_SKB_1);
        Total_Level_SKB_2 = (TextView)rootView.findViewById(R.id.Total_Level_SKB_2);

        Total_Level_POKOK_1 = (TextView)rootView.findViewById(R.id.Total_Level_POKOK_1);
        Total_Level_POKOK_2 = (TextView)rootView.findViewById(R.id.Total_Level_POKOK_2);
        Total_Level_POKOK_3 = (TextView)rootView.findViewById(R.id.Total_Level_POKOK_3);
        Total_Level_POKOK_4 = (TextView)rootView.findViewById(R.id.Total_Level_POKOK_4);
        Total_Level_POKOK_5 = (TextView)rootView.findViewById(R.id.Total_Level_POKOK_5);
        Total_Level_POKOK_6 = (TextView)rootView.findViewById(R.id.Total_Level_POKOK_6);
        Total_Level_POKOK_7 = (TextView)rootView.findViewById(R.id.Total_Level_POKOK_7);
        Total_Level_POKOK_8 = (TextView)rootView.findViewById(R.id.Total_Level_POKOK_8);

        Hijau_1 = (LinearLayout)rootView.findViewById(R.id.Hijau_1);
        Hijau_2 = (LinearLayout)rootView.findViewById(R.id.Hijau_2);

        Kuning_1 = (LinearLayout)rootView.findViewById(R.id.Kuning_1);
        Kuning_2 = (LinearLayout)rootView.findViewById(R.id.Kuning_2);

        Merah_1 = (LinearLayout)rootView.findViewById(R.id.Merah_1);
        Merah_2 = (LinearLayout)rootView.findViewById(R.id.Merah_2);
        Total_Surveyed_1.setOnClickListener(this);
        Total_Surveyed_2.setOnClickListener(this);
        Total_Surveyed_3.setOnClickListener(this);
        Total_Surveyed_4.setOnClickListener(this);
        Total_Surveyed_5.setOnClickListener(this);
        Total_Surveyed_6.setOnClickListener(this);
        Total_Surveyed_7.setOnClickListener(this);
        Total_Surveyed_8.setOnClickListener(this);
        Total_Surveyed_9.setOnClickListener(this);
        Total_Surveyed_10.setOnClickListener(this);
        Total_Surveyed_SKB_1.setOnClickListener(this);
        Total_Surveyed_SKB_2.setOnClickListener(this);

        Total_Surveyed_POKOK_1.setOnClickListener(this);
        Total_Surveyed_POKOK_2.setOnClickListener(this);
        Total_Surveyed_POKOK_3.setOnClickListener(this);
        Total_Surveyed_POKOK_4.setOnClickListener(this);
        Total_Surveyed_POKOK_5.setOnClickListener(this);
        Total_Surveyed_POKOK_6.setOnClickListener(this);
        Total_Surveyed_POKOK_7.setOnClickListener(this);
        Total_Surveyed_POKOK_8.setOnClickListener(this);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentHomeListener) {
            fragmentHomeListener = (FragmentHomeListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentLevelBlockListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentHomeListener = null;
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }

    @Override
    public void onStart() {
        super.onStart();
        MenuBottom.setVisibility(View.GONE);
            if(StatusRun == 0){
              /*  ToastMessage toastMessage2 = new ToastMessage(getActivity());
                toastMessage2.shortMessage(String.valueOf(StatusRun));*/
                SESS_PERIOD =  new PreferenceManager(getActivity(),Constants.shared_name).getPeriod();
                SESS_ZYEAR=  new PreferenceManager(getActivity(),Constants.shared_name).getYear();
                if(SESS_PERIOD.equalsIgnoreCase("0")|| SESS_ZYEAR.equalsIgnoreCase("0")){
                    SESS_PERIOD.equalsIgnoreCase(SESS_HPERIOD);
                    SESS_ZYEAR.equalsIgnoreCase(SESS_HZYEAR);
                    txtHome_Date.setText(SESS_PERIOD+" / "+SESS_ZYEAR);
                    txtHome_Date.setOnClickListener(this);
            /*BackgroundHeader task = new BackgroundHeader(MainFragment.this);
            task.execute();*/
                    setHeader();
                }
                else{
                    SESS_PERIOD =  new PreferenceManager(getActivity(),Constants.shared_name).getPeriod();
                    SESS_ZYEAR=  new PreferenceManager(getActivity(),Constants.shared_name).getYear();
                    txtHome_Date.setText(SESS_PERIOD+" / "+SESS_ZYEAR);
                    txtHome_Date.setOnClickListener(this);
                    setHeader();
                }
            }else{
      /*  ToastMessage toastMessage2 = new ToastMessage(getActivity());
                toastMessage2.shortMessage(String.valueOf(StatusRun));*/
                SESS_PERIOD =  new PreferenceManager(getActivity(),Constants.shared_name).getPeriod();
                SESS_ZYEAR=  new PreferenceManager(getActivity(),Constants.shared_name).getYear();
                if(SESS_PERIOD.equalsIgnoreCase("0")|| SESS_ZYEAR.equalsIgnoreCase("0")){
                    SESS_PERIOD.equalsIgnoreCase(SESS_HPERIOD);
                    SESS_ZYEAR.equalsIgnoreCase(SESS_HZYEAR);
                    txtHome_Date.setText(SESS_PERIOD+" / "+SESS_ZYEAR);
                    txtHome_Date.setOnClickListener(this);
            /*BackgroundHeader task = new BackgroundHeader(MainFragment.this);
            task.execute();*/
                    setHeader();
                }
                else{
                    SESS_PERIOD =  new PreferenceManager(getActivity(),Constants.shared_name).getPeriod();
                    SESS_ZYEAR=  new PreferenceManager(getActivity(),Constants.shared_name).getYear();
                    txtHome_Date.setText(SESS_PERIOD+" / "+SESS_ZYEAR);
                    txtHome_Date.setOnClickListener(this);
                    setHeader();
                }
            }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.set_entry:
                if (fragmentHomeListener != null)
                    fragmentHomeListener.onSendClickHomeFragment(getResources().getString(R.string.entry_condition),0,"0");
                break;
            case R.id.set_summary:
                if (fragmentHomeListener != null)
                    fragmentHomeListener.onSendClickHomeFragment(getResources().getString(R.string.summary_condition),0,"0");
                break;
            case R.id.set_setup:
                if (fragmentHomeListener != null)
                    fragmentHomeListener.onSendClickHomeFragment(getResources().getString(R.string.setup_condition),0,"0");
                break;
            case R.id.txtHome_Date:
                if (fragmentHomeListener != null)
                    fragmentHomeListener.onSendClickHomeFragment("DATE",Constants.txtHome_Date,"0");
                MonthYearPickerDialog pd = new MonthYearPickerDialog();
                pd.setListener(this);
                pd.show(getFragmentManager(), "MonthYearPickerDialog");
                break;
            case R.id.txtHome_MatureNotSurveyedBlock:
                if (fragmentHomeListener != null)
                    fragmentHomeListener.onSendClickHomeFragment("header",Constants.MatureNotSurveyedBlock,"MATURE");
                break;
            case R.id.txtHome_IMMatureNotSurveyedBlock:
                if (fragmentHomeListener != null)
                    fragmentHomeListener.onSendClickHomeFragment("header",Constants.IMMatureNotSurveyedBlock,"IMMATURE");
                break;
            case R.id.immatureblock:
                SetTabIMMature();
                textTabImmature.setBackgroundColor(getResources().getColor(R.color.colorOcean));
                textTabMature.setBackgroundColor(getResources().getColor(R.color.colorTransparant));
                break;
            case R.id.matureblock:
                SetTabMature();
                textTabImmature.setBackgroundColor(getResources().getColor(R.color.colorTransparant));
                textTabMature.setBackgroundColor(getResources().getColor(R.color.colorOcean));
                break;
            case R.id.Total_Surveyed_1:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_Block,tblT_BlockCondition.XML_TitiPanen);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_Block,tblT_BlockCondition.XML_TitiRintis);
                    break;
                }
            case R.id.Total_Surveyed_2:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_Block,tblT_BlockCondition.XML_Parit);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_Block,tblT_BlockCondition.XML_Parit);
                    break;
                }
            case R.id.Total_Surveyed_3:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_Block,tblT_BlockCondition.XML_Jalan);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_Block,tblT_BlockCondition.XML_Jalan);
                    break;
                }

            case R.id.Total_Surveyed_4:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_Block,tblT_BlockCondition.XML_Jembatan);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_Block,tblT_BlockCondition.XML_Jembatan);
                    break;
                }

            case R.id.Total_Surveyed_5:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_Block,tblT_BlockCondition.XML_Tikus);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_Block,tblT_BlockCondition.XML_Tikus);
                    break;
                }

            case R.id.Total_Surveyed_6:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_Block,tblT_BlockCondition.XML_BW);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_Block,tblT_BlockCondition.XML_BW);
                    break;
                }

            case R.id.Total_Surveyed_7:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_Block,tblT_BlockCondition.XML_Pencurian);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_Block,tblT_BlockCondition.XML_Pencurian);
                    break;
                }

            case R.id.Total_Surveyed_SKB_1:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_SKB,tblT_BlockCondition.XML_TPHBersih);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_SKB,tblT_BlockCondition.XML_TPH);
                    break;
                }
            case R.id.Total_Surveyed_SKB_2:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_SKB,tblT_BlockCondition.XML_TPHBersih2);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_SKB,tblT_BlockCondition.XML_TPH2);
                    break;
                }
            case R.id.Total_Surveyed_POKOK_1:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_Piringan);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_Piringan);
                    break;
                }
            case R.id.Total_Surveyed_POKOK_2:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_PasarPanen);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_PasarRintis);
                    break;
                }
            case R.id.Total_Surveyed_POKOK_3:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_TunasPokok);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_Sanitasi);
                    break;
                }
            case R.id.Total_Surveyed_POKOK_4:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_Gawangan);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_Gawangan);
                    break;
                }
            case R.id.Total_Surveyed_POKOK_5:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_Drainase);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_Drainase);
                    break;
                }
            case R.id.Total_Surveyed_POKOK_6:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_Ganoderma);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_Kacangan);
                    break;
                }
            case R.id.Total_Surveyed_POKOK_7:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_Rayap);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_Rayap);
                    break;
                }
            case R.id.Total_Surveyed_POKOK_8:
                if(fase.equalsIgnoreCase("MATURE")){
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("MATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_Orcytes);
                    break;
                }else{
                    if (fragmentHomeListener != null)
                        fragmentHomeListener.onSendClickHomeFragment("IMMATURE",Constants.MainResult_POKOK,tblT_BlockCondition.XML_Orcytes);
                    break;
                }
            default:
                break;
        }
    }

    private void setHeader(){
    dialogProgress = new DialogProgress(MainFragment.this.getContext(), getResources().getString(R.string.wait));
    dialogProgress.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try{
                    database.openTransaction();
                    UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
//                    database.closeTransaction();
//                    database.openTransaction();
                    getTotalBlock(userLogin);
                    getSurveyedBlock(userLogin);
                    numberOfBlock_Mature(userLogin);
                    surveyedBlock_Mature(userLogin);
                    numberOfBlock_IMMature(userLogin);
                    surveyedBlock_IMMature(userLogin);
                }catch(SQLiteConstraintException e){
                    e.printStackTrace();
                    database.closeTransaction();
                    ToastMessage toastMessage = new ToastMessage(getActivity());
                    toastMessage.shortMessage(String.valueOf(e.getMessage()));
                }finally{
                    txtHome_TotalBlock.setText("Total Block : "+TotalBlock);
                    txtHome_TotalSurveyedBlock.setText("Total Surveyed Block : "+TotalSurveyBlock);
                    txtHome_MatureNumberOfBlock.setText("Number Of Block : "+NumberBlockMature);
                    txtHome_MatureSurveyedBlock.setText("Surveyed Block : "+SurveyeBlockMature);
                    txtHome_MatureNotSurveyedBlock.setText("Not Surveyed Block "+String.valueOf(Integer.parseInt(NumberBlockMature)-Integer.parseInt(SurveyeBlockMature)));
                    txtHome_IMMatureNumberOfBlock.setText("Number Of Block : "+NumberBlockImmature);
                    txtHome_IMMatureSurveyedBlock.setText("Surveyed Block : "+SurveyedBlockImmature);
                    txtHome_IMMatureNotSurveyedBlock.setText("Not Surveyed Block "+String.valueOf(Integer.parseInt(NumberBlockImmature)-Integer.parseInt(SurveyedBlockImmature)));
                    dialogProgress.dismiss();
                    database.closeTransaction();
                    Halaman();
                }
            }
        }, 2000);
    }

    private class BackgroundHeader extends AsyncTask <Void, Void, Void> {
        private ProgressDialog dialog;
        public BackgroundHeader(MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
        }
        @Override
        protected void onPreExecute() {
            if(!dialog.isShowing()){
                dialog.setMessage("Get Header Dashboard, please wait.");
                dialog.show();
            }

        }
        @Override
        protected void onPostExecute(Void result) {

            if (dialog.isShowing()) {
                dialog.dismiss();
                txtHome_TotalBlock.setText("Total Block : "+TotalBlock);
                txtHome_TotalSurveyedBlock.setText("Total Surveyed Block : "+TotalSurveyBlock);
                txtHome_MatureNumberOfBlock.setText("Number Of Block : "+NumberBlockMature);
                txtHome_MatureSurveyedBlock.setText("Surveyed Block : "+SurveyeBlockMature);
                txtHome_MatureNotSurveyedBlock.setText("Not Surveyed Block "+String.valueOf(Integer.parseInt(NumberBlockMature)-Integer.parseInt(SurveyeBlockMature)));
                txtHome_IMMatureNumberOfBlock.setText("Number Of Block : "+NumberBlockImmature);
                txtHome_IMMatureSurveyedBlock.setText("Surveyed Block : "+SurveyedBlockImmature);
                txtHome_IMMatureNotSurveyedBlock.setText("Not Surveyed Block "+String.valueOf(Integer.parseInt(NumberBlockImmature)-Integer.parseInt(SurveyedBlockImmature)));
                Halaman();
            }

        }
        @Override
        protected Void doInBackground(Void... params) {
            database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();
            try{
                database.openTransaction();
                getTotalBlock(userLogin);
                getSurveyedBlock(userLogin);
                numberOfBlock_Mature(userLogin);
                surveyedBlock_Mature(userLogin);
                numberOfBlock_IMMature(userLogin);
                surveyedBlock_IMMature(userLogin);
            }catch(SQLiteConstraintException e){
                e.printStackTrace();
                database.closeTransaction();
                ToastMessage toastMessage = new ToastMessage(getActivity());
                toastMessage.shortMessage(String.valueOf(e.getMessage()));
            }finally{
                database.closeTransaction();
            }
            return null;
        }
    }

    private void getTotalBlock(UserLogin userLogin){
            String result = null;
            String[] a = new String[4];
            a[0] = "9999-12-31";
            a[1] = "01";
            a[2] = "01";
            a[3] =userLogin.getDivision();
            String query = "SELECT * FROM BLOCK_HDRC WHERE VALIDTO=? AND STATUS=? " +
                    "AND TYPE=? AND DIVISI=?  AND ( project LIKE 'M%' OR project LIKE 'T%') ";
            int Total = database.getTaskCount(query,a);
            result = String.valueOf(Total);
            TotalBlock = result;
    }
    private void getSurveyedBlock(UserLogin userLogin){
        String result = null;
        String[] a = new String[5];
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] =SESS_PERIOD;
        a[4] = "0";
        String query = "SELECT *  FROM tblT_BlockCondition a " +
                "INNER JOIN block_hdrc b ON a.block=b.block AND b.validto=? AND b.divisi=? " +
                "WHERE a.zyear=? AND  a.period=? AND a.TransLevel=?";
        int Total = database.getTaskCount(query,a);
        result = String.valueOf(Total);
        TotalSurveyBlock = result;
    }

    private void numberOfBlock_Mature(UserLogin userLogin){
        String result = null;
        String[] a = new String[4];
        a[0] = "9999-12-31";
        a[1] = "01";
        a[2] = "01";
        a[3] = userLogin.getDivision();
        String query = "SELECT * FROM BLOCK_HDRC WHERE validto=? AND status=? " +
                "AND type =? AND project LIKE 'M%' AND divisi=?";
        int Total = database.getTaskCount(query,a);
        result = String.valueOf(Total);
        NumberBlockMature = result;
    }

    private void surveyedBlock_Mature(UserLogin userLogin){
        String result = null;
        String[] a = new String[6];
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = "01";
        a[3] = SESS_ZYEAR;
        a[4] = SESS_PERIOD;
        a[5] = "0";

        String query = "SELECT *  FROM tblT_BlockCondition a " +
                "INNER JOIN block_hdrc b ON a.block=b.block AND b.validto=? AND b.divisi=? AND b.type=? AND b.project LIKE 'M%'" +
                "WHERE a.zyear=? AND  a.period=? AND a.TransLevel=?";
        int Total = database.getTaskCount(query,a);
        result = String.valueOf(Total);
        SurveyeBlockMature = result;
    }

    private void numberOfBlock_IMMature(UserLogin userLogin){
        String result = null;
        String[] a = new String[4];
        a[0] = "9999-12-31";
        a[1] = "01";
        a[2] = "01";
        a[3] = userLogin.getDivision();
        String query = "SELECT * FROM BLOCK_HDRC WHERE validto=? AND status=? " +
                "AND type =? AND project LIKE 'T%' AND divisi=?";
        int Total = database.getTaskCount(query,a);
        result = String.valueOf(Total);
        NumberBlockImmature = result;
    }

    private void surveyedBlock_IMMature(UserLogin userLogin){
        String result = null;
          String[] a = new String[6];
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = "01";
        a[3] = SESS_ZYEAR;
        a[4] = SESS_PERIOD;
        a[5] = "0";
        String query = "SELECT *  FROM tblT_BlockCondition a " +
                "INNER JOIN block_hdrc b ON a.block=b.block AND b.validto=? AND b.divisi=? AND b.type=? AND b.project LIKE 'T%'" +
                "WHERE a.zyear=? AND  a.period=? AND a.TransLevel=?";
        int Total = database.getTaskCount(query,a);
        result = String.valueOf(Total);
        SurveyedBlockImmature = result;
    }

    private void Halaman(){
        txtHome_TotalBlock.setOnClickListener(this);
        txtHome_TotalSurveyedBlock.setOnClickListener(this);
        txtHome_MatureNumberOfBlock.setOnClickListener(this);
        txtHome_MatureSurveyedBlock.setOnClickListener(this);
        txtHome_MatureNotSurveyedBlock.setOnClickListener(this);
        txtHome_IMMatureNumberOfBlock.setOnClickListener(this);
        txtHome_IMMatureSurveyedBlock.setOnClickListener(this);
        txtHome_IMMatureNotSurveyedBlock.setOnClickListener(this);
        if(fase.equalsIgnoreCase("MATURE")){
            matureFrame1.setVisibility(View.VISIBLE);
            matureFrame2.setVisibility(View.VISIBLE);
            matureFrame3.setVisibility(View.VISIBLE);
            viewLine1.setVisibility(View.VISIBLE);
            viewLine2.setVisibility(View.VISIBLE);
            SetTabMature();
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            matureFrame1.setVisibility(View.VISIBLE);
            matureFrame2.setVisibility(View.VISIBLE);
            matureFrame3.setVisibility(View.VISIBLE);
            viewLine1.setVisibility(View.VISIBLE);
            viewLine2.setVisibility(View.VISIBLE);
            SetTabIMMature();
        }

    }
    private void SetTabMature(){
        Row3.setVisibility(View.VISIBLE);
        fase = "MATURE";
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        try{
            if(TitiPanen(userLogin)){
                Parit(userLogin);
            }
            if(Parit(userLogin)){
                Jalan(userLogin);
            }
            if(Jalan(userLogin)){
                Jembatan(userLogin);
            }
            if(Jembatan(userLogin)){
                Tikus(userLogin);
            }
            if(BW(userLogin)){
                Pencurian(userLogin);
            }
            if(Pencurian(userLogin)){
                SKB1(userLogin);
            }
            if(SKB1(userLogin)){
                SKB2(userLogin);
            }
            if(SKB2(userLogin)){
                Piringan(userLogin);
            }
            if(Piringan(userLogin)){
                PasarPanen(userLogin);
            }
            if(PasarPanen(userLogin)){
                TunasPokok(userLogin);
            }
            if(TunasPokok(userLogin)){
                Gawangan(userLogin);
            }
            if(Gawangan(userLogin)){
                Drainase(userLogin);
            }
            if(Drainase(userLogin)){
                Ganoderma(userLogin);
            }
            if(Ganoderma( userLogin)){
                Kacangan(userLogin);
            }
            if(Kacangan(userLogin)){
                Rayap(userLogin);
            }
            if(Rayap(userLogin)){
                Orcytes(userLogin);
            }
        }catch(SQLiteConstraintException e){
            e.printStackTrace();
            database.closeTransaction();
            ToastMessage toastMessage = new ToastMessage(getActivity());
            toastMessage.shortMessage(String.valueOf(e.getMessage()));
        }finally{
               /* matureFrame1.setVisibility(View.VISIBLE);
                matureFrame2.setVisibility(View.VISIBLE);
                matureFrame3.setVisibility(View.VISIBLE);
                viewLine1.setVisibility(View.VISIBLE);
                viewLine2.setVisibility(View.VISIBLE);*/
            database.closeTransaction();
        }
    }

    private void SetTabIMMature(){
        Row3.setVisibility(View.GONE);
        fase = "IMMATURE";
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        try{
            database.openTransaction();
            if(TitiRintis(userLogin)){
                Parit(userLogin);
            }
            if(Parit(userLogin)){
                Jalan(userLogin);
            }
            if(Jalan(userLogin)){
                Jembatan(userLogin);
            }
            if(Jembatan(userLogin)){
                Tikus(userLogin);
            }
            if(BW(userLogin)){
                Pencurian(userLogin);
            }
            if(Pencurian(userLogin)){
                SKBTPH1(userLogin);
            }
            if(SKBTPH1(userLogin)){
                SKBTPH2(userLogin);
            }
            if(SKBTPH2(userLogin)){
                Piringan(userLogin);
            }
            if(Piringan(userLogin)){
                PasarRintis(userLogin);
            }
            if(PasarRintis(userLogin)){
                Sanitasi(userLogin);
            }
            if(Sanitasi(userLogin)){
                Gawangan(userLogin);
            }
            if(Gawangan(userLogin)){
                Drainase(userLogin);
            }
            if(Drainase(userLogin)){
                Kacangan(userLogin);
            }
            if(Kacangan(userLogin)){
                Rayap(userLogin);
            }
            if(Rayap(userLogin)){
                Orcytes(userLogin);
            }

        }catch(SQLiteConstraintException e){
            e.printStackTrace();
            database.closeTransaction();
            ToastMessage toastMessage = new ToastMessage(getActivity());
            toastMessage.shortMessage(String.valueOf(e.getMessage()));
        }finally{
            database.closeTransaction();
        }
    }

    private boolean TitiRintis(UserLogin userLogin){
        Nama_Level_1.setText("Titi Rintis");
        return HitungTitiRintis(userLogin);
    }
    private boolean TitiPanen(UserLogin userLogin){
        Nama_Level_1.setText("Titi Panen");
        return HitungTitiPanen(userLogin);
    }
    private boolean Parit(UserLogin userLogin){
        Nama_Level_2.setText("Parit");
       return HitungParit(userLogin);
    }
    private boolean Jalan(UserLogin userLogin){
        Nama_Level_3.setText("Jalan");
        return HitungJalan(userLogin);
    }
    private boolean Jembatan(UserLogin userLogin){
        Nama_Level_4.setText("Jembatan");
        return HitungJembatan(userLogin);
    }
    private boolean Tikus(UserLogin userLogin){
        //Total_Surveyed_5.setText("20");
        Nama_Level_5.setText("Tikus");
        return HitungTikus(userLogin);
    }
    private boolean BW(UserLogin userLogin){
        Nama_Level_6.setText("Benefecial Weed");
       return HitungBW(userLogin);
    }
    private boolean Pencurian(UserLogin userLogin){
        Nama_Level_7.setText("Pencurian");
        return HitungPencurian(userLogin);
    }

    private boolean SKB1(UserLogin userLogin){
      //  Total_Surveyed_SKB_1.setText("20");
        Nama_Level_SKB_1.setText("TPH Bersih 1");
        Total_Level_SKB_1.setText("("+HitungJumlahSKB1()+" SKB)");
        return HitungTPHBersih1(userLogin);
    }
    private boolean SKB2(UserLogin userLogin){
        Nama_Level_SKB_2.setText("TPH Bersih 2");
        Total_Level_SKB_2.setText("("+HitungJumlahSKB2()+" SKB)");
        return HitungTPHBersih2(userLogin);
    }
    private boolean SKBTPH1(UserLogin userLogin){
        Nama_Level_SKB_1.setText("TPH 1");
        Total_Level_SKB_1.setText("("+HitungJumlahSKBTPH1()+" SKB)");
        return HitungTPH1(userLogin);
    }
    private boolean SKBTPH2(UserLogin userLogin){
        Nama_Level_SKB_2.setText("TPH 2");
        Total_Level_SKB_2.setText("("+HitungJumlahSKBTPH2()+" SKB)");
       return HitungTPH2(userLogin);
    }
    private boolean Piringan(UserLogin userLogin){
        Nama_Level_POKOK_1.setText("Piringan");
        Total_Level_POKOK_1.setText("("+HitungJumlahPiringan()+" Pokok)");
        return HitungPiringan(userLogin);
    }
    private boolean PasarPanen(UserLogin userLogin){
    //    Total_Surveyed_POKOK_2.setText("20");
        Nama_Level_POKOK_2.setText("Pasar Panen");
        Total_Level_POKOK_2.setText("("+HitungJumlahPasarPanen()+" Pokok)");
        return HitungPasarPanen(userLogin);
    }
    private boolean PasarRintis(UserLogin userLogin){
        Nama_Level_POKOK_2.setText("Pasar Rintis");
        Total_Level_POKOK_2.setText("("+HitungJumlahPasarRintis()+" Pokok)");
        return HitungPasarRintis(userLogin);
    }
    private boolean TunasPokok(UserLogin userLogin){
        Nama_Level_POKOK_3.setText("Tunas Pokok");
        Total_Level_POKOK_3.setText("("+HitungJumlahTunasPokok()+" Pokok)");
        return HitungTunasPokok(userLogin);
    }
    private boolean Sanitasi(UserLogin userLogin){
        Nama_Level_POKOK_3.setText("Sanitasi");
        Total_Level_POKOK_3.setText("("+HitungJumlahSanitasi()+" Pokok)");
        return HitungSanitasi(userLogin);
    }
    private boolean Gawangan(UserLogin userLogin){
        Nama_Level_POKOK_4.setText("Gawangan");
        Total_Level_POKOK_4.setText("("+HitungJumlahGawangan()+" Pokok)");
        return HitungGawangan(userLogin);
    }
    private boolean Drainase(UserLogin userLogin){
        Nama_Level_POKOK_5.setText("Drainase");
        Total_Level_POKOK_5.setText("("+HitungJumlahDrainase()+" Pokok)");
       return HitungDrainase(userLogin);
    }
    private boolean Kacangan(UserLogin userLogin){
        Nama_Level_POKOK_6.setText("Kacangan");
        if(fase.equalsIgnoreCase("MATURE")){
            Total_Level_POKOK_6.setText("("+"0"+" Pokok)");
        }else{
            Total_Level_POKOK_6.setText("("+HitungJumlahKacangan()+" Pokok)");
        }

        return HitungKacangan(userLogin);
    }

    private boolean Ganoderma(UserLogin userLogin){
        Nama_Level_8.setText("Ganoderma");
     //   Total_Level_POKOK_6.setText("("+HitungJumlahGanoderma()+" Pokok)");
        return HitungGanoderma(userLogin);
    }

    private boolean Rayap(UserLogin userLogin){
        Nama_Level_9.setText("Rayap");
   //     Total_Level_POKOK_7.setText("("+HitungJumlahRayap()+" Pokok)");
        return HitungRayap(userLogin);
    }

    private boolean Orcytes(UserLogin userLogin){
        Nama_Level_10.setText("Orcytes");
      //  Total_Level_POKOK_8.setText("("+HitungJumlahOrcytes()+" Pokok)");
       return HitungOrcytes(userLogin);
    }

    public boolean HitungTitiRintis(UserLogin userLogin) {
       List<Object> listObject;
        String query = null;
        String[] a = new String[5];
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT TitiRintis FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%' " +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND  a.TitiRintis > 0 AND a.TitiRintis != 0.0";
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT TitiRintis FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%' " +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND  a.TitiRintis > 0 AND a.TitiRintis != 0.0";
        }

            listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
          /* LinearLayout contentValue = (LinearLayout)rootView.findViewById(R.id.contentValue);
            if(((LinearLayout) contentValue).getChildCount() > 0)
                ((LinearLayout) contentValue).removeAllViews();*/
        /*    Merah_1_skb_value.setText("");*/
           /* return false;*/
            AsyncTitiRintis titiRintis = new AsyncTitiRintis(listObject,MainFragment.this);
            titiRintis.execute();
            return true;
        }else{
         /*   LinearLayout contentValue = (LinearLayout)rootView.findViewById(R.id.contentValue);
            if(((LinearLayout) contentValue).getChildCount() > 0)
                ((LinearLayout) contentValue).removeAllViews();*/
            AsyncTitiRintis titiRintis = new AsyncTitiRintis(listObject,MainFragment.this);
            titiRintis.execute();
            return true;
        }


    }

    public boolean HitungTitiPanen(UserLogin userLogin) {
        List<Object> listObject;
        String query = null;
        String[] a = new String[5];
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT TitiPanen FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%' " +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND  a.TitiPanen > 0 AND a.TitiPanen != 0.0";
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT TitiPanen FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%' " +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND  a.TitiPanen > 0 AND a.TitiPanen != 0.0";
        }

        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
          /* LinearLayout contentValue = (LinearLayout)rootView.findViewById(R.id.contentValue);
            if(((LinearLayout) contentValue).getChildCount() > 0)
                ((LinearLayout) contentValue).removeAllViews();*/
            /*    Merah_1_skb_value.setText("");*/
            /* return false;*/
            AsyncTitiPanen titiPanen = new AsyncTitiPanen(listObject,MainFragment.this);
            titiPanen.execute();
            return true;
        }else{
         /*   LinearLayout contentValue = (LinearLayout)rootView.findViewById(R.id.contentValue);
            if(((LinearLayout) contentValue).getChildCount() > 0)
                ((LinearLayout) contentValue).removeAllViews();*/
            AsyncTitiPanen titiPanen = new AsyncTitiPanen(listObject,MainFragment.this);
            titiPanen.execute();
            return true;
        }


    }

    public boolean HitungParit(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";

        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT Parit FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ?  AND project LIKE 'M%' " +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Parit > 0 AND a.Parit != 0.0";
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT Parit FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ?  AND project LIKE 'T%' " +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Parit > 0 AND a.Parit != 0.0";
        }
            listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncParit asyncParit = new AsyncParit(listObject,MainFragment.this);
            asyncParit.execute();
            return true;
        }else{
            AsyncParit asyncParit = new AsyncParit(listObject,MainFragment.this);
            asyncParit.execute();
            return true;
        }

    }

    public boolean HitungJalan(UserLogin userLogin) {
         List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT Jalan FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ?  AND project LIKE 'M%' " +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Jalan > 0 AND a.Jalan != 0.0";
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT Jalan FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ?  AND project LIKE 'T%' " +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Jalan != 0";
        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiJalan asyncKondisiJalan = new AsyncKondisiJalan(listObject,MainFragment.this);
            asyncKondisiJalan.execute();
            return true;
        }else{
            AsyncKondisiJalan asyncKondisiJalan = new AsyncKondisiJalan(listObject,MainFragment.this);
            asyncKondisiJalan.execute();
            return true;
        }
    }

    public boolean HitungJembatan(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT Jembatan FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Jembatan > 0 AND a.Jembatan != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT Jembatan FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Jembatan > 0 AND a.Jembatan != 0.0";

        }
            listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiJembatan asyncParit = new AsyncKondisiJembatan(listObject, MainFragment.this);
            asyncParit.execute();
            return true;
        }else {
            AsyncKondisiJembatan asyncParit = new AsyncKondisiJembatan(listObject, MainFragment.this);
            asyncParit.execute();
            return true;
        }
        }

    public boolean HitungTikus(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";

        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT Tikus FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Tikus > 0 AND a.Tikus != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT Tikus FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Tikus > 0 AND a.Tikus != 0.0";

        }

        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiTikus asyncKondisiTikus= new AsyncKondisiTikus(listObject,MainFragment.this);
            asyncKondisiTikus.execute();
            return true;
        }else {
            AsyncKondisiTikus asyncKondisiTikus= new AsyncKondisiTikus(listObject,MainFragment.this);
            asyncKondisiTikus.execute();
            return true;
        }
    }

    public boolean HitungBW(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";

        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT BW FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.BW > 0 AND a.BW != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT BW FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.BW > 0 AND a.BW != 0.0";

        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiBW asyncParit = new AsyncKondisiBW(listObject,MainFragment.this);
            asyncParit.execute();
            return true;
        }else {
            AsyncKondisiBW asyncParit = new AsyncKondisiBW(listObject,MainFragment.this);
            asyncParit.execute();
            return true;
        }
    }

    public boolean HitungPencurian(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";

        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT Pencurian FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Pencurian > 0 AND a.Pencurian != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT Pencurian FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Pencurian > 0 AND a.Pencurian != 0.0";

        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiPencurian asyncParit = new AsyncKondisiPencurian(listObject, MainFragment.this);
            asyncParit.execute();
            return true;
        }else {
            AsyncKondisiPencurian asyncParit = new AsyncKondisiPencurian(listObject, MainFragment.this);
            asyncParit.execute();
            return true;
        }

    }

    public boolean HitungTPHBersih1(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT TPHBersih FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.TPHBersih > 0 AND a.TPHBersih != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT TPHBersih FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.TPHBersih > 0 AND a.TPHBersih != 0.0";

        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiTPHBersih1 asyncBersih = new AsyncKondisiTPHBersih1(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }else {
            AsyncKondisiTPHBersih1 asyncBersih = new AsyncKondisiTPHBersih1(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }

    }

    public boolean HitungTPHBersih2(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT TPHBersih2 FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.TPHBersih2 > 0 AND a.TPHBersih2 != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT TPHBersih2 FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.TPHBersih2 > 0 AND a.TPHBersih2 != 0.0";

        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiTPHBersih2 asyncBersih = new AsyncKondisiTPHBersih2(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }else {
            AsyncKondisiTPHBersih2 asyncBersih = new AsyncKondisiTPHBersih2(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }
    }

    public boolean HitungTPH1(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT TPH FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.TPH > 0 AND a.TPH != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT TPH FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.TPH > 0 AND a.TPH != 0.0";

        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiTPH1 asyncBersih = new AsyncKondisiTPH1(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }else {
            AsyncKondisiTPH1 asyncBersih = new AsyncKondisiTPH1(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }
    }

    public boolean HitungTPH2(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT TPH2 FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.TPH2 > 0 AND a.TPH2 != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT TPH2 FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.TPH2 > 0 AND a.TPH2 != 0.0";

        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiTPH2 asyncBersih = new AsyncKondisiTPH2(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }else {
            AsyncKondisiTPH2 asyncBersih = new AsyncKondisiTPH2(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }
    }

    public boolean HitungPiringan(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT Piringan FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Piringan > 0 AND a.Piringan != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT Piringan FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Piringan > 0 AND a.Piringan != 0.0";

        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiPiringan asyncBersih = new AsyncKondisiPiringan(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }else {
            AsyncKondisiPiringan asyncBersih = new AsyncKondisiPiringan(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }
        }

    public boolean HitungPasarPanen(UserLogin userLogin) {
          List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT PasarPanen FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.PasarPanen > 0 AND a.PasarPanen != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT PasarPanen FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.PasarPanen > 0 AND a.PasarPanen != 0.0";

        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiPasarPanen asyncBersih = new AsyncKondisiPasarPanen(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }else {
            AsyncKondisiPasarPanen asyncBersih = new AsyncKondisiPasarPanen(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }
    }

    public boolean HitungPasarRintis(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT PasarRintis FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.PasarRintis > 0 AND a.PasarRintis != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT PasarRintis FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.PasarRintis > 0 AND a.PasarRintis != 0.0";

        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiPasarRintis asyncBersih = new AsyncKondisiPasarRintis(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }else {
            AsyncKondisiPasarRintis asyncBersih = new AsyncKondisiPasarRintis(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }
    }

    public boolean HitungTunasPokok(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT TunasPokok FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.TunasPokok > 0 AND a.TunasPokok != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT TunasPokok FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.TunasPokok > 0 AND a.TunasPokok != 0.0";

        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiTunasPokok asyncBersih = new AsyncKondisiTunasPokok(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }else {
            AsyncKondisiTunasPokok asyncBersih = new AsyncKondisiTunasPokok(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }
    }

    public boolean HitungSanitasi(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT Sanitasi FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Sanitasi > 0 AND a.Sanitasi != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT Sanitasi FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Sanitasi > 0 AND a.Sanitasi != 0.0";

        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiSanitasi asyncBersih = new AsyncKondisiSanitasi(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }else {
            AsyncKondisiSanitasi asyncBersih = new AsyncKondisiSanitasi(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }
    }

    public boolean HitungGawangan(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT Gawangan FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Gawangan > 0 AND a.Gawangan != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT Gawangan FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Gawangan > 0 AND a.Gawangan != 0.0";

        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiGawangan asyncBersih = new AsyncKondisiGawangan(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }else {
            AsyncKondisiGawangan asyncBersih = new AsyncKondisiGawangan(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }
    }

    public boolean HitungDrainase(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT Drainase FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Drainase > 0 AND a.Drainase != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT Drainase FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Drainase > 0 AND a.Drainase != 0.0";

        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiDrainase asyncBersih = new AsyncKondisiDrainase(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }else {
            AsyncKondisiDrainase asyncBersih = new AsyncKondisiDrainase(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }
    }

    public boolean HitungGanoderma(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT Ganoderma FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Ganoderma > 0 AND a.Ganoderma != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT Ganoderma FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Ganoderma > 0 AND a.Ganoderma != 0.0";
        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiGanoderma asyncBersih = new AsyncKondisiGanoderma(listObject,MainFragment.this);
            asyncBersih.execute();
            return true;
        }else {
            AsyncKondisiGanoderma asyncBersih = new AsyncKondisiGanoderma(listObject,MainFragment.this);
            asyncBersih.execute();
            return true;
        }
    }

    public boolean HitungKacangan(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT Kacangan FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Kacangan > 0 AND a.Kacangan != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT Kacangan FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Kacangan > 0 AND a.Kacangan != 0.0";

        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiKacangan asyncBersih = new AsyncKondisiKacangan(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }else {
            AsyncKondisiKacangan asyncBersih = new AsyncKondisiKacangan(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }
    }

    public boolean HitungRayap(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT Rayap FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Rayap > 0 AND a.Rayap != 0.0";

        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT Rayap FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Rayap > 0 AND a.Rayap != 0.0";

        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiRayap asyncBersih = new AsyncKondisiRayap(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }else {
            AsyncKondisiRayap asyncBersih = new AsyncKondisiRayap(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }
    }

    public boolean HitungOrcytes(UserLogin userLogin) {
        List<Object> listObject;
        String[] a = new String[5];
        String query = null;
        a[0] = "9999-12-31";
        a[1] = userLogin.getDivision();
        a[2] = SESS_ZYEAR;
        a[3] = SESS_PERIOD;
        a[4] = "0";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT Orcytes FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'M%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Orcytes > 0 AND a.Orcytes != 0.0";
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT Orcytes FROM tblT_BlockCondition a " +
                    "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi= ? AND project LIKE 'T%'" +
                    "WHERE a.ZYear = ? AND  a.Period = ? AND a.TransLevel = ? AND a.Orcytes > 0 AND a.Orcytes != 0.0";
        }
        listObject = database.getListDataRawQuery(query, tblT_BlockCondition.TABLE_NAME, a);
        if(listObject.size()==0){
            AsyncKondisiOrcytes asyncBersih = new AsyncKondisiOrcytes(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }else {
            AsyncKondisiOrcytes asyncBersih = new AsyncKondisiOrcytes(listObject, MainFragment.this);
            asyncBersih.execute();
            return true;
        }
    }

    public class AsyncTitiRintis extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        //private ProgressDialog dialog;
        int myProgressCount;
        public AsyncTitiRintis(List<Object> objects,MainFragment fragment) {
            //dialog = new ProgressDialog(fragment.getContext());

            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Titi Rintis "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_TitiRintis() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        if (dt3.getAcc_TitiRintis().equals("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (dt3.getAcc_TitiRintis().equals("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (dt3.getAcc_TitiRintis().equals("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);
            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;
            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;


            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_1_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_1_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_1_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_1_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_1_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_1_value.setLayoutParams(loparamsMerah);

            Merah_1_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_1_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_1_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_1.setText(String.valueOf(lvar_color_cnt_tot));
            //PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncTitiPanen extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        //private ProgressDialog dialog;
        int myProgressCount;
        public AsyncTitiPanen(List<Object> objects,MainFragment fragment) {
            //dialog = new ProgressDialog(fragment.getContext());

            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Titi Panen "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_TitiPanen() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        if (dt3.getAcc_TitiPanen().equals("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (dt3.getAcc_TitiPanen().equals("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (dt3.getAcc_TitiPanen().equals("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);
            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;
            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;


            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_1_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_1_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_1_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_1_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_1_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_1_value.setLayoutParams(loparamsMerah);

            Merah_1_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_1_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_1_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_1.setText(String.valueOf(lvar_color_cnt_tot));
            //PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncParit extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncParit(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Parit "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_Parit() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        if (dt3.getAcc_Parit().equals("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (dt3.getAcc_Parit().equals("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (dt3.getAcc_Parit().equals("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;


            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_2_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_2_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_2_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_2_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_2_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_2_value.setLayoutParams(loparamsMerah);
            Merah_2_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_2_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_2_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_2.setText(String.valueOf(lvar_color_cnt_tot));
            //PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiJalan extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiJalan(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Jalan "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_Jalan() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        if (dt3.getAcc_Jalan().equals("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (dt3.getAcc_Jalan().equals("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (dt3.getAcc_Jalan().equals("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_3_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_3_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_3_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_3_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_3_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_3_value.setLayoutParams(loparamsMerah);
            Merah_3_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_3_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_3_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_3.setText(String.valueOf(lvar_color_cnt_tot));
            //PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiJembatan extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiJembatan(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Jembatan "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_Jembatan() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        if (dt3.getAcc_Jembatan().equals("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (dt3.getAcc_Jembatan().equals("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (dt3.getAcc_Jembatan().equals("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_4_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_4_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_4_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_4_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_4_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_4_value.setLayoutParams(loparamsMerah);
            Merah_4_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_4_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_4_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_4.setText(String.valueOf(lvar_color_cnt_tot));
           // PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiTikus extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiTikus(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Tikus "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_Tikus() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        if (dt3.getAcc_Tikus().equals("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (dt3.getAcc_Tikus().equals("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (dt3.getAcc_Tikus().equals("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_5_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_5_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_5_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_5_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_5_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_5_value.setLayoutParams(loparamsMerah);
            Merah_5_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_5_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_5_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_5.setText(String.valueOf(lvar_color_cnt_tot));
        //    PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiBW extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiBW(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("BW "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_BW() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        if (dt3.getAcc_BW().equals("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (dt3.getAcc_BW().equals("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (dt3.getAcc_BW().equals("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_6_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_6_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_6_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_6_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_6_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_6_value.setLayoutParams(loparamsMerah);
            Merah_6_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_6_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_6_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_6.setText(String.valueOf(lvar_color_cnt_tot));
          //  PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiPencurian extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiPencurian(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Pencurian "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_Pencurian() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        if (dt3.getAcc_Pencurian().equals("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (dt3.getAcc_Pencurian().equals("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (dt3.getAcc_Pencurian().equals("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_7_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_7_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_7_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_7_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_7_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_7_value.setLayoutParams(loparamsMerah);
            Merah_7_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_7_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_7_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_7.setText(String.valueOf(lvar_color_cnt_tot));
            //PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiTPHBersih1 extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList = null;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiTPHBersih1(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("TPH Bersih 1"+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_TPHBersih() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        String value =  dt3.getAcc_TPHBersih().replaceAll(".?0*$", "");
                        if (value.equalsIgnoreCase("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_1_skb_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_1_skb_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_1_skb_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_1_skb_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_1_skb_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_1_skb_value.setLayoutParams(loparamsMerah);
            Merah_1_skb_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_1_skb_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_1_skb_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_SKB_1.setText(String.valueOf(lvar_color_cnt_tot));
            //PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiTPHBersih2 extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList = null;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiTPHBersih2(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("TPH Bersih 2"+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_TPHBersih2() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        String value =  dt3.getAcc_TPHBersih2().replaceAll(".?0*$", "");
                        if (value.equalsIgnoreCase("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_2_skb_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_2_skb_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_2_skb_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_2_skb_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_2_skb_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_2_skb_value.setLayoutParams(loparamsMerah);
            Merah_2_skb_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_2_skb_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_2_skb_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_SKB_2.setText(String.valueOf(lvar_color_cnt_tot));
           // PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiTPH1 extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList = null;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiTPH1(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("TPH 1"+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_TPH() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        String value =  dt3.getAcc_TPH().replaceAll(".?0*$", "");
                        if (value.equalsIgnoreCase("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_1_skb_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_1_skb_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_1_skb_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_1_skb_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_1_skb_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_1_skb_value.setLayoutParams(loparamsMerah);
            Merah_1_skb_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_1_skb_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_1_skb_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_SKB_1.setText(String.valueOf(lvar_color_cnt_tot));
           // PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiTPH2 extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList = null;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiTPH2(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("TPH 2"+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_TPH2() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        String value =  dt3.getAcc_TPH2().replaceAll(".?0*$", "");
                        if (value.equalsIgnoreCase("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_2_skb_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_2_skb_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_2_skb_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_2_skb_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_2_skb_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_2_skb_value.setLayoutParams(loparamsMerah);
            Merah_2_skb_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_2_skb_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_2_skb_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_SKB_2.setText(String.valueOf(lvar_color_cnt_tot));
           // PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiPiringan extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiPiringan(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Piringan "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_Piringan() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        String value =  dt3.getAcc_Piringan().replaceAll(".?0*$", "");
                        if (value.equalsIgnoreCase("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_1_pokok_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_1_pokok_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_1_pokok_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_1_pokok_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_1_pokok_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_1_pokok_value.setLayoutParams(loparamsMerah);
            Merah_1_pokok_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_1_pokok_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_1_pokok_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_POKOK_1.setText(String.valueOf(lvar_color_cnt_tot));
            //PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiPasarPanen extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiPasarPanen(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Pasar Panen "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_PasarPanen() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        String value =  dt3.getAcc_PasarPanen().replaceAll(".?0*$", "");
                        if (value.equalsIgnoreCase("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_2_pokok_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_2_pokok_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_2_pokok_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_2_pokok_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_2_pokok_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_2_pokok_value.setLayoutParams(loparamsMerah);
            Merah_2_pokok_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_2_pokok_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_2_pokok_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_POKOK_2.setText(String.valueOf(lvar_color_cnt_tot));
          //  PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiPasarRintis extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiPasarRintis(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Pasar Rintis "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_PasarRintis() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        String value =  dt3.getAcc_PasarRintis().replaceAll(".?0*$", "");
                        if (value.equalsIgnoreCase("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_2_pokok_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_2_pokok_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_2_pokok_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_2_pokok_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_2_pokok_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_2_pokok_value.setLayoutParams(loparamsMerah);
            Merah_2_pokok_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_2_pokok_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_2_pokok_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_POKOK_2.setText(String.valueOf(lvar_color_cnt_tot));
          //  PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiTunasPokok extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiTunasPokok(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Tunas Pokok "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_TunasPokok() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        String value =  dt3.getAcc_TunasPokok().replaceAll(".?0*$", "");
                        if (value.equalsIgnoreCase("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_3_pokok_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_3_pokok_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_3_pokok_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_3_pokok_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_3_pokok_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_3_pokok_value.setLayoutParams(loparamsMerah);
            Merah_3_pokok_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_3_pokok_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_3_pokok_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_POKOK_3.setText(String.valueOf(lvar_color_cnt_tot));
           // PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiSanitasi extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiSanitasi(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Sanitasi "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_Sanitasi() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        String value =  dt3.getAcc_Sanitasi().replaceAll(".?0*$", "");
                        if (value.equalsIgnoreCase("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_3_pokok_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_3_pokok_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_3_pokok_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_3_pokok_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_3_pokok_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_3_pokok_value.setLayoutParams(loparamsMerah);
            Merah_3_pokok_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_3_pokok_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_3_pokok_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_POKOK_3.setText(String.valueOf(lvar_color_cnt_tot));
          //  PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiGawangan extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiGawangan(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Gawangan "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_Gawangan() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        String value =  dt3.getAcc_Gawangan().replaceAll(".?0*$", "");
                        if (value.equalsIgnoreCase("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_4_pokok_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_4_pokok_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_4_pokok_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_4_pokok_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_4_pokok_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_4_pokok_value.setLayoutParams(loparamsMerah);
            Merah_4_pokok_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_4_pokok_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_4_pokok_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_POKOK_4.setText(String.valueOf(lvar_color_cnt_tot));
            //PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiDrainase extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiDrainase(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Drainase "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_Drainase() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        String value =  dt3.getAcc_Drainase().replaceAll(".?0*$", "");
                        if (value.equalsIgnoreCase("1")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("2")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("3")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_5_pokok_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_5_pokok_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_5_pokok_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_5_pokok_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_5_pokok_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_5_pokok_value.setLayoutParams(loparamsMerah);
            Merah_5_pokok_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_5_pokok_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_5_pokok_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_POKOK_5.setText(String.valueOf(lvar_color_cnt_tot));
            PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiGanoderma extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiGanoderma(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Ganoderma "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_Ganoderma() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        String value =  dt3.getAcc_Ganoderma();
                        if (value.equalsIgnoreCase("1") || value.equalsIgnoreCase("1.0")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("2") || value.equalsIgnoreCase("2.0")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("3") || value.equalsIgnoreCase("3.0")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_8_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_8_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_8_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_8_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_8_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_8_value.setLayoutParams(loparamsMerah);
            Merah_8_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_8_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_8_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_8.setText(String.valueOf(lvar_color_cnt_tot));
          //  PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiKacangan extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiKacangan(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Kacangan "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_Kacangan() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        String value =  dt3.getAcc_Kacangan();
                        if (value.equalsIgnoreCase("1") || value.equalsIgnoreCase("1.0")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("2") || value.equalsIgnoreCase("2.0")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("3") || value.equalsIgnoreCase("3.0")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_6_pokok_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_6_pokok_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_6_pokok_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_6_pokok_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_6_pokok_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_6_pokok_value.setLayoutParams(loparamsMerah);
            Merah_6_pokok_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_6_pokok_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_6_pokok_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_POKOK_6.setText(String.valueOf(lvar_color_cnt_tot));
            PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiRayap extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiRayap(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Rayap "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_Rayap() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        String value =  dt3.getAcc_Rayap();
                        if (value.equalsIgnoreCase("1") || value.equalsIgnoreCase("1.0")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("2") || value.equalsIgnoreCase("2.0")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("3") || value.equalsIgnoreCase("3.0")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_9_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_9_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_9_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_9_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_9_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_9_value.setLayoutParams(loparamsMerah);
            Merah_9_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_9_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_9_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_9.setText(String.valueOf(lvar_color_cnt_tot));
           // PanelLoading.setVisibility(View.GONE);
        }
    }

    public class AsyncKondisiOrcytes extends AsyncTask<Void, Integer, List<Object>> {
        int lvar_color1_cnt = 0;
        int lvar_color2_cnt = 0;
        int lvar_color3_cnt = 0;
        int lvar_color_cnt_tot = 0;
        List<Object> objectList;
        private ProgressDialog dialog;
        int myProgressCount;
        public AsyncKondisiOrcytes(List<Object> objects,MainFragment fragment) {
            dialog = new ProgressDialog(fragment.getContext());
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText("Orcytes "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected List<Object> doInBackground(Void... arg0) {
            for (int dt = 0; dt < objectList.size(); dt++) {
                if (isCancelled()) {
                    break;
                } else {
                    Log.e("In Background","current value;"+ dt);
                    publishProgress(dt);
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) objectList.get(dt);
                    if (dt3.getAcc_Orcytes() == null) {
                        lvar_color1_cnt = 0;
                        lvar_color2_cnt = 0;
                        lvar_color3_cnt = 0;
                    } else {
                        String value =  dt3.getAcc_Orcytes();
                        if (value.equalsIgnoreCase("1") || value.equalsIgnoreCase("1.0")) {
                            lvar_color1_cnt = lvar_color1_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("2") || value.equalsIgnoreCase("2.0")) {
                            lvar_color2_cnt = lvar_color2_cnt + 1;
                        }
                        if (value.equalsIgnoreCase("3") || value.equalsIgnoreCase("3.0")) {
                            lvar_color3_cnt = lvar_color3_cnt + 1;
                        }
                    }
                    lvar_color_cnt_tot = lvar_color_cnt_tot + 1;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Object> str) {
            super.onPostExecute(str);

            double angka3 = (double)lvar_color3_cnt/(double)lvar_color_cnt_tot;
            double angka2 = (double)lvar_color2_cnt/(double)lvar_color_cnt_tot;
            double angka1 = (double)lvar_color1_cnt/(double)lvar_color_cnt_tot;

            double PercentageGreen = angka3 * 100;
            double PercentageYellow = angka2 * 100;
            double PercentageRed = angka1 * 100;
            float graphicHijau = (float)(angka3 * 100) / 100;
            float graphicKuning = (float)(angka2 * 100) / 100;
            float graphicMerah = (float)(angka1 * 100) / 100;

            LinearLayout.LayoutParams loparamsHijau = (LinearLayout.LayoutParams) Hijau_10_value.getLayoutParams();
            loparamsHijau.weight = graphicHijau;
            Hijau_10_value.setLayoutParams(loparamsHijau);
            LinearLayout.LayoutParams loparamsKuning = (LinearLayout.LayoutParams) Kuning_10_value.getLayoutParams();
            loparamsKuning.weight = graphicKuning;
            Kuning_10_value.setLayoutParams(loparamsKuning);
            LinearLayout.LayoutParams loparamsMerah = (LinearLayout.LayoutParams) Merah_10_value.getLayoutParams();
            loparamsMerah.weight = graphicMerah;
            Merah_10_value.setLayoutParams(loparamsMerah);
            Merah_10_value.setText(String.valueOf(Math.round(PercentageRed*100)/100));
            Kuning_10_value.setText(String.valueOf(Math.round(PercentageYellow*100)/100));
            Hijau_10_value.setText(String.valueOf(Math.round(PercentageGreen*100)/100));
            Total_Surveyed_10.setText(String.valueOf(lvar_color_cnt_tot));
            PanelLoading.setVisibility(View.GONE);
            MenuBottom.setVisibility(View.VISIBLE);
        }
    }


    private String HitungJumlahSKB1(){
        String result = null;
        String[] a = new String[4];
        a[0] = "3";
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "2";

        String query = "SELECT * FROM tblt_blockcondition a inner join " +
                "(select block, skb, max(zdate) as zdate from tblT_BlockCondition " +
                "where translevel = ? " +
                "group by block, skb) b " +
                "on a.block=b.block and a.skb=b.skb and a.zdate=b.zdate " +
                "where  a.zyear=? AND  a.period=? AND a.translevel = ? " +
                "and a.TPHBersih != 0 and (a.TPHBersih <> 0) or (a.TPHBersih <> null)";
        int Total = database.getTaskCount(query,a);

        result = String.valueOf(Total);

        return result;
    }

    private String HitungJumlahSKBTPH1(){
        String result = null;
        String[] a = new String[4];
        a[0] = "3";
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "2";

        String query = "SELECT * FROM tblt_blockcondition a inner join " +
                "(select block, skb, max(zdate) as zdate from tblT_BlockCondition " +
                "where translevel = ? " +
                "group by block, skb) b " +
                "on a.block=b.block and a.skb=b.skb and a.zdate=b.zdate " +
                "where  a.zyear=? AND  a.period=? AND a.translevel = ? " +
                "and a.TPH != 0 and (a.TPH <> 0) or (a.TPH <> null)";
        int Total = database.getTaskCount(query,a);

        result = String.valueOf(Total);

        return result;
    }

    private String HitungJumlahSKB2(){
        String result = null;
         String[] a = new String[4];
        a[0] = "3";
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "2";

        String query = "SELECT * FROM tblt_blockcondition a inner join " +
                "(select block, skb, max(zdate) as zdate from tblT_BlockCondition " +
                "where translevel = ? " +
                "group by block, skb) b " +
                "on a.block=b.block and a.skb=b.skb and a.zdate=b.zdate " +
                "where  a.zyear=? AND  a.period=? AND a.translevel = ? " +
                "and a.TPHBersih2 != 0 and (a.TPHBersih2 <> 0) or (a.TPHBersih2 <> null)";
        int Total = database.getTaskCount(query,a);

        result = String.valueOf(Total);

        return result;
    }

    private String HitungJumlahSKBTPH2(){
        String result = null;
         String[] a = new String[4];
        a[0] = "3";
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "2";

        String query = "SELECT * FROM tblt_blockcondition a inner join " +
                "(select block, skb, max(zdate) as zdate from tblT_BlockCondition " +
                "where translevel = ? " +
                "group by block, skb) b " +
                "on a.block=b.block and a.skb=b.skb and a.zdate=b.zdate " +
                "where  a.zyear=? AND  a.period=? AND a.translevel = ? " +
                "and a.TPH2 != 0 and (a.TPH2 <> 0) or (a.TPH2 <> null)";
        int Total = database.getTaskCount(query,a);

        result = String.valueOf(Total);

        return result;
    }

    private String HitungJumlahPiringan(){
        String result = null;
        String[] a = new String[4];
        String query = null;
        a[0] = "3";
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "3";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'M%' " +
                    "AND  a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.Piringan != 0 and ((a.Piringan > 0) or (a.Piringan != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
       if(fase.equalsIgnoreCase("IMMATURE")){
           query = "SELECT * FROM tblt_blockcondition a inner join " +
                   "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                   "where translevel = ? " +
                   "group by block, skb, blockrow, censuspoint ) b " +
                   "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                   "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'T%' " +
                   "AND   a.zyear=? AND  a.period=? AND a.translevel = ? " +
                   "and ((a.PokokCondition = 'Normal' and a.Piringan != 0 and ((a.Piringan > 0) or (a.Piringan != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
       }
        int Total = database.getTaskCount(query,a);

        result = String.valueOf(Total);

        return result;
    }

    private String HitungJumlahPasarPanen(){
        String result = null;
         String[] a = new String[4];
        String query = null;
        a[0] = "3";
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "3";

        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'M%' " +
                    "AND  a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.PasarPanen != 0 and ((a.PasarPanen > 0) or (a.PasarPanen != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'T%' " +
                    "AND   a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.PasarPanen != 0 and ((a.PasarPanen > 0) or (a.PasarPanen != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        int Total = database.getTaskCount(query,a);

        result = String.valueOf(Total);

        return result;
    }

    private String HitungJumlahPasarRintis(){
        String result = null;
        String[] a = new String[4];
        String query = null;
        a[0] = "3";
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "3";

        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'M%' " +
                    "AND  a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.PasarRintis != 0 and ((a.PasarRintis > 0) or (a.PasarRintis != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'T%' " +
                    "AND   a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.PasarRintis != 0 and ((a.PasarRintis > 0) or (a.PasarRintis != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        int Total = database.getTaskCount(query,a);

        result = String.valueOf(Total);

        return result;
    }

    private String HitungJumlahTunasPokok(){
        String result = null;
        String[] a = new String[4];
        String query = null;
        a[0] = "3";
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "3";

        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'M%' " +
                    "AND  a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.TunasPokok != 0 and ((a.TunasPokok > 0) or (a.TunasPokok != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'T%' " +
                    "AND   a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.TunasPokok != 0 and ((a.TunasPokok > 0) or (a.TunasPokok != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        int Total = database.getTaskCount(query,a);

        result = String.valueOf(Total);

        return result;
    }

    private String HitungJumlahSanitasi(){
        String result = null;
         String[] a = new String[4];
        String query = null;
        a[0] = "3";
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "3";

        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'M%' " +
                    "AND  a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.Sanitasi != 0 and ((a.Sanitasi > 0) or (a.Sanitasi != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'T%' " +
                    "AND   a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.Sanitasi != 0 and ((a.Sanitasi > 0) or (a.Sanitasi != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        int Total = database.getTaskCount(query,a);

        result = String.valueOf(Total);

        return result;
    }

    private String HitungJumlahGawangan(){
        String result = null;
        String[] a = new String[4];
        String query = null;
        a[0] = "3";
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "3";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'M%' " +
                    "AND  a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.Gawangan != 0 and ((a.Gawangan > 0) or (a.Gawangan != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'T%' " +
                    "AND   a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.Gawangan != 0 and ((a.Gawangan > 0) or (a.Gawangan != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }

        int Total = database.getTaskCount(query,a);

        result = String.valueOf(Total);

        return result;
    }

    private String HitungJumlahDrainase(){
        String result = null;
        String[] a = new String[4];
        String query = null;
        a[0] = "3";
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "3";
        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'M%' " +
                    "AND  a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.Drainase != 0 and ((a.Drainase > 0) or (a.Drainase != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'T%' " +
                    "AND   a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.Drainase != 0 and ((a.Drainase > 0) or (a.Drainase != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }

        int Total = database.getTaskCount(query,a);

        result = String.valueOf(Total);

        return result;
    }

    private String HitungJumlahGanoderma(){
        String result = null;
        String[] a = new String[4];
        String query = null;
        a[0] = "3";
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "3";

        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'M%' " +
                    "AND  a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.Ganoderma != 0 and ((a.Ganoderma > 0) or (a.Ganoderma != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'T%' " +
                    "AND   a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.Ganoderma != 0 and ((a.Ganoderma > 0) or (a.Ganoderma != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        int Total = database.getTaskCount(query,a);

        result = String.valueOf(Total);

        return result;
    }

    private String HitungJumlahKacangan(){
        String result = null;
        String[] a = new String[4];
        String query = null;
        a[0] = "3";
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "3";

        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'M%' " +
                    "AND  a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.Kacangan != 0 and ((a.Kacangan > 0) or (a.Kacangan != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'T%' " +
                    "AND   a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.Kacangan != 0 and ((a.Kacangan > 0) or (a.Kacangan != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        int Total = database.getTaskCount(query,a);

        result = String.valueOf(Total);

        return result;
    }

    private String HitungJumlahRayap(){
        String result = null;
        String[] a = new String[4];
        String query = null;
        a[0] = "3";
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "3";

        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'M%' " +
                    "AND  a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.Rayap != 0 and ((a.Rayap > 0) or (a.Rayap != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'T%' " +
                    "AND   a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.Rayap != 0 and ((a.Rayap > 0) or (a.Rayap != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }

        int Total = database.getTaskCount(query,a);

        result = String.valueOf(Total);

        return result;
    }

    private String HitungJumlahOrcytes(){
        String result = null;
        String[] a = new String[4];
        String query = null;
        a[0] = "3";
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "3";

        if(fase.equalsIgnoreCase("MATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'M%' " +
                    "AND  a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.Orcytes != 0 and ((a.Orcytes > 0) or (a.Orcytes != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }
        if(fase.equalsIgnoreCase("IMMATURE")){
            query = "SELECT * FROM tblt_blockcondition a inner join " +
                    "(select block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition " +
                    "where translevel = ? " +
                    "group by block, skb, blockrow, censuspoint ) b " +
                    "on a.block=b.block and a.skb=b.skb and a.blockrow=b.blockrow and a.censuspoint=b.censuspoint and a.zdate=b.zdate " +
                    "inner join BLOCK_HDRC c ON c.block = a.block where translevel = 3 AND project LIKE 'T%' " +
                    "AND   a.zyear=? AND  a.period=? AND a.translevel = ? " +
                    "and ((a.PokokCondition = 'Normal' and a.Orcytes != 0 and ((a.Orcytes > 0) or (a.Orcytes != 0.0))) OR (a.PokokCondition in ('Mati','Kosong')))";
        }

        int Total = database.getTaskCount(query,a);

        result = String.valueOf(Total);

        return result;
    }


}
