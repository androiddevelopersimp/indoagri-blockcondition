package co.indoagri.blockcondition.fragment;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.dialog.DialogConfirmation;
import co.indoagri.blockcondition.listener.DialogConfirmationListener;
import co.indoagri.blockcondition.model.Data.BJR;
import co.indoagri.blockcondition.model.Data.BlockHdrc;
import co.indoagri.blockcondition.model.Data.tblM_BlockConditionScore;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.model.Data.tblT_BlockConditionScore;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.Users.UserLogin;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.widget.ToastMessage;

import static co.indoagri.blockcondition.activity.BlockActivity.SESS_PERIOD;
import static co.indoagri.blockcondition.activity.BlockActivity.SESS_ZYEAR;
import static co.indoagri.blockcondition.activity.BlockActivity.titleFragment;

@SuppressWarnings("unchecked")
public class LevelSKBFragment extends Fragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener{
    View view;
    Fragment fragment;
    FragmentLevelSKBListener fragmentLevelSKBListener;
    private static final String PHASE = "Phase";
    String block = null;
    String phase=null;
    TextView txtCodeBlock,txtSampleTitle,txtCodePhase;
    private Button btnNext,btnBack;
    private RadioGroup rgpBlock1,rgpBlock2;
    private RadioButton rbnblockred1,rbnblockgreen1,rbnblockyellow1;
    private RadioButton rbnblockred2,rbnblockgreen2,rbnblockyellow2;
    AutoCompleteTextView editSearchSKB;
    String TPHBERSIH= "0";
    String TPHBERSIH2= "0";
    String TPH= "0";
    String TPH2= "0";
    DatabaseHandler database;
    BlockHdrc blkDetail;
    String LastDate;
    static String companyCode;
    static String estate;
    static String SESS_SKB = null;
    View SKBEDIT;
    EditText editSKBNumber;
    BJR bjrField;

    String MaXDate = null;
    int lval_color1_val = 0;
    int lval_color2_val = 0;
    int lval_color3_val= 0;
    int lvar_color_cnt_tot = 0;
    int lvar_color1_calc  = 0;
    int lvar_color2_calc = 0;
    int lvar_color3_calc = 0;
    double lvar_color_calc_avg = 0;
    int lvar_color_calc_tot = 0;
    int lvar_final_score = 0;

    boolean First = false;
    EditText editRemark;
    String SESS_ZDATEFragment;
    public LevelSKBFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

        if(phase.equalsIgnoreCase("MATURE")){
            if(checkedId == R.id.rb_green1){
                TPHBERSIH="3";
            }else if(checkedId == R.id.rb_red1){
                TPHBERSIH="1";
            }else if(checkedId == R.id.rb_yellow1){
                TPHBERSIH="2";
            }
            else if(checkedId == R.id.rb_green2){
                TPHBERSIH2="3";
            }else if(checkedId == R.id.rb_red2){
                TPHBERSIH2="1";
            }else if(checkedId == R.id.rb_yellow2){
                TPHBERSIH2="2";
            }
        }else{
            if(checkedId == R.id.rb_green1){
                TPH="3";
            }else if(checkedId == R.id.rb_red1){
                TPH="1";
            }else if(checkedId == R.id.rb_yellow1){
                TPH="2";
            }else if(checkedId == R.id.rb_green2){
                TPH2="3";
            }else if(checkedId == R.id.rb_red2){
                TPH2="1";
            }else if(checkedId == R.id.rb_yellow2){
                TPH2="2";
            }
        }

    }

    public interface FragmentLevelSKBListener {
        void onInputLevelSKB(String input,String block,String phase,String SKB);
    }

    public static LevelSKBFragment newInstance(String input,String Block, String Phase) {
        LevelSKBFragment fragment = new LevelSKBFragment();
        Bundle args = new Bundle();
        args.putString(BlockHdrc.XML_BLOCK, Block);
        args.putString(PHASE, Phase);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        database = new DatabaseHandler(LevelSKBFragment.this.getContext());
        Bundle args = getArguments();
        if (args != null) {
            block = getArguments().getString(BlockHdrc.XML_BLOCK);
            phase = getArguments().getString(PHASE);
            if(phase.equalsIgnoreCase("MATURE")){
                view = inflater.inflate(R.layout.fragment_level_skb_mature, container, false);
            }else{
                view = inflater.inflate(R.layout.fragment_level_skb_immature, container, false);
            }
        }
        return  view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null)
            block = getArguments().getString(BlockHdrc.XML_BLOCK);
            phase = getArguments().getString(PHASE);
        if (getActivity() instanceof FragmentLevelSKBListener)
            fragmentLevelSKBListener = (FragmentLevelSKBListener) getActivity();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnBack = (Button)view.findViewById(R.id.btnBack);
        btnNext = (Button)view.findViewById(R.id.btnNext);
        txtCodeBlock = (TextView)view.findViewById(R.id.txtCodeBlock);
        txtCodePhase = (TextView)view.findViewById(R.id.txtCodePhase);
        rgpBlock1 = (RadioGroup) view.findViewById(R.id.rb_group1);
        rbnblockgreen1 = (RadioButton) view.findViewById(R.id.rb_green1);
        rbnblockred1 = (RadioButton) view.findViewById(R.id.rb_red1);
        rbnblockyellow1 = (RadioButton) view.findViewById(R.id.rb_yellow1);
        rgpBlock2 = (RadioGroup) view.findViewById(R.id.rb_group2);
        rbnblockgreen2 = (RadioButton) view.findViewById(R.id.rb_green2);
        rbnblockred2 = (RadioButton) view.findViewById(R.id.rb_red2);
        rbnblockyellow2 = (RadioButton) view.findViewById(R.id.rb_yellow2);
        editSearchSKB= (AutoCompleteTextView) view.findViewById(R.id.editSearchSKB);
        txtSampleTitle = (TextView)view.findViewById(R.id.txtSampleTitle);
        SKBEDIT = (View)view.findViewById(R.id.viewEdit);
        editSKBNumber = (EditText)view.findViewById(R.id.editSKBNumber);
        rgpBlock1.setOnCheckedChangeListener(this);
        rgpBlock2.setOnCheckedChangeListener(this);
        btnNext.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        editRemark = (EditText)view.findViewById(R.id.editRemark);
        SESS_ZDATEFragment = new PreferenceManager(getActivity(), Constants.shared_name).getDatein();
        rbnblockgreen1.setOnTouchListener(rb1);
        rbnblockyellow1.setOnTouchListener(rb1);
        rbnblockred1.setOnTouchListener(rb1);

        rbnblockgreen2.setOnTouchListener(rb2);
        rbnblockyellow2.setOnTouchListener(rb2);
        rbnblockred2.setOnTouchListener(rb2);

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentLevelSKBListener) {
            fragmentLevelSKBListener = (FragmentLevelSKBListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentLevelBlockListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentLevelSKBListener= null;
    }

    @Override
    public void onStart() {
        super.onStart();
        btnBack.setText(getResources().getString(R.string.blocks));
        btnNext.setText(getResources().getString(R.string.pokok));
        if(First){
            SKBEDIT.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.VISIBLE);

        }else{
            SKBEDIT.setVisibility(View.GONE);
            btnNext.setVisibility(View.GONE);
            First=true;
        }

        initView();
    }
    void initView(){

        txtCodeBlock.setText(block);
        txtCodePhase.setText(phase);
        titleFragment.setText("Condition of SKB");
        DetailBlock();
        companyCode = blkDetail.getCompanyCode();
        estate = blkDetail.getEstate();
        initAutoCompleteSKB();
    }
    private void initAutoCompleteSKB(){
        ArrayAdapter adapter = null;
        String[] SKBSTRING = null;
        database.openTransaction();
        String[] a = new String[4];
        a[0] = SESS_ZYEAR;
        a[1] = SESS_PERIOD;
        a[2] = block;
        a[3] = "2";
        String query = "SELECT DISTINCT SKB  FROM  tblT_BlockCondition " +
                "WHERE ZYear=? " +
                "AND Period=? " +
                "AND Block = ? " +
                "AND TransLevel = ?";
        tblT_BlockCondition cari = (tblT_BlockCondition) database.getDataFirstRaw(query,tblT_BlockCondition.TABLE_NAME,a);

        if(cari==null){
            editSearchSKB.setHint("SKB No");
            editSearchSKB.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
                        SESS_SKB = editSearchSKB.getText().toString();
                        DisplayDialogNew();
                        rgpBlock1.clearCheck();
                        rgpBlock2.clearCheck();
                        SKBEDIT.setVisibility(View.VISIBLE);
                        editSKBNumber.setText(SESS_SKB);
                        handled = true;
                    }
                    return handled;
                }
            });
        }
        else{
            SKBSTRING = database.SelectSKB(block);
            database.closeTransaction();
            if(SKBSTRING!=null){
                adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, SKBSTRING);
                editSearchSKB.setAdapter(adapter);
                editSearchSKB.setThreshold(1);
                editSearchSKB.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        database.closeTransaction();
                        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
                        SESS_SKB = editSearchSKB.getText().toString();
                        rgpBlock1.clearCheck();
                        rgpBlock2.clearCheck();
                        ONENTERSEARCH();
                        btnNext.setVisibility(View.VISIBLE);
                        SKBEDIT.setVisibility(View.VISIBLE);

                        editSKBNumber.setText(SESS_SKB);
                    }
                });
                editSearchSKB.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        boolean handled = false;
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
                            SESS_SKB = editSearchSKB.getText().toString();
                            ONENTERSEARCH();
                            btnNext.setVisibility(View.VISIBLE);
                            SKBEDIT.setVisibility(View.VISIBLE);
                            editSKBNumber.setText(SESS_SKB);
                            handled = true;
                        }
                        return handled;
                    }
                });
            }



        }

    }


    void GetBJR(){
        bjrField = (BJR) database.getDataFirst(false, BJR.TABLE_NAME, null,
                BlockHdrc.XML_BLOCK + "=? ",
                new String [] {blkDetail.getBlock()},
                null, null, BJR.XMl_EFF_DATE+" DESC", null);
        database.closeTransaction();
    }

    void DetailBlock(){
        String blocks = block;
        database.openTransaction();
        blkDetail = (BlockHdrc) database.getDataFirst(false, BlockHdrc.TABLE_NAME, null,
                BlockHdrc.XML_BLOCK + "=? ",
                new String [] {blocks},
                BlockHdrc.XML_BLOCK, null, BlockHdrc.XML_BLOCK, null);
        GetBJR();
        }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                database.closeTransaction();
                if (fragmentLevelSKBListener != null)
                    fragmentLevelSKBListener.onInputLevelSKB(getResources().getString(R.string.back),block,phase,SESS_SKB);
                break;
            case R.id.btnNext:
                saveData();
                break;
            default:
                break;
        }
    }

    void ONENTERSEARCH(){
        database.closeTransaction();
        database.openTransaction();
        SESS_SKB = editSearchSKB.getText().toString();
        String[] a = new String[5];
        a[0] = SESS_ZYEAR;
        a[1] = SESS_PERIOD;
        a[2] = block;
        a[3] = "2";
        a[4] = SESS_SKB;
        String query = " SELECT MAX(ZDate) as ZDate FROM tblT_BlockCondition " +
                "WHERE ZYear=? " +
                "AND Period=? " +
                "AND Block = ? " +
                "AND TransLevel = ? " +
                "AND SKB=?";
        tblT_BlockCondition lastDate = (tblT_BlockCondition) database.getDataFirstRaw(query,tblT_BlockCondition.TABLE_NAME,a);

        if(lastDate.getAcc_ZDate()!=null){
            LastDate = lastDate.getAcc_ZDate();
            String[] b = new String[6];
            b[0] = SESS_ZYEAR;
            b[1] = SESS_PERIOD;
            b[2] = block;
            b[3] = "2";
            b[4] = SESS_SKB;
            b[5] = lastDate.getAcc_ZDate();
            String dtQuery = " SELECT TPH,TPH2,TPHBersih,TPHBersih2,Remark FROM  tblT_BlockCondition " +
                    "WHERE ZYear=? " +
                    "AND Period=? " +
                    "AND Block = ? " +
                    "AND TransLevel = ? "  +
                    "AND SKB=? " +
                    "AND ZDate = ? LIMIT 1";
            tblT_BlockCondition dtsource = (tblT_BlockCondition) database.getDataFirstRaw(dtQuery,tblT_BlockCondition.TABLE_NAME,b);
            lastDateNotNull(dtsource);
            database.closeTransaction();
        }
        else{
            SESS_SKB = editSearchSKB.getText().toString();
            rgpBlock1.clearCheck();
            rgpBlock2.clearCheck();
            DisplayDialogNew();
        }
    }

    void DisplayDialogNew(){
        new DialogConfirmation(getActivity(), getResources().getString(R.string.dialogSKB) +" "+SESS_SKB+" ?",
                getResources().getString(R.string.data_save), null, new DialogConfirmationListener() {
            @Override
            public void onConfirmOK(Object object, int id) {
                btnNext.setVisibility(View.VISIBLE);
                rgpBlock1.clearCheck();
                rgpBlock2.clearCheck();
            }

            @Override
            public void onConfirmCancel(Object object, int id) {
                btnNext.setVisibility(View.GONE);
                rgpBlock1.clearCheck();
                rgpBlock2.clearCheck();
            }
        }).show();
    }
    private void lastDateNotNull(tblT_BlockCondition dtSource){
        if(phase.equalsIgnoreCase("IMMATURE")){
            if(dtSource.getAcc_TPH()!=null){
                TPH = dtSource.getAcc_TPH();
              if(TPH.equals("3")){
                  rbnblockgreen1.setChecked(true);
              }
              if(TPH.equals("1")){
                  rbnblockred1.setChecked(true);
              }
              if(TPH.equals("2")){
                  rbnblockyellow1.setChecked(true);
              }
          }
            if(dtSource.getAcc_TPH2()!=null){
                TPH2 = dtSource.getAcc_TPH2();
                if(TPH2.equals("3")){
                    rbnblockgreen2.setChecked(true);
                }
                if(TPH2.equals("1")){
                    rbnblockred2.setChecked(true);
                }
                if(TPH2.equals("2")){
                    rbnblockyellow2.setChecked(true);
                }
            }
            if(dtSource.getAcc_Remark()!=null){
                editRemark.setText(dtSource.getAcc_Remark());
            }
        }
       else {
            if(dtSource.getAcc_TPHBersih()!=null){
                TPHBERSIH = dtSource.getAcc_TPHBersih();
                if(TPHBERSIH.equals("3")){
                    rbnblockgreen1.setChecked(true);
                }
                if(TPHBERSIH.equals("1")){
                    rbnblockred1.setChecked(true);
                }
                if(TPHBERSIH.equals("2")){
                    rbnblockyellow1.setChecked(true);
                }
            }
            if(dtSource.getAcc_TPHBersih2()!=null){
                TPHBERSIH2 = dtSource.getAcc_TPHBersih2();
                if(TPHBERSIH2.equals("3")){
                    rbnblockgreen2.setChecked(true);
                }
                if(TPHBERSIH2.equals("1")){
                    rbnblockred2.setChecked(true);
                }
                if(TPHBERSIH2.equals("2")){
                    rbnblockyellow2.setChecked(true);
                }
            }
            if(dtSource.getAcc_Remark()!=null){
                editRemark.setText(dtSource.getAcc_Remark());
            }
        }
    }


    void saveData(){
        String todayDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT);
        database.closeTransaction();
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        int deleted = database.deleteData(tblT_BlockCondition.TABLE_NAME,
                tblT_BlockCondition.XML_ZYear+ "=? AND "+
                        tblT_BlockCondition.XML_Period+ "=? AND "+
                        tblT_BlockCondition.XML_ZDate+ "=? AND "+
                        tblT_BlockCondition.XML_Block+ "=? AND "+
                        tblT_BlockCondition.XML_TransLevel+ "=? AND "+
                        tblT_BlockCondition.XML_SKB+ "=? ",
                new String [] {SESS_ZYEAR,SESS_PERIOD,SESS_ZDATEFragment,block,"2",SESS_SKB});
        if(deleted>0){
            if(phase.equalsIgnoreCase("MATURE")){
                if(TPHBERSIH.equalsIgnoreCase("0") || TPHBERSIH2.equalsIgnoreCase("0")){
               /* ToastMessage toastMessage = new ToastMessage(getActivity());
                toastMessage.shortMessage("Harapa Beri Penilaian");*/
                    MatureSave(todayDate, userLogin);
                }
                else {
                    if(editSearchSKB.getText().toString() != editSKBNumber.getText().toString()){
                        long rest ;
                        rest = UpdateSKBNumber(companyCode, estate,
                                SESS_ZYEAR, SESS_PERIOD, SESS_ZDATEFragment,
                                "2", blkDetail.getBlock(), SESS_SKB,editSKBNumber.getText().toString());
                        if(rest>0){
                            MatureSave(todayDate, userLogin);
                        }
                    }
                    else{
                        MatureSave(todayDate, userLogin);
                    }

                }
            }else{
                if(TPH.equalsIgnoreCase("0") || TPH2.equalsIgnoreCase("0")){
              /*  ToastMessage toastMessage = new ToastMessage(getActivity());
                toastMessage.shortMessage("Harap Beri Penilaian");*/
                    IMMatureSave(todayDate,userLogin);
                }else {
                    if(editSearchSKB.getText().toString() != editSKBNumber.getText().toString()){
                        long rest ;
                        rest = UpdateSKBNumber(companyCode, estate,
                                SESS_ZYEAR, SESS_PERIOD, SESS_ZDATEFragment,
                                "2", blkDetail.getBlock(), SESS_SKB,editSKBNumber.getText().toString());
                        if(rest>0){
                            IMMatureSave(todayDate,userLogin);
                        }
                    }
                    else{
                        IMMatureSave(todayDate,userLogin);
                    }

                }
            }
        }else{
            if(phase.equalsIgnoreCase("MATURE")){
                if(TPHBERSIH.equalsIgnoreCase("0") || TPHBERSIH2.equalsIgnoreCase("0")){
               /* ToastMessage toastMessage = new ToastMessage(getActivity());
                toastMessage.shortMessage("Harapa Beri Penilaian");*/
                    MatureSave(todayDate, userLogin);
                }
                else {
                    if(editSearchSKB.getText().toString() != editSKBNumber.getText().toString()){
                        long rest ;
                        rest = UpdateSKBNumber(companyCode, estate,
                                SESS_ZYEAR, SESS_PERIOD, SESS_ZDATEFragment,
                                "2", blkDetail.getBlock(), SESS_SKB,editSKBNumber.getText().toString());
                        if(rest>0){
                            MatureSave(todayDate, userLogin);
                        }
                    }
                    else{
                        MatureSave(todayDate, userLogin);
                    }

                }
            }else{
                if(TPH.equalsIgnoreCase("0") || TPH2.equalsIgnoreCase("0")){
              /*  ToastMessage toastMessage = new ToastMessage(getActivity());
                toastMessage.shortMessage("Harap Beri Penilaian");*/
                    IMMatureSave(todayDate,userLogin);
                }else {
                    if(editSearchSKB.getText().toString() != editSKBNumber.getText().toString()){
                        long rest ;
                        rest = UpdateSKBNumber(companyCode, estate,
                                SESS_ZYEAR, SESS_PERIOD, SESS_ZDATEFragment,
                                "2", blkDetail.getBlock(), SESS_SKB,editSKBNumber.getText().toString());
                        if(rest>0){
                            IMMatureSave(todayDate,userLogin);
                        }
                    }
                    else{
                        IMMatureSave(todayDate,userLogin);
                    }

                }
            }
        }

    }

    void MatureSave(String todayDate,UserLogin userLogin){
        database.closeTransaction();
        database.openTransaction();
        long  RowID = database.setData(new tblT_BlockCondition(0,companyCode, estate,
                    SESS_ZYEAR, SESS_PERIOD, SESS_ZDATEFragment,
                    2, blkDetail.getBlock(), SESS_SKB,
                    null, null,
                    null, null,
                    null, null,
                    null, null,
                    null, null,
                    null, null,
                    null, TPHBERSIH,
                    TPHBERSIH2,null,
                    null,null,null,
                    null,null,
                    null,null,
                    null, null,
                    null, null,
                    null,todayDate,
                    userLogin.getNik(),todayDate, userLogin.getNik(),null,userLogin.getDivision(),editRemark.getText().toString()));
            database.commitTransaction();
            if(RowID>0){
                database.closeTransaction();
                HitungScoreMature();
            }
            else{
                database.closeTransaction();
                ToastMessage toastMessage = new ToastMessage(getActivity());
                toastMessage.shortMessage("TIDAK DAPAT DI SETS");
            }
    }



    void IMMatureSave(String todayDate,UserLogin userLogin){
        database.closeTransaction();
        database.openTransaction();
       long RowID = database.setData(new tblT_BlockCondition(0,companyCode, estate,
                SESS_ZYEAR, SESS_PERIOD, SESS_ZDATEFragment,
                2, blkDetail.getBlock(), SESS_SKB,
                null, null,
                null, null,
                null, null,
                null, null,
                null, null,
                null, null,
                null, null,
                null,TPH,
                TPH2,null,
                null,null,
                null,
                null,null,
                null, null,
                null, null,
                null,todayDate,
                userLogin.getNik(),todayDate, userLogin.getNik(),null,userLogin.getDivision(),editRemark.getText().toString()));
        database.commitTransaction();
        if(RowID>0){
            database.closeTransaction();
            HitungScoreMature();
        }
        else{
            database.closeTransaction();
            ToastMessage toastMessage = new ToastMessage(getActivity());
            toastMessage.shortMessage("TIDAK DAPAT DI SETS");
        }

    }

    private void HitungScoreMature(){
        MaximumDateMature();
    }

    public String MaximumDateMature(){
        database.closeTransaction();
        database.openTransaction();
        String[] a = new String[4];
        List<Object> listObject;
        a[0] = SESS_ZYEAR;
        a[1] = SESS_PERIOD;
        a[2] = block;
        a[3] = "2";
        String query = "SELECT a.SKB,a.TPH,a.TPH2,a.TPHBersih,a.TPHBersih2 " +
                "FROM tblT_BlockCondition a " +
                "INNER JOIN " +
                "  (SELECT block, skb, max(zdate) as zdate from tblT_BlockCondition WHERE translevel = 2 " +
                "  GROUP BY block, skb ) b " +
                "  ON a.block=b.block and a.skb=b.skb  " +
                "  AND a.zdate=b.zdate " +
                "WHERE a.ZYear=? AND a.Period=? " +
                "AND a.Block=? AND a.TransLevel = ? " +
                "GROUP BY a.SKB,a.TPH,a.TPH2,a.TPHBersih,a.TPHBersih2 ";
        String queryList = "SELECT a.SKB,a.TPH,a.TPH2,a.TPHBersih,a.TPHBersih2 " +
                "FROM tblT_BlockCondition a " +
                "INNER JOIN " +
                "  (SELECT block, skb, max(zdate) as zdate from tblT_BlockCondition WHERE translevel = 2 " +
                "  GROUP BY block, skb ) b " +
                "  ON a.block=b.block and a.skb=b.skb " +
                "  AND a.zdate=b.zdate " +
                "WHERE a.ZYear=? AND a.Period=? " +
                "AND a.Block=? AND a.TransLevel = ? " +
                "GROUP BY a.SKB,a.TPH,a.TPH2,a.TPHBersih,a.TPHBersih2 ";
        tblT_BlockCondition dtSource3= (tblT_BlockCondition) database.getDataFirstRaw(query,tblT_BlockCondition.TABLE_NAME,a);
        if(dtSource3==null){
            //MaXDate = null;
        }else{
            //MaXDate = dtSource3.getAcc_ZDate();
            ArrayList<String> data = new ArrayList<String>();
            data.add(dtSource3.getAcc_TPHBersih());
            data.add(dtSource3.getAcc_TPHBersih2());
            listObject= database.getListDataRawQuery(queryList,tblT_BlockCondition.TABLE_NAME,a);
            HitungBobotDariWarna(dtSource3,listObject);
        }
        return  MaXDate;
    }

    private void HitungBobotDariWarna(tblT_BlockCondition dtSource,  List<Object> listObject){
        int color1 = (int)ColorVal_1(dtSource);
        int color2 = (int)ColorVal_2(dtSource);
        int color3 = (int)ColorVal_3(dtSource);
        int Delete = DeleteTransScore(dtSource);
        int TotalScore ;
        List<Object> data = listObject;
        List<String> arrFields =  new ArrayList<String>();

        arrFields.add(tblT_BlockCondition.XML_TPH);
        arrFields.add(tblT_BlockCondition.XML_TPH2);
        arrFields.add(tblT_BlockCondition.XML_TPHBersih);
        arrFields.add(tblT_BlockCondition.XML_TPHBersih2);
        String[] namesArr = new String[arrFields.size()];
        if(Delete>0) {
            for (int i = 0; i < arrFields.size(); i++) {
                lval_color1_val = 0;
                lval_color2_val = 0;
                lval_color3_val= 0;
                lvar_color_cnt_tot = 0;
                lvar_color1_calc  = 0;
                lvar_color2_calc = 0;
                lvar_color3_calc = 0;
                lvar_color_calc_avg = 0;
                lvar_color_calc_tot = 0;
                lvar_final_score = 0;
                namesArr[i] = arrFields.get(i);
                int lvar_color1_cnt = 0;
                int lvar_color2_cnt = 0;
                int lvar_color3_cnt = 0;
                for (int dt = 0; dt < data.size(); dt++) {
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) data.get(dt);
                    if (namesArr[i].equals(tblT_BlockCondition.XML_TPH)) {
                        if(dt3.getAcc_TPH()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_TPH().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_TPH().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_TPH().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }
                    if (namesArr[i].equals(tblT_BlockCondition.XML_TPH2)) {
                        if(dt3.getAcc_TPH2()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_TPH2().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_TPH2().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_TPH2().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }
                    if (namesArr[i].equals(tblT_BlockCondition.XML_TPHBersih)) {
                        if(dt3.getAcc_TPHBersih()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_TPHBersih().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_TPHBersih().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_TPHBersih().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }
                    if (namesArr[i].equals(tblT_BlockCondition.XML_TPHBersih2)) {
                        if(dt3.getAcc_TPHBersih2()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_TPHBersih2().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_TPHBersih2().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_TPHBersih2().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }
                }
                if(lvar_color1_cnt>0 || lvar_color2_cnt>0 || lvar_color3_cnt>0){
                    lvar_color_cnt_tot = lvar_color1_cnt + lvar_color2_cnt + lvar_color3_cnt;
                    lvar_color1_calc = lvar_color1_cnt * color1;
                    lvar_color2_calc = lvar_color2_cnt * color2;
                    lvar_color3_calc = lvar_color3_cnt * color3;
                    lvar_color_calc_tot = lvar_color1_calc + lvar_color2_calc + lvar_color3_calc;
                    double a = lvar_color_calc_tot;
                    double b = lvar_color_cnt_tot;
                    double total = a/b;
                    double roundOff = Math.round(total*100)/100.00;
                    lvar_color_calc_avg = roundOff;
                    TotalScore = (int) finalScore(lvar_color_calc_avg);
                    insertScoreMature(lvar_color1_calc, lvar_color2_calc, lvar_color3_calc,namesArr[i]);
                    UpdateScoreMATURELevelKosong(companyCode, estate, blkDetail.getDivisi(), phase, TotalScore,namesArr[i]);
                }if(lvar_color1_cnt==0 && lvar_color2_cnt==0 && lvar_color3_cnt==0){
                    TotalScore = (int) finalScore(0);
                    insertScoreMature(0, 0, 0,namesArr[i]);
                    UpdateScoreMATURELevelKosong(companyCode, estate, blkDetail.getDivisi(), phase, 0,namesArr[i]);
                }

            }
            database.commitTransaction();
            database.closeTransaction();
            if (fragmentLevelSKBListener != null)
                fragmentLevelSKBListener.onInputLevelSKB(getResources().getString(R.string.next),block,phase,SESS_SKB);
        }
        else{
            for (int i = 0; i < arrFields.size(); i++) {
                lval_color1_val = 0;
                lval_color2_val = 0;
                lval_color3_val= 0;
                lvar_color_cnt_tot = 0;
                lvar_color1_calc  = 0;
                lvar_color2_calc = 0;
                lvar_color3_calc = 0;
                lvar_color_calc_avg = 0;
                lvar_color_calc_tot = 0;
                lvar_final_score = 0;
                namesArr[i] = arrFields.get(i);
                int lvar_color1_cnt = 0;
                int lvar_color2_cnt = 0;
                int lvar_color3_cnt = 0;
                for (int dt = 0; dt < data.size(); dt++) {
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) data.get(dt);
                    if (namesArr[i].equals(tblT_BlockCondition.XML_TPH)) {
                        if(dt3.getAcc_TPH()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_TPH().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_TPH().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_TPH().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }
                    if (namesArr[i].equals(tblT_BlockCondition.XML_TPH2)) {
                        if(dt3.getAcc_TPH2()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_TPH2().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_TPH2().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_TPH2().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }
                    if (namesArr[i].equals(tblT_BlockCondition.XML_TPHBersih)) {
                        if(dt3.getAcc_TPHBersih()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_TPHBersih().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_TPHBersih().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_TPHBersih().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }
                    if (namesArr[i].equals(tblT_BlockCondition.XML_TPHBersih2)) {
                        if(dt3.getAcc_TPHBersih2()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_TPHBersih2().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_TPHBersih2().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_TPHBersih2().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }
                }
                if(lvar_color1_cnt>0 || lvar_color2_cnt>0 || lvar_color3_cnt>0){
                    lvar_color_cnt_tot = lvar_color1_cnt + lvar_color2_cnt + lvar_color3_cnt;
                    lvar_color1_calc = lvar_color1_cnt * color1;
                    lvar_color2_calc = lvar_color2_cnt * color2;
                    lvar_color3_calc = lvar_color3_cnt * color3;
                    lvar_color_calc_tot = lvar_color1_calc + lvar_color2_calc + lvar_color3_calc;
//                    lvar_color_calc_avg = Math.round((lvar_color_calc_tot / lvar_color_cnt_tot) * 100 / 100);
                    double a = lvar_color_calc_tot;
                    double b = lvar_color_cnt_tot;
                    double total = a/b;
                    double roundOff = Math.round(total*100)/100.00;
                    lvar_color_calc_avg = roundOff;
                    TotalScore = (int) finalScore(lvar_color_calc_avg);
                    insertScoreMature(lvar_color1_calc, lvar_color2_calc, lvar_color3_calc,namesArr[i]);
                    UpdateScoreMATURELevelKosong(companyCode, estate, blkDetail.getDivisi(), phase, TotalScore,namesArr[i]);
                }if(lvar_color1_cnt==0 && lvar_color2_cnt==0 && lvar_color3_cnt==0){
                    TotalScore = (int) finalScore(0);
                    insertScoreMature(0, 0, 0,namesArr[i]);
                    UpdateScoreMATURELevelKosong(companyCode, estate, blkDetail.getDivisi(), phase, 0,namesArr[i]);
                }

            }
            database.commitTransaction();
            database.closeTransaction();
            if (fragmentLevelSKBListener != null)
                fragmentLevelSKBListener.onInputLevelSKB(getResources().getString(R.string.next),block,phase,SESS_SKB);
        }

    }

    private double ColorVal_1(tblT_BlockCondition dtSource){
        double color1Val;
        String[] a = new String[2];
        a[0] = SESS_ZDATEFragment;
        a[1] = SESS_ZDATEFragment;
        String query = "SELECT Score1 FROM tblM_BlockConditionScore WHERE ScoreType=1 AND " +
                "Color = 1 AND  ValidFrom <= ? AND ValidTo >= ?";
        tblM_BlockConditionScore color1 = (tblM_BlockConditionScore) database.getDataFirstRaw(query, tblM_BlockConditionScore.TABLE_NAME, a);
        if(color1 == null){
            color1Val =0;
            lval_color1_val =0;
        }
        else{
            color1Val = color1.getAcc_Score1();
            lval_color1_val = (int)color1.getAcc_Score1();
        }
        return color1Val;
    }

    private double ColorVal_2(tblT_BlockCondition dtSource) {
        double color2Val;
        String[] b = new String[2];
        b[0] = SESS_ZDATEFragment;
        b[1] = SESS_ZDATEFragment;
        String query2 = "SELECT Score1 FROM tblM_BlockConditionScore WHERE ScoreType=1 AND " +
                "Color = 2 AND  ValidFrom <= ? AND ValidTo >= ?";
        tblM_BlockConditionScore color2 = (tblM_BlockConditionScore) database.getDataFirstRaw(query2, tblM_BlockConditionScore.TABLE_NAME, b);
        if(color2 == null){
            color2Val =0;
            lval_color2_val =0;
        }
        else{
            color2Val = color2.getAcc_Score1();
            lval_color2_val = (int)color2.getAcc_Score1();
        }
        return color2Val;
    }
    private double ColorVal_3(tblT_BlockCondition dtSource) {
        double color3Val;
        String[] c = new String[2];
        c[0] = SESS_ZDATEFragment;
        c[1] = SESS_ZDATEFragment;
        String query3 = "SELECT Score1 FROM tblM_BlockConditionScore WHERE ScoreType=1 AND " +
                "Color = 3 AND  ValidFrom <= ? AND ValidTo >= ?";
        tblM_BlockConditionScore color3 = (tblM_BlockConditionScore) database.getDataFirstRaw(query3, tblM_BlockConditionScore.TABLE_NAME, c);
        if(color3 == null){
            color3Val =0;
            lval_color3_val =0;
        }
        else{
            color3Val = color3.getAcc_Score1();
            lval_color3_val = (int)color3.getAcc_Score1();
        }
        return color3Val;
    }

    private int DeleteTransScore(tblT_BlockCondition dtSource){
        int Delete;
        Delete = database.deleteData(tblT_BlockConditionScore.TABLE_NAME,
                tblT_BlockCondition.XML_ZYear+ "=? AND "+
                        tblT_BlockCondition.XML_Period+ "=? AND "+
                        tblT_BlockCondition.XML_Block+ "=? AND "+
                        tblT_BlockCondition.XML_TransLevel+ "=? ",
                new String [] {SESS_ZYEAR,SESS_PERIOD,block,"2"});
        return Delete;
    }

    private double finalScore(double lvar_color_calc_avg) {
        double finalScore;
        String[] c = new String[4];
        c[0] = "2";
        c[1] = SESS_ZDATEFragment;
        c[2] = SESS_ZDATEFragment;
        c[3] = String.valueOf(lvar_color_calc_avg);
        String query3 = "SELECT Score1 FROM tblM_BlockConditionScore WHERE ScoreType=?" +
                "AND  ValidFrom <= ? AND ValidTo >= ?" +
                "AND ?  BETWEEN Interval1 AND Interval2";
        tblM_BlockConditionScore finalscore = (tblM_BlockConditionScore) database.getDataFirstRaw(query3, tblM_BlockConditionScore.TABLE_NAME, c);
        if(finalscore == null){
            finalScore =0;
            lvar_final_score = 0;
        }
        else{
            finalScore = finalscore.getAcc_Score1();
            lvar_final_score = (int)finalscore.getAcc_Score1();
        };
        return  finalScore;
    }

    private void insertScoreMature(double lvar_color1_calc,double  lvar_color2_calc,double lvar_color3_calc, String field){
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        String todayDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT);
        ContentValues values = new ContentValues();
        values.put(tblT_BlockConditionScore.XML_CompanyCode,companyCode);
        values.put(tblT_BlockConditionScore.XML_Estate,estate);
        values.put(tblT_BlockConditionScore.XML_ZYear,SESS_ZYEAR);
        values.put(tblT_BlockConditionScore.XML_Period,SESS_PERIOD);
        values.put(tblT_BlockConditionScore.XML_Block,block);
        values.put(tblT_BlockConditionScore.XML_TransLevel,"3");
        values.put(tblT_BlockConditionScore.XML_Color,"1");
        values.put(field,lvar_color1_calc);
        database.insertDataSQL(tblT_BlockConditionScore.TABLE_NAME,values);
        ContentValues values2 = new ContentValues();
        values2.put(tblT_BlockConditionScore.XML_CompanyCode,companyCode);
        values2.put(tblT_BlockConditionScore.XML_Estate,estate);
        values2.put(tblT_BlockConditionScore.XML_ZYear,SESS_ZYEAR);
        values2.put(tblT_BlockConditionScore.XML_Period,SESS_PERIOD);
        values2.put(tblT_BlockConditionScore.XML_Block,block);
        values2.put(tblT_BlockConditionScore.XML_TransLevel,"3");
        values2.put(tblT_BlockConditionScore.XML_Color,"2");
        values2.put(field,lvar_color2_calc);
        database.insertDataSQL(tblT_BlockConditionScore.TABLE_NAME,values2);
        ContentValues values3 = new ContentValues();
        values3.put(tblT_BlockConditionScore.XML_CompanyCode,companyCode);
        values3.put(tblT_BlockConditionScore.XML_Estate,estate);
        values3.put(tblT_BlockConditionScore.XML_ZYear,SESS_ZYEAR);
        values3.put(tblT_BlockConditionScore.XML_Period,SESS_PERIOD);
        values3.put(tblT_BlockConditionScore.XML_Block,block);
        values3.put(tblT_BlockConditionScore.XML_TransLevel,"3");
        values3.put(tblT_BlockConditionScore.XML_Color,"3");
        values3.put(field,lvar_color3_calc);
        database.insertDataSQL(tblT_BlockConditionScore.TABLE_NAME,values3);

    }
    private void UpdateScoreMATURELevelKosong(String companyCode,String estate,String divisi,String phase, double lvar_final_score, String field){
        database.updateDataSQL(tblT_BlockCondition.TABLE_NAME,
                "UPDATE tblT_BlockCondition" +
                        "     SET "+field+" = ?"+
                        "     WHERE ZYear=? " +
                        "     AND Period=? " +
                        "     AND Block=? AND TransLevel = ? ",
                new Object [] {String.valueOf(lvar_final_score),SESS_ZYEAR,SESS_PERIOD,block,"0"});
    }

    private long UpdateSKBNumber(String companyCode, String estate,
                                 String SESS_ZYEAR, String  SESS_PERIOD, String SESS_ZDATE,
                                 String Translevel, String block, String  SKBNUMBER,String skbChange){
        long  rest;
        SESS_SKB = skbChange;
        rest = database.updateDataSQL(tblT_BlockCondition.TABLE_NAME,
                "UPDATE tblT_BlockCondition" +
                        "     SET SKB = ? "+
                        "     WHERE SKB=? " +
                        "     AND CompanyCode=? " +
                        "     AND Estate=? " +
                        "     AND ZYear=? " +
                        "     AND Period=? " +
                        "     AND ZDate=? " +
                        "     AND Block=? " +
                        "     AND Remark=? " +
                        "     AND TransLevel = ? ",
                new Object [] {skbChange,SKBNUMBER,companyCode,estate,SESS_ZYEAR,SESS_PERIOD,SESS_ZDATE,block,editRemark.getText().toString(),"2"});
        database.commitTransaction();
        database.closeTransaction();
        return rest;
    }
    View.OnTouchListener rb1 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpBlock1.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };
    View.OnTouchListener rb2 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpBlock2.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

}
