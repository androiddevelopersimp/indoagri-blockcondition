package co.indoagri.blockcondition.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import co.indoagri.blockcondition.R;

public class ExportFragment extends Fragment implements View.OnClickListener {
    View view;

    FragmentExportListener fragmentExportListener;
    private static final String ARG_PARAM1 = "param1";

    private String mParam1;
    public ExportFragment() {
        // Required empty public constructor
    }


    public interface FragmentExportListener {
        void onInterfaceExport(String input);
    }

    public static ExportFragment newInstance() {
        return (new ExportFragment());

    }
    public static ExportFragment newInstance(String param1) {
        ExportFragment fragment = new ExportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_export, container, false);
        return  view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null)
            mParam1 = getArguments().getString(ARG_PARAM1);
        if (getActivity() instanceof FragmentExportListener)
            fragmentExportListener = (FragmentExportListener) getActivity();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    void setTextVoid(){
        String params1 = mParam1;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentExportListener) {
            fragmentExportListener = (FragmentExportListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentLevelBlockListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentExportListener = null;
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        this.getArguments().clear();
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            default:
                break;
        }
    }
}
