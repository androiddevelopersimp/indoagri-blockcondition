package co.indoagri.blockcondition.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;

public class AuthRestoreDatabaseFragment extends Fragment implements View.OnClickListener {


    private static final String ARG_PARAM1 = "param1";
    private String mParam1;
    View view;
    FragmentRestoreListener fragmentRestoreListener;
    DatabaseHandler database;
    public interface FragmentRestoreListener {
        void onInterfaceRestore(String input);
    }
    public AuthRestoreDatabaseFragment() {
        // Required empty public constructor
    }

    public static AuthRestoreDatabaseFragment newInstance(String param1) {
        AuthRestoreDatabaseFragment fragment = new AuthRestoreDatabaseFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_restore_database, container, false);
        database = new DatabaseHandler(AuthRestoreDatabaseFragment.this.getContext());
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null)
            mParam1 = getArguments().getString(ARG_PARAM1);
        if (getActivity() instanceof FragmentRestoreListener)
            fragmentRestoreListener = (FragmentRestoreListener) getActivity();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            default:
                break;
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentRestoreListener) {
            fragmentRestoreListener = (FragmentRestoreListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentLoginListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        fragmentRestoreListener = null;
    }
    @Override
    public void onStart() {
        super.onStart();

    }

}
