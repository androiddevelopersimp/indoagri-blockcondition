package co.indoagri.blockcondition.fragment;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.AdapterMasterDownload;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.dialog.DialogNotification;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.handler.FileXMLHandler;
import co.indoagri.blockcondition.handler.FolderHandler;
import co.indoagri.blockcondition.handler.ParsingHandler;
import co.indoagri.blockcondition.model.Data.MasterDownload;
import co.indoagri.blockcondition.model.FileXML;
import co.indoagri.blockcondition.model.MessageStatus;
import co.indoagri.blockcondition.routines.Constants;

import static co.indoagri.blockcondition.activity.AuthActivity.isUsbOtgAvailable;
import static co.indoagri.blockcondition.activity.AuthActivity.isUsbOtgPermit;

public class AuthMasterDownloadFragment extends Fragment implements View.OnClickListener{

    View view;
    private ListView lsvSettingsMasterDownload;
    private CheckBox cbxSettingsMasterDownUsbOtg;
    private Button btnSettingsMasterDownloadSync;
    private List<MasterDownload> lstMasterDownload;
    private AdapterMasterDownload adapter;
    TextView tv;
    private DialogProgress dialogProgress;
    FragmentMasterListener fragmentMasterListener;
    private static final String ARG_PARAM1 = Constants.PARAM1;
    private String mParam1;
    DatabaseHandler database;
    public AuthMasterDownloadFragment() {
        // Required empty public constructor
    }


    public interface FragmentMasterListener {
        void onInputMaster(CharSequence input);
    }

  public static AuthMasterDownloadFragment newInstance(String param1) {
      AuthMasterDownloadFragment fragment = new AuthMasterDownloadFragment();
      Bundle args = new Bundle();
      args.putString(ARG_PARAM1, param1);
      fragment.setArguments(args);
      return fragment;
  }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_masterdownload, container, false);
        database = new DatabaseHandler(AuthMasterDownloadFragment.this.getContext());
        return  view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null)
            mParam1 = getArguments().getString(ARG_PARAM1);
        if (getActivity() instanceof FragmentMasterListener)
            fragmentMasterListener = (FragmentMasterListener) getActivity();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lsvSettingsMasterDownload = (ListView) view.findViewById(R.id.lsvSettingsMasterDownload);
        cbxSettingsMasterDownUsbOtg = (CheckBox) view.findViewById(R.id.cbxSettingsMasterDownUsbOtg);
        btnSettingsMasterDownloadSync = (Button) view.findViewById(R.id.btnSettingsMasterDownloadSync);
        btnSettingsMasterDownloadSync.setOnClickListener(this);
        tv=(TextView)view.findViewById(R.id.txtNoteMasterDownload);
    }

    void setTextVoid(){
      String params1 = mParam1;
        tv.setText(getResources().getString(R.string.usb_otg_import));
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentMasterListener) {
            fragmentMasterListener = (FragmentMasterListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentAListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentMasterListener = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        setTextVoid();
        new GetDataAsyncTask().execute();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSettingsMasterDownloadSync:
               /* if (fragmentMasterListener != null)
                    fragmentMasterListener.onInputMaster(getResources().getString(R.string.settings_sync_master));*/
                boolean isUsbOtg = isUsbOtgAvailable && isUsbOtgPermit && cbxSettingsMasterDownUsbOtg.isChecked();

                FolderHandler folderHandler = new FolderHandler(getActivity());

                if(folderHandler.isSDCardWritable() && folderHandler.init()){
                    new ReadDataAsynctask(folderHandler, isUsbOtg).execute();
                }else{
                    new DialogNotification(getActivity(), getResources().getString(R.string.informasi),
                            getResources().getString(R.string.error_sd_card), true).show();
                }

                break;
            default:
                break;
        }
    }

    private class GetDataAsyncTask extends AsyncTask<Void, MasterDownload, List<MasterDownload>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!getActivity().isFinishing()){
                dialogProgress = new DialogProgress(getActivity(), getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }

        @Override
        protected List<MasterDownload> doInBackground(Void... params) {
            database.openTransaction();
            List<Object> lstObject = database.getListData(false, MasterDownload.TABLE_NAME, null,
                    null, null,null, null, MasterDownload.XML_NAME, null);
            database.closeTransaction();

            List<MasterDownload> lstMaster = new ArrayList<MasterDownload>();

            if(lstObject.size() > 0){
                for(int i = 0; i < lstObject.size(); i++){
                    MasterDownload md = (MasterDownload) lstObject.get(i);

                    lstMaster.add(md);
                }
            }

            return lstMaster;
        }

        @Override
        protected void onPostExecute(List<MasterDownload> lstTemp) {
            super.onPostExecute(lstTemp);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }


            lstMasterDownload = lstTemp;
            adapter = new AdapterMasterDownload(getActivity(), lstMasterDownload, R.layout.item_master_download);
            lsvSettingsMasterDownload.setAdapter(adapter);
        }
    }


    private class ReadDataAsynctask extends AsyncTask<Void, String, List<MessageStatus>>{
        FolderHandler folderHandler;
        boolean isUsbOtg;

        public ReadDataAsynctask(FolderHandler folderHandler, boolean isUsbOtg) {
            this.folderHandler = folderHandler;
            this.isUsbOtg = isUsbOtg;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!getActivity().isFinishing()){
                dialogProgress = new DialogProgress(getActivity(), getResources().getString(R.string.wait));
                dialogProgress.show();
            }
//
//			if(isUsbOtg){
//				Toast.makeText(SettingsMasterDownloadActivity.this, "ada", Toast.LENGTH_SHORT).show();
//			}else{
//				Toast.makeText(SettingsMasterDownloadActivity.this, "tidak ada", Toast.LENGTH_SHORT).show();
//			}
        }

        @Override
        protected List<MessageStatus> doInBackground(Void... voids) {
            FileXMLHandler fileMasterHandler = new FileXMLHandler(getActivity());
            List<FileXML> lstMaster;
            List<MessageStatus> lstMsgStatus = null;

            if(isUsbOtg){
                String sourceFolder = "/storage/UsbDriveA/" + FolderHandler.ROOT + "/" + FolderHandler.MASTER + "/" + FolderHandler.NEW;

                List<FileXML> lstFileOtg = fileMasterHandler.getFiles(sourceFolder, "", ".xml");

                try{
                    for(FileXML fileXml : lstFileOtg){
                        String fileNameSource = fileXml.getFileNew();

                        Uri uri = Uri.parse(fileNameSource);
                        String fileName = uri.getLastPathSegment();

                        String fileNameDest = folderHandler.getFileMasterNew() + "/"+ fileName;

                        FileInputStream fis = new FileInputStream(new File(fileNameSource));
                        FileOutputStream fos = new FileOutputStream(new File(fileNameDest));

                        fos.getChannel().transferFrom(fis.getChannel(), 0, fis.getChannel().size());
                        fis.close();
                        fos.close();

                        MediaScannerConnection.scanFile(getActivity(), new String[] {new File(fileNameDest).getAbsolutePath() }, null, null);

                        publishProgress(fileName);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                    publishProgress(e.getMessage());
                }
            }

            lstMaster = fileMasterHandler.getFiles(folderHandler.getFileMasterNew(), folderHandler.getFileMasterBackup(), ".xml");

            if(lstMaster.size() > 0){
                lstMsgStatus = new ParsingHandler(getActivity()).ParsingXML(lstMaster);
            }

            return lstMsgStatus;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(List<MessageStatus> lstMsgStatus) {
            super.onPostExecute(lstMsgStatus);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            if(lstMsgStatus != null && lstMsgStatus.size() > 0){
                String message = "";
                for(MessageStatus msg : lstMsgStatus){
                    if(msg != null) {
                        message = message + "\n" + msg.getMenu() + ": " + msg.getMessage();
                    }
                }

                if(!TextUtils.isEmpty(message))
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();


                new GetDataAsyncTask().execute();
            }
        }
    }

}
