package co.indoagri.blockcondition.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.indoagri.blockcondition.R;
public class NotificationFragment extends Fragment implements View.OnClickListener {
    View view;

    FragmentNotificationListener fragmentNotificationListener;
    private static final String ARG_PARAM1 = "param1";

    private String mParam1;
    public NotificationFragment() {
        // Required empty public constructor
    }


    public interface FragmentNotificationListener {
        void onInterfaceNotification(String input);
    }

    public static NotificationFragment newInstance() {
        return (new NotificationFragment());

    }
    public static NotificationFragment newInstance(String param1) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notification, container, false);
        return  view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null)
            mParam1 = getArguments().getString(ARG_PARAM1);
        if (getActivity() instanceof FragmentNotificationListener)
            fragmentNotificationListener = (FragmentNotificationListener) getActivity();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



    }
    void setTextVoid(){
        String params1 = mParam1;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentNotificationListener) {
            fragmentNotificationListener = (FragmentNotificationListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentLevelBlockListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentNotificationListener = null;
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        if (getArguments() != null)
            this.getArguments().clear();
    }
    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            default:
                break;
        }
    }
}
