package co.indoagri.blockcondition.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.activity.ViewProfile;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.listener.DialogNotificationListener;
import co.indoagri.blockcondition.model.Data.BJR;
import co.indoagri.blockcondition.model.Data.BLKPLT;
import co.indoagri.blockcondition.model.Data.BlockHdrc;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.Users.UserLogin;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.widget.ToastMessage;

import static co.indoagri.blockcondition.activity.BlockActivity.SESS_PERIOD;
import static co.indoagri.blockcondition.activity.BlockActivity.SESS_PERIOD_FROMDATE;
import static co.indoagri.blockcondition.activity.BlockActivity.SESS_ZYEAR;
import static co.indoagri.blockcondition.activity.BlockActivity.relContainerSubTitle;
import static co.indoagri.blockcondition.activity.BlockActivity.relContainerTitle;
import static java.sql.Types.NULL;


public class LevelCodeFragment extends Fragment implements View.OnClickListener,DialogNotificationListener, DatePickerDialog.OnDateSetListener {

    View view;
    FragmentLevelCodeListener fragmentLevelCodeListener;
    Button btnNext,btnBack;
    TextView tglPengamatan,txtAsistant,txtBlock,txtPhase,txtbjr,txtProdtrees,txtProduksiTM,txtProduksiYTD,txtProduksiAOPTM,txtProduksiAOPYTD,txtProduksiLASTYEAR;
    String companyCode = null;
    String estate= null;
    String division= null;
    String block = null;
    String phase=null;
    String ProdTM = null;
    String ProdYTD = null;
    String ProdAOPTM = null;
    String ProdAOPYTD = null;
    String ProdLASTYEAR = null;
    String SKB = "0";
    String BlockRow = "0";
    String CensusPoint = "0";
    String PokokLabel = "0";
    String PokokSide = "0";
    BlockHdrc blkDetail;
    BLKPLT blkplt;
    BJR bjrField;
    DatabaseHandler database;
    DialogProgress dialogProgress;
    String SESS_ZDATEFragment;
    public LevelCodeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onOK(boolean is_finish) {
        if (fragmentLevelCodeListener != null)
            fragmentLevelCodeListener.onInputLevelCode(getResources().getString(R.string.next),
                    blkDetail.getBlock(),txtPhase.getText().toString());
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String Smonth;
        String Syear;
        String Sday;
        Calendar cal=Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        if(month<10){
            Smonth = "0"+String.valueOf(month);
            Syear = String.valueOf(year);
            Sday = String.valueOf(dayOfMonth);
        }
        else{
            Smonth = String.valueOf(month);
            Syear = String.valueOf(year);
            Sday = String.valueOf(dayOfMonth);
        }
       // showSetDate(Syear,Smonth,Sday);
        String Dates = year + "-" +(month<10?("0"+month):(month)) + "-" + (dayOfMonth<10?("0"+Sday):(Sday));
        tglPengamatan.setText(Dates);
        new PreferenceManager(getActivity(), Constants.shared_name).setDatein(Dates);
    }


    public interface FragmentLevelCodeListener {
        void onInputLevelCode(String input, String Block, String phase);
    }
       public static LevelCodeFragment newInstance() {
        return (new LevelCodeFragment());

    }

    public static LevelCodeFragment newInstance(String companycode,
                                                      String Estate,
                                                      String Division,
                                                      String block) {
        LevelCodeFragment fragment = new LevelCodeFragment();
        Bundle args = new Bundle();
        args.putString(BlockHdrc.XML_COMPANY_CODE, companycode);
        args.putString(BlockHdrc.XML_ESTATE, Estate);
        args.putString(BlockHdrc.XML_DIVISION, Division);
        args.putString(BlockHdrc.XML_BLOCK, block);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        database = new DatabaseHandler(LevelCodeFragment.this.getContext());
        relContainerSubTitle.setVisibility(View.GONE);
        relContainerTitle.setVisibility(View.GONE);
        Bundle b=getArguments();
        if (b != null){
            companyCode = b.getString(BlockHdrc.XML_COMPANY_CODE);
            estate = b.getString(BlockHdrc.XML_ESTATE);
            division = b.getString(BlockHdrc.XML_DIVISION);
            block = b.getString(BlockHdrc.XML_BLOCK);
        }
        dialogProgress = new DialogProgress(getActivity());
        view = inflater.inflate(R.layout.fragment_level_code_home, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            companyCode = getArguments().getString(BlockHdrc.XML_COMPANY_CODE);
            estate = getArguments().getString(BlockHdrc.XML_ESTATE);
            division = getArguments().getString(BlockHdrc.XML_DIVISION);
            block = getArguments().getString(BlockHdrc.XML_BLOCK);
        }
        if (getActivity() instanceof FragmentLevelCodeListener)
            fragmentLevelCodeListener = (FragmentLevelCodeListener) getActivity();
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnNext = (Button)view.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        btnBack = (Button)view.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
        tglPengamatan = (TextView)view.findViewById(R.id.txtTanggalPengamatan);
        txtAsistant = (TextView)view.findViewById(R.id.txtAsistant);
        txtBlock = (TextView)view.findViewById(R.id.txtBlock);
        txtPhase = (TextView)view.findViewById(R.id.txtPhase);
        txtbjr = (TextView)view.findViewById(R.id.txtbjr);
        txtProdtrees = (TextView)view.findViewById(R.id.txtProdtrees);
        txtProduksiAOPTM = (TextView)view.findViewById(R.id.txtProduksiAOPTM);
        txtProduksiAOPYTD = (TextView)view.findViewById(R.id.txtProduksiAOPYTD);
        txtProduksiLASTYEAR = (TextView)view.findViewById(R.id.txtProduksiLASTYEAR);
        txtProduksiTM = (TextView)view.findViewById(R.id.txtProduksiTM);
        txtProduksiYTD = (TextView)view.findViewById(R.id.txtProduksiYTD);
        txtBlock.setOnClickListener(this);
        tglPengamatan.setOnClickListener(this);
        SESS_ZDATEFragment = new PreferenceManager(getActivity(),Constants.shared_name).getDatein();
    }
    @Override
    public void onDestroyView(){
        super.onDestroyView();
        this.getArguments().clear();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentLevelCodeListener) {
            fragmentLevelCodeListener = (FragmentLevelCodeListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentLoginListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentLevelCodeListener = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        btnNext.setText("BLOCK");
        btnBack.setText("HOME");
        btnBack.setVisibility(View.VISIBLE);
        InitView();
    }

    void InitView(){
        if(block!=null){
            database.openTransaction();
            try{
            blkDetail = (BlockHdrc) database.getDataFirst(false, BlockHdrc.TABLE_NAME, null,
                    BlockHdrc.XML_BLOCK + "=? ",
                    new String [] {block},
                    BlockHdrc.XML_BLOCK, null, BlockHdrc.XML_BLOCK, null);

            if(blkDetail.getProjectDefinition().substring(0,1).equalsIgnoreCase("M")){
                txtPhase.setText("MATURE");
                phase = "MATURE";
                bjrField = (BJR) database.getDataFirst(false, BJR.TABLE_NAME, null,
                        BlockHdrc.XML_BLOCK + "=? ",
                        new String [] {blkDetail.getBlock()},
                        null, null, BJR.XMl_EFF_DATE+" DESC", null);

                blkplt = (BLKPLT) database.getDataFirst(false, BLKPLT.TABLE_NAME, null,
                        BLKPLT.XML_BLOCK + "=? ",
                        new String [] {blkDetail.getBlock()},
                        BlockHdrc.XML_BLOCK, null, BLKPLT.XML_BLOCK, null);
                if(bjrField==null){
                    txtbjr.setText("0");
                }else{
                    int BJRINT = (int)bjrField.getBjr();
                    if(BJRINT==0 || BJRINT==NULL){
                        txtbjr.setText(String.valueOf(0));
                    }else{
                        txtbjr.setText(String.valueOf(bjrField.getBjr()));
                    }
                }

            }
            else{
                phase = "IMMATURE";
                txtPhase.setText("IMMATURE");
                blkplt = (BLKPLT) database.getDataFirst(false, BLKPLT.TABLE_NAME, null,
                        BLKPLT.XML_BLOCK + "=? ",
                        new String [] {blkDetail.getBlock()},
                        BlockHdrc.XML_BLOCK, null, BLKPLT.XML_BLOCK, null);
                bjrField = (BJR) database.getDataFirst(false, BJR.TABLE_NAME, null,
                        BlockHdrc.XML_BLOCK + "=? ",
                        new String [] {blkDetail.getBlock()},
                        null, null, BJR.XMl_EFF_DATE+" DESC", null);
                if(blkplt == null){
                    txtProdtrees.setText("0");
                }else{
                    int blkpltInt = (int)blkplt.getProdTrees();
                    if(blkpltInt==0 || blkpltInt==NULL){
                        txtProdtrees.setText("0");
                    }else{
                        txtProdtrees.setText(String.valueOf(blkplt.getProdTrees()));
                    }
                }
                if(bjrField==null){
                    txtbjr.setText("0");
                }else{
                    int BJRINT = (int)bjrField.getBjr();
                    if(BJRINT==0 || BJRINT==NULL){
                        txtbjr.setText(String.valueOf(0));
                    }else{
                        txtbjr.setText(String.valueOf(bjrField.getBjr()));
                    }
                }

            }


        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
        }
            txtBlock.setText(blkDetail.getBlock());
            txtProduksiYTD.setText(ProdYTD);
            txtProduksiTM.setText(ProdTM);
            txtProduksiLASTYEAR.setText(ProdLASTYEAR);
            txtProduksiAOPTM.setText(ProdAOPTM);
            txtProduksiAOPYTD.setText(ProdAOPYTD);
            tglPengamatan.setText(SESS_ZDATEFragment);
        }
        else{
            txtProduksiYTD.setText(getResources().getString(R.string.data_empty));
            txtProduksiTM.setText(getResources().getString(R.string.data_empty));
            txtProduksiLASTYEAR.setText(getResources().getString(R.string.data_empty));
            txtProduksiAOPTM.setText(getResources().getString(R.string.data_empty));
            txtProduksiAOPYTD.setText(getResources().getString(R.string.data_empty));
            txtPhase.setText(getResources().getString(R.string.data_empty));
            txtbjr.setText(getResources().getString(R.string.data_empty));
            txtProdtrees.setText(getResources().getString(R.string.data_empty));
            txtBlock.setText(getResources().getString(R.string.data_empty));
            tglPengamatan.setText(SESS_ZDATEFragment);
            if (ViewProfile.CheckLogin()) {
                txtAsistant.setText(ViewProfile.getUserName());
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                if (fragmentLevelCodeListener != null)
                    fragmentLevelCodeListener.onInputLevelCode(getResources().getString(R.string.nav_home),null,null);
                break;
            case R.id.btnNext:
                if(txtBlock.getText().toString().equalsIgnoreCase(getResources().getString(R.string.data_empty))){
                    ToastMessage toastMessage = new ToastMessage(getActivity());
                    toastMessage.shortMessage("Block Harap Dipilih");
                }
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String Now = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
                Date myDate = null;
                Date myDate2 = null;
                try {
                    myDate = dateFormat.parse(new PreferenceManager(getActivity(),Constants.shared_name).getDatein());
                    myDate2 = dateFormat.parse(Now);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(myDate);
                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(myDate2);
                Date newDate = calendar.getTime();
                Date newDate2 = calendar2.getTime();
                String DateSave = new PreferenceManager(getActivity(),Constants.shared_name).getDatein();
                long days =((newDate.getTime() - newDate2.getTime()) / (1000 * 60 * 60 * 24));
                if(days<-5 ){
                    ToastMessage toastMessage = new ToastMessage(getActivity());
                    toastMessage.shortMessage("Tanggal Melibihi 5 Hari ");
                }
                if(days>0 ){
                    ToastMessage toastMessage = new ToastMessage(getActivity());
                    toastMessage.shortMessage("Tanggal Melebihi 5 Hari ");
                } if(!txtBlock.getText().toString().equalsIgnoreCase(getResources().getString(R.string.data_empty)) && days>=-5){
                    Save();
//                    ToastMessage toastMessage = new ToastMessage(getActivity());
//                    toastMessage.shortMessage(Now +" "+DateSave);
                }
                break;
            case R.id.txtBlock:
                if (fragmentLevelCodeListener != null)
                    fragmentLevelCodeListener.onInputLevelCode(getResources().getString(R.string.blocks),null,null);
                break;
            case R.id.txtTanggalPengamatan:
                if (fragmentLevelCodeListener != null)
                    fragmentLevelCodeListener.onInputLevelCode(getResources().getString(R.string.m_date),null,null);
                DatePickerFragmentDialog pd = DatePickerFragmentDialog.newInstance(5,12, 1999);
               // DateMonthYearPickerDialog pd = new DateMonthYearPickerDialog();
                pd.setListener(this);
                pd.show(getFragmentManager(), "DateMonthYearPickerDialog");
                break;
            default:
                break;
        }
    }

    void Save() {
        String todayDate =  new DateLocal(new Date()).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT);
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        String[] a = new String[4];
        a[0] = blkDetail.getBlock();
        a[1] = SESS_ZYEAR;
        a[2] = SESS_PERIOD;
        a[3] = "0";
        String query = "SELECT * FROM "+tblT_BlockCondition.TABLE_NAME+" " +
                "WHERE  Block = ? AND ZYear = ? AND  Period = ? AND TransLevel = ? " +
                "LIMIT 1";
        tblT_BlockCondition dtsource1 = (tblT_BlockCondition) database.getDataFirstRaw(query,tblT_BlockCondition.TABLE_NAME,a);
        database.closeTransaction();
        if(dtsource1!= null){
            if (fragmentLevelCodeListener != null)
                fragmentLevelCodeListener.onInputLevelCode(getResources().getString(R.string.next),blkDetail.getBlock(),phase);
        }
        else{
            boolean res;
            if(phase.equalsIgnoreCase("MATURE")){
                res = saveMature(todayDate,userLogin);
                if(res){
                    if (fragmentLevelCodeListener != null)
                        fragmentLevelCodeListener.onInputLevelCode(getResources().getString(R.string.next),blkDetail.getBlock(),phase);
                }
                if(!res){
                    ToastMessage toastMessage = new ToastMessage(getActivity());
                    toastMessage.shortMessage(getResources().getString(R.string.save_failed));
                }
            }
            else{
                res=saveIMMature(todayDate,userLogin);
                if(res){
                    if (fragmentLevelCodeListener != null)
                        fragmentLevelCodeListener.onInputLevelCode(getResources().getString(R.string.next),blkDetail.getBlock(),phase);
                }
                if(!res){
                    ToastMessage toastMessage = new ToastMessage(getActivity());
                    toastMessage.shortMessage(getResources().getString(R.string.save_failed));
                }

            }
        }
    }

    private boolean saveMature(String todayDate,UserLogin userLogin){
        database.openTransaction();
        boolean res = false;
        long RowId = 0;
        try{
            RowId = database.setData(new tblT_BlockCondition(0,companyCode, estate,
                    SESS_ZYEAR, SESS_PERIOD, SESS_PERIOD_FROMDATE,
                    0, blkDetail.getBlock(), "0",
                    "0", "0",
                    "0", "0",
                    null, null,
                    null, null,
                    null, null,
                    null, null,
                    null, null,
                    null,null,
                    null,null,
                    null,
                    null,null,
                    null,null,
                    null, null,
                    null, null,
                    null,todayDate,
                    userLogin.getNik(),todayDate, userLogin.getNik(),null,division,null));
            database.commitTransaction();
            if(RowId>0){
                res = true;
            }
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
            res = false;
        }finally{
            database.closeTransaction();
            if(RowId>0){
                res = true;
            }
        }
        return res;
    }

    private boolean saveIMMature(String todayDate,UserLogin userLogin){
        database.openTransaction();
        boolean res = false;
        long RowId = 0;
        try{
            RowId = database.setData(new tblT_BlockCondition(0,companyCode, estate,
                    SESS_ZYEAR, SESS_PERIOD, SESS_PERIOD_FROMDATE,
                    0, blkDetail.getBlock(), "0",
                    "0", "0",
                    "0", "0",
                    null, null,
                    null, null,
                    null, null,
                    null, null,
                    null, null,
                    null,null,
                    null,null,
                    null,
                    null,null,
                    null,null,
                    null, null,
                    null, null,
                    null,todayDate,
                    userLogin.getNik(),todayDate, userLogin.getNik(),null,division,null));
            database.commitTransaction();
            if(RowId>0){
                res = true;
            }
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
            res = false;
        }finally{
            database.closeTransaction();
            if(RowId>0){
                res = true;
            }
        }
        return res;
    }



}
