package co.indoagri.blockcondition.fragment;


import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.dialog.DialogConfirmation;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.listener.DialogConfirmationListener;
import co.indoagri.blockcondition.model.Data.BJR;
import co.indoagri.blockcondition.model.Data.BlockHdrc;
import co.indoagri.blockcondition.model.Data.tblM_BlockConditionScore;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.model.Data.tblT_BlockConditionScore;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.Users.UserLogin;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.widget.ToastMessage;

import static co.indoagri.blockcondition.activity.BlockActivity.SESS_PERIOD;
import static co.indoagri.blockcondition.activity.BlockActivity.SESS_ZYEAR;
import static co.indoagri.blockcondition.activity.BlockActivity.titleFragment;
import static java.sql.Types.NULL;

public class LevelPokokFragment extends Fragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    View view;
    LinearLayout bariskiri,pokokkiri,kondisikiri,bariskanan,pokokkanan,kondisikanan;
    private RadioGroup rgpL1,rgpR1,rgpL2,rgpR2,rgpL3,rgpR3,rgpL4,rgpR4,rgpL5,rgpR5,rgpL6,rgpR6,rgpL7,rgpR7,rgpL8,rgpR8;
    private RadioButton rbnblockred1,rbnblockgreen1,rbnblockyellow1;
    private RadioButton rbnblockred2,rbnblockgreen2,rbnblockyellow2;
    private RadioButton rbnblockred3,rbnblockgreen3,rbnblockyellow3;
    private RadioButton rbnblockred4,rbnblockgreen4,rbnblockyellow4;
    private RadioButton rbnblockred5,rbnblockgreen5,rbnblockyellow5;
    private RadioButton rbnblockred6,rbnblockgreen6,rbnblockyellow6;
    private RadioButton rbnblockred7,rbnblockgreen7,rbnblockyellow7;
    private RadioButton rbnblockred8,rbnblockgreen8,rbnblockyellow8;

    private RadioButton rbnblockred1R,rbnblockgreen1R,rbnblockyellow1R;
    private RadioButton rbnblockred2R,rbnblockgreen2R,rbnblockyellow2R;
    private RadioButton rbnblockred3R,rbnblockgreen3R,rbnblockyellow3R;
    private RadioButton rbnblockred4R,rbnblockgreen4R,rbnblockyellow4R;
    private RadioButton rbnblockred5R,rbnblockgreen5R,rbnblockyellow5R;
    private RadioButton rbnblockred6R,rbnblockgreen6R,rbnblockyellow6R;
    private RadioButton rbnblockred7R,rbnblockgreen7R,rbnblockyellow7R;
    private RadioButton rbnblockred8R,rbnblockgreen8R,rbnblockyellow8R;
    View Line1;
    private DialogProgress dialogProgress;
    String Piringan_L = "0";
    String PasarPanen_L = "0";
    String TunasPokok_L = "0";
    String PasarRintis_L = "0";
    String Sanitasi_L = "0";
    String Kacangan_L = "0";
    String Gawangan_L = "0";
    String Drainase_L ="0";
    String Ganoderma_L = "0";
    String Rayap_L = "0";
    String Oryctes_L = "0";
    String Piringan_R = "0";
    String PasarPanen_R = "0";
    String TunasPokok_R = "0";
    String PasarRintis_R = "0";
    String Sanitasi_R = "0";
    String Kacangan_R = "0";
    String Gawangan_R = "0";
    String Drainase_R ="0";
    String Ganoderma_R = "0";
    String Rayap_R = "0";
    String Oryctes_R = "0";

    String Baris_L = "0";
    String PokokKe_L = "0";
    String Kondisi_L = null;
    String Baris_R = "0";
    String PokokKe_R = "0";
    String Kondisi_R = null;
    FragmentLevelPokokListener fragmentLevelPokokListener;
    private static final String PHASE = "Phase";
    String block = null;
    String phase=null;
    private Button btnNext,btnBack,btnRight,btnLeft;
    TextView txtSKBNumber,txtCodeBlock,txtCodePhase;
    EditText editTxtPokokLeft,editTxtPokokRight,editTxtBarisLeft,editTxtBarisRight;

    DatabaseHandler database;
    BlockHdrc blkDetail;
    String LastDate;
    static String companyCode;
    static String estate;
    static String division;
    static String type;
    static String status;
    static String project;
    static String SESS_SKB = null;
    BJR bjrField;
    ImageView flagBack,flagFront;
    String MaXDate = null;
    int lval_color1_val = 0;
    int lval_color2_val = 0;
    int lval_color3_val= 0;
    int lvar_color_cnt_tot = 0;
    int lvar_color1_calc  = 0;
    int lvar_color2_calc = 0;
    int lvar_color3_calc = 0;
    double lvar_color_calc_avg = 0;
    int lvar_color_calc_tot = 0;
    int lvar_final_score = 0;
    Spinner spinLeft,spinRight;
    /*List<String> kondisi;*/
    String Flag = null;
    ImageButton btnShow1,btnShow2;
    EditText editRemark1,editRemark2;
    String SESS_ZDATEFragment;
    public LevelPokokFragment() {
        // Required empty public constructor
    }

    public interface FragmentLevelPokokListener {
        void onInputLevelPokok(String input, String block, String phase,String skb);
    }

    public static LevelPokokFragment newInstance(String input, String Block,
                                                 String phase, String SKB) {
        LevelPokokFragment fragment = new LevelPokokFragment();
        Bundle args = new Bundle();
        args.putString(BlockHdrc.XML_BLOCK, Block);
        args.putString(PHASE, phase);
        args.putString(tblT_BlockCondition.XML_SKB, SKB);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        database = new DatabaseHandler(LevelPokokFragment.this.getContext());
        Bundle args = getArguments();
        if (args != null) {
            block = getArguments().getString(BlockHdrc.XML_BLOCK);
            phase = getArguments().getString(PHASE);
            SESS_SKB = getArguments().getString(tblT_BlockCondition.XML_SKB);
            if(phase.equalsIgnoreCase("MATURE")){
                view = inflater.inflate(R.layout.fragment_level_pokok_mature, container, false);
            }else{
                view = inflater.inflate(R.layout.fragment_level_pokok_immature, container, false);
            }
        }
        return  view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null)
            block = getArguments().getString(BlockHdrc.XML_BLOCK);
        phase = getArguments().getString(PHASE);
        if (getActivity() instanceof FragmentLevelPokokListener)
            fragmentLevelPokokListener = (FragmentLevelPokokListener) getActivity();
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SESS_ZDATEFragment = new PreferenceManager(getActivity(), Constants.shared_name).getDatein();
        spinLeft = (Spinner) view.findViewById(R.id.spinnerKondisiLeft);
        spinRight = (Spinner) view.findViewById(R.id.spinnerKondisiRight);
        btnBack = (Button)view.findViewById(R.id.btnBack);
        btnNext = (Button)view.findViewById(R.id.btnNext);
        btnLeft = (Button)view.findViewById(R.id.btnLeft);
        btnRight= (Button)view.findViewById(R.id.btnRight);
        flagFront = (ImageView)view.findViewById(R.id.flagfront);
        flagBack = (ImageView)view.findViewById(R.id.flagback);
        txtSKBNumber = (TextView)view.findViewById(R.id.txtSKBNumber);
        txtCodeBlock = (TextView)view.findViewById(R.id.txtCodeBlock);
        txtCodePhase = (TextView)view.findViewById(R.id.txtCodePhase);
        rgpL1 = (RadioGroup) view.findViewById(R.id.rb_groupL1);
        rgpL2= (RadioGroup) view.findViewById(R.id.rb_groupL2);
        rgpL3 = (RadioGroup) view.findViewById(R.id.rb_groupL3);
        rgpL4= (RadioGroup) view.findViewById(R.id.rb_groupL4);
        rgpL5 = (RadioGroup) view.findViewById(R.id.rb_groupL5);
        rgpL6= (RadioGroup) view.findViewById(R.id.rb_groupL6);
        rgpL7 = (RadioGroup) view.findViewById(R.id.rb_groupL7);
        rgpL8= (RadioGroup) view.findViewById(R.id.rb_groupL8);
        rgpR1 = (RadioGroup) view.findViewById(R.id.rb_groupR1);
        rgpR2= (RadioGroup) view.findViewById(R.id.rb_groupR2);
        rgpR3 = (RadioGroup) view.findViewById(R.id.rb_groupR3);
        rgpR4= (RadioGroup) view.findViewById(R.id.rb_groupR4);
        rgpR5 = (RadioGroup) view.findViewById(R.id.rb_groupR5);
        rgpR6= (RadioGroup) view.findViewById(R.id.rb_groupR6);
        rgpR7 = (RadioGroup) view.findViewById(R.id.rb_groupR7);
        rgpR8= (RadioGroup) view.findViewById(R.id.rb_groupR8);
        rgpL1.setOnCheckedChangeListener(this);
        rgpL2.setOnCheckedChangeListener(this);
        rgpL3.setOnCheckedChangeListener(this);
        rgpL4.setOnCheckedChangeListener(this);
        rgpL5.setOnCheckedChangeListener(this);
        rgpL6.setOnCheckedChangeListener(this);
        rgpL7.setOnCheckedChangeListener(this);
        rgpL8.setOnCheckedChangeListener(this);
        rgpR1.setOnCheckedChangeListener(this);
        rgpR2.setOnCheckedChangeListener(this);
        rgpR3.setOnCheckedChangeListener(this);
        rgpR4.setOnCheckedChangeListener(this);
        rgpR5.setOnCheckedChangeListener(this);
        rgpR6.setOnCheckedChangeListener(this);
        rgpR7.setOnCheckedChangeListener(this);
        rgpR8.setOnCheckedChangeListener(this);
        bariskiri = (LinearLayout)view.findViewById(R.id.barisKiri);
        pokokkiri = (LinearLayout)view.findViewById(R.id.pokokKiri);
        kondisikiri = (LinearLayout)view.findViewById(R.id.kondisiKiri);
        bariskanan = (LinearLayout)view.findViewById(R.id.barisKanan);
        pokokkanan = (LinearLayout)view.findViewById(R.id.pokokKanan);
        kondisikanan = (LinearLayout)view.findViewById(R.id.kondisiKanan);

        rbnblockgreen1 = (RadioButton) view.findViewById(R.id.rb_greenL1);
        rbnblockgreen2 = (RadioButton) view.findViewById(R.id.rb_greenL2);
        rbnblockgreen3 = (RadioButton) view.findViewById(R.id.rb_greenL3);
        rbnblockgreen4 = (RadioButton) view.findViewById(R.id.rb_greenL4);
        rbnblockgreen5 = (RadioButton) view.findViewById(R.id.rb_greenL5);
        rbnblockgreen6 = (RadioButton) view.findViewById(R.id.rb_greenL6);
        rbnblockgreen7 = (RadioButton) view.findViewById(R.id.rb_greenL7);
        rbnblockgreen8 = (RadioButton) view.findViewById(R.id.rb_greenL8);
        rbnblockred1 = (RadioButton) view.findViewById(R.id.rb_redL1);
        rbnblockred2 = (RadioButton) view.findViewById(R.id.rb_redL2);
        rbnblockred3 = (RadioButton) view.findViewById(R.id.rb_redL3);
        rbnblockred4 = (RadioButton) view.findViewById(R.id.rb_redL4);
        rbnblockred5 = (RadioButton) view.findViewById(R.id.rb_redL5);
        rbnblockred6 = (RadioButton) view.findViewById(R.id.rb_redL6);
        rbnblockred7 = (RadioButton) view.findViewById(R.id.rb_redL7);
        rbnblockred8 = (RadioButton) view.findViewById(R.id.rb_redL8);
        rbnblockyellow1 = (RadioButton) view.findViewById(R.id.rb_yellowL1);
        rbnblockyellow2 = (RadioButton) view.findViewById(R.id.rb_yellowL2);
        rbnblockyellow3 = (RadioButton) view.findViewById(R.id.rb_yellowL3);
        rbnblockyellow4 = (RadioButton) view.findViewById(R.id.rb_yellowL4);
        rbnblockyellow5 = (RadioButton) view.findViewById(R.id.rb_yellowL5);
        rbnblockyellow6 = (RadioButton) view.findViewById(R.id.rb_yellowL6);
        rbnblockyellow7 = (RadioButton) view.findViewById(R.id.rb_yellowL7);
        rbnblockyellow8 = (RadioButton) view.findViewById(R.id.rb_yellowL8);

        rbnblockgreen1R = (RadioButton) view.findViewById(R.id.rb_greenR1);
        rbnblockgreen2R = (RadioButton) view.findViewById(R.id.rb_greenR2);
        rbnblockgreen3R = (RadioButton) view.findViewById(R.id.rb_greenR3);
        rbnblockgreen4R = (RadioButton) view.findViewById(R.id.rb_greenR4);
        rbnblockgreen5R = (RadioButton) view.findViewById(R.id.rb_greenR5);
        rbnblockgreen6R = (RadioButton) view.findViewById(R.id.rb_greenR6);
        rbnblockgreen7R = (RadioButton) view.findViewById(R.id.rb_greenR7);
        rbnblockgreen8R = (RadioButton) view.findViewById(R.id.rb_greenR8);
        rbnblockred1R = (RadioButton) view.findViewById(R.id.rb_redR1);
        rbnblockred2R = (RadioButton) view.findViewById(R.id.rb_redR2);
        rbnblockred3R = (RadioButton) view.findViewById(R.id.rb_redR3);
        rbnblockred4R = (RadioButton) view.findViewById(R.id.rb_redR4);
        rbnblockred5R = (RadioButton) view.findViewById(R.id.rb_redR5);
        rbnblockred6R = (RadioButton) view.findViewById(R.id.rb_redR6);
        rbnblockred7R = (RadioButton) view.findViewById(R.id.rb_redR7);
        rbnblockred8R = (RadioButton) view.findViewById(R.id.rb_redR8);
        rbnblockyellow1R = (RadioButton) view.findViewById(R.id.rb_yellowR1);
        rbnblockyellow2R = (RadioButton) view.findViewById(R.id.rb_yellowR2);
        rbnblockyellow3R = (RadioButton) view.findViewById(R.id.rb_yellowR3);
        rbnblockyellow4R = (RadioButton) view.findViewById(R.id.rb_yellowR4);
        rbnblockyellow5R = (RadioButton) view.findViewById(R.id.rb_yellowR5);
        rbnblockyellow6R = (RadioButton) view.findViewById(R.id.rb_yellowR6);
        rbnblockyellow7R = (RadioButton) view.findViewById(R.id.rb_yellowR7);
        rbnblockyellow8R = (RadioButton) view.findViewById(R.id.rb_yellowR8);
        editTxtPokokLeft= (EditText)view.findViewById(R.id.editTxtPokokLeft);
        editTxtPokokRight= (EditText)view.findViewById(R.id.editTxtPokokRight);
        editTxtBarisLeft= (EditText)view.findViewById(R.id.editTxtBarisLeft);
        Line1 = (View)view.findViewById(R.id.line1);
        editTxtBarisLeft.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                // If user press done key
                if(i == EditorInfo.IME_ACTION_DONE){
                    // Get the input method manager
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    // Hide the soft keyboard
                    inputMethodManager.hideSoftInputFromWindow(editTxtBarisLeft.getWindowToken(),0);
                    Baris_L = editTxtBarisLeft.getText().toString();
                    int nilai1, nilai2;
                    if(editTxtBarisRight.getText().toString().length()==0){
                        new PreferenceManager(getActivity(),Constants.shared_name).setKondisipokokR("EMPTY");
                        setKosongSpinnerR();
                        setRBR("Kosong");
                    }
                    if(ischar(editTxtBarisLeft)||editTxtBarisLeft.getText().toString().length()==0){
                        new PreferenceManager(getActivity(),Constants.shared_name).setKondisipokokL("");
                        setNormalSpinnerL();
                        flagFrontON();
                    }else{
                        new PreferenceManager(getActivity(),Constants.shared_name).setKondisipokokL("");
                        setNormalSpinnerL();
                        if(isdigit(editTxtBarisLeft)){
                            if(editTxtBarisLeft.getText().toString().length()==0 || editTxtBarisRight.getText().toString().length()==0){
                                nilai1 = 0;
                                nilai2 = 0;
                                if(nilai1<nilai2){
                                    flagFrontON();
                                }
                                if(nilai1>nilai2){
                                    flagBackON();
                                }
                            }
                            else{
                                nilai1 = Integer.parseInt(editTxtBarisLeft.getText().toString());
                                nilai2 = Integer.parseInt(editTxtBarisRight.getText().toString());
                                if(nilai1<nilai2){
                                    flagFrontON();
                                }
                                if(nilai1>nilai2){
                                    flagBackON();
                                }
                            }

                        }
                    }
                    // Show the user inputted text in the snack bar
                    return true;
                }
                return false;
            }
        });
        editTxtBarisRight = (EditText)view.findViewById(R.id.editTxtBarisRight);
        editTxtBarisRight.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                // If user press done key
                if(i == EditorInfo.IME_ACTION_DONE){
                    // Get the input method manager
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    // Hide the soft keyboard
                    inputMethodManager.hideSoftInputFromWindow(editTxtBarisRight.getWindowToken(),0);
                    Baris_R = editTxtBarisRight.getText().toString();
                    int nilai1, nilai2;
                    if(editTxtBarisLeft.getText().toString().length()==0){
                        new PreferenceManager(getActivity(),Constants.shared_name).setKondisipokokL("EMPTY");
                        setKosongSpinnerL();
                        setRBL("Kosong");
                    }
                    if(ischar(editTxtBarisRight)||editTxtBarisRight.getText().toString().length()==0){
                        new PreferenceManager(getActivity(),Constants.shared_name).setKondisipokokR("");
                        setNormalSpinnerR();
                        flagFrontON();
                    }else{
                        new PreferenceManager(getActivity(),Constants.shared_name).setKondisipokokR("");
                        setNormalSpinnerR();
                        if(isdigit(editTxtBarisRight)){
                            if(editTxtBarisLeft.getText().toString().length()==0 || editTxtBarisRight.getText().toString().length()==0){
                                nilai2 = 0;
                                nilai1 = 0;
                                if(nilai1<nilai2){
                                    flagFrontON();
                                }
                                if(nilai1>nilai2){
                                    flagBackON();
                                }
                            }
                            else{
                                nilai1 = Integer.parseInt(editTxtBarisLeft.getText().toString());
                                nilai2 = Integer.parseInt(editTxtBarisRight.getText().toString());
                                if(nilai1<nilai2){
                                    flagFrontON();
                                }
                                if(nilai1>nilai2){
                                    flagBackON();
                                }
                            }
                        }
                    }
                    // Show the user inputted text in the snack bar
                    return true;
                }
                return false;
            }
        });
        btnLeft.setOnClickListener(this);
        btnRight.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        editRemark1 = (EditText)view.findViewById(R.id.editRemark1);
        editRemark2 = (EditText)view.findViewById(R.id.editRemark2);
        btnShow1 = (ImageButton)view.findViewById(R.id.btnShow1);
        btnShow2 = (ImageButton)view.findViewById(R.id.btnShow2);
        btnShow1.setOnClickListener(this);
        btnShow2.setOnClickListener(this);

        rbnblockgreen1.setOnTouchListener(rbL1);
        rbnblockyellow1.setOnTouchListener(rbL1);
        rbnblockred1.setOnTouchListener(rbL1);

        rbnblockgreen2.setOnTouchListener(rbL2);
        rbnblockyellow2.setOnTouchListener(rbL2);
        rbnblockred2.setOnTouchListener(rbL2);

        rbnblockgreen3.setOnTouchListener(rbL3);
        rbnblockyellow3.setOnTouchListener(rbL3);
        rbnblockred3.setOnTouchListener(rbL3);

        rbnblockgreen4.setOnTouchListener(rbL4);
        rbnblockyellow4.setOnTouchListener(rbL4);
        rbnblockred4.setOnTouchListener(rbL4);

        rbnblockgreen5.setOnTouchListener(rbL5);
        rbnblockyellow5.setOnTouchListener(rbL5);
        rbnblockred5.setOnTouchListener(rbL5);

        rbnblockgreen6.setOnTouchListener(rbL6);
        rbnblockyellow6.setOnTouchListener(rbL6);
        rbnblockred6.setOnTouchListener(rbL6);

        rbnblockgreen7.setOnTouchListener(rbL7);
        rbnblockyellow7.setOnTouchListener(rbL7);
        rbnblockred7.setOnTouchListener(rbL7);

        rbnblockgreen8.setOnTouchListener(rbL8);
        rbnblockyellow8.setOnTouchListener(rbL8);
        rbnblockred8.setOnTouchListener(rbL8);
//-------------------//
        rbnblockgreen1R.setOnTouchListener(rbR1);
        rbnblockyellow1R.setOnTouchListener(rbR1);
        rbnblockred1R.setOnTouchListener(rbR1);

        rbnblockgreen2R.setOnTouchListener(rbR2);
        rbnblockyellow2R.setOnTouchListener(rbR2);
        rbnblockred2R.setOnTouchListener(rbR2);

        rbnblockgreen3R.setOnTouchListener(rbR3);
        rbnblockyellow3R.setOnTouchListener(rbR3);
        rbnblockred3R.setOnTouchListener(rbR3);

        rbnblockgreen4R.setOnTouchListener(rbR4);
        rbnblockyellow4R.setOnTouchListener(rbR4);
        rbnblockred4R.setOnTouchListener(rbR4);

        rbnblockgreen5R.setOnTouchListener(rbR5);
        rbnblockyellow5R.setOnTouchListener(rbR5);
        rbnblockred5R.setOnTouchListener(rbR5);

        rbnblockgreen6R.setOnTouchListener(rbR6);
        rbnblockyellow6R.setOnTouchListener(rbR6);
        rbnblockred6R.setOnTouchListener(rbR6);

        rbnblockgreen7R.setOnTouchListener(rbR7);
        rbnblockyellow7R.setOnTouchListener(rbR7);
        rbnblockred7R.setOnTouchListener(rbR7);

        rbnblockgreen8R.setOnTouchListener(rbR8);
        rbnblockyellow8R.setOnTouchListener(rbR8);
        rbnblockred8R.setOnTouchListener(rbR8);

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentLevelPokokListener) {
            fragmentLevelPokokListener = (FragmentLevelPokokListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentLevelBlockListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentLevelPokokListener= null;
    }

    @Override
    public void onStart() {
        super.onStart();
        btnBack.setText(getResources().getString(R.string.skb));
        btnNext.setVisibility(View.GONE);
        initView();
    }
    void flagBackON(){
        flagBack.setVisibility(View.VISIBLE);
        flagFront.setVisibility(View.GONE);
        Flag = "back";
    }
    void flagFrontON(){
        Flag = "front";
        flagBack.setVisibility(View.GONE);
        flagFront.setVisibility(View.VISIBLE);
    }
    public static boolean isdigit(EditText input)
    {

        String data=input.getText().toString().trim();
        for(int i=0;i<data.length();i++)
        {
            if (!Character.isDigit(data.charAt(i)))
                return false;

        }
        return true;
    }

    public static boolean ischar(EditText input)
    {

        String data=input.getText().toString().trim();
        for(int i=0;i<data.length();i++)
        {
            if (!Character.isDigit(data.charAt(i)))
                return true;

        }
        return false;
    }
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if(phase.equalsIgnoreCase("MATURE")){
            if(checkedId == R.id.rb_greenL1){
                Piringan_L="3";
            }else if(checkedId == R.id.rb_greenL2){
                PasarPanen_L="3";
            }else if(checkedId == R.id.rb_greenL3){
                TunasPokok_L="3";
            }else if(checkedId == R.id.rb_greenL4){
                Gawangan_L="3";
            }else if(checkedId == R.id.rb_greenL5){
                Drainase_L="3";
            }else if(checkedId == R.id.rb_greenL6){
                Ganoderma_L="3";
            }else if(checkedId == R.id.rb_greenL7){
                Rayap_L="3";
            }else if(checkedId == R.id.rb_greenL8) {
                Oryctes_L = "3";
            } else if(checkedId == R.id.rb_greenR1){
                Piringan_R="3";
            }else if(checkedId == R.id.rb_greenR2){
                PasarPanen_R="3";
            }else if(checkedId == R.id.rb_greenR3){
                TunasPokok_R="3";
            }else if(checkedId == R.id.rb_greenR4){
                Gawangan_R="3";
            }else if(checkedId == R.id.rb_greenR5){
                Drainase_R="3";
            }else if(checkedId == R.id.rb_greenR6){
                Ganoderma_R="3";
            }else if(checkedId == R.id.rb_greenR7) {
                Rayap_R = "3";
            }else if(checkedId == R.id.rb_greenR8) {
                Oryctes_R = "3";
            }
            else if(checkedId == R.id.rb_yellowL1){
                Piringan_L="2";
            }else if(checkedId == R.id.rb_yellowL2){
                PasarPanen_L="2";
            }else if(checkedId == R.id.rb_yellowL3){
                TunasPokok_L="2";
            }else if(checkedId == R.id.rb_yellowL4){
                Gawangan_L="2";
            }else if(checkedId == R.id.rb_yellowL5){
                Drainase_L="2";
            }else if(checkedId == R.id.rb_yellowL6){
                Ganoderma_L="2";
            }else if(checkedId == R.id.rb_yellowL7){
                Rayap_L="2";
            }else if(checkedId == R.id.rb_yellowL8) {
                Oryctes_L = "2";
            } else if(checkedId == R.id.rb_yellowR1){
                Piringan_R="2";
            }else if(checkedId == R.id.rb_yellowR2){
                PasarPanen_R="2";
            }else if(checkedId == R.id.rb_yellowR3){
                TunasPokok_R="2";
            }else if(checkedId == R.id.rb_yellowR4){
                Gawangan_R="2";
            }else if(checkedId == R.id.rb_yellowR5){
                Drainase_R="2";
            }else if(checkedId == R.id.rb_yellowR6){
                Ganoderma_R="2";
            }else if(checkedId == R.id.rb_yellowR7) {
                Rayap_R = "2";
            }else if(checkedId == R.id.rb_yellowR8) {
                Oryctes_R = "2";
            }else if(checkedId == R.id.rb_redL1){
                Piringan_L="1";
            }else if(checkedId == R.id.rb_redL2){
                PasarPanen_L="1";
            }else if(checkedId == R.id.rb_redL3){
                TunasPokok_L="1";
            }else if(checkedId == R.id.rb_redL4){
                Gawangan_L="1";
            }else if(checkedId == R.id.rb_redL5){
                Drainase_L="1";
            }else if(checkedId == R.id.rb_redL6){
                Ganoderma_L="1";
            }else if(checkedId == R.id.rb_redL7){
                Rayap_L="1";
            }else if(checkedId == R.id.rb_redL8) {
                Oryctes_L = "1";
            } else if(checkedId == R.id.rb_redR1){
                Piringan_R="1";
            }else if(checkedId == R.id.rb_redR2){
                PasarPanen_R="1";
            }else if(checkedId == R.id.rb_redR3){
                TunasPokok_R="1";
            }else if(checkedId == R.id.rb_redR4){
                Gawangan_R="1";
            }else if(checkedId == R.id.rb_redR5){
                Drainase_R="1";
            }else if(checkedId == R.id.rb_redR6){
                Ganoderma_R="1";
            }else if(checkedId == R.id.rb_redR7) {
                Rayap_R = "1";
            }else if(checkedId == R.id.rb_redR8) {
                Oryctes_R = "1";
            }
        }
        if(phase.equalsIgnoreCase("IMMATURE")){
            if(checkedId == R.id.rb_greenL1){
                Piringan_L="3";
            }else if(checkedId == R.id.rb_greenL2){
                PasarRintis_L="3";
            }else if(checkedId == R.id.rb_greenL3){
                Sanitasi_L="3";
            }else if(checkedId == R.id.rb_greenL4){
                Gawangan_L="3";
            }else if(checkedId == R.id.rb_greenL5){
                Kacangan_L="3";
            }else if(checkedId == R.id.rb_greenL6){
                Drainase_L="3";
            }else if(checkedId == R.id.rb_greenL7){
                Rayap_L="3";
            }else if(checkedId == R.id.rb_greenL8) {
                Oryctes_L = "3";
            } else if(checkedId == R.id.rb_greenR1){
                Piringan_R="3";
            }else if(checkedId == R.id.rb_greenR2){
                PasarRintis_R="3";
            }else if(checkedId == R.id.rb_greenR3){
                Sanitasi_R="3";
            }else if(checkedId == R.id.rb_greenR4){
                Gawangan_R="3";
            }else if(checkedId == R.id.rb_greenR5){
                Kacangan_R="3";
            }else if(checkedId == R.id.rb_greenR6){
                Drainase_R="3";
            }else if(checkedId == R.id.rb_greenR7) {
                Rayap_R = "3";
            }else if(checkedId == R.id.rb_greenR8) {
                Oryctes_R = "3";
            }
            else if(checkedId == R.id.rb_yellowL1){
                Piringan_L="2";
            }else if(checkedId == R.id.rb_yellowL2){
                PasarRintis_L="2";
            }else if(checkedId == R.id.rb_yellowL3){
                Sanitasi_L="2";
            }else if(checkedId == R.id.rb_yellowL4){
                Gawangan_L="2";
            }else if(checkedId == R.id.rb_yellowL5){
                Kacangan_L="2";
            }else if(checkedId == R.id.rb_yellowL6){
                Drainase_L="2";
            }else if(checkedId == R.id.rb_yellowL7){
                Rayap_L="2";
            }else if(checkedId == R.id.rb_yellowL8) {
                Oryctes_L = "2";
            } else if(checkedId == R.id.rb_yellowR1){
                Piringan_R="2";
            }else if(checkedId == R.id.rb_yellowR2){
                PasarRintis_R="2";
            }else if(checkedId == R.id.rb_yellowR3){
                Sanitasi_R="2";
            }else if(checkedId == R.id.rb_yellowR4){
                Gawangan_R="2";
            }else if(checkedId == R.id.rb_yellowR5){
                Kacangan_R="2";
            }else if(checkedId == R.id.rb_yellowR6){
                Drainase_R="2";
            }else if(checkedId == R.id.rb_yellowR7) {
                Rayap_R = "2";
            }else if(checkedId == R.id.rb_yellowR8) {
                Oryctes_R = "2";
            }else if(checkedId == R.id.rb_redL1){
                Piringan_L="1";
            }else if(checkedId == R.id.rb_redL2){
                PasarRintis_L="1";
            }else if(checkedId == R.id.rb_redL3){
                Sanitasi_L="1";
            }else if(checkedId == R.id.rb_redL4){
                Gawangan_L="1";
            }else if(checkedId == R.id.rb_redL5){
                Kacangan_L="1";
            }else if(checkedId == R.id.rb_redL6){
                Drainase_L="1";
            }else if(checkedId == R.id.rb_redL7){
                Rayap_L="1";
            }else if(checkedId == R.id.rb_redL8) {
                Oryctes_L = "1";
            } else if(checkedId == R.id.rb_redR1){
                Piringan_R="1";
            }else if(checkedId == R.id.rb_redR2){
                PasarRintis_R="1";
            }else if(checkedId == R.id.rb_redR3){
                Sanitasi_R="1";
            }else if(checkedId == R.id.rb_redR4){
                Gawangan_R="1";
            }else if(checkedId == R.id.rb_redR5){
                Kacangan_R="1";
            }else if(checkedId == R.id.rb_redR6){
                Drainase_R="1";
            }else if(checkedId == R.id.rb_redR7) {
                Rayap_R = "1";
            }else if(checkedId == R.id.rb_redR8) {
                Oryctes_R = "1";
            }
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                if (fragmentLevelPokokListener != null)
                    fragmentLevelPokokListener.onInputLevelPokok(getResources().getString(R.string.skb),block,phase,SESS_SKB);
                break;
            case R.id.btnNext:
                if (fragmentLevelPokokListener != null)
                    fragmentLevelPokokListener.onInputLevelPokok(getResources().getString(R.string.pokok),block,phase,SESS_SKB);
                break;
            case R.id.btnShow1:
                DialogComment1();
                break;
            case R.id.btnShow2:
                DialogComment2();
            case R.id.btnLeft:
                setCek();
                if(Integer.parseInt(PokokKe_L)>1){
                    initDataSource1("Back");
                    initDataSource2("Back");
                }else{
                    Toast.makeText(getContext(), "Data Terakhir ", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.btnRight:
                initBtnRight();
                break;
            default:
                break;
        }
    }

    void initView(){
        editTxtPokokLeft.setEnabled(true);
        editTxtPokokRight.setEnabled(true);
        titleFragment.setText("Condition of Pokok");
        txtSKBNumber.setText(SESS_SKB);
        txtCodeBlock.setText(block);
        txtCodePhase.setText(phase);
        LastDate();
        DataSource1();
        DataSource2();

    }

    private void LastDate(){
        database.openTransaction();
        blkDetail = (BlockHdrc) database.getDataFirst(false, BlockHdrc.TABLE_NAME, null,
                BlockHdrc.XML_BLOCK + "=? ",
                new String [] {block},
                BlockHdrc.XML_BLOCK, null, BlockHdrc.XML_BLOCK, null);
        companyCode = blkDetail.getCompanyCode();
        estate = blkDetail.getEstate();
        String[] a = new String[5];
        a[0] = SESS_ZYEAR;
        a[1] = SESS_PERIOD;
        a[2] = block;
        a[3] = "3";
        a[4] = SESS_SKB;
        String query = "SELECT MAX(ZDate) as ZDate FROM tblT_BlockCondition " +
                "WHERE ZYear=? " +
                "AND Period=? " +
                "AND Block = ? " +
                "AND TransLevel = ? " +
                "AND SKB=? ";
        tblT_BlockCondition lst = (tblT_BlockCondition) database.getDataFirstRaw(query, tblT_BlockCondition.TABLE_NAME, a);
        if(lst.getAcc_ZDate() == null){
            LastDate="";
        }
        else{
            LastDate = lst.getAcc_ZDate();
        }

    }

    private void DataSource1(){
        List<tblT_BlockCondition> listTemp = new ArrayList<tblT_BlockCondition>();
        List<Object> listObject;
        String[] a = new String[7];
        a[0] = SESS_ZYEAR;
        a[1] = SESS_PERIOD;
        a[2] = block;
        a[3] = "3";
        a[4] = SESS_SKB;
        a[5] = LastDate;
        a[6] = "L";

        String sqldb_query = "SELECT * FROM "+tblT_BlockCondition.TABLE_NAME+" WHERE "+
                tblT_BlockCondition.XML_ZYear+ "=?" + " and " +
                tblT_BlockCondition.XML_Period + "=?" + " and " +
                tblT_BlockCondition.XML_Block+ "=?" + " and " +
                tblT_BlockCondition.XML_TransLevel+ "=?" + " and " +
                tblT_BlockCondition.XML_SKB + "=? and "+
                tblT_BlockCondition.XML_ZDate+" =? and "+
                tblT_BlockCondition.XML_PokokSide+" = ? ORDER BY CAST(CensusPoint AS INTEGER) DESC LIMIT 1";

        // listObject = database.getListData(distinct, tableName, columns, whereClause, whereArgs, groupBy, having, orderBy, limit);
        listObject = database.getListDataRawQuery(sqldb_query,tblT_BlockCondition.TABLE_NAME,a);

        if(listObject.size() > 0){
            ClickYourFlagDisable();
            for(int i = 0; i < listObject.size(); i++){
                tblT_BlockCondition blk = (tblT_BlockCondition) listObject.get(i);
                listTemp.add(blk);
                String barisL = blk.getAcc_BlockRow();
                editTxtPokokLeft.setText(String.valueOf(Integer.parseInt(blk.getAcc_CensusPoint())+1));
                createSpinnerKondisiLeft();
                if(barisL.isEmpty()){
                    DeteksiBaris(barisL,blk.getAcc_Flag());
                    editTxtBarisLeft.setHint(getResources().getString(R.string.dataempty));
                    editTxtBarisLeft.setText("");

                }
                else{
                    DeteksiBaris(barisL,blk.getAcc_Flag());

                }
            }
        }
        else{
            String barisL = new PreferenceManager(getActivity(),Constants.shared_name).getBarisL();
            flagFrontON();
            ClickYourFlag();
            editTxtBarisLeft.setHint(getResources().getString(R.string.dataempty));
            editTxtBarisLeft.setText("");
            editTxtPokokLeft.setText(String.valueOf(listTemp.size()+1));
            btnLeft.setVisibility(View.GONE);
            createSpinnerKondisiLeft();
        }
    }

    private void DataSource2(){
        List<tblT_BlockCondition> listTemp = new ArrayList<tblT_BlockCondition>();
        List<Object> listObject;
        String[] a = new String[7];
        a[0] = SESS_ZYEAR;
        a[1] = SESS_PERIOD;
        a[2] = block;
        a[3] = "3";
        a[4] = SESS_SKB;
        a[5] = LastDate;
        a[6] = "R";

        String sqldb_query = "SELECT * FROM "+tblT_BlockCondition.TABLE_NAME+" WHERE "+
                tblT_BlockCondition.XML_ZYear+ "=?" + " and " +
                tblT_BlockCondition.XML_Period + "=?" + " and " +
                tblT_BlockCondition.XML_Block+ "=?" + " and " +
                tblT_BlockCondition.XML_TransLevel+ "=?" + " and " +
                tblT_BlockCondition.XML_SKB + "=? and "+
                tblT_BlockCondition.XML_ZDate+" =? and "+
                tblT_BlockCondition.XML_PokokSide+" = ? ORDER BY CAST(CensusPoint AS INTEGER) DESC LIMIT 1";

        // listObject = database.getListData(distinct, tableName, columns, whereClause, whereArgs, groupBy, having, orderBy, limit);
        listObject = database.getListDataRawQuery(sqldb_query,tblT_BlockCondition.TABLE_NAME,a);
        database.closeTransaction();
        if(listObject.size() > 0){
            ClickYourFlagDisable();
            for(int i = 0; i < listObject.size(); i++){
                tblT_BlockCondition blk = (tblT_BlockCondition) listObject.get(i);
                listTemp.add(blk);
                String barisR = blk.getAcc_BlockRow();
                editTxtPokokRight.setText(String.valueOf(Integer.parseInt(blk.getAcc_CensusPoint())+1));
                createSpinnerKondisiRight();
                if(barisR.isEmpty()){
                    DeteksiBarisR(barisR,blk.getAcc_Flag());
                    editTxtBarisRight.setHint(getResources().getString(R.string.dataempty));
                    editTxtBarisRight.setText("");

                }
                else{
                    //editTxtBarisRight.setText(barisR);
                    DeteksiBarisR(barisR,blk.getAcc_Flag());

                }
            }
        }
        else{
            String barisR = new PreferenceManager(getActivity(),Constants.shared_name).getBarisR();

            flagFrontON();
            ClickYourFlag();
            editTxtBarisRight.setHint(getResources().getString(R.string.dataempty));
            editTxtBarisRight.setText("");
            editTxtPokokRight.setText(String.valueOf(listTemp.size()+1));
            btnLeft.setVisibility(View.GONE);
            createSpinnerKondisiRight();
        }
    }

    private void setDataL(tblT_BlockCondition dataScore) {
        List<String> kondisiL = new ArrayList<String>();
        kondisiL.clear();
        if(dataScore.getAcc_PokokCondition().equalsIgnoreCase("Normal")){
            kondisiL.add(dataScore.getAcc_PokokCondition());
            kondisiL.add("Mati");
            kondisiL.add("Kosong");
            kondisiL.add("HCV");

        }
        if(dataScore.getAcc_PokokCondition().equalsIgnoreCase("Mati")){
            kondisiL.add(dataScore.getAcc_PokokCondition());
            kondisiL.add("Normal");
            kondisiL.add("Kosong");
            kondisiL.add("HCV");
        }
        if(dataScore.getAcc_PokokCondition().equalsIgnoreCase("Kosong")){
            kondisiL.add(dataScore.getAcc_PokokCondition());
            kondisiL.add("Normal");
            kondisiL.add("Mati");
            kondisiL.add("HCV");
        }
        if(dataScore.getAcc_PokokCondition().equalsIgnoreCase("HCV")){
            kondisiL.add(dataScore.getAcc_PokokCondition());
            kondisiL.add("Mati");
            kondisiL.add("Kosong");
            kondisiL.add("Normal");
        }

        //  Toast.makeText(getContext(),  "Set Data L "+dataScore.getAcc_PokokCondition(), Toast.LENGTH_LONG).show();
//        ArrayAdapter<String> dataAdapterL = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kondisiL);
//
//        // Drop down layout style - list view with radio button
//        dataAdapterL.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<String> dataAdapterL = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kondisiL);

        // Drop down layout style - list view with radio button
        dataAdapterL.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinLeft.setAdapter(dataAdapterL);
        spinLeft.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                Kondisi_L = String.valueOf(parentView1.getItemAtPosition(position));
                setRBL(String.valueOf(parentView1.getItemAtPosition(position)));
                //    Toast.makeText(getContext(),  parentView1.getItemAtPosition(position) + " selected", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        if (phase.equalsIgnoreCase("IMMATURE")) {
            bariskiri.setEnabled(false);
            bariskanan.setEnabled(true);
            editTxtPokokLeft.setText(dataScore.getAcc_PokokLabel());
            //   editTxtBarisLeft.setText(dataScore.getAcc_BlockRow());
            DeteksiBaris(dataScore.getAcc_BlockRow(),dataScore.getAcc_Flag());
            setPiringan_L(dataScore);
            setPasarRintis_L(dataScore);
            setSanitasi_L(dataScore);
            setGawangan_L(dataScore);
            setKacangan_L(dataScore);
            setDrainase_LIM(dataScore);
            setRayap_L(dataScore);
            setOryctes_L(dataScore);
            editRemark1.setText(dataScore.getAcc_Remark());

        } else {
            bariskiri.setEnabled(false);
            bariskanan.setEnabled(true);
            editTxtPokokLeft.setText(dataScore.getAcc_CensusPoint());
            /*editTxtBarisLeft.setText(dataScore.getAcc_BlockRow());*/
            DeteksiBaris(dataScore.getAcc_BlockRow(),dataScore.getAcc_Flag());
            setPiringan_L(dataScore);
            setPasarPanen_L(dataScore);
            setTunasPokok_L(dataScore);
            setGawangan_L(dataScore);
            setDrainase_L(dataScore);
            setGanoderma_L(dataScore);
            setRayap_L(dataScore);
            setOryctes_L(dataScore);
            editRemark1.setText(dataScore.getAcc_Remark());
        }
    }

    private void setKosongSpinnerL(){
        List<String> kondisiL= new ArrayList<String>();
        kondisiL.clear();
        kondisiL.add("Kosong");
        kondisiL.add("Mati");
        kondisiL.add("Normal");
        kondisiL.add("HCV");
        ArrayAdapter<String> dataAdapterL = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kondisiL);

        // Drop down layout style - list view with radio button
        dataAdapterL.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinLeft.setAdapter(dataAdapterL);
        spinLeft.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                Kondisi_L = String.valueOf(parentView1.getItemAtPosition(position));
                setRBL(String.valueOf(parentView1.getItemAtPosition(position)));
                //    Toast.makeText(getContext(),  parentView1.getItemAtPosition(position) + " selected", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void setKosongSpinnerR(){
        List<String> kondisiR = new ArrayList<String>();
        kondisiR.clear();
            kondisiR.add("Kosong");
            kondisiR.add("Mati");
            kondisiR.add("Normal");
            kondisiR.add("HCV");
        ArrayAdapter<String> dataAdapterR = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kondisiR);

        // Drop down layout style - list view with radio button
        dataAdapterR.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinRight.setAdapter(dataAdapterR);
        spinRight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                Kondisi_R = String.valueOf(parentView1.getItemAtPosition(position));
                setRBR(String.valueOf(parentView1.getItemAtPosition(position)));
                //    Toast.makeText(getContext(),  parentView1.getItemAtPosition(position) + " selected", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void setNormalSpinnerL(){
        List<String> kondisiL= new ArrayList<String>();
        kondisiL.clear();
        kondisiL.add("Normal");
        kondisiL.add("Mati");
        kondisiL.add("Kosong");
        kondisiL.add("HCV");
        ArrayAdapter<String> dataAdapterL = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kondisiL);

        // Drop down layout style - list view with radio button
        dataAdapterL.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinLeft.setAdapter(dataAdapterL);
        spinLeft.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                Kondisi_L = String.valueOf(parentView1.getItemAtPosition(position));
                setRBL(String.valueOf(parentView1.getItemAtPosition(position)));
                //    Toast.makeText(getContext(),  parentView1.getItemAtPosition(position) + " selected", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void setNormalSpinnerR(){
        List<String> kondisiR = new ArrayList<String>();
        kondisiR.clear();
        kondisiR.add("Normal");
        kondisiR.add("Mati");
        kondisiR.add("Kosong");
        kondisiR.add("HCV");
        ArrayAdapter<String> dataAdapterR = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kondisiR);

        // Drop down layout style - list view with radio button
        dataAdapterR.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinRight.setAdapter(dataAdapterR);
        spinRight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                Kondisi_R = String.valueOf(parentView1.getItemAtPosition(position));
                setRBR(String.valueOf(parentView1.getItemAtPosition(position)));
                //    Toast.makeText(getContext(),  parentView1.getItemAtPosition(position) + " selected", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }
    private void setDataR(tblT_BlockCondition dataScore) {
        List<String> kondisiR = new ArrayList<String>();
        kondisiR.clear();
        if(dataScore.getAcc_PokokCondition().equalsIgnoreCase("Normal")){
            kondisiR.add(dataScore.getAcc_PokokCondition());
            kondisiR.add("Mati");
            kondisiR.add("Kosong");
            kondisiR.add("HCV");
        }
        if(dataScore.getAcc_PokokCondition().equalsIgnoreCase("Mati")){
            kondisiR.add(dataScore.getAcc_PokokCondition());
            kondisiR.add("Normal");
            kondisiR.add("Kosong");
            kondisiR.add("HCV");
        }
        if(dataScore.getAcc_PokokCondition().equalsIgnoreCase("Kosong")){
            kondisiR.add(dataScore.getAcc_PokokCondition());
            kondisiR.add("Normal");
            kondisiR.add("Mati");
            kondisiR.add("HCV");
        }
        if(dataScore.getAcc_PokokCondition().equalsIgnoreCase("HCV")){
            kondisiR.add(dataScore.getAcc_PokokCondition());
            kondisiR.add("Mati");
            kondisiR.add("Kosong");
            kondisiR.add("Normal");
        }
        //  Toast.makeText(getContext(),  "Set Data R "+dataScore.getAcc_PokokCondition(), Toast.LENGTH_LONG).show();
    //    ArrayAdapter<String> dataAdapterR = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kondisiR);

        // Drop down layout style - list view with radio button
      //  dataAdapterR.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<String> dataAdapterR = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kondisiR);

        // Drop down layout style - list view with radio button
        dataAdapterR.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinRight.setAdapter(dataAdapterR);
        spinRight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                Kondisi_R = String.valueOf(parentView1.getItemAtPosition(position));
                setRBR(String.valueOf(parentView1.getItemAtPosition(position)));
                //    Toast.makeText(getContext(),  parentView1.getItemAtPosition(position) + " selected", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        if (phase.equalsIgnoreCase("IMMATURE")) {
            bariskiri.setEnabled(true);
            bariskanan.setEnabled(false);
            editTxtPokokRight.setText(dataScore.getAcc_CensusPoint());
            /*editTxtBarisRight.setText(dataScore.getAcc_BlockRow());*/
            DeteksiBarisR(dataScore.getAcc_BlockRow(),dataScore.getAcc_Flag());
            setPiringan_R(dataScore);
            setPasarRintis_R(dataScore);
            setSanitasi_R(dataScore);
            setGawangan_R(dataScore);
            setKacangan_R(dataScore);
            setDrainase_RIM(dataScore);
            setRayap_R(dataScore);
            setOryctes_R(dataScore);
            editRemark2.setText(dataScore.getAcc_Remark());
        } else {
            bariskiri.setEnabled(true);
            bariskanan.setEnabled(false);
            editTxtPokokRight.setText(dataScore.getAcc_CensusPoint());
            /*editTxtBarisRight.setText(dataScore.getAcc_BlockRow());*/
            DeteksiBarisR(dataScore.getAcc_BlockRow(),dataScore.getAcc_Flag());
            setPiringan_R(dataScore);
            setPasarPanen_R(dataScore);
            setTunasPokok_R(dataScore);
            setGawangan_R(dataScore);
            setDrainase_R(dataScore);
            setGanoderma_R(dataScore);
            setRayap_R(dataScore);
            setOryctes_R(dataScore);
            editRemark2.setText(dataScore.getAcc_Remark());
        }
    }

    void setPiringan_L(tblT_BlockCondition dataScore){
        String val = dataScore.getAcc_Piringan();
        if (val.equals("1")) {
            rbnblockred1.setChecked(true);
        }
        if (val.equals("2")) {
            rbnblockyellow1.setChecked(true);
        }
        if (val.equals("3")) {
            rbnblockgreen1.setChecked(true);
        }
    }
    void setPiringan_R(tblT_BlockCondition dataScore) {
        String val = dataScore.getAcc_Piringan();
        if (val.equals("1")) {
            rbnblockred1R.setChecked(true);
        }
        if (dataScore.getAcc_Piringan().equalsIgnoreCase("2")) {
            rbnblockyellow1R.setChecked(true);
        }
        if (dataScore.getAcc_Piringan().equalsIgnoreCase("3")) {
            rbnblockgreen1R.setChecked(true);
        }
    }
    void setPasarRintis_L (tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_PasarRintis().equalsIgnoreCase("1")) {
            rbnblockred2.setChecked(true);
        }
        if (dataScore.getAcc_PasarRintis().equalsIgnoreCase("2")) {
            rbnblockyellow2.setChecked(true);
        }
        if (dataScore.getAcc_PasarRintis().equalsIgnoreCase("3")) {
            rbnblockgreen2.setChecked(true);
        }
    }
    void setPasarRintis_R (tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_PasarRintis().equalsIgnoreCase("1")) {
            rbnblockred2R.setChecked(true);
        }
        if (dataScore.getAcc_PasarRintis().equalsIgnoreCase("2")) {
            rbnblockyellow2R.setChecked(true);
        }
        if (dataScore.getAcc_PasarRintis().equalsIgnoreCase("3")) {
            rbnblockgreen2R.setChecked(true);
        }
    }

    void setPasarPanen_L (tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_PasarPanen().equalsIgnoreCase("1")) {
            rbnblockred2.setChecked(true);
        }
        if (dataScore.getAcc_PasarPanen().equalsIgnoreCase("2")) {
            rbnblockyellow2.setChecked(true);
        }
        if (dataScore.getAcc_PasarPanen().equalsIgnoreCase("3")) {
            rbnblockgreen2.setChecked(true);
        }
    }
    void setPasarPanen_R (tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_PasarPanen().equalsIgnoreCase("1")) {
            rbnblockred2R.setChecked(true);
        }
        if (dataScore.getAcc_PasarPanen().equalsIgnoreCase("2")) {
            rbnblockyellow2R.setChecked(true);
        }
        if (dataScore.getAcc_PasarPanen().equalsIgnoreCase("3")) {
            rbnblockgreen2R.setChecked(true);
        }
    }
    void setSanitasi_L (tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Sanitasi().equalsIgnoreCase("1")) {
            rbnblockred3.setChecked(true);
        }
        if (dataScore.getAcc_Sanitasi().equalsIgnoreCase("2")) {
            rbnblockyellow3.setChecked(true);
        }
        if (dataScore.getAcc_Sanitasi().equalsIgnoreCase("3")) {
            rbnblockgreen3.setChecked(true);
        }
    }
    void setSanitasi_R (tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Sanitasi().equalsIgnoreCase("1")) {
            rbnblockred3R.setChecked(true);
        }
        if (dataScore.getAcc_Sanitasi().equalsIgnoreCase("2")) {
            rbnblockyellow3R.setChecked(true);
        }
        if (dataScore.getAcc_Sanitasi().equalsIgnoreCase("3")) {
            rbnblockgreen3R.setChecked(true);
        }
    }
    void setTunasPokok_L(tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_TunasPokok().equalsIgnoreCase("1")) {
            rbnblockred3.setChecked(true);
        }
        if (dataScore.getAcc_TunasPokok().equalsIgnoreCase("2")) {
            rbnblockyellow3.setChecked(true);
        }
        if (dataScore.getAcc_TunasPokok().equalsIgnoreCase("3")) {
            rbnblockgreen3.setChecked(true);
        }
    }
    void setTunasPokok_R(tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_TunasPokok().equalsIgnoreCase("1")) {
            rbnblockred3R.setChecked(true);
        }
        if (dataScore.getAcc_TunasPokok().equalsIgnoreCase("2")) {
            rbnblockyellow3R.setChecked(true);
        }
        if (dataScore.getAcc_TunasPokok().equalsIgnoreCase("3")) {
            rbnblockgreen3R.setChecked(true);
        }
    }
    void setGawangan_L (tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Gawangan().equalsIgnoreCase("1")) {
            rbnblockred4.setChecked(true);
        }
        if (dataScore.getAcc_Gawangan().equalsIgnoreCase("2")) {
            rbnblockyellow4.setChecked(true);
        }
        if (dataScore.getAcc_Gawangan().equalsIgnoreCase("3")) {
            rbnblockgreen4.setChecked(true);
        }
    }
    void setGawangan_R (tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Gawangan().equalsIgnoreCase("1")) {
            rbnblockred4R.setChecked(true);
        }
        if (dataScore.getAcc_Gawangan().equalsIgnoreCase("2")) {
            rbnblockyellow4R.setChecked(true);
        }
        if (dataScore.getAcc_Gawangan().equalsIgnoreCase("3")) {
            rbnblockgreen4R.setChecked(true);
        }
    }

    void setKacangan_L (tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Kacangan().equalsIgnoreCase("1")) {
            rbnblockred5.setChecked(true);
        }
        if (dataScore.getAcc_Kacangan().equalsIgnoreCase("2")) {
            rbnblockyellow5.setChecked(true);
        }
        if (dataScore.getAcc_Kacangan().equalsIgnoreCase("3")) {
            rbnblockgreen5.setChecked(true);
        }
    }
    void setKacangan_R (tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Kacangan().equalsIgnoreCase("1")) {
            rbnblockred5R.setChecked(true);
        }
        if (dataScore.getAcc_Kacangan().equalsIgnoreCase("2")) {
            rbnblockyellow5R.setChecked(true);
        }
        if (dataScore.getAcc_Kacangan().equalsIgnoreCase("3")) {
            rbnblockgreen5R.setChecked(true);
        }
    }

    void setDrainase_L (tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Drainase().equalsIgnoreCase("1")) {
            rbnblockred5.setChecked(true);
        }
        if (dataScore.getAcc_Drainase().equalsIgnoreCase("2")) {
            rbnblockyellow5.setChecked(true);
        }
        if (dataScore.getAcc_Drainase().equalsIgnoreCase("3")) {
            rbnblockgreen5.setChecked(true);
        }
    }

    void setDrainase_R (tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Drainase().equalsIgnoreCase("1")) {
            rbnblockred5R.setChecked(true);
        }
        if (dataScore.getAcc_Drainase().equalsIgnoreCase("2")) {
            rbnblockyellow5R.setChecked(true);
        }
        if (dataScore.getAcc_Drainase().equalsIgnoreCase("3")) {
            rbnblockgreen5R.setChecked(true);
        }
    }

    void setDrainase_LIM (tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Drainase().equalsIgnoreCase("1")) {
            rbnblockred6.setChecked(true);
        }
        if (dataScore.getAcc_Drainase().equalsIgnoreCase("2")) {
            rbnblockyellow6.setChecked(true);
        }
        if (dataScore.getAcc_Drainase().equalsIgnoreCase("3")) {
            rbnblockgreen6.setChecked(true);
        }
    }

    void setDrainase_RIM (tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Drainase().equalsIgnoreCase("1")) {
            rbnblockred6R.setChecked(true);
        }
        if (dataScore.getAcc_Drainase().equalsIgnoreCase("2")) {
            rbnblockyellow6R.setChecked(true);
        }
        if (dataScore.getAcc_Drainase().equalsIgnoreCase("3")) {
            rbnblockgreen6R.setChecked(true);
        }
    }

    void setGanoderma_L(tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Ganoderma().equalsIgnoreCase("1")) {
            rbnblockred6.setChecked(true);
        }
        if (dataScore.getAcc_Ganoderma().equalsIgnoreCase("2")) {
            rbnblockyellow6.setChecked(true);
        }
        if (dataScore.getAcc_Ganoderma().equalsIgnoreCase("3")) {
            rbnblockgreen6.setChecked(true);
        }
    }

    private void setGanoderma_R(tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Ganoderma().equalsIgnoreCase("1")) {
            rbnblockred6R.setChecked(true);
        }
        if (dataScore.getAcc_Ganoderma().equalsIgnoreCase("2")) {
            rbnblockyellow6R.setChecked(true);
        }
        if (dataScore.getAcc_Ganoderma().equalsIgnoreCase("3")) {
            rbnblockgreen6R.setChecked(true);
        }
    }

    private void setRayap_L(tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Rayap().equalsIgnoreCase("1")) {
            rbnblockred7.setChecked(true);
        }
        if (dataScore.getAcc_Rayap().equalsIgnoreCase("2")) {
            rbnblockyellow7.setChecked(true);
        }
        if (dataScore.getAcc_Rayap().equalsIgnoreCase("3")) {
            rbnblockgreen7.setChecked(true);
        }
    }

    private void setRayap_R(tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Rayap().equalsIgnoreCase("1")) {
            rbnblockred7R.setChecked(true);
        }
        if (dataScore.getAcc_Rayap().equalsIgnoreCase("2")) {
            rbnblockyellow7R.setChecked(true);
        }
        if (dataScore.getAcc_Rayap().equalsIgnoreCase("3")) {
            rbnblockgreen7R.setChecked(true);
        }
    }

    private void setOryctes_L(tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Orcytes().equalsIgnoreCase("1")) {
            rbnblockred8.setChecked(true);
        }
        if (dataScore.getAcc_Orcytes().equalsIgnoreCase("2")) {
            rbnblockyellow8.setChecked(true);
        }
        if (dataScore.getAcc_Orcytes().equalsIgnoreCase("3")) {
            rbnblockgreen8.setChecked(true);
        }
    }

    private void setOryctes_R(tblT_BlockCondition dataScore) {
        if (dataScore.getAcc_Orcytes().equalsIgnoreCase("1")) {
            rbnblockred8R.setChecked(true);
        }
        if (dataScore.getAcc_Orcytes().equalsIgnoreCase("2")) {
            rbnblockyellow8R.setChecked(true);
        }
        if (dataScore.getAcc_Orcytes().equalsIgnoreCase("3")) {
            rbnblockgreen8R.setChecked(true);
        }
    }



    private void createSpinnerKondisiLeft(){
        // Spinner Drop down elements
        List<String> kondisi = new ArrayList<String>();
        kondisi.clear();
        kondisi.add("Normal");
        kondisi.add("Kosong");
        kondisi.add("Mati");
        kondisi.add("HCV");

        // Creating adapter for spinner

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kondisi);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinLeft.setAdapter(dataAdapter);
        spinLeft.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                Kondisi_L = String.valueOf(parentView1.getItemAtPosition(position));
                setRBL(String.valueOf(parentView1.getItemAtPosition(position)));

                //    Toast.makeText(getContext(),  parentView1.getItemAtPosition(position) + " selected", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

    }
    private void createSpinnerKondisiRight(){
        // Spinner click listener
        // Spinner Drop down elements
        List<String> kondisi = new ArrayList<String>();
        kondisi.clear();
        kondisi.add("Normal");
        kondisi.add("Kosong");
        kondisi.add("Mati");
        kondisi.add("HCV");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kondisi);

        // Drop down layout style - list view with radio button
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinRight.setAdapter(dataAdapter2);
        spinRight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView2, View selectedItemView, int position, long id) {
                //   Toast.makeText(getContext(),  parentView2.getItemAtPosition(position) + " selected", Toast.LENGTH_LONG).show();
                Kondisi_R = String.valueOf(parentView2.getItemAtPosition(position));
                setRBR(String.valueOf(parentView2.getItemAtPosition(position)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }


    private void initDataSource1(String status){
        setCek();
        clearCheck();
        database.openTransaction();
        String[] a = new String[8];
        String[] a2 = new String[7];
        if(status.equals("Next")){
            a[0] = SESS_ZYEAR;
            a[1] = SESS_PERIOD;
            a[2] = block;
            a[3] = "3";
            a[4] = SESS_SKB;
            a[5] = SESS_ZDATEFragment;
            a[6] = String.valueOf(Integer.parseInt(PokokKe_L)+1);
            a[7] = "L";
            a2[0] = SESS_ZYEAR;
            a2[1] = SESS_PERIOD;
            a2[2] = block;
            a2[3] = "3";
            a2[4] = SESS_SKB;
            a2[5] = String.valueOf(Integer.parseInt(PokokKe_L)+1);
            a2[6] = "L";
        }
        if(status.equals("Back")){
            a[0] = SESS_ZYEAR;
            a[1] = SESS_PERIOD;
            a[2] = block;
            a[3] = "3";
            a[4] = SESS_SKB;
            a[5] = SESS_ZDATEFragment;
            a[6] = String.valueOf(Integer.parseInt(PokokKe_L)-1);
            a[7] = "L";
            a2[0] = SESS_ZYEAR;
            a2[1] = SESS_PERIOD;
            a2[2] = block;
            a2[3] = "3";
            a2[4] = SESS_SKB;
            a2[5] = String.valueOf(Integer.parseInt(PokokKe_L)-1);
            a2[6] = "L";
        }
        String query = "SELECT * FROM  tblT_BlockCondition " +
                "WHERE ZYear=? " +
                "AND Period=? " +
                "AND Block = ? " +
                "AND Translevel = ? " +
                "AND SKB= ? " +
                "AND ZDate = ? " +
                "AND CensusPoint = ? " +
                "AND POKOKSIDE=?";
        tblT_BlockCondition dt1 = (tblT_BlockCondition) database.getDataFirstRaw(query,tblT_BlockCondition.TABLE_NAME,a);
        String query2 = "SELECT * FROM  tblT_BlockCondition " +
                "WHERE ZYear=? " +
                "AND Period=? " +
                "AND Block = ? " +
                "AND Translevel = ? " +
                "AND SKB= ? " +
                "AND CensusPoint = ? " +
                "AND POKOKSIDE=?";
        tblT_BlockCondition dt12 = (tblT_BlockCondition) database.getDataFirstRaw(query2,tblT_BlockCondition.TABLE_NAME,a2);
        if(dt1!=null){
            database.closeTransaction();
            setDataL(dt1);
            editTxtBarisLeft.setText(dt1.getAcc_BlockRow());
            editTxtPokokLeft.setText(String.valueOf(dt1.getAcc_CensusPoint()));
        }
        if(dt1==null){
            if(dt12!=null){
                database.closeTransaction();
                setDataL(dt12);
                editTxtBarisLeft.setText(dt12.getAcc_BlockRow());
                editTxtPokokLeft.setText(String.valueOf(dt12.getAcc_CensusPoint()));
            }
            if(dt12==null){
                String KondisiPokok = new PreferenceManager(getActivity(),Constants.shared_name).getKondisipokokL();
                if(!KondisiPokok.isEmpty()){
                    setKosongSpinnerL();
                }else{
                    setNormalSpinnerL();
                }
                database.closeTransaction();
                editTxtPokokLeft.setText(a[6]);
                if(status.equals("Next")){
                    btnLeft.setVisibility(View.VISIBLE);
                }
            }


        }

    }
    private void initDataSource2(String status){
        setCek();
        clearCheck2();
        database.openTransaction();
        String[] a = new String[8];
        String[] a2 = new String[7];
        if(status.equals("Next")){
            a[0] = SESS_ZYEAR;
            a[1] = SESS_PERIOD;
            a[2] = block;
            a[3] = "3";
            a[4] = SESS_SKB;
            a[5] = SESS_ZDATEFragment;
            a[6] = String.valueOf(Integer.parseInt(PokokKe_R)+1);
            a[7] = "R";
            a2[0] = SESS_ZYEAR;
            a2[1] = SESS_PERIOD;
            a2[2] = block;
            a2[3] = "3";
            a2[4] = SESS_SKB;
            a2[5] = String.valueOf(Integer.parseInt(PokokKe_R)+1);
            a2[6] = "R";
        }
        if(status.equals("Back")){
            a[0] = SESS_ZYEAR;
            a[1] = SESS_PERIOD;
            a[2] = block;
            a[3] = "3";
            a[4] = SESS_SKB;
            a[5] = SESS_ZDATEFragment;
            a[6] = String.valueOf(Integer.parseInt(PokokKe_R)-1);
            a[7] = "R";
            a2[0] = SESS_ZYEAR;
            a2[1] = SESS_PERIOD;
            a2[2] = block;
            a2[3] = "3";
            a2[4] = SESS_SKB;
            a2[5] = String.valueOf(Integer.parseInt(PokokKe_R)-1);
            a2[6] = "R";
        }
        String query = "SELECT * FROM  tblT_BlockCondition " +
                "WHERE ZYear=? " +
                "AND Period=? " +
                "AND Block = ? " +
                "AND Translevel = ? " +
                "AND SKB= ? " +
                "AND ZDate = ? " +
                "AND CensusPoint = ? " +
                "AND POKOKSIDE=?";
        tblT_BlockCondition dt2 = (tblT_BlockCondition) database.getDataFirstRaw(query,tblT_BlockCondition.TABLE_NAME,a);
        String query2 = "SELECT * FROM  tblT_BlockCondition " +
                "WHERE ZYear=? " +
                "AND Period=? " +
                "AND Block = ? " +
                "AND Translevel = ? " +
                "AND SKB= ? " +
                "AND CensusPoint = ? " +
                "AND POKOKSIDE=?";
        tblT_BlockCondition dt22 = (tblT_BlockCondition) database.getDataFirstRaw(query2,tblT_BlockCondition.TABLE_NAME,a2);
        if(dt2!=null){
            database.closeTransaction();
            setDataR(dt2);
            editTxtBarisRight.setText(dt2.getAcc_BlockRow());
            editTxtPokokRight.setText(String.valueOf(dt2.getAcc_CensusPoint()));
        }
        if(dt2==null){
            if(dt22!=null){
                database.closeTransaction();
                setDataR(dt22);
                editTxtBarisRight.setText(dt22.getAcc_BlockRow());
                editTxtPokokRight.setText(String.valueOf(dt22.getAcc_CensusPoint()));
            }
            if(dt22==null){
                String KondisiPokok = new PreferenceManager(getActivity(),Constants.shared_name).getKondisipokokR();
                if(!KondisiPokok.isEmpty()){
                    setKosongSpinnerR();
                }else{
                    setNormalSpinnerR();
                }
                database.closeTransaction();
                editTxtPokokRight.setText(a[6]);
                if(status.equals("Next")){
                    btnLeft.setVisibility(View.VISIBLE);
                }
            }

        }
    }

    private void setCek(){
        Baris_L = editTxtBarisLeft.getText().toString();
        PokokKe_L = editTxtPokokLeft.getText().toString();
        Baris_R = editTxtBarisRight.getText().toString();
        PokokKe_R = editTxtPokokRight.getText().toString();
        if(Integer.parseInt(PokokKe_L)>1){
            editTxtBarisLeft.setEnabled(false);
            editTxtBarisRight.setEnabled(false);
        }
    }

    private void initBtnRight(){
        setCek();
        if(Integer.parseInt(PokokKe_L)==1){
            if(editTxtBarisLeft.getText().toString().length()==0 && editTxtBarisRight.getText().toString().length()==0){
                ToastMessage toastMessage = new ToastMessage(getActivity());
                toastMessage.shortMessage("KOLOM BARIS HARUS DI ISI ");
            }else{
                DisplayDialogNew();

            }

        }else{
            DisplayDialogNew();
        }
    }

    private void DisplayDialogNew(){
        new DialogConfirmation(getActivity(), "Are you sure want to continue to next pokok ?" ,
                getResources().getString(R.string.data_save), null, new DialogConfirmationListener() {
            @Override
            public void onConfirmOK(Object object, int id) {
                hapusDataKondisiPokokL();
                PokokKe_L= String.valueOf(Integer.parseInt(PokokKe_L + 1));
                PokokKe_R= String.valueOf(Integer.parseInt(PokokKe_R + 1));
            }

            @Override
            public void onConfirmCancel(Object object, int id) {
                hapusDataKondisiPokokL();
                PokokKe_L= String.valueOf(Integer.parseInt(PokokKe_L + 1));
                PokokKe_R= String.valueOf(Integer.parseInt(PokokKe_R + 1));
            }
        }).show();
    }

    private  void hapusDataKondisiPokokL(){
        String todayDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT);
        setCek();
        database.openTransaction();
        int Deleted;
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        Deleted = database.deleteData(tblT_BlockCondition.TABLE_NAME,
                tblT_BlockCondition.XML_ZYear+ "=? AND "+
                        tblT_BlockCondition.XML_Period+ "=? AND "+
                        tblT_BlockCondition.XML_ZDate+ "=? AND "+
                        tblT_BlockCondition.XML_Block+ "=? AND "+
                        tblT_BlockCondition.XML_TransLevel+ "=? AND "+
                        tblT_BlockCondition.XML_SKB+ "=? AND "+
                        tblT_BlockCondition.XML_BlockRow+ "=? AND "+
                        tblT_BlockCondition.XML_PokokSide+ "=? AND "+
                        tblT_BlockCondition.XML_CensusPoint+ "=? ",
                new String [] {SESS_ZYEAR,SESS_PERIOD,SESS_ZDATEFragment,block,"3",SESS_SKB,Baris_L,"L",PokokKe_L});
        if(Deleted>0){
            if(phase.equalsIgnoreCase("MATURE")){
                saveMature(blkDetail.getCompanyCode(),estate,blkDetail.getDivisi(),phase,todayDate,userLogin);
            }
            else{
                saveIMMature(blkDetail.getCompanyCode(),estate,blkDetail.getDivisi(),phase,todayDate,userLogin);
            }
        }else{
            if(phase.equalsIgnoreCase("MATURE")){
                saveMature(blkDetail.getCompanyCode(),estate,blkDetail.getDivisi(),phase,todayDate,userLogin);
            }
            else{
                saveIMMature(blkDetail.getCompanyCode(),estate,blkDetail.getDivisi(),phase,todayDate,userLogin);
            }
        }
    }

    private void saveMature(String companyCode,String estate,String divisi,String phase,String todayDate,UserLogin userLogin){
        database.setData(new tblT_BlockCondition(0,companyCode, estate,
                SESS_ZYEAR, SESS_PERIOD, SESS_ZDATEFragment,
                3, blkDetail.getBlock(), SESS_SKB,
                Baris_L,PokokKe_L,
                PokokKe_L, "L",
                Kondisi_L, null,
                null, null,
                null, null,
                null, null,
                null, null,
                null,null,
                null,Piringan_L,
                PasarPanen_L,null,TunasPokok_L,
                Gawangan_L,Drainase_L,
                Ganoderma_L, Rayap_L,
                Oryctes_L, null,
                null,todayDate,
                userLogin.getNik(),todayDate, userLogin.getNik(),Flag,divisi,editRemark1.getText().toString()));
        database.commitTransaction();
        new PreferenceManager(getActivity(),Constants.shared_name).setBarisL(Baris_L);
        database.closeTransaction();
        hapusDataKondisiPokokR();

    }
    private void saveIMMature(String companyCode,String estate,String divisi,String phase,String todayDate,UserLogin userLogin){
        database.setData(new tblT_BlockCondition(0,companyCode, estate,
                SESS_ZYEAR, SESS_PERIOD, SESS_ZDATEFragment,
                3, blkDetail.getBlock(), SESS_SKB,
                Baris_L,PokokKe_L,
                PokokKe_L, "L",
                Kondisi_L, null,
                null, null,
                null, null,
                null, null,
                null, null,
                null,null,
                null,Piringan_L,
                null,PasarRintis_L,null,
                Gawangan_L,Drainase_L,
                null, Rayap_L,
                Oryctes_L, Sanitasi_L,
                Kacangan_L,todayDate,
                userLogin.getNik(),todayDate, userLogin.getNik(),Flag,divisi,editRemark1.getText().toString()));
        database.commitTransaction();
        database.closeTransaction();
        hapusDataKondisiPokokR();
    }

    private void hapusDataKondisiPokokR(){
        database.closeTransaction();
        database.openTransaction();
        String todayDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT);
        int Deleted;
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        Deleted = database.deleteData(tblT_BlockCondition.TABLE_NAME,
                tblT_BlockCondition.XML_ZYear+ "=? AND "+
                        tblT_BlockCondition.XML_Period+ "=? AND "+
                        tblT_BlockCondition.XML_ZDate+ "=? AND "+
                        tblT_BlockCondition.XML_Block+ "=? AND "+
                        tblT_BlockCondition.XML_TransLevel+ "=? AND "+
                        tblT_BlockCondition.XML_SKB+ "=? AND "+
                        tblT_BlockCondition.XML_BlockRow+ "=? AND "+
                        tblT_BlockCondition.XML_PokokSide+ "=? AND "+
                        tblT_BlockCondition.XML_CensusPoint+ "=? ",
                new String [] {SESS_ZYEAR,SESS_PERIOD,SESS_ZDATEFragment,block,"3",SESS_SKB,Baris_R,"R",PokokKe_R});

        if(Deleted>0){
            if(phase.equalsIgnoreCase("MATURE")){
                saveMatureR(blkDetail.getCompanyCode(),estate,blkDetail.getDivisi(),phase,todayDate,userLogin);
            }
            else{
                saveIMMatureR(blkDetail.getCompanyCode(),estate,blkDetail.getDivisi(),phase,todayDate,userLogin);
            }
        }else{
            if(phase.equalsIgnoreCase("MATURE")){
                saveMatureR(blkDetail.getCompanyCode(),estate,blkDetail.getDivisi(),phase,todayDate,userLogin);
            }
            else{
                saveIMMatureR(blkDetail.getCompanyCode(),estate,blkDetail.getDivisi(),phase,todayDate,userLogin);
            }
        }
    }

    private void saveMatureR(String companyCode,String estate,String divisi,String phase,String todayDate,UserLogin userLogin){
        long RowID = database.setData(new tblT_BlockCondition(0,companyCode, estate,
                SESS_ZYEAR, SESS_PERIOD, SESS_ZDATEFragment,
                3, blkDetail.getBlock(), SESS_SKB,
                Baris_R,PokokKe_R,
                PokokKe_R, "R",
                Kondisi_R, null,
                null, null,
                null, null,
                null, null,
                null, null,
                null,null,
                null,Piringan_R,
                PasarPanen_R,null,TunasPokok_R,
                Gawangan_R,Drainase_R,
                Ganoderma_R, Rayap_R,
                Oryctes_R, null,
                null,todayDate,
                userLogin.getNik(),todayDate, userLogin.getNik(),Flag,divisi,editRemark2.getText().toString()));
        new PreferenceManager(getActivity(),Constants.shared_name).setBarisR(Baris_R);
        database.commitTransaction();
        if(RowID>0){
            database.closeTransaction();
            HitungScoreMature();
        }
        else{
            database.closeTransaction();
            ToastMessage toastMessage = new ToastMessage(getActivity());
            toastMessage.shortMessage("TIDAK DAPAT DI SETS");
        }
    }
    private void saveIMMatureR(String companyCode,String estate,String divisi,String phase,String todayDate,UserLogin userLogin){
        long RowID = database.setData(new tblT_BlockCondition(0,companyCode, estate,
                SESS_ZYEAR, SESS_PERIOD, SESS_ZDATEFragment,
                3, blkDetail.getBlock(), SESS_SKB,
                Baris_R,PokokKe_R,
                PokokKe_R, "R",
                Kondisi_R, null,
                null, null,
                null, null,
                null, null,
                null, null,
                null,null,
                null,Piringan_R,
                null,PasarRintis_R,null,
                Gawangan_R,Drainase_R,
                null, Rayap_R,
                Oryctes_R, Sanitasi_R,
                Kacangan_R,todayDate,
                userLogin.getNik(),todayDate, userLogin.getNik(),Flag,divisi,editRemark2.getText().toString()));
        database.commitTransaction();
        if(RowID>0){
            database.closeTransaction();
            HitungScoreMature();
        }
        else{
            database.closeTransaction();
            ToastMessage toastMessage = new ToastMessage(getActivity());
            toastMessage.shortMessage("TIDAK DAPAT DI SETS");
        }
    }

    private void clearCheck(){
        rgpL1.clearCheck();
        rgpL2.clearCheck();
        rgpL3.clearCheck();
        rgpL4.clearCheck();
        rgpL5.clearCheck();
        rgpL6.clearCheck();
        rgpL7.clearCheck();
        rgpL8.clearCheck();
        Piringan_L = "0";
        PasarPanen_L ="0";
        TunasPokok_L ="0";
        Gawangan_L = "0";
        Drainase_L ="0";
        Ganoderma_L="0";
        Rayap_L="0";
        Oryctes_L = "0";
        Sanitasi_L = "0";
        Kacangan_L ="0";

    }

    private void clearCheck2(){
        rgpR1.clearCheck();
        rgpR2.clearCheck();
        rgpR3.clearCheck();
        rgpR4.clearCheck();
        rgpR5.clearCheck();
        rgpR6.clearCheck();
        rgpR7.clearCheck();
        rgpR8.clearCheck();
        Piringan_R = "0";
        PasarPanen_R ="0";
        TunasPokok_R ="0";
        Gawangan_R = "0";
        Drainase_R ="0";
        Ganoderma_R="0";
        Rayap_R="0";
        Oryctes_R = "0";
        Sanitasi_R = "0";
        Kacangan_R ="0";
    }


    private void DeteksiBaris(String brs,String flag){
        boolean statusB;
        int nilai1, nilai2;
        editTxtBarisLeft.setText(brs);
        if(flag.equalsIgnoreCase("back")){
            flagBackON();
            ClickYourFlagDisable();
        }
        if(flag.equalsIgnoreCase("front")){
            flagFrontON();
            ClickYourFlagDisable();
        }
    }


    private void DeteksiBarisR(String brs, String flag){
        boolean statusB;
        int nilai1, nilai2;
        editTxtBarisRight.setText(brs);
        if(flag.equalsIgnoreCase("back")){
            ClickYourFlagDisable();
            flagBackON();
        }
        if(flag.equalsIgnoreCase("front")){
            ClickYourFlagDisable();
            flagFrontON();
        }
    }

    private void ClickYourFlag(){
        flagBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                flagBack.setVisibility(View.GONE);
                flagFront.setVisibility(View.VISIBLE);
                Flag = "front";
            }

        });
        flagFront.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                flagBack.setVisibility(View.VISIBLE);
                flagFront.setVisibility(View.GONE);
                Flag = "back";
            }

        });
    }
    private void ClickYourFlagDisable(){
        flagBack.setClickable(false);
        flagFront.setClickable(false);
    }

    private void setRBL(String Kondisi){
        if(Kondisi.equalsIgnoreCase("Mati") || Kondisi.equalsIgnoreCase("Kosong")|| Kondisi.equalsIgnoreCase("HCV")){
            rbnblockgreen1.setEnabled(false);
            rbnblockgreen2.setEnabled(false);
            rbnblockgreen3.setEnabled(false);
            rbnblockgreen4.setEnabled(false);
            rbnblockgreen5.setEnabled(false);
            rbnblockgreen6.setEnabled(false);
            rbnblockgreen7.setEnabled(false);
            rbnblockgreen8.setEnabled(false);
            rbnblockred1.setEnabled(false);
            rbnblockred2.setEnabled(false);
            rbnblockred3.setEnabled(false);
            rbnblockred4.setEnabled(false);
            rbnblockred5.setEnabled(false);
            rbnblockred6.setEnabled(false);
            rbnblockred7.setEnabled(false);
            rbnblockred8.setEnabled(false);
            rbnblockyellow1.setEnabled(false);
            rbnblockyellow2.setEnabled(false);
            rbnblockyellow3.setEnabled(false);
            rbnblockyellow4.setEnabled(false);
            rbnblockyellow5.setEnabled(false);
            rbnblockyellow6.setEnabled(false);
            rbnblockyellow7.setEnabled(false);
            rbnblockyellow8.setEnabled(false);
            clearCheck();
        }  else{
            rbnblockgreen1.setEnabled(true);
            rbnblockgreen2.setEnabled(true);
            rbnblockgreen3.setEnabled(true);
            rbnblockgreen4.setEnabled(true);
            rbnblockgreen5.setEnabled(true);
            rbnblockgreen6.setEnabled(true);
            rbnblockgreen7.setEnabled(true);
            rbnblockgreen8.setEnabled(true);
            rbnblockred1.setEnabled(true);
            rbnblockred2.setEnabled(true);
            rbnblockred3.setEnabled(true);
            rbnblockred4.setEnabled(true);
            rbnblockred5.setEnabled(true);
            rbnblockred6.setEnabled(true);
            rbnblockred7.setEnabled(true);
            rbnblockred8.setEnabled(true);
            rbnblockyellow1.setEnabled(true);
            rbnblockyellow2.setEnabled(true);
            rbnblockyellow3.setEnabled(true);
            rbnblockyellow4.setEnabled(true);
            rbnblockyellow5.setEnabled(true);
            rbnblockyellow6.setEnabled(true);
            rbnblockyellow7.setEnabled(true);
            rbnblockyellow8.setEnabled(true);
        }
    }
    private void setRBR(String Kondisi){
        if(Kondisi.equalsIgnoreCase("Mati") || Kondisi.equalsIgnoreCase("Kosong") || Kondisi.equalsIgnoreCase("HCV")){
            rbnblockgreen1R.setEnabled(false);
            rbnblockgreen2R.setEnabled(false);
            rbnblockgreen3R.setEnabled(false);
            rbnblockgreen4R.setEnabled(false);
            rbnblockgreen5R.setEnabled(false);
            rbnblockgreen6R.setEnabled(false);
            rbnblockgreen7R.setEnabled(false);
            rbnblockgreen8R.setEnabled(false);
            rbnblockred1R.setEnabled(false);
            rbnblockred2R.setEnabled(false);
            rbnblockred3R.setEnabled(false);
            rbnblockred4R.setEnabled(false);
            rbnblockred5R.setEnabled(false);
            rbnblockred6R.setEnabled(false);
            rbnblockred7R.setEnabled(false);
            rbnblockred8R.setEnabled(false);
            rbnblockyellow1R.setEnabled(false);
            rbnblockyellow2R.setEnabled(false);
            rbnblockyellow3R.setEnabled(false);
            rbnblockyellow4R.setEnabled(false);
            rbnblockyellow5R.setEnabled(false);
            rbnblockyellow6R.setEnabled(false);
            rbnblockyellow7R.setEnabled(false);
            rbnblockyellow8R.setEnabled(false);
            clearCheck2();
        }
        else{
            rbnblockgreen1R.setEnabled(true);
            rbnblockgreen2R.setEnabled(true);
            rbnblockgreen3R.setEnabled(true);
            rbnblockgreen4R.setEnabled(true);
            rbnblockgreen5R.setEnabled(true);
            rbnblockgreen6R.setEnabled(true);
            rbnblockgreen7R.setEnabled(true);
            rbnblockgreen8R.setEnabled(true);
            rbnblockred1R.setEnabled(true);
            rbnblockred2R.setEnabled(true);
            rbnblockred3R.setEnabled(true);
            rbnblockred4R.setEnabled(true);
            rbnblockred5R.setEnabled(true);
            rbnblockred6R.setEnabled(true);
            rbnblockred7R.setEnabled(true);
            rbnblockred8R.setEnabled(true);
            rbnblockyellow1R.setEnabled(true);
            rbnblockyellow2R.setEnabled(true);
            rbnblockyellow3R.setEnabled(true);
            rbnblockyellow4R.setEnabled(true);
            rbnblockyellow5R.setEnabled(true);
            rbnblockyellow6R.setEnabled(true);
            rbnblockyellow7R.setEnabled(true);
            rbnblockyellow8R.setEnabled(true);
        }
    }

    private void HitungScoreMature(){
        editRemark1.setText("");
        editRemark2.setText("");
        editRemark1.setHint("Remark");
        editRemark2.setHint("Remark");
        MaximumDate();
    }
    public String MaximumDate(){
        database.closeTransaction();
        database.openTransaction();
        String MaximumDate = null;
        String[] a = new String[4];
        List<Object> listObject;
        a[0] = SESS_ZYEAR;
        a[1] = SESS_PERIOD;
        a[2] = block;
        a[3] = "3";
        String query ="SELECT a.BlockRow, a.CensusPoint," +
                "                a.Piringan, a.PasarRintis, a.PasarPanen, a.TunasPokok, a.Sanitasi, a.Gawangan, a.Drainase, " +
                "                a.Ganoderma, a.Kacangan, a.Rayap, a.Orcytes" +
                "                FROM tblT_BlockCondition a " +
                "                INNER JOIN " +
                "                (SELECT block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition WHERE translevel = 3 " +
                "                GROUP BY block, skb, blockrow, censuspoint ) b " +
                "                ON a.block=b.block and a.skb=b.skb AND a.blockrow=b.blockrow " +
                "                AND a.censuspoint=b.censuspoint AND a.zdate=b.zdate " +
                "                WHERE a.ZYear=? AND a.Period=? " +
                "                AND a.Block=? AND a.TransLevel= ?" +
                "                GROUP BY a.BlockRow, a.CensusPoint, a.Piringan, a.PasarRintis, a.PasarPanen, a.TunasPokok, a.Sanitasi, a.Gawangan, " +
                "                a.Drainase, a.Ganoderma, a.Kacangan, a.Rayap, a.Orcytes";
        String queryList ="SELECT a.BlockRow, a.CensusPoint," +
                "                a.Piringan, a.PasarRintis, a.PasarPanen, a.TunasPokok, a.Sanitasi, a.Gawangan, a.Drainase, " +
                "                a.Ganoderma, a.Kacangan, a.Rayap, a.Orcytes" +
                "                FROM tblT_BlockCondition a " +
                "                INNER JOIN " +
                "                (SELECT block, skb, blockrow, censuspoint,  max(zdate) as zdate from tblT_BlockCondition WHERE translevel = 3 " +
                "                GROUP BY block, skb, blockrow, censuspoint ) b " +
                "                ON a.block=b.block and a.skb=b.skb AND a.blockrow=b.blockrow " +
                "                AND a.censuspoint=b.censuspoint AND a.zdate=b.zdate " +
                "                WHERE a.ZYear=? AND a.Period=? " +
                "                AND a.Block=? AND a.TransLevel= ?" +
                "                GROUP BY a.BlockRow, a.CensusPoint, a.Piringan, a.PasarRintis, a.PasarPanen, a.TunasPokok, a.Sanitasi, a.Gawangan, " +
                "                a.Drainase, a.Ganoderma, a.Kacangan, a.Rayap, a.Orcytes";

        tblT_BlockCondition dtSource3= (tblT_BlockCondition) database.getDataFirstRaw(query,tblT_BlockCondition.TABLE_NAME,a);
        if(dtSource3==null){
            // MaXDate = null;
        }else{
            //MaXDate = dtSource3.getAcc_ZDate();
            listObject= database.getListDataRawQuery(queryList,tblT_BlockCondition.TABLE_NAME,a);
            HitungBobotDariWarna(dtSource3,listObject);
        }
        return  MaXDate;
    }

    private void HitungBobotDariWarna(tblT_BlockCondition dtSource,  List<Object> listObject){
        int color1 = (int)ColorVal_1(dtSource);
        int color2 = (int)ColorVal_2(dtSource);
        int color3 = (int)ColorVal_3(dtSource);
        int Delete = DeleteTransScore(dtSource);
        int TotalScore ;
        List<Object> data = listObject;
        List<String> arrFields =  new ArrayList<String>();

        arrFields.add(tblT_BlockCondition.XML_Piringan);
        arrFields.add(tblT_BlockCondition.XML_PasarRintis);
        arrFields.add(tblT_BlockCondition.XML_PasarPanen);
        arrFields.add(tblT_BlockCondition.XML_TunasPokok);
        arrFields.add(tblT_BlockCondition.XML_Sanitasi);
        arrFields.add(tblT_BlockCondition.XML_Gawangan);
        arrFields.add(tblT_BlockCondition.XML_Drainase);
        arrFields.add(tblT_BlockCondition.XML_Ganoderma);
        arrFields.add(tblT_BlockCondition.XML_Kacangan);
        arrFields.add(tblT_BlockCondition.XML_Rayap);
        arrFields.add(tblT_BlockCondition.XML_Orcytes);
        String[] namesArr = new String[arrFields.size()];

        if(Delete>0) {
            for (int i = 0; i < arrFields.size(); i++) {
                lval_color1_val = 0;
                lval_color2_val = 0;
                lval_color3_val= 0;
                lvar_color_cnt_tot = 0;
                lvar_color1_calc  = 0;
                lvar_color2_calc = 0;
                lvar_color3_calc = 0;
                lvar_color_calc_avg = 0;
                lvar_color_calc_tot = 0;
                lvar_final_score = 0;
                namesArr[i] = arrFields.get(i);
                int lvar_color1_cnt = 0;
                int lvar_color2_cnt = 0;
                int lvar_color3_cnt = 0;
                for (int dt = 0; dt < data.size(); dt++) {
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) data.get(dt);
                    if (namesArr[i].equals(tblT_BlockCondition.XML_Piringan)) {
                        if(dt3.getAcc_Piringan()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Piringan().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Piringan().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Piringan().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }

                    if (namesArr[i].equals(tblT_BlockCondition.XML_PasarRintis)) {
                        if(dt3.getAcc_PasarRintis()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_PasarRintis().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_PasarRintis().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_PasarRintis().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }

                    if (namesArr[i].equals(tblT_BlockCondition.XML_PasarPanen)) {
                        if(dt3.getAcc_PasarPanen()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_PasarPanen().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_PasarPanen().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_PasarPanen().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }

                    if (namesArr[i].equals(tblT_BlockCondition.XML_TunasPokok))  {
                        if(dt3.getAcc_TunasPokok()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_TunasPokok().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_TunasPokok().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_TunasPokok().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }

                    }

                    if (namesArr[i].equals(tblT_BlockCondition.XML_Sanitasi)) {
                        if(dt3.getAcc_Sanitasi()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Sanitasi().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Sanitasi().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Sanitasi().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }

                    }

                    if (namesArr[i].equals(tblT_BlockCondition.XML_Gawangan)) {
                        if(dt3.getAcc_Gawangan()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Gawangan().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Gawangan().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Gawangan().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }


                    }

                    if (namesArr[i].equals(tblT_BlockCondition.XML_Drainase)) {
                        if(dt3.getAcc_Drainase()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Drainase().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Drainase().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Drainase().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }


                    }

                    if (namesArr[i].equals(tblT_BlockCondition.XML_Ganoderma)) {
                        if(dt3.getAcc_Ganoderma()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Ganoderma().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Ganoderma().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Ganoderma().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }

                    }

                    if (namesArr[i].equals(tblT_BlockCondition.XML_Kacangan)) {
                        if(dt3.getAcc_Kacangan()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Kacangan().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Kacangan().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Kacangan().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }

                    }

                    if (namesArr[i].equals(tblT_BlockCondition.XML_Rayap)) {
                        if(dt3.getAcc_Rayap()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Rayap().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Rayap().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Rayap().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }

                    if (namesArr[i].equals(tblT_BlockCondition.XML_Orcytes)) {
                        if(dt3.getAcc_Orcytes()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Orcytes().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Orcytes().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Orcytes().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }


                    }
                }
                if(lvar_color1_cnt>0 || lvar_color2_cnt>0 || lvar_color3_cnt>0){
                    lvar_color_cnt_tot = lvar_color1_cnt + lvar_color2_cnt + lvar_color3_cnt;
                    lvar_color1_calc = lvar_color1_cnt * color1;
                    lvar_color2_calc = lvar_color2_cnt * color2;
                    lvar_color3_calc = lvar_color3_cnt * color3;
                    lvar_color_calc_tot = lvar_color1_calc + lvar_color2_calc + lvar_color3_calc;
                    double a = lvar_color_calc_tot;
                    double b = lvar_color_cnt_tot;
                    double total = a/b;
                    double roundOff = Math.round(total*100)/100.00;
                    lvar_color_calc_avg = roundOff;
                   /* ToastMessage toastMessage = new ToastMessage(getActivity());
                    toastMessage.shortMessage(data.get(i));*/
                    TotalScore = (int) finalScore(lvar_color_calc_avg);
                    insertScoreMature(lvar_color1_calc, lvar_color2_calc, lvar_color3_calc,namesArr[i]);
                    UpdateScoreMATURELevelKosong(companyCode, estate, blkDetail.getDivisi(), phase, TotalScore,namesArr[i]);
                }if(lvar_color1_cnt==0 && lvar_color2_cnt==0 && lvar_color3_cnt==0){
                    TotalScore = (int) finalScore(0);
                    insertScoreMature(0, 0, 0,namesArr[i]);
                    UpdateScoreMATURELevelKosong(companyCode, estate, blkDetail.getDivisi(), phase, 0,namesArr[i]);
                }

            }
            database.commitTransaction();
            database.closeTransaction();
            initDataSource1("Next");
            initDataSource2("Next");
        }
        else{
            for (int i = 0; i < arrFields.size(); i++) {
                lval_color1_val = 0;
                lval_color2_val = 0;
                lval_color3_val= 0;
                lvar_color_cnt_tot = 0;
                lvar_color1_calc  = 0;
                lvar_color2_calc = 0;
                lvar_color3_calc = 0;
                lvar_color_calc_avg = 0;
                lvar_color_calc_tot = 0;
                lvar_final_score = 0;
                namesArr[i] = arrFields.get(i);
                int lvar_color1_cnt = 0;
                int lvar_color2_cnt = 0;
                int lvar_color3_cnt = 0;
                for (int dt = 0; dt < data.size(); dt++) {
                    tblT_BlockCondition dt3 = (tblT_BlockCondition) data.get(dt);
                    if (namesArr[i].equals(tblT_BlockCondition.XML_Piringan)) {
                        if(dt3.getAcc_Piringan()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Piringan().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Piringan().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Piringan().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }

                    if (namesArr[i].equals(tblT_BlockCondition.XML_PasarRintis)) {
                        if(dt3.getAcc_PasarRintis()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_PasarRintis().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_PasarRintis().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_PasarRintis().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }

                    if (namesArr[i].equals(tblT_BlockCondition.XML_PasarPanen)) {
                        if(dt3.getAcc_PasarPanen()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_PasarPanen().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_PasarPanen().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_PasarPanen().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }

                    if (namesArr[i].equals(tblT_BlockCondition.XML_TunasPokok))  {
                        if(dt3.getAcc_TunasPokok()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_TunasPokok().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_TunasPokok().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_TunasPokok().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }


                    }
                    if (namesArr[i].equals(tblT_BlockCondition.XML_Sanitasi)) {
                        if(dt3.getAcc_Sanitasi()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Sanitasi().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Sanitasi().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Sanitasi().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }


                    }
                    if (namesArr[i].equals(tblT_BlockCondition.XML_Gawangan)) {
                        if(dt3.getAcc_Gawangan()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Gawangan().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Gawangan().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Gawangan().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }


                    }
                    if (namesArr[i].equals(tblT_BlockCondition.XML_Drainase)) {
                        if(dt3.getAcc_Drainase()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Drainase().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Drainase().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Drainase().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }


                    }
                    if (namesArr[i].equals(tblT_BlockCondition.XML_Ganoderma)) {
                        if(dt3.getAcc_Ganoderma()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Ganoderma().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Ganoderma().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Ganoderma().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }


                    }
                    if (namesArr[i].equals(tblT_BlockCondition.XML_Kacangan)) {
                        if(dt3.getAcc_Kacangan()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Kacangan().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Kacangan().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Kacangan().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }


                    }
                    if (namesArr[i].equals(tblT_BlockCondition.XML_Rayap)) {
                        if(dt3.getAcc_Rayap()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Rayap().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Rayap().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Rayap().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }
                    }
                    if (namesArr[i].equals(tblT_BlockCondition.XML_Orcytes)) {
                        if(dt3.getAcc_Orcytes()==null){
                            lvar_color1_cnt = 0;
                            lvar_color2_cnt = 0;
                            lvar_color3_cnt = 0;
                        }
                        else{
                            if (dt3.getAcc_Orcytes().equals("1")) {
                                lvar_color1_cnt = lvar_color1_cnt + 1;
                            }
                            if (dt3.getAcc_Orcytes().equals("2")) {
                                lvar_color2_cnt = lvar_color2_cnt + 1;
                            }
                            if (dt3.getAcc_Orcytes().equals("3")) {
                                lvar_color3_cnt = lvar_color3_cnt + 1;
                            }
                        }


                    }
                }
                if(lvar_color1_cnt>0 || lvar_color2_cnt>0 || lvar_color3_cnt>0){
                    lvar_color_cnt_tot = lvar_color1_cnt + lvar_color2_cnt + lvar_color3_cnt;
                    lvar_color1_calc = lvar_color1_cnt * color1;
                    lvar_color2_calc = lvar_color2_cnt * color2;
                    lvar_color3_calc = lvar_color3_cnt * color3;
                    lvar_color_calc_tot = lvar_color1_calc + lvar_color2_calc + lvar_color3_calc;
                    double a = lvar_color_calc_tot;
                    double b = lvar_color_cnt_tot;
                    double total = a/b;
                    double roundOff = Math.round(total*100)/100.00;
                    lvar_color_calc_avg = roundOff;
                    TotalScore = (int) finalScore(lvar_color_calc_avg);
                    insertScoreMature(lvar_color1_calc, lvar_color2_calc, lvar_color3_calc,namesArr[i]);
                    UpdateScoreMATURELevelKosong(companyCode, estate, blkDetail.getDivisi(), phase, TotalScore,namesArr[i]);
                }if(lvar_color1_cnt==0 && lvar_color2_cnt==0 && lvar_color3_cnt==0){
                    TotalScore = (int) finalScore(0);
                    insertScoreMature(0, 0, 0,namesArr[i]);
                    UpdateScoreMATURELevelKosong(companyCode, estate, blkDetail.getDivisi(), phase, 0,namesArr[i]);
                }

            }
            database.commitTransaction();
            database.closeTransaction();
            initDataSource1("Next");
            initDataSource2("Next");
        }

    }


    private double ColorVal_1(tblT_BlockCondition dtSource){
        double color1Val;
        String[] a = new String[2];
        a[0] = SESS_ZDATEFragment;
        a[1] = SESS_ZDATEFragment;
        String query = "SELECT Score1 FROM tblM_BlockConditionScore WHERE ScoreType=1 AND " +
                "Color = 1 AND  ValidFrom <= ? AND ValidTo >= ?";
        tblM_BlockConditionScore color1 = (tblM_BlockConditionScore) database.getDataFirstRaw(query, tblM_BlockConditionScore.TABLE_NAME, a);
        if(color1 == null){
            color1Val =0;
            lval_color1_val =0;
        }
        else{
            color1Val = color1.getAcc_Score1();
            lval_color1_val = (int)color1.getAcc_Score1();
        }
        return color1Val;
    }

    private double ColorVal_2(tblT_BlockCondition dtSource) {
        double color2Val;
        String[] b = new String[2];
        b[0] = SESS_ZDATEFragment;
        b[1] = SESS_ZDATEFragment;
        String query2 = "SELECT Score1 FROM tblM_BlockConditionScore WHERE ScoreType=1 AND " +
                "Color = 2 AND  ValidFrom <= ? AND ValidTo >= ?";
        tblM_BlockConditionScore color2 = (tblM_BlockConditionScore) database.getDataFirstRaw(query2, tblM_BlockConditionScore.TABLE_NAME, b);
        if(color2 == null){
            color2Val =0;
            lval_color2_val =0;
        }
        else{
            color2Val = color2.getAcc_Score1();
            lval_color2_val = (int)color2.getAcc_Score1();
        }
        return color2Val;
    }
    private double ColorVal_3(tblT_BlockCondition dtSource) {
        double color3Val;
        String[] c = new String[2];
        c[0] = SESS_ZDATEFragment;
        c[1] = SESS_ZDATEFragment;
        String query3 = "SELECT Score1 FROM tblM_BlockConditionScore WHERE ScoreType=1 AND " +
                "Color = 3 AND  ValidFrom <= ? AND ValidTo >= ?";
        tblM_BlockConditionScore color3 = (tblM_BlockConditionScore) database.getDataFirstRaw(query3, tblM_BlockConditionScore.TABLE_NAME, c);
        if(color3 == null){
            color3Val =0;
            lval_color3_val =0;
        }
        else{
            color3Val = color3.getAcc_Score1();
            lval_color3_val = (int)color3.getAcc_Score1();
        }
        return color3Val;
    }

    private int DeleteTransScore(tblT_BlockCondition dtSource){
        int Delete;
        Delete = database.deleteData(tblT_BlockConditionScore.TABLE_NAME,
                tblT_BlockCondition.XML_ZYear+ "=? AND "+
                        tblT_BlockCondition.XML_Period+ "=? AND "+
                        tblT_BlockCondition.XML_Block+ "=? AND "+
                        tblT_BlockCondition.XML_TransLevel+ "=? ",
                new String [] {SESS_ZYEAR,SESS_PERIOD,block,"3"});
        return Delete;
    }

    private double finalScore(double lvar_color_calc_avg) {
        double finalScore;
        String[] c = new String[4];
        c[0] = "2";
        c[1] = SESS_ZDATEFragment;
        c[2] = SESS_ZDATEFragment;
        c[3] = String.valueOf(lvar_color_calc_avg);
        String query3 = "SELECT Score1 FROM tblM_BlockConditionScore WHERE ScoreType=?" +
                "AND  ValidFrom <= ? AND ValidTo >= ?" +
                "AND ?  BETWEEN Interval1 AND Interval2";
        tblM_BlockConditionScore finalscore = (tblM_BlockConditionScore) database.getDataFirstRaw(query3, tblM_BlockConditionScore.TABLE_NAME, c);
        if(finalscore == null){
            finalScore =0;
            lvar_final_score = 0;
        }
        else{
            finalScore = finalscore.getAcc_Score1();
            lvar_final_score = (int)finalscore.getAcc_Score1();
        };
        return  finalScore;
    }


    private void insertScoreMature(double lvar_color1_calc,double lvar_color2_calc,double lvar_color3_calc, String field){
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        String todayDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT);
        ContentValues values = new ContentValues();
        values.put(tblT_BlockConditionScore.XML_CompanyCode,companyCode);
        values.put(tblT_BlockConditionScore.XML_Estate,estate);
        values.put(tblT_BlockConditionScore.XML_ZYear,SESS_ZYEAR);
        values.put(tblT_BlockConditionScore.XML_Period,SESS_PERIOD);
        values.put(tblT_BlockConditionScore.XML_Block,block);
        values.put(tblT_BlockConditionScore.XML_TransLevel,"3");
        values.put(tblT_BlockConditionScore.XML_Color,"1");
        values.put(field,lvar_color1_calc);
        database.insertDataSQL(tblT_BlockConditionScore.TABLE_NAME,values);
        ContentValues values2 = new ContentValues();
        values2.put(tblT_BlockConditionScore.XML_CompanyCode,companyCode);
        values2.put(tblT_BlockConditionScore.XML_Estate,estate);
        values2.put(tblT_BlockConditionScore.XML_ZYear,SESS_ZYEAR);
        values2.put(tblT_BlockConditionScore.XML_Period,SESS_PERIOD);
        values2.put(tblT_BlockConditionScore.XML_Block,block);
        values2.put(tblT_BlockConditionScore.XML_TransLevel,"3");
        values2.put(tblT_BlockConditionScore.XML_Color,"2");
        values2.put(field,lvar_color2_calc);
        database.insertDataSQL(tblT_BlockConditionScore.TABLE_NAME,values2);
        ContentValues values3 = new ContentValues();
        values3.put(tblT_BlockConditionScore.XML_CompanyCode,companyCode);
        values3.put(tblT_BlockConditionScore.XML_Estate,estate);
        values3.put(tblT_BlockConditionScore.XML_ZYear,SESS_ZYEAR);
        values3.put(tblT_BlockConditionScore.XML_Period,SESS_PERIOD);
        values3.put(tblT_BlockConditionScore.XML_Block,block);
        values3.put(tblT_BlockConditionScore.XML_TransLevel,"3");
        values3.put(tblT_BlockConditionScore.XML_Color,"3");
        values3.put(field,lvar_color3_calc);
        database.insertDataSQL(tblT_BlockConditionScore.TABLE_NAME,values3);
    }
    private void UpdateScoreMATURELevelKosong(String companyCode,String estate,String divisi,String phase, double lvar_final_score,String field){
        database.updateDataSQL(tblT_BlockCondition.TABLE_NAME,
                "UPDATE tblT_BlockCondition" +
                        "     SET "+field+" = ?"+
                        "     WHERE ZYear=? " +
                        "     AND Period=? " +
                        "     AND Block=? AND TransLevel = ? ",
                new Object [] {String.valueOf(lvar_final_score),SESS_ZYEAR,SESS_PERIOD,block,"0"});
    }

    private void DialogComment1(){
        final AlertDialog dialogBuilder = new AlertDialog.Builder(LevelPokokFragment.this.getContext()).create();
        LayoutInflater inflater = this.getLayoutInflater();
        dialogBuilder.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        View dialogView = inflater.inflate(R.layout.dialog_comment, null);
        dialogBuilder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        final EditText editText = (EditText) dialogView.findViewById(R.id.editRemark);
        Button button1 = (Button) dialogView.findViewById(R.id.btn_cancel);
        Button button2 = (Button) dialogView.findViewById(R.id.btn_write);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBuilder.dismiss();
                editRemark1.setText(editText.getText().toString());
                dialogBuilder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // DO SOMETHINGS
                dialogBuilder.dismiss();
                dialogBuilder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });

        dialogBuilder.setView(dialogView);
        dialogBuilder.show();
    }

    private void DialogComment2(){
        final AlertDialog dialogBuilder = new AlertDialog.Builder(LevelPokokFragment.this.getContext()).create();
        LayoutInflater inflater = this.getLayoutInflater();
        dialogBuilder.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        View dialogView = inflater.inflate(R.layout.dialog_comment, null);
        dialogBuilder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        final EditText editText = (EditText) dialogView.findViewById(R.id.editRemark);
        Button button1 = (Button) dialogView.findViewById(R.id.btn_cancel);
        Button button2 = (Button) dialogView.findViewById(R.id.btn_write);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBuilder.dismiss();
                editRemark2.setText(editText.getText().toString());
                dialogBuilder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // DO SOMETHINGS
                dialogBuilder.dismiss();
                dialogBuilder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });

        dialogBuilder.setView(dialogView);
        dialogBuilder.show();
    }

    View.OnTouchListener rbL1 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpL1.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };
    View.OnTouchListener rbR1 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpR1.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };
    View.OnTouchListener rbL2 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpL2.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rbR2 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpR2.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rbL3 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpL3.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rbR3 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpR3.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };
    View.OnTouchListener rbL4 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpL4.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rbR4 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpR4.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };
    View.OnTouchListener rbL5 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpL5.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rbR5 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpR5.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rbL6 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the but6on was already checked, uncheck them all
                rgpL6.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rbR6 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpR6.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rbL7 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpL7.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rbR7 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpR7.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rbL8 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpL8.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rbR8 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpR8.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

}
