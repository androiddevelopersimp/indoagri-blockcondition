package co.indoagri.blockcondition.fragment;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import java.util.Date;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.model.Data.BlockHdrc;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.Users.UserLogin;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.widget.ToastMessage;

import static co.indoagri.blockcondition.activity.BlockActivity.SESS_PERIOD;
import static co.indoagri.blockcondition.activity.BlockActivity.SESS_ZYEAR;
import static co.indoagri.blockcondition.activity.BlockActivity.relContainerSubTitle;
import static co.indoagri.blockcondition.activity.BlockActivity.relContainerTitle;
import static co.indoagri.blockcondition.activity.BlockActivity.titleFragment;

public class LevelBlockFragment extends Fragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    View view;
    FragmentLevelBlockListener fragmentLevelBlockListener;
    private static final String PHASE = "Phase";
    String block = null;
    String phase=null;
    TextView txtCodeBlock,txtCodePhase;
    String LastDate;
    private EditText editRemark;
    private RadioGroup rgpBlock1,rgpBlock2,rgpBlock3,rgpBlock4,rgpBlock5,rgpBlock6,rgpBlock7,rgpBlock8,rgpBlock9,rgpBlock10;
    private RadioButton rbnblockred1,rbnblockgreen1,rbnblockyellow1;
    private RadioButton rbnblockred2,rbnblockgreen2,rbnblockyellow2;
    private RadioButton rbnblockred3,rbnblockgreen3,rbnblockyellow3;
    private RadioButton rbnblockred4,rbnblockgreen4,rbnblockyellow4;
    private RadioButton rbnblockred5,rbnblockgreen5,rbnblockyellow5;
    private RadioButton rbnblockred6,rbnblockgreen6,rbnblockyellow6;
    private RadioButton rbnblockred7,rbnblockgreen7,rbnblockyellow7;
    private RadioButton rbnblockred8,rbnblockgreen8,rbnblockyellow8;
    private RadioButton rbnblockred9,rbnblockgreen9,rbnblockyellow9;
    private RadioButton rbnblockred10,rbnblockgreen10,rbnblockyellow10;
    private Button btnNext,btnBack;
    DatabaseHandler database;
    BlockHdrc blkDetail;
    String TitiRintis="0";
    String TitiPanen="0";
    String Parit="0";
    String Jalan="0";
    String Jembatan="0";
    String Tikus="0";
    String BenefecialWeed="0";
    String Curi="0";
    String Ganoderma="0";
    String Rayap="0";
    String Orcytes="0";
    boolean Navi = false;
    ImageButton btnShow,btnHide;
    String SESS_ZDATEFragment;
    public LevelBlockFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

        if(checkedId == R.id.rb_green1){
            TitiRintis="3";
        }else if(checkedId == R.id.rb_red1){
            TitiRintis="1";
        }else if(checkedId == R.id.rb_yellow1){
            TitiRintis="2";
        }else if(checkedId == R.id.rb_green2){
            Parit="3";
        }else if(checkedId == R.id.rb_red2){
            Parit="1";
        }else if(checkedId == R.id.rb_yellow2){
            Parit="2";
        }else if(checkedId == R.id.rb_green3){
            Jalan="3";
        }else if(checkedId == R.id.rb_red3){
            Jalan="1";
        }else if(checkedId == R.id.rb_yellow3){
            Jalan="2";
        }else if(checkedId == R.id.rb_green4){
            Jembatan="3";
        }else if(checkedId == R.id.rb_red4){
            Jembatan="1";
        }else if(checkedId == R.id.rb_yellow4){
            Jembatan="2";
        }else if(checkedId == R.id.rb_green5){
            Tikus="3";
        }else if(checkedId == R.id.rb_red5){
            Tikus="1";
        }else if(checkedId == R.id.rb_yellow5){
            Tikus="2";
        }else if(checkedId == R.id.rb_green6){
            BenefecialWeed="3";
        }else if(checkedId == R.id.rb_red6){
            BenefecialWeed="1";
        }else if(checkedId == R.id.rb_yellow6){
            BenefecialWeed="2";
        }else if(checkedId == R.id.rb_green7){
            Curi="3";
        }else if(checkedId == R.id.rb_red7){
            Curi="1";
        }else if(checkedId == R.id.rb_yellow7){
            Curi="2";
        }else if(checkedId == R.id.rb_green8){
            Ganoderma="3";
        }else if(checkedId == R.id.rb_red8){
            Ganoderma="1";
        }else if(checkedId == R.id.rb_yellow8){
            Ganoderma="2";
        }else if(checkedId == R.id.rb_green9){
            Rayap="3";
        }else if(checkedId == R.id.rb_red9){
            Rayap="1";
        }else if(checkedId == R.id.rb_yellow9){
            Rayap="2";
        }else if(checkedId == R.id.rb_green10){
            Orcytes="3";
        }else if(checkedId == R.id.rb_red10){
            Orcytes="1";
        }else if(checkedId == R.id.rb_yellow10){
            Orcytes="2";
        }

    }


    public interface FragmentLevelBlockListener {
        void onInputLevelBlock(String input,String block,String phase);
    }

    public static LevelBlockFragment newInstance() {
        return (new LevelBlockFragment());

    }
    public static LevelBlockFragment newInstance(String Block,String Phase) {
        LevelBlockFragment fragment = new LevelBlockFragment();
        Bundle args = new Bundle();
        args.putString(BlockHdrc.XML_BLOCK, Block);
        args.putString(PHASE, Phase);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        database = new DatabaseHandler(LevelBlockFragment.this.getContext());
        relContainerTitle.setVisibility(View.GONE);
        relContainerSubTitle.setVisibility(View.VISIBLE);
        Bundle args = getArguments();
        if (args != null) {
            block = getArguments().getString(BlockHdrc.XML_BLOCK);
            phase = getArguments().getString(PHASE);
            if(phase.equalsIgnoreCase("MATURE")){
                view = inflater.inflate(R.layout.fragment_level_block_mature, container, false);
            }else{
                view = inflater.inflate(R.layout.fragment_level_block_immature, container, false);
            }
        }
        return  view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null)
            block = getArguments().getString(BlockHdrc.XML_BLOCK);
        phase = getArguments().getString(PHASE);
        if (getActivity() instanceof FragmentLevelBlockListener)
            fragmentLevelBlockListener = (FragmentLevelBlockListener) getActivity();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnBack = (Button)view.findViewById(R.id.btnBack);
        btnNext = (Button)view.findViewById(R.id.btnNext);
        txtCodeBlock = (TextView)view.findViewById(R.id.txtCodeBlock);
        txtCodePhase = (TextView)view.findViewById(R.id.txtCodePhase);
        rgpBlock1 = (RadioGroup) view.findViewById(R.id.rb_group1);
        rgpBlock2 = (RadioGroup) view.findViewById(R.id.rb_group2);
        rgpBlock3 = (RadioGroup) view.findViewById(R.id.rb_group3);
        rgpBlock4 = (RadioGroup) view.findViewById(R.id.rb_group4);
        rgpBlock5 = (RadioGroup) view.findViewById(R.id.rb_group5);
        rgpBlock6 = (RadioGroup) view.findViewById(R.id.rb_group6);
        rgpBlock7 = (RadioGroup) view.findViewById(R.id.rb_group7);
        rgpBlock8 = (RadioGroup) view.findViewById(R.id.rb_group8);
        rgpBlock9 = (RadioGroup) view.findViewById(R.id.rb_group9);
        rgpBlock10 = (RadioGroup) view.findViewById(R.id.rb_group10);
        rgpBlock1.setOnCheckedChangeListener(this);
        rgpBlock2.setOnCheckedChangeListener(this);
        rgpBlock3.setOnCheckedChangeListener(this);
        rgpBlock4.setOnCheckedChangeListener(this);
        rgpBlock5.setOnCheckedChangeListener(this);
        rgpBlock6.setOnCheckedChangeListener(this);
        rgpBlock7.setOnCheckedChangeListener(this);
        rgpBlock8.setOnCheckedChangeListener(this);
        rgpBlock9.setOnCheckedChangeListener(this);
        rgpBlock10.setOnCheckedChangeListener(this);
        rbnblockgreen1 = (RadioButton) view.findViewById(R.id.rb_green1);
        rbnblockgreen2 = (RadioButton) view.findViewById(R.id.rb_green2);
        rbnblockgreen3 = (RadioButton) view.findViewById(R.id.rb_green3);
        rbnblockgreen4 = (RadioButton) view.findViewById(R.id.rb_green4);
        rbnblockgreen5 = (RadioButton) view.findViewById(R.id.rb_green5);
        rbnblockgreen6 = (RadioButton) view.findViewById(R.id.rb_green6);
        rbnblockgreen7 = (RadioButton) view.findViewById(R.id.rb_green7);
        rbnblockgreen8 = (RadioButton) view.findViewById(R.id.rb_green8);
        rbnblockgreen9 = (RadioButton) view.findViewById(R.id.rb_green9);
        rbnblockgreen10 = (RadioButton) view.findViewById(R.id.rb_green10);

        rbnblockred1 = (RadioButton) view.findViewById(R.id.rb_red1);
        rbnblockred2 = (RadioButton) view.findViewById(R.id.rb_red2);
        rbnblockred3 = (RadioButton) view.findViewById(R.id.rb_red3);
        rbnblockred4 = (RadioButton) view.findViewById(R.id.rb_red4);
        rbnblockred5 = (RadioButton) view.findViewById(R.id.rb_red5);
        rbnblockred6 = (RadioButton) view.findViewById(R.id.rb_red6);
        rbnblockred7 = (RadioButton) view.findViewById(R.id.rb_red7);
        rbnblockred8 = (RadioButton) view.findViewById(R.id.rb_red8);
        rbnblockred9 = (RadioButton) view.findViewById(R.id.rb_red9);
        rbnblockred10 = (RadioButton) view.findViewById(R.id.rb_red10);
        rbnblockyellow1 = (RadioButton) view.findViewById(R.id.rb_yellow1);
        rbnblockyellow2 = (RadioButton) view.findViewById(R.id.rb_yellow2);
        rbnblockyellow3 = (RadioButton) view.findViewById(R.id.rb_yellow3);
        rbnblockyellow4 = (RadioButton) view.findViewById(R.id.rb_yellow4);
        rbnblockyellow5 = (RadioButton) view.findViewById(R.id.rb_yellow5);
        rbnblockyellow6 = (RadioButton) view.findViewById(R.id.rb_yellow6);
        rbnblockyellow7 = (RadioButton) view.findViewById(R.id.rb_yellow7);
        rbnblockyellow8 = (RadioButton) view.findViewById(R.id.rb_yellow8);
        rbnblockyellow9 = (RadioButton) view.findViewById(R.id.rb_yellow9);
        rbnblockyellow10 = (RadioButton) view.findViewById(R.id.rb_yellow10);
        btnNext.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        editRemark = (EditText)view.findViewById(R.id.editRemark);
        btnShow = (ImageButton)view.findViewById(R.id.btnShow);
        btnHide = (ImageButton)view.findViewById(R.id.btnHide);
        btnShow.setOnClickListener(this);
        btnHide.setOnClickListener(this);
        SESS_ZDATEFragment = new PreferenceManager(getActivity(), Constants.shared_name).getDatein();
        rbnblockgreen1.setOnTouchListener(rb1);
        rbnblockyellow1.setOnTouchListener(rb1);
        rbnblockred1.setOnTouchListener(rb1);

        rbnblockgreen2.setOnTouchListener(rb2);
        rbnblockyellow2.setOnTouchListener(rb2);
        rbnblockred2.setOnTouchListener(rb2);

        rbnblockgreen3.setOnTouchListener(rb3);
        rbnblockyellow3.setOnTouchListener(rb3);
        rbnblockgreen3.setOnTouchListener(rb3);

        rbnblockgreen4.setOnTouchListener(rb4);
        rbnblockyellow4.setOnTouchListener(rb4);
        rbnblockred4.setOnTouchListener(rb4);

        rbnblockgreen5.setOnTouchListener(rb5);
        rbnblockyellow5.setOnTouchListener(rb5);
        rbnblockred5.setOnTouchListener(rb5);

        rbnblockgreen6.setOnTouchListener(rb6);
        rbnblockyellow6.setOnTouchListener(rb6);
        rbnblockred6.setOnTouchListener(rb6);

        rbnblockgreen7.setOnTouchListener(rb7);
        rbnblockyellow7.setOnTouchListener(rb7);
        rbnblockred7.setOnTouchListener(rb7);

        rbnblockgreen8.setOnTouchListener(rb8);
        rbnblockyellow8.setOnTouchListener(rb8);
        rbnblockred8.setOnTouchListener(rb8);

        rbnblockgreen9.setOnTouchListener(rb9);
        rbnblockyellow9.setOnTouchListener(rb9);
        rbnblockred9.setOnTouchListener(rb9);

        rbnblockgreen10.setOnTouchListener(rb10);
        rbnblockyellow10.setOnTouchListener(rb10);
        rbnblockred10.setOnTouchListener(rb10);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentLevelBlockListener) {
            fragmentLevelBlockListener = (FragmentLevelBlockListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentLevelBlockListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentLevelBlockListener = null;
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        this.getArguments().clear();
    }
    @Override
    public void onStart() {
        super.onStart();
        btnBack.setText(getResources().getString(R.string.back));
        btnNext.setText(getResources().getString(R.string.skb));
        initView();
    }

    void initView(){
        txtCodeBlock.setText(block);
        txtCodePhase.setText(phase);
        titleFragment.setText("Condition of Block");
        GetData();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                Save(blkDetail.getCompanyCode(),blkDetail.getEstate(),blkDetail.getDivisi(),phase);
                Navi = false;
                break;
            case R.id.btnShow:
                ToastMessage toastMessage = new ToastMessage(getActivity());
                toastMessage.shortMessage("Show");
                btnShow.setVisibility(View.GONE);
                btnHide.setVisibility(View.VISIBLE);
                editRemark.setVisibility(View.VISIBLE);
                break;
            case R.id.btnHide:
                ToastMessage toastMessage2 = new ToastMessage(getActivity());
                toastMessage2.shortMessage("HIDE");
                btnHide.setVisibility(View.GONE);
                btnShow.setVisibility(View.VISIBLE);
                editRemark.setVisibility(View.GONE);
                break;
            case R.id.btnNext:
                Navi = true;
                Save(blkDetail.getCompanyCode(),blkDetail.getEstate(),blkDetail.getDivisi(),phase);
                break;
            default:
                break;
        }
    }
    void GetData() {
        database.openTransaction();
        blkDetail = (BlockHdrc) database.getDataFirst(false, BlockHdrc.TABLE_NAME, null,
                    BlockHdrc.XML_BLOCK + "=? ",
                    new String [] {block},
                    BlockHdrc.XML_BLOCK, null, BlockHdrc.XML_BLOCK, null);
            String[] a = new String[4];
            a[0] = SESS_ZYEAR;
            a[1] = SESS_PERIOD;
            a[2] = block;
            a[3] = "1";
            String query = "SELECT MAX(ZDate) as ZDate FROM tblT_BlockCondition WHERE ZYear=?" +
                    "                             AND Period=? AND Block = ? AND TRANSLEVEL = ?";
            tblT_BlockCondition lastDate = (tblT_BlockCondition) database.getDataFirstRaw(query,tblT_BlockCondition.TABLE_NAME,a);
           database.closeTransaction();
            if(lastDate.getAcc_ZDate()!=null){
                setData(lastDate);
                LastDate = lastDate.getAcc_ZDate();
            }
    }

    void setData(tblT_BlockCondition lastDate){
        database.openTransaction();
            List<Object> listObject =  database.getListData(true, tblT_BlockCondition.TABLE_NAME, null,
                    tblT_BlockCondition.XML_ZYear+ "=? and "+
                            tblT_BlockCondition.XML_Period+ "=? and "+
                            tblT_BlockCondition.XML_Block+ "=? and "+
                            tblT_BlockCondition.XML_TransLevel+ "=? and "+
                            tblT_BlockCondition.XML_ZDate+ "=? ",
                    new String [] {SESS_ZYEAR,SESS_PERIOD,block,"1",lastDate.getAcc_ZDate()},
                    null, null, null, "1");
            database.closeTransaction();
            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    tblT_BlockCondition dt = (tblT_BlockCondition) listObject.get(i);
                    setDataScore(dt);
                }
            }
    }

    void Save(String companyCode,String estate,String divisi,String phase){
        String todayDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT);
        database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.deleteData(tblT_BlockCondition.TABLE_NAME,
                    tblT_BlockCondition.XML_TransLevel+ "=? AND "+
                            tblT_BlockCondition.XML_ZYear+ "=? AND "+
                            tblT_BlockCondition.XML_Period+ "=? AND "+
                            tblT_BlockCondition.XML_ZDate+ "=? AND "+
                            tblT_BlockCondition.XML_Block+ "=? ",
                    new String [] {"1",SESS_ZYEAR,SESS_PERIOD,SESS_ZDATEFragment,block});

                if(phase.equalsIgnoreCase("MATURE")){
                    boolean res = false;
                    res = saveMature(companyCode,estate,divisi,phase,todayDate,userLogin);
                    if(res){
                        updateDataMature(companyCode,estate,divisi,phase);
                    }
                    else{
                        updateDataMature(companyCode,estate,divisi,phase);
                    }
                }
                else{
                    boolean res = false;
                    res = saveIMMature(companyCode,estate,divisi,phase,todayDate,userLogin);
                    if(res){
                        updateDataIMMature(companyCode,estate,divisi,phase);
                    }
                    else{
                        updateDataIMMature(companyCode,estate,divisi,phase);
                    }
                }
    }

    private boolean saveMature(String companyCode,String estate,String divisi,
                               String phase,String todayDate, UserLogin userLogin){
        boolean res = false;
        long RowId = 0;
        try {
            RowId = database.setData(new tblT_BlockCondition(0, companyCode, estate,
                    SESS_ZYEAR, SESS_PERIOD, SESS_ZDATEFragment,
                    1, blkDetail.getBlock(), "0",
                    "0", "0",
                    "0", "0",
                    "0", Jalan,
                    Jembatan, Parit,
                    TitiRintis, "0",
                    Tikus, Curi,
                    BenefecialWeed, null,null,
                    null,null,
                    null, null,
                    null, null,
                    null, null,
                    Ganoderma, Rayap,
                    Orcytes, null,
                    null, todayDate,
                    userLogin.getNik(), todayDate, userLogin.getNik(),null,divisi,editRemark.getText().toString()));
            database.commitTransaction();
            if(RowId>0){
                res = true;
            }
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
            res = false;
        }finally{
            database.closeTransaction();
            if(RowId>0){
                res = true;
            }
        }
        return res;
    }

    private boolean saveIMMature(String companyCode,String estate,String divisi,
                                 String phase,String todayDate, UserLogin userLogin) {
        boolean res = false;
        long RowId = 0;
        try{
            database.setData(new tblT_BlockCondition(0, companyCode, estate,
                    SESS_ZYEAR, SESS_PERIOD, SESS_ZDATEFragment,
                    1, blkDetail.getBlock(), "0",
                    "0", "0",
                    "0", "0",
                    "0", Jalan,
                    Jembatan, Parit,
                    "0", TitiRintis,
                    Tikus, "0",
                    BenefecialWeed, null,null,
                    null, null,
                    null,null,
                    null, null,
                    null, null,
                    null, Rayap,
                    Orcytes, null,
                    null, todayDate,
                    userLogin.getNik(), todayDate, userLogin.getNik(),null,divisi,editRemark.getText().toString()));
            database.commitTransaction();
            if(RowId>0){
                res = true;
            }
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
            res = false;
        }finally{
            database.closeTransaction();
            if(RowId>0){
                res = true;
            }
        }
        return res;
    }



    void updateDataMature(String companyCode,String estate,String divisi,String phase){
        database.openTransaction();
        database.updateDataSQL(tblT_BlockCondition.TABLE_NAME," UPDATE "+ tblT_BlockCondition.TABLE_NAME+
                        " SET TitiPanen =?, " +
                        "Parit =?, " +
                        "Jalan =?, " +
                        "Jembatan =?, " +
                        "Tikus =?, " +
                        "BW =?, " +
                        "Pencurian =?, " +
                        "Ganoderma =?, " +
                        "Rayap =?, " +
                        "Orcytes =?, " +
                        "Remark =? " +
                        "WHERE ZYear=? AND Period=? " +
                        "AND Block=? AND TransLevel= ? ",
                new Object [] {TitiRintis,Parit,Jalan,Jembatan,Tikus,BenefecialWeed,
                        Curi,Ganoderma,Rayap,Orcytes,editRemark.getText().toString(),SESS_ZYEAR,SESS_PERIOD,block,"0"});
                if(Navi) {
                    Navi =false;
                    database.commitTransaction();
                    database.closeTransaction();
                    if (fragmentLevelBlockListener != null)
                        fragmentLevelBlockListener.onInputLevelBlock(getResources().getString(R.string.next),blkDetail.getBlock(),phase);
                }
                else {
                    Navi =true;
                    database.commitTransaction();
                    database.closeTransaction();
                    if (fragmentLevelBlockListener != null)
                        fragmentLevelBlockListener.onInputLevelBlock(getResources().getString(R.string.back),blkDetail.getBlock(),phase);
                }

    }

    void updateDataIMMature(String companyCode,String estate,String divisi,String phase){
        database.openTransaction();
        database.updateDataSQL(tblT_BlockCondition.TABLE_NAME,"UPDATE "+ tblT_BlockCondition.TABLE_NAME+
                            " SET TitiRintis =?," +
                            "Parit =?, " +
                            "Jalan =?, " +
                            "Jembatan =?, " +
                            "Tikus =?, " +
                            "BW =?, " +
                            "Rayap =?, " +
                            "Orcytes =?, " +
                            "Remark =? " +
                            "WHERE ZYear=? AND Period=? " +
                            "AND Block=? AND TransLevel= ? ",
                    new Object [] {TitiRintis,Parit,Jalan,Jembatan,Tikus,BenefecialWeed,Rayap,Orcytes,editRemark.getText().toString(),SESS_ZYEAR,SESS_PERIOD,block,"0"});
                if(Navi) {
                    Navi =false;
                    database.commitTransaction();
                    database.closeTransaction();
                    if (fragmentLevelBlockListener != null)
                        fragmentLevelBlockListener.onInputLevelBlock(getResources().getString(R.string.next),blkDetail.getBlock(),phase);
                }
                else {
                    Navi =true;
                    database.commitTransaction();
                    database.closeTransaction();
                    if (fragmentLevelBlockListener != null)
                        fragmentLevelBlockListener.onInputLevelBlock(getResources().getString(R.string.back),blkDetail.getBlock(),phase);
                }
    }

    void setDataScore(tblT_BlockCondition dataScore){
        if(phase.equalsIgnoreCase("MATURE")){
            setScorePanen(dataScore);
            setScoreParit(dataScore);
            setScoreJalan(dataScore);
            setScoreJembatan(dataScore);
            setScoreTikus(dataScore);
            setScoreBW(dataScore);
            setScoreCuri(dataScore);
            setScoreGanoderma(dataScore);
            setScoreRayap(dataScore);
            setScoreOrcytes(dataScore);
            setRemark(dataScore);
        }
        else{
            setScoreRintis(dataScore);
            setScoreParit(dataScore);
            setScoreJalan(dataScore);
            setScoreJembatan(dataScore);
            setScoreTikus(dataScore);
            setScoreBW(dataScore);
            setScoreRayap(dataScore);
            setScoreOrcytes(dataScore);
            setRemark(dataScore);
        }
    }

    void setScorePanen(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_TitiPanen().equalsIgnoreCase("1")) {
            rbnblockred1.setChecked(true);
        }
        if (dataScore.getAcc_TitiPanen().equalsIgnoreCase("2")) {
            rbnblockyellow1.setChecked(true);
        }
        if (dataScore.getAcc_TitiPanen().equalsIgnoreCase("3")) {
            rbnblockgreen1.setChecked(true);
        }
    }

    void setScoreRintis(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_TitiRintis().equalsIgnoreCase("1")) {
            rbnblockred1.setChecked(true);
        }
        if (dataScore.getAcc_TitiRintis().equalsIgnoreCase("2")) {
            rbnblockyellow1.setChecked(true);
        }
        if (dataScore.getAcc_TitiRintis().equalsIgnoreCase("3")) {
            rbnblockgreen1.setChecked(true);
        }
    }
    void setScoreParit(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_Parit().equalsIgnoreCase("1")) {
            rbnblockred2.setChecked(true);
        }
        if (dataScore.getAcc_Parit().equalsIgnoreCase("2")) {
            rbnblockyellow2.setChecked(true);
        }
        if (dataScore.getAcc_Parit().equalsIgnoreCase("3")) {
            rbnblockgreen2.setChecked(true);
        }
    }
    void setScoreJalan(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_Jalan().equalsIgnoreCase("1")) {
            rbnblockred3.setChecked(true);
        }
        if (dataScore.getAcc_Jalan().equalsIgnoreCase("2")) {
            rbnblockyellow3.setChecked(true);
        }
        if (dataScore.getAcc_Jalan().equalsIgnoreCase("3")) {
            rbnblockgreen3.setChecked(true);
        }
    }
    void setScoreJembatan(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_Jembatan().equalsIgnoreCase("1")) {
            rbnblockred4.setChecked(true);
        }
        if (dataScore.getAcc_Jembatan().equalsIgnoreCase("2")) {
            rbnblockyellow4.setChecked(true);
        }
        if (dataScore.getAcc_Jembatan().equalsIgnoreCase("3")) {
            rbnblockgreen4.setChecked(true);
        }
    }
    void setScoreTikus(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_Tikus().equalsIgnoreCase("1")) {
            rbnblockred5.setChecked(true);
        }
        if (dataScore.getAcc_Tikus().equalsIgnoreCase("2")) {
            rbnblockyellow5.setChecked(true);
        }
        if (dataScore.getAcc_Tikus().equalsIgnoreCase("3")) {
            rbnblockgreen5.setChecked(true);
        }
    }
    void setScoreBW(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_BW().equalsIgnoreCase("1")) {
            rbnblockred6.setChecked(true);
        }
        if (dataScore.getAcc_BW().equalsIgnoreCase("2")) {
            rbnblockyellow6.setChecked(true);
        }
        if (dataScore.getAcc_BW().equalsIgnoreCase("3")) {
            rbnblockgreen6.setChecked(true);
        }
    }
    void setScoreCuri(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_Pencurian().equalsIgnoreCase("1")) {
            rbnblockred7.setChecked(true);
        }
        if (dataScore.getAcc_Pencurian().equalsIgnoreCase("2")) {
            rbnblockyellow7.setChecked(true);
        }
        if (dataScore.getAcc_Pencurian().equalsIgnoreCase("3")) {
            rbnblockgreen7.setChecked(true);
        }
    }
    void setScoreGanoderma(tblT_BlockCondition dataScore){
        if(dataScore.getAcc_Ganoderma()==null){
            rbnblockred8.setChecked(false);
            rbnblockyellow8.setChecked(false);
            rbnblockgreen8.setChecked(false);
        }else {
            if (dataScore.getAcc_Ganoderma().equalsIgnoreCase("1")) {
                rbnblockred8.setChecked(true);
            }
            if (dataScore.getAcc_Ganoderma().equalsIgnoreCase("2")) {
                rbnblockyellow8.setChecked(true);
            }
            if (dataScore.getAcc_Ganoderma().equalsIgnoreCase("3")) {
                rbnblockgreen8.setChecked(true);
            }
        }
    }
    void setScoreRayap(tblT_BlockCondition dataScore){
        if(dataScore.getAcc_Rayap()==null){
            rbnblockred9.setChecked(false);
            rbnblockyellow9.setChecked(false);
            rbnblockgreen9.setChecked(false);
        }else {
            if (dataScore.getAcc_Rayap().equalsIgnoreCase("1")) {
                rbnblockred9.setChecked(true);
            }
            if (dataScore.getAcc_Rayap().equalsIgnoreCase("2")) {
                rbnblockyellow9.setChecked(true);
            }
            if (dataScore.getAcc_Rayap().equalsIgnoreCase("3")) {
                rbnblockgreen9.setChecked(true);
            }
        }
    }
    void setScoreOrcytes(tblT_BlockCondition dataScore){
        if(dataScore.getAcc_Orcytes()==null){
            rbnblockred10.setChecked(false);
            rbnblockyellow10.setChecked(false);
            rbnblockgreen10.setChecked(false);
        }else {
            if (dataScore.getAcc_Orcytes().equalsIgnoreCase("1")) {
                rbnblockred10.setChecked(true);
            }
            if (dataScore.getAcc_Orcytes().equalsIgnoreCase("2")) {
                rbnblockyellow10.setChecked(true);
            }
            if (dataScore.getAcc_Orcytes().equalsIgnoreCase("3")) {
                rbnblockgreen10.setChecked(true);
            }
        }
    }

    void setRemark(tblT_BlockCondition dataScore){
        if(dataScore.getAcc_Remark()==null){
            editRemark.setText("");
            editRemark.setHint("Remark");
        }else {
            editRemark.setText(dataScore.getAcc_Remark());
        }
    }

    View.OnTouchListener rb1 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpBlock1.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };
    View.OnTouchListener rb2 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpBlock2.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };
    View.OnTouchListener rb3 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpBlock3.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rb4 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpBlock4.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rb5 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpBlock5.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rb6 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpBlock6.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };
    View.OnTouchListener rb7 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpBlock7.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rb8 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpBlock8.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };
    View.OnTouchListener rb9 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpBlock9.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };

    View.OnTouchListener rb10 = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (((RadioButton) v).isChecked()) {
                // If the button was already checked, uncheck them all
                rgpBlock10.clearCheck();
                // Prevent the system from re-checking it
                return true;
            }
            return false;
        }
    };
}
