package co.indoagri.blockcondition.fragment;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.AdapterRole;
import co.indoagri.blockcondition.animation.SlideUpandDown;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.dialog.DialogConfirmation;
import co.indoagri.blockcondition.dialog.DialogConfirmationFragment;
import co.indoagri.blockcondition.dialog.DialogNotification;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.handler.JsonHandler;
import co.indoagri.blockcondition.handler.UpdateHandler;
import co.indoagri.blockcondition.listener.DialogConfirmationFragmentListener;
import co.indoagri.blockcondition.listener.DialogNotificationListener;
import co.indoagri.blockcondition.model.Apps;
import co.indoagri.blockcondition.model.Users.Employee;
import co.indoagri.blockcondition.model.Users.ForemanActive;
import co.indoagri.blockcondition.model.Users.Role;
import co.indoagri.blockcondition.model.Users.UserApp;
import co.indoagri.blockcondition.model.Users.UserLogin;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.widget.ToastMessage;

public class AuthLoginFragment extends Fragment implements View.OnClickListener,
        DialogConfirmationFragmentListener,
        DialogNotificationListener {
    View view;
    ImageButton imgLoginSettings,img_restore_database;
    FragmentLoginListener fragmentLoginListener;
    EditText edt_login_username,edt_login_password;
    Button btn_login_submit;
    TextView txt_login_app_version;
    private DialogProgress dialogProgress;
    private static final String ARG_PARAM1 = Constants.PARAM1;
    private String mParam1,role;
    Apps apps;
    private CheckUpdateAsyncTask checkAysncTask;
    private AdapterRole adapter;
    private Spinner spn_login_role;
    String [] strRoles;
    List<Role> lstRole;
    DatabaseHandler database;
    public interface FragmentLoginListener {
        void onInterfaceLogin(String input);
    }
    public AuthLoginFragment() {
        // Required empty public constructor
    }

    public static AuthLoginFragment newInstance(String param1) {
        AuthLoginFragment fragment = new AuthLoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        database = new DatabaseHandler(AuthLoginFragment.this.getContext());
        view = inflater.inflate(R.layout.fragment_login, container, false);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null)
            mParam1 = getArguments().getString(ARG_PARAM1);
        if (getActivity() instanceof FragmentLoginListener)
            fragmentLoginListener = (FragmentLoginListener) getActivity();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spn_login_role = (Spinner) view.findViewById(R.id.spn_login_role);
        btn_login_submit = (Button) view.findViewById(R.id.btnLogin);
        imgLoginSettings = (ImageButton) view.findViewById(R.id.imgLoginSettings);
        img_restore_database = (ImageButton) view.findViewById(R.id.img_restore_database);
        edt_login_username = (EditText) view.findViewById(R.id.edt_login_username);
        edt_login_password = (EditText) view.findViewById(R.id.edt_login_password);
        txt_login_app_version = (TextView)view.findViewById(R.id.txt_login_app_version);

        btn_login_submit.setOnClickListener(this);
        imgLoginSettings.setOnClickListener(this);
        img_restore_database.setOnClickListener(this);

        txt_login_app_version.setText(String.format(getResources().getString(R.string.settings_version),
                Constants.const_VersionName));
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:

               /* boolean isConnected = BroadcastConnectorHandler.isConnect(getActivity());
                if(isConnected){
                    checkAysncTask = new CheckUpdateAsyncTask();
                    checkAysncTask.execute();
                }else{*/
                    login();

                break;
            case R.id.imgLoginSettings:
                if (fragmentLoginListener != null)
                    fragmentLoginListener.onInterfaceLogin(getResources().getString(R.string.settings_sync_master));
                break;
            case R.id.img_restore_database:
                if(fragmentLoginListener != null)
                    fragmentLoginListener.onInterfaceLogin(getResources().getString(R.string.restore_database));
            default:
                break;
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentLoginListener) {
            fragmentLoginListener = (FragmentLoginListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentLoginListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        fragmentLoginListener = null;

    }
    @Override
    public void onStart() {
        super.onStart();
        long today = new Date().getTime();
    //    edt_login_username.setText(String.valueOf(today));
        SlideUpandDown slideUpandDown = new SlideUpandDown(getActivity());
        slideUpandDown.AnimationSide(img_restore_database);
        slideUpandDown.AnimationSideRight(imgLoginSettings);
        createSpinner();
    }

    public void ShowDialogFragmentConfirmation(int values,String message,String informasi){
        DialogConfirmationFragment dialogConfirmFragment = new DialogConfirmationFragment();

        Bundle bundle = new Bundle();

        bundle.putString("title", getResources().getString(R.string.informasi));
        bundle.putString("message", getResources().getString(R.string.layanan_lokasi));
        bundle.putInt("id", values);

        dialogConfirmFragment.setArguments(bundle);

        dialogConfirmFragment.setTargetFragment(AuthLoginFragment.this, 1);
        dialogConfirmFragment.show(getFragmentManager(), "dialog");
    }

    @Override
    public void onConfirmFragmentOK(int id) {
        switch (id) {
            case 1:
                ToastMessage toastMessage = new ToastMessage(getActivity());
                toastMessage.shortMessage("Testing Confirmation OK");
            default:
                break;
        }
    }


    @Override
    public void onOK(boolean is_finish) {
        edt_login_username.requestFocus();
    }



    private void createSpinner(){
        strRoles= getResources().getStringArray(R.array.roles);
        lstRole = new ArrayList<Role>();

        for(String rol : strRoles){
            Role role = new Role(rol);

            lstRole.add(role);
        }

        role = lstRole.get(0).getName();

        adapter = new AdapterRole(getActivity(), lstRole, R.layout.item_role);
        spn_login_role.setAdapter(adapter);

        spn_login_role.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                if(position==0){
                    role = String.valueOf(lstRole.get(position).getName());
                }
                if(position>0){
                    role = String.valueOf(lstRole.get(position).getName());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private class CheckUpdateAsyncTask extends AsyncTask<Void, Void, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!getActivity().isFinishing()){
                dialogProgress = new DialogProgress(getActivity(), getResources().getString(R.string.wait));
                dialogProgress.show();
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            String json = new UpdateHandler().checkVersion();

            return json;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            if(!TextUtils.isEmpty(json)){
                Log.d("tag", json);

                apps = new JsonHandler(json).getApps();

                if(apps != null){
                    String curVersion = Constants.const_APIVERSION;

                    if(!apps.getVersion().equals(curVersion)){
                        new DialogConfirmation(getActivity(), getResources().getString(R.string.informasi),
                                getResources().getString(R.string.apps_update), null, 1).show();
                    }else{
                        login();
                    }
                }else{
                    login();
                }

            }else{
                login();
            }
        }
    }


   /* private void login(){
            LocationManager location_manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            if(location_manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                DatabaseHandler database = new DatabaseHandler(getActivity());

                String username = edt_login_username.getText().toString().trim();
                String password = edt_login_password.getText().toString().trim();

                long today = new Date().getTime();

                database.openTransaction();
                UserApp userApp = (UserApp) database.getDataFirst(false, UserApp.TABLE_NAME, null,
                        UserApp.XML_USERNAME + "=? " + " and " +
                                UserApp.XML_PASSWORD + "=?" + " and " +
                                UserApp.XML_VALID_TO + ">=?",
                        new String [] {username, password, String.valueOf(today)},
                        null, null, null, null);
                database.closeTransaction();

                if(userApp != null){
                    String nik = userApp.getNik();

                    database.openTransaction();
                    Employee employee = (Employee) database.getDataFirst(false, Employee.TABLE_NAME, null,
                            Employee.XML_NIK + "=?",
                            new String [] {nik},
                            null, null, null, null);
                    database.closeTransaction();

                    if(employee != null){
                        try{
                            database.openTransaction();
                            database.deleteData(UserLogin.TABLE_NAME, null, null);
                            database.deleteData(ForemanActive.TABLE_NAME, null, null);
                            database.setData(new UserLogin(employee.getRowId(), employee.getCompanyCode(), employee.getEstate(),
                                    employee.getFiscalYear(), employee.getFiscalPeriod(), employee.getNik(), employee.getName(), employee.getTermDate(),
                                    employee.getDivision(), role, employee.getJobPos(), employee.getGang(), employee.getCostCenter(),
                                    employee.getEmpType(), employee.getValidFrom(), employee.getHarvesterCode()));
                            database.commitTransaction();

                            if (fragmentLoginListener != null)
                                fragmentLoginListener.onInterfaceLogin(getResources().getString(R.string.login));
                        }catch(SQLiteException e){
                            e.printStackTrace();
                            database.closeTransaction();
                            new DialogNotification(getActivity(), getResources().getString(R.string.informasi), e.getMessage(), false).show();
                        }finally{
                            database.closeTransaction();
                        }
                    }else{
                        new DialogNotification(getActivity(), getResources().getString(R.string.informasi),
                                getResources().getString(R.string.invalid_login), false).show();
                    }
                }else{
                    new DialogNotification(getActivity(), getResources().getString(R.string.informasi),
                            getResources().getString(R.string.invalid_login), false).show();
                }
            }else{
                new DialogConfirmation(getActivity(), getResources().getString(R.string.informasi),
                        getResources().getString(R.string.layanan_lokasi), null, R.id.btnLogin).show();
            }

    }*/

   void login(){
       String username = edt_login_username.getText().toString().trim();
       String password = edt_login_password.getText().toString().trim();
       long today = new Date().getTime();
       database.openTransaction();
       UserApp userApp = (UserApp) database.getDataFirst(false, UserApp.TABLE_NAME, null,
               UserApp.XML_USERNAME + "=? " + " and " +
                       UserApp.XML_PASSWORD + "=?" + " and " +
                       UserApp.XML_VALID_TO + ">=?",
               new String [] {username, password, String.valueOf(today)},
               null, null, null, null);
       database.closeTransaction();

       if(userApp != null){
           String nik = userApp.getNik();

           database.openTransaction();
           Employee employee = (Employee) database.getDataFirst(false, Employee.TABLE_NAME, null,
                   Employee.XML_NIK + "=?",
                   new String [] {nik},
                   null, null, null, null);
           database.closeTransaction();

           if(employee != null){
               try{
                   database.openTransaction();
                   database.deleteData(UserLogin.TABLE_NAME, null, null);
                  // database.deleteData(ForemanActive.TABLE_NAME, null, null);
                   database.setData(new UserLogin(employee.getRowId(), employee.getCompanyCode(), employee.getEstate(),
                           employee.getFiscalYear(), employee.getFiscalPeriod(), employee.getNik(), employee.getName(), employee.getTermDate(),
                           employee.getDivision(), role, employee.getJobPos(), employee.getGang(), employee.getCostCenter(),
                           employee.getEmpType(), employee.getValidFrom(), employee.getHarvesterCode()));
                   database.commitTransaction();

                   if (fragmentLoginListener != null)
                       fragmentLoginListener.onInterfaceLogin(getResources().getString(R.string.login));
               }catch(SQLiteException e){
                   e.printStackTrace();
                   database.closeTransaction();
                   new DialogNotification(getActivity(), getResources().getString(R.string.informasi), e.getMessage(), false).show();
               }finally{
                   database.closeTransaction();
               }
           }else{
               new DialogNotification(getActivity(), getResources().getString(R.string.informasi),
                       getResources().getString(R.string.invalid_login), false).show();
           }
       }else{
           new DialogNotification(getActivity(), getResources().getString(R.string.informasi),
                   getResources().getString(R.string.invalid_login), false).show();
       }
   }
}
