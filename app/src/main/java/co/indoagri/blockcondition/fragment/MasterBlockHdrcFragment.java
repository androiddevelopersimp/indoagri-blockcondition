package co.indoagri.blockcondition.fragment;


import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.AdapterBlockHdrc;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.model.Data.BlockHdrc;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.widget.ToastMessage;

import static co.indoagri.blockcondition.activity.BlockActivity.SESS_PERIOD;
import static co.indoagri.blockcondition.activity.BlockActivity.SESS_ZYEAR;

public class MasterBlockHdrcFragment extends Fragment implements View.OnClickListener,
        TextWatcher, AdapterView.OnItemClickListener {
    View view;

    FragmentListBlockListener fragmentListBlockListener;
    private Button btnNext,btnBack;
    String value = null;
    private List<BlockHdrc> listBlockHdrc = new ArrayList<BlockHdrc>();
    private AdapterBlockHdrc adapter;
    private GetDataAsyncTask getDataAsync;
    boolean isSearch = false;
    static String companyCode;
    static String estate;
    static String division;
    static String type;
    static String status;
    static String project;
    static int inStatus;
    private ListView lsvMasterBlockHdrc;
    private EditText edtMasterBlockHdrcSearch;
    private Button btnMasterBlockHdrcLoadMore;
    DatabaseHandler database;
    public MasterBlockHdrcFragment() {
        // Required empty public constructor
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        BlockHdrc block_hdrc = (BlockHdrc) adapter.getItem(pos);
        if(isSearch){
            if (fragmentListBlockListener!= null)
                fragmentListBlockListener.onInputBlockList(block_hdrc.getBlock());
        }
    }


    public interface FragmentListBlockListener{
        void onInputBlockList(String hdrc);
    }

    public static MasterBlockHdrcFragment newInstance() {
        return (new MasterBlockHdrcFragment());

    }
    public static MasterBlockHdrcFragment newInstance(String companycode,
                                                      String Estate,
                                                      String Division,
                                                      boolean isSearch,
                                                      int inStatus,
                                                      String KONDISI) {
        MasterBlockHdrcFragment fragment = new MasterBlockHdrcFragment();
        Bundle args = new Bundle();
        args.putString(BlockHdrc.XML_COMPANY_CODE, companycode);
        args.putString(BlockHdrc.XML_ESTATE, Estate);
        args.putString(BlockHdrc.XML_DIVISION, Division);
        args.putBoolean(Constants.SEARCH, isSearch);
        args.putInt(Constants.inStatus,inStatus);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list_block, container, false);
        database = new DatabaseHandler(MasterBlockHdrcFragment.this.getContext());
        return  view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null)
            companyCode = getArguments().getString(BlockHdrc.XML_COMPANY_CODE);
            estate = getArguments().getString(BlockHdrc.XML_ESTATE);
            division = getArguments().getString(BlockHdrc.XML_DIVISION);
            isSearch = getArguments().getBoolean(Constants.SEARCH);
            inStatus= getArguments().getInt(Constants.inStatus,0);
            type = "01";
            status="01";
            project = "T";
        if (getActivity() instanceof FragmentListBlockListener)
            fragmentListBlockListener = (FragmentListBlockListener) getActivity();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnBack = (Button)view.findViewById(R.id.btnBack);
        btnNext = (Button)view.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        lsvMasterBlockHdrc = (ListView) view.findViewById(R.id.lsvMasterBlockHdrc);
        edtMasterBlockHdrcSearch = (EditText) view.findViewById(R.id.edtMasterBlockHdrcSearch);
        btnMasterBlockHdrcLoadMore = (Button) view.findViewById(R.id.btnMasterBlockHdrcLoadMore);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListBlockListener) {
            fragmentListBlockListener = (FragmentListBlockListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentLevelBlockListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentListBlockListener = null;
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        this.getArguments().clear();
    }
    @Override
    public void onStart() {
        super.onStart();
        btnBack.setText(getResources().getString(R.string.back));
        btnNext.setText(getResources().getString(R.string.skb));
        btnNext.setVisibility(View.GONE);
        init();
    }


    void init(){
        edtMasterBlockHdrcSearch.addTextChangedListener(this);
        lsvMasterBlockHdrc.setOnItemClickListener(this);
        btnMasterBlockHdrcLoadMore.setOnClickListener(this);
        adapter = new AdapterBlockHdrc(getActivity(), listBlockHdrc, R.layout.item_block_hrdc);
        lsvMasterBlockHdrc.setAdapter(adapter);
        lsvMasterBlockHdrc.setScrollingCacheEnabled(false);
        getData();

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                if (fragmentListBlockListener != null)
                    fragmentListBlockListener.onInputBlockList(value);
                break;
            case R.id.btnMasterBlockHdrcLoadMore:
                getDataAsync = new GetDataAsyncTask();
                getDataAsync.execute();
                break;
            default:
                break;
        }
    }
    @Override
    public void afterTextChanged(Editable filter) {
        adapter.getFilter().filter(filter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {}

    @Override
    public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
    private void getData(){

        getDataAsync = new GetDataAsyncTask();
        getDataAsync.execute();
    }

    private class GetDataAsyncTask extends AsyncTask<Void, List<BlockHdrc>, List<BlockHdrc>> {
        private DialogProgress dialogProgress;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(!getActivity().isFinishing()){
                dialogProgress = new DialogProgress(getActivity(),getContext().getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }
        @Override
        protected List<BlockHdrc> doInBackground(Void... voids) {
            database.openTransaction();
            List<BlockHdrc> listTemp = new ArrayList<BlockHdrc>();
            List<Object> listObject;
            String[] a = null;
            String query = null;
            try{
            if(isSearch){
                if(inStatus == Constants.MatureNotSurveyedBlock){
                    a = new String[3];
                    a[0] = SESS_ZYEAR;
                    a[1] = SESS_PERIOD;
                    a[2] = "0";
                    query = "SELECT DISTINCT Block,Project FROM BLOCK_HDRC " +
                            "WHERE project LIKE 'M%'" +
                            "AND Block NOT IN " +
                            "(SELECT DISTINCT Block FROM tblT_BlockCondition " +
                            "WHERE zyear= ? " +
                            "AND period= ? " +
                            "AND translevel= ?) ";
                }else if(inStatus==Constants.IMMatureNotSurveyedBlock){
                    a = new String[3];
                    a[0] = SESS_ZYEAR;
                    a[1] = SESS_PERIOD;
                    a[2] = "0";
                    query = "SELECT DISTINCT Block,Project FROM BLOCK_HDRC " +
                            "WHERE project LIKE 'T%'" +
                            "AND Block NOT IN " +
                            "(SELECT DISTINCT Block FROM tblT_BlockCondition " +
                            "WHERE zyear= ? " +
                            "AND period= ? " +
                            "AND translevel = ?) ";
                }else{
                    a = new String[7];

                    a[0] = companyCode;
                    a[1] = estate;
                    a[2] = type;
                    a[3] = status;
                    a[4] = division;
                    a[5] = "M" + '%';
                    a[6] = "T" + '%';
                    query = "SELECT DISTINCT "+BlockHdrc.XML_BLOCK+","+BlockHdrc.XML_PROJECT_DEFINITION+" FROM "+BlockHdrc.TABLE_NAME+" WHERE "+
                            BlockHdrc.XML_COMPANY_CODE + "=?" + " and " +
                            BlockHdrc.XML_ESTATE + "=?" + " and " +
                            BlockHdrc.XML_TYPE+ "=?" + " and " +
                            BlockHdrc.XML_STATUS+ "=?" + " and " +
                            BlockHdrc.XML_DIVISION + "=? and ("+
                            BlockHdrc.XML_PROJECT_DEFINITION +" LIKE ? OR "+
                            BlockHdrc.XML_PROJECT_DEFINITION+" LIKE ? )";
                }
                listObject = database.getListDataRawQuery(query,BlockHdrc.TABLE_NAME,a);
            }else{
                if(inStatus==Constants.MatureNotSurveyedBlock){
                    a = new String[3];
                    a[0] = SESS_ZYEAR;
                    a[1] = SESS_PERIOD;
                    a[2] = "0";
                    query = "SELECT DISTINCT Block,Project FROM BLOCK_HDRC " +
                            "WHERE project LIKE 'M%' " +
                            "AND Block NOT IN " +
                            "(SELECT DISTINCT Block FROM tblT_BlockCondition " +
                            "WHERE zyear= ? " +
                            "AND period= ? " +
                            "AND translevel= ?) ";
                }else if(inStatus==Constants.IMMatureNotSurveyedBlock){
                    a = new String[3];
                    a[0] = SESS_ZYEAR;
                    a[1] = SESS_PERIOD;
                    a[2] = "0";
                    query = "SELECT DISTINCT Block,Project FROM BLOCK_HDRC " +
                            "WHERE project  LIKE 'T%' " +
                            "AND Block NOT IN " +
                            "(SELECT DISTINCT Block FROM tblT_BlockCondition " +
                            "WHERE zyear= ? " +
                            "AND period= ? " +
                            "AND translevel= ?) ";
                }else{
                    a = new String[7];

                    a[0] = companyCode;
                    a[1] = estate;
                    a[2] = type;
                    a[3] = status;
                    a[4] = division;
                    a[5] = "M" + '%';
                    a[6] = "T" + '%';
                    query = "SELECT DISTINCT "+BlockHdrc.XML_BLOCK+","+BlockHdrc.XML_PROJECT_DEFINITION+" FROM "+BlockHdrc.TABLE_NAME+" WHERE "+
                            BlockHdrc.XML_COMPANY_CODE + "=?" + " and " +
                            BlockHdrc.XML_ESTATE + "=?" + " and " +
                            BlockHdrc.XML_TYPE+ "=?" + " and " +
                            BlockHdrc.XML_STATUS+ "=?" + " and " +
                            BlockHdrc.XML_DIVISION + "=? and ("+
                            BlockHdrc.XML_PROJECT_DEFINITION +" LIKE ? OR "+
                            BlockHdrc.XML_PROJECT_DEFINITION+" LIKE ? )";
                }
                listObject = database.getListDataRawQuery(query,BlockHdrc.TABLE_NAME,a);
            }
            database.closeTransaction();
            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    BlockHdrc blockHdrc = (BlockHdrc) listObject.get(i);

                    listTemp.add(blockHdrc);
                }
            }
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
        }
            return listTemp;

        }

        @Override
        protected void onPostExecute(List<BlockHdrc> listTemp) {
            super.onPostExecute(listTemp);
            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }
            for(int i = 0; i < listTemp.size(); i++){
                BlockHdrc blockHdrc = (BlockHdrc) listTemp.get(i);
                adapter.addData(blockHdrc);

            }
        }

    }
}
