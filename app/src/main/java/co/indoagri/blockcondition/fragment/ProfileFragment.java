package co.indoagri.blockcondition.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.activity.ViewProfile;

public class ProfileFragment extends Fragment implements View.OnClickListener {
    View view;
    TextView tvName,tvEducation,tvAddress,tvTitle,tvSummary;
    Button btnLogout;
    FragmentProfileListener fragmentProfileListener;
    private static final String ARG_PARAM1 = "param1";

    private String mParam1;
    public ProfileFragment() {
        // Required empty public constructor
    }


    public interface FragmentProfileListener {
        void onInterfaceProfile(String input);
    }

    public static ImportFragment newInstance() {
        return (new ImportFragment());

    }
    public static ProfileFragment newInstance(String param1) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        return  view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null)
            mParam1 = getArguments().getString(ARG_PARAM1);
        if (getActivity() instanceof FragmentProfileListener)
            fragmentProfileListener = (FragmentProfileListener) getActivity();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvTitle = (TextView)view.findViewById(R.id.tvTitle);
        tvAddress = (TextView)view.findViewById(R.id.tvAddress);
        tvEducation = (TextView)view.findViewById(R.id.tvEducation);
        tvName = (TextView)view.findViewById(R.id.tvName);
        tvSummary = (TextView)view.findViewById(R.id.tvSummary);
        btnLogout = (Button)view.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(this);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentProfileListener) {
            fragmentProfileListener = (FragmentProfileListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentLevelBlockListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentProfileListener = null;
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        if (getArguments() != null)
            this.getArguments().clear();

    }
    @Override
    public void onStart() {
        super.onStart();

        if(ViewProfile.CheckLogin()){
            tvName.setText(ViewProfile.getUserName());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogout:
                if (fragmentProfileListener != null)
                    fragmentProfileListener.onInterfaceProfile(getResources().getString(R.string.logout));

                break;
            default:
                break;
        }
    }
}
