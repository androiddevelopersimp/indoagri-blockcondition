package co.indoagri.blockcondition.adapter;

import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.view.ViewRoleItem;
import co.indoagri.blockcondition.adapter.view.ViewSummarySKB;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.Users.Role;
import co.indoagri.blockcondition.routines.Constants;

public class AdapterSummarySKB extends BaseAdapter{
    private Context context;
    private List<tblT_BlockCondition> tblT_blockConditions;
    private int layout;

    ViewSummarySKB viewSummarySKB;
    TextView txtTitle;
    TextView txtJudul1,txtJudul2;
    TextView txtSKB;
    TextView txtRemark;
    RadioGroup radioGroup1;
    RadioButton radioButtonGreen1;
    RadioButton radioButtonYellow1;
    RadioButton radioButtonRed1;
    RadioGroup radioGroup2;
    RadioButton radioButtonGreen2;
    RadioButton radioButtonYellow2;
    RadioButton radioButtonRed2;

    public AdapterSummarySKB(Context context, List<tblT_BlockCondition> tblT_blockConditions, int layout){
        this.context = context;
        this.tblT_blockConditions = tblT_blockConditions;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return tblT_blockConditions.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return tblT_blockConditions.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        tblT_BlockCondition condition = tblT_blockConditions.get(position);

        if(convertView == null){
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
            txtJudul1 = (TextView) convertView.findViewById(R.id.txtJudul1);
            txtJudul2 = (TextView) convertView.findViewById(R.id.txtJudul2);
            txtTitle = (TextView) convertView.findViewById(R.id.txt_Title);
            txtSKB = (TextView) convertView.findViewById(R.id.txt_skb);
            txtRemark = (TextView) convertView.findViewById(R.id.txt_Remark);
            radioGroup1 = (RadioGroup) convertView.findViewById(R.id.rb_group1);
            radioButtonGreen1 = (RadioButton) convertView.findViewById(R.id.rb_green1);
            radioButtonYellow1= (RadioButton) convertView.findViewById(R.id.rb_yellow1);
            radioButtonRed1= (RadioButton) convertView.findViewById(R.id.rb_red1);
            radioGroup2 = (RadioGroup) convertView.findViewById(R.id.rb_group2);
            radioButtonGreen2 = (RadioButton) convertView.findViewById(R.id.rb_green2);
            radioButtonYellow2= (RadioButton) convertView.findViewById(R.id.rb_yellow2);
            radioButtonRed2= (RadioButton) convertView.findViewById(R.id.rb_red2);

            radioButtonGreen1.setClickable(false);
            radioButtonGreen2.setClickable(false);
            radioButtonYellow1.setClickable(false);
            radioButtonYellow2.setClickable(false);
            radioButtonRed1.setClickable(false);
            radioButtonRed2.setClickable(false);

            viewSummarySKB = new ViewSummarySKB(txtTitle,radioGroup1,
                    radioButtonGreen1,radioButtonYellow1,
                    radioButtonRed1,radioGroup2,
                    radioButtonGreen2,radioButtonYellow2,
                    radioButtonRed2,
                    txtRemark);

            convertView.setTag(viewSummarySKB);
        }else {
            viewSummarySKB = (ViewSummarySKB) convertView.getTag();

            txtTitle = viewSummarySKB.getTxtTitle();
            txtRemark = viewSummarySKB.getTxtRemark();
            radioButtonGreen1 = viewSummarySKB.getRadioButtonGreen1();
            radioButtonGreen2 = viewSummarySKB.getRadioButtonGreen2();
            radioButtonYellow1 = viewSummarySKB.getRadioButtonYellow1();
            radioButtonYellow2 = viewSummarySKB.getRadioButtonYellow2();
            radioButtonRed1 = viewSummarySKB.getRadioButtonRed1();
            radioButtonRed2 = viewSummarySKB.getRadioButtonRed2();
        }
        String DateString = condition.getAcc_CreatedDateTime().substring(0,10);
        txtTitle.setText(new DateLocal(DateString, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
        txtRemark.setText(condition.getAcc_Remark());
        txtSKB.setText("SKB ("+ condition.getAcc_SKB()+")");
        String phase = new PreferenceManager(context, Constants.shared_name).getSSPhase();
        if(phase.equalsIgnoreCase("Mature")){
            txtJudul1.setText("TPH Bersih 1");
            txtJudul2.setText("TPH Bersih 2");
            if(condition.getAcc_TPHBersih()!=null){
                String TPHBERSIH = condition.getAcc_TPHBersih();
                if(TPHBERSIH.equals("3")){
                    radioButtonGreen1.setChecked(true);
                }
                if(TPHBERSIH.equals("1")){
                    radioButtonRed1.setChecked(true);
                }
                if(TPHBERSIH.equals("2")){
                    radioButtonYellow1.setChecked(true);
                }
            }
            if(condition.getAcc_TPHBersih2()!=null){
                String TPHBERSIH2 = condition.getAcc_TPHBersih2();
                if(TPHBERSIH2.equals("3")){
                    radioButtonGreen2.setChecked(true);
                }
                if(TPHBERSIH2.equals("1")){
                    radioButtonRed2.setChecked(true);
                }
                if(TPHBERSIH2.equals("2")){
                    radioButtonYellow2.setChecked(true);
                }
            }
            if(condition.getAcc_Remark()!=null){
                txtRemark.setText(condition.getAcc_Remark());
            }
        }else{
            txtJudul1.setText("TPH 1");
            txtJudul2.setText("TPH 2");
            if(condition.getAcc_TPHBersih()!=null){
                String TPHBERSIH = condition.getAcc_TPH();
                if(TPHBERSIH.equals("3")){
                    radioButtonGreen1.setChecked(true);
                }
                if(TPHBERSIH.equals("1")){
                    radioButtonRed1.setChecked(true);
                }
                if(TPHBERSIH.equals("2")){
                    radioButtonYellow1.setChecked(true);
                }
            }
            if(condition.getAcc_TPHBersih2()!=null){
                String TPHBERSIH2 = condition.getAcc_TPH2();
                if(TPHBERSIH2.equals("3")){
                    radioButtonGreen2.setChecked(true);
                }
                if(TPHBERSIH2.equals("1")){
                    radioButtonRed2.setChecked(true);
                }
                if(TPHBERSIH2.equals("2")){
                    radioButtonYellow2.setChecked(true);
                }
            }
            if(condition.getAcc_Remark()!=null){
                txtRemark.setText(condition.getAcc_Remark());
            }
        }


        return convertView;
    }
    public void addData(tblT_BlockCondition blockCondition){
        boolean found = false;

        for(int i = 0; i < tblT_blockConditions.size(); i++){
            if(blockCondition.getAcc_CreatedDateTime().equalsIgnoreCase(tblT_blockConditions.get(i).getAcc_CreatedDateTime())){
                found = true;
                break;
            }
        }

        if(!found){
            tblT_blockConditions.add(blockCondition);
            notifyDataSetChanged();
        }
    }
}
