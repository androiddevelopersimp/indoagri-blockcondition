package co.indoagri.blockcondition.adapter;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.activity.RKHJOBActivity;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.RKH.RKH_HEADER;
import co.indoagri.blockcondition.model.RKH.RKH_ITEM;
import co.indoagri.blockcondition.model.Users.Employee;
import co.indoagri.blockcondition.routines.Constants;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;

public class RKHMaingGangAdapter extends RecyclerView.Adapter<RKHMaingGangAdapter.ViewHolder> {

    List<Employee> mValues;
    Context mContext;
    protected ItemListener mListener;
    protected CardTugaskan cardTugaskanListener;
    protected  CardEdit cardEditListener;
    protected QrCode qrCodeListener;
    DatabaseHandler database;
    ChildAdapter childAdapter;
    private String QR_CODE_DELIMITER = ";";
    private String QR_CODE_DATA_DELIMITER = "|";
    private String QR_CODE_DATA_DELIMITER_CLERK = ",";
    String headerQr ="";
    String contentQr = "";
    String lastRKHNumber ="";
    public RKHMaingGangAdapter(Context context, List<Employee> values, ItemListener itemListener, CardTugaskan cardTugaskanListener, CardEdit cardEditListener, QrCode qrCodeListener) {

        mValues = values;
        mContext = context;
        mListener=itemListener;
        this.cardEditListener = cardEditListener;
        this.cardTugaskanListener = cardTugaskanListener;
        this.qrCodeListener =qrCodeListener;
        database = new DatabaseHandler(mContext);


    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txtForeMan;
        public TextView txtNIK;
        public TextView txtNIKClerk;
        public TextView txtClerk;
        public TextView txtGang;
        public TextView txtGang2;
        public TextView txtAktifitas;
        public TextView txtBlok;
        public TextView txtHariKerja;
        public TextView txtSKU;
        public TextView txtGender;
        public TextView txtPHL;
        public TextView txtHA;
        public TextView txtJanjang;
        public TextView txtTonase;
        public LinearLayout cardButton;
        public LinearLayout cardButton2;
        public LinearLayout expendableLayout;
        public ListView ListItem;
        public LinearLayout layoutList;
        public LinearLayout layoutArrow;
        public ImageView imageArrow;
        public RecyclerView listChildItem;
        public ImageView imageQrCode;
        Employee item;

        public ViewHolder(View v) {

            super(v);

            v.setOnClickListener(this);
            txtForeMan = (TextView) v.findViewById(R.id.txtForeman);
            txtNIK = (TextView) v.findViewById(R.id.txtNik);
            txtNIKClerk = (TextView) v.findViewById(R.id.txtNikKerani);
            txtClerk = (TextView) v.findViewById(R.id.txtClerk);
            txtGang = (TextView) v.findViewById(R.id.txtGang);
            txtGang2 = (TextView) v.findViewById(R.id.txtGang2);
            txtAktifitas = (TextView) v.findViewById(R.id.itemAktifitas);
            txtBlok = (TextView) v.findViewById(R.id.itemBlock);
            txtHariKerja = (TextView) v.findViewById(R.id.itemHariKerja);
            txtSKU = (TextView) v.findViewById(R.id.itemSKU);
            txtGender = (TextView) v.findViewById(R.id.itemResultGender);
            txtGender.setVisibility(View.GONE);
            txtPHL = (TextView) v.findViewById(R.id.itemPHL);
            txtHA = (TextView) v.findViewById(R.id.itemHA);
            txtJanjang = (TextView) v.findViewById(R.id.itemJenjang);
            txtTonase = (TextView) v.findViewById(R.id.itemTonase);
            cardButton = (LinearLayout) v.findViewById(R.id.cardJob);
            cardButton2 = (LinearLayout) v.findViewById(R.id.cardJob2);

            layoutArrow = (LinearLayout) v.findViewById(R.id.layarArrow);
            imageArrow = (ImageView) v.findViewById(R.id.arrow);

            imageQrCode = (ImageView)v.findViewById(R.id.imageQr);

            expendableLayout = (LinearLayout)v.findViewById(R.id.expendableLayout);
            layoutList = (LinearLayout)v.findViewById(R.id.layoutList);
            listChildItem = (RecyclerView) itemView.findViewById(R.id.listItem);
            listChildItem.setHasFixedSize(false);
            listChildItem.setLayoutManager(new LinearLayoutManager(mContext));
            cardButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cardTugaskanListener != null) {
                        cardTugaskanListener.onCardTugaskan(item);
                    }
                }
            });
            cardButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cardEditListener != null) {
                        cardEditListener.onCardEdit(item);
                    }
                }
            });
            imageQrCode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (qrCodeListener != null) {
                        qrCodeListener.onQrCode(item);
                    }
                }
            });
        }

        public void setData(Employee item, ViewHolder viewHolder) {
            this.item = item;
            database.openTransaction();
            String LastYear = String.valueOf(getPeriode().getFiscalYear());
            String LastPeriod = String.valueOf(getPeriode().getFiscalPeriod());
            Employee itemKrani = (Employee) database.getDataFirst(false, Employee.TABLE_NAME, null,
                    Employee.XML_GANG+ "=? AND "+
                            Employee.XML_ROLE_ID+ "=? ",
                    new String [] {item.getGang(),"KERANI"},
                    null, null, null, null);
            txtForeMan.setText(item.getName());
            txtGang.setText(item.getGang());
            txtNIK.setText(item.getNik());
            txtNIKClerk.setText(itemKrani.getNik());
            txtClerk.setText(itemKrani.getName());
            txtGang2.setText(itemKrani.getGang());
            List<Object> listObject;
            String RKHDATE = null;
            if(new PreferenceManager(mContext, Constants.shared_name).getRkhInputrkhdate()!=null){
                RKHDATE = new PreferenceManager(mContext, Constants.shared_name).getRkhInputrkhdate();
            }else{
                RKHDATE = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
            }

            listObject = database.getListData(false, RKH_HEADER.TABLE_NAME, null,
                    RKH_HEADER.XML_NIK_FOREMAN+ "=? AND "+
                            RKH_HEADER.XML_RKH_DATE+ "=? ",
                    new String [] {item.getNik(),RKHDATE},
                    null, null, null, null);

            if(listObject.size()>0){
                cardButton.setVisibility(View.GONE);
                cardButton2.setVisibility(View.VISIBLE);
                layoutArrow.setVisibility(View.VISIBLE);
                if (item.isExpendable()) {
                    Resources res = mContext.getResources();
                    imageArrow.setImageResource(R.drawable.ic_arr_up);
                } else {
                    Resources res = mContext.getResources();
                    imageArrow.setImageResource(R.drawable.ic_arr_down);
                }
                for(int i = 0; i < listObject.size(); i++) {
                     RKH_HEADER rkh_header = (RKH_HEADER) listObject.get(i);
                    Employee getForeman = (Employee) database.getDataFirst(false, Employee.TABLE_NAME, null,
                                    Employee.XML_NIK+ "=? ",
                            new String [] {rkh_header.getNIK_FOREMAN()},
                            null, null, null, null);
                    Employee getClerk = (Employee) database.getDataFirst(false, Employee.TABLE_NAME, null,
                            Employee.XML_NIK+ "=? ",
                            new String [] {rkh_header.getNIK_CLERK()},
                            null, null, null, null);
                    List<Object> listObject2;
                    txtForeMan.setText(getForeman.getName());
                    txtGang.setText(getForeman.getGang());
                    txtNIK.setText(getForeman.getNik());
                    txtNIKClerk.setText(getClerk.getNik());
                    txtClerk.setText(getClerk.getName());
                    txtGang2.setText(getClerk.getGang());
                    listObject2 = database.getListData(false, RKH_ITEM.TABLE_NAME, null,
                            RKH_ITEM.XML_RKH_ID + "=? ",
                            new String[]{rkh_header.getRKH_ID()},
                            null, null, null, null);
                    if (listObject2.size() > 0) {
                        for (Object object : listObject2) {
                            RKH_ITEM rkh_item = (RKH_ITEM) object;
                            createContentQR(rkh_header,rkh_item);
                        }
                        layoutList.setVisibility(View.VISIBLE);
                        ArrayList<RKH_ITEM> rkh_items = (ArrayList) listObject2;
                        String ItemSKU = getSKU(rkh_header.getESTATE(),LastYear,LastPeriod,rkh_header.getGANG(),rkh_header);
                        String  ItemPHL= getPHL(rkh_header.getESTATE(),LastYear,LastPeriod,rkh_header.getGANG(),rkh_header);
                        childAdapter = new ChildAdapter(rkh_items,ItemSKU,ItemPHL);
                        viewHolder.listChildItem.setAdapter(childAdapter);
                        boolean isExpendable = item.isExpendable();
                        layoutList.setVisibility(isExpendable ? View.VISIBLE : View.GONE);
                        imageQrCode.setVisibility(View.VISIBLE);
                    }
                }
                try {
                    Bitmap bitmapQr = encodeAsBitmap(contentQr, BarcodeFormat.QR_CODE, 200, 200);
                    imageQrCode.setImageBitmap(bitmapQr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                layoutList.setVisibility(View.GONE);
                cardButton2.setVisibility(View.GONE);
                cardButton.setVisibility(View.VISIBLE);
                imageQrCode.setVisibility(View.GONE);
            }
            database.closeTransaction();
        }

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(item,getAdapterPosition());
            }
        }
    }

    @Override
    public RKHMaingGangAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_recyclerview_rkh, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, int position) {
        Vholder.setData(mValues.get(position),Vholder);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public interface ItemListener {
        void onItemClick(Employee item, int position);
    }
    public interface CardTugaskan {
        void onCardTugaskan(Employee item);
    }
    public interface CardEdit {
        void onCardEdit(Employee item);
    }
    public interface QrCode {
        void onQrCode(Employee item);
    }
    public void addData(Employee rkh_header){
        boolean found = false;

        for(int i = 0; i < mValues.size(); i++){
            if(rkh_header.getGang().equalsIgnoreCase(mValues.get(i).getGang())){
                found = true;
                break;
            }
        }

        if(!found){
            mValues.add(rkh_header);
            notifyDataSetChanged();
        }
    }
    private Employee getPeriode(){
        Employee datas = (Employee)database.getDataFirst(false, Employee.TABLE_NAME, null,
                null,
                null,
                null, null, Employee.XML_FISCAL_YEAR +" DESC ,"+Employee.XML_FISCAL_PERIOD+" DESC ", "1");
        return datas;
    }
    private String getSKU(String Estate,String Year,String Period, String gang,RKH_HEADER rkhHeader){
        String result = "";
        int SKU = 0;
        List<Object> listObject = database.getListData(false, Employee.TABLE_NAME, null,
                Employee.XML_COMPANY_CODE + "=?" + " and " +
                        Employee.XML_ESTATE + "=?" + " and " +
                        Employee.XML_FISCAL_PERIOD + "=?" + " and " +
                        Employee.XML_FISCAL_YEAR + "=?" + " and " +
                        Employee.XML_GANG + "=?" + " and " +
                        Employee.XML_EMP_TYPE + " like '%SKU%'" + " and " +
                        Employee.XML_DIVISION + "=?",
                new String [] {rkhHeader.getCOMPANY_CODE(),rkhHeader.getESTATE(),Period,Year,rkhHeader.getGANG(),rkhHeader.getDIVISION()},
                null, null, null, null);
        SKU = listObject.size();
        result = String.valueOf(SKU);
        return result;
    }
    private String getPHL(String Estate,String Year, String Period, String gang,RKH_HEADER rkhHeader){
        String result = "";
        int PHL = 0;
        List<Object> listObject = database.getListData(false, Employee.TABLE_NAME, null,
                Employee.XML_COMPANY_CODE + "=?" + " and " +
                        Employee.XML_ESTATE + "=?" + " and " +
                        Employee.XML_FISCAL_PERIOD + "=?" + " and " +
                        Employee.XML_FISCAL_YEAR + "=?" + " and " +
                        Employee.XML_GANG + "=?" + " and " +
                        Employee.XML_EMP_TYPE + " like '%PHL%'" + " and " +
                        Employee.XML_DIVISION + "=?",
                new String [] {rkhHeader.getCOMPANY_CODE(),rkhHeader.getESTATE(),Period,Year,rkhHeader.getGANG(),rkhHeader.getDIVISION()},
                null, null, null, null);
        PHL = listObject.size();
        result = String.valueOf(PHL);
        return result;
    }

    public class ChildAdapter extends RecyclerView.Adapter<ChildAdapter.ChildListItemViewHolder>{
        List<RKH_ITEM> rkh_items;
        String ItemSKU;
        String ItemPHL;
        public ChildAdapter(List<RKH_ITEM> rkhItemList, String ItemSKU, String ItemPHL) {

            if (rkhItemList == null) {
                throw new IllegalArgumentException(
                        "PrescriptionProductList must not be null");
            }
            this.rkh_items = rkhItemList;
            this.ItemSKU = ItemSKU;
            this.ItemPHL= ItemPHL;
        }

        @Override
        public ChildListItemViewHolder onCreateViewHolder(
                ViewGroup viewGroup, int viewType) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.row_recyclerview_rkh_child,
                            viewGroup,
                            false);
            return new ChildListItemViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(
                ChildListItemViewHolder viewHolder, int position) {
            RKH_ITEM rkhItem = rkh_items.get(position);
            String ItemDay = String.valueOf(Integer.parseInt(ItemSKU)+Integer.parseInt(ItemPHL));
            String[] separated = rkhItem.getACTIVITY().split(";");
            String ActivityType = separated[0];
            String ActivityTypeName = separated[1];
            viewHolder.txtAktifitas.setText(Html.fromHtml("Activitas : <b>"+ ActivityType+" - "+ActivityTypeName +"</b>"));
            viewHolder.txtBlok.setText(Html.fromHtml("Blok : <b>"+ rkhItem.getBLOCK() +"</b>"));
            viewHolder.txtHariKerja.setText(Html.fromHtml("Hari Kerja : <b>"+ ItemDay +"</b>"));
            viewHolder.txtSKU.setText(Html.fromHtml("SKU : <b>"+ rkhItem.getSKU() +"</b>"));
            viewHolder.txtGender.setText(Html.fromHtml(" Gender : <b>"+ "0" +"</b>"));
            viewHolder.txtPHL.setText(Html.fromHtml("PHL : <b>"+ rkhItem.getPHL() +"</b>"));
            viewHolder.txtHA.setText(Html.fromHtml("HA : <b>"+ rkhItem.getTARGET_OUTPUT() +"</b>"));
            viewHolder.txtJanjang.setText(Html.fromHtml("Target Janjang : <b>"+ rkhItem.getTARGET_JANJANG() +"</b>"));
            viewHolder.txtTonase.setText(Html.fromHtml("Tonase : <b>"+ rkhItem.getTARGET_HASIL() +"</b>"));
        }

        @Override
        public int getItemCount() {
            return rkh_items.size();
        }

        public final class ChildListItemViewHolder
                extends RecyclerView.ViewHolder {

            public TextView txtAktifitas;
            public TextView txtBlok;
            public TextView txtHariKerja;
            public TextView txtSKU;
            public TextView txtGender;
            public TextView txtPHL;
            public TextView txtHA;
            public TextView txtJanjang;
            public TextView txtTonase;
            public ChildListItemViewHolder(View itemView) {
                super(itemView);
                txtAktifitas = (TextView) itemView.findViewById(R.id.itemAktifitas);
                txtAktifitas = (TextView) itemView.findViewById(R.id.itemAktifitas);
                txtBlok = (TextView) itemView.findViewById(R.id.itemBlock);
                txtHariKerja = (TextView) itemView.findViewById(R.id.itemHariKerja);
                txtSKU = (TextView) itemView.findViewById(R.id.itemSKU);
                txtGender = (TextView) itemView.findViewById(R.id.itemResultGender);
                txtGender.setVisibility(View.GONE);
                txtPHL = (TextView) itemView.findViewById(R.id.itemPHL);
                txtHA = (TextView) itemView.findViewById(R.id.itemHA);
                txtJanjang = (TextView) itemView.findViewById(R.id.itemJenjang);
                txtTonase = (TextView) itemView.findViewById(R.id.itemTonase);
            }
        }
    }

    public void createContentQR(RKH_HEADER rkh_header, RKH_ITEM rkh_item) {
        String currentString = rkh_item.getACTIVITY();
        String[] act = currentString.split(";");
        if (!lastRKHNumber.equals("")) {
            contentQr += QR_CODE_DELIMITER;
        }
        contentQr +=rkh_header.getRKH_DATE()+ QR_CODE_DELIMITER +rkh_header.getNIK_FOREMAN()+QR_CODE_DATA_DELIMITER_CLERK+rkh_header.getFOREMAN()+QR_CODE_DATA_DELIMITER_CLERK+rkh_header.getNIK_CLERK()+QR_CODE_DATA_DELIMITER_CLERK+rkh_header.getCLERK()+QR_CODE_DATA_DELIMITER_CLERK+rkh_item.getLINE()+QR_CODE_DELIMITER+act[0] + QR_CODE_DELIMITER + rkh_item.getBLOCK() + QR_CODE_DELIMITER
                    + rkh_item.getTARGET_OUTPUT();
        lastRKHNumber = rkh_header.getRKH_ID();
    }
    Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        Log.e(ContentValues.TAG, "encodeAsBitmap: " + bitmap.toString());
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }

}