package co.indoagri.blockcondition.adapter;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.TextView;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.view.ViewBluetoothItem;
import co.indoagri.blockcondition.model.Bluetooth;

public class AdapterBluetooth extends BaseAdapter{
    private Context context;
    private List<Bluetooth> listData, listTemp;
    private int layout;

    public AdapterBluetooth(Context context, List<Bluetooth> listData, int layout){
        this.context = context;
        this.listData = listData;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        if(listData != null){
            return listData.size();
        }else{
            return 0;
        }
    }

    @Override
    public Object getItem(int pos) {
        return listData.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        Bluetooth item = listData.get(pos);

        ViewBluetoothItem view;

        TextView txtBluetoothItemDeviceName;
        TextView txtBluetoothItemDeviceAddress;
        CheckBox cbxBluetoothItemDeviceSelected;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtBluetoothItemDeviceName = (TextView) convertView.findViewById(R.id.txtBluetoothItemDeviceName);
            txtBluetoothItemDeviceAddress = (TextView) convertView.findViewById(R.id.txtBluetoothItemDeviceAddress);
            cbxBluetoothItemDeviceSelected = (CheckBox) convertView.findViewById(R.id.cbxBluetoothItemDeviceSelected);

            view = new ViewBluetoothItem(txtBluetoothItemDeviceName, txtBluetoothItemDeviceAddress, cbxBluetoothItemDeviceSelected);

            convertView.setTag(view);
        }else{
            view = (ViewBluetoothItem) convertView.getTag();

            txtBluetoothItemDeviceName = view.getTxtBluetoothItemDeviceName();
            txtBluetoothItemDeviceAddress = view.getTxtBluetoothItemDeviceAddress();
            cbxBluetoothItemDeviceSelected = view.getCbxBluetoothItemDeviceSelected();
        }

        txtBluetoothItemDeviceName.setText(item.getName());
        txtBluetoothItemDeviceAddress.setText(item.getAddress());
        cbxBluetoothItemDeviceSelected.setChecked(item.isSelected());

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listData = null;
                listData = (List<Bluetooth>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<Bluetooth> listResult = new ArrayList<Bluetooth>();
                Bluetooth item;

                if(listTemp == null){
                    listTemp = new ArrayList<Bluetooth>(listData);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = listTemp.size();
                    result.values = listTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < listTemp.size(); i++){
                        item = listTemp.get(i);

                        if(item.getName().toLowerCase(Locale.getDefault()).contains(constraint)){
                            listResult.add(item);
                        }
                    }
                    result.count = listResult.size();
                    result.values = listResult;
                }

                return result;
            }
        };
        return filter;
    }

    public void add(Bluetooth device){
        if(listData == null){
            listData = new ArrayList<Bluetooth>();
        }


        boolean found = false;
        for(int i = 0; i < listData.size(); i++){
            Bluetooth deviceCurr = (Bluetooth) listData.get(i);

            if(deviceCurr.getAddress().equalsIgnoreCase(device.getAddress())){
                found = true;
                break;
            }
        }

        if(!found){
            listData.add(device);
            notifyDataSetChanged();
        }
    }

    public void updateCheckbox(int idSelected){
        for(int i = 0; i < listData.size(); i++){
            Bluetooth device = (Bluetooth) listData.get(i);

            if(i != idSelected){
                device.setSelected(false);
            }else{
                device.setSelected(true);
            }
        }

        notifyDataSetChanged();
    }

    public void clear(){
        listData = null;

        notifyDataSetChanged();
    }
}
