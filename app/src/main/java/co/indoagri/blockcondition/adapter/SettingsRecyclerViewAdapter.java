package co.indoagri.blockcondition.adapter;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.model.MenuDashboard;
import co.indoagri.blockcondition.model.MenuSettings;

public class SettingsRecyclerViewAdapter extends RecyclerView.Adapter<SettingsRecyclerViewAdapter.ViewHolder> {

    ArrayList<MenuSettings> mValues;
    Context mContext;
    protected ItemListener mListener;
    protected LayoutListener mLayoutListener;
    public SettingsRecyclerViewAdapter(Context context, ArrayList<MenuSettings> values, ItemListener itemListener, LayoutListener LayoutListener) {

        mValues = values;
        mContext = context;
        mListener=itemListener;
        this.mLayoutListener = LayoutListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView textView;
        public ImageView imageView;
        public RelativeLayout relativeLayout;
        MenuSettings item;

        public ViewHolder(View v) {

            super(v);

            v.setOnClickListener(this);
            textView = (TextView) v.findViewById(R.id.textView);
            imageView = (ImageView) v.findViewById(R.id.imageView);
            relativeLayout = (RelativeLayout) v.findViewById(R.id.relativeLayout);

        }

        public void setData(final MenuSettings item) {
            this.item = item;

            textView.setText(item.text);
            imageView.setImageResource(item.drawable);
            if(item.active==false){
                relativeLayout.setBackgroundColor(mContext.getResources().getColor(R.color.transparent_black_hex_2));
            }else{
                relativeLayout.setBackgroundColor(Color.parseColor(item.color));
            }
            relativeLayout.setOnClickListener(mBtnClick);
        }

        private View.OnClickListener mBtnClick = new View.OnClickListener() {
            public void onClick(View v) {
                if(item.active==false){
                    Toast.makeText(mContext, "Hanya yang menggunakan Tab Samsung V3", Toast.LENGTH_SHORT).show();
                }else{
                    if (mLayoutListener != null) {
                        mLayoutListener.onLayoutClick(item);
                    }
                }
            }
        };

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(item);
            }
        }
    }

    @Override
    public SettingsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.row_recyclerview_menu_circle, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, int position) {
        Vholder.setData(mValues.get(position));

    }

    @Override
    public int getItemCount() {

        return mValues.size();
    }

    public interface ItemListener {
        void onItemClick(MenuSettings item);
    }
    public interface LayoutListener {
        void onLayoutClick(MenuSettings item);
    }
}