package co.indoagri.blockcondition.adapter.view;


import android.widget.TextView;

public class ViewLogProcess {
    private TextView txtLogID;
    private TextView txtLogDescription;
    private TextView txtLogDate;
    private TextView txtLogUser;

    public ViewLogProcess(TextView txtLogID,
                                  TextView txtLogDescription,
                                  TextView txtLogDate,
                                  TextView txtLogUser) {
        super();
        this.txtLogID = txtLogID;
        this.txtLogDescription= txtLogDescription;
        this.txtLogDate= txtLogDate;
        this.txtLogUser= txtLogUser;
    }

    public TextView getTxtLogID() {
        return txtLogID;
    }

    public void setTxtLogID(TextView setTxtLogID) {
        this.txtLogID= setTxtLogID;
    }

    public TextView getTxtLogDate() {
        return txtLogDate;
    }

    public void setTxtLogDate(
            TextView txtLogDate) {
        this.txtLogDate= txtLogDate;
    }

    public TextView getTxtLogDescription() {
        return txtLogDescription;
    }

    public void setTxtLogDescription(
            TextView txtLogDescription) {
        this.txtLogDescription = txtLogDescription;
    }

    public TextView getTxtLogUser() {
        return txtLogUser;
    }

    public void setTxtLogUser(
            TextView txtLogUser) {
        this.txtLogUser= txtLogUser;
    }

}

