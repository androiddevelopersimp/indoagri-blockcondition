package co.indoagri.blockcondition.adapter.view;



import android.widget.TextView;

public class ViewResultItem {
    private TextView txtBlock;

    public ViewResultItem(TextView txtBlock) {
        super();
        this.txtBlock = txtBlock;
    }

    public TextView getTxtBlock() {
        return txtBlock;
    }

    public void setTxtBlock(TextView txtBlock) {
        this.txtBlock = txtBlock;
    }
}
