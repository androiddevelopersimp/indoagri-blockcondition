package co.indoagri.blockcondition.adapter.view;

import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class ViewSummarySKB {
    private TextView txtTitle;
    private RadioGroup radioGroup1;
    private RadioButton radioButtonGreen1;
    private RadioButton radioButtonYellow1;
    private RadioButton radioButtonRed1;
    private RadioGroup radioGroup2;
    private RadioButton radioButtonGreen2;
    private RadioButton radioButtonYellow2;
    private RadioButton radioButtonRed2;
    private TextView txtRemark;

    public ViewSummarySKB(TextView txtTitle,RadioGroup radioGroup1,
                          RadioButton radioButtonGreen1,RadioButton radioButtonYellow1,
                          RadioButton radioButtonRed1,RadioGroup radioGroup2,
                          RadioButton radioButtonGreen2,RadioButton radioButtonYellow2,
                          RadioButton radioButtonRed2,
                          TextView txtRemark) {
        super();
        this.txtTitle = txtTitle;
        this.radioGroup1 = radioGroup1;
        this.radioGroup2 = radioGroup2;
        this.radioButtonGreen1 = radioButtonGreen1;
        this.radioButtonYellow1 = radioButtonYellow1;
        this.radioButtonRed1 = radioButtonRed1;
        this.radioButtonGreen2 = radioButtonGreen2;
        this.radioButtonYellow2 = radioButtonYellow2;
        this.radioButtonRed2 = radioButtonRed2;
        this.txtRemark = txtRemark;
    }

    public TextView getTxtTitle() {
        return txtTitle;
    }
    public RadioGroup getRadioGroup1() {
        return radioGroup1;
    }
    public RadioButton getRadioButtonGreen1() {
        return radioButtonGreen1;
    }
    public RadioButton getRadioButtonYellow1() {
        return radioButtonYellow1;
    }
    public RadioButton getRadioButtonRed1() {
        return radioButtonRed1;
    }
    public RadioGroup getRadioGroup2() {
        return radioGroup2;
    }
    public RadioButton getRadioButtonGreen2() {
        return radioButtonGreen2;
    }
    public RadioButton getRadioButtonYellow2() {
        return radioButtonYellow2;
    }
    public RadioButton getRadioButtonRed2() {
        return radioButtonRed2;
    }
    public TextView getTxtRemark() {
        return txtRemark;
    }

    public void setTxtTitle(TextView txtTitle) {
        this.txtTitle= txtTitle;
    }
    public void setRadioGroup1(RadioGroup radioGroup1) {
        this.radioGroup1= radioGroup1;
    }
    public void setRadioButtonGreen1(RadioButton radioButtonGreen1) {
        this.radioButtonGreen1= radioButtonGreen1;
    }
    public void setRadioButtonYellow1(RadioButton radioButtonYellow1) {
        this.radioButtonYellow1= radioButtonYellow1;
    }
    public void setRadioButtonRed(RadioButton radioButtonRed1) {
        this.radioButtonRed1= radioButtonRed1;
    }
    public void setRadioGroup2(RadioGroup radioGroup2) {
        this.radioGroup2= radioGroup2;
    }
    public void setRadioButtonGreen2(RadioButton radioButtonGreen2) {
        this.radioButtonGreen2= radioButtonGreen2;
    }
    public void setRadioButtonYellow2(RadioButton radioButtonYellow2) {
        this.radioButtonYellow2= radioButtonYellow2;
    }
    public void setRadioButtonRed2(RadioButton radioButtonRed2) {
        this.radioButtonRed2= radioButtonRed2;
    }
    public void setTxtRemark(TextView txtRemark) {
        this.txtRemark= txtRemark;
    }
}

