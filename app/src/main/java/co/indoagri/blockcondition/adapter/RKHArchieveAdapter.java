package co.indoagri.blockcondition.adapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.RKH.RKH_HEADER;
import co.indoagri.blockcondition.model.RKH.RKH_ITEM;

public class RKHArchieveAdapter extends RecyclerView.Adapter<RKHArchieveAdapter.ViewHolder> {
    ArrayList<RKH_HEADER> mValues;
    ArrayList<RKH_HEADER> filteredNameList;
    Context mContext;
    protected ItemListener mListener;
    protected ItemChooseListener itemChooseListener;
    private CompoundButton lastCheckedRB = null;
    DatabaseHandler database;
    public RKHArchieveAdapter(Context context, ArrayList<RKH_HEADER> values, ItemListener itemListener, ItemChooseListener itemChooseListener) {
        mValues = values;
        filteredNameList = values;
        mContext = context;
        mListener=itemListener;
        this.itemChooseListener = itemChooseListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView txtUsername;
        public ImageView iv_pekerja;
        public RadioButton rb_active;
        RKH_HEADER item;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);

            txtUsername = (TextView) v.findViewById(R.id.txtUsername);
            iv_pekerja = (ImageView)v.findViewById(R.id.iv_pekerja);
            rb_active = (RadioButton)v.findViewById(R.id.rb_Active);
        }

        public void setData(RKH_HEADER item) {
            this.item = item;
            database = new DatabaseHandler(mContext);
            //get first letter of each String item
            String firstLetter = String.valueOf(item.getRKH_DATE().charAt(0));
            String[] Datesss = String.valueOf(item.getRKH_DATE()).split(", ");

            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            // generate random color
            int color = generator.getColor(item);
            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(firstLetter, color); // radius in px
            iv_pekerja.setImageDrawable(drawable);
            txtUsername.setText(item.getRKH_DATE());
            CheckSetDate(item.getRKH_DATE(),txtUsername);
                rb_active.setOnCheckedChangeListener(ls);
                rb_active.setTag(getAdapterPosition());

        }

        private CompoundButton.OnCheckedChangeListener ls = (new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (itemChooseListener != null) {
                    itemChooseListener.onItemChoose(item,getAdapterPosition());
                }
                int tag = (int) buttonView.getTag();
                ;
                if (lastCheckedRB == null) {
                    lastCheckedRB = buttonView;
                } else if (tag != (int) lastCheckedRB.getTag()) {
                    lastCheckedRB.setChecked(false);
                    lastCheckedRB = buttonView;
                }

            }
        });

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(item,getAdapterPosition());
            }

        }
    }

    @Override
    public RKHArchieveAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_recyclerview_archieve, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, int position) {
        Vholder.setData(mValues.get(position));

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
    public void addData(RKH_HEADER rkh_header){
        boolean found = false;

        for(int i = 0; i < mValues.size(); i++){
            if(rkh_header.getRKH_ID().equalsIgnoreCase(mValues.get(i).getRKH_ID())){
                found = true;
                break;
            }
        }

        if(!found){
            mValues.add(rkh_header);
            notifyDataSetChanged();
        }
    }
    public interface ItemListener {
        void onItemClick(RKH_HEADER item, int position);
    }
    public interface ItemChooseListener {
        void onItemChoose(RKH_HEADER item, int position);
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charSequenceString = constraint.toString();
                if (charSequenceString.isEmpty()) {
                    mValues = filteredNameList;
                } else {
                    ArrayList<RKH_HEADER> filteredList = new ArrayList<>();
                    for (RKH_HEADER name : filteredNameList) {
                        if (convertDate(name.getRKH_DATE()).toLowerCase().contains(charSequenceString.toLowerCase())) {
                            filteredList.add(name);
                        }
                        mValues = filteredList;
                    }

                }
                FilterResults results = new FilterResults();
                results.values = mValues;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mValues = null;
                mValues = (ArrayList<RKH_HEADER>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    void CheckSetDate(String Dates, TextView txtDate){
        try{
            Date parsedDate = DateLocal.FORMAT_INPUT.parse(Dates);
            Calendar cal = Calendar.getInstance();
            cal.setTime(parsedDate);
            int days = cal.get(Calendar.DAY_OF_WEEK);
            int dates = cal.get(Calendar.DATE);
            int months = cal.get(Calendar.MONTH);
            int years = cal.get(Calendar.YEAR);

            int hours = cal.get(Calendar.HOUR);
            int minutes = cal.get(Calendar.MINUTE);
            int seconds = cal.get(Calendar.SECOND);
            int amPm = cal.get(Calendar.AM_PM);

            switch (amPm) {
                case Calendar.AM:

                    break;
                case Calendar.PM:
                    hours = hours + 12;
                default:
                    break;
            }

            // txtDate.setText(digitFormat(hours, 2) + ":" + digitFormat(minutes,2) + ":" + digitFormat(seconds,2));

            String hari = "";

            switch (days) {
                case Calendar.SUNDAY:
                    hari = mContext.getResources().getString(R.string.minggu);
                    break;
                case Calendar.MONDAY:
                    hari = mContext.getResources().getString(R.string.senin);
                    break;
                case Calendar.TUESDAY:
                    hari = mContext.getResources().getString(R.string.selasa);
                    break;
                case Calendar.WEDNESDAY:
                    hari = mContext.getResources().getString(R.string.rabu);
                    break;
                case Calendar.THURSDAY:
                    hari = mContext.getResources().getString(R.string.kamis);
                    break;
                case Calendar.FRIDAY:
                    hari = mContext.getResources().getString(R.string.jumat);
                    break;
                case Calendar.SATURDAY:
                    hari = mContext.getResources().getString(R.string.sabtu);
                    break;
                default:
                    break;
            }


            String bulan = "";

            switch (months) {
                case Calendar.JANUARY:
                    bulan = mContext.getResources().getString(R.string.januari);
                    break;
                case Calendar.FEBRUARY:
                    bulan =mContext.getResources().getString(R.string.februari);
                    break;
                case Calendar.MARCH:
                    bulan = mContext.getResources().getString(R.string.maret);
                    break;
                case Calendar.APRIL:
                    bulan = mContext.getResources().getString(R.string.april);
                    break;
                case Calendar.MAY:
                    bulan = mContext.getResources().getString(R.string.mei);
                    break;
                case Calendar.JUNE:
                    bulan = mContext.getResources().getString(R.string.juni);
                    break;
                case Calendar.JULY:
                    bulan = mContext.getResources().getString(R.string.juli);
                    break;
                case Calendar.AUGUST:
                    bulan = mContext.getResources().getString(R.string.agustus);
                    break;
                case Calendar.SEPTEMBER:
                    bulan = mContext.getResources().getString(R.string.september);
                    break;
                case Calendar.OCTOBER:
                    bulan = mContext.getResources().getString(R.string.oktober);
                    break;
                case Calendar.NOVEMBER:
                    bulan = mContext.getResources().getString(R.string.november);
                    break;
                case Calendar.DECEMBER:
                    bulan = mContext.getResources().getString(R.string.desember);
                    break;
                default:
                    break;
            }
            int TotalRKH = TotalItem(Dates);
            String value =  hari + " , " + dates + " " + bulan + " " + years+" ( "+String.valueOf(TotalRKH)+" RKH )";
            txtDate.setText(value);

        }catch (Exception e) {}

    }
     private String convertDate(String Dates) {
        String value ="";
        try {
            Date parsedDate = DateLocal.FORMAT_INPUT.parse(Dates);
            Calendar cal = Calendar.getInstance();
            cal.setTime(parsedDate);
            int days = cal.get(Calendar.DAY_OF_WEEK);
            int dates = cal.get(Calendar.DATE);
            int months = cal.get(Calendar.MONTH);
            int years = cal.get(Calendar.YEAR);

            int hours = cal.get(Calendar.HOUR);
            int minutes = cal.get(Calendar.MINUTE);
            int seconds = cal.get(Calendar.SECOND);
            int amPm = cal.get(Calendar.AM_PM);

            switch (amPm) {
                case Calendar.AM:

                    break;
                case Calendar.PM:
                    hours = hours + 12;
                default:
                    break;
            }

            // txtDate.setText(digitFormat(hours, 2) + ":" + digitFormat(minutes,2) + ":" + digitFormat(seconds,2));

            String hari = "";

            switch (days) {
                case Calendar.SUNDAY:
                    hari = mContext.getResources().getString(R.string.minggu);
                    break;
                case Calendar.MONDAY:
                    hari = mContext.getResources().getString(R.string.senin);
                    break;
                case Calendar.TUESDAY:
                    hari = mContext.getResources().getString(R.string.selasa);
                    break;
                case Calendar.WEDNESDAY:
                    hari = mContext.getResources().getString(R.string.rabu);
                    break;
                case Calendar.THURSDAY:
                    hari = mContext.getResources().getString(R.string.kamis);
                    break;
                case Calendar.FRIDAY:
                    hari = mContext.getResources().getString(R.string.jumat);
                    break;
                case Calendar.SATURDAY:
                    hari = mContext.getResources().getString(R.string.sabtu);
                    break;
                default:
                    break;
            }


            String bulan = "";

            switch (months) {
                case Calendar.JANUARY:
                    bulan = mContext.getResources().getString(R.string.januari);
                    break;
                case Calendar.FEBRUARY:
                    bulan = mContext.getResources().getString(R.string.februari);
                    break;
                case Calendar.MARCH:
                    bulan = mContext.getResources().getString(R.string.maret);
                    break;
                case Calendar.APRIL:
                    bulan = mContext.getResources().getString(R.string.april);
                    break;
                case Calendar.MAY:
                    bulan = mContext.getResources().getString(R.string.mei);
                    break;
                case Calendar.JUNE:
                    bulan = mContext.getResources().getString(R.string.juni);
                    break;
                case Calendar.JULY:
                    bulan = mContext.getResources().getString(R.string.juli);
                    break;
                case Calendar.AUGUST:
                    bulan = mContext.getResources().getString(R.string.agustus);
                    break;
                case Calendar.SEPTEMBER:
                    bulan = mContext.getResources().getString(R.string.september);
                    break;
                case Calendar.OCTOBER:
                    bulan = mContext.getResources().getString(R.string.oktober);
                    break;
                case Calendar.NOVEMBER:
                    bulan = mContext.getResources().getString(R.string.november);
                    break;
                case Calendar.DECEMBER:
                    bulan = mContext.getResources().getString(R.string.desember);
                    break;
                default:
                    break;
            }
            int TotalRKH = TotalItem(Dates);
                value =  hari + " , " + dates + " " + bulan + " " + years+" ( "+String.valueOf(TotalRKH)+" RKH )";
//            value =  hari + " , " + dates + " " + bulan + " " + years;

        } catch (Exception e) {
        }

         return value;
     }
    private int TotalItem(String Datess){
        database.openTransaction();
        String[] a = new String[1];
        a[0] = Datess;
        String query = "Select * from "+ RKH_HEADER.TABLE_NAME+" where "+RKH_HEADER.XML_RKH_DATE+" = ? ";
        int Total = database.getTaskCount(query,a);
        database.commitTransaction();
        database.closeTransaction();
        return Total;
    }
}