package co.indoagri.blockcondition.adapter;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.view.ViewLogProcess;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.LogModel;

public class AdapterLogProcess extends BaseAdapter{
    private Context context;
    private List<LogModel> lstLog, lstTemp;
    private int layout;

    public AdapterLogProcess(Context context, List<LogModel> listMaster, int layout){
        this.context = context;
        this.lstLog = listMaster;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return lstLog.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstLog.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        LogModel item = lstLog.get(pos);

        ViewLogProcess view;

        TextView txtLogID;
        TextView txtLogDescription;
        TextView txtLogDate;
        TextView txtLogUser;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtLogID = (TextView) convertView.findViewById(R.id.txtLogID);
            txtLogDescription= (TextView) convertView.findViewById(R.id.txtLogDescripition);
            txtLogDate= (TextView) convertView.findViewById(R.id.txtLogTime);
            txtLogUser= (TextView) convertView.findViewById(R.id.txtLogUser);

            view = new ViewLogProcess(txtLogID, txtLogDescription, txtLogDate, txtLogUser);
            convertView.setTag(view);
        }else{
            view = (ViewLogProcess) convertView.getTag();

            txtLogID = view.getTxtLogID();
            txtLogDescription= view.getTxtLogDescription();
            txtLogDate= view.getTxtLogDate();
            txtLogUser= view.getTxtLogUser();
        }

        txtLogID.setText(item.getRowId());
        txtLogDescription.setText(item.getDescription());
        txtLogDate.setText(item.getCreatedDate());
        txtLogUser.setText(item.getUserCode());

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstLog = null;
                lstLog= (List<LogModel>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<LogModel> listResult = new ArrayList<LogModel>();
                LogModel md;

                if(lstTemp == null){
                    lstTemp = new ArrayList<LogModel>(lstLog);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        md = lstTemp.get(i);

                        if(md.getDescription().toLowerCase(Locale.getDefault()).contains(constraint)){
                            listResult.add(md);
                        }
                    }
                    result.count = listResult.size();
                    result.values = listResult;
                }

                return result;
            }
        };
        return filter;
    }
}
