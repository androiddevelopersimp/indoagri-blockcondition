package co.indoagri.blockcondition.adapter.view;

import android.widget.TextView;

public class ViewBlockHdrcItem {
	private TextView txt_block_hdrc_item_id;
	private TextView txt_block_hdrc_item_company_code;
	private TextView txt_block_hdrc_item_estate;
	private TextView txt_block_hdrc_item_block;
	private TextView txt_block_hdrc_item_valid_from;
	private TextView txt_block_hdrc_item_valid_to;
	private TextView txt_block_hdrc_item_divisi;
	private TextView txt_block_hdrc_item_type;
	private TextView txt_block_hdrc_item_status;
	private TextView txt_block_hdrc_item_project_definition;
	private TextView txt_block_hdrc_item_owner;
	private TextView txt_block_hdrc_item_tgl_tanam;
	private TextView txt_block_hdrc_item_mandt;
	
	public ViewBlockHdrcItem(TextView txt_block_hdrc_item_id, TextView txt_block_hdrc_item_company_code, TextView txt_block_hdrc_item_estate, TextView txt_block_hdrc_item_block,
                             TextView txt_block_hdrc_item_valid_from, TextView txt_block_hdrc_item_valid_to, TextView txt_block_hdrc_item_baris_blok,
                             TextView txt_block_hdrc_item_jumlah_pokok, TextView txt_block_hdrc_item_pokok_mati, TextView txt_block_hdrc_item_tanggal_tanam,
                             TextView txt_block_hdrc_item_line_block_hdrc, TextView txt_block_hdrc_item_tgl_tanam, TextView txt_block_hdrc_item_mandt){
		this.txt_block_hdrc_item_id = txt_block_hdrc_item_id;
		this.txt_block_hdrc_item_company_code = txt_block_hdrc_item_company_code;
		this.txt_block_hdrc_item_estate = txt_block_hdrc_item_estate;
		this.txt_block_hdrc_item_block = txt_block_hdrc_item_block;
		this.txt_block_hdrc_item_valid_from = txt_block_hdrc_item_valid_from;
		this.txt_block_hdrc_item_valid_to = txt_block_hdrc_item_valid_to;   
		this.txt_block_hdrc_item_divisi = txt_block_hdrc_item_baris_blok;
		this.txt_block_hdrc_item_type = txt_block_hdrc_item_jumlah_pokok;
		this.txt_block_hdrc_item_status = txt_block_hdrc_item_pokok_mati;
		this.txt_block_hdrc_item_project_definition = txt_block_hdrc_item_tanggal_tanam;
		this.txt_block_hdrc_item_owner = txt_block_hdrc_item_line_block_hdrc;
		this.txt_block_hdrc_item_tgl_tanam = txt_block_hdrc_item_tgl_tanam;
		this.txt_block_hdrc_item_mandt = txt_block_hdrc_item_mandt;
	}

	public TextView getTxtblockHdrcItemId() {
		return txt_block_hdrc_item_id;
	}

	public void setTxtblockHdrcItemId(TextView txt_block_hdrc_item_id) {
		this.txt_block_hdrc_item_id = txt_block_hdrc_item_id;
	}

	public TextView getTxtBlockHdrcItemCompanyCode() {
		return txt_block_hdrc_item_company_code;
	}

	public void setTxtblockHdrcItemCompanyCode(TextView txt_block_hdrc_item_company_code) {
		this.txt_block_hdrc_item_company_code = txt_block_hdrc_item_company_code;
	}

	public TextView getTxtblockHdrcItemEstate() {
		return txt_block_hdrc_item_estate;
	}

	public void setTxtblockHdrcItemEstate(TextView txt_block_hdrc_item_estate) {
		this.txt_block_hdrc_item_estate = txt_block_hdrc_item_estate;
	}

	public TextView getTxtblockHdrcItemBlock() {
		return txt_block_hdrc_item_block;
	}

	public void setTxtblockHdrcItemBlock(TextView txt_block_hdrc_item_block) {
		this.txt_block_hdrc_item_block = txt_block_hdrc_item_block;
	}
	
	public TextView getTxtblockHdrcItemValidFrom() {
		return txt_block_hdrc_item_valid_from;
	}

	public void setTxtblockHdrcItemValidFrom(TextView txt_block_hdrc_item_valid_from) {
		this.txt_block_hdrc_item_valid_from = txt_block_hdrc_item_valid_from;
	}

	public TextView getTxtblockHdrcItemValidTo() {
		return txt_block_hdrc_item_valid_to;
	}

	public void setTxtblockHdrcItemValidTo(TextView txt_block_hdrc_item_valid_to) {
		this.txt_block_hdrc_item_valid_to = txt_block_hdrc_item_valid_to;
	}

	public TextView getTxtblockHdrcItemDivisi() {
		return txt_block_hdrc_item_divisi;
	}

	public void setTxtblockHdrcItemDivisi(TextView txt_block_hdrc_item_divisi) {
		this.txt_block_hdrc_item_divisi = txt_block_hdrc_item_divisi;
	}

	public TextView getTxtblockHdrcItemType() {
		return txt_block_hdrc_item_type;
	}

	public void setTxtblockHdrcItemType(TextView txt_block_hdrc_item_type) {
		this.txt_block_hdrc_item_type = txt_block_hdrc_item_type;
	}

	public TextView getTxtblockHdrcItemStatus() {
		return txt_block_hdrc_item_status;
	}

	public void setTxtblockHdrcItemStatus(TextView txt_block_hdrc_item_status) {
		this.txt_block_hdrc_item_status = txt_block_hdrc_item_status;
	}

	public TextView getTxtblockHdrcItemProjectDefinition() {
		return txt_block_hdrc_item_project_definition;
	}

	public void setTxtblockHdrcItemProjectDefinition(TextView txt_block_hdrc_item_project_definition) {
		this.txt_block_hdrc_item_project_definition = txt_block_hdrc_item_project_definition;
	}

	public TextView getTxtblockHdrcItemOwner() {
		return txt_block_hdrc_item_owner;
	}

	public void setTxtblockHdrcItemOwner(TextView txt_block_hdrc_item_owner) {
		this.txt_block_hdrc_item_owner = txt_block_hdrc_item_owner;
	}

	public TextView getTxtBlockHdrcItemTglTanam() {
		return txt_block_hdrc_item_tgl_tanam;
	}

	public void setTxtBlockHdrcItemTglTanam(TextView txt_block_hdrc_item_tgl_tanam) {
		this.txt_block_hdrc_item_tgl_tanam = txt_block_hdrc_item_tgl_tanam;
	}

	public TextView getTxtblockHdrcItemMandt() {
		return txt_block_hdrc_item_mandt;
	}

	public void setTxtblockHdrcItemMandt(TextView txt_block_hdrc_item_mandt) {
		this.txt_block_hdrc_item_mandt = txt_block_hdrc_item_mandt;
	}
}
