package co.indoagri.blockcondition.adapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.model.RKH.RKH_ACTIVITY_TYPE;

public class RKHActivityTypeAdapter extends RecyclerView.Adapter<RKHActivityTypeAdapter.ViewHolder> {
    ArrayList<RKH_ACTIVITY_TYPE> mValues;
    ArrayList<RKH_ACTIVITY_TYPE> filteredNameList;
    Context mContext;
    protected ItemListener mListener;
    protected ItemChooseListener itemChooseListener;
    private CompoundButton lastCheckedRB = null;
    public RKHActivityTypeAdapter(Context context, ArrayList<RKH_ACTIVITY_TYPE> values, ItemListener itemListener, ItemChooseListener itemChooseListener) {
        mValues = values;
        filteredNameList = values;
        mContext = context;
        mListener=itemListener;
        this.itemChooseListener = itemChooseListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView txtUsername;
        public ImageView iv_pekerja;
        public TextView txtID;
        public RadioButton rb_active;
        RKH_ACTIVITY_TYPE item;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);

            txtUsername = (TextView) v.findViewById(R.id.txtUsername);
            txtID = (TextView) v.findViewById(R.id.txtID);
            iv_pekerja = (ImageView)v.findViewById(R.id.iv_pekerja);
            rb_active = (RadioButton)v.findViewById(R.id.rb_Active);
        }

        public void setData(RKH_ACTIVITY_TYPE item) {
            this.item = item;
            //get first letter of each String item
            String firstLetter = String.valueOf(item.getNAME().charAt(0));
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            // generate random color
            int color = generator.getColor(item);
            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(firstLetter, color); // radius in px
            iv_pekerja.setImageDrawable(drawable);
            txtUsername.setText(item.getNAME());
            txtID.setVisibility(View.VISIBLE);
            txtID.setText(item.getACTTYPE());
                rb_active.setOnCheckedChangeListener(ls);
                rb_active.setTag(getAdapterPosition());

        }

        private CompoundButton.OnCheckedChangeListener ls = (new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (itemChooseListener != null) {
                    itemChooseListener.onItemChoose(item,getAdapterPosition());
                }
                int tag = (int) buttonView.getTag();
                ;
                if (lastCheckedRB == null) {
                    lastCheckedRB = buttonView;
                } else if (tag != (int) lastCheckedRB.getTag()) {
                    lastCheckedRB.setChecked(false);
                    lastCheckedRB = buttonView;
                }

            }
        });

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(item,getAdapterPosition());
            }

        }
    }

    @Override
    public RKHActivityTypeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_recyclerview_pekerja, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, int position) {
        Vholder.setData(mValues.get(position));

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
    public void addData(RKH_ACTIVITY_TYPE employee){
        boolean found = false;

        for(int i = 0; i < mValues.size(); i++){
            if(employee.getNAME().equalsIgnoreCase(mValues.get(i).getACTTYPE())){
                found = true;
                break;
            }
        }

        if(!found){
            mValues.add(employee);
            notifyDataSetChanged();
        }
    }
    public interface ItemListener {
        void onItemClick(RKH_ACTIVITY_TYPE item, int position);
    }
    public interface ItemChooseListener {
        void onItemChoose(RKH_ACTIVITY_TYPE item, int position);
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charSequenceString = constraint.toString();
                if (charSequenceString.isEmpty()) {
                    mValues = filteredNameList;
                } else {
                    ArrayList<RKH_ACTIVITY_TYPE> filteredList = new ArrayList<>();
                    for (RKH_ACTIVITY_TYPE name : filteredNameList) {
                        if (name.getACTTYPE().toLowerCase().contains(charSequenceString.toLowerCase())) {
                            filteredList.add(name);
                        }
                        mValues = filteredList;
                    }

                }
                FilterResults results = new FilterResults();
                results.values = mValues;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mValues = null;
                mValues = (ArrayList<RKH_ACTIVITY_TYPE>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}