package co.indoagri.blockcondition.adapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.model.Data.BlockHdrc;

public class RKHBlockAdapter extends RecyclerView.Adapter<RKHBlockAdapter.ViewHolder> {
    ArrayList<BlockHdrc> mValues;
    ArrayList<BlockHdrc> filteredNameList;
    Context mContext;
    protected ItemListener mListener;
    protected ItemChooseListener itemChooseListener;
    private CompoundButton lastCheckedRB = null;
    public RKHBlockAdapter(Context context, ArrayList<BlockHdrc> values, ItemListener itemListener, ItemChooseListener itemChooseListener) {
        mValues = values;
        filteredNameList = values;
        mContext = context;
        mListener=itemListener;
        this.itemChooseListener = itemChooseListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView txtUsername;
        public ImageView iv_pekerja;
        public RadioButton rb_active;
        BlockHdrc item;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);

            txtUsername = (TextView) v.findViewById(R.id.txtUsername);
            iv_pekerja = (ImageView)v.findViewById(R.id.iv_pekerja);
            rb_active = (RadioButton)v.findViewById(R.id.rb_Active);
        }

        public void setData(BlockHdrc item) {
            this.item = item;
            //get first letter of each String item
            String firstLetter = String.valueOf(item.getBlock().charAt(0));
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            // generate random color
            int color = generator.getColor(item);
            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(firstLetter, color); // radius in px
            iv_pekerja.setImageDrawable(drawable);
            txtUsername.setText(item.getBlock());
                rb_active.setOnCheckedChangeListener(ls);
                rb_active.setTag(getAdapterPosition());

        }

        private CompoundButton.OnCheckedChangeListener ls = (new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (itemChooseListener != null) {
                    itemChooseListener.onItemChoose(item,getAdapterPosition());
                }
                int tag = (int) buttonView.getTag();
                ;
                if (lastCheckedRB == null) {
                    lastCheckedRB = buttonView;
                } else if (tag != (int) lastCheckedRB.getTag()) {
                    lastCheckedRB.setChecked(false);
                    lastCheckedRB = buttonView;
                }

            }
        });

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(item,getAdapterPosition());
            }

        }
    }

    @Override
    public RKHBlockAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_recyclerview_pekerja, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, int position) {
        Vholder.setData(mValues.get(position));

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
    public void addData(BlockHdrc blockHdrc){
        boolean found = false;

        for(int i = 0; i < mValues.size(); i++){
            if(blockHdrc.getBlock().equalsIgnoreCase(mValues.get(i).getBlock())){
                found = true;
                break;
            }
        }

        if(!found){
            mValues.add(blockHdrc);
            notifyDataSetChanged();
        }
    }
    public interface ItemListener {
        void onItemClick(BlockHdrc item, int position);
    }
    public interface ItemChooseListener {
        void onItemChoose(BlockHdrc item, int position);
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charSequenceString = constraint.toString();
                if (charSequenceString.isEmpty()) {
                    mValues = filteredNameList;
                } else {
                    ArrayList<BlockHdrc> filteredList = new ArrayList<>();
                    for (BlockHdrc name : filteredNameList) {
                        if (name.getBlock().toLowerCase().contains(charSequenceString.toLowerCase())) {
                            filteredList.add(name);
                        }
                        mValues = filteredList;
                    }

                }
                FilterResults results = new FilterResults();
                results.values = mValues;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mValues = null;
                mValues = (ArrayList<BlockHdrc>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}