package co.indoagri.blockcondition.adapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.model.RKH.RKH_ITEM;

public class RKHITEMAdapter extends RecyclerView.Adapter<RKHITEMAdapter.ViewHolder> {

    List<RKH_ITEM> mValues;
    Context mContext;
    protected ItemListener mListener;
    protected CardDelete cardDeleteListener;
    protected  CardEdit cardEditListener;

    public RKHITEMAdapter(Context context, List<RKH_ITEM> values, ItemListener itemListener, CardDelete cardDeleteListener, CardEdit cardEditListener) {

        mValues = values;
        mContext = context;
        mListener=itemListener;
        this.cardEditListener = cardEditListener;
        this.cardDeleteListener = cardDeleteListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView txtAktifitas;
        public TextView txtBlok;
        public TextView txtHariKerja;
        public TextView txtSKU;
        public TextView txtGender;
        public TextView txtPHL;
        public TextView txtHA;
        public TextView txtJanjang;
        public TextView txtTonase;
        public TextView txtAktifitasCount;
        public LinearLayout cardButton;
        public LinearLayout cardButton2;
        public LinearLayout expendableLayout;
        RKH_ITEM item;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            txtAktifitasCount = (TextView) v.findViewById(R.id.txtAktifitasCount);
            txtAktifitas = (TextView) v.findViewById(R.id.itemAktifitas);
            txtBlok = (TextView) v.findViewById(R.id.itemBlock);
            txtHariKerja = (TextView) v.findViewById(R.id.itemHariKerja);
            txtSKU = (TextView) v.findViewById(R.id.itemSKU);
            txtGender = (TextView) v.findViewById(R.id.itemResultGender);
            txtPHL = (TextView) v.findViewById(R.id.itemPHL);
            txtHA = (TextView) v.findViewById(R.id.itemHA);
            txtJanjang = (TextView) v.findViewById(R.id.itemJenjang);
            txtTonase = (TextView) v.findViewById(R.id.itemTonase);
            cardButton = (LinearLayout) v.findViewById(R.id.cardJob);
            cardButton2 = (LinearLayout) v.findViewById(R.id.cardJob2);
            expendableLayout = (LinearLayout)v.findViewById(R.id.expendableLayout);
            cardButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cardDeleteListener != null) {
                        cardDeleteListener.onCardDelete(item);
                    }
                }
            });
            cardButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cardEditListener != null) {
                        cardEditListener.onCardEdit(item);
                    }
                }
            });

        }

        public void setData(RKH_ITEM item) {
            this.item = item;
            String[] separated = item.getACTIVITY().split(";");
            String ActivityType = separated[0];
            String ActivityTypeName = separated[1];
            txtAktifitasCount.setText(Html.fromHtml("Aktifitas : <b>"+ item.getLINE() +"</b>"));
            txtAktifitas.setText(Html.fromHtml("Aktifitas : <b>"+ ActivityType+" - "+ActivityTypeName +"</b>"));
            txtBlok.setText(Html.fromHtml("Blok : <b>"+ item.getBLOCK() +"</b>"));
            txtHariKerja.setText(Html.fromHtml("Hari Kerja : <b>"+ "1" +"</b>"));
            txtSKU.setText(Html.fromHtml("SKU : <b>"+ item.getSKU() +"</b>"));
            txtGender.setText(Html.fromHtml(" Gender <b>"+ "---" +"</b>"));
            txtGender.setVisibility(View.GONE);
            txtPHL.setText(Html.fromHtml("PHL : <b>"+ item.getPHL() +"</b>"));
            txtHA.setText(Html.fromHtml("HA : <b>"+ item.getTARGET_OUTPUT() +"</b>"));
            txtJanjang.setText(Html.fromHtml("Target Janjang : <b>"+ item.getTARGET_JANJANG() +"</b>"));
            txtTonase.setText(Html.fromHtml("Tonase : <b>"+ item.getTARGET_HASIL() +"</b>"));
        }

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(item,getAdapterPosition());
            }
        }
    }

    @Override
    public RKHITEMAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_recyclerview_rkh_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, int position) {
        Vholder.setData(mValues.get(position));

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public interface ItemListener {
        void onItemClick(RKH_ITEM item, int position);
    }
    public interface CardDelete {
        void onCardDelete(RKH_ITEM item);
    }
    public interface CardEdit {
        void onCardEdit(RKH_ITEM item);
    }
    public void addData(RKH_ITEM rkh_item){
        boolean found = false;
        for(int i = 0; i < mValues.size(); i++){
            if(rkh_item.getLINE().equalsIgnoreCase(mValues.get(i).getLINE())){
                found = true;
                break;
            }
        }

        if(!found){
            mValues.add(rkh_item);
            notifyDataSetChanged();
        }
    }
}