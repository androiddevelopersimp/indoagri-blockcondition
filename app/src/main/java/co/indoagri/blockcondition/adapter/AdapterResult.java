package co.indoagri.blockcondition.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.view.ViewBlockHdrcItem;
import co.indoagri.blockcondition.adapter.view.ViewResultItem;
import co.indoagri.blockcondition.model.Data.BlockHdrc;
import co.indoagri.blockcondition.model.Data.ResultModel;

public class AdapterResult extends BaseAdapter {
    private Context context;
    private List<ResultModel> resultModels, lst_temp;
    private int layout;

    private ViewResultItem viewResultItem;
    private TextView txtItemBlock;
    private CardView cardView;
    public AdapterResult(Context context, List<ResultModel> resultModels, int layout){
        this.context = context;
        this.resultModels = resultModels;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return resultModels.size();
    }

    @Override
    public Object getItem(int pos) {
        return resultModels.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        ResultModel resultModel = resultModels.get(pos);

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtItemBlock = (TextView) convertView.findViewById(R.id.txtItemBlock);
            cardView = (CardView) convertView.findViewById(R.id.cardView);

            viewResultItem = new ViewResultItem(txtItemBlock);
            convertView.setTag(viewResultItem);
        }else{
            viewResultItem = (ViewResultItem) convertView.getTag();

            txtItemBlock = viewResultItem.getTxtBlock();
        }

        double Condition = Double.parseDouble(resultModel.getCondition());


        if(Condition <= 3){
            txtItemBlock.setText(resultModel.getBlock());
            cardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorGreen));
        }
        if(Condition <= 2){
            txtItemBlock.setText(resultModel.getBlock());
            cardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorYellow));
        }
        if(Condition <= 1){
            txtItemBlock.setText(resultModel.getBlock());
            cardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorRed));
        }

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                resultModels = null;
                resultModels = (List<ResultModel>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<ResultModel> list_result = new ArrayList<ResultModel>();
                ResultModel resultModel;

                if(lst_temp == null){
                    lst_temp = new ArrayList<ResultModel>(resultModels);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lst_temp.size();
                    result.values = lst_temp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lst_temp.size(); i++){
                        resultModel = lst_temp.get(i);

                        if(resultModel.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
                            list_result.add(resultModel);
                        }
                    }
                    result.count = list_result.size();
                    result.values = list_result;
                }

                return result;
            }
        };
        return filter;
    }

    public void addData(ResultModel resultModel){
        boolean found = false;

        for(int i = 0; i < resultModels.size(); i++){
            if(resultModel.getBlock().equalsIgnoreCase(resultModels.get(i).getBlock())){
                found = true;
                break;
            }
        }

        if(!found){
            resultModels.add(resultModel);
            notifyDataSetChanged();
        }
    }
}
