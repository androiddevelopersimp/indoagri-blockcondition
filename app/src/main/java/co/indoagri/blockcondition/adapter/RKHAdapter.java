package co.indoagri.blockcondition.adapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.activity.RKHJOBActivity;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.model.RKH.RKH_HEADER;
import co.indoagri.blockcondition.model.RKH.RKH_ITEM;

public class RKHAdapter extends RecyclerView.Adapter<RKHAdapter.ViewHolder> {

    List<RKH_HEADER> mValues;
    Context mContext;
    protected ItemListener mListener;
    protected CardTugaskan cardTugaskanListener;
    protected  CardEdit cardEditListener;

    public RKHAdapter(Context context, List<RKH_HEADER> values, ItemListener itemListener, CardTugaskan cardTugaskanListener, CardEdit cardEditListener) {

        mValues = values;
        mContext = context;
        mListener=itemListener;
        this.cardEditListener = cardEditListener;
        this.cardTugaskanListener = cardTugaskanListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txtForeMan;
        public TextView txtNIK;
        public TextView txtNIKClerk;
        public TextView txtClerk;
        public TextView txtGang;
        public TextView txtGang2;
        public TextView txtAktifitas;
        public TextView txtBlok;
        public TextView txtHariKerja;
        public TextView txtSKU;
        public TextView txtGender;
        public TextView txtPHL;
        public TextView txtHA;
        public TextView txtJanjang;
        public TextView txtTonase;
        public LinearLayout cardButton;
        public LinearLayout cardButton2;
        public LinearLayout expendableLayout;
        RKH_HEADER item;

        public ViewHolder(View v) {

            super(v);

            v.setOnClickListener(this);
            txtForeMan = (TextView) v.findViewById(R.id.txtForeman);
            txtNIK = (TextView) v.findViewById(R.id.txtNik);
            txtNIKClerk = (TextView) v.findViewById(R.id.txtNikKerani);
            txtClerk = (TextView) v.findViewById(R.id.txtClerk);
            txtGang = (TextView) v.findViewById(R.id.txtGang);
            txtGang2 = (TextView) v.findViewById(R.id.txtGang2);
            txtAktifitas = (TextView) v.findViewById(R.id.itemAktifitas);
            txtBlok = (TextView) v.findViewById(R.id.itemBlock);
            txtHariKerja = (TextView) v.findViewById(R.id.itemHariKerja);
            txtSKU = (TextView) v.findViewById(R.id.itemSKU);
            txtGender = (TextView) v.findViewById(R.id.itemResultGender);
            txtPHL = (TextView) v.findViewById(R.id.itemPHL);
            txtHA = (TextView) v.findViewById(R.id.itemHA);
            txtJanjang = (TextView) v.findViewById(R.id.itemJenjang);
            txtTonase = (TextView) v.findViewById(R.id.itemTonase);
            cardButton = (LinearLayout) v.findViewById(R.id.cardJob);
            cardButton2 = (LinearLayout) v.findViewById(R.id.cardJob2);
            expendableLayout = (LinearLayout)v.findViewById(R.id.expendableLayout);
            cardButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cardTugaskanListener != null) {
                        cardTugaskanListener.onCardTugaskan(item);
                    }
                }
            });
            cardButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cardEditListener != null) {
                        cardEditListener.onCardEdit(item);
                    }
                }
            });

        }

        public void setData(RKH_HEADER item) {
            this.item = item;
            DatabaseHandler database = new DatabaseHandler(mContext);
            txtNIK.setText(item.NIK_FOREMAN);

            txtGang.setText(item.GANG);
            if(item.getNIK_CLERK().isEmpty()){
                txtNIKClerk.setText(null);
                txtGang2.setText(null);
                txtClerk.setText(null);
            }else{
                txtNIKClerk.setText(item.NIK_CLERK);
                txtGang2.setText(item.GANG);
                txtClerk.setText(item.CLERK);
            }

            txtForeMan.setText(item.FOREMAN);
            String testing = "41-02 Harvest";
            String gender = "19 Lak-laki, 0 Perempuan";

            List<RKH_HEADER> listTemp = new ArrayList<RKH_HEADER>();
            List<Object> listObject;

            database.openTransaction();
            listObject = database.getListData(false, RKH_ITEM.TABLE_NAME, null,
                    RKH_ITEM.XML_RKH_ID+ "=? ",
                    new String [] {item.getRKH_ID()},
                    null, null, null, null);
            database.closeTransaction();
            if(listObject.size()>0){
                cardButton.setVisibility(View.GONE);
                cardButton2.setVisibility(View.VISIBLE);
                for(int i = 0; i < listObject.size(); i++){
                    RKH_ITEM rkh_item = (RKH_ITEM) listObject.get(i);
                    String[] separated = rkh_item.getACTIVITY().split(";");
                    String ActivityType = separated[0];
                    String ActivityTypeName = separated[1];
                    txtAktifitas.setText(Html.fromHtml("Activitas : <b>"+ ActivityType+" - "+ActivityTypeName +"</b>"));
                    txtBlok.setText(Html.fromHtml("Blok : <b>"+ rkh_item.getBLOCK() +"</b>"));
                    txtHariKerja.setText(Html.fromHtml("Hari Kerja : <b>"+ "1" +"</b>"));
                    txtSKU.setText(Html.fromHtml("SKU : <b>"+ rkh_item.getSKU() +"</b>"));
                    txtGender.setText(Html.fromHtml(" Gender : <b>"+ "0" +"</b>"));
                    txtPHL.setText(Html.fromHtml("PHL : <b>"+ rkh_item.getPHL() +"</b>"));
                    txtHA.setText(Html.fromHtml("HA : <b>"+ rkh_item.getTARGET_OUTPUT() +"</b>"));
                    txtJanjang.setText(Html.fromHtml("Target Janjang : <b>"+ rkh_item.getTARGET_JANJANG() +"</b>"));
                    txtTonase.setText(Html.fromHtml("Tonase : <b>"+ rkh_item.getTARGET_HASIL() +"</b>"));
                }
                boolean isExpendable = item.isExpendable();
                expendableLayout.setClickable(true);
                expendableLayout.setVisibility(isExpendable ? View.VISIBLE : View.GONE);
                expendableLayout.setVisibility(View.VISIBLE);
            }else{
                expendableLayout.setVisibility(View.GONE);
                cardButton2.setVisibility(View.GONE);
                cardButton.setVisibility(View.VISIBLE);
                expendableLayout.setClickable(false);
            }

        }

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(item,getAdapterPosition());
            }
        }
    }

    @Override
    public RKHAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_recyclerview_rkh, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, int position) {
        Vholder.setData(mValues.get(position));

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public interface ItemListener {
        void onItemClick(RKH_HEADER item, int position);
    }
    public interface CardTugaskan {
        void onCardTugaskan(RKH_HEADER item);
    }
    public interface CardEdit {
        void onCardEdit(RKH_HEADER item);
    }
    public void addData(RKH_HEADER rkh_header){
        boolean found = false;

        for(int i = 0; i < mValues.size(); i++){
            if(rkh_header.getRKH_ID().equalsIgnoreCase(mValues.get(i).getRKH_ID())){
                found = true;
                break;
            }
        }

        if(!found){
            mValues.add(rkh_header);
            notifyDataSetChanged();
        }
    }
}