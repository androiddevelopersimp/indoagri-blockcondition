package co.indoagri.blockcondition.handler;


import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

import co.indoagri.blockcondition.model.Apps;

public class JsonHandler {
    private String json;

    public JsonHandler(String json){
        this.json = json;
    }

    public Apps getApps(){
        Apps apps = null;

        try {
            JSONObject jsonObject = new JSONObject(json);

            String version = jsonObject.has(Apps.TAG_VERSION) ? jsonObject.getString(Apps.TAG_VERSION) : "";
            String filename = jsonObject.has(Apps.TAG_FILENAME) ? jsonObject.getString(Apps.TAG_FILENAME) : "";

            if(!TextUtils.isEmpty(version) && !TextUtils.isEmpty(filename)){
                apps = new Apps(version, filename);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return apps;
    }
}
