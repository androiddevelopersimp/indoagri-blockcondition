package co.indoagri.blockcondition.handler;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.media.MediaScannerConnection;

import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.input.BOMInputStream;

import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.model.Data.MasterDownload;
import co.indoagri.blockcondition.model.FileXML;
import co.indoagri.blockcondition.model.MessageStatus;
public class ParsingHandler {
    private Context context;

    public ParsingHandler(Context context){
        this.context = context;
    }

    private String convertStreamToString(File file){
        String xml = "";
        StringBuilder textFile = new StringBuilder();

        try{
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while((line = br.readLine()) != null){
                textFile.append(line);
            }

            br.close();
            xml = textFile.toString();
        }catch (IOException e){
            e.printStackTrace();
        }

        return xml;
    }

    public List<MessageStatus> ParsingXML(List<FileXML> lstFile){
        List<MessageStatus> lstMsgStatus = new ArrayList<MessageStatus>();
        MessageStatus msgStatus = new MessageStatus();

        for(int i = 0; i < lstFile.size(); i++){
            File file = new File(lstFile.get(i).getFileNew());

            String xml = convertStreamToString(file);

            try {
                InputStream is = new BOMInputStream(new ByteArrayInputStream(xml.getBytes("UTF-8")),false, ByteOrderMark.UTF_8 );
                msgStatus = new XmlHandler(context).parsingXML(is);

                if(msgStatus != null){
                    int status = msgStatus.isStatus() ? 1:-1;

                    if(msgStatus.isStatus()){
                        if(file.exists()){
                            File to = new File(lstFile.get(i).getPathBackup() + File.separator +  file.getName());
                            file.renameTo(to);
                            MediaScannerConnection.scanFile(context, new String[] { file.getAbsolutePath() }, null, null);
                            MediaScannerConnection.scanFile(context, new String[] { to.getAbsolutePath() }, null, null);
                        }
                    }

                    DatabaseHandler database = new DatabaseHandler(context);

                    try{
                        database.openTransaction();

                        MasterDownload md = (MasterDownload) database.getDataFirst(false, MasterDownload.TABLE_NAME, null,
                                MasterDownload.XML_NAME + "=?",
                                new String [] {msgStatus.getMenu()},
                                null, null, null, null);

                        if(md != null){
                            database.updateData(new MasterDownload(0, msgStatus.getMenu(), file.getName(), new Date().getTime(), status),
                                    MasterDownload.XML_NAME + "=?",
                                    new String [] {msgStatus.getMenu()});
                        }else{
                            database.setData(new MasterDownload(0, msgStatus.getMenu(), file.getName(), new Date().getTime(), status));
                        }

                        database.commitTransaction();
                    }catch(SQLiteException e){
                        e.printStackTrace();
                        database.closeTransaction();
                    }finally{
                        database.closeTransaction();
                    }
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                msgStatus.setMessage(e.getMessage());
            }

            lstMsgStatus.add(msgStatus);
        }

        return lstMsgStatus;
    }


}
