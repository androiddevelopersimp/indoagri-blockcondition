package co.indoagri.blockcondition.handler;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import static co.indoagri.blockcondition.utils.NetworkUtils.isConnected;
import static co.indoagri.blockcondition.utils.NetworkUtils.isConnectedFast;
import static co.indoagri.blockcondition.utils.NetworkUtils.isConnectedMobile;
import static co.indoagri.blockcondition.utils.NetworkUtils.isConnectedWifi;


public class BroadcastConnectorHandler extends BroadcastReceiver
{
    public static ConnectivityReceiverListener connectivityReceiverListener;

    public BroadcastConnectorHandler() {
        super();
    }
    @Override
    public void onReceive(Context context, Intent intent)
    {
        try
        {
            if (connectivityReceiverListener != null) {
                connectivityReceiverListener.onNetworkConnectionChanged(isConnected(context));
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    public static boolean isConnect(Context context) {
        boolean result = false;
        if(isConnected(context)){
            result = true;
        }
        if(isConnectedFast(context)){
            result = true;
        }
        if(isConnectedMobile(context)){
            result = true;
        }
        if(isConnectedWifi(context)){
            result = true;
        }
        return  result;
    }
    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }

}