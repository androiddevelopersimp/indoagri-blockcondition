package co.indoagri.blockcondition.handler;


import java.io.File;

import android.app.backup.FileBackupHelper;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.Environment;

import co.indoagri.blockcondition.routines.Constants;

public class FolderHandler {
    private Context context;
    private File fileRoot;
    private String fileMasterNew;
    private String fileMasterBackup;
    //	private String fileReportTaksasi;
//	private String fileReportBPN;
//	private String fileReportSPBS;
//	private String fileReportBKM;
    private String fileReportNew;
    private String fileReportBackup;
    private String fileRestoreNew;
    private String fileRestoreBackup;
    private String fileDatabase;
    private String fileDatabaseExport;
    private String fileDatabaseImport;
    private String filePhoto;

    public static final String ROOT = Constants.folder_root;

    public static final String MASTER = "MASTER";
    public static final String REPORT = "REPORT";
    public static final String RESTORE = "RESTORE";
    public static final String DATABASE = "DATABASE";
    public static final String PHOTO = "PHOTO";
    public static final String EXPORT = "EXPORT";
    public static final String IMPORT = "IMPORT";

    public static final String NEW = "NEW";
    public static final String BACKUP = "BACKUP";

    public FolderHandler(Context context){
        this.context = context;
    }

    public String getFileMasterNew(){
        return fileMasterNew;
    }

    public String getFileMasterBackup(){
        return fileMasterBackup;
    }

//	public String getfileReportBPN(){
//		return fileReportBPN;
//	}
//
//	public String getFileReportBKM(){
//		return fileReportBKM;
//	}
//
//	public String getFileReportSPBS(){
//		return fileReportSPBS;
//	}
//
//	public String getFileReportTaksasi(){
//		return fileReportTaksasi;
//	}

    public String getFileReportNew(){
        return fileReportNew;
    }

    public String getFileReportBackup(){
        return fileReportBackup;
    }

    public String getFileRestoreNew() {
        return fileRestoreNew;
    }

    public String getFileRestoreBackup() {
        return fileRestoreBackup;
    }

    public String getFileDatabase() {
        return fileDatabase;
    }

    public String getFileDatabaseExport() {
        return fileDatabaseExport;
    }

    public String getFileDatabaseImport(){
        return fileDatabaseImport;
    }

    public String getFilePhoto(){
        return filePhoto;
    }

    public boolean init(){
        if(createRoot(ROOT)){
            createFolder(new File(fileRoot.getPath() + File.separator + MASTER));
            fileMasterNew = createFolder(new File(fileRoot.getPath() + File.separator + MASTER + File.separator + NEW));
            fileMasterBackup = createFolder(new File(fileRoot.getPath() + File.separator + MASTER + File.separator + BACKUP));

            createFolder(new File(fileRoot.getPath() + File.separator + REPORT));
            fileReportNew = createFolder(new File(fileRoot.getPath() + File.separator + REPORT + File.separator + NEW));
            fileReportBackup = createFolder(new File(fileRoot.getPath() + File.separator + REPORT + File.separator + BACKUP));

//			fileReportTaksasi = createFolder(new File(fileRoot.getPath() + File.separator + REPORT + File.separator + TAKSASI));
//			fileReportBPN = createFolder(new File(fileRoot.getPath() + File.separator + REPORT + File.separator + BPN));
//			fileReportSPBS = createFolder(new File(fileRoot.getPath() + File.separator + REPORT + File.separator + SPBS));
//			fileReportBKM = createFolder(new File(fileRoot.getPath() + File.separator + REPORT + File.separator + BKM));

            createFolder(new File(fileRoot.getPath() + File.separator + RESTORE));
            fileRestoreNew = createFolder(new File(fileRoot.getPath() + File.separator + RESTORE + File.separator + NEW));
            fileRestoreBackup = createFolder(new File(fileRoot.getPath() + File.separator + RESTORE + File.separator + BACKUP));

            fileDatabase = createFolder(new File(fileRoot.getPath() + File.separator + DATABASE));
            fileDatabaseExport = createFolder(new File(fileRoot.getPath() + File.separator + DATABASE + File.separator + EXPORT));
            fileDatabaseImport = createFolder(new File(fileRoot.getPath() + File.separator + DATABASE + File.separator + IMPORT));

            filePhoto = createFolder(new File(fileRoot.getPath() + File.separator + PHOTO));

            return true;
        }else{
            return false;
        }
    }

    public boolean isSDCardWritable() {
        String status = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(status)) {
            return true;
        }else{
            return false;
        }
    }

    private boolean createRoot(String root){
        if(isSDCardWritable()){
            fileRoot = new File(Environment.getExternalStorageDirectory(), root);

            if(!fileRoot.exists()){

                boolean status = fileRoot.mkdirs();

              //  MediaScannerConnection.scanFile(context, new String[] { fileRoot.getAbsolutePath() }, null, null);

                return status;
            }else{
                return true;
            }
        } if(!isSDCardWritable()){
            fileRoot = new File(Environment.getExternalStorageDirectory(), root);

            if(!fileRoot.exists()){

                boolean status = fileRoot.mkdirs();

                //  MediaScannerConnection.scanFile(context, new String[] { fileRoot.getAbsolutePath() }, null, null);

                return status;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }

    private String createFolder(File fileFolder){
        boolean isSuccess = false;

        if(isSDCardWritable()){
            if(!fileFolder.exists()){
                isSuccess = fileFolder.mkdirs();

                //MediaScannerConnection.scanFile(context, new String[] { fileFolder.getAbsolutePath() }, null, null);
            }else{
                isSuccess = true;
            }
            if(!isSDCardWritable()){
                if(!fileFolder.exists()){
                    isSuccess = fileFolder.mkdirs();

                   // MediaScannerConnection.scanFile(context, new String[] { fileFolder.getAbsolutePath() }, null, null);
                }else{
                    isSuccess = true;
                }
            }
        }else{
            isSuccess = false;
        }

        if(isSuccess){
            return fileFolder.getPath();
        }else{
            return null;
        }
    }
}
