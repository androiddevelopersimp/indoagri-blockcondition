package co.indoagri.blockcondition.handler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;


import java.util.Calendar;
import java.util.TimeZone;

import co.indoagri.blockcondition.services.GPSTriggerService;

public class GpsHandler {
	
	private Context context;
	private PendingIntent pendingIntent;
	private AlarmManager alarmManager;
	
	public GpsHandler(Context context){
		this.context = context;
	}
	
	public void startGPS() {    
		try{
			Calendar updateTime = Calendar.getInstance();
			updateTime.setTimeZone(TimeZone.getDefault());
        
			Intent intent = new Intent(context, GPSTriggerService.class);
		  
			pendingIntent = PendingIntent.getService(context, 1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.setRepeating(AlarmManager.RTC, updateTime.getTimeInMillis(), 2000, pendingIntent);
        
			Toast.makeText(context, "GPS Service: Start", Toast.LENGTH_SHORT).show();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void stopGPS(){
		try{
			Intent intent = new Intent(context, GPSTriggerService.class);
				
			context.stopService(intent);
				
			pendingIntent = PendingIntent.getService(context, 1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(pendingIntent);
				
			Toast.makeText(context, "GPS Service: Stop", Toast.LENGTH_SHORT).show();
   		}catch (Exception e){
   			e.printStackTrace();
   		}
	}
}
