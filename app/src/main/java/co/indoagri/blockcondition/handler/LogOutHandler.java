package co.indoagri.blockcondition.handler;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.IOException;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.activity.ExitActivity;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.model.Users.UserLogin;
import co.indoagri.blockcondition.widget.ToastMessage;

public class LogOutHandler {
    LogOutListener logOutListener;
    Context context;
    Activity activity;
    boolean onUserInteraction = false;
    ToastMessage toastMessage;
    boolean statusLogout= false;
    BackupTask backupTask;
    DatabaseHandler database;
    public LogOutHandler(Context context,Activity activity){
        toastMessage = new ToastMessage(context);
        this.context = context;
        this.activity = activity;
        database = new DatabaseHandler(context);
    }
    public interface LogOutListener {
        void onLogoutInterface(String string, boolean status);
    }

    public void setOnEventListener(LogOutListener listener) {
        logOutListener = listener;
    }

    public void Keluar() {
        backupTask = new BackupTask();
        backupTask.execute();
    }
    private class BackupTask extends AsyncTask<Void, Void, Boolean> {

        DialogProgress dialogProgress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialogProgress = new DialogProgress(activity, activity.getResources().getString(R.string.wait));
            dialogProgress.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                return copyAppDbToDownloadFolder();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
            }

            if(result){
                toastMessage.shortMessage(activity.getResources().getString(R.string.backup_successed));
            }else{
                toastMessage.shortMessage(activity.getResources().getString(R.string.backup_failed));
            }

            try{
                database.openTransaction();
                database.deleteData(UserLogin.TABLE_NAME, null, null);
                database.commitTransaction();
                database.closeTransaction();
                statusLogout = true;
                if (statusLogout) {
                    if (logOutListener != null)
                        logOutListener.onLogoutInterface(null,true);
                }else{
                    if (logOutListener != null)
                        logOutListener.onLogoutInterface(null,false);
                }
            }catch(SQLiteException e){
                e.printStackTrace();
                database.closeTransaction();
            }
        }
    }

    private boolean copyAppDbToDownloadFolder() throws IOException {
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
//
        if(mExternalStorageAvailable && mExternalStorageWriteable){
            boolean success = false;

            success = new FileEncryptionHandler(activity).encrypt(FileEncryptionHandler.STORAGE_INTERNAL);
            success &= new FileEncryptionHandler(activity).encrypt(FileEncryptionHandler.STORAGE_EXTERNAL);

            return success;
        }

        return false;
    }

}
