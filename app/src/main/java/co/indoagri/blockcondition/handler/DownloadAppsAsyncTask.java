package co.indoagri.blockcondition.handler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.routines.Constants;

public class DownloadAppsAsyncTask extends AsyncTask<Void, Void, Void>{

    private Context context;
    private String url;

    private DialogProgress dlgProgress;

    public DownloadAppsAsyncTask(Context context, String url){
        this.context = context;
        this.url = url;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        dlgProgress = new DialogProgress(context, context.getResources().getString(R.string.wait));
        dlgProgress.show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        String filename;
        File file_dir, file_output;
        FileOutputStream file_output_stream;
        InputStream input_stream;
        byte[] buffer = new byte[1024];
        int length = 0;
        Intent intent;

        try{
            URL apk_url = new URL(url);
            HttpURLConnection http = (HttpURLConnection) apk_url.openConnection();
            http.setRequestMethod("GET");

            if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.GINGERBREAD) {
                http.setDoOutput(true);
            }

            http.connect();

            File sd = Environment.getExternalStorageDirectory();
            String backupDBPath = FolderHandler.ROOT;
            file_dir = new File(sd, backupDBPath);

            filename = Constants.apk_name;

            if(!file_dir.exists()){
                file_dir.mkdirs();
            }

            file_output = new File(file_dir, filename);

            if(!file_output.exists()){
                try {
                    file_output.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                file_output.delete();
            }

            file_output_stream = new FileOutputStream(file_output);
            input_stream = http.getInputStream();

            while((length = input_stream.read(buffer)) != -1){
                file_output_stream.write(buffer, 0, length);
            }

            file_output_stream.close();
            input_stream.close();

            intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(new File(sd, backupDBPath + "/" + filename)), "application/vnd.android.package-archive");
            context.startActivity(intent);

        }catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

        dlgProgress.dismiss();
    }
}