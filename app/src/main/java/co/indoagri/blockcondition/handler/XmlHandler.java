package co.indoagri.blockcondition.handler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;


import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.text.TextUtils;
import android.util.Xml;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.model.Converter;
import co.indoagri.blockcondition.model.Data.AncakPanenHeader;
import co.indoagri.blockcondition.model.Data.AncakPanenQuality;
import co.indoagri.blockcondition.model.Data.BJR;
import co.indoagri.blockcondition.model.Data.BKMHeader;
import co.indoagri.blockcondition.model.Data.BKMLine;
import co.indoagri.blockcondition.model.Data.BKMOutput;
import co.indoagri.blockcondition.model.Data.BLKPLT;
import co.indoagri.blockcondition.model.Data.BLKSBC;
import co.indoagri.blockcondition.model.Data.BLKSBCDetail;
import co.indoagri.blockcondition.model.Data.BLKSUGC;
import co.indoagri.blockcondition.model.Data.BPNHeader;
import co.indoagri.blockcondition.model.Data.BPNQuality;
import co.indoagri.blockcondition.model.Data.BPNQuantity;
import co.indoagri.blockcondition.model.Data.BlockHdrc;
import co.indoagri.blockcondition.model.Data.DayOff;
import co.indoagri.blockcondition.model.Data.Penalty;
import co.indoagri.blockcondition.model.Data.RunningAccount;
import co.indoagri.blockcondition.model.Data.SKB;
import co.indoagri.blockcondition.model.Data.SPBSDestination;
import co.indoagri.blockcondition.model.Data.SPBSHeader;
import co.indoagri.blockcondition.model.Data.SPBSLine;
import co.indoagri.blockcondition.model.Data.SPBSRunningNumber;
import co.indoagri.blockcondition.model.Data.SPTARunningNumber;
import co.indoagri.blockcondition.model.Data.TaksasiHeader;
import co.indoagri.blockcondition.model.Data.TaksasiLine;
import co.indoagri.blockcondition.model.Data.Vendor;
import co.indoagri.blockcondition.model.Data.tblM_AccountingPeriod;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.MessageStatus;
import co.indoagri.blockcondition.model.RKH.RKH_ACTIVITY_TYPE;
import co.indoagri.blockcondition.model.RKH.RKH_HEADER;
import co.indoagri.blockcondition.model.RKH.RKH_ITEM;
import co.indoagri.blockcondition.model.Users.AbsentType;
import co.indoagri.blockcondition.model.Users.DivisionAssistant;
import co.indoagri.blockcondition.model.Users.Employee;
import co.indoagri.blockcondition.model.Users.GroupHead;
import co.indoagri.blockcondition.model.Users.UserApp;
import co.indoagri.blockcondition.model.Users.UserLogin;

public class XmlHandler {
    private Context context;
    private DatabaseHandler database;

    public XmlHandler(Context context){
        this.context = context;
        this.database = new DatabaseHandler(context);
    }

    public MessageStatus parsingXML(InputStream inputStream){
        MessageStatus msgStatus = null;
        String tableName = "";
        XmlPullParserFactory factory = null;
        XmlPullParser parser = null;

        try{
            SKB skb = null;
            BlockHdrc blockHdrc = null;
            BLKSBC blksbc = null;
            BLKSBCDetail blksbcDetail = null;
            BJR bjr = null;
            BLKPLT blkplt = null;
            Employee employee = null;
            UserApp userApp = null;
            AbsentType absentType = null;
            DayOff dayOff = null;
            DivisionAssistant assDiv = null;
            RunningAccount runAccount = null;
            SPBSRunningNumber spbsRunNo = null;
            Penalty penalty = null;
            SPBSDestination spbsDestination = null;
            SPTARunningNumber sptaRunNo = null;
            Vendor vendor = null;
            BLKSUGC blksugc = null;
            GroupHead groupHead = null;
            tblM_AccountingPeriod tblM_accountingPeriod = null;
            RKH_ACTIVITY_TYPE rkh_activity_type = null;

            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            parser = factory.newPullParser();

            parser.setInput(inputStream, null);

            int eventType = parser.getEventType();
            String textValue = null;

            database.openTransaction();

            while(eventType != XmlPullParser.END_DOCUMENT){
                String tagName = parser.getName();

                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if(tagName.equalsIgnoreCase(BJR.XML_DOCUMENT)){
                            tableName = BJR.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(BLKPLT.XML_DOCUMENT)){
                            tableName = BLKPLT.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(BLKSBC.XML_DOCUMENT)){
                            tableName = BLKSBC.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(BLKSBCDetail.XML_DOCUMENT)){
                            tableName = BLKSBCDetail.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(BlockHdrc.XML_DOCUMENT)){
                            tableName = BlockHdrc.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(Employee.XML_DOCUMENT)){
                            tableName = Employee.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(SKB.XML_DOCUMENT)){
                            tableName = SKB.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(UserApp.XML_DOCUMENT)){
                            tableName = UserApp.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(AbsentType.XML_DOCUMENT)){
                            tableName = AbsentType.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(DayOff.XML_DOCUMENT)){
                            tableName = DayOff.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(DivisionAssistant.XML_DOCUMENT)){
                            tableName = DivisionAssistant.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(RunningAccount.XML_DOCUMENT)){
                            tableName = RunningAccount.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(SPBSRunningNumber.XML_DOCUMENT)){
                            tableName = SPBSRunningNumber.TABLE_NAME;
//						database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(Penalty.XML_DOCUMENT)){
                            tableName = Penalty.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(SPBSDestination.XML_DOCUMENT)){
                            tableName = SPBSDestination.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(SPTARunningNumber.XML_DOCUMENT)){
                            tableName = SPTARunningNumber.TABLE_NAME;
                        }else if(tagName.equalsIgnoreCase(Vendor.XML_DOCUMENT)){
                            tableName = Vendor.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(BLKSUGC.XML_DOCUMENT)){
                            tableName = BLKSUGC.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(GroupHead.XML_DOCUMENT)){
                            tableName = GroupHead.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(tblM_AccountingPeriod.XML_DOCUMENT)){
                            tableName = tblM_AccountingPeriod.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }else if(tagName.equalsIgnoreCase(RKH_ACTIVITY_TYPE.XML_DOCUMENT)){
                            tableName = rkh_activity_type.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }

                        if(tableName.equalsIgnoreCase(SKB.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(SKB.XML_ITEM)){
                                skb = new SKB();
                            }
                        }else if(tableName.equalsIgnoreCase(BlockHdrc.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BlockHdrc.XML_ITEM)){
                                blockHdrc = new BlockHdrc();
                            }
                        }else if(tableName.equalsIgnoreCase(BLKSBC.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BLKSBC.XML_ITEM)){
                                blksbc = new BLKSBC();
                            }
                        }else if(tableName.equalsIgnoreCase(BLKSBCDetail.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BLKSBCDetail.XML_ITEM)){
                                blksbcDetail = new BLKSBCDetail();
                            }
                        }else if(tableName.equalsIgnoreCase(BJR.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BJR.XML_ITEM)){
                                bjr = new BJR();
                            }
                        }else if(tableName.equalsIgnoreCase(BLKPLT.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BLKPLT.XML_ITEM)){
                                blkplt = new BLKPLT();
                            }
                        }else if(tableName.equalsIgnoreCase(Employee.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(Employee.XML_ITEM)){
                                employee = new Employee();
                            }
                        }else if(tableName.equalsIgnoreCase(UserApp.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(UserApp.XML_ITEM)){
                                userApp = new UserApp();
                            }
                        }else if(tableName.equalsIgnoreCase(AbsentType.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(AbsentType.XML_ITEM)){
                                absentType = new AbsentType();
                            }
                        }else if(tableName.equalsIgnoreCase(DayOff.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(DayOff.XML_ITEM)){
                                dayOff = new DayOff();
                            }
                        }else if(tableName.equalsIgnoreCase(DivisionAssistant.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(DivisionAssistant.XML_ITEM)){
                                assDiv = new DivisionAssistant();
                            }
                        }else if(tableName.equalsIgnoreCase(RunningAccount.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(RunningAccount.XML_ITEM)){
                                runAccount = new RunningAccount();
                            }
                        }else if(tableName.equalsIgnoreCase(SPBSRunningNumber.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(SPBSRunningNumber.XML_ITEM)){
                                spbsRunNo = new SPBSRunningNumber();
                            }
                        }else if(tableName.equalsIgnoreCase(Penalty.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(Penalty.XML_ITEM)){
                                penalty = new Penalty();
                            }
                        }else if(tableName.equalsIgnoreCase(SPBSDestination.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(SPBSDestination.XML_ITEM)){
                                spbsDestination = new SPBSDestination();
                            }
                        }else if(tableName.equalsIgnoreCase(SPTARunningNumber.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(SPTARunningNumber.XML_ITEM)){
                                sptaRunNo = new SPTARunningNumber();
                            }
                        }else if(tableName.equalsIgnoreCase(Vendor.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(Vendor.XML_ITEM)){
                                vendor = new Vendor();
                            }
                        }else if(tableName.equalsIgnoreCase(BLKSUGC.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BLKSUGC.XML_ITEM)){
                                blksugc = new BLKSUGC();
                            }
                        }else if(tableName.equalsIgnoreCase(GroupHead.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(GroupHead.XML_ITEM)){
                                groupHead = new GroupHead();
                            }
                        }else if(tableName.equalsIgnoreCase(tblM_AccountingPeriod.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(tblM_AccountingPeriod.XML_ITEM)){
                                tblM_accountingPeriod = new tblM_AccountingPeriod();
                            }
                        }else if(tableName.equalsIgnoreCase(rkh_activity_type.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(rkh_activity_type.XML_ITEM)){
                                rkh_activity_type = new RKH_ACTIVITY_TYPE();
                            }
                        }

                        break;
                    case XmlPullParser.TEXT:
                        textValue = parser.getText().trim();
                        break;
                    case XmlPullParser.END_TAG:
                        if(tableName.equalsIgnoreCase(SKB.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(SKB.XML_ITEM)){
                                database.setData(skb);
                            }else if (tagName.equalsIgnoreCase(SKB.XML_COMPANY_CODE)){
                                skb.setCompanyCode(textValue);
                            }else if(tagName.equalsIgnoreCase(SKB.XML_ESTATE)){
                                skb.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(SKB.XML_BLOCK)){
                                skb.setBlock(textValue);
                            }else if(tagName.equalsIgnoreCase(SKB.XML_BARIS_SKB)){
                                skb.setBarisSkb(new Converter(textValue).StrToInt());
                            }else if(tagName.equalsIgnoreCase(SKB.XML_VALID_FROM)){
                                skb.setValidFrom(textValue);
                            }else if(tagName.equalsIgnoreCase(SKB.XML_VALID_TO)){
                                skb.setValidTo(textValue);
                            }else if(tagName.equalsIgnoreCase(SKB.XML_BARIS_BLOCK)){
                                skb.setBarisBlok(new Converter(textValue).StrToInt());
                            }else if(tagName.equalsIgnoreCase(SKB.XML_JUMLAH_POKOK)){
                                skb.setJumlahPokok(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(SKB.XML_POKOK_MATI)){
                                skb.setPokokMati(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(SKB.XML_TANGGAL_TANAM)){
                                skb.setTanggalTanam(textValue);
                            }else if(tagName.equalsIgnoreCase(SKB.XML_LINE_SKB)){
                                skb.setLineSkb(new Converter(textValue).StrToInt());
                            }
                        }else if(tableName.equalsIgnoreCase(BlockHdrc.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BlockHdrc.XML_ITEM)){
                                database.setData(blockHdrc);
                            }else if (tagName.equalsIgnoreCase(BlockHdrc.XML_COMPANY_CODE)){
                                blockHdrc.setCompanyCode(textValue);
                            }else if(tagName.equalsIgnoreCase(BlockHdrc.XML_ESTATE)){
                                blockHdrc.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(BlockHdrc.XML_BLOCK)){
                                blockHdrc.setBlock(textValue);
                            }else if(tagName.equalsIgnoreCase(BlockHdrc.XML_VALID_FROM)){
                                blockHdrc.setValidFrom(textValue);
                            }else if(tagName.equalsIgnoreCase(BlockHdrc.XML_VALID_TO)){
                                blockHdrc.setValidTo(textValue);
                            }else if(tagName.equalsIgnoreCase(BlockHdrc.XML_DIVISION)){
                                blockHdrc.setDivisi(textValue);
                            }else if(tagName.equalsIgnoreCase(BlockHdrc.XML_TYPE)){
                                blockHdrc.setType(textValue);
                            }else if(tagName.equalsIgnoreCase(BlockHdrc.XML_STATUS)){
                                blockHdrc.setStatus(textValue);
                            }else if(tagName.equalsIgnoreCase(BlockHdrc.XML_PROJECT_DEFINITION)){
                                blockHdrc.setProjectDefinition(textValue);
                            }else if(tagName.equalsIgnoreCase(BlockHdrc.XML_OWNER)){
                                blockHdrc.setOwner(textValue);
                            }else if(tagName.equalsIgnoreCase(BlockHdrc.XML_TGL_TANAM)){
                                blockHdrc.setTglTanam(textValue);
                            }else if(tagName.equalsIgnoreCase(BlockHdrc.XML_MANDT)){
                                blockHdrc.setMandt(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(BLKSBC.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BLKSBC.XML_ITEM)){
                                database.setData(blksbc);
                            }else if (tagName.equalsIgnoreCase(BLKSBC.XML_COMPANY_CODE)){
                                blksbc.setCompanyCode(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKSBC.XML_ESTATE)){
                                blksbc.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKSBC.XML_BLOCK)){
                                blksbc.setBlock(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKSBC.XML_VALID_FROM)){
                                blksbc.setValidFrom(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKSBC.XML_BASIS_NORMAL)){
                                blksbc.setBasisNormal(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(BLKSBC.XML_BASIS_FRIDAY)){
                                blksbc.setBasisFriday(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(BLKSBC.XML_BASIS_HOLIDAY)){
                                blksbc.setBasisHoliday(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(BLKSBC.XML_PREMI_NORMAL)){
                                blksbc.setPremiNormal(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(BLKSBC.XML_PREMI_FRIDAY)){
                                blksbc.setPremiFriday(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(BLKSBC.XML_PREMI_HOLIDAY)){
                                blksbc.setPremiHoliday(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(BLKSBC.XML_PRIORITY)){
                                blksbc.setPriority(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(BLKSBCDetail.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BLKSBCDetail.XML_ITEM)){
                                database.setData(blksbcDetail);
                            }else if (tagName.equalsIgnoreCase(BLKSBCDetail.XML_COMPANY_CODE)){
                                blksbcDetail.setCompanyCode(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKSBCDetail.XML_ESTATE)){
                                blksbcDetail.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKSBCDetail.XML_BLOCK)){
                                blksbcDetail.setBlock(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKSBCDetail.XML_VALID_FROM)){
                                blksbcDetail.setValidFrom(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKSBCDetail.XML_MIN_VAL)){
                                blksbcDetail.setMinimumValue(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(BLKSBCDetail.XML_MAX_VAL)){
                                blksbcDetail.setMaximumValue(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(BLKSBCDetail.XML_OVER_BASIC_RATE)){
                                blksbcDetail.setOverBasicRate(new Converter(textValue).StrToDouble());
                            }
                        }else if(tableName.equalsIgnoreCase(BJR.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BJR.XML_ITEM)){
                                database.setData(bjr);
                            }else if (tagName.equalsIgnoreCase(BJR.XML_COMPANY_CODE)){
                                bjr.setCompanyCode(textValue);
                            }else if(tagName.equalsIgnoreCase(BJR.XML_ESTATE)){
                                bjr.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(BJR.XML_BLOCK)){
                                bjr.setBlock(textValue);
                            }else if(tagName.equalsIgnoreCase(BJR.XMl_EFF_DATE)){
                                bjr.setEffDate(textValue);
                            }else if(tagName.equalsIgnoreCase(BJR.XML_BJR)){
                                bjr.setBjr(new Converter(textValue).StrToDouble());
                            }
                        }else if(tableName.equalsIgnoreCase(BLKPLT.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BLKPLT.XML_ITEM)){
                                database.setData(blkplt);
                            }else if (tagName.equalsIgnoreCase(BLKPLT.XML_COMPANY_CODE)){
                                blkplt.setCompanyCode(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_ESTATE)){
                                blkplt.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_BLOCK)){
                                blkplt.setBlock(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_VALID_FROM)){
                                blkplt.setValidFrom(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_VALID_TO)){
                                blkplt.setValidTo(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_CROP_TYPE)){
                                blkplt.setCropType(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_PREVIOUS_CROP)){
                                blkplt.setPreviousCrop(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_FINISH_DATE)){
                                blkplt.setFinishDate(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_REFERENCE)){
                                blkplt.setReference(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_JARAK_TANAM)){
                                blkplt.setJarakTanam(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_HARVESTING_DATE)){
                                blkplt.setHarvestingDate(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_HARVESTED)){
                                blkplt.setHarvested(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_PLAN_DATE)){
                                blkplt.setPlanDate(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_TOPOGRAPHY)){
                                blkplt.setTopography(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_SOIL_TYPE)){
                                blkplt.setSoilType(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_SOIL_CATEGORY)){
                                blkplt.setSoilCategory(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKPLT.XML_PROD_TREES)){
                                blkplt.setProdTrees(new Converter(textValue).StrToDouble());
                            }
                        }else if(tableName.equalsIgnoreCase(Employee.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(Employee.XML_ITEM)){
                                database.setData(employee);
                            }else if (tagName.equalsIgnoreCase(Employee.XML_COMPANY_CODE)){
                                employee.setCompanyCode(textValue);
                            }else if(tagName.equalsIgnoreCase(Employee.XML_ESTATE)){
                                employee.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(Employee.XML_FISCAL_YEAR)){
                                employee.setFiscalYear(new Converter(textValue).StrToInt());
                            }else if(tagName.equalsIgnoreCase(Employee.XML_FISCAL_PERIOD)){
                                employee.setFiscalPeriod(new Converter(textValue).StrToInt());
                            }else if(tagName.equalsIgnoreCase(Employee.XML_NIK)){
                                employee.setNik(textValue);
                            }else if(tagName.equalsIgnoreCase(Employee.XML_NAME)){
                                employee.setName(textValue.toUpperCase());
                            }else if(tagName.equalsIgnoreCase(Employee.XML_TERM_DATE)){
                                employee.setTermDate(textValue);
                            }else if(tagName.equalsIgnoreCase(Employee.XML_DIVISION)){
                                employee.setDivision(textValue);
                            }else if(tagName.equalsIgnoreCase(Employee.XML_ROLE_ID)){
                                employee.setRoleId(textValue.toUpperCase());
                            }else if(tagName.equalsIgnoreCase(Employee.XML_JOB_POS)){
                                employee.setJobPos(textValue.toUpperCase());
                            }else if(tagName.equalsIgnoreCase(Employee.XML_EMP_TYPE)){
                                employee.setEmpType(textValue.toUpperCase());
                            }else if(tagName.equalsIgnoreCase(Employee.XML_GANG)){
                                employee.setGang(textValue.toUpperCase());
                            }else if(tagName.equalsIgnoreCase(Employee.XML_COST_CENTER)){
                                employee.setCostCenter(textValue);
                            }else if(tagName.equalsIgnoreCase(Employee.XML_VALID_FROM)){
                                employee.setValidFrom(textValue);
                            }else if(tagName.equalsIgnoreCase(Employee.XML_HARVESTER_CODE)){
                                employee.setHarvesterCode(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(UserApp.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(UserApp.XML_ITEM)){
                                database.setData(userApp);
                            }else if (tagName.equalsIgnoreCase(UserApp.XML_NIK)){
                                userApp.setNik(textValue);
                            }else if(tagName.equalsIgnoreCase(UserApp.XML_USERNAME)){
                                userApp.setUsername(textValue);
                            }else if(tagName.equalsIgnoreCase(UserApp.XML_PASSWORD)){
                                String enc = textValue;
                                String dec = "";

                                try {
                                    dec = new CryptoHandler().decrypt(enc, CryptoHandler.KEY);
                                } catch (KeyException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (InvalidAlgorithmParameterException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (IllegalBlockSizeException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (BadPaddingException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (GeneralSecurityException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

//							if(TextUtils.isEmpty(dec)){
//								dec = "Indoagri2015";
//							}

                                userApp.setPassword(dec);
                            }else if(tagName.equalsIgnoreCase(UserApp.XML_VALID_TO)){
                                String validTo = textValue + "235959";
                                userApp.setValidTo(new DateLocal(validTo, DateLocal.FORMAT_ID).getDate().getTime());
                            }else if(tagName.equalsIgnoreCase(UserApp.XML_CREATED_DATE)){
                                String createdDate = textValue + "000000";
                                userApp.setCreatedDate(new DateLocal(createdDate, DateLocal.FORMAT_ID).getDate().getTime());
                            }else if(tagName.equalsIgnoreCase(UserApp.XML_CREATED_BY)){
                                userApp.setCreatedBy(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(AbsentType.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(AbsentType.XML_ITEM)){
                                database.setData(absentType);
                            }else if(tagName.equalsIgnoreCase(AbsentType.XML_COMPANY_CODE)){
                                absentType.setCompanyCode(textValue);
                            }else if(tagName.equalsIgnoreCase(AbsentType.XML_ABSENT_TYPE)){
                                absentType.setAbsentType(textValue);
                            }else if(tagName.equalsIgnoreCase(AbsentType.XML_DESCRIPTION)){
                                absentType.setDescription(textValue);
                            }else if(tagName.equalsIgnoreCase(AbsentType.XML_HKRLLO)){
                                absentType.setHkrllo(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(AbsentType.XML_HKRLHI)){
                                absentType.setHkrlhi(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(AbsentType.XML_HKPYLO)){
                                absentType.setHkpylo(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(AbsentType.XML_HKPYHI)){
                                absentType.setHkpyhi(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(AbsentType.XML_HKVLLO)){
                                absentType.setHkvllo(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(AbsentType.XML_HKVLHI)){
                                absentType.setHkvlhi(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(AbsentType.XML_HKMEIN)){
                                absentType.setHkmein(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(AbsentType.XML_AGROUP)){
                                absentType.setAgroup(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(DayOff.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(DayOff.XML_ITEM)){
                                database.setData(dayOff);
                            }else if(tagName.equalsIgnoreCase(DayOff.XML_ESTATE)){
                                dayOff.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(DayOff.XML_DATE)){
                                dayOff.setDate(textValue);
                            }else if(tagName.equalsIgnoreCase(DayOff.XML_DAY_OFF_TYPE)){
                                dayOff.setDayOffType(textValue);
                            }else if(tagName.endsWith(DayOff.XML_DESCRIPTION)){
                                dayOff.setDayOffType(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(DivisionAssistant.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(DivisionAssistant.XML_ITEM)){
                                database.setData(assDiv);
                            }else if(tagName.equalsIgnoreCase(DivisionAssistant.XML_ESTATE)){
                                assDiv.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(DivisionAssistant.XML_DIVISION)){
                                assDiv.setDivision(textValue);
                            }else if(tagName.equalsIgnoreCase(DivisionAssistant.XML_SPRAS)){
                                assDiv.setSpras(textValue);
                            }else if(tagName.equalsIgnoreCase(DivisionAssistant.XML_DESCRIPTION)){
                                assDiv.setDescription(textValue);
                            }else if(tagName.equalsIgnoreCase(DivisionAssistant.XML_ASSISTANT)){
                                assDiv.setAssistant(textValue);
                            }else if(tagName.equalsIgnoreCase(DivisionAssistant.XML_DISTANCE_TO_MILL)){
                                assDiv.setDistanceToMill(textValue);
                            }else if(tagName.equalsIgnoreCase(DivisionAssistant.XML_UOM)){
                                assDiv.setUom(textValue);
                            }else if(tagName.equalsIgnoreCase(DivisionAssistant.XML_ASSISTANT_NAME)){
                                assDiv.setAssistantName(textValue);
                            }else if(tagName.equalsIgnoreCase(DivisionAssistant.XML_LIFNR)){
                                assDiv.setLifnr(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(RunningAccount.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(RunningAccount.XML_ITEM)){
                                database.setData(runAccount);
                            }else if(tagName.equalsIgnoreCase(RunningAccount.XML_COMPANY_CODE)){
                                runAccount.setCompanyCode(textValue);
                            }else if(tagName.equalsIgnoreCase(RunningAccount.XML_ESTATE)){
                                runAccount.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(RunningAccount.XML_RUNNING_ACCOUNT)){
                                runAccount.setRunningAccount(textValue);
                            }else if(tagName.equalsIgnoreCase(RunningAccount.XML_LICENSE_PLATE)){
                                runAccount.setLicensePlate(textValue);
                            }else if(tagName.equalsIgnoreCase(RunningAccount.XML_LIFNR)){
                                runAccount.setLifnr(textValue);
                            }else if(tagName.equalsIgnoreCase(RunningAccount.XML_OWNERSHIPFLAG)){
                                runAccount.setOwnerShipFlag(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(SPBSRunningNumber.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(SPBSRunningNumber.XML_ITEM)){
                                SPBSRunningNumber temp = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null,
                                        SPBSRunningNumber.XML_ESTATE + "=?" + " and " +
                                                SPBSRunningNumber.XML_YEAR + "=?" + " and " +
                                                SPBSRunningNumber.XML_MONTH + "=?" + " and " +
                                                SPBSRunningNumber.XML_IMEI + "=?" + " and " +
                                                SPBSRunningNumber.XML_DEVICE_ALIAS + "=?",
                                        new String [] {spbsRunNo.getEstate(), spbsRunNo.getYear(),
                                                spbsRunNo.getMonth(), spbsRunNo.getImei(), String.valueOf(spbsRunNo.getDeviceAlias())},
                                        null, null, null, null);

                                if(temp != null){
                                    int lastId = temp.getId();
                                    int newId = new Converter(spbsRunNo.getRunningNumber()).StrToInt();

                                    if(lastId < newId){
                                        database.updateData(spbsRunNo,
                                                SPBSRunningNumber.XML_ESTATE + "=?" + " and " +
                                                        SPBSRunningNumber.XML_YEAR + "=?" + " and " +
                                                        SPBSRunningNumber.XML_MONTH + "=?" + " and " +
                                                        SPBSRunningNumber.XML_IMEI + "=?" + " and " +
                                                        SPBSRunningNumber.XML_DEVICE_ALIAS + "=?",
                                                new String [] {spbsRunNo.getEstate(), spbsRunNo.getYear(),
                                                        spbsRunNo.getMonth(), spbsRunNo.getImei(), String.valueOf(spbsRunNo.getDeviceAlias())});
                                    }
                                }else{
                                    database.setData(spbsRunNo);
                                }
                            }else if(tagName.equalsIgnoreCase(SPBSRunningNumber.XML_ID)){
                                spbsRunNo.setId(new Converter(textValue).StrToInt());
                            }else if(tagName.equalsIgnoreCase(SPBSRunningNumber.XML_ESTATE)){
                                spbsRunNo.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSRunningNumber.XML_DIVISION)){
                                spbsRunNo.setDivision(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSRunningNumber.XML_YEAR)){
                                spbsRunNo.setYear(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSRunningNumber.XML_MONTH)){
                                spbsRunNo.setMonth(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSRunningNumber.XML_IMEI)){
                                spbsRunNo.setImei(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSRunningNumber.XML_RUNNING_NUMBER)){
                                spbsRunNo.setRunningNumber(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSRunningNumber.XML_DEVICE_ALIAS)){
                                spbsRunNo.setDeviceAlias(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(Penalty.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(Penalty.XML_ITEM)){
                                database.setData(penalty);
                            }else if(tagName.equalsIgnoreCase(Penalty.XML_PENALTY_CODE)){
                                penalty.setPenaltyCode(textValue);
                            }else if(tagName.equalsIgnoreCase(Penalty.XML_PENALTY_DESC)){
                                penalty.setPenaltyDesc(textValue);
                            }else if(tagName.equalsIgnoreCase(Penalty.XML_UOM)){
                                penalty.setUom(textValue);
                            }else if(tagName.equalsIgnoreCase(Penalty.XMl_CROP_TYPE)){
                                penalty.setCropType(textValue);
                            }else if(tagName.equalsIgnoreCase(Penalty.XML_IS_LOADING)){
                                penalty.setIsLoading(new Converter(textValue).StrToInt());
                            }else if(tagName.equalsIgnoreCase(Penalty.XML_IS_ANCAK)){
                                penalty.setIsAncak(new Converter(textValue).StrToInt());
                            }
                        }else if(tableName.equalsIgnoreCase(SPBSDestination.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(SPBSDestination.XML_ITEM)){
                                database.setData(spbsDestination);
                            }else if(tagName.equalsIgnoreCase(SPBSDestination.XML_MANDT)){
                                spbsDestination.setMandt(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSDestination.XML_ESTATE)){
                                spbsDestination.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSDestination.XML_DEST_TYPE)){
                                spbsDestination.setDestType(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSDestination.XML_DEST_ID)){
                                spbsDestination.setDestId(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSDestination.XML_DEST_DESC)){
                                spbsDestination.setDestDesc(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSDestination.XML_ACTIVE)){
                                spbsDestination.setActive(new Converter(textValue).StrToInt());
                            }
                        }else if(tableName.equalsIgnoreCase(SPTARunningNumber.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(SPTARunningNumber.XML_ITEM)){
                                SPTARunningNumber temp = (SPTARunningNumber) database.getDataFirst(false, SPTARunningNumber.TABLE_NAME, null,
                                        SPTARunningNumber.XML_ESTATE + "=?" + " and " +
                                                SPTARunningNumber.XML_YEAR + "=?" + " and " +
                                                SPTARunningNumber.XML_MONTH + "=?" + " and " +
                                                SPTARunningNumber.XML_IMEI + "=?" + " and " +
                                                SPTARunningNumber.XML_DEVICE_ALIAS + "=?",
                                        new String [] {sptaRunNo.getEstate(), sptaRunNo.getYear(),
                                                sptaRunNo.getMonth(), sptaRunNo.getImei(), String.valueOf(sptaRunNo.getDeviceAlias())},
                                        null, null, null, null);

                                if(temp != null){
                                    int lastId = temp.getId();
                                    int newId = new Converter(sptaRunNo.getRunningNumber()).StrToInt();

                                    if(lastId < newId){
                                        database.updateData(sptaRunNo,
                                                SPTARunningNumber.XML_ESTATE + "=?" + " and " +
                                                        SPTARunningNumber.XML_YEAR + "=?" + " and " +
                                                        SPTARunningNumber.XML_MONTH + "=?" + " and " +
                                                        SPTARunningNumber.XML_IMEI + "=?" + " and " +
                                                        SPTARunningNumber.XML_DEVICE_ALIAS + "=?",
                                                new String [] {sptaRunNo.getEstate(), sptaRunNo.getYear(),
                                                        sptaRunNo.getMonth(), sptaRunNo.getImei(), String.valueOf(sptaRunNo.getDeviceAlias())});
                                    }
                                }else{
                                    database.setData(sptaRunNo);
                                }
                            }else if(tagName.equalsIgnoreCase(SPTARunningNumber.XML_ID)){
                                sptaRunNo.setId(new Converter(textValue).StrToInt());
                            }else if(tagName.equalsIgnoreCase(SPTARunningNumber.XML_ESTATE)){
                                sptaRunNo.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(SPTARunningNumber.XML_DIVISION)){
                                sptaRunNo.setDivision(textValue);
                            }else if(tagName.equalsIgnoreCase(SPTARunningNumber.XML_YEAR)){
                                sptaRunNo.setYear(textValue);
                            }else if(tagName.equalsIgnoreCase(SPTARunningNumber.XML_MONTH)){
                                sptaRunNo.setMonth(textValue);
                            }else if(tagName.equalsIgnoreCase(SPTARunningNumber.XML_IMEI)){
                                sptaRunNo.setImei(textValue);
                            }else if(tagName.equalsIgnoreCase(SPTARunningNumber.XML_RUNNING_NUMBER)){
                                sptaRunNo.setRunningNumber(textValue);
                            }else if(tagName.equalsIgnoreCase(SPTARunningNumber.XML_DEVICE_ALIAS)){
                                sptaRunNo.setDeviceAlias(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(Vendor.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(Vendor.XML_ITEM)){
                                database.setData(vendor);
                            }else if(tagName.equalsIgnoreCase(Vendor.XML_ESTATE)){
                                vendor.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(Vendor.XML_LIFNR)){
                                vendor.setLifnr(textValue);
                            }else if(tagName.equalsIgnoreCase(Vendor.XML_NAME)){
                                vendor.setName(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(BLKSUGC.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BLKSUGC.XML_ITEM)){
                                database.setData(blksugc);
                            }else if(tagName.equalsIgnoreCase(BLKSUGC.XML_COMPANY_CODE)){
                                blksugc.setCompanyCode(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKSUGC.XML_ESTATE)){
                                blksugc.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKSUGC.XML_BLOCK)){
                                blksugc.setBlock(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKSUGC.XML_VALID_FROM)){
                                blksugc.setValidFrom(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKSUGC.XML_VALID_TO)){
                                blksugc.setValidTo(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKSUGC.XML_PHASE)){
                                blksugc.setPhase(textValue);
                            }else if(tagName.equalsIgnoreCase(BLKSUGC.XML_DISTANCE)){
                                blksugc.setDistance(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(BLKSUGC.XML_SUB_DIVISION)){
                                blksugc.setSubDivision(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(GroupHead.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(GroupHead.XML_ITEM)){
                                database.setData(groupHead);
                            }else if(tagName.equalsIgnoreCase(GroupHead.XML_COMPANY_CODE)){
                                groupHead.setCompanyCode(textValue);
                            }else if(tagName.equalsIgnoreCase(GroupHead.XML_ESTATE)){
                                groupHead.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(GroupHead.XML_LIFNR)){
                                groupHead.setLifnr(textValue);
                            }else if(tagName.equalsIgnoreCase(GroupHead.XML_INITIAL)){
                                groupHead.setInitial(textValue);
                            }else if(tagName.equalsIgnoreCase(GroupHead.XML_VALID_FROM)){
                                groupHead.setValidFrom(textValue);
                            }else if(tagName.equalsIgnoreCase(GroupHead.XML_VALID_TO)){
                                groupHead.setValidTo(textValue);
                            }else if(tagName.equalsIgnoreCase(GroupHead.XML_NAME)){
                                groupHead.setName(textValue);
                            }else if(tagName.equalsIgnoreCase(GroupHead.XML_STATUS)){
                                groupHead.setStatus(new Converter(textValue).StrToInt());
                            }
                        }else if(tableName.equalsIgnoreCase(tblM_AccountingPeriod.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(tblM_AccountingPeriod.XML_ITEM)){
                                database.setData(tblM_accountingPeriod);
                            }else if(tagName.equalsIgnoreCase(tblM_AccountingPeriod.XML_Estate)){
                                tblM_accountingPeriod.setAcc_Estate(textValue);
                            }else if(tagName.equalsIgnoreCase(tblM_AccountingPeriod.XML_ZYear)){
                                tblM_accountingPeriod.setAcc_ZYear(textValue);
                            }else if(tagName.equalsIgnoreCase(tblM_AccountingPeriod.XML_Period)){
                                tblM_accountingPeriod.setAcc_Period(textValue);
                            }else if(tagName.equalsIgnoreCase(tblM_AccountingPeriod.XML_ClosingDate)){
                                tblM_accountingPeriod.setAcc_ClosingDate(new DateLocal(textValue, DateLocal.FORMAT_REPORT_VIEW3).getDateString(DateLocal.FORMAT_INPUT));
                            }else if(tagName.equalsIgnoreCase(tblM_AccountingPeriod.XML_Status)){
                                tblM_accountingPeriod.setAcc_Status(textValue);
                            }else if(tagName.equalsIgnoreCase(tblM_AccountingPeriod.XML_Active)){
                                tblM_accountingPeriod.setAcc_Active(textValue);
                            }else if(tagName.equalsIgnoreCase(tblM_AccountingPeriod.XML_CreatedBy)){
                                tblM_accountingPeriod.setAcc_CreatedBy(textValue);
                            }else if(tagName.equalsIgnoreCase(tblM_AccountingPeriod.XML_CreatedDate)){
                                tblM_accountingPeriod.setAcc_CreatedDate(textValue);
                            }else if(tagName.equalsIgnoreCase(tblM_AccountingPeriod.XML_ModifiedBy)){
                                tblM_accountingPeriod.setAcc_ModifiedBy(textValue);
                            }else if(tagName.equalsIgnoreCase(tblM_AccountingPeriod.XML_ModifiedDate)){
                                tblM_accountingPeriod.setAcc_ModifiedDate(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(rkh_activity_type.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(rkh_activity_type.XML_ITEM)){
                                database.setData(rkh_activity_type);
                            }else if(tagName.equalsIgnoreCase(rkh_activity_type.XML_ACTTYPE)){
                                rkh_activity_type.setACTTYPE(textValue);
                            }else if(tagName.equalsIgnoreCase(rkh_activity_type.XML_NAME)){
                                rkh_activity_type.setNAME(textValue);
                            }
                        }

                    default:
                        break;
                }
                eventType = parser.next();
            }

            if(tableName.equals(BJR.TABLE_NAME)){
                msgStatus = new MessageStatus(BJR.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(BLKPLT.TABLE_NAME)){
                msgStatus = new MessageStatus(BLKPLT.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(BLKSBC.TABLE_NAME)){
                msgStatus = new MessageStatus(BLKSBC.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(BLKSBCDetail.TABLE_NAME)){
                msgStatus = new MessageStatus(BLKSBCDetail.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(BlockHdrc.TABLE_NAME)){
                msgStatus = new MessageStatus(BlockHdrc.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(Employee.TABLE_NAME)){
                msgStatus = new MessageStatus(Employee.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(SKB.TABLE_NAME)){
                msgStatus = new MessageStatus(SKB.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(UserApp.TABLE_NAME)){
                msgStatus = new MessageStatus(UserApp.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(AbsentType.TABLE_NAME)){
                msgStatus = new MessageStatus(AbsentType.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(DayOff.TABLE_NAME)){
                msgStatus = new MessageStatus(DayOff.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(DivisionAssistant.TABLE_NAME)){
                msgStatus = new MessageStatus(DivisionAssistant.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(RunningAccount.TABLE_NAME)){
                msgStatus = new MessageStatus(RunningAccount.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(SPBSRunningNumber.TABLE_NAME)){
                msgStatus = new MessageStatus(SPBSRunningNumber.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(Penalty.TABLE_NAME)){
                msgStatus = new MessageStatus(Penalty.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(SPBSDestination.TABLE_NAME)){
                msgStatus = new MessageStatus(SPBSDestination.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(SPTARunningNumber.TABLE_NAME)){
                msgStatus = new MessageStatus(SPTARunningNumber.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(Vendor.TABLE_NAME)){
                msgStatus = new MessageStatus(Vendor.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(BLKSUGC.TABLE_NAME)){
                msgStatus = new MessageStatus(BLKSUGC.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(GroupHead.TABLE_NAME)){
                msgStatus = new MessageStatus(GroupHead.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(tblM_AccountingPeriod.TABLE_NAME)){
                msgStatus = new MessageStatus(tblM_AccountingPeriod.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }else if(tableName.equals(RKH_ACTIVITY_TYPE.TABLE_NAME)){
                msgStatus = new MessageStatus(RKH_ACTIVITY_TYPE.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }

            database.commitTransaction();
        }catch(XmlPullParserException e){
            e.printStackTrace();
            database.closeTransaction();
            msgStatus = new MessageStatus(tableName, false, e.getMessage());
        }catch(IOException e){
            e.printStackTrace();
            database.closeTransaction();
            msgStatus = new MessageStatus(tableName, false, e.getMessage());
        }catch (SQLiteException e) {
            e.printStackTrace();
            database.closeTransaction();
            msgStatus = new MessageStatus(tableName, false, e.getMessage());
        }finally{
            database.closeTransaction();
        }

        return msgStatus;
    }


    public MessageStatus createXML(int btnId, String status, boolean isUsbOtgSave, String date, String Search){
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        String companyCode = "";
        String estate = "";
        String division = "";
        String nik = "";
        MessageStatus msgStatus = null;

        int rowCount = 0;

        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();

        if(userLogin != null){
            companyCode = userLogin.getCompanyCode();
            estate = userLogin.getEstate();
            division = userLogin.getDivision();
            nik = userLogin.getNik();
        }


        switch (btnId) {
            case R.id.cbxExportTransaksiBC:
                try{
                    List<Object> listTransaction;
                    database.openTransaction();
                    if(status != null){
                        String GroupBY = "CompanyCode, Estate, Divisi, ZYear, Period, ZDate, Translevel, Block, SKB, BlockRow, CensusPoint, PokokLabel, PokokSide";
                        listTransaction = database.getListData(false, tblT_BlockCondition.TABLE_NAME, null,
                                tblT_BlockCondition.XML_CompanyCode+ "=? AND "+
                                        tblT_BlockCondition.XML_Period+ "=?",
                                new String [] {companyCode,Search},
                                GroupBY, null, tblT_BlockCondition.XML_Block, null);
                    }else{
                        String GroupBY = "CompanyCode, Estate, Divisi, ZYear, Period, ZDate, Translevel, Block, SKB, BlockRow, CensusPoint, PokokLabel, PokokSide";
                        listTransaction = database.getListData(false, tblT_BlockCondition.TABLE_NAME, null,
                                tblT_BlockCondition.XML_CompanyCode+ "=? AND "+
                                        tblT_BlockCondition.XML_Period+ "=?",
                                new String [] {companyCode,Search},
                                GroupBY, null, tblT_BlockCondition.XML_Block, null);
                    }

                    if(listTransaction.size() > 0){
                        xmlSerializer.setOutput(writer);
                        xmlSerializer.startDocument("UTF-8", true);
                        xmlSerializer.startTag("", tblT_BlockCondition.XML_FILE);

                        rowCount = rowCount + listTransaction.size();

                        for(int i = 0; i < listTransaction.size(); i++){
                            tblT_BlockCondition tblT_blockCondition = (tblT_BlockCondition) listTransaction.get(i);
                           // bpnHeader.setStatus(1);

                            String Tcompanycode = Set(tblT_blockCondition.getAcc_CompanyCode());

                            String Testate = Set(tblT_blockCondition.getAcc_Estate());
                            String TYear = Set(tblT_blockCondition.getAcc_ZYear());
                            String TPeriod = Set(tblT_blockCondition.getAcc_Period());
                            String TZDate = Set(tblT_blockCondition.getAcc_ZDate());
                            String TTransLevel = Set(String.valueOf(tblT_blockCondition.getAcc_TransLevel()));
                            String TBlock = Set(tblT_blockCondition.getAcc_Block());
                            String TSKB = Set(tblT_blockCondition.getAcc_SKB());
                            String TBlockRow = Set(tblT_blockCondition.getAcc_BlockRow());
                            String TCensusPoint = Set(tblT_blockCondition.getAcc_CensusPoint());
                            String TPokokLabel = Set(tblT_blockCondition.getAcc_PokokLabel());
                            String TPokokSide = Set(tblT_blockCondition.getAcc_PokokSide());
                            String TPokokCondition = Set(tblT_blockCondition.getAcc_PokokCondition());
                            String TJalan = Set(tblT_blockCondition.getAcc_Jalan());
                            String TJembatan = Set(tblT_blockCondition.getAcc_Jembatan());
                            String TParit = Set(tblT_blockCondition.getAcc_Parit());
                            String TTitiPanen = Set(tblT_blockCondition.getAcc_TitiPanen());
                            String TTitiRintis = Set(tblT_blockCondition.getAcc_TitiRintis());
                            String TTikus = Set(tblT_blockCondition.getAcc_Tikus());
                            String TPencurian = Set(tblT_blockCondition.getAcc_Pencurian());
                            String TBW = Set(tblT_blockCondition.getAcc_BW());
                            String TTPHBersih= Set(tblT_blockCondition.getAcc_TPHBersih());
                            String TTPHBersih2 = Set(tblT_blockCondition.getAcc_TPHBersih2());
                            String TTPH = Set(tblT_blockCondition.getAcc_TPH());
                            String TTPH2= Set(tblT_blockCondition.getAcc_TPH2());
                            String TPiringan= Set(tblT_blockCondition.getAcc_Piringan());
                            String TPasarPanen = Set(tblT_blockCondition.getAcc_PasarPanen());
                            String TPasarRintis = Set(tblT_blockCondition.getAcc_PasarRintis());
                            String TTunasPokok = Set(tblT_blockCondition.getAcc_TunasPokok());
                            String TGawangan = Set(tblT_blockCondition.getAcc_Gawangan());
                            String TDrainase = Set(tblT_blockCondition.getAcc_Drainase());
                            String TGanoderma = Set(tblT_blockCondition.getAcc_Ganoderma());
                            String TRayap = Set(tblT_blockCondition.getAcc_Rayap());
                            String TOrcytes = Set(tblT_blockCondition.getAcc_Orcytes());
                            String TSanitasi = Set(tblT_blockCondition.getAcc_Sanitasi());
                            String TKacangan = Set(tblT_blockCondition.getAcc_Kacangan());
                            String TCreatedDateTime = Set(tblT_blockCondition.getAcc_CreatedDateTime());
                            String TCreatedBy = Set(tblT_blockCondition.getAcc_CreatedBy());
                            String TModifiedDateTime = Set(tblT_blockCondition.getAcc_ModifiedDateTime());
                            String TModifiedBy = Set(tblT_blockCondition.getAcc_ModifiedBy());
                            String TFlag = Set(tblT_blockCondition.getAcc_Flag());
                            String TDivisi = Set(tblT_blockCondition.getAcc_DIVISI());
                            String TRemark = Set(tblT_blockCondition.getAcc_Remark());

                            xmlSerializer.startTag("", tblT_blockCondition.XML_DOCUMENT);
                            xmlSerializer.startTag("", tblT_blockCondition.XML_ID);
                            xmlSerializer.text(String.valueOf(i));
                            xmlSerializer.endTag("", tblT_blockCondition.XML_ID);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_CompanyCode);
                            xmlSerializer.text(Tcompanycode);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_CompanyCode);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Estate);
                            xmlSerializer.text(Testate);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Estate);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_ZYear);
                            xmlSerializer.text(TYear);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_ZYear);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Period);
                            xmlSerializer.text(TPeriod);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Period);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_ZDate);
                            xmlSerializer.text(TZDate);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_ZDate);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_TransLevel);
                            xmlSerializer.text(TTransLevel);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_TransLevel);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Block);
                            xmlSerializer.text(TBlock);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Block);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_SKB);
                            xmlSerializer.text(TSKB);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_SKB);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_BlockRow);
                            xmlSerializer.text(TBlockRow);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_BlockRow);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_CensusPoint);
                            xmlSerializer.text(TCensusPoint);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_CensusPoint);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_PokokLabel);
                            xmlSerializer.text(TPokokLabel);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_PokokLabel);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_PokokSide);
                            xmlSerializer.text(TPokokSide);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_PokokSide);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_PokokCondition);
                            xmlSerializer.text(TPokokCondition);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_PokokCondition);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Jalan);
                            xmlSerializer.text(TJalan);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Jalan);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Jembatan);
                            xmlSerializer.text(TJembatan);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Jembatan);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Parit);
                            xmlSerializer.text(TParit);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Parit);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_TitiPanen);
                            xmlSerializer.text(TTitiPanen);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_TitiPanen);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_TitiRintis);
                            xmlSerializer.text(TTitiRintis);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_TitiRintis);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Tikus);
                            xmlSerializer.text(TTikus);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Tikus);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Pencurian);
                            xmlSerializer.text(TPencurian);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Pencurian);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_BW);
                            xmlSerializer.text(TBW);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_BW);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_TPHBersih);
                            xmlSerializer.text(TTPHBersih);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_TPHBersih);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_TPHBersih2);
                            xmlSerializer.text(TTPHBersih2);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_TPHBersih2);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_TPH);
                            xmlSerializer.text(TTPH);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_TPH);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_TPH2);
                            xmlSerializer.text(TTPH2);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_TPH2);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Piringan);
                            xmlSerializer.text(TPiringan);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Piringan);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_PasarPanen);
                            xmlSerializer.text(TPasarPanen);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_PasarPanen);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_PasarRintis);
                            xmlSerializer.text(TPasarRintis);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_PasarRintis);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_TunasPokok);
                            xmlSerializer.text(TTunasPokok);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_TunasPokok);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Gawangan);
                            xmlSerializer.text(TGawangan);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Gawangan);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Drainase);
                            xmlSerializer.text(TDrainase);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Drainase);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Ganoderma);
                            xmlSerializer.text(TGanoderma);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Ganoderma);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Rayap);
                            xmlSerializer.text(TRayap);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Rayap);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Orcytes);
                            xmlSerializer.text(TOrcytes);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Orcytes);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Sanitasi);
                            xmlSerializer.text(TSanitasi);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Sanitasi);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Kacangan);
                            xmlSerializer.text(TKacangan);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Kacangan);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_CreatedDateTime);
                            xmlSerializer.text(TCreatedDateTime);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_CreatedDateTime);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_CreatedBy);
                            xmlSerializer.text(TCreatedBy);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_CreatedBy);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_ModifiedDateTime);
                            xmlSerializer.text(TModifiedDateTime);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_ModifiedDateTime);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_ModifiedBy);
                            xmlSerializer.text(TModifiedBy);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_ModifiedBy);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Flag);
                            xmlSerializer.text(TFlag);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Flag);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_DIVISI);
                            xmlSerializer.text(TDivisi);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_DIVISI);

                            xmlSerializer.startTag("", tblT_blockCondition.XML_Remark);
                            xmlSerializer.text(TRemark);
                            xmlSerializer.endTag("", tblT_blockCondition.XML_Remark);

                            xmlSerializer.endTag("", tblT_blockCondition.XML_DOCUMENT);
                        }

                        xmlSerializer.endTag("", tblT_BlockCondition.XML_FILE);
                        xmlSerializer.endDocument();

                        String xml = writer.toString();
                        //String fileName = "M2NTAKSASI" + "_" + estate + "_" + division + "_" + new DateLocal(new Date()).getDateString(DateLocal.FORMAT_FILE) + "_" + rowCount + ".xml";
                        //modified by adit 20161226
                        String fileName = "tblTBC" + "_" + estate + "_" + division + "_" + date.replace("-","")+"_"+"010101" + "_" + rowCount + ".xml";

                        msgStatus = saveXml(xml, fileName, "tblTBC", isUsbOtgSave);

                        if(msgStatus != null && msgStatus.isStatus()){
                            database.commitTransaction();
                        }


                    }else{
                        msgStatus = new MessageStatus("tblT_BC", false, context.getResources().getString(R.string.data_empty));
                    }
                }catch(NullPointerException e){
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("tblT_BC", false, e.getMessage());
                }catch(IllegalArgumentException e){
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("tblT_BC", false, e.getMessage());
                }catch(IllegalStateException e){
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("tblT_BC", false, e.getMessage());
                }catch(IOException e){
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("tblT_BC", false, e.getMessage());
                }catch(SQLiteException e){
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("tblT_BC", false, e.getMessage());
                }finally{
                    database.closeTransaction();
                }
                break;
            case R.id.cb_rkh:
                try {
                    List<Object> listRKHeader;

                    database.openTransaction();
                    if (status != null) {
                        listRKHeader = database.getListData(false, RKH_HEADER.TABLE_NAME, null,
                                        RKH_HEADER.XML_CREATED_BY + "=?" + " and " +
                                        RKH_HEADER.XML_STATUS + "=?" + " and " +
                                        RKH_HEADER.XML_RKH_DATE + "=?",
                                new String[]{nik, status, date},
                                null, null, null, null);
                    } else {
                        listRKHeader = database.getListData(false, RKH_HEADER.TABLE_NAME, null,

                                RKH_HEADER.XML_CREATED_BY + "=?" + " and " +
                                        RKH_HEADER.XML_RKH_DATE + "=?",
                                new String[]{nik, date},
                                null, null, null, null);
                    }

                    if (listRKHeader.size() > 0) {
                        xmlSerializer.setOutput(writer);
                        xmlSerializer.startDocument("UTF-8", true);
                        xmlSerializer.startTag("", RKH_HEADER.XML_FILE);

                        rowCount = rowCount + listRKHeader.size();

                        for (int i = 0; i < listRKHeader.size(); i++) {
                            RKH_HEADER rkhHeader = (RKH_HEADER) listRKHeader.get(i);
                            rkhHeader.setSTATUS(1);

                            String rkhimei = rkhHeader.getIMEI();
                            String rkhcompany = rkhHeader.getCOMPANY_CODE();
                            String rkhestate = rkhHeader.getESTATE();
                            String rkhgang = rkhHeader.getGANG();
                            String rkhdivision = rkhHeader.getDIVISION();
                            String rkhclerk = rkhHeader.getNIK_CLERK();
                            String rkhID = rkhHeader.getRKH_ID();
                            String rkhrkhForman = rkhHeader.getNIK_FOREMAN();
                            String rkhDate = rkhHeader.getRKH_DATE();
                            String createdDate = new DateLocal(new Date(rkhHeader.getCREATED_DATE())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT);
                            String modifiedDate = new DateLocal(new Date(rkhHeader.getMODIFIED_DATE())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT);


                            xmlSerializer.startTag("", RKH_HEADER.XML_DOCUMENT);
                            xmlSerializer.startTag("", RKH_HEADER.XML_IMEI);
                            xmlSerializer.text(rkhHeader.getIMEI());
                            xmlSerializer.endTag("", RKH_HEADER.XML_IMEI);

                            xmlSerializer.startTag("", RKH_HEADER.XML_COMPANY_CODE);
                            xmlSerializer.text(rkhHeader.getCOMPANY_CODE());
                            xmlSerializer.endTag("", RKH_HEADER.XML_COMPANY_CODE);

                            xmlSerializer.startTag("", RKH_HEADER.XML_ESTATE);
                            xmlSerializer.text(rkhHeader.getESTATE());
                            xmlSerializer.endTag("", RKH_HEADER.XML_ESTATE);

                            xmlSerializer.startTag("", RKH_HEADER.XML_RKH_DATE);
                            xmlSerializer.text(rkhHeader.getRKH_DATE());
                            xmlSerializer.endTag("", RKH_HEADER.XML_RKH_DATE);

                            xmlSerializer.startTag("", RKH_HEADER.XML_DIVISION);
                            xmlSerializer.text(rkhHeader.getDIVISION());
                            xmlSerializer.endTag("", RKH_HEADER.XML_DIVISION);

                            xmlSerializer.startTag("", RKH_HEADER.XML_GANG);
                            xmlSerializer.text(rkhHeader.getGANG());
                            xmlSerializer.endTag("", RKH_HEADER.XML_GANG);

                            xmlSerializer.startTag("", RKH_HEADER.XML_NIK_FOREMAN);
                            xmlSerializer.text(rkhHeader.getNIK_FOREMAN());
                            xmlSerializer.endTag("", RKH_HEADER.XML_NIK_FOREMAN);

                            xmlSerializer.startTag("", RKH_HEADER.XML_FOREMAN);
                            xmlSerializer.text(rkhHeader.getFOREMAN());
                            xmlSerializer.endTag("", RKH_HEADER.XML_FOREMAN);

                            xmlSerializer.startTag("", RKH_HEADER.XML_NIK_CLERK);
                            xmlSerializer.text(rkhHeader.getNIK_CLERK());
                            xmlSerializer.endTag("", RKH_HEADER.XML_NIK_CLERK);

                            xmlSerializer.startTag("", RKH_HEADER.XML_CLERK);
                            xmlSerializer.text(rkhHeader.getCLERK());
                            xmlSerializer.endTag("", RKH_HEADER.XML_CLERK);

                            xmlSerializer.startTag("", RKH_HEADER.XML_GPS_KOORDINAT);
                            xmlSerializer.text(rkhHeader.getGPS_KOORDINAT());
                            xmlSerializer.endTag("", RKH_HEADER.XML_GPS_KOORDINAT);

                            xmlSerializer.startTag("", RKH_HEADER.XML_STATUS);
                            xmlSerializer.text(String.valueOf(rkhHeader.getSTATUS()));
                            xmlSerializer.endTag("", RKH_HEADER.XML_STATUS);

                            xmlSerializer.startTag("", RKH_HEADER.XML_CREATED_DATE);

                            if (rkhHeader.getCREATED_DATE() > 0) {
                                xmlSerializer.text(new DateLocal(new Date(rkhHeader.getCREATED_DATE())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                            } else {
                                xmlSerializer.text("");
                            }
                            xmlSerializer.endTag("", RKH_HEADER.XML_CREATED_DATE);

                            xmlSerializer.startTag("", RKH_HEADER.XML_CREATED_BY);
                            xmlSerializer.text(rkhHeader.getCREATED_BY());
                            xmlSerializer.endTag("", RKH_HEADER.XML_CREATED_BY);

                            xmlSerializer.startTag("", RKH_HEADER.XML_MODIFIED_DATE);

                            if (rkhHeader.getMODIFIED_DATE() > 0) {
                                xmlSerializer.text(new DateLocal(new Date(rkhHeader.getMODIFIED_DATE())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                            } else {
                                xmlSerializer.text("");
                            }
                            xmlSerializer.endTag("", RKH_HEADER.XML_MODIFIED_DATE);

                            xmlSerializer.startTag("", RKH_HEADER.XML_MODIFIED_BY);
                            xmlSerializer.text(rkhHeader.getMODIFIED_BY());
                            xmlSerializer.endTag("", RKH_HEADER.XML_MODIFIED_BY);

                            xmlSerializer.startTag("", RKH_HEADER.XML_RKH_ID);
                            xmlSerializer.text("RKH"+rkhHeader.getRKH_ID());
                            xmlSerializer.endTag("", RKH_HEADER.XML_RKH_ID);


                            //line
                            List<Object> listRKHItem;

                            if (status != null) {
                                listRKHItem = database.getListData(false, RKH_ITEM.TABLE_NAME, null,
                                        RKH_ITEM.XML_RKH_ID + "=?" + " and " +
                                                RKH_ITEM.XML_STATUS + "=?",
                                        new String[]{rkhID, status},
                                        null, null, RKH_ITEM.XML_RKH_ID + ", " + RKH_ITEM.XML_CREATED_DATE, null);
                            } else {
                                listRKHItem = database.getListData(false, RKH_ITEM.TABLE_NAME, null,
                                        RKH_ITEM.XML_RKH_ID + "=?",
                                        new String[]{rkhID},
                                        null, null, RKH_ITEM.XML_RKH_ID + ", " + RKH_ITEM.XML_CREATED_DATE, null);
                            }

                            if (listRKHItem.size() > 0) {

                                rowCount = rowCount + listRKHItem.size();
                                for (int j = 0; j < listRKHItem.size(); j++) {
                                    RKH_ITEM rkh_item = (RKH_ITEM) listRKHItem.get(j);
                                    String item_line = rkh_item.getLINE();
                                    String item_activity = rkh_item.getACTIVITY();
                                    String item_block = rkh_item.getBLOCK();
                                    String item_target_output = rkh_item.getTARGET_OUTPUT();
                                    String item_sku = rkh_item.getSKU();
                                    String item_phl = rkh_item.getPHL();
                                    String item_target_hasil = rkh_item.getTARGET_HASIL();
                                    String item_target_janjang = rkh_item.getTARGET_JANJANG();
                                    String item_bjr = rkh_item.getBJR();
                                    String item_GPS = rkh_item.getGPS_KOORDINAT();
                                    int item_status = rkh_item.getSTATUS();
                                    long item_createdDate = rkh_item.getCREATED_DATE();
                                    String item_createdBy = rkh_item.getCREATED_BY();
                                    long item_modifiedDate = rkh_item.getMODIFIED_DATE();
                                    String item_modifiedBy = rkh_item.getMODIFIED_BY();
                                    String item_rkhid = "RKH"+rkh_item.getRKH_ID();
                                    String currentString = item_activity;
                                    String[] separated = currentString.split(";");


                                    xmlSerializer.startTag("", RKH_ITEM.XML_DOCUMENT);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_RKH_ID);
                                    xmlSerializer.text(item_rkhid);
                                    xmlSerializer.endTag("", RKH_ITEM.XML_RKH_ID);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_LINE);
                                    xmlSerializer.text(String.valueOf(rkh_item.getLINE()));
                                    xmlSerializer.endTag("", RKH_ITEM.XML_LINE);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_ACTIVITY);
                                    xmlSerializer.text(separated[0]);
                                    xmlSerializer.endTag("", RKH_ITEM.XML_ACTIVITY);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_BLOCK);
                                    xmlSerializer.text(String.valueOf(rkh_item.getBLOCK()));
                                    xmlSerializer.endTag("", RKH_ITEM.XML_BLOCK);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_TARGET_OUTPUT);
                                    xmlSerializer.text(String.valueOf(rkh_item.getTARGET_OUTPUT()));
                                    xmlSerializer.endTag("", RKH_ITEM.XML_TARGET_OUTPUT);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_SKU);
                                    xmlSerializer.text(String.valueOf(rkh_item.getSKU()));
                                    xmlSerializer.endTag("", RKH_ITEM.XML_SKU);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_PHL);
                                    xmlSerializer.text(String.valueOf(rkh_item.getPHL()));
                                    xmlSerializer.endTag("", RKH_ITEM.XML_PHL);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_TARGET_HASIL);
                                    xmlSerializer.text(String.valueOf(rkh_item.getTARGET_HASIL()));
                                    xmlSerializer.endTag("", RKH_ITEM.XML_TARGET_HASIL);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_TARGET_JANJANG);
                                    xmlSerializer.text(String.valueOf(rkh_item.getTARGET_JANJANG()));
                                    xmlSerializer.endTag("", RKH_ITEM.XML_TARGET_JANJANG);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_BJR);
                                    xmlSerializer.text(String.valueOf(rkh_item.getBJR()));
                                    xmlSerializer.endTag("", RKH_ITEM.XML_BJR);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_GPS_KOORDINAT);
                                    xmlSerializer.text(String.valueOf(rkh_item.getGPS_KOORDINAT()));
                                    xmlSerializer.endTag("", RKH_ITEM.XML_GPS_KOORDINAT);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_STATUS);
                                    xmlSerializer.text(String.valueOf(rkh_item.getSTATUS()));
                                    xmlSerializer.endTag("", RKH_ITEM.XML_STATUS);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_CREATED_DATE);
                                    if (rkh_item.getCREATED_DATE() > 0) {
                                        xmlSerializer.text(new DateLocal(new Date(rkh_item.getCREATED_DATE())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                    } else {
                                        xmlSerializer.text("");
                                    }
                                    xmlSerializer.endTag("", RKH_ITEM.XML_CREATED_DATE);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_CREATED_BY);
                                    xmlSerializer.text(rkh_item.getCREATED_BY());
                                    xmlSerializer.endTag("", RKH_ITEM.XML_CREATED_BY);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_MODIFIED_DATE);
                                    if (rkh_item.getMODIFIED_DATE() > 0) {
                                        xmlSerializer.text(new DateLocal(new Date(rkh_item.getMODIFIED_DATE())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                    } else {
                                        xmlSerializer.text("");
                                    }

                                    xmlSerializer.endTag("", RKH_ITEM.XML_MODIFIED_DATE);

                                    xmlSerializer.startTag("", RKH_ITEM.XML_MODIFIED_BY);
                                    xmlSerializer.text(rkh_item.getMODIFIED_BY());
                                    xmlSerializer.endTag("", RKH_ITEM.XML_MODIFIED_BY);

                                    xmlSerializer.endTag("", RKH_ITEM.XML_DOCUMENT);
                                }
                            }
                            xmlSerializer.endTag("", RKH_HEADER.XML_DOCUMENT);
                        }

                        xmlSerializer.endTag("", RKH_HEADER.XML_FILE);
                        xmlSerializer.endDocument();

                        String xml = writer.toString();
                        //String fileName = "M2NSPBS" + "_" + estate + "_" + division + "_" + new DateLocal(new Date()).getDateString(DateLocal.FORMAT_FILE) + "_" + rowCount + ".xml";
                        //modified by adit 20161226
                        String fileName = "M2NRKH" + "_" + estate + "_" + division + "_" + date.replace("-", "") + "_" + "010101" + "_" + rowCount + ".xml";

                        msgStatus = saveXml(xml, fileName, "RKH", isUsbOtgSave);

                        if (msgStatus != null && msgStatus.isStatus()) {
                            database.commitTransaction();
                        }

//						FolderHandler folderHandler = new FolderHandler(context);
//
//						if(folderHandler.init()){
//							String fileName = "M2NSPBS" + "_" + estate + "_" + division + "_" + new DateLocal(new Date()).getDateString(DateLocal.FORMAT_FILE) + "_" + rowCount + ".xml";
//							String folder = folderHandler.getFileReportNew();
//
//							if(new FileXMLHandler(context).createFile(folder, fileName, xml)){
//								database.commitTransaction();
//
//								msgStatus = new MessageStatus("SPBS", true, context.getResources().getString(R.string.export_successed));
//							}else{
//								msgStatus = new MessageStatus("SPBS", false, context.getResources().getString(R.string.export_failed));
//							}
//						}else{
//							msgStatus = new MessageStatus("SPBS", false, context.getResources().getString(R.string.error_sd_card));
//						}
                    } else {
                        msgStatus = new MessageStatus("RKH", false, context.getResources().getString(R.string.data_empty));
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("RKH", false, e.getMessage());
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("RKH", false, e.getMessage());
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("RKH", false, e.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("RKH", false, e.getMessage());
                } catch (SQLiteException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("RKH", false, e.getMessage());
                } finally {
                    database.closeTransaction();
                }
                break;
            default:
                break;
        }

        return msgStatus;
    }

    public MessageStatus importXML(InputStream inputStream){
        String tableName = "";
        XmlPullParserFactory factory = null;
        XmlPullParser parser = null;
        boolean success = false;
        MessageStatus msgStatus = null;

        try{
            database.openTransaction();

            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            parser = factory.newPullParser();

            parser.setInput(inputStream, null);

            int eventType = parser.getEventType();
            String textValue = null;

            long todayDate = new Date().getTime();

            BKMHeader bkmHeader = null;
            BKMLine bkmLine = null;
            BKMOutput bkmOutput = null;

            BPNHeader bpnHeader = null;
            BPNQuantity bpnQuantity = null;
            BPNQuality bpnQuality = null;

            SPBSHeader spbsHeader = null;
            SPBSLine spbsLine = null;

            TaksasiHeader taksasiHeader = null;
            TaksasiLine taksasiLine = null;

            String fileType = "";

            int status = 1;
            int isSave = 1;
            long createdDate = todayDate;
            String createdBy = "";
            long modifiedDate = 0;
            String modifiedBy = "";

            msgStatus = new MessageStatus();

            while(eventType != XmlPullParser.END_DOCUMENT){
                String tagName = parser.getName();

                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if(TextUtils.isEmpty(fileType)){
                            if(tagName.equalsIgnoreCase(BKMHeader.XML_DOCUMENT_RESTORE)){
                                fileType = BKMHeader.XML_DOCUMENT_RESTORE;
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_DOCUMENT_RESTORE)){
                                fileType = BPNHeader.XML_DOCUMENT_RESTORE;
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_DOCUMENT_RESTORE)){
                                fileType = SPBSHeader.XML_DOCUMENT_RESTORE;
                            }else if(tagName.equalsIgnoreCase(TaksasiHeader.XML_DOCUMENT_RESTORE)){
                                fileType = TaksasiHeader.XML_DOCUMENT_RESTORE;
                            }
                        }

                        if(fileType.equalsIgnoreCase(BKMHeader.XML_DOCUMENT_RESTORE)){
                            if(tagName.equalsIgnoreCase(BKMHeader.XML_DOCUMENT_RESTORE)){
                                tableName = BKMHeader.TABLE_NAME;
                                fileType = BKMHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("BKM");
                            }else if(tagName.equalsIgnoreCase(BKMLine.XML_DOCUMENT_RESTORE)){
                                tableName = BKMLine.TABLE_NAME;
                                fileType = BKMHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("BKM");
                            }else if(tagName.equalsIgnoreCase(BKMOutput.XML_DOCUMENT_RESTORE)){
                                tableName = BKMOutput.TABLE_NAME;
                                fileType = BKMHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("BKM");
                            }

                            if(tableName.equalsIgnoreCase(BKMHeader.TABLE_NAME)){
                                if(tagName.equalsIgnoreCase(BKMHeader.XML_ITEM_RESTORE)){
                                    bkmHeader = new BKMHeader();
                                }
                            }else if(tableName.equalsIgnoreCase(BKMLine.TABLE_NAME)){
                                if(tagName.equalsIgnoreCase(BKMLine.XML_ITEM_RESTORE)){
                                    bkmLine = new BKMLine();
                                }
                            }else if(tableName.equalsIgnoreCase(BKMOutput.TABLE_NAME)){
                                if(tagName.equalsIgnoreCase(BKMOutput.XML_ITEM_RESTORE)){
                                    bkmOutput = new BKMOutput();
                                }
                            }
                        }else if(fileType.equalsIgnoreCase(BPNHeader.XML_DOCUMENT_RESTORE)){
                            if(tagName.equalsIgnoreCase(BPNHeader.XML_DOCUMENT_RESTORE)){
                                tableName = BPNHeader.TABLE_NAME;
                                fileType = BPNHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("BPN");
                            }else if(tagName.equalsIgnoreCase(BPNQuantity.XML_DOCUMENT_RESTORE)){
                                tableName = BPNQuantity.TABLE_NAME;
                                fileType = BPNHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("BPN");
                            }else if(tagName.equalsIgnoreCase(BPNQuality.XML_DOCUMENT_RESTORE)){
                                tableName = BPNQuality.TABLE_NAME;
                                fileType = BPNHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("BPN");
                            }

                            if(tableName.equalsIgnoreCase(BPNHeader.TABLE_NAME)){
                                if(tagName.equalsIgnoreCase(BPNHeader.XML_ITEM_RESTORE)){
                                    bpnHeader = new BPNHeader();
                                }
                            }else if(tableName.equalsIgnoreCase(BPNQuantity.TABLE_NAME)){
                                if(tagName.equalsIgnoreCase(BPNQuantity.XML_ITEM_RESTORE)){
                                    bpnQuantity = new BPNQuantity();
                                }
                            }else if(tableName.equalsIgnoreCase(BPNQuality.TABLE_NAME)){
                                if(tagName.equalsIgnoreCase(BPNQuality.XML_ITEM_RESTORE)){
                                    bpnQuality = new BPNQuality();
                                }
                            }
                        }else if(fileType.equalsIgnoreCase(SPBSHeader.XML_DOCUMENT_RESTORE)){
                            if(tagName.equalsIgnoreCase(SPBSHeader.XML_DOCUMENT_RESTORE)){
                                tableName = SPBSHeader.TABLE_NAME;
                                fileType = SPBSHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("SPBS");
                            }else if(tagName.equalsIgnoreCase(SPBSLine.XML_DOCUMENT_RESTORE)){
                                tableName = SPBSLine.TABLE_NAME;
                                fileType = SPBSHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("SPBS");
                            }

                            if(tableName.equalsIgnoreCase(SPBSHeader.TABLE_NAME)){
                                if(tagName.equalsIgnoreCase(SPBSHeader.XML_ITEM_RESTORE)){
                                    spbsHeader = new SPBSHeader();
                                }
                            }else if(tableName.equalsIgnoreCase(SPBSLine.TABLE_NAME)){
                                if(tagName.equalsIgnoreCase(SPBSLine.XML_ITEM_RESTORE)){
                                    spbsLine = new SPBSLine();
                                }
                            }
                        }else if(fileType.equalsIgnoreCase(TaksasiHeader.XML_DOCUMENT_RESTORE)){
                            if(tagName.equalsIgnoreCase(TaksasiHeader.XML_DOCUMENT_RESTORE)){
                                tableName = TaksasiHeader.TABLE_NAME;
                                fileType = TaksasiHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("TAKSASI");
                            }else if(tagName.equalsIgnoreCase(TaksasiLine.XML_DOCUMENT_RESTORE)){
                                tableName = TaksasiLine.TABLE_NAME;
                                fileType = TaksasiHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("TAKSASI");
                            }

                            if(tableName.equalsIgnoreCase(TaksasiHeader.TABLE_NAME)){
                                if(tagName.equalsIgnoreCase(TaksasiHeader.XML_ITEM_RESTORE)){
                                    taksasiHeader = new TaksasiHeader();
                                }
                            }else if(tableName.equalsIgnoreCase(TaksasiLine.TABLE_NAME)){
                                if(tagName.equalsIgnoreCase(TaksasiLine.XML_ITEM_RESTORE)){
                                    taksasiLine = new TaksasiLine();
                                }
                            }
                        }
                        break;
                    case XmlPullParser.TEXT:
                        textValue = parser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if(fileType.equalsIgnoreCase(BPNHeader.XML_DOCUMENT_RESTORE)){
                            if(tagName.equalsIgnoreCase(BPNHeader.XML_ITEM_RESTORE)){
                                tableName = BPNHeader.TABLE_NAME;
                            }
                        }else if(fileType.equalsIgnoreCase(SPBSHeader.XML_DOCUMENT_RESTORE)){
                            if(tagName.equalsIgnoreCase(SPBSHeader.XML_ITEM_RESTORE)){
                                tableName = SPBSHeader.TABLE_NAME;
                            }
                        }else if(fileType.equalsIgnoreCase(TaksasiHeader.XML_DOCUMENT_RESTORE)){
                            if(tagName.equalsIgnoreCase(TaksasiHeader.XML_ITEM_RESTORE)){
                                tableName = TaksasiHeader.TABLE_NAME;
                            }
                        }else if(fileType.equalsIgnoreCase(BKMHeader.XML_DOCUMENT_RESTORE)){
                            if(tagName.equalsIgnoreCase(BKMHeader.XML_ITEM_RESTORE)){
                                tableName = BKMHeader.TABLE_NAME;
                            }
                        }

                        if(tableName.equalsIgnoreCase(BKMHeader.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BKMHeader.XML_ITEM_RESTORE)){
                                bkmHeader.setStatus(status);

                                BKMHeader bkm = (BKMHeader) database.getDataFirst(false, BKMHeader.TABLE_NAME, null,
                                        BKMHeader.XML_IMEI + "=?" + " and " +
                                                BKMHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                BKMHeader.XML_ESTATE + "=?" + " and " +
                                                BKMHeader.XML_BKM_DATE + "=?" + " and " +
                                                BKMHeader.XML_DIVISION + "=?" + " and " +
                                                BKMHeader.XML_GANG + "=?",
                                        new String [] {bkmHeader.getImei(), bkmHeader.getCompanyCode(), bkmHeader.getEstate(),
                                                bkmHeader.getBkmDate(), bkmHeader.getDivision(), bkmHeader.getGang()},
                                        null, null, null, null);

                                if(bkm != null){
                                    database.updateData(bkmHeader,
                                            BKMHeader.XML_IMEI + "=?" + " and " +
                                                    BKMHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                    BKMHeader.XML_ESTATE + "=?" + " and " +
                                                    BKMHeader.XML_BKM_DATE + "=?" + " and " +
                                                    BKMHeader.XML_DIVISION + "=?" + " and " +
                                                    BKMHeader.XML_GANG + "=?",
                                            new String [] {bkmHeader.getImei(), bkmHeader.getCompanyCode(), bkmHeader.getEstate(),
                                                    bkmHeader.getBkmDate(), bkmHeader.getDivision(), bkmHeader.getGang()});
                                }else{
                                    database.setData(bkmHeader);
                                }
                            }else if (tagName.equalsIgnoreCase(BKMHeader.XML_BKM_DATE_RESTORE)){
                                bkmHeader.setBkmDate(new DateLocal(textValue, DateLocal.FORMAT_RESTORE).getDateString(DateLocal.FORMAT_INPUT));
                            }else if(tagName.equalsIgnoreCase(BKMHeader.XML_IMEI_RESTORE)){
                                bkmHeader.setImei(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMHeader.XML_ESTATE_RESTORE)){
                                bkmHeader.setEstate(textValue);
                                bkmHeader.setCompanyCode(textValue.substring(0, 2) + "00");
                            }else if(tagName.equalsIgnoreCase(BKMHeader.XML_DIVISION_RESTORE)){
                                bkmHeader.setDivision(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMHeader.XML_GANG_RESTORE)){
                                bkmHeader.setGang(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMHeader.XML_NIK_FOREMAN_RESTORE)){
                                bkmHeader.setNikForeman(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMHeader.XML_NIK_CLERK_RESTORE)){
                                bkmHeader.setNikClerk(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMHeader.XML_CREATED_DATE_RESTORE)){
                                bkmHeader.setCreatedDate(todayDate);
                            }else if(tagName.equalsIgnoreCase(BKMHeader.XML_CREATED_BY_RESTORE)){
                                bkmHeader.setCreatedBy(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(BKMLine.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BKMLine.XML_ITEM_RESTORE)){
                                bkmLine.setImei(bkmHeader.getImei());
                                bkmLine.setStatus(status);

                                BKMLine bkm = (BKMLine) database.getDataFirst(false, BKMLine.TABLE_NAME, null,
                                        BKMLine.XML_IMEI + "=?" + " and " +
                                                BKMLine.XML_COMPANY_CODE + "=?" + " and " +
                                                BKMLine.XML_ESTATE + "=?" + " and " +
                                                BKMLine.XML_BKM_DATE + "=?" + " and " +
                                                BKMLine.XML_DIVISION + "=?" + " and " +
                                                BKMLine.XML_GANG + "=?" + " and " +
                                                BKMLine.XML_NIK + "=?",
                                        new String [] {bkmLine.getImei(), bkmLine.getCompanyCode(), bkmLine.getEstate(),
                                                bkmLine.getBkmDate(), bkmLine.getDivision(), bkmLine.getGang(), bkmLine.getNik()},
                                        null, null, null, null);

                                if(bkm != null){
                                    database.updateData(bkm, BKMLine.XML_IMEI + "=?" + " and " +
                                                    BKMLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    BKMLine.XML_ESTATE + "=?" + " and " +
                                                    BKMLine.XML_BKM_DATE + "=?" + " and " +
                                                    BKMLine.XML_DIVISION + "=?" + " and " +
                                                    BKMLine.XML_GANG + "=?" + " and " +
                                                    BKMLine.XML_NIK + "=?",
                                            new String [] {bkmLine.getImei(), bkmLine.getCompanyCode(), bkmLine.getEstate(),
                                                    bkmLine.getBkmDate(), bkmLine.getDivision(), bkmLine.getGang(), bkmLine.getNik()});
                                }else{
                                    database.setData(bkmLine);
                                }
                            }else if(tagName.equalsIgnoreCase(BKMLine.XML_BKM_DATE_RESTORE)){
                                bkmLine.setBkmDate(new DateLocal(textValue, DateLocal.FORMAT_RESTORE).getDateString(DateLocal.FORMAT_INPUT));
                            }else if(tagName.equalsIgnoreCase(BKMLine.XML_ESTATE_RESTORE)){
                                bkmLine.setEstate(textValue);
                                bkmLine.setCompanyCode(textValue.substring(0, 2) + "00");
                            }else if(tagName.equalsIgnoreCase(BKMLine.XML_DIVISION_RESTORE)){
                                bkmLine.setDivision(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMLine.XML_GANG_RESTORE)){
                                bkmLine.setGang(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMLine.XML_NIK_RESTORE)){
                                bkmLine.setNik(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMLine.XML_NAME_RESTORE)){
                                bkmLine.setName(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMLine.XML_ABSENT_TYPE_RESTORE)){
                                bkmLine.setAbsentType(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMLine.XML_MANDAYS_RESTORE)){
                                bkmLine.setMandays(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(BKMLine.XML_UOM_RESTORE)){
                                bkmLine.setUom(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMLine.XML_CREATED_DATE_RESTORE)){
                                bkmLine.setCreatedDate(todayDate);
                            }else if(tagName.equalsIgnoreCase(BKMLine.XML_CREATED_BY_RESTORE)){
                                bkmLine.setCreatedBy(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(BKMOutput.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BKMOutput.XML_ITEM_RESTORE)){
                                bkmOutput.setImei(bkmHeader.getImei());
                                bkmOutput.setStatus(status);
                                bkmOutput.setCreatedDate(bkmHeader.getCreatedDate());
                                bkmOutput.setCreatedBy(bkmHeader.getCreatedBy());

                                BKMOutput bkm = (BKMOutput) database.getDataFirst(false, BKMOutput.TABLE_NAME, null,
                                        BKMOutput.XML_IMEI + "=?" + " and " +
                                                BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
                                                BKMOutput.XML_ESTATE + "=?" + " and " +
                                                BKMOutput.XML_BKM_DATE + "=?" + " and " +
                                                BKMOutput.XML_DIVISION + "=?" + " and " +
                                                BKMOutput.XML_GANG + "=?" + " and " +
                                                BKMOutput.XML_NIK + "=?" + " and " +
                                                BKMOutput.XML_BLOCK + "=?",
                                        new String [] {bkmOutput.getImei(), bkmOutput.getCompanyCode(), bkmOutput.getEstate(),
                                                bkmLine.getBkmDate(), bkmOutput.getDivision(), bkmOutput.getGang(), bkmOutput.getNik(), bkmOutput.getBlock()},
                                        null, null, null, null);

                                if(bkm != null){
                                    database.updateData(bkmOutput,
                                            BKMOutput.XML_IMEI + "=?" + " and " +
                                                    BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
                                                    BKMOutput.XML_ESTATE + "=?" + " and " +
                                                    BKMOutput.XML_BKM_DATE + "=?" + " and " +
                                                    BKMOutput.XML_DIVISION + "=?" + " and " +
                                                    BKMOutput.XML_GANG + "=?" + " and " +
                                                    BKMOutput.XML_NIK + "=?" + " and " +
                                                    BKMOutput.XML_BLOCK + "=?",
                                            new String [] {bkmOutput.getImei(), bkmOutput.getCompanyCode(), bkmOutput.getEstate(),
                                                    bkmLine.getBkmDate(), bkmOutput.getDivision(), bkmOutput.getGang(), bkmOutput.getNik(), bkmOutput.getBlock()});
                                }else{
                                    database.setData(bkmOutput);
                                }
                            }else if(tagName.equalsIgnoreCase(BKMOutput.XML_BKM_DATE_RESTORE)){
                                bkmOutput.setBkmDate(new DateLocal(textValue, DateLocal.FORMAT_RESTORE).getDateString(DateLocal.FORMAT_INPUT));
                            }else if(tagName.equalsIgnoreCase(BKMOutput.XML_ESTATE_RESTORE)){
                                bkmOutput.setEstate(textValue);
                                bkmOutput.setCompanyCode(textValue.substring(0, 2) + "00");
                            }else if(tagName.equalsIgnoreCase(BKMOutput.XML_DIVISION_RESTORE)){
                                bkmOutput.setDivision(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMOutput.XML_GANG_RESTORE)){
                                bkmOutput.setGang(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMOutput.XML_BLOCK_RESTORE)){
                                bkmOutput.setBlock(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMOutput.XML_NIK_RESTORE)){
                                bkmOutput.setNik(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMOutput.XML_NAME_RESTORE)){
                                bkmOutput.setName(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMOutput.XML_OUTPUT_RESTORE)){
                                bkmOutput.setOutput(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(BKMOutput.XML_UOM_RESTORE)){
                                bkmOutput.setUom(textValue);
                            }else if(tagName.equalsIgnoreCase(BKMOutput.XML_DOCUMENT_RESTORE)){
                                tableName = BKMHeader.TABLE_NAME;
                            }
                        }else if(tableName.equalsIgnoreCase(BPNHeader.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BPNHeader.XML_ITEM_RESTORE)){
                                bpnHeader.setStatus(status);

                                BPNHeader bpn = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null,
                                        BPNHeader.XML_BPN_ID + "=? " + " and " +
                                                BPNHeader.XML_IMEI + "=? " + " and " +
                                                BPNHeader.XML_COMPANY_CODE + "=? " + " and " +
                                                BPNHeader.XML_ESTATE + "=? " + " and " +
                                                BPNHeader.XML_BPN_DATE + "=? " + " and " +
                                                BPNHeader.XML_DIVISION + "=? " + " and " +
                                                BPNHeader.XML_GANG + "=? " + " and " +
                                                BPNHeader.XML_LOCATION + "=? " + " and " +
                                                BPNHeader.XML_TPH + "=? " + " and " +
                                                BPNHeader.XML_NIK_HARVESTER + "=?",
                                        new String [] {bpnHeader.getBpnId(), bpnHeader.getImei(), bpnHeader.getCompanyCode(), bpnHeader.getEstate(), bpnHeader.getBpnDate(),
                                                bpnHeader.getDivision(), bpnHeader.getGang(), bpnHeader.getLocation(), bpnHeader.getTph(), bpnHeader.getNikHarvester()},
                                        null, null, null, null);

                                if(bpn != null){
                                    database.updateData(bpnHeader,
                                            BPNHeader.XML_BPN_ID + "=? " + " and " +
                                                    BPNHeader.XML_IMEI + "=? " + " and " +
                                                    BPNHeader.XML_COMPANY_CODE + "=? " + " and " +
                                                    BPNHeader.XML_ESTATE + "=? " + " and " +
                                                    BPNHeader.XML_BPN_DATE + "=? " + " and " +
                                                    BPNHeader.XML_DIVISION + "=? " + " and " +
                                                    BPNHeader.XML_GANG + "=? " + " and " +
                                                    BPNHeader.XML_LOCATION + "=? " + " and " +
                                                    BPNHeader.XML_TPH + "=? " + " and " +
                                                    BPNHeader.XML_NIK_HARVESTER + "=?",
                                            new String [] {bpnHeader.getBpnId(), bpnHeader.getImei(), bpnHeader.getCompanyCode(), bpnHeader.getEstate(), bpnHeader.getBpnDate(),
                                                    bpnHeader.getDivision(), bpnHeader.getGang(), bpnHeader.getLocation(), bpnHeader.getTph(), bpnHeader.getNikHarvester()});
                                }else{
                                    database.setData(bpnHeader);
                                }
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_ID_RESTORE)){
                                bpnHeader.setBpnId(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_COMPANY_CODE_RESTORE)){
                                bpnHeader.setCompanyCode(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_ESTATE_RESTORE)){
                                bpnHeader.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_BPN_DATE_RESTORE)){
                                bpnHeader.setBpnDate(new DateLocal(textValue, DateLocal.FORMAT_RESTORE).getDateString(DateLocal.FORMAT_INPUT));
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_DIVISION_RESTORE)){
                                bpnHeader.setDivision(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_GANG_RESTORE)){
                                bpnHeader.setGang(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_FOREMAN_RESTORE)){
                                bpnHeader.setForeman(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_NIK_FOREMAN_RESTORE)){
                                bpnHeader.setNikForeman(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_CLERK_RESTORE)){
                                bpnHeader.setClerk(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_NIK_CLERK_RESTORE)){
                                bpnHeader.setNikClerk(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_HARVERSTER_RESTORE)){
                                bpnHeader.setHarvester(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_NIK_HARVESTER_RESTORE)){
                                bpnHeader.setNikHarvester(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_LOCATION_RESTORE)){
                                bpnHeader.setLocation(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_TPH_RESTORE)){
                                bpnHeader.setTph(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_CROP_RESTORE)){
                                bpnHeader.setCrop(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_USE_GERDANG_RESTORE)){
                                bpnHeader.setUseGerdang(new Converter(textValue).StrToInt());
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_GPS_KOORDINAT_RESTORE)){
                                bpnHeader.setGpsKoordinat(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_IMEI_RESTORE)){
                                bpnHeader.setImei(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_CREATED_DATE_RESTORE)){
                                bpnHeader.setCreatedDate(todayDate);
                            }else if(tagName.equalsIgnoreCase(BPNHeader.XML_CREATED_BY_RESTORE)){
                                bpnHeader.setCreatedBy(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(BPNQuantity.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BPNQuantity.XML_ITEM_RESTORE)){
                                bpnQuantity.setBpnId(bpnHeader.getBpnId());
                                bpnQuantity.setImei(bpnHeader.getImei());
                                bpnQuantity.setCompanyCode(bpnHeader.getCompanyCode());
                                bpnQuantity.setEstate(bpnHeader.getEstate());
                                bpnQuantity.setBpnDate(bpnHeader.getBpnDate());
                                bpnQuantity.setDivision(bpnHeader.getDivision());
                                bpnQuantity.setGang(bpnHeader.getGang());
                                bpnQuantity.setLocation(bpnHeader.getLocation());
                                bpnQuantity.setTph(bpnHeader.getTph());
                                bpnQuantity.setNikHarvester(bpnHeader.getNikHarvester());
                                bpnQuantity.setCrop(bpnHeader.getCrop());
                                bpnQuantity.setStatus(status);
                                bpnQuantity.setCreatedDate(bpnHeader.getCreatedDate());
                                bpnQuantity.setCreatedBy(bpnHeader.getCreatedBy());

                                BPNQuantity bpn = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null,
                                        BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                BPNQuantity.XML_IMEI + "=?" + " and " +
                                                BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                BPNQuantity.XML_DIVISION + "=?" + " and " +
                                                BPNQuantity.XML_GANG + "=?" + " and " +
                                                BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                BPNQuantity.XML_TPH + "=?" + " and " +
                                                BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
                                                BPNQuantity.XML_CROP + "=?" + " and " +
                                                BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                        new String [] {bpnQuantity.getBpnId(), bpnQuantity.getImei(), bpnQuantity.getCompanyCode(), bpnQuantity.getEstate(),
                                                bpnQuantity.getBpnDate(), bpnQuantity.getDivision(), bpnQuantity.getGang(), bpnQuantity.getLocation(),
                                                bpnQuantity.getTph(), bpnQuantity.getNikHarvester(), bpnQuantity.getCrop(), bpnQuantity.getAchievementCode()},
                                        null, null, null, null);

                                if(bpn != null){
                                    database.updateData(bpnQuantity,
                                            BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                    BPNQuantity.XML_IMEI + "=?" + " and " +
                                                    BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                    BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                    BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                    BPNQuantity.XML_DIVISION + "=?" + " and " +
                                                    BPNQuantity.XML_GANG + "=?" + " and " +
                                                    BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                    BPNQuantity.XML_TPH + "=?" + " and " +
                                                    BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
                                                    BPNQuantity.XML_CROP + "=?" + " and " +
                                                    BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                            new String [] {bpnQuantity.getBpnId(), bpnQuantity.getImei(), bpnQuantity.getCompanyCode(), bpnQuantity.getEstate(),
                                                    bpnQuantity.getBpnDate(), bpnQuantity.getDivision(), bpnQuantity.getGang(), bpnQuantity.getLocation(),
                                                    bpnQuantity.getTph(), bpnQuantity.getNikHarvester(), bpnQuantity.getCrop(), bpnQuantity.getAchievementCode()});
                                }else{
                                    database.setData(bpnQuantity);
                                }
                            }else if(tagName.equalsIgnoreCase(BPNQuantity.XML_ACHIEVEMENT_CODE_RESTORE)){
                                bpnQuantity.setAchievementCode(textValue);
                            }else if(tagName.equalsIgnoreCase(bpnQuantity.XML_QUANTITY_RESTORE)){
                                bpnQuantity.setQuantity(new Converter(textValue).StrToDouble());
                            }
                        }else if(tableName.equalsIgnoreCase(BPNQuality.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(BPNQuality.XML_ITEM_RESTORE)){
                                bpnQuality.setBpnId(bpnHeader.getBpnId());
                                bpnQuality.setImei(bpnHeader.getImei());
                                bpnQuality.setCompanyCode(bpnHeader.getCompanyCode());
                                bpnQuality.setEstate(bpnHeader.getEstate());
                                bpnQuality.setBpnDate(bpnHeader.getBpnDate());
                                bpnQuality.setDivision(bpnHeader.getDivision());
                                bpnQuality.setGang(bpnHeader.getGang());
                                bpnQuality.setLocation(bpnHeader.getLocation());
                                bpnQuality.setTph(bpnHeader.getTph());
                                bpnQuality.setNikHarvester(bpnHeader.getNikHarvester());
                                bpnQuality.setCrop(bpnHeader.getCrop());
                                bpnQuality.setAchievementCode("01");
                                bpnQuality.setStatus(status);
                                bpnQuality.setCreatedDate(bpnHeader.getCreatedDate());
                                bpnQuality.setCreatedBy(bpnHeader.getCreatedBy());

                                BPNQuality bpn = (BPNQuality) database.getDataFirst(false, BPNQuality.TABLE_NAME, null,
                                        BPNQuality.XML_BPN_ID + "=?" + " and " +
                                                BPNQuality.XML_IMEI + "=?" + " and " +
                                                BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
                                                BPNQuality.XML_ESTATE + "=?" + " and " +
                                                BPNQuality.XML_BPN_DATE + "=?" + " and " +
                                                BPNQuality.XML_DIVISION + "=?" + " and " +
                                                BPNQuality.XML_GANG + "=?" + " and " +
                                                BPNQuality.XML_LOCATION + "=?" + " and " +
                                                BPNQuality.XML_TPH + "=?" + " and " +
                                                BPNQuality.XML_NIK_HARVESTER + "=?" + " and " +
                                                BPNQuality.XML_CROP + "=?" + " and " +
                                                BPNQuality.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                BPNQuality.XML_QUALITY_CODE + "=?",
                                        new String [] {bpnQuality.getBpnId(), bpnQuality.getImei(), bpnQuality.getCompanyCode(), bpnQuality.getEstate(),
                                                bpnQuality.getBpnDate(), bpnQuality.getDivision(), bpnQuality.getGang(), bpnQuality.getLocation(),
                                                bpnQuality.getTph(), bpnQuality.getNikHarvester(), bpnQuality.getCrop(), bpnQuality.getAchievementCode(),
                                                bpnQuality.getQualityCode()},
                                        null, null, null, null);

                                if(bpn != null){
                                    database.updateData(bpnQuality,
                                            BPNQuality.XML_BPN_ID + "=?" + " and " +
                                                    BPNQuality.XML_IMEI + "=?" + " and " +
                                                    BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
                                                    BPNQuality.XML_ESTATE + "=?" + " and " +
                                                    BPNQuality.XML_BPN_DATE + "=?" + " and " +
                                                    BPNQuality.XML_DIVISION + "=?" + " and " +
                                                    BPNQuality.XML_GANG + "=?" + " and " +
                                                    BPNQuality.XML_LOCATION + "=?" + " and " +
                                                    BPNQuality.XML_TPH + "=?" + " and " +
                                                    BPNQuality.XML_NIK_HARVESTER + "=?" + " and " +
                                                    BPNQuality.XML_CROP + "=?" + " and " +
                                                    BPNQuality.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                    BPNQuality.XML_QUALITY_CODE + "=?",
                                            new String [] {bpnQuality.getBpnId(), bpnQuality.getImei(), bpnQuality.getCompanyCode(), bpnQuality.getEstate(),
                                                    bpnQuality.getBpnDate(), bpnQuality.getDivision(), bpnQuality.getGang(), bpnQuality.getLocation(),
                                                    bpnQuality.getTph(), bpnQuality.getNikHarvester(), bpnQuality.getCrop(), bpnQuality.getAchievementCode(),
                                                    bpnQuality.getQualityCode()});
                                }else{
                                    database.setData(bpnQuality);
                                }
                            }else if(tagName.equalsIgnoreCase(BPNQuality.XML_QUALITY_CODE_RESTORE)){
                                bpnQuality.setQualityCode(textValue);
                            }else if(tagName.equalsIgnoreCase(BPNQuality.XML_QUANTITY_RESTORE)){
                                bpnQuality.setQuantity(new Converter(textValue).StrToDouble());
                            }
                        }else if(tableName.equalsIgnoreCase(SPBSHeader.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(SPBSHeader.XML_ITEM_RESTORE)){
                                spbsHeader.setStatus(status);
                                spbsHeader.setIsSave(isSave);

                                SPBSHeader spbs = (SPBSHeader) database.getDataFirst(false, SPBSHeader.TABLE_NAME, null,
                                        SPBSHeader.XML_IMEI + "=?" + " and " +
                                                SPBSHeader.XML_YEAR + "=?" + " and " +
                                                SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                SPBSHeader.XML_ESTATE + "=?" + " and " +
                                                SPBSHeader.XML_CROP + "=?" + " and " +
                                                SPBSHeader.XML_SPBS_NUMBER + "=?",
                                        new String [] {spbsHeader.getImei(), spbsHeader.getYear(), spbsHeader.getCompanyCode(), spbsHeader.getEstate(),
                                                spbsHeader.getCrop(), spbsHeader.getSpbsNumber()},
                                        null, null, null, null);

                                if(spbs != null){
                                    database.updateData(spbsHeader,
                                            SPBSHeader.XML_IMEI + "=?" + " and " +
                                                    SPBSHeader.XML_YEAR + "=?" + " and " +
                                                    SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                    SPBSHeader.XML_ESTATE + "=?" + " and " +
                                                    SPBSHeader.XML_CROP + "=?" + " and " +
                                                    SPBSHeader.XML_SPBS_NUMBER + "=?",
                                            new String [] {spbsHeader.getImei(), spbsHeader.getYear(), spbsHeader.getCompanyCode(), spbsHeader.getEstate(),
                                                    spbsHeader.getCrop(), spbsHeader.getSpbsNumber()});
                                }else{
                                    database.setData(spbsHeader);
                                }
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_IMEI_RESTORE)){
                                spbsHeader.setImei(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_YEAR_RESTORE)){
                                spbsHeader.setYear(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_COMPANY_CODE_RESTORE)){
                                spbsHeader.setCompanyCode(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_ESTATE_RESTORE)){
                                spbsHeader.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_CROP_RESTORE)){
                                spbsHeader.setCrop(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_SPBS_NUMBER_RESTORE)){
                                spbsHeader.setSpbsNumber(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_SPBS_DATE_RESTORE)){
                                spbsHeader.setSpbsDate(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_DIVISION_RESTORE)){
                                spbsHeader.setDivision(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_NIK_ASSISTANT_RESTORE)){
                                spbsHeader.setNikAssistant(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_NIK_CLERK_RESTORE)){
                                spbsHeader.setNikClerk(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_NIK_DRIVER_RESTORE)){
                                spbsHeader.setNikDriver(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_DRIVER_RESTORE)){
                                spbsHeader.setDriver(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_RUNNING_ACCOUNT_RESTORE)){
                                spbsHeader.setRunningAccount(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_LICENSE_PLATE_RESTORE)){
                                spbsHeader.setLicensePlate(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_CREATED_DATE_RESTORE)){
                                spbsHeader.setCreatedDate(todayDate);
                            }else if(tagName.equalsIgnoreCase(SPBSHeader.XML_CREATED_BY_RESTORE)){
                                spbsHeader.setCreatedBy(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(SPBSLine.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(SPBSLine.XML_ITEM_RESTORE)){
                                spbsLine.setImei(spbsHeader.getImei());
                                spbsLine.setYear(spbsHeader.getYear());
                                spbsLine.setCompanyCode(spbsHeader.getCompanyCode());
                                spbsLine.setEstate(spbsHeader.getEstate());
                                spbsLine.setCrop(spbsHeader.getCrop());
                                spbsLine.setSpbsNumber(spbsHeader.getSpbsNumber());
                                spbsLine.setSpbsDate(spbsHeader.getSpbsDate());
                                spbsLine.setStatus(status);
                                spbsLine.setIsSave(isSave);
                                spbsLine.setCreatedDate(spbsHeader.getCreatedDate());
                                spbsLine.setCreatedBy(spbsHeader.getCreatedBy());

                                SPBSLine spbs = (SPBSLine) database.getDataFirst(false, SPBSLine.TABLE_NAME, null,
                                        SPBSLine.XML_IMEI + "=?" + " and " +
                                                SPBSLine.XML_YEAR + "=?" + " and " +
                                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                                SPBSLine.XML_CROP + "=?" + " and " +
                                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                                SPBSLine.XML_TPH + "=?" + " and " +
                                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                        new String [] {spbsLine.getImei(), spbsLine.getYear(), spbsLine.getCompanyCode(), spbsLine.getEstate(),
                                                spbsLine.getCrop(), spbsLine.getSpbsNumber(), spbsLine.getSpbsDate(), spbsLine.getBlock(), spbsLine.getTph(),
                                                spbsLine.getBpnDate(), spbsLine.getAchievementCode()},
                                        null, null, null, null);

                                if(spbs != null){
                                    database.updateData(spbsLine,
                                            SPBSLine.XML_IMEI + "=?" + " and " +
                                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                                    SPBSLine.XML_CROP + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                                    SPBSLine.XML_TPH + "=?" + " and " +
                                                    SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                            new String [] {spbsLine.getImei(), spbsLine.getYear(), spbsLine.getCompanyCode(), spbsLine.getEstate(),
                                                    spbsLine.getCrop(), spbsLine.getSpbsNumber(), spbsLine.getSpbsDate(), spbsLine.getBlock(), spbsLine.getTph(),
                                                    spbsLine.getBpnDate(), spbsLine.getAchievementCode()});
                                }else{
                                    database.setData(spbsLine);
                                }
                            }else if(tagName.equalsIgnoreCase(SPBSLine.XML_BLOCK_RESTORE)){
                                spbsLine.setBlock(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSLine.XML_TPH_RESTORE)){
                                spbsLine.setTph(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSLine.XML_GPS_KOORDINAT_RESTORE)){
                                spbsLine.setGpsKoordinat(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSLine.XML_ACHIEVEMENT_CODE_RESTORE)){
                                spbsLine.setAchievementCode(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSLine.XML_BPN_DATE_RESTORE)){
                                spbsLine.setBpnDate(new DateLocal(textValue, DateLocal.FORMAT_RESTORE).getDateString(DateLocal.FORMAT_INPUT));
                            }else if(tagName.equalsIgnoreCase(SPBSLine.XML_TOTAL_HARVESTER_RESTORE)){
                                spbsLine.setTotalHarvester(new Converter(textValue).StrToInt());
                            }else if(tagName.equalsIgnoreCase(SPBSLine.XML_QUANTITY_RESTORE)){
                                spbsLine.setQuantity(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(SPBSLine.XML_UOM_RESTORE)){
                                spbsLine.setUom(textValue);
                            }else if(tagName.equalsIgnoreCase(SPBSLine.XML_CREATED_DATE_RESTORE)){
                                spbsLine.setCreatedDate(todayDate);
                            }else if(tagName.equalsIgnoreCase(SPBSLine.XML_CREATED_BY_RESTORE)){
                                spbsLine.setCreatedBy(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(TaksasiHeader.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(TaksasiHeader.XML_ITEM_RESTORE)){
                                taksasiHeader.setStatus(status);
                                taksasiHeader.setIsSave(isSave);

                                TaksasiHeader taksasi = (TaksasiHeader) database.getDataFirst(false, TaksasiHeader.TABLE_NAME, null,
                                        TaksasiHeader.XML_IMEI + "=?" + " and " +
                                                TaksasiHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                TaksasiHeader.XML_ESTATE + "=?" + " and " +
                                                TaksasiHeader.XML_DIVISION + "=?" + " and " +
                                                TaksasiHeader.XML_TAKSASI_DATE + "=?" + " and " +
                                                TaksasiHeader.XML_BLOCK + "=?" + " and " +
                                                TaksasiHeader.XML_CROP + "=?",
                                        new String [] {taksasiHeader.getImei(), taksasiHeader.getCompanyCode(), taksasiHeader.getEstate(), taksasiHeader.getDivision(),
                                                taksasiHeader.getTaksasiDate(), taksasiHeader.getBlock(), taksasiHeader.getCrop()},
                                        null, null, null, null);

                                if(taksasi != null){
                                    database.updateData(taksasiHeader,
                                            TaksasiHeader.XML_IMEI + "=?" + " and " +
                                                    TaksasiHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                    TaksasiHeader.XML_ESTATE + "=?" + " and " +
                                                    TaksasiHeader.XML_DIVISION + "=?" + " and " +
                                                    TaksasiHeader.XML_TAKSASI_DATE + "=?" + " and " +
                                                    TaksasiHeader.XML_BLOCK + "=?" + " and " +
                                                    TaksasiHeader.XML_CROP + "=?",
                                            new String [] {taksasiHeader.getImei(), taksasiHeader.getCompanyCode(), taksasiHeader.getEstate(), taksasiHeader.getDivision(),
                                                    taksasiHeader.getTaksasiDate(), taksasiHeader.getBlock(), taksasiHeader.getCrop()});
                                }else{
                                    database.setData(taksasiHeader);
                                }
                            }else if(tagName.equalsIgnoreCase(TaksasiHeader.XML_IMEI_RESTORE)){
                                taksasiHeader.setImei(textValue);
                            }else if(tagName.equalsIgnoreCase(TaksasiHeader.XML_ESTATE_RESTORE)){
                                taksasiHeader.setCompanyCode(textValue.substring(0, 2) + "00");
                                taksasiHeader.setEstate(textValue);
                            }else if(tagName.equalsIgnoreCase(TaksasiHeader.XML_DIVISION_RESTORE)){
                                taksasiHeader.setDivision(textValue);
                            }else if(tagName.equalsIgnoreCase(TaksasiHeader.XML_TAKSASI_DATE_RESTORE)){
                                taksasiHeader.setTaksasiDate(new DateLocal(textValue, DateLocal.FORMAT_RESTORE).getDateString(DateLocal.FORMAT_INPUT));
                            }else if(tagName.equalsIgnoreCase(TaksasiHeader.XML_BLOCK_RESTORE)){
                                taksasiHeader.setBlock(textValue);
                            }else if(tagName.equalsIgnoreCase(TaksasiHeader.XML_CROP_RESTORE)){
                                taksasiHeader.setCrop(textValue);
                            }else if(tagName.equalsIgnoreCase(TaksasiHeader.XML_NIK_FOREMAN_RESTORE)){
                                taksasiHeader.setNikForeman(textValue);
                            }else if(tagName.equalsIgnoreCase(TaksasiHeader.XML_FOREMAN_RESTORE)){
                                taksasiHeader.setForeman(textValue);
                            }else if(tagName.equalsIgnoreCase(TaksasiHeader.XML_BJR_RESTORE)){
                                taksasiHeader.setBjr(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(TaksasiHeader.XML_PROD_TREES_RESTORE)){
                                taksasiHeader.setProdTrees(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(TaksasiHeader.XML_GPS_KOORDINAT_RESTORE)){
                                taksasiHeader.setGpsKoordinat(textValue);
                            }else if(tagName.equalsIgnoreCase(TaksasiHeader.XML_CREATED_DATE_RESTORE)){
                                taksasiHeader.setCreatedDate(todayDate);
                            }else if(tagName.equalsIgnoreCase(TaksasiHeader.XML_CREATED_BY_RESTORE)){
                                taksasiHeader.setCreatedBy(textValue);
                            }
                        }else if(tableName.equalsIgnoreCase(TaksasiLine.TABLE_NAME)){
                            if(tagName.equalsIgnoreCase(TaksasiLine.XML_ITEM_RESTORE)){
                                taksasiLine.setImei(taksasiHeader.getImei());
                                taksasiLine.setCompanyCode(taksasiHeader.getEstate().substring(0, 2) + "00");
                                taksasiLine.setEstate(taksasiHeader.getEstate());
                                taksasiLine.setDivision(taksasiHeader.getDivision());
                                taksasiLine.setTaksasiDate(taksasiHeader.getTaksasiDate());
                                taksasiLine.setBlock(taksasiHeader.getBlock());
                                taksasiLine.setCrop(taksasiHeader.getCrop());
                                taksasiLine.setStatus(status);
                                taksasiLine.setIsSave(isSave);
                                taksasiLine.setCreatedDate(taksasiHeader.getCreatedDate());
                                taksasiLine.setCreatedBy(taksasiHeader.getCreatedBy());

                                TaksasiLine taksasi = (TaksasiLine) database.getDataFirst(false, TaksasiLine.TABLE_NAME, null,
                                        TaksasiLine.XML_IMEI + "=?" + " and " +
                                                TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
                                                TaksasiLine.XML_ESTATE + "=?" + " and " +
                                                TaksasiLine.XML_DIVISION + "=?" + " and " +
                                                TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
                                                TaksasiLine.XML_BLOCK + "=?" + " and " +
                                                TaksasiLine.XML_CROP + "=?" + " and " +
                                                TaksasiLine.XML_BARIS_SKB + "=?" + " and " +
                                                TaksasiLine.XML_BARIS_BLOCK + "=?" + " and " +
                                                TaksasiLine.XML_LINE_SKB + "=?",
                                        new String [] {taksasiLine.getImei(), taksasiLine.getCompanyCode(), taksasiLine.getEstate(), taksasiLine.getDivision(),
                                                taksasiLine.getTaksasiDate(), taksasiLine.getBlock(), taksasiLine.getCrop(), String.valueOf(taksasiLine.getBarisSkb()), String.valueOf(taksasiLine.getBarisBlock()),
                                                String.valueOf(taksasiLine.getLineSkb())},
                                        null, null, null, null);

                                if(taksasi != null){
                                    database.updateData(taksasiLine,
                                            TaksasiLine.XML_IMEI + "=?" + " and " +
                                                    TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    TaksasiLine.XML_ESTATE + "=?" + " and " +
                                                    TaksasiLine.XML_DIVISION + "=?" + " and " +
                                                    TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
                                                    TaksasiLine.XML_BLOCK + "=?" + " and " +
                                                    TaksasiLine.XML_CROP + "=?" + " and " +
                                                    TaksasiLine.XML_BARIS_SKB + "=?" + " and " +
                                                    TaksasiLine.XML_BARIS_BLOCK + "=?" + " and " +
                                                    TaksasiLine.XML_LINE_SKB + "=?",
                                            new String [] {taksasiLine.getImei(), taksasiLine.getCompanyCode(), taksasiLine.getEstate(), taksasiLine.getDivision(),
                                                    taksasiLine.getTaksasiDate(), taksasiLine.getBlock(), taksasiLine.getCrop(), String.valueOf(taksasiLine.getBarisSkb()), String.valueOf(taksasiLine.getBarisBlock()),
                                                    String.valueOf(taksasiLine.getLineSkb())});
                                }else{
                                    database.setData(taksasiLine);
                                }
                            }else if(tagName.equalsIgnoreCase(TaksasiLine.XML_BARIS_BLOCK_RESTORE)){
                                taksasiLine.setBarisBlock(new Converter(textValue).StrToInt());
                            }else if(tagName.equalsIgnoreCase(TaksasiLine.XML_BARIS_SKB_RESTORE)){
                                taksasiLine.setBarisSkb(new Converter(textValue).StrToInt());
                            }else if(tagName.equalsIgnoreCase(TaksasiLine.XML_LINE_SKB_RESTORE)){
                                taksasiLine.setLineSkb(new Converter(textValue).StrToInt());
                            }else if(tagName.equalsIgnoreCase(TaksasiLine.XML_QTY_POKOK_RESTORE)){
                                taksasiLine.setQtyPokok(new Converter(textValue).StrToDouble());
                            }else if(tagName.equalsIgnoreCase(TaksasiLine.XML_QTY_JANJANG_RESTORE)){
                                taksasiLine.setQtyJanjang(new Converter(textValue).StrToInt());
                            }
                        }
                    default:
                        break;
                }
                eventType = parser.next();
            }

            database.commitTransaction();
            success = true;
            msgStatus.setMessage(context.getResources().getString(R.string.import_successed));
            msgStatus.setStatus(success);
        }catch(XmlPullParserException e){
            e.printStackTrace();
            database.closeTransaction();
            msgStatus.setMessage(e.getMessage());
            msgStatus.setStatus(success);
        }catch(IOException e){
            e.printStackTrace();
            database.closeTransaction();
            msgStatus.setMessage(e.getMessage());
            msgStatus.setStatus(success);
        }catch (SQLiteException e) {
            e.printStackTrace();
            database.closeTransaction();
            msgStatus.setMessage(e.getMessage());
            msgStatus.setStatus(success);
        }finally{
            database.closeTransaction();
        }

        return msgStatus;
    }

    private MessageStatus saveXml(String xml, String fileName, String type, boolean isUsbOtgSave){
//		String xml = writer.toString();
        String folder = null;
//		String fileName = "M2NBPN" + "_" + estate + "_" + division + "_" + new DateLocal(new Date()).getDateString(DateLocal.FORMAT_FILE) + "_" + rowCount + ".xml";
        MessageStatus msgStatus;

        if(isUsbOtgSave){
//			UsbDevice usbDevice;

            UsbManager manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
            HashMap<String, UsbDevice> mapUsb = manager.getDeviceList();

            String[] usbKey = mapUsb.keySet().toArray(new String[mapUsb.keySet().size()]);

            if(usbKey.length > 0){
//				folder = new File("/storage/UsbDriveA", "Android/data/com.simp.hms/files/Documents/" + FolderHandler.REPORT + "/" + FolderHandler.NEW).getAbsolutePath();
                folder = new File("/storage/UsbDriveA", FolderHandler.ROOT + "/" + FolderHandler.REPORT + "/" + FolderHandler.NEW).getAbsolutePath();

                File folderFile = new File(folder);

                if(!folderFile.exists()){
                    folderFile.mkdirs();
                }

            }


        }else{
            FolderHandler folderHandler = new FolderHandler(context);

            if(folderHandler.init()){
                folder = folderHandler.getFileReportNew();
            }
        }

        if(!TextUtils.isEmpty(folder)){
            if(new FileXMLHandler(context).createFile(folder, fileName, xml)){
                msgStatus = new MessageStatus(type, true, context.getResources().getString(R.string.export_successed));
            }else{
                msgStatus = new MessageStatus(type, false, context.getResources().getString(R.string.export_failed));
            }
        }else{
            msgStatus = new MessageStatus(type, false, context.getResources().getString(R.string.error_sd_card));
        }

        return msgStatus;
    }

    public String Set(Object object) {
        String value = null;
        if(object==null){
            value = "0";
        }
        else{
            if(!object.toString().contains(".")) {
                value = object.toString();
            }
            if(object.toString().contains(".")){
                value = object.toString().replaceAll(".?0*$", "");
            }

        }
        return value;
    }

}