package co.indoagri.blockcondition.widget;


import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Locale;

import co.indoagri.blockcondition.R;

public class NewDatePickerCustom extends LinearLayout{

    private int startYear = 1900;
    private int endYear = 9999;

    private View myPickerView;

    private ImageButton month_plus;
    private TextView month_display;
    private ImageButton month_minus;

    private ImageButton date_plus;
    private TextView date_display;
    private ImageButton date_minus;

    private ImageButton year_plus;
    private TextView year_display;
    private ImageButton year_minus;

    private Calendar cal;

    public NewDatePickerCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context mContext) {
        LayoutInflater inflator = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        myPickerView = inflator.inflate(R.layout.new_widget_datepicker, null);
        this.addView(myPickerView);

        initializeReference();
    }

    private void initializeReference() {

        month_plus = (ImageButton) myPickerView.findViewById(R.id.btn_month_plus);
        month_plus.setOnClickListener(month_plus_listener);
        month_display = (TextView) myPickerView
                .findViewById(R.id.txt_month_display);
        month_minus = (ImageButton) myPickerView.findViewById(R.id.btn_month_minus);
        month_minus.setOnClickListener(month_minus_listener);

        date_plus = (ImageButton) myPickerView.findViewById(R.id.btn_date_plus);
        date_plus.setOnClickListener(date_plus_listener);
        date_display = (TextView) myPickerView.findViewById(R.id.txt_date_display);
//		date_display.addTextChangedListener(date_watcher);
        date_minus = (ImageButton) myPickerView.findViewById(R.id.btn_date_minus);
        date_minus.setOnClickListener(date_minus_listener);

        year_plus = (ImageButton) myPickerView.findViewById(R.id.btn_year_plus);
        year_plus.setOnClickListener(year_plus_listener);
        year_display = (TextView) myPickerView.findViewById(R.id.txt_year_display);
        year_display.setOnFocusChangeListener(mLostFocusYear);
        year_display.addTextChangedListener(year_watcher);
        year_minus = (ImageButton) myPickerView.findViewById(R.id.btn_year_minus);
        year_minus.setOnClickListener(year_minus_listener);

        initData();
        initFilterNumericDigit();

    }

    private void initData() {
        cal = Calendar.getInstance(Locale.getDefault());
        month_display.setText(months[cal.get(Calendar.MONTH)]);
        date_display.setText(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
        year_display.setText(String.valueOf(cal.get(Calendar.YEAR)));
    }

    private void initFilterNumericDigit() {

        try {
//			date_display.setFilters(new InputFilter[] { new InputFilterMinMax(
//					1, cal.getActualMaximum(Calendar.DAY_OF_MONTH)) });

            InputFilter[] filterArray_year = new InputFilter[1];
            filterArray_year[0] = new InputFilter.LengthFilter(4);
            year_display.setFilters(filterArray_year);
        } catch (Exception e) {
//			date_display.setText("" + cal.get(Calendar.DAY_OF_MONTH));
            e.printStackTrace();
        }
    }

    private void changeFilter() {
        try {
//			date_display.setFilters(new InputFilter[] { new InputFilterMinMax(
//					1, cal.getActualMaximum(Calendar.DAY_OF_MONTH)) });
        } catch (Exception e) {
//			date_display.setText("" + cal.get(Calendar.DAY_OF_MONTH));
            e.printStackTrace();
        }
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) throws Exception {
        if (startYear < this.endYear && startYear > this.startYear) {
            this.startYear = startYear;
            swapStartEndYear();
        } else {
            throw new NumberFormatException(
                    "StartYear should be in the range of 1900 to 9999");
        }
    }

    public void reset() {
        initData();
    }

    public int getEndYear() {
        return endYear;
    }

    public void setDateChangedListener(DateWatcher listener) {
        this.mDateWatcher = listener;
    }

    public void removeDateChangedListener() {
        this.mDateWatcher = null;
    }

    public void setEndYear(int endYear) throws Exception {
        if (endYear < this.endYear && endYear > this.startYear) {
            this.endYear = endYear;
            swapStartEndYear();
        } else {
            throw new NumberFormatException(
                    "endYear should be in the range of 1900 to 9999");
        }
    }

    private String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec" };

    OnClickListener month_plus_listener = new OnClickListener() {

        public void onClick(View v) {

            try {
                cal.add(Calendar.MONTH, 1);

//				month_display.setText(months[cal.get(Calendar.MONTH)]);
//				year_display.setText(String.valueOf(cal.get(Calendar.YEAR)));
//				date_display.setText(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));

                setDisplay(cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));

                changeFilter();
                sendToListener();
            } catch (Exception e) {
                Log.e("", e.toString());
            }
        }
    };

    OnClickListener month_minus_listener = new OnClickListener() {

        public void onClick(View v) {
            try {
                cal.add(Calendar.MONTH, -1);

//				month_display.setText(months[cal.get(Calendar.MONTH)]);
//				year_display.setText(String.valueOf(cal.get(Calendar.YEAR)));
//				date_display.setText(String.valueOf(cal
//						.get(Calendar.DAY_OF_MONTH)));

                setDisplay(cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));

                changeFilter();
                sendToListener();
            } catch (Exception e) {
                Log.e("", e.toString());
            }
        }
    };

    OnClickListener date_plus_listener = new OnClickListener() {

        public void onClick(View v) {

            try {
                date_display.requestFocus();
                cal.add(Calendar.DAY_OF_MONTH, 1);

//				month_display.setText(months[cal.get(Calendar.MONTH)]);
//				year_display.setText(String.valueOf(cal.get(Calendar.YEAR)));
//				date_display.setText(String.valueOf(cal
//						.get(Calendar.DAY_OF_MONTH)));

                setDisplay(cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));

                sendToListener();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    };

    OnClickListener date_minus_listener = new OnClickListener() {

        public void onClick(View v) {

            try {
                date_display.requestFocus();
                cal.add(Calendar.DAY_OF_MONTH, -1);

//				month_display.setText(months[cal.get(Calendar.MONTH)]);
//				year_display.setText(String.valueOf(cal.get(Calendar.YEAR)));
//				date_display.setText(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));

                setDisplay(cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));

                sendToListener();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    };

    OnClickListener year_plus_listener = new OnClickListener() {

        public void onClick(View v) {

            try {
                year_display.requestFocus();

                if (cal.get(Calendar.YEAR) >= endYear) {

                    cal.set(Calendar.YEAR, startYear);

                } else {
                    cal.add(Calendar.YEAR, +1);

                }

//				month_display.setText(months[cal.get(Calendar.MONTH)]);
//				year_display.setText(String.valueOf(cal.get(Calendar.YEAR)));
//				date_display.setText(String.valueOf(cal
//						.get(Calendar.DAY_OF_MONTH)));

                setDisplay(cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));

                changeFilter();
                sendToListener();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    };

    OnClickListener year_minus_listener = new OnClickListener() {

        public void onClick(View v) {

            try {
                year_display.requestFocus();

                if (cal.get(Calendar.YEAR) <= startYear) {
                    cal.set(Calendar.YEAR, endYear);

                } else {
                    cal.add(Calendar.YEAR, -1);

                }

//				month_display.setText(months[cal.get(Calendar.MONTH)]);
//				year_display.setText(String.valueOf(cal.get(Calendar.YEAR)));
//				date_display.setText(String.valueOf(cal
//						.get(Calendar.DAY_OF_MONTH)));

                setDisplay(cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));

                changeFilter();
                sendToListener();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    };

    OnFocusChangeListener mLostFocusYear = new OnFocusChangeListener() {

        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {

                year_display.setText(String.valueOf(cal.get(Calendar.YEAR)));
            }
        }
    };

    class InputFilterMinMax implements InputFilter {

        private int min, max;

        public InputFilterMinMax(int min, int max) {
            this.min = min;
            this.max = max;
        }

        public InputFilterMinMax(String min, String max) {
            this.min = Integer.parseInt(min);
            this.max = Integer.parseInt(max);
        }

        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            try {
                int input = Integer.parseInt(dest.toString()
                        + source.toString());
                if (isInRange(min, max, input)) {
                    return null;
                }
            } catch (NumberFormatException nfe) {
            }
            return "";
        }

        private boolean isInRange(int a, int b, int c) {
            return b > a ? c >= a && c <= b : c >= b && c <= a;
        }
    }

    TextWatcher date_watcher = new TextWatcher() {

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        public void afterTextChanged(Editable s) {

            try {
                if (s.toString().length() > 0) {
                    // Log.e("", "afterTextChanged : " + s.toString());
                    cal.set(Calendar.DAY_OF_MONTH,
                            Integer.parseInt(s.toString()));

                    month_display.setText(months[cal.get(Calendar.MONTH)]);

                    sendToListener();
                }
            } catch (NumberFormatException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };

    TextWatcher year_watcher = new TextWatcher() {

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        public void afterTextChanged(Editable s) {
            try {
                if (s.toString().length() == 4) {
                    int year = Integer.parseInt(s.toString());

                    if (year > endYear) {
                        cal.set(Calendar.YEAR, endYear);
                    } else if (year < startYear) {
                        cal.set(Calendar.YEAR, startYear);
                    } else {
                        cal.set(Calendar.YEAR, year);
                    }
                }

                sendToListener();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    };

    private void sendToListener() {

        if (mDateWatcher != null) {
            mDateWatcher.onDateChanged(cal);
        }

    }

    DateWatcher mDateWatcher = null;

    public interface DateWatcher {
        void onDateChanged(Calendar c);
    }

    private void swapStartEndYear() {
        if (this.startYear > this.endYear) {
            int temp = endYear;
            endYear = startYear;
            startYear = temp;
        }

        cal.set(Calendar.YEAR, endYear);
        initDisplay();

    }

    private void initDisplay() {
        month_display.setText(months[cal.get(Calendar.MONTH)]);
        date_display.setText(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
        year_display.setText(String.valueOf(cal.get(Calendar.YEAR)));

    }

    public void setDisplay(int vDate, int vMonth, int vYear){

        Log.d("date", String.valueOf(vDate));
        Log.d("month", String.valueOf(vMonth));
        Log.d("year", String.valueOf(vYear));

        date_display.setText(String.valueOf(vDate));
        month_display.setText(months[vMonth]);
        year_display.setText(String.valueOf(vYear));
    }

    public String getDate(){
        return date_display.getText().toString() + "/" + month_display.getText().toString() + "/" + year_display.getText().toString();
    }

    private int monthIndex(String vMonth){
        int vIndex = 1;

        for(int i = 0; i < months.length; i++){
            if(vMonth.equals(months[i])){
                vIndex =  i + 1;
                break;
            }
        }

        return vIndex;
    }
}
