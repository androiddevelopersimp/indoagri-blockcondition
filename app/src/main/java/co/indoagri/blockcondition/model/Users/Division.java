package co.indoagri.blockcondition.model.Users;

public class Division {
    private String code;

    public static final String XML_DIVISION = "DIVISION";

    public Division(String code){
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
