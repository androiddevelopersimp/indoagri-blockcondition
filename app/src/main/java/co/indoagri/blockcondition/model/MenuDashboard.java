package co.indoagri.blockcondition.model;

import android.app.Activity;

public class MenuDashboard {
    public String text;
    public int drawable;
    public String color;
    public boolean active;
    public String ActivityClass;

    public MenuDashboard(String t, int d, String c, boolean act,String cls )
    {
        text=t;
        drawable=d;
        color=c;
        active = act;
        ActivityClass = cls;
    }
}
