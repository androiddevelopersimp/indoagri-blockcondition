package co.indoagri.blockcondition.model.Data;

import org.w3c.dom.Text;

public class tblM_AccountingPeriod {
    private long rowId = 0;
    private String acc_Estate = "";
    private String acc_ZYear = "";
    private String acc_Period = "";
    private String acc_ClosingDate = "";
    private String acc_Status = "";
    private String acc_Active = "";
    private String acc_CreatedBy = "";
    private String acc_CreatedDate = "";
    private String acc_ModifiedBy = "";
    private String acc_ModifiedDate = "";

    public static final String TABLE_NAME = "tblM_AccountingPeriod";
    public static final String ALIAS = "ACCOUNTING PERIOD MASTER";
    public static final String XML_DOCUMENT = "IT_ACPRD";
    public static final String XML_ITEM = "ITEM";
    public static final String XML_Estate = "Estate";
    public static final String XML_ZYear = "ZYear";
    public static final String XML_Period = "Periode";
    public static final String XML_ClosingDate = "ClosingDate";
    public static final String XML_Status = "Status";
    public static final String XML_Active = "Active";
    public static final String XML_CreatedBy = "CreatedBy";
    public static final String XML_CreatedDate = "CreatedDate";
    public static final String XML_ModifiedBy = "ModifiedBy";
    public static final String XML_ModifiedDate = "ModifiedDate";
    public static final String XML_RowVersion = "RowVersion";

    public tblM_AccountingPeriod(){

    }
    public tblM_AccountingPeriod(long rowId,String acc_Estate, String acc_ZYear,
                                 String acc_Period, String acc_ClosingDate,
                                 String acc_Status, String acc_Active,
                                 String acc_CreatedBy, String acc_CreatedDate,
                                 String acc_ModifiedBy, String acc_ModifiedDate) {
        this.rowId = rowId;
        this.acc_Estate = acc_Estate;
        this.acc_ZYear = acc_ZYear;
        this.acc_Period = acc_Period;
        this.acc_ClosingDate = acc_ClosingDate;
        this.acc_Status = acc_Status;
        this.acc_Active = acc_Active;
        this.acc_CreatedBy = acc_CreatedBy;
        this.acc_CreatedDate = acc_CreatedDate;
        this.acc_ModifiedBy = acc_ModifiedBy;
        this.acc_ModifiedDate = acc_ModifiedDate;
    }

    public long getRowId() {
        return rowId;
    }

    public void setRowId(long rowId) {
        this.rowId = rowId;
    }

    public String getAcc_Estate() {
        return acc_Estate;
    }

    public void setAcc_Estate(String acc_Estate) {
        this.acc_Estate = acc_Estate;
    }

    public String getAcc_ZYear() {
        return acc_ZYear;
    }

    public void setAcc_ZYear(String acc_ZYear) {
        this.acc_ZYear = acc_ZYear;
    }

    public String getAcc_Period() {
        return acc_Period;
    }

    public void setAcc_Period(String acc_Period) {
        this.acc_Period = acc_Period;
    }

    public String getAcc_ClosingDate() {
        return acc_ClosingDate;
    }

    public void setAcc_ClosingDate(String acc_ClosingDate) {
        this.acc_ClosingDate = acc_ClosingDate;
    }

    public String getAcc_Status() {
        return acc_Status;
    }

    public void setAcc_Status(String acc_Status) {
        this.acc_Status = acc_Status;
    }

    public String getAcc_Active() {
        return acc_Active;
    }

    public void setAcc_Active(String acc_Active) {
        this.acc_Active = acc_Active;
    }

    public String getAcc_CreatedBy() {
        return acc_CreatedBy;
    }

    public void setAcc_CreatedBy(String acc_CreatedBy) {
        this.acc_CreatedBy = acc_CreatedBy;
    }

    public String getAcc_CreatedDate() {
        return acc_CreatedDate;
    }

    public void setAcc_CreatedDate(String acc_CreatedDate) {
        this.acc_CreatedDate = acc_CreatedDate;
    }

    public String getAcc_ModifiedBy() {
        return acc_ModifiedBy;
    }

    public void setAcc_ModifiedBy(String acc_ModifiedBy) {
        this.acc_ModifiedBy = acc_ModifiedBy;
    }

    public String getAcc_ModifiedDate() {
        return acc_ModifiedDate;
    }

    public void setAcc_ModifiedDate(String acc_ModifiedDate) {
        this.acc_ModifiedDate = acc_ModifiedDate;
    }
}
