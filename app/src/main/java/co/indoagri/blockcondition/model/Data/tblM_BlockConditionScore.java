package co.indoagri.blockcondition.model.Data;

public class tblM_BlockConditionScore {
    private String acc_ScoreType = "";
    private String acc_ValidFrom = "";
    private String acc_ValidTo = "";
    private String acc_Color = "";
    private double acc_Interval1 = 0;
    private double acc_Interval2 = 0;
    private double acc_Score1 = 0;
    private double acc_Score2 = 0;
    private double acc_Score3 = 0;

    public static final String TABLE_NAME = "tblM_BlockConditionScore";
    public static final String ALIAS = "CONDITION SCORE MASTER";
    public static final String XML_DOCUMENT = "IT_BLOCK_CONDITION_MASTER";
    public static final String XML_ScoreType = "ScoreType";
    public static final String XML_ValidFrom = "ValidFrom";
    public static final String XML_ValidTo = "ValidTo";
    public static final String XML_Color = "Color";
    public static final String XML_Interval1 = "Interval1";
    public static final String XML_Interval2 = "Interval2";
    public static final String XML_Score1 = "Score1";
    public static final String XML_Score2 = "Score2";
    public static final String XML_Score3 = "Score3";

    public tblM_BlockConditionScore(){

    }
    public tblM_BlockConditionScore(String acc_ScoreType,
                                    String acc_ValidFrom, String acc_ValidTo,String acc_Color,
                                    double acc_Interval1, double acc_Interval2,
                                    double acc_Score1, double acc_Score2,
                                    double acc_Score3) {
        this.acc_ScoreType = acc_ScoreType;
        this.acc_ValidFrom = acc_ValidFrom;
        this.acc_ValidTo = acc_ValidTo;
        this.acc_Color = acc_Color;
        this.acc_Interval1 = acc_Interval1;
        this.acc_Interval2 = acc_Interval2;
        this.acc_Score1 = acc_Score1;
        this.acc_Score2 = acc_Score2;
        this.acc_Score3 = acc_Score3;
    }

    public String getAcc_ValidTo() {
        return acc_ValidTo;
    }

    public void setAcc_ValidTo(String acc_ValidTo) {
        this.acc_ValidTo = acc_ValidTo;
    }

    public String getAcc_ScoreType() {
        return acc_ScoreType;
    }

    public void setAcc_ScoreType(String acc_ScoreType) {
        this.acc_ScoreType = acc_ScoreType;
    }

    public String getAcc_ValidFrom() {
        return acc_ValidFrom;
    }

    public void setAcc_ValidFrom(String acc_ValidFrom) {
        this.acc_ValidFrom = acc_ValidFrom;
    }

    public String getAcc_Color() {
        return acc_Color;
    }

    public void setAcc_Color(String acc_Color) {
        this.acc_Color = acc_Color;
    }

    public double getAcc_Interval1() {
        return acc_Interval1;
    }

    public void setAcc_Interval1(double acc_Interval1) {
        this.acc_Interval1 = acc_Interval1;
    }

    public double getAcc_Interval2() {
        return acc_Interval2;
    }

    public void setAcc_Interval2(double acc_Interval2) {
        this.acc_Interval2 = acc_Interval2;
    }

    public double getAcc_Score1() {
        return acc_Score1;
    }

    public void setAcc_Score1(double acc_Score1) {
        this.acc_Score1 = acc_Score1;
    }

    public double getAcc_Score2() {
        return acc_Score2;
    }

    public void setAcc_Score2(double acc_Score2) {
        this.acc_Score2 = acc_Score2;
    }

    public double getAcc_Score3() {
        return acc_Score3;
    }

    public void setAcc_Score3(double acc_Score3) {
        this.acc_Score3 = acc_Score3;
    }

}
