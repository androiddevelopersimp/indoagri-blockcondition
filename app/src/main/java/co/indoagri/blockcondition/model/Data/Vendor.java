package co.indoagri.blockcondition.model.Data;

/**
 * Created by Anka.Wirawan on 8/4/2016.
 */
public class Vendor {
    public String estate;
    public String lifnr;
    public String name;

    public static final String TABLE_NAME = "VENDOR";
    public static final String ALIAS = "VENDOR";
    public static final String XML_DOCUMENT = "IT_VENDOR";
    public static final String XML_ITEM = "ITEM";
    public static final String XML_ESTATE = "ESTATE";
    public static final String XML_LIFNR = "LIFNR";
    public static final String XML_NAME = "NAME";

    public Vendor() {
    }

    public Vendor(String estate, String lifnr, String name) {
        this.estate = estate;
        this.lifnr = lifnr;
        this.name = name;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getLifnr() {
        return lifnr;
    }

    public void setLifnr(String lifnr) {
        this.lifnr = lifnr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
