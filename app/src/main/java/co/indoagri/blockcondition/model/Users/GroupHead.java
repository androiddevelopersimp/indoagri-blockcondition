package co.indoagri.blockcondition.model.Users;


public class GroupHead {
    private String companyCode;
    private String estate;
    private String lifnr;
    private String initial;
    private String validFrom;
    private String validTo;
    private String name;
    private int status;

    public static final String TABLE_NAME = "GROUP_HEAD";
    public static final String ALIAS = "GROUP HEAD";
    public static final String XML_DOCUMENT = "IT_GROUPHEAD";
    public static final String XML_ITEM = "ITEM";
    public static final String XML_COMPANY_CODE = "COMPANY_CODE";
    public static final String XML_ESTATE = "ESTATE";
    public static final String XML_LIFNR = "LIFNR";
    public static final String XML_INITIAL = "INITL";
    public static final String XML_VALID_FROM = "VALIDFROM";
    public static final String XML_VALID_TO = "VALIDTO";
    public static final String XML_NAME = "NAME";
    public static final String XML_STATUS = "STATUS";

    public GroupHead() {
    }

    public GroupHead(String companyCode, String estate, String lifnr, String initial, String validFrom,
                     String validTo, String name, int status) {
        this.companyCode = companyCode;
        this.estate = estate;
        this.lifnr = lifnr;
        this.initial = initial;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.name = name;
        this.status = status;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getLifnr() {
        return lifnr;
    }

    public void setLifnr(String lifnr) {
        this.lifnr = lifnr;
    }

    public String getInitial() {
        return initial;
    }

    public void setInitial(String initial) {
        this.initial = initial;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
