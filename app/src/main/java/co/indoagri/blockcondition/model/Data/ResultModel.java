package co.indoagri.blockcondition.model.Data;

public class ResultModel {
    private String Block;
    private String Condition;

    public ResultModel() {
    }

    public ResultModel(String block, String condition) {
        Block = block;
        Condition = condition;
    }

    public String getBlock() {
        return Block;
    }

    public void setBlock(String block) {
        Block = block;
    }

    public String getCondition() {
        return Condition;
    }

    public void setCondition(String condition) {
        Condition = condition;
    }

    @Override
    public String toString() {
        return "ResultModel{" +
                "Block='" + Block + '\'' +
                ", Condition='" + Condition + '\'' +
                '}';
    }


}
