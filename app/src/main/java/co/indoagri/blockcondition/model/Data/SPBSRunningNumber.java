package co.indoagri.blockcondition.model.Data;

public class SPBSRunningNumber {
	private long rowId = 0;
	private int id = 0; // p
	private String estate = ""; // p
	private String division = ""; // p
	private String year = ""; // p
	private String month = ""; // p
	private String imei = ""; // p
	private String runningNumber = "";
	private String deviceAlias = "";

	public static final String TABLE_NAME = "SPBS_RUNNING_NO";
	public static final String ALIAS = "SPBS RUNNING NO";
	public static final String XML_DOCUMENT = "IT_SPBS_RUNNING_NO";
	public static final String XML_ITEM = "ITEM";
	public static final String XML_ID = "ID";
	public static final String XML_ESTATE = "ESTATE";
	public static final String XML_DIVISION = "DIVISI";
	public static final String XML_YEAR = "YEAR";
	public static final String XML_MONTH = "MONTH";
	public static final String XML_IMEI = "IMEI";
	public static final String XML_RUNNING_NUMBER = "RUNNING_NO";
	public static final String XML_DEVICE_ALIAS = "DEVICE_ALIAS";

	public SPBSRunningNumber() {
	}

	public SPBSRunningNumber(long rowId, int id, String estate,
                             String division, String year, String month, String imei,
                             String runningNumber, String deviceAlias) {
		super();
		this.rowId = rowId;
		this.id = id;
		this.estate = estate;
		this.division = division;
		this.year = year;
		this.month = month;
		this.imei = imei;
		this.runningNumber = runningNumber;
		this.deviceAlias = deviceAlias;
	}

	public long getRowId() {
		return rowId;
	}

	public void setRowId(long rowId) {
		this.rowId = rowId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getRunningNumber() {
		return runningNumber;
	}

	public void setRunningNumber(String runningNumber) {
		this.runningNumber = runningNumber;
	}

	public String getDeviceAlias() {
		return deviceAlias;
	}

	public void setDeviceAlias(String deviceAlias) {
		this.deviceAlias = deviceAlias;
	}
}
