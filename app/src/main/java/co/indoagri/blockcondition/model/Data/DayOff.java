package co.indoagri.blockcondition.model.Data;

public class DayOff {
    private long rowId;
    private String estate;		//p
    private String date;		//p
    private String dayOffType;
    private String description;

    public static final String TABLE_NAME = "DAY_OFF";
    public static final String ALIAS= "DAY OFF";
    public static final String XML_DOCUMENT = "IT_OFF";
    public static final String XML_ITEM = "ITEM";
    public static final String XML_ESTATE = "ESTATE";
    public static final String XML_DATE = "ZDATE";
    public static final String XML_DAY_OFF_TYPE = "DAYOFFTYPE";
    public static final String XML_DESCRIPTION = "DESCRIPTION";

    public DayOff(){}

    public DayOff(long rowId, String estate, String date, String dayOffType,
                  String description) {
        super();
        this.rowId = rowId;
        this.estate = estate;
        this.date = date;
        this.dayOffType = dayOffType;
        this.description = description;
    }

    public long getRowId() {
        return rowId;
    }

    public void setRowId(long rowId) {
        this.rowId = rowId;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDayOffType() {
        return dayOffType;
    }

    public void setDayOffType(String dayOffType) {
        this.dayOffType = dayOffType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
