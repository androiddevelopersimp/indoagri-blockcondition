package co.indoagri.blockcondition.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateLocal {
    private Date date;

    public static final SimpleDateFormat FORMAT_VIEW = new SimpleDateFormat(
            "dd-MM-yyyy", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_INPUT = new SimpleDateFormat(
            "yyyy-MM-dd", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_INPUT_MMDDYYYY = new SimpleDateFormat(
            "MM/dd/yyyy", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_DATE_ONLY = new SimpleDateFormat(
            "yyyy-MM-dd", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_DATE_TIME_INPUT = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_FILE = new SimpleDateFormat(
            "yyyyMMdd_HHmmss", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_ID = new SimpleDateFormat(
            "yyyyMMddHHmmss", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_MASTER_XML = new SimpleDateFormat(
            "dd-MM-yyyy hh:mm:ss", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_YEAR_ONLY = new SimpleDateFormat(
            "yyyy", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_MONTH_ONLY = new SimpleDateFormat(
            "MM", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_YEAR_SPBS = new SimpleDateFormat(
            "yy", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_MONTH_SPBS = new SimpleDateFormat(
            "MM", Locale.getDefault());

    public static final SimpleDateFormat FORMAT_PRINT_SPBS = new SimpleDateFormat(
            "dd MMM yyyy", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_QR_CODE_SPBS = new SimpleDateFormat(
            "yyyyMMdd", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_RESTORE = new SimpleDateFormat(
            "yyyyMMdd", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_REPORT_VIEW = new SimpleDateFormat(
            "dd-MM-yyyy HH:mm:ss", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_REPORT_VIEW2 = new SimpleDateFormat(
            "dd/MM/yyyy HH:mm:ss", Locale.getDefault());
    public static final SimpleDateFormat FORMAT_REPORT_VIEW3 = new SimpleDateFormat(
            "MM/dd/yyyy HH:mm:ss", Locale.getDefault());


    public DateLocal(Date date) {
        this.date = date;
    }

    public DateLocal(String date, SimpleDateFormat dateFormat) {
        try {
            this.date = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            this.date = new Date();
        }
    }

    public String getDateString(SimpleDateFormat dateFormat) {
        return dateFormat.format(date);
    }

    public Date getDate() {
        return date;
    }

    public String getMonthAlphabet(){
        String monthVal = FORMAT_MONTH_SPBS.format(date);

        int month = new Converter(monthVal).StrToInt();

        switch (month) {
            case 1:
                return "A";
            case 2:
                return "B";
            case 3:
                return "C";
            case 4:
                return "D";
            case 5:
                return "E";
            case 6:
                return "F";
            case 7:
                return "G";
            case 8:
                return "H";
            case 9:
                return "I";
            case 10:
                return "J";
            case 11:
                return "K";
            case 12:
                return "L";
            default:
                return "";
        }
    }
}
