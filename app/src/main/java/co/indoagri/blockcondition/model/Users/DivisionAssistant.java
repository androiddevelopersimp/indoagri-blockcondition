package co.indoagri.blockcondition.model.Users;


public class DivisionAssistant {
    private long rowId;
    private String estate;
    private String division;
    private String spras;
    private String description;
    private String assistant;
    private String distanceToMill;
    private String uom;
    private String assistantName;
    private String lifnr;

    public static final String TABLE_NAME = "DIVISION_ASSISTANT";
    public static final String ALIAS = "DIVISION";
    public static final String XML_DOCUMENT = "IT_DIVISI";
    public static final String XML_ITEM = "ITEM";
    public static final String XML_ID = "ID";
    public static final String XML_ESTATE = "ESTATE";
    public static final String XML_DIVISION = "DIVISI";
    public static final String XML_SPRAS = "SPRAS";
    public static final String XML_DESCRIPTION = "DESCRIPTION";
    public static final String XML_ASSISTANT = "ASSISTANT";
    public static final String XML_DISTANCE_TO_MILL = "DISTANCETOMILL";
    public static final String XML_UOM = "UOM";
    public static final String XML_ASSISTANT_NAME = "ASSISTANTNAME";
    public static final String XML_LIFNR = "LIFNR";

    public DivisionAssistant() {
    }

    public DivisionAssistant(long rowId, int id, String estate, String division,
                             String spras, String description, String assistant,
                             String distanceToMill, String uom, String assistantName, String lifnr) {
        super();
        this.rowId = rowId;
        this.estate = estate;
        this.division = division;
        this.spras = spras;
        this.description = description;
        this.assistant = assistant;
        this.distanceToMill = distanceToMill;
        this.uom = uom;
        this.assistantName = assistantName;
        this.lifnr = lifnr;
    }

    public long getRowId() {
        return rowId;
    }

    public void setRowId(long rowId) {
        this.rowId = rowId;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getSpras() {
        return spras;
    }

    public void setSpras(String spras) {
        this.spras = spras;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAssistant() {
        return assistant;
    }

    public void setAssistant(String assistant) {
        this.assistant = assistant;
    }

    public String getDistanceToMill() {
        return distanceToMill;
    }

    public void setDistanceToMill(String distanceToMill) {
        this.distanceToMill = distanceToMill;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getAssistantName() {
        return assistantName;
    }

    public void setAssistantName(String assistantName) {
        this.assistantName = assistantName;
    }

    public String getLifnr() {
        return lifnr;
    }

    public void setLifnr(String lifnr) {
        this.lifnr = lifnr;
    }

}
