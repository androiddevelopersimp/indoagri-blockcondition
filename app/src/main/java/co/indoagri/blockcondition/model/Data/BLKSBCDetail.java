package co.indoagri.blockcondition.model.Data;


public class BLKSBCDetail {
    private int rowId;
    private String companyCode;			//p
    private String estate;				//p
    private String block;				//p
    private String validFrom;			//p
    private double minimumValue;
    private double maximumValue;
    private double overBasicRate;

    public static final String TABLE_NAME = "BLKBSCDetail";
    public static final String ALIAS = "BLOCK BASIC DETAIL";
    public static final String XML_DOCUMENT = "IT_BSC";
    public static final String XML_ID = "ID";
    public static final String XML_ITEM = "ITEM";
    public static final String XML_COMPANY_CODE = "COMPANY";
    public static final String XML_ESTATE = "ESTATE";
    public static final String XML_BLOCK = "BLOCK";
    public static final String XML_VALID_FROM = "VALIDFROM";
    public static final String XML_MIN_VAL = "MIN_VALUE";
    public static final String XML_MAX_VAL = "MAX_VALUE";
    public static final String XML_OVER_BASIC_RATE = "OVERBASICRATE";

    public BLKSBCDetail(){}

    public BLKSBCDetail(int rowId, String mandt, String company_code, String estate, String block, String valid_from, double minimum_value,
                        double maximum_value, double over_basic_rate) {
        this.rowId = rowId;
        this.companyCode = company_code;
        this.estate = estate;
        this.block = block;
        this.validFrom = valid_from;
        this.minimumValue = minimum_value;
        this.maximumValue = maximum_value;
        this.overBasicRate = over_basic_rate;
    }

    public int getId() {
        return rowId;
    }

    public void setId(int id) {
        this.rowId = id;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String company_code) {
        this.companyCode = company_code;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String valid_from) {
        this.validFrom = valid_from;
    }

    public double getMinimumValue() {
        return minimumValue;
    }

    public void setMinimumValue(double minimum_value) {
        this.minimumValue = minimum_value;
    }

    public double getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(double maximum_value) {
        this.maximumValue = maximum_value;
    }

    public double getOverBasicRate() {
        return overBasicRate;
    }

    public void setOverBasicRate(double over_basic_rate) {
        this.overBasicRate = over_basic_rate;
    }
}
