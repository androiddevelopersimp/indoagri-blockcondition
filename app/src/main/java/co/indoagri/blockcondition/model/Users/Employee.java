package co.indoagri.blockcondition.model.Users;


import android.os.Parcel;
import android.os.Parcelable;

public class Employee implements Parcelable {

    private int rowId;
    private String companyCode;			//p
    private String estate;				//p
    private int fiscalYear;			//p
    private int fiscalPeriod;		//p
    private String nik;					//p
    private String name;
    private String termDate;
    private String division;
    private String roleId;
    private String jobPos;
    private String gang;
    private String costCenter;
    private String empType;
    private String validFrom;
    private String harvesterCode;
    public boolean Checked;
    public boolean Expendable;
    public static final String TABLE_NAME = "EMPLOYEE";
    public static final String ALIAS = "EMPLOYEE";
    public static final String XML_DOCUMENT = "IT_EMPLOYEE";
    public static final String XML_ITEM = "ITEM";
    public static final String XML_ID = "ID";
    public static final String XML_COMPANY_CODE = "COMPANY_CODE";
    public static final String XML_ESTATE = "ESTATE";
    public static final String XML_FISCAL_YEAR = "FISCAL_YEAR";
    public static final String XML_FISCAL_PERIOD = "FISCAL_PERIOD";
    public static final String XML_NIK = "NIK";
    public static final String XML_NAME = "NAME";
    public static final String XML_TERM_DATE = "TERM_DATE";
    public static final String XML_DIVISION = "DIVISI";
    public static final String XML_ROLE_ID = "ROLE_ID";
    public static final String XML_JOB_POS = "JOBPOS";
    public static final String XML_GANG = "GANG";
    public static final String XML_COST_CENTER = "COST_CENTER";
    public static final String XML_EMP_TYPE = "EMPLOYEE_TYPE";
    public static final String XML_VALID_FROM = "VALID_FROM";
    public static final String XML_HARVESTER_CODE = "HARVESTER_CODE";

    public Employee(){}

    public Employee(int rowId, String company_code, String estate, int fiscal_year, int fiscal_period, String nik, String name,
                    String termination_date, String divisi, String role_id, String job_pos, String gang_code,
                    String cost_center, String emp_type, String valid_from, String harvesterCode) {
        this.rowId = rowId;
        this.companyCode = company_code;
        this.estate = estate;
        this.fiscalYear = fiscal_year;
        this.fiscalPeriod = fiscal_period;
        this.nik = nik;
        this.name = name;
        this.termDate = termination_date;
        this.division = divisi;
        this.roleId = role_id;
        this.jobPos = job_pos;
        this.gang = gang_code;
        this.costCenter = cost_center;
        this.empType = emp_type;
        this.validFrom = valid_from;
        this.harvesterCode = harvesterCode;
        this.Checked = false;
        this.Expendable = false;
    }

    public boolean isChecked() {
        return Checked;
    }

    public void setChecked(boolean checked) {
        Checked = checked;
    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int RowId) {
        this.rowId = RowId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String company_code) {
        this.companyCode = company_code;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public int getFiscalYear() {
        return fiscalYear;
    }

    public void setFiscalYear(int fiscal_year) {
        this.fiscalYear = fiscal_year;
    }

    public int getFiscalPeriod() {
        return fiscalPeriod;
    }

    public void setFiscalPeriod(int fiscal_period) {
        this.fiscalPeriod = fiscal_period;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTermDate() {
        return termDate;
    }

    public void setTermDate(String termination_date) {
        this.termDate = termination_date;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String role_id) {
        this.roleId = role_id;
    }

    public String getJobPos() {
        return jobPos;
    }

    public void setJobPos(String job_pos) {
        this.jobPos = job_pos;
    }

    public String getGang() {
        return gang;
    }

    public void setGang(String gang_code) {
        this.gang = gang_code;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String cost_center) {
        this.costCenter = cost_center;
    }

    public String getEmpType() {
        return empType;
    }

    public void setEmpType(String emp_type) {
        this.empType = emp_type;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String valid_from) {
        this.validFrom = valid_from;
    }

    public String getHarvesterCode() {
        return harvesterCode;
    }

    public void setHarvesterCode(String harvesterCode) {
        this.harvesterCode = harvesterCode;
    }
    public boolean isExpendable() {
        return Expendable;
    }

    public void setExpendable(boolean expendable) {
        Expendable = expendable;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.rowId);
        dest.writeString(this.companyCode);
        dest.writeString(this.estate);
        dest.writeInt(this.fiscalYear);
        dest.writeInt(this.fiscalPeriod);
        dest.writeString(this.nik);
        dest.writeString(this.name);
        dest.writeString(this.termDate);
        dest.writeString(this.division);
        dest.writeString(this.roleId);
        dest.writeString(this.jobPos);
        dest.writeString(this.gang);
        dest.writeString(this.costCenter);
        dest.writeString(this.empType);
        dest.writeString(this.validFrom);
        dest.writeString(this.harvesterCode);
        dest.writeByte(this.Checked ? (byte) 1 : (byte) 0);
        dest.writeByte(this.Expendable ? (byte) 1 : (byte) 0);
    }

    protected Employee(Parcel in) {
        this.rowId = in.readInt();
        this.companyCode = in.readString();
        this.estate = in.readString();
        this.fiscalYear = in.readInt();
        this.fiscalPeriod = in.readInt();
        this.nik = in.readString();
        this.name = in.readString();
        this.termDate = in.readString();
        this.division = in.readString();
        this.roleId = in.readString();
        this.jobPos = in.readString();
        this.gang = in.readString();
        this.costCenter = in.readString();
        this.empType = in.readString();
        this.validFrom = in.readString();
        this.harvesterCode = in.readString();
        this.Checked = in.readByte() != 0;
        this.Expendable = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Employee> CREATOR = new Parcelable.Creator<Employee>() {
        @Override
        public Employee createFromParcel(Parcel source) {
            return new Employee(source);
        }

        @Override
        public Employee[] newArray(int size) {
            return new Employee[size];
        }
    };
}
