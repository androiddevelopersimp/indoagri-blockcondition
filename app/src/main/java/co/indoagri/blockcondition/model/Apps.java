package co.indoagri.blockcondition.model;


public class Apps {
    private String version;
    private String filename;

    public static final String TAG_VERSION = "version";
    public static final String TAG_FILENAME = "filename";

    public Apps(String version, String filename) {
        super();
        this.version = version;
        this.filename = filename;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
