package co.indoagri.blockcondition.model.Data;

public class SPBSDestination {
	private String mandt = "";
	private String estate = "";
	private String destType = "";
	private String destId = "";
	private String destDesc = "";
	private int active;
	
	public static final String TABLE_NAME = "SPBS_DESTINATION";
	public static final String ALIAS = "SPBS DESTINATION";
	public static final String XML_DOCUMENT = "IT_SPBS_DEST";
	public static final String XML_ITEM = "ITEM";
	public static final String XML_MANDT = "MANDT";
	public static final String XML_ESTATE = "ESTATE";
	public static final String XML_DEST_TYPE = "DESTTYPE";
	public static final String XML_DEST_ID = "DESTID";
	public static final String XML_DEST_DESC = "DESTDESC";
	public static final String XML_ACTIVE = "ACTIVE";
	
	public SPBSDestination(){}

	public SPBSDestination(String mandt, String estate, String destType,
                           String destId, String destDesc, int active) {
		super();
		this.mandt = mandt;
		this.estate = estate;
		this.destType = destType;
		this.destId = destId;
		this.destDesc = destDesc;
		this.active = active;
	}

	public String getMandt() {
		return mandt;
	}

	public void setMandt(String mandt) {
		this.mandt = mandt;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getDestType() {
		return destType;
	}

	public void setDestType(String destType) {
		this.destType = destType;
	}

	public String getDestId() {
		return destId;
	}

	public void setDestId(String destId) {
		this.destId = destId;
	}

	public String getDestDesc() {
		return destDesc;
	}

	public void setDestDesc(String destDesc) {
		this.destDesc = destDesc;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}
}
