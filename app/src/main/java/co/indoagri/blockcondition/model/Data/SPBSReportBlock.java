package co.indoagri.blockcondition.model.Data;

import android.os.Parcel;
import android.os.Parcelable;

public class SPBSReportBlock implements Parcelable {
	private String year;
	private String companyCode;
	private String estate;
	private String crop;
	private String spbsNumber;
	private String spbsDate;
	private String block;
	private double qtyJanjangAngkut;
	private double qtyLooseFruitAngkut;

	public static final String TABLE_NAME = "SPBS_REPORT_BLOCK";

	public SPBSReportBlock(String year, String companyCode, String estate,
                           String crop, String spbsNumber, String spbsDate, String block,
                           double qtyJanjangAngkut, double qtyLooseFruitAngkut) {
		super();
		this.year = year;
		this.companyCode = companyCode;
		this.estate = estate;
		this.crop = crop;
		this.spbsNumber = spbsNumber;
		this.spbsDate = spbsDate;
		this.block = block;
		this.qtyJanjangAngkut = qtyJanjangAngkut;
		this.qtyLooseFruitAngkut = qtyLooseFruitAngkut;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getCrop() {
		return crop;
	}

	public void setCrop(String crop) {
		this.crop = crop;
	}

	public String getSpbsNumber() {
		return spbsNumber;
	}

	public void setSpbsNumber(String spbsNumber) {
		this.spbsNumber = spbsNumber;
	}

	public String getSpbsDate() {
		return spbsDate;
	}

	public void setSpbsDate(String spbsDate) {
		this.spbsDate = spbsDate;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public double getQtyJanjangAngkut() {
		return qtyJanjangAngkut;
	}

	public void setQtyJanjangAngkut(double qtyJanjangAngkut) {
		this.qtyJanjangAngkut = qtyJanjangAngkut;
	}

	public double getQtyLooseFruitAngkut() {
		return qtyLooseFruitAngkut;
	}

	public void setQtyLooseFruitAngkut(double qtyLooseFruitAngkut) {
		this.qtyLooseFruitAngkut = qtyLooseFruitAngkut;
	}

	public static final Parcelable.Creator<SPBSReportBlock> CREATOR = new Creator<SPBSReportBlock>() {

		@Override
		public SPBSReportBlock createFromParcel(Parcel parcel) {
			SPBSReportBlock bpnReport = new SPBSReportBlock(
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readDouble(),
					parcel.readDouble());

			return bpnReport;
		}

		@Override
		public SPBSReportBlock[] newArray(int size) {
			return new SPBSReportBlock[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeString(year);
		parcel.writeString(companyCode);
		parcel.writeString(estate);
		parcel.writeString(crop);
		parcel.writeString(spbsNumber);
		parcel.writeString(spbsDate);
		parcel.writeString(block);
		parcel.writeDouble(qtyJanjangAngkut);
		parcel.writeDouble(qtyLooseFruitAngkut);
	}

}
