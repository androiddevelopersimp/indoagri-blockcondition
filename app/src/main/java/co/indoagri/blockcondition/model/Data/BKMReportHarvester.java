package co.indoagri.blockcondition.model.Data;

import android.os.Parcel;
import android.os.Parcelable;

public class BKMReportHarvester implements Parcelable {
	private String companyCode;
	private String estate;
	private String division;
	private String gang;
	private String nik;
	private String name;
	private String absentType;
	private double mandays;
	private double output;
	
	public static final String TABLE_NAME = "BKM_REPORT_HARVESTER";
	
	public BKMReportHarvester(String companyCode, String estate,
                              String division, String gang, String nik, String name, String absentType,
                              double mandays, double output) {
		super();
		this.companyCode = companyCode;
		this.estate = estate;
		this.division = division;
		this.gang = gang;
		this.nik = nik;
		this.name = name;
		this.absentType = absentType;
		this.mandays = mandays;
		this.output = output;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getGang() {
		return gang;
	}

	public void setGang(String gang) {
		this.gang = gang;
	}

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbsentType() {
		return absentType;
	}

	public void setAbsentType(String absentType) {
		this.absentType = absentType;
	}

	public double getMandays() {
		return mandays;
	}

	public void setMandays(double mandays) {
		this.mandays = mandays;
	}

	public double getOutput() {
		return output;
	}

	public void setOutput(double output) {
		this.output = output;
	}

	public static final Parcelable.Creator<BKMReportHarvester> CREATOR = new Creator<BKMReportHarvester>() {

		@Override
		public BKMReportHarvester createFromParcel(Parcel parcel) {
			BKMReportHarvester bpnReport  = new BKMReportHarvester(parcel.readString(), parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readDouble(), parcel.readDouble());
			
			return bpnReport;
		}

		@Override
		public BKMReportHarvester[] newArray(int size) {
			return new BKMReportHarvester[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeString(companyCode);
		parcel.writeString(estate);
		parcel.writeString(division);
		parcel.writeString(gang);
		parcel.writeString(nik);
		parcel.writeString(name);
		parcel.writeString(absentType);
		parcel.writeDouble(mandays);
		parcel.writeDouble(output);
	}

}
