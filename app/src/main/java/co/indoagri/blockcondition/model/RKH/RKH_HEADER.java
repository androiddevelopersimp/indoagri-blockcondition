package co.indoagri.blockcondition.model.RKH;

import android.os.Parcel;
import android.os.Parcelable;

public class RKH_HEADER implements Parcelable {
    public String IMEI;
    public String COMPANY_CODE;
    public String ESTATE;
    public String RKH_DATE;
    public String DIVISION;
    public String GANG;
    public String NIK_FOREMAN;
    public String FOREMAN;
    public String NIK_CLERK;
    public String CLERK;
    public String GPS_KOORDINAT;
    public int STATUS = 0;
    private long CREATED_DATE = 0;
    public String CREATED_BY;
    private long MODIFIED_DATE = 0;
    public String MODIFIED_BY;
    public String RKH_ID;
    public boolean Expendable;
    public static final String TABLE_NAME = "RKH_HEADER";
    public static final String XML_DOCUMENT = "RKH_HEADER";
    public static final String XML_FILE = "RKH_ASSISTANCE";
    public static final String ALIAS = "RKH_HEADER";
    public static final String XML_IMEI = "IMEI";
    public static final String XML_COMPANY_CODE = "COMPANY_CODE";
    public static final String XML_ESTATE= "ESTATE";
    public static final String XML_RKH_DATE = "RKH_DATE";
    public static final String XML_DIVISION = "DIVISION";
    public static final String XML_GANG = "GANG";
    public static final String XML_NIK_FOREMAN = "NIK_FOREMAN";
    public static final String XML_FOREMAN ="FOREMAN";
    public static final String XML_NIK_CLERK = "NIK_CLERK";
    public static final String XML_CLERK = "CLERK";
    public static final String XML_GPS_KOORDINAT = "GPS_KOORDINAT";
    public static final String XML_STATUS = "STATUS";
    public static final String XML_CREATED_DATE = "CREATED_DATE";
    public static final String XML_CREATED_BY = "CREATED_BY";
    public static final String XML_MODIFIED_DATE = "MODIFIED_DATE";
    public static final String XML_MODIFIED_BY = "MODIFIED_BY";
    public static final String XML_RKH_ID = "RKH_ID";
    public RKH_HEADER(){}
    public RKH_HEADER(String IMEI,
                      String COMPANY_CODE,
                      String ESTATE,
                      String RKH_DATE,
                      String DIVISION,
                      String GANG,
                      String NIK_FOREMAN,
                      String FOREMAN,
                      String NIK_CLERK,
                      String CLERK,
                      String GPS_KOORDINAT,
                      int STATUS,
                      long CREATED_DATE,
                      String CREATED_BY,
                      long MODIFIED_DATE,
                      String MODIFIED_BY,
                      String RKH_ID)
    {
        this.IMEI = IMEI;
        this.RKH_ID = RKH_ID;
        this.COMPANY_CODE = COMPANY_CODE;
        this.ESTATE = ESTATE;
        this.FOREMAN = FOREMAN;
        this.DIVISION = DIVISION;
        this.GANG = GANG;
        this.NIK_FOREMAN = NIK_FOREMAN;
        this.NIK_CLERK = NIK_CLERK;
        this.CLERK = CLERK;
        this.RKH_DATE = RKH_DATE;
        this.GPS_KOORDINAT = GPS_KOORDINAT;
        this.STATUS = STATUS;
        this.MODIFIED_BY = MODIFIED_BY;
        this.MODIFIED_DATE = MODIFIED_DATE;
        this.CREATED_BY = CREATED_BY;
        this.CREATED_DATE = CREATED_DATE;
        this.Expendable = false;
    }

    public String getIMEI() {
        return IMEI;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    public String getCOMPANY_CODE() {
        return COMPANY_CODE;
    }

    public void setCOMPANY_CODE(String COMPANY_CODE) {
        this.COMPANY_CODE = COMPANY_CODE;
    }

    public String getESTATE() {
        return ESTATE;
    }

    public void setESTATE(String ESTATE) {
        this.ESTATE = ESTATE;
    }

    public String getRKH_DATE() {
        return RKH_DATE;
    }

    public void setRKH_DATE(String RKH_DATE) {
        this.RKH_DATE = RKH_DATE;
    }

    public String getDIVISION() {
        return DIVISION;
    }

    public void setDIVISION(String DIVISION) {
        this.DIVISION = DIVISION;
    }

    public String getGANG() {
        return GANG;
    }

    public void setGANG(String GANG) {
        this.GANG = GANG;
    }

    public String getNIK_FOREMAN() {
        return NIK_FOREMAN;
    }

    public void setNIK_FOREMAN(String NIK_FOREMAN) {
        this.NIK_FOREMAN = NIK_FOREMAN;
    }

    public String getFOREMAN() {
        return FOREMAN;
    }

    public void setFOREMAN(String FOREMAN) {
        this.FOREMAN = FOREMAN;
    }

    public String getNIK_CLERK() {
        return NIK_CLERK;
    }

    public void setNIK_CLERK(String NIK_CLERK) {
        this.NIK_CLERK = NIK_CLERK;
    }

    public String getCLERK() {
        return CLERK;
    }

    public void setCLERK(String CLERK) {
        this.CLERK = CLERK;
    }

    public String getGPS_KOORDINAT() {
        return GPS_KOORDINAT;
    }

    public void setGPS_KOORDINAT(String GPS_KOORDINAT) {
        this.GPS_KOORDINAT = GPS_KOORDINAT;
    }

    public int getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(int STATUS) {
        this.STATUS = STATUS;
    }

    public long getCREATED_DATE() {
        return CREATED_DATE;
    }

    public void setCREATED_DATE(long CREATED_DATE) {
        this.CREATED_DATE = CREATED_DATE;
    }

    public String getCREATED_BY() {
        return CREATED_BY;
    }

    public void setCREATED_BY(String CREATED_BY) {
        this.CREATED_BY = CREATED_BY;
    }

    public long getMODIFIED_DATE() {
        return MODIFIED_DATE;
    }

    public void setMODIFIED_DATE(long MODIFIED_DATE) {
        this.MODIFIED_DATE = MODIFIED_DATE;
    }

    public String getMODIFIED_BY() {
        return MODIFIED_BY;
    }

    public void setMODIFIED_BY(String MODIFIED_BY) {
        this.MODIFIED_BY = MODIFIED_BY;
    }

    public String getRKH_ID() {
        return RKH_ID;
    }

    public void setRKH_ID(String RKH_ID) {
        this.RKH_ID = RKH_ID;
    }

    public boolean isExpendable() {
        return Expendable;
    }

    public void setExpendable(boolean expendable) {
        Expendable = expendable;
    }

    public static String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.IMEI);
        dest.writeString(this.COMPANY_CODE);
        dest.writeString(this.ESTATE);
        dest.writeString(this.RKH_DATE);
        dest.writeString(this.DIVISION);
        dest.writeString(this.GANG);
        dest.writeString(this.NIK_FOREMAN);
        dest.writeString(this.FOREMAN);
        dest.writeString(this.NIK_CLERK);
        dest.writeString(this.CLERK);
        dest.writeString(this.GPS_KOORDINAT);
        dest.writeInt(this.STATUS);
        dest.writeLong(this.CREATED_DATE);
        dest.writeString(this.CREATED_BY);
        dest.writeLong(this.MODIFIED_DATE);
        dest.writeString(this.MODIFIED_BY);
        dest.writeString(this.RKH_ID);
        dest.writeByte(this.Expendable ? (byte) 1 : (byte) 0);
    }

    protected RKH_HEADER(Parcel in) {
        this.IMEI = in.readString();
        this.COMPANY_CODE = in.readString();
        this.ESTATE = in.readString();
        this.RKH_DATE = in.readString();
        this.DIVISION = in.readString();
        this.GANG = in.readString();
        this.NIK_FOREMAN = in.readString();
        this.FOREMAN = in.readString();
        this.NIK_CLERK = in.readString();
        this.CLERK = in.readString();
        this.GPS_KOORDINAT = in.readString();
        this.STATUS = in.readInt();
        this.CREATED_DATE = in.readLong();
        this.CREATED_BY = in.readString();
        this.MODIFIED_DATE = in.readLong();
        this.MODIFIED_BY = in.readString();
        this.RKH_ID = in.readString();
        this.Expendable = in.readByte() != 0;
    }

    public static final Parcelable.Creator<RKH_HEADER> CREATOR = new Parcelable.Creator<RKH_HEADER>() {
        @Override
        public RKH_HEADER createFromParcel(Parcel source) {
            return new RKH_HEADER(source);
        }

        @Override
        public RKH_HEADER[] newArray(int size) {
            return new RKH_HEADER[size];
        }
    };
}
