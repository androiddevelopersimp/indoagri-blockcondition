package co.indoagri.blockcondition.model.Data;

public class SPBSLine {
	private long rowId = 0;
	private int id = 1;						//p
	private String imei = ""; 				// p
	private String year = ""; 				// p
	private String companyCode = ""; 		// p
	private String estate = ""; 			// p
	private String crop = "";		 		// p
	private String spbsNumber = ""; 		// p
	private String spbsDate = ""; 			// p
	private String block = ""; 				// p
	private String tph = ""; 				// p
	private String bpnDate = ""; 			// p
	private String achievementCode = ""; 	// p
	private double quantity = 0;
	private double quantityAngkut = 0;
	private double quantityRemaining = 0;
	private int totalHarvester = 0;
	private String uom = "";
	private String gpsKoordinat = "0.0:0.0";
	private int isSave = 0;
	private int status = 0;
	private String bpnId = "";
	private String spbsRef = "";
	private String spbsNext = "";
	private long createdDate = 0;
	private String createdBy = "";
	private long modifiedDate = 0;
	private String modifiedBy = "";

	public static final String TABLE_NAME = "SPBS_LINE";
	public static final String XML_FILE = "IT_SPBS";
	public static final String XML_DOCUMENT = "SPBS_LINE";
	public static final String XML_ID = "ID";
	public static final String XML_IMEI = "IMEI";
	public static final String XML_YEAR = "YEAR";
	public static final String XML_COMPANY_CODE = "COMPANY_CODE";
	public static final String XML_ESTATE = "ESTATE";
	public static final String XML_CROP = "CROP";
	public static final String XML_SPBS_NUMBER = "SPBS_NUMBER";
	public static final String XML_SPBS_DATE = "SPBS_DATE";
	public static final String XML_BLOCK = "BLOCK";
	public static final String XML_TPH = "TPH";
	public static final String XML_BPN_DATE = "BPN_DATE";
	public static final String XML_ACHIEVEMENT_CODE = "ACHIEVEMENT_CODE";
	public static final String XML_QUANTITY = "QUANTITY";
	public static final String XML_QUANTITY_ANGKUT = "QUANTITY_ANGKUT";
	public static final String XML_QUANTITY_REMAINING = "QUANTITY_REMAINING";
	public static final String XML_TOTAL_HARVESTER = "TOTAL_HARVESTER";
	public static final String XML_UOM = "UOM";
	public static final String XML_GPS_KOORDINAT = "GPS_KOORDINAT";
	public static final String XML_IS_SAVE = "IS_SAVE";
	public static final String XML_STATUS = "STATUS";
	public static final String XML_BPN_ID = "BPN_ID";
	public static final String XML_SPBS_REF = "SPBS_REF";
	public static final String XML_SPBS_PREV = "SPBS_PREV";
	public static final String XML_SPBS_NEXT = "SPBS_NEXT";
	public static final String XML_CREATED_DATE = "CREATED_DATE";
	public static final String XML_CREATED_BY = "CREATED_BY";
	public static final String XML_MODIFIED_DATE = "MODIFIED_DATE";
	public static final String XML_MODIFIED_BY = "MODIFIED_BY";

	public static final String XML_DOCUMENT_RESTORE = "DETAIL";
	public static final String XML_ITEM_RESTORE = "DETAIL";
	public static final String XML_BLOCK_RESTORE = "BLOCK";
	public static final String XML_TPH_RESTORE = "TPH";
	public static final String XML_GPS_KOORDINAT_RESTORE = "GPS";
	public static final String XML_ACHIEVEMENT_CODE_RESTORE = "ACHIEVEMENT";
	public static final String XML_BPN_DATE_RESTORE = "HARVEST_DATE";
	public static final String XML_TOTAL_HARVESTER_RESTORE = "TOTAL_HARVESTER";
	public static final String XML_QUANTITY_RESTORE = "QTY";
	public static final String XML_UOM_RESTORE = "UOM";
	public static final String XML_CREATED_DATE_RESTORE = "CREATED_DATE";
	public static final String XML_CREATED_BY_RESTORE = "CREATED_BY";

	public SPBSLine() {
	}

	public SPBSLine(long rowId, int id, String imei, String year, String companyCode,
                    String estate, String crop, String spbsNumber, String spbsDate,
                    String block, String tph, String bpnDate, String achievementCode,
                    double quantity, double quantityAngkut, int totalHarvester, String uom,
                    String gpsKoordinat, int isSave, int status, String bpnId, String spbsRef,
                    String spbsNext, long createdDate, String createdBy, long modifiedDate, String modifiedBy, double quantityRemaining) {
		super();
		this.rowId = rowId;
		this.id = id;
		this.imei = imei;
		this.year = year;
		this.companyCode = companyCode;
		this.estate = estate;
		this.crop = crop;
		this.spbsNumber = spbsNumber;
		this.spbsDate = spbsDate;
		this.block = block;
		this.tph = tph;
		this.bpnDate = bpnDate;
		this.achievementCode = achievementCode;
		this.quantity = quantity;
		this.quantityAngkut = quantityAngkut;
		this.quantityRemaining = quantityRemaining;
		this.totalHarvester = totalHarvester;
		this.uom = uom;
		this.gpsKoordinat = gpsKoordinat;
		this.isSave = isSave;
		this.status = status;
		this.bpnId = bpnId;
		this.spbsRef = spbsRef;
		this.spbsNext = spbsNext;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.modifiedDate = modifiedDate;
		this.modifiedBy = modifiedBy;
	}

	public long getRowId() {
		return rowId;
	}

	public void setRowId(long rowId) {
		this.rowId = rowId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getCrop() {
		return crop;
	}

	public void setCrop(String crop) {
		this.crop = crop;
	}

	public String getSpbsNumber() {
		return spbsNumber;
	}

	public void setSpbsNumber(String spbsNumber) {
		this.spbsNumber = spbsNumber;
	}

	public String getSpbsDate() {
		return spbsDate;
	}

	public void setSpbsDate(String spbsDate) {
		this.spbsDate = spbsDate;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getTph() {
		return tph;
	}

	public void setTph(String tph) {
		this.tph = tph;
	}

	public String getBpnDate() {
		return bpnDate;
	}

	public void setBpnDate(String bpnDate) {
		this.bpnDate = bpnDate;
	}

	public String getAchievementCode() {
		return achievementCode;
	}

	public void setAchievementCode(String achievementCode) {
		this.achievementCode = achievementCode;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	
	public double getQuantityAngkut() {
		return quantityAngkut;
	}

	public void setQuantityAngkut(double quantityAngkut) {
		this.quantityAngkut = quantityAngkut;
	}

	public double getQuantityRemaining() {
		return quantityRemaining;
	}

	public void setQuantityRemaining(double quantityRemaining) { this.quantityRemaining = quantityRemaining; }

	public int getTotalHarvester() {
		return totalHarvester;
	}

	public void setTotalHarvester(int totalHarvester) {
		this.totalHarvester = totalHarvester;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getGpsKoordinat() {
		return gpsKoordinat;
	}

	public void setGpsKoordinat(String gpsKoordinat) {
		this.gpsKoordinat = gpsKoordinat;
	}

	public int getIsSave() {
		return isSave;
	}

	public void setIsSave(int isSaved) {
		this.isSave = isSaved;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getBpnId() {
		return bpnId;
	}

	public void setBpnId(String bpnId) {
		this.bpnId = bpnId;
	}

	public String getSpbsRef() {
		return spbsRef;
	}

	public void setSpbsRef(String spbsRef) {
		this.spbsRef = spbsRef;
	}

	public String getSpbsNext() {
		return spbsNext;
	}

	public void setSpbsNext(String spbsNext) {
		this.spbsNext = spbsNext;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
