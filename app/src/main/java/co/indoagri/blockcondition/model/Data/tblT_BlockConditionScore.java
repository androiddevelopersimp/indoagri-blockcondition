package co.indoagri.blockcondition.model.Data;

public class tblT_BlockConditionScore {

    private String acc_CompanyCode = "";
    private String acc_Estate = "";
    private String acc_ZYear = "";
    private String acc_Period = "";
    private String acc_Block= "";
    private String acc_TransLevel= "";
    private String acc_Color= "";
    private String acc_Jalan= "";
    private String acc_Jembatan= "";
    private String acc_Parit= "";
    private String acc_TitiPanen= "";
    private String acc_TitiRintis= "";
    private String acc_Tikus= "";
    private String acc_Pencurian= "";
    private String acc_BW= "";
    private String acc_TPHBersih= "";
    private String acc_TPH= "";
    private String acc_TPHBersih2= "";
    private String acc_TPH2= "";
    private String acc_Piringan= "";
    private String acc_PasarPanen= "";
    private String acc_PasarRintis= "";
    private String acc_TunasPokok= "";
    private String acc_Gawangan= "";
    private String acc_Drainase= "";
    private String acc_Ganoderma= "";
    private String acc_Rayap= "";
    private String acc_Orcytes= "";
    private String acc_Sanitasi= "";
    private String acc_Kacangan= "";
    private String acc_CreatedDateTime= "";
    private String acc_CreatedBy= "";
    private String acc_ModifiedDateTime= "";
    private String acc_ModifiedBy= "";
    public static final String TABLE_NAME = "tblT_BlockConditionScore";
    public static final String ALIAS = "BLOCK CONDITION SCORE";
    public static final String XML_DOCUMENT = "IT_BLOCK_CONDITION_SCORE";
    public static final String XML_CompanyCode = "CompanyCode";
    public static final String XML_Estate = "Estate";
    public static final String XML_ZYear = "ZYear";
    public static final String XML_Color= "Color";
    public static final String XML_Period = "Period";
    public static final String XML_TransLevel = "TransLevel";
    public static final String XML_Block= "Block";
    public static final String XML_Jalan= "Jalan";
    public static final String XML_Jembatan= "Jembatan";
    public static final String XML_Parit= "Parit";
    public static final String XML_TitiPanen= "TitiPanen";
    public static final String XML_TitiRintis= "TitiRintis";
    public static final String XML_Tikus= "Tikus";
    public static final String XML_Pencurian= "Pencurian";
    public static final String XML_BW= "BW";
    public static final String XML_TPHBersih= "TPHBersih";
    public static final String XML_TPH= "TPH";
    public static final String XML_TPHBersih2= "TPHBersih2";
    public static final String XML_TPH2= "TPH2";
    public static final String XML_Piringan= "Piringan";
    public static final String XML_PasarPanen= "PasarPanen";
    public static final String XML_PasarRintis= "PasarRintis";
    public static final String XML_TunasPokok= "TunasPokok";
    public static final String XML_Gawangan= "Gawangan";
    public static final String XML_Drainase= "Drainase";
    public static final String XML_Ganoderma= "Ganoderma";
    public static final String XML_Rayap= "Rayap";
    public static final String XML_Orcytes= "Orcytes";
    public static final String XML_Sanitasi= "Sanitasi";
    public static final String XML_Kacangan= "Kacangan";
    public static final String XML_CreatedDateTime= "CreatedDateTime";
    public static final String XML_CreatedBy= "CreatedBy";
    public static final String XML_ModifiedDateTime= "ModifiedDateTime";
    public static final String XML_ModifiedBy= "ModifiedBy";

    public tblT_BlockConditionScore(){

    }


    public tblT_BlockConditionScore(String acc_CompanyCode, String acc_Estate,
                                    String acc_ZYear, String acc_Period,
                                    String acc_Block, String acc_TransLevel,
                                    String acc_Color, String acc_Jalan,
                                    String acc_Jembatan, String acc_Parit,
                                    String acc_TitiPanen, String acc_TitiRintis,
                                    String acc_Tikus, String acc_Pencurian,
                                    String acc_BW, String acc_TPHBersih,
                                    String acc_TPH,String acc_TPHBersih2,
                                    String acc_TPH2, String acc_Piringan,
                                    String acc_PasarPanen, String acc_PasarRintis,
                                    String acc_TunasPokok,String acc_Gawangan,
                                    String acc_Drainase, String acc_Ganoderma,
                                    String acc_Rayap,String acc_Orcytes, String acc_Sanitasi,
                                    String acc_Kacangan, String acc_CreatedDateTime,
                                    String acc_CreatedBy, String acc_ModifiedDateTime,
                                    String acc_ModifiedBy) {
        this.acc_CompanyCode = acc_CompanyCode;
        this.acc_Estate = acc_Estate;
        this.acc_ZYear = acc_ZYear;
        this.acc_Period = acc_Period;
        this.acc_Block = acc_Block;
        this.acc_TransLevel = acc_TransLevel;
        this.acc_Color = acc_Color;
        this.acc_Jalan = acc_Jalan;
        this.acc_Jembatan = acc_Jembatan;
        this.acc_Parit = acc_Parit;
        this.acc_TitiPanen = acc_TitiPanen;
        this.acc_TitiRintis = acc_TitiRintis;
        this.acc_Tikus = acc_Tikus;
        this.acc_Pencurian = acc_Pencurian;
        this.acc_BW = acc_BW;
        this.acc_TPHBersih = acc_TPHBersih;
        this.acc_TPH = acc_TPH;
        this.acc_TPHBersih2 = acc_TPHBersih2;
        this.acc_TPH2 = acc_TPH2;
        this.acc_Piringan = acc_Piringan;
        this.acc_PasarPanen = acc_PasarPanen;
        this.acc_PasarRintis = acc_PasarRintis;
        this.acc_TunasPokok = acc_TunasPokok;
        this.acc_Gawangan = acc_Gawangan;
        this.acc_Drainase = acc_Drainase;
        this.acc_Ganoderma = acc_Ganoderma;
        this.acc_Rayap = acc_Rayap;
        this.acc_Orcytes = acc_Orcytes;
        this.acc_Sanitasi = acc_Sanitasi;
        this.acc_Kacangan = acc_Kacangan;
        this.acc_CreatedDateTime = acc_CreatedDateTime;
        this.acc_CreatedBy = acc_CreatedBy;
        this.acc_ModifiedDateTime = acc_ModifiedDateTime;
        this.acc_ModifiedBy = acc_ModifiedBy;
    }

    public String getAcc_TPHBersih2() {
        return acc_TPHBersih2;
    }

    public void setAcc_TPHBersih2(String acc_TPHBersih2) {
        this.acc_TPHBersih2 = acc_TPHBersih2;
    }

    public String getAcc_TPH2() {
        return acc_TPH2;
    }

    public void setAcc_TPH2(String acc_TPH2) {
        this.acc_TPH2 = acc_TPH2;
    }

    public String getAcc_CompanyCode() {
        return acc_CompanyCode;
    }

    public void setAcc_CompanyCode(String acc_CompanyCode) {
        this.acc_CompanyCode = acc_CompanyCode;
    }

    public String getAcc_Estate() {
        return acc_Estate;
    }

    public void setAcc_Estate(String acc_Estate) {
        this.acc_Estate = acc_Estate;
    }

    public String getAcc_ZYear() {
        return acc_ZYear;
    }

    public void setAcc_ZYear(String acc_ZYear) {
        this.acc_ZYear = acc_ZYear;
    }

    public String getAcc_Period() {
        return acc_Period;
    }

    public void setAcc_Period(String acc_Period) {
        this.acc_Period = acc_Period;
    }

    public String getAcc_PasarRintis() {
        return acc_PasarRintis;
    }

    public void setAcc_PasarRintis(String acc_PasarRintis) {
        this.acc_PasarRintis = acc_PasarRintis;
    }

    public String getAcc_Block() {
        return acc_Block;
    }

    public void setAcc_Block(String acc_Block) {
        this.acc_Block = acc_Block;
    }

    public String getAcc_TransLevel() {
        return acc_TransLevel;
    }

    public void setAcc_TransLevel(String acc_TransLevel) {
        this.acc_TransLevel = acc_TransLevel;
    }

    public String getAcc_Color() {
        return acc_Color;
    }

    public void setAcc_Color(String acc_Color) {
        this.acc_Color = acc_Color;
    }

    public String getAcc_Jalan() {
        return acc_Jalan;
    }

    public void setAcc_Jalan(String acc_Jalan) {
        this.acc_Jalan = acc_Jalan;
    }

    public String getAcc_Jembatan() {
        return acc_Jembatan;
    }

    public void setAcc_Jembatan(String acc_Jembatan) {
        this.acc_Jembatan = acc_Jembatan;
    }

    public String getAcc_Parit() {
        return acc_Parit;
    }

    public void setAcc_Parit(String acc_Parit) {
        this.acc_Parit = acc_Parit;
    }

    public String getAcc_TitiPanen() {
        return acc_TitiPanen;
    }

    public void setAcc_TitiPanen(String acc_TitiPanen) {
        this.acc_TitiPanen = acc_TitiPanen;
    }

    public String getAcc_TitiRintis() {
        return acc_TitiRintis;
    }

    public void setAcc_TitiRintis(String acc_TitiRintis) {
        this.acc_TitiRintis = acc_TitiRintis;
    }

    public String getAcc_Tikus() {
        return acc_Tikus;
    }

    public void setAcc_Tikus(String acc_Tikus) {
        this.acc_Tikus = acc_Tikus;
    }

    public String getAcc_Pencurian() {
        return acc_Pencurian;
    }

    public void setAcc_Pencurian(String acc_Pencurian) {
        this.acc_Pencurian = acc_Pencurian;
    }

    public String getAcc_BW() {
        return acc_BW;
    }

    public void setAcc_BW(String acc_BW) {
        this.acc_BW = acc_BW;
    }

    public String getAcc_TPHBersih() {
        return acc_TPHBersih;
    }

    public void setAcc_TPHBersih(String acc_TPHBersih) {
        this.acc_TPHBersih = acc_TPHBersih;
    }

    public String getAcc_TPH() {
        return acc_TPH;
    }

    public void setAcc_TPH(String acc_TPH) {
        this.acc_TPH = acc_TPH;
    }

    public String getAcc_Piringan() {
        return acc_Piringan;
    }

    public void setAcc_Piringan(String acc_Piringan) {
        this.acc_Piringan = acc_Piringan;
    }

    public String getAcc_PasarPanen() {
        return acc_PasarPanen;
    }

    public void setAcc_PasarPanen(String acc_PasarPanen) {
        this.acc_PasarPanen = acc_PasarPanen;
    }

    public String getAcc_TunasPokok() {
        return acc_TunasPokok;
    }

    public void setAcc_TunasPokok(String acc_TunasPokok) {
        this.acc_TunasPokok = acc_TunasPokok;
    }

    public String getAcc_Gawangan() {
        return acc_Gawangan;
    }

    public void setAcc_Gawangan(String acc_Gawangan) {
        this.acc_Gawangan = acc_Gawangan;
    }

    public String getAcc_Drainase() {
        return acc_Drainase;
    }

    public void setAcc_Drainase(String acc_Drainase) {
        this.acc_Drainase = acc_Drainase;
    }

    public String getAcc_Ganoderma() {
        return acc_Ganoderma;
    }

    public void setAcc_Ganoderma(String acc_Ganoderma) {
        this.acc_Ganoderma = acc_Ganoderma;
    }

    public String getAcc_Rayap() {
        return acc_Rayap;
    }

    public void setAcc_Rayap(String acc_Rayap) {
        this.acc_Rayap = acc_Rayap;
    }

    public String getAcc_Orcytes() {
        return acc_Orcytes;
    }

    public void setAcc_Orcytes(String acc_Orcytes) {
        this.acc_Orcytes = acc_Orcytes;
    }

    public String getAcc_Sanitasi() {
        return acc_Sanitasi;
    }

    public void setAcc_Sanitasi(String acc_Sanitasi) {
        this.acc_Sanitasi = acc_Sanitasi;
    }

    public String getAcc_Kacangan() {
        return acc_Kacangan;
    }

    public void setAcc_Kacangan(String acc_Kacangan) {
        this.acc_Kacangan = acc_Kacangan;
    }

    public String getAcc_CreatedDateTime() {
        return acc_CreatedDateTime;
    }

    public void setAcc_CreatedDateTime(String acc_CreatedDateTime) {
        this.acc_CreatedDateTime = acc_CreatedDateTime;
    }

    public String getAcc_CreatedBy() {
        return acc_CreatedBy;
    }

    public void setAcc_CreatedBy(String acc_CreatedBy) {
        this.acc_CreatedBy = acc_CreatedBy;
    }

    public String getAcc_ModifiedDateTime() {
        return acc_ModifiedDateTime;
    }

    public void setAcc_ModifiedDateTime(String acc_ModifiedDateTime) {
        this.acc_ModifiedDateTime = acc_ModifiedDateTime;
    }

    public String getAcc_ModifiedBy() {
        return acc_ModifiedBy;
    }

    public void setAcc_ModifiedBy(String acc_ModifiedBy) {
        this.acc_ModifiedBy = acc_ModifiedBy;
    }
}
