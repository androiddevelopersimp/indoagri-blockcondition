package co.indoagri.blockcondition.model.Data;

public class SPBSPrint {
	private String block;
	private String bpnDate;
	private int qtyJanjang;
	private double qtyLooseFruit;

	public SPBSPrint(String block, String bpnDate, int qtyJanjang, double qtyLooseFruit) {
		super();
		this.block = block;
		this.bpnDate = bpnDate;
		this.qtyJanjang = qtyJanjang;
		this.qtyLooseFruit = qtyLooseFruit;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}
	
	public String getBpnDate() {
		return bpnDate;
	}

	public void setBpnDate(String bpnDate) {
		this.bpnDate = bpnDate;
	}

	public int getQtyJanjang() {
		return qtyJanjang;
	}

	public void setQtyJanjang(int qtyJanjang) {
		this.qtyJanjang = qtyJanjang;
	}

	public double getQtyLooseFruit() {
		return qtyLooseFruit;
	}

	public void setQtyLooseFruit(double qtyLooseFruit) {
		this.qtyLooseFruit = qtyLooseFruit;
	}

}
