package co.indoagri.blockcondition.model.Data;

import android.os.Parcel;
import android.os.Parcelable;

public class BPNReportTph implements Parcelable {
	private String id;
	private String companyCode;
	private String estate;
	private String division;
	private String tph;
	private double qtyJanjang;
	private double qtyLooseFruit;
	private double qtyMentah;
	private double qtyBusuk;
	private double qtyTangkaiPanjang;
	private long createdDate;

	public BPNReportTph(String id, String companyCode, String estate,
                        String division, String tph, double qtyJanjang,
                        double qtyLooseFruit, double qtyMentah, double qtyBusuk,
                        double qtyTangkaiPanjang, long createdDate) {
		super();
		this.id = id;
		this.companyCode = companyCode;
		this.estate = estate;
		this.division = division;
		this.tph = tph;
		this.qtyJanjang = qtyJanjang;
		this.qtyLooseFruit = qtyLooseFruit;
		this.qtyMentah = qtyMentah;
		this.qtyBusuk = qtyBusuk;
		this.qtyTangkaiPanjang = qtyTangkaiPanjang;
		this.createdDate = createdDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getTph() {
		return tph;
	}

	public void setTph(String tph) {
		this.tph = tph;
	}

	public double getQtyJanjang() {
		return qtyJanjang;
	}

	public void setQtyJanjang(double qtyJanjang) {
		this.qtyJanjang = qtyJanjang;
	}

	public double getQtyLooseFruit() {
		return qtyLooseFruit;
	}

	public void setQtyLooseFruit(double qtyLooseFruit) {
		this.qtyLooseFruit = qtyLooseFruit;
	}

	public double getQtyMentah() {
		return qtyMentah;
	}

	public void setQtyMentah(double qtyMentah) {
		this.qtyMentah = qtyMentah;
	}

	public double getQtyBusuk() {
		return qtyBusuk;
	}

	public void setQtyBusuk(double qtyBusuk) {
		this.qtyBusuk = qtyBusuk;
	}

	public double getQtyTangkaiPanjang() {
		return qtyTangkaiPanjang;
	}

	public void setQtyTangkaiPanjang(double qtyTangkaiPanjang) {
		this.qtyTangkaiPanjang = qtyTangkaiPanjang;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public static final Parcelable.Creator<BPNReportTph> CREATOR = new Creator<BPNReportTph>() {

		@Override
		public BPNReportTph createFromParcel(Parcel parcel) {
			BPNReportTph bpnHeader = new BPNReportTph(parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readDouble(),
					parcel.readDouble(), parcel.readDouble(),
					parcel.readDouble(), parcel.readDouble(),
					parcel.readLong());

			return bpnHeader;
		}

		@Override
		public BPNReportTph[] newArray(int size) {
			return new BPNReportTph[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeString(id);
		parcel.writeString(companyCode);
		parcel.writeString(estate);
		parcel.writeString(division);
		parcel.writeString(tph);
		parcel.writeDouble(qtyJanjang);
		parcel.writeDouble(qtyLooseFruit);
		parcel.writeDouble(qtyMentah);
		parcel.writeDouble(qtyBusuk);
		parcel.writeDouble(qtyTangkaiPanjang);
		parcel.writeLong(createdDate);
	}

}
