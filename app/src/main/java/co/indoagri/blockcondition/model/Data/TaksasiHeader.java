package co.indoagri.blockcondition.model.Data;

import android.os.Parcel;
import android.os.Parcelable;

public class TaksasiHeader implements Parcelable {
	private long rowId;
	private String imei; 		// p
	private String companyCode; // p
	private String estate; 		// p
	private String division;	// p
	private String taksasiDate;	// p
	private String block;		// p
	private String crop;		// p
	private String nikForeman;
	private String foreman;
	private double prodTrees;
	private double bjr;
	private String gpsKoordinat;
	private int isSave;
	private int status;
	private long createdDate;
	private String createdBy;
	private long modifiedDate;
	private String modifiedBy;

	public static final String TABLE_NAME = "TAKSASI_HEADER";
	public static final String XML_FILE = "IT_TAKSASI";
	public static final String XML_DOCUMENT = "TAKSASI_HEADER";
	public static final String XML_IMEI = "IMEI";
	public static final String XML_COMPANY_CODE = "COMPANY_CODE";
	public static final String XML_ESTATE = "ESTATE";
	public static final String XML_DIVISION = "DIVISION";
	public static final String XML_TAKSASI_DATE = "TAKSASI_DATE";
	public static final String XML_BLOCK = "BLOCK";
	public static final String XML_CROP = "CROP";
	public static final String XML_NIK_FOREMAN = "NIK_FOREMAN";
	public static final String XML_FOREMAN = "FOREMAN";
	public static final String XML_PROD_TREES = "PROD_TREES";
	public static final String XML_BJR = "BJR";
	public static final String XML_GPS_KOORDINAT = "GPS_KOORDINAT";
	public static final String XML_IS_SAVE = "IS_SAVE";
	public static final String XML_STATUS = "STATUS";
	public static final String XML_CREATED_DATE = "CREATED_DATE";
	public static final String XML_CREATED_BY = "CREATED_BY";
	public static final String XML_MODIFIED_DATE = "MODIFIED_DATE";
	public static final String XML_MODIFIED_BY = "MODIFIED_BY";
	public static final String XML_BARIS_SKBS = "BARIS_SKBS";
	
	public static final String XML_DOCUMENT_RESTORE = "TAKSASI";
	public static final String XML_ITEM_RESTORE = "HEADER";
	public static final String XML_IMEI_RESTORE = "IMEI";
	public static final String XML_ESTATE_RESTORE = "ESTATE";
	public static final String XML_DIVISION_RESTORE = "DIVISION";
	public static final String XML_TAKSASI_DATE_RESTORE = "TAKSASI_DATE";
	public static final String XML_BLOCK_RESTORE = "BLOCK";
	public static final String XML_CROP_RESTORE = "CROP";
	public static final String XML_NIK_FOREMAN_RESTORE = "NIK_FOREMAN";
	public static final String XML_FOREMAN_RESTORE = "FOREMAN";
	public static final String XML_BJR_RESTORE = "BJR";
	public static final String XML_PROD_TREES_RESTORE = "PRODTREES";
	public static final String XML_GPS_KOORDINAT_RESTORE = "GPS";
	public static final String XML_CREATED_DATE_RESTORE = "CREATED_DATE";
	public static final String XML_CREATED_BY_RESTORE = "CREATED_BY";
	
	public TaksasiHeader(){}
	
	public TaksasiHeader(long rowId, String imei, String companyCode,
                         String estate, String division, String taksasiDate, String block, String crop,
                         String nikForeman, String Foreman, double prodTrees, double bjr,
                         String gpsKoordinat, int isSave, int status, long createdDate,
                         String createdBy, long modifiedDate, String modifiedBy) {
		super();
		this.rowId = rowId;
		this.imei = imei;
		this.companyCode = companyCode;
		this.estate = estate;
		this.division = division;
		this.taksasiDate = taksasiDate;
		this.block = block;
		this.crop = crop;
		this.nikForeman = nikForeman;
		this.foreman = Foreman;
		this.prodTrees = prodTrees;
		this.bjr = bjr;
		this.gpsKoordinat = gpsKoordinat;
		this.isSave = isSave;
		this.status = status;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.modifiedDate = modifiedDate;
		this.modifiedBy = modifiedBy;
	}

	public long getRowId() {
		return rowId;
	}

	public void setRowId(long rowId) {
		this.rowId = rowId;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getTaksasiDate() {
		return taksasiDate;
	}

	public void setTaksasiDate(String taksasiDate) {
		this.taksasiDate = taksasiDate;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getCrop() {
		return crop;
	}

	public void setCrop(String crop) {
		this.crop = crop;
	}

	public String getNikForeman() {
		return nikForeman;
	}

	public void setNikForeman(String nikForeman) {
		this.nikForeman = nikForeman;
	}

	public String getForeman() {
		return foreman;
	}

	public void setForeman(String clerk) {
		this.foreman = clerk;
	}

	public double getProdTrees() {
		return prodTrees;
	}

	public void setProdTrees(double prodTrees) {
		this.prodTrees = prodTrees;
	}

	public double getBjr() {
		return bjr;
	}

	public void setBjr(double bjr) {
		this.bjr = bjr;
	}

	public String getGpsKoordinat() {
		return gpsKoordinat;
	}

	public void setGpsKoordinat(String gpsKoordinat) {
		this.gpsKoordinat = gpsKoordinat;
	}

	public int getIsSave() {
		return isSave;
	}

	public void setIsSave(int isSave) {
		this.isSave = isSave;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	
	
	public static final Parcelable.Creator<TaksasiHeader> CREATOR = new Creator<TaksasiHeader>() {

		@Override
		public TaksasiHeader createFromParcel(Parcel parcel) {

			TaksasiHeader bpnHeader = new TaksasiHeader(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString(), 
					parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(),  parcel.readString(),
					parcel.readDouble(), parcel.readDouble(), parcel.readString(),parcel.readInt(), parcel.readInt(), parcel.readLong(), 
					parcel.readString(), parcel.readLong(), parcel.readString());
			
			return bpnHeader;
		}

		@Override
		public TaksasiHeader[] newArray(int size) {
			return new TaksasiHeader[size];
		}
	};
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeLong(rowId);
		parcel.writeString(imei);
		parcel.writeString(companyCode);
		parcel.writeString(estate);
		parcel.writeString(division);
		parcel.writeString(taksasiDate);
		parcel.writeString(block);
		parcel.writeString(crop);
		parcel.writeString(nikForeman);
		parcel.writeString(foreman);
		parcel.writeDouble(prodTrees);
		parcel.writeDouble(bjr);
		parcel.writeString(gpsKoordinat);
		parcel.writeInt(isSave);
		parcel.writeInt(status);
		parcel.writeLong(createdDate);
		parcel.writeString(createdBy);
		parcel.writeLong(modifiedDate);
		parcel.writeString(modifiedBy);
	}

}
