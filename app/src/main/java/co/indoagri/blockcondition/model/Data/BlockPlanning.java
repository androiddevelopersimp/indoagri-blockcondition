package co.indoagri.blockcondition.model.Data;

public class BlockPlanning {
	private String block;
	private String createdDate;
	private String createdBy;
	
	public final static String TABLE_NAME = "BLOCK_PLANNING";
	public final static String XML_BLOCK = "BLOCK";
	public final static String XML_CREATED_DATE = "CREATED_DATE";
	public final static String XML_CREATED_BY = "CREATED_BY";
	
	public BlockPlanning(){}
	
	public BlockPlanning(String block, String createdDate, String createdBy) {
		super();
		this.block = block;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}
