package co.indoagri.blockcondition.model;

public class LogModel {
    private int rowId;
    private String userCode;
    private String CreatedDate;
    private String Description;
    private String Reference;


    public final static String TABLE_NAME = "Log_UserProcess";
    public static final String ALIAS= "Log_User";
    public final static String XML_ID= "ID";
    public final static String XML_USERCODE = "USER";
    public final static String XML_CREATEDDATE= "CREATED";
    public final static String XML_DESCRIPTION= "DESCRIPTION";
    public final static String XML_REFERENCE= "REFERENCE";

    public LogModel(){

    }
    public LogModel(int rowId,String userCode,String createdDate,String description,String reference) {
        super();
        this.rowId = rowId;
        this.userCode = userCode;
        this.CreatedDate = createdDate;
        this.Description = description;
        this.Reference = reference;
    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        this.CreatedDate = createdDate;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public String getReference() {
        return Reference;
    }

    public void setReference(String reference) {
        this.Reference = reference;
    }
}
