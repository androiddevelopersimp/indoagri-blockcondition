package co.indoagri.blockcondition.model.Data;


public class RunningAccount {
    private String companyCode;
    private String estate;
    private String runningAccount;
    private String licensePlate;
    private String lifnr;
    private String ownerShipFlag;

    public final static String TABLE_NAME = "RUNNING_ACCOUNT";
    public static final String ALIAS= "RUNNING ACCOUNT";
    public final static String XML_DOCUMENT = "IT_KODEKENDARAAN";
    public final static String XML_ITEM = "ITEM";
    public final static String XML_COMPANY_CODE = "COMPANY_CODE";
    public final static String XML_ESTATE = "ESTATE";
    public final static String XML_RUNNING_ACCOUNT = "RUNNING_ACCOUNT";
    public final static String XML_LICENSE_PLATE = "LICENSE_PLATE";
    public final static String XML_LIFNR = "LIFNR";
    public final static String XML_OWNERSHIPFLAG = "OWNERSHIPFLAG";

    public RunningAccount() {
    }

    public RunningAccount(String companyCode, String estate, String runningAccount,
                          String licensePlate, String lifnr, String ownerShipFlag) {
        super();
        this.companyCode = companyCode;
        this.estate = estate;
        this.runningAccount = runningAccount;
        this.licensePlate = licensePlate;
        this.lifnr = lifnr;
        this.ownerShipFlag = ownerShipFlag;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getRunningAccount() {
        return runningAccount;
    }

    public void setRunningAccount(String runningAccount) {
        this.runningAccount = runningAccount;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getLifnr() {
        return lifnr;
    }

    public void setLifnr(String lifnr) {
        this.lifnr = lifnr;
    }

    public String getOwnerShipFlag() {
        return ownerShipFlag;
    }

    public void setOwnerShipFlag(String ownerShipFlag) {
        this.ownerShipFlag = ownerShipFlag;
    }
}
