package co.indoagri.blockcondition.model.Users;

public class AbsentType {
    private long rowId = 0;
    private String companyCode = "";	//p
    private String absentType = "";	//p
    private String description = "";
    private double hkrllo = 0;
    private double hkrlhi = 0;
    private double hkpylo = 0;
    private double hkpyhi = 0;
    private double hkvllo = 0;
    private double hkvlhi = 0;
    private double hkmein = 0;
    private String agroup = "";

    public static final String TABLE_NAME = "ABSENT_TYPE";
    public static final String ALIAS = "ABSENT TYPE";
    public static final String XML_DOCUMENT = "IT_ABSENT";
    public static final String XML_ITEM = "ITEM";
    public static final String XML_COMPANY_CODE = "COMPANYCODE";
    public static final String XML_ABSENT_TYPE = "ABSTYPE";
    public static final String XML_DESCRIPTION = "DESCRIPTION";
    public static final String XML_HKRLLO = "HKRLLO";
    public static final String XML_HKRLHI = "HKRLHI";
    public static final String XML_HKPYLO = "HKPYLO";
    public static final String XML_HKPYHI = "HKPYHI";
    public static final String XML_HKVLLO = "HKVLLO";
    public static final String XML_HKVLHI = "HKVLHI";
    public static final String XML_HKMEIN = "HKMEIN";
    public static final String XML_AGROUP = "AGROUP";

    public AbsentType(){}

    public AbsentType(long rowId, String companyCode, String absentType,
                      String description, double hkrllo, double hkrlhi, double hkpylo,
                      double hkpyhi, double hkvllo, double hkvlhi, double hkmein,
                      String agroup) {
        super();
        this.rowId = rowId;
        this.companyCode = companyCode;
        this.absentType = absentType;
        this.description = description;
        this.hkrllo = hkrllo;
        this.hkrlhi = hkrlhi;
        this.hkpylo = hkpylo;
        this.hkpyhi = hkpyhi;
        this.hkvllo = hkvllo;
        this.hkvlhi = hkvlhi;
        this.hkmein = hkmein;
        this.agroup = agroup;
    }

    public long getRowId() {
        return rowId;
    }

    public void setRowId(long rowId) {
        this.rowId = rowId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getAbsentType() {
        return absentType;
    }

    public void setAbsentType(String absentType) {
        this.absentType = absentType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getHkrllo() {
        return hkrllo;
    }

    public void setHkrllo(double hkrllo) {
        this.hkrllo = hkrllo;
    }

    public double getHkrlhi() {
        return hkrlhi;
    }

    public void setHkrlhi(double hkrlhi) {
        this.hkrlhi = hkrlhi;
    }

    public double getHkpylo() {
        return hkpylo;
    }

    public void setHkpylo(double hkpylo) {
        this.hkpylo = hkpylo;
    }

    public double getHkpyhi() {
        return hkpyhi;
    }

    public void setHkpyhi(double hkpyhi) {
        this.hkpyhi = hkpyhi;
    }

    public double getHkvllo() {
        return hkvllo;
    }

    public void setHkvllo(double hkvllo) {
        this.hkvllo = hkvllo;
    }

    public double getHkvlhi() {
        return hkvlhi;
    }

    public void setHkvlhi(double hkvlhi) {
        this.hkvlhi = hkvlhi;
    }

    public double getHkmein() {
        return hkmein;
    }

    public void setHkmein(double hkmein) {
        this.hkmein = hkmein;
    }

    public String getAgroup() {
        return agroup;
    }

    public void setAgroup(String agroup) {
        this.agroup = agroup;
    }
}
