package co.indoagri.blockcondition.model.Data;

public class SPBSSummary {
	private String spbsNumber;
	private String spbsDate;
	private String block;
	private double qtyJanjang;
	private double qtyLooseFruit;
	private double qtyJanjangPrev;

	public SPBSSummary(String spbsNumber, String spbsDate, String block, double qtyJanjang, double qtyLooseFruit) {
		super();
		this.spbsNumber = spbsNumber;
		this.spbsDate = spbsDate;
		this.block = block;
		this.qtyJanjang = qtyJanjang;
		this.qtyLooseFruit = qtyLooseFruit;
	}

	public String getSpbsNumber() {
		return spbsNumber;
	}

	public void setSpbsNumber(String spbsNumber) {
		this.spbsNumber = spbsNumber;
	}

	public String getSpbsDate() {
		return spbsDate;
	}

	public void setSpbsDate(String spbsDate) {
		this.spbsDate = spbsDate;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public double getQtyJanjang() {
		return qtyJanjang;
	}

	public void setQtyJanjang(double qtyJanjang) {
		this.qtyJanjang = qtyJanjang;
	}

	/*public double getQtyJanjangRemaining() {
		return qtyJanjangRemaining;
	}

	public void setQtyJanjangRemaining(double qtyJanjangRemaining) { this.qtyJanjangRemaining = qtyJanjangRemaining; }*/

	public double getQtyLooseFruit() {
		return qtyLooseFruit;
	}

	public void setQtyLooseFruit(double qtyLooseFruit) {
		this.qtyLooseFruit = qtyLooseFruit;
	}

}
