package co.indoagri.blockcondition.model.Data;

import android.os.Parcel;
import android.os.Parcelable;

public class SPBSReportTph implements Parcelable {
	private String year;
	private String companyCode;
	private String estate;
	private String crop;
	private String spbsNumber;
	private String spbsDate;
	private String block;
	private String tph;
	private String bpnDate;
	private double qtyJanjang;
	private double qtyLooseFruit;

	public static final String TABLE_NAME = "SPBS_REPORT_TPH";

	public SPBSReportTph(String year, String companyCode, String estate,
                         String crop, String spbsNumber, String spbsDate, String block,
                         String tph, String bpnDate, double qtyJanjang, double qtyLooseFruit) {
		super();
		this.year = year;
		this.companyCode = companyCode;
		this.estate = estate;
		this.crop = crop;
		this.spbsNumber = spbsNumber;
		this.spbsDate = spbsDate;
		this.block = block;
		this.tph = tph;
		this.bpnDate = bpnDate;
		this.qtyJanjang = qtyJanjang;
		this.qtyLooseFruit = qtyLooseFruit;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getCrop() {
		return crop;
	}

	public void setCrop(String crop) {
		this.crop = crop;
	}

	public String getSpbsNumber() {
		return spbsNumber;
	}

	public void setSpbsNumber(String spbsNumber) {
		this.spbsNumber = spbsNumber;
	}

	public String getSpbsDate() {
		return spbsDate;
	}

	public void setSpbsDate(String spbsDate) {
		this.spbsDate = spbsDate;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getTph() {
		return tph;
	}

	public void setTph(String tph) {
		this.tph = tph;
	}

	public String getBpnDate() {
		return bpnDate;
	}

	public void setBpnDate(String bpnDate) {
		this.bpnDate = bpnDate;
	}

	public double getQtyJanjang() {
		return qtyJanjang;
	}

	public void setQtyJanjang(double qtyJanjang) {
		this.qtyJanjang = qtyJanjang;
	}

	public double getQtyLooseFruit() {
		return qtyLooseFruit;
	}

	public void setQtyLooseFruit(double qtyLooseFruit) {
		this.qtyLooseFruit = qtyLooseFruit;
	}

	public static final Parcelable.Creator<SPBSReportTph> CREATOR = new Creator<SPBSReportTph>() {

		@Override
		public SPBSReportTph createFromParcel(Parcel parcel) {
			SPBSReportTph bpnReport = new SPBSReportTph(parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readDouble(), parcel.readDouble());

			return bpnReport;
		}

		@Override
		public SPBSReportTph[] newArray(int size) {
			return new SPBSReportTph[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeString(year);
		parcel.writeString(companyCode);
		parcel.writeString(estate);
		parcel.writeString(crop);
		parcel.writeString(spbsNumber);
		parcel.writeString(spbsDate);
		parcel.writeString(block);
		parcel.writeString(tph);
		parcel.writeString(bpnDate);
		parcel.writeDouble(qtyJanjang);
		parcel.writeDouble(qtyLooseFruit);
	}

}
