package co.indoagri.blockcondition.model.Data;

public class DeviceAlias {
	private String deviceAlias;
	
	public static String TABLE_NAME = "DEVICE_ALIAS";
	public static String XML_DEVICE_ALIAS = "DEVICE_ALIAS";
	

	public DeviceAlias() {
	}
	
	public DeviceAlias(String deviceAlias){
		this.deviceAlias = deviceAlias;
	}

	public String getDeviceAlias() {
		return deviceAlias;
	}

	public void setDeviceAlias(String deviceAlias) {
		this.deviceAlias = deviceAlias;
	}
}
