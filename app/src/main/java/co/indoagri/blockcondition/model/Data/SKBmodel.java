package co.indoagri.blockcondition.model.Data;

public class SKBmodel {

    String skbNo;

    public SKBmodel(String skbNo) {
        this.skbNo = skbNo;
    }

    public String getSkbNo() {
        return skbNo;
    }

    public void setSkbNo(String skbNo) {
        this.skbNo = skbNo;
    }
}
