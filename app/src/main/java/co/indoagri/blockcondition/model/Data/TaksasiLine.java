package co.indoagri.blockcondition.model.Data;

public class TaksasiLine{
	private long rowId;
	private String imei; 		// p
	private String companyCode; // p
	private String estate; 		// p
	private String division;	// p
	private String taksasiDate;	// p
	private String block;		// p
	private String crop;		// p
	private int barisSkb;		// p
	private int barisBlock;	// p
	private int lineSkb;		// p
	private double qtyPokok;
	private int qtyJanjang;
	private String gpsKoordinat;
	private int isSave;
	private int status;
	private long createdDate;
	private String createdBy;
	private long modifiedDate;
	private String modifiedBy;

	public static final String TABLE_NAME = "TAKSASI_LINE";
	public static final String XML_FILE = "IT_TAKSASI";
	public static final String XML_DOCUMENT = "TAKSASI_LINE";
	public static final String XML_IMEI = "IMEI";
	public static final String XML_COMPANY_CODE = "COMPANY_CODE";
	public static final String XML_ESTATE = "ESTATE";
	public static final String XML_DIVISION = "DIVISION";
	public static final String XML_TAKSASI_DATE = "TAKSASI_DATE";
	public static final String XML_BLOCK = "BLOCK";
	public static final String XML_CROP = "CROP";
	public static final String XML_BARIS_SKB = "BARIS_SKB";
	public static final String XML_BARIS_BLOCK = "BARIS_BLOCK";
	public static final String XML_LINE_SKB = "LINE_SKB";
	public static final String XML_QTY_POKOK = "QTY_POKOK";
	public static final String XML_QTY_JANJANG = "QTY_JANJANG";
	public static final String XML_GPS_KOORDINAT = "GPS_KOORDINAT";
	public static final String XML_IS_SAVE = "IS_SAVE";
	public static final String XML_STATUS = "STATUS";
	public static final String XML_CREATED_DATE = "CREATED_DATE";
	public static final String XML_CREATED_BY = "CREATED_BY";
	public static final String XML_MODIFIED_DATE = "MODIFIED_DATE";
	public static final String XML_MODIFIED_BY = "MODIFIED_BY";
	
	public static final String XML_DOCUMENT_RESTORE = "ITEM";
	public static final String XML_ITEM_RESTORE = "ITEM";
	public static final String XML_BARIS_BLOCK_RESTORE = "BARIS_BLOCK";
	public static final String XML_BARIS_SKB_RESTORE = "BARIS_SKB";
	public static final String XML_LINE_SKB_RESTORE = "LINE_SKB";
	public static final String XML_QTY_POKOK_RESTORE = "QTY_POKOK";
	public static final String XML_QTY_JANJANG_RESTORE = "QTY_JANJANG";
	
	
	public TaksasiLine(){}
	
	public TaksasiLine(long rowId, String imei, String companyCode,
                       String estate, String division, String taksasiDate, String block, String crop,
                       int barisSkb, int barisBlock, int lineSkb, double qtyPokok, int qtyJanjang,
                       String gpsKoordinat, int isSave, int status, long createdDate,
                       String createdBy, long modifiedDate, String modifiedBy) {
		super();
		this.rowId = rowId;
		this.imei = imei;
		this.companyCode = companyCode;
		this.estate = estate;
		this.division = division;
		this.taksasiDate = taksasiDate;
		this.block = block;
		this.crop = crop;
		this.barisSkb = barisSkb;
		this.barisBlock = barisBlock;
		this.lineSkb = lineSkb;
		this.qtyPokok = qtyPokok;
		this.qtyJanjang = qtyJanjang;
		this.gpsKoordinat = gpsKoordinat;
		this.isSave = isSave;
		this.status = status;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.modifiedDate = modifiedDate;
		this.modifiedBy = modifiedBy;
	}

	public long getRowId() {
		return rowId;
	}

	public void setRowId(long rowId) {
		this.rowId = rowId;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getTaksasiDate() {
		return taksasiDate;
	}

	public void setTaksasiDate(String taksasiDate) {
		this.taksasiDate = taksasiDate;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getCrop() {
		return crop;
	}

	public void setCrop(String crop) {
		this.crop = crop;
	}

	public int getBarisSkb() {
		return barisSkb;
	}

	public void setBarisSkb(int barisSkb) {
		this.barisSkb = barisSkb;
	}

	public int getBarisBlock() {
		return barisBlock;
	}

	public void setBarisBlock(int barisBlock) {
		this.barisBlock = barisBlock;
	}

	public int getLineSkb() {
		return lineSkb;
	}

	public void setLineSkb(int lineSkb) {
		this.lineSkb = lineSkb;
	}
	
	public double getQtyPokok() {
		return qtyPokok;
	}

	public void setQtyPokok(double qtyPokok) {
		this.qtyPokok = qtyPokok;
	}

	public int getQtyJanjang() {
		return qtyJanjang;
	}

	public void setQtyJanjang(int qtyJanjang) {
		this.qtyJanjang = qtyJanjang;
	}

	public String getGpsKoordinat() {
		return gpsKoordinat;
	}

	public void setGpsKoordinat(String gpsKoordinat) {
		this.gpsKoordinat = gpsKoordinat;
	}

	public int getIsSave() {
		return isSave;
	}

	public void setIsSave(int isSave) {
		this.isSave = isSave;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
}
