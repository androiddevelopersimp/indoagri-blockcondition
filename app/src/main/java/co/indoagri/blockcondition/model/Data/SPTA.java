package co.indoagri.blockcondition.model.Data;

/**
 * Created by Anka.Wirawan on 8/4/2016.
 */
public class SPTA {
    private String zyear = "";
    private String imei = "";
    private String sptaNum = "";
    private String companyCode = "";
    private String estate = "";
    private String divisi = "";
    private String sptaDate = "";
    private String subDiv = "";
    private String petakId = "";
    private String vendorId = "";
    private String nopol = "";
    private String logo = "";
    private double jarak = 0;
    private String runAcc1 = "";
    private String emplId1 = "";
    private String runAcc2 = "";
    private String emplId2 = "";
    private String choppedDate = "";
    private String choppedHour = "";
    private String burnDate = "";
    private String burnHour = "";
    private String loadDate = "";
    private String loadHour = "";
    private String quality = "";
    private int insentiveGulma = 0;
    private int insentiveLangsir = 0;
    private int insentiveRoboh = 0;
    private String caneType = "";
    private int costTebang = 0;
    private int costMuat = 0;
    private int costAngkut = 0;
    private int penaltyTrash = 0;
    private String gpsKoordinat = "";
    private int isSave = 0;
    private int status = 0;
    private long createdDate = 0;
    private String createdBy = "";
    private long modifiedDate = 0;
    private String modifiedBy = "";

    public static final String TABLE_NAME = "SPTA";
    public static final String XML_FILE = "IT_SPTA";
    public static final String XML_DOCUMENT = "ITEM";
    public static final String XML_ITEM = "ITEM";
    public static final String XML_ZYEAR = "ZYEAR";
    public static final String XML_IMEI = "IMEI";
    public static final String XML_SPTA_NUM = "SPTANUM";
    public static final String XML_COMPANY_CODE = "COMPANY_CODE";
    public static final String XML_ESTATE = "ESTATE";
    public static final String XML_DIVISI = "DIVISI";
    public static final String XML_SPTA_DATE = "SPTA_DATE";
    public static final String XML_SUB_DIV = "SUB_DIV";
    public static final String XML_PETAK_ID = "PETAK_ID";
    public static final String XML_VENDOR_ID = "VENDOR_ID";
    public static final String XML_NOPOL = "NOPOL";
    public static final String XML_LOGO = "LOGO";
    public static final String XML_JARAK = "JARAK";
    public static final String XML_RUN_ACC_1 = "RUN_ACC_1";
    public static final String XML_EMPL_ID_1 = "EMPL_ID_1";
    public static final String XML_RUN_ACC_2 = "RUN_ACC_2";
    public static final String XML_EMPL_ID_2 = "EMPL_ID_2";
    public static final String XML_CHOPPED_DATE = "CHOPPED_DATE";
    public static final String XML_CHOPPED_HOUR = "CHOPPED_HOUR";
    public static final String XML_BURN_DATE = "BURN_DATE";
    public static final String XML_BURN_HOUR = "BURN_HOUR";
    public static final String XML_LOAD_DATE = "LOAD_DATE";
    public static final String XML_LOAD_HOUR = "LOAD_HOUR";
    public static final String XML_QUALITY = "QUALITY";
    public static final String XML_INSENTIVE_GULMA = "INSENTIVE_GULMA";
    public static final String XML_INSENTIVE_LANGSIR = "INSENTIVE_LANGSIR";
    public static final String XML_INSENTIVE_ROBOH = "INSENTIVE_ROBOH";
    public static final String XML_CANE_TYPE = "CANE_TYPE";
    public static final String XML_COST_TEBANG = "COST_TEBANG";
    public static final String XML_COST_MUAT = "COST_MUAT";
    public static final String XML_COST_ANGKUT = "COST_ANGKUT";
    public static final String XML_PENALTY_TRASH = "PENALTY_TRASH";
    public static final String XML_GPS_KOORDINAT = "GPS_KOORDINAT";
    public static final String XML_IS_SAVE = "IS_SAVE";
    public static final String XML_STATUS = "STATUS";
    public static final String XML_CREATED_DATE = "CREATED_DATE";
    public static final String XML_CREATED_BY = "CREATED_BY";
    public static final String XML_MODIFIED_DATE = "MODIFIED_DATE";
    public static final String XML_MODIFIED_BY = "MODIFIED_BY";

    public SPTA() {
    }

    public SPTA(String zyear, String imei, String sptaNum, String companyCode, String estate, String divisi,
                String sptaDate, String subDiv, String petakId, String vendorId, String nopol, String logo,
                double jarak, String runAcc1, String emplId1, String runAcc2, String emplId2, String choppedDate,
                String choppedHour, String burnDate, String burnHour, String loadDate, String loadHour,
                String quality, int insentiveGulma, int insentiveLangsir, int insentiveRoboh, String caneType,
                int costTebang, int costMuat, int costAngkut, int penaltyTrash, String gpsKoordinat,
                int isSave, int status, long createdDate, String createdBy, long modifiedDate, String modifiedBy) {
        this.zyear = zyear;
        this.imei = imei;
        this.sptaNum = sptaNum;
        this.companyCode = companyCode;
        this.estate = estate;
        this.divisi = divisi;
        this.sptaDate = sptaDate;
        this.subDiv = subDiv;
        this.petakId = petakId;
        this.vendorId = vendorId;
        this.nopol = nopol;
        this.logo = logo;
        this.jarak = jarak;
        this.runAcc1 = runAcc1;
        this.emplId1 = emplId1;
        this.runAcc2 = runAcc2;
        this.emplId2 = emplId2;
        this.choppedDate = choppedDate;
        this.choppedHour = choppedHour;
        this.burnDate = burnDate;
        this.burnHour = burnHour;
        this.loadDate = loadDate;
        this.loadHour = loadHour;
        this.quality = quality;
        this.insentiveGulma = insentiveGulma;
        this.insentiveLangsir = insentiveLangsir;
        this.insentiveRoboh = insentiveRoboh;
        this.caneType = caneType;
        this.costTebang = costTebang;
        this.costMuat = costMuat;
        this.costAngkut = costAngkut;
        this.penaltyTrash = penaltyTrash;
        this.gpsKoordinat = gpsKoordinat;
        this.isSave = isSave;
        this.status = status;
        this.createdDate = createdDate;
        this.createdBy = createdBy;
        this.modifiedDate = modifiedDate;
        this.modifiedBy = modifiedBy;
    }

    public String getZyear() {
        return zyear;
    }

    public void setZyear(String zyear) {
        this.zyear = zyear;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSptaNum() {
        return sptaNum;
    }

    public void setSptaNum(String sptaNum) {
        this.sptaNum = sptaNum;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getDivisi() {
        return divisi;
    }

    public void setDivisi(String divisi) {
        this.divisi = divisi;
    }

    public String getSptaDate() {
        return sptaDate;
    }

    public void setSptaDate(String sptaDate) {
        this.sptaDate = sptaDate;
    }

    public String getSubDiv() {
        return subDiv;
    }

    public void setSubDiv(String subDiv) {
        this.subDiv = subDiv;
    }

    public String getPetakId() {
        return petakId;
    }

    public void setPetakId(String petakId) {
        this.petakId = petakId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getNopol() {
        return nopol;
    }

    public void setNopol(String nopol) {
        this.nopol = nopol;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public double getJarak() {
        return jarak;
    }

    public void setJarak(double jarak) {
        this.jarak = jarak;
    }

    public String getRunAcc1() {
        return runAcc1;
    }

    public void setRunAcc1(String runAcc1) {
        this.runAcc1 = runAcc1;
    }

    public String getEmplId1() {
        return emplId1;
    }

    public void setEmplId1(String emplId1) {
        this.emplId1 = emplId1;
    }

    public String getRunAcc2() {
        return runAcc2;
    }

    public void setRunAcc2(String runAcc2) {
        this.runAcc2 = runAcc2;
    }

    public String getEmplId2() {
        return emplId2;
    }

    public void setEmplId2(String emplId2) {
        this.emplId2 = emplId2;
    }

    public String getChoppedDate() {
        return choppedDate;
    }

    public void setChoppedDate(String choppedDate) {
        this.choppedDate = choppedDate;
    }

    public String getChoppedHour() {
        return choppedHour;
    }

    public void setChoppedHour(String choppedHour) {
        this.choppedHour = choppedHour;
    }

    public String getBurnDate() {
        return burnDate;
    }

    public void setBurnDate(String burnDate) {
        this.burnDate = burnDate;
    }

    public String getBurnHour() {
        return burnHour;
    }

    public void setBurnHour(String burnHour) {
        this.burnHour = burnHour;
    }

    public String getLoadDate() {
        return loadDate;
    }

    public void setLoadDate(String loadDate) {
        this.loadDate = loadDate;
    }

    public String getLoadHour() {
        return loadHour;
    }

    public void setLoadHour(String loadHour) {
        this.loadHour = loadHour;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public int getInsentiveGulma() {
        return insentiveGulma;
    }

    public void setInsentiveGulma(int insentiveGulma) {
        this.insentiveGulma = insentiveGulma;
    }

    public int getInsentiveLangsir() {
        return insentiveLangsir;
    }

    public void setInsentiveLangsir(int insentiveLangsir) {
        this.insentiveLangsir = insentiveLangsir;
    }

    public int getInsentiveRoboh() {
        return insentiveRoboh;
    }

    public void setInsentiveRoboh(int insentiveRoboh) {
        this.insentiveRoboh = insentiveRoboh;
    }

    public String getCaneType() {
        return caneType;
    }

    public void setCaneType(String caneType) {
        this.caneType = caneType;
    }

    public int getCostTebang() {
        return costTebang;
    }

    public void setCostTebang(int costTebang) {
        this.costTebang = costTebang;
    }

    public int getCostMuat() {
        return costMuat;
    }

    public void setCostMuat(int costMuat) {
        this.costMuat = costMuat;
    }

    public int getCostAngkut() {
        return costAngkut;
    }

    public void setCostAngkut(int costAngkut) {
        this.costAngkut = costAngkut;
    }

    public int getPenaltyTrash() {
        return penaltyTrash;
    }

    public void setPenaltyTrash(int penaltyTrash) {
        this.penaltyTrash = penaltyTrash;
    }

    public String getGpsKoordinat() {
        return gpsKoordinat;
    }

    public void setGpsKoordinat(String gpsKoordinat) {
        this.gpsKoordinat = gpsKoordinat;
    }

    public int getIsSave() {
        return isSave;
    }

    public void setIsSave(int isSave) {
        this.isSave = isSave;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public long getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(long modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

}
