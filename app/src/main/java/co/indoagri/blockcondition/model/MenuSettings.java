package co.indoagri.blockcondition.model;

public class MenuSettings {
    public String text;
    public int drawable;
    public String color;
    public boolean active;
    public String ActivityClass;

    public MenuSettings(String t, int d, String c, boolean act, String cls )
    {
        text=t;
        drawable=d;
        color=c;
        active = act;
        ActivityClass = cls;
    }
}
