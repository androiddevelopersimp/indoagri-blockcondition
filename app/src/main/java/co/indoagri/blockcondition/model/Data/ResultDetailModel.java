package co.indoagri.blockcondition.model.Data;


public class ResultDetailModel {
    private String Block;
    private String SKB;
    private String Condition;
    private String BlockRow;
    private String PokokLabel;
    private String PokokCondition;
    private String CensusPoint;
    private String PokokSide;
    private String Flag;
    private String ZDate;
    private String PROD_TREES;
    public static final String XML_Condition= "Condition";
    public ResultDetailModel() {
    }

    public ResultDetailModel(String block, String SKB, String condition, String blockRow, String ZDate,String pokokLabel, String pokokCondition,String censusPoint, String pokokSide, String flag, String PROD_TREES) {
        Block = block;
        this.SKB = SKB;
        Condition = condition;
        ZDate = ZDate;
        BlockRow = blockRow;
        PokokLabel = pokokLabel;
        PokokCondition = pokokCondition;
        CensusPoint = censusPoint;
        PokokSide = pokokSide;
        Flag = flag;
        this.PROD_TREES = PROD_TREES;
    }

    public String getZDate() {
        return ZDate;
    }

    public void setZDate(String ZDate) {
        this.ZDate = ZDate;
    }

    public String getPokokCondition() {
        return PokokCondition;
    }

    public void setPokokCondition(String pokokCondition) {
        PokokCondition = pokokCondition;
    }

    public String getCensusPoint() {
        return CensusPoint;
    }

    public void setCensusPoint(String censusPoint) {
        CensusPoint = censusPoint;
    }

    public String getBlock() {
        return Block;
    }

    public void setBlock(String block) {
        Block = block;
    }

    public String getCondition() {
        return Condition;
    }

    public void setCondition(String condition) {
        Condition = condition;
    }

    public String getSKB() {
        return SKB;
    }

    public void setSKB(String SKB) {
        this.SKB = SKB;
    }

    public String getBlockRow() {
        return BlockRow;
    }

    public void setBlockRow(String blockRow) {
        BlockRow = blockRow;
    }

    public String getPokokLabel() {
        return PokokLabel;
    }

    public void setPokokLabel(String pokokLabel) {
        PokokLabel = pokokLabel;
    }

    public String getPokokSide() {
        return PokokSide;
    }

    public void setPokokSide(String pokokSide) {
        PokokSide = pokokSide;
    }

    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public String getPROD_TREES() {
        return PROD_TREES;
    }

    public void setPROD_TREES(String PROD_TREES) {
        this.PROD_TREES = PROD_TREES;
    }

    @Override
    public String toString() {
        return "ResultDetailModel{" +
                "Block='" + Block + '\'' +
                ", SKB='" + SKB + '\'' +
                ", Condition='" + Condition + '\'' +
                ", BlockRow='" + BlockRow + '\'' +
                ", PokokLabel='" + PokokLabel + '\'' +
                ", PokokCondition='" + PokokCondition + '\'' +
                ", CensusPoint='" + CensusPoint + '\'' +
                ", PokokSide='" + PokokSide + '\'' +
                ", Flag='" + Flag + '\'' +
                ", PROD_TREES='" + PROD_TREES + '\'' +
                '}';
    }
}
