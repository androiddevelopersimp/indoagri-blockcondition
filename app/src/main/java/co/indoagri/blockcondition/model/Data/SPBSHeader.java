package co.indoagri.blockcondition.model.Data;

import android.os.Parcel;
import android.os.Parcelable;

public class SPBSHeader implements Parcelable {
	private long rowId = 0;
	private String imei = ""; 			// p
	private String year = ""; 			// p
	private String companyCode = ""; 	// p
	private String estate = ""; 		// p
	private String crop = ""; 			// p
	private String spbsNumber = ""; 	// p
	private String spbsDate = "";
	private String destId = "";
	private String destDesc = "";
	private String destType = "";
	private String division = "";
	private String nikAssistant = "";
	private String assistant = "";
	private String nikClerk = "";
	private String clerk = "";
	private String nikDriver = "";
	private String driver = "";
	private String nikKernet = "";
	private String kernet = "";
	private String licensePlate = "";
	private String runningAccount = "";
	private String gpsKoordinat = "";
	private int isSave = 0;
	private int status = 0;
	private long createdDate = 0;
	private String createdBy = "";
	private long modifiedDate = 0;
	private String modifiedBy = "";
	private String lifnr = "";

	public static final String TABLE_NAME = "SPBS_HEADER";
	public static final String XML_FILE = "IT_SPBS";
	public static final String XML_DOCUMENT = "SPBS_HEADER";
	public static final String XML_IMEI = "IMEI";
	public static final String XML_YEAR = "YEAR";
	public static final String XML_COMPANY_CODE = "COMPANY_CODE";
	public static final String XML_ESTATE = "ESTATE";
	public static final String XML_CROP = "CROP";
	public static final String XML_SPBS_NUMBER = "SPBS_NUMBER";
	public static final String XML_SPBS_DATE = "SPBS_DATE";
	public static final String XML_DEST_ID = "DEST_ID";
	public static final String XML_DEST_DESC = "DEST_DESC";
	public static final String XML_DEST_TYPE = "DEST_TYPE";
	public static final String XML_DIVISION = "DIVISION";
	public static final String XML_NIK_ASSISTANT = "NIK_ASSISTANT";
	public static final String XML_ASSISTANT = "ASSISTANT";
	public static final String XML_NIK_CLERK = "NIK_CLERK";
	public static final String XML_CLERK = "CLERK";
	public static final String XML_NIK_DRIVER = "NIK_DRIVER";
	public static final String XML_DRIVER = "DRIVER";
	public static final String XML_NIK_KERNET = "NIK_KERNET";
	public static final String XML_KERNET = "KERNET";
	public static final String XML_LICENSE_PLATE = "LICENSE_PLATE";
	public static final String XML_RUNNING_ACCOUNT = "RUNNING_ACCOUNT";
	public static final String XML_GPS_KOORDINAT = "GPS_KOORDINAT";
	public static final String XML_IS_SAVE = "IS_SAVE";
	public static final String XML_STATUS = "STATUS";
	public static final String XML_CREATED_DATE = "CREATED_DATE";
	public static final String XML_CREATED_BY = "CREATED_BY";
	public static final String XML_MODIFIED_DATE = "MODIFIED_DATE";
	public static final String XML_MODIFIED_BY = "MODIFIED_BY";
	public static final String XML_BLOCKS = "BLOCKS";

	public static final String XML_DOCUMENT_RESTORE = "SPBS";
	public static final String XML_ITEM_RESTORE = "HEADER";
	public static final String XML_IMEI_RESTORE = "IMEI";
	public static final String XML_YEAR_RESTORE = "YEAR";
	public static final String XML_COMPANY_CODE_RESTORE = "COMPANY";
	public static final String XML_ESTATE_RESTORE = "ESTATE";
	public static final String XML_CROP_RESTORE = "CROP";
	public static final String XML_SPBS_NUMBER_RESTORE = "SPBS_NO";
	public static final String XML_SPBS_DATE_RESTORE = "SPBS_DATE";
	public static final String XML_DIVISION_RESTORE = "DIVISION";
	public static final String XML_NIK_ASSISTANT_RESTORE = "NIK_ASSISTANT";
	public static final String XML_NIK_CLERK_RESTORE = "NIK_KRANI";
	public static final String XML_NIK_DRIVER_RESTORE = "NIK_DRIVER";
	public static final String XML_DRIVER_RESTORE = "DRIVER";
	public static final String XML_NIK_KERNET_RESTORE = "NIK_KERNET";
	public static final String XML_RUNNING_ACCOUNT_RESTORE = "RUNNING_ACCOUNT";
	public static final String XML_LICENSE_PLATE_RESTORE = "LICENSE_PLATE";
	public static final String XML_CREATED_DATE_RESTORE = "CREATED_DARE";
	public static final String XML_CREATED_BY_RESTORE = "CREATED_BY";
	public static final String XML_LIFNR = "LIFNR";
	
	public SPBSHeader() {
	}

	public SPBSHeader(long rowId, String imei, String year, String companyCode,
                      String estate, String crop, String spbsNumber, String spbsDate,
                      String destId, String destDesc, String destType, String division, String nikAssistant, String assistant,
                      String nikClerk, String clerk, String nikDriver, String driver, String nikKernet, String kernet,
                      String licensePlate, String runningAccount, String gpsKoordinat, int isSave,
                      int status, long createdDate, String createdBy, long modifiedDate,
                      String modifiedBy, String lifnr) {
		super();
		this.rowId = rowId;
		this.imei = imei;
		this.year = year;
		this.companyCode = companyCode;
		this.estate = estate;
		this.crop = crop;
		this.spbsNumber = spbsNumber;
		this.spbsDate = spbsDate;
		this.destId =  destId;
		this.destDesc = destDesc;
		this.destType = destType;
		this.division = division;
		this.nikAssistant = nikAssistant;
		this.assistant = assistant;
		this.nikClerk = nikClerk;
		this.clerk = clerk;
		this.nikDriver = nikDriver;
		this.driver = driver;
		this.nikKernet = nikKernet;
		this.kernet = kernet;
		this.licensePlate = licensePlate;
		this.runningAccount = runningAccount;
		this.gpsKoordinat = gpsKoordinat;
		this.isSave = isSave;
		this.status = status;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.modifiedDate = modifiedDate;
		this.modifiedBy = modifiedBy;
		this.lifnr = lifnr;
	}

	public long getRowId() {
		return rowId;
	}

	public void setRowId(long rowId) {
		this.rowId = rowId;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getCrop() {
		return crop;
	}

	public void setCrop(String crop) {
		this.crop = crop;
	}

	public String getSpbsNumber() {
		return spbsNumber;
	}

	public void setSpbsNumber(String spbsNumber) {
		this.spbsNumber = spbsNumber;
	}

	public String getSpbsDate() {
		return spbsDate;
	}

	public void setSpbsDate(String spbsDate) {
		this.spbsDate = spbsDate;
	}

	public String getDestId() {
		return destId;
	}

	public void setDestId(String destId) {
		this.destId = destId;
	}

	public String getDestDesc() {
		return destDesc;
	}

	public void setDestDesc(String destDesc) {
		this.destDesc = destDesc;
	}

	public String getDestType() {
		return destType;
	}

	public void setDestType(String destType) {
		this.destType = destType;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getNikAssistant() {
		return nikAssistant;
	}

	public void setNikAssistant(String nikAssistant) {
		this.nikAssistant = nikAssistant;
	}

	public String getAssistant() {
		return assistant;
	}

	public void setAssistant(String assistant) {
		this.assistant = assistant;
	}

	public String getNikClerk() {
		return nikClerk;
	}

	public void setNikClerk(String nikClerk) {
		this.nikClerk = nikClerk;
	}

	public String getClerk() {
		return clerk;
	}

	public void setClerk(String clerk) {
		this.clerk = clerk;
	}

	public String getNikDriver() {
		return nikDriver;
	}

	public void setNikDriver(String nikDriver) {
		this.nikDriver = nikDriver;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}
	
	public String getNikKernet() {
		return nikKernet;
	}

	public void setNikKernet(String nikKernet) {
		this.nikKernet = nikKernet;
	}

	public String getKernet() {
		return kernet;
	}

	public void setKernet(String kernet) {
		this.kernet = kernet;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getRunningAccount() {
		return runningAccount;
	}

	public void setRunningAccount(String runningAccount) {
		this.runningAccount = runningAccount;
	}

	public String getGpsKoordinat() {
		return gpsKoordinat;
	}

	public void setGpsKoordinat(String gpsKoordinat) {
		this.gpsKoordinat = gpsKoordinat;
	}

	public int getIsSave() {
		return isSave;
	}

	public void setIsSave(int isSave) {
		this.isSave = isSave;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getLifnr() {
		return lifnr;
	}

	public void setLifnr(String lifnr) {
		this.lifnr = lifnr;
	}

	public static final Parcelable.Creator<SPBSHeader> CREATOR = new Creator<SPBSHeader>() {

		@Override
		public SPBSHeader createFromParcel(Parcel parcel) {
			SPBSHeader bpnHeader = new SPBSHeader(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString(), 
					parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), 
					parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), 
					parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readInt(), parcel.readInt(), parcel.readLong(), parcel.readString(), parcel.readLong(), parcel.readString(),
					parcel.readString());
			
			return bpnHeader;
		}

		@Override
		public SPBSHeader[] newArray(int size) {
			return new SPBSHeader[size];
		}
	};
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeLong(rowId);
		parcel.writeString(imei);
		parcel.writeString(year);
		parcel.writeString(companyCode);
		parcel.writeString(estate);
		parcel.writeString(crop);
		parcel.writeString(spbsNumber);
		parcel.writeString(spbsDate);
		parcel.writeString(destId);
		parcel.writeString(destDesc);
		parcel.writeString(destType);
		parcel.writeString(division);
		parcel.writeString(nikAssistant);
		parcel.writeString(assistant);
		parcel.writeString(nikClerk);
		parcel.writeString(clerk);
		parcel.writeString(nikDriver);
		parcel.writeString(driver);
		parcel.writeString(nikKernet);
		parcel.writeString(kernet);
		parcel.writeString(licensePlate);
		parcel.writeString(runningAccount);
		parcel.writeString(gpsKoordinat);
		parcel.writeInt(isSave);
		parcel.writeInt(status);
		parcel.writeLong(createdDate);
		parcel.writeString(createdBy);
		parcel.writeLong(modifiedDate);
		parcel.writeString(modifiedBy);
		parcel.writeString(lifnr);
	}

}
