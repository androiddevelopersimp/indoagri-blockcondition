package co.indoagri.blockcondition.model.RKH;

public class RKH_ACTIVITY_TYPE {
    public String ACTTYPE;
    public String NAME;
    public static final String TABLE_NAME = "RKH_ACTIVITY_TYPE";
    public static final String XML_DOCUMENT = "IT_ACTTYP";
    public static final String XML_ITEM = "ITEM";
    public static final String ALIAS = "RKH ACTIVITY TAPE";
    public static final String XML_ACTTYPE = "ACTTYPE";
    public static final String XML_NAME = "NAME";
    public String getACTTYPE() {
        return ACTTYPE;
    }

    public void setACTTYPE(String ACTTYPE) {
        this.ACTTYPE = ACTTYPE;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public static String getTableName() {
        return TABLE_NAME;
    }
    public RKH_ACTIVITY_TYPE(){

    }
    public RKH_ACTIVITY_TYPE(String ACTTYPE, String NAME){
        this.ACTTYPE = ACTTYPE;
        this.NAME = NAME;
    }
}
