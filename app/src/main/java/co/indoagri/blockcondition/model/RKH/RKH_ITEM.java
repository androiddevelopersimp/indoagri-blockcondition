package co.indoagri.blockcondition.model.RKH;

import android.os.Parcel;
import android.os.Parcelable;

public class RKH_ITEM implements Parcelable {

    public String RKH_ID;
    public String LINE;
    public String ACTIVITY;
    public String BLOCK;
    public String TARGET_OUTPUT;
    public String SKU;
    public String PHL;
    public String TARGET_HASIL;
    public String TARGET_JANJANG;
    public String BJR;
    public String GPS_KOORDINAT;
    public int STATUS = 0;
    private long CREATED_DATE;
    public String CREATED_BY;
    private long MODIFIED_DATE;
    public String MODIFIED_BY;

    public static final String TABLE_NAME = "RKH_ITEM";
    public static final String XML_DOCUMENT = "RKH_ITEM";
    public static final String XML_FILE = "RKH_ITEM";
    public static final String XML_RKH_ID = "RKH_ID";
    public static final String XML_LINE = "LINE";
    public static final String XML_ACTIVITY = "ACTIVITY";
    public static final String XML_BLOCK = "BLOCK";
    public static final String XML_TARGET_OUTPUT = "TARGET_OUTPUT";
    public static final String XML_SKU = "SKU";
    public static final String XML_PHL = "PHL";
    public static final String XML_TARGET_HASIL = "TARGET_HASIL";
    public static final String XML_TARGET_JANJANG = "TARGET_JANJANG";
    public static final String XML_BJR = "BJR";
    public static final String XML_GPS_KOORDINAT ="GPS_KOORDINAT";
    public static final String XML_STATUS = "STATUS";
    public static final String XML_CREATED_DATE = "CREATED_DATE";
    public static final String XML_CREATED_BY ="CREATED_BY";
    public static final String XML_MODIFIED_DATE = "MODIFIED_DATE";
    public static final String XML_MODIFIED_BY = "MODIFIED_BY";
    public RKH_ITEM(){}
    public RKH_ITEM(String RKH_ID,
                    String LINE,
                    String ACTIVITY,
                    String BLOCK,
                    String TARGET_OUTPUT,
                    String SKU,
                    String PHL,
                    String TARGET_HASIL,
                    String TARGET_JANJANG,
                    String BJR,
                    String GPS_KOORDINAT,
                    int STATUS,
                    long CREATED_DATE,
                    String CREATED_BY,
                    long MODIFIED_DATE,
                    String MODIFIED_BY)
    {
       this.RKH_ID = RKH_ID;
       this.LINE = LINE;
       this.ACTIVITY = ACTIVITY;
       this.BLOCK = BLOCK;
       this.TARGET_OUTPUT = TARGET_OUTPUT;
       this.TARGET_HASIL = TARGET_HASIL;
        this.TARGET_JANJANG = TARGET_JANJANG;
        this.SKU = SKU;
        this.PHL = PHL;
        this.BJR = BJR;
        this.GPS_KOORDINAT = GPS_KOORDINAT;
        this.STATUS = STATUS;
        this.MODIFIED_BY = MODIFIED_BY;
        this.MODIFIED_DATE = MODIFIED_DATE;
        this.CREATED_BY = CREATED_BY;
        this.CREATED_DATE = CREATED_DATE;

    }

    public String getRKH_ID() {
        return RKH_ID;
    }

    public void setRKH_ID(String RKH_ID) {
        this.RKH_ID = RKH_ID;
    }

    public String getLINE() {
        return LINE;
    }

    public void setLINE(String LINE) {
        this.LINE = LINE;
    }

    public String getACTIVITY() {
        return ACTIVITY;
    }

    public void setACTIVITY(String ACTIVITY) {
        this.ACTIVITY = ACTIVITY;
    }

    public String getBLOCK() {
        return BLOCK;
    }

    public void setBLOCK(String BLOCK) {
        this.BLOCK = BLOCK;
    }

    public String getTARGET_OUTPUT() {
        return TARGET_OUTPUT;
    }

    public void setTARGET_OUTPUT(String TARGET_OUTPUT) {
        this.TARGET_OUTPUT = TARGET_OUTPUT;
    }

    public String getSKU() {
        return SKU;
    }

    public void setSKU(String SKU) {
        this.SKU = SKU;
    }

    public String getPHL() {
        return PHL;
    }

    public void setPHL(String PHL) {
        this.PHL = PHL;
    }

    public String getTARGET_HASIL() {
        return TARGET_HASIL;
    }

    public void setTARGET_HASIL(String TARGET_HASIL) {
        this.TARGET_HASIL = TARGET_HASIL;
    }

    public String getTARGET_JANJANG() {
        return TARGET_JANJANG;
    }

    public void setTARGET_JANJANG(String TARGET_JANJANG) {
        this.TARGET_JANJANG = TARGET_JANJANG;
    }

    public String getBJR() {
        return BJR;
    }

    public void setBJR(String BJR) {
        this.BJR = BJR;
    }

    public String getGPS_KOORDINAT() {
        return GPS_KOORDINAT;
    }

    public void setGPS_KOORDINAT(String GPS_KOORDINAT) {
        this.GPS_KOORDINAT = GPS_KOORDINAT;
    }

    public int getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(int STATUS) {
        this.STATUS = STATUS;
    }

    public long getCREATED_DATE() {
        return CREATED_DATE;
    }

    public void setCREATED_DATE(long CREATED_DATE) {
        this.CREATED_DATE = CREATED_DATE;
    }

    public String getCREATED_BY() {
        return CREATED_BY;
    }

    public void setCREATED_BY(String CREATED_BY) {
        this.CREATED_BY = CREATED_BY;
    }

    public long getMODIFIED_DATE() {
        return MODIFIED_DATE;
    }

    public void setMODIFIED_DATE(long MODIFIED_DATE) {
        this.MODIFIED_DATE = MODIFIED_DATE;
    }

    public String getMODIFIED_BY() {
        return MODIFIED_BY;
    }

    public void setMODIFIED_BY(String MODIFIED_BY) {
        this.MODIFIED_BY = MODIFIED_BY;
    }

    public static String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.RKH_ID);
        dest.writeString(this.LINE);
        dest.writeString(this.ACTIVITY);
        dest.writeString(this.BLOCK);
        dest.writeString(this.TARGET_OUTPUT);
        dest.writeString(this.SKU);
        dest.writeString(this.PHL);
        dest.writeString(this.TARGET_HASIL);
        dest.writeString(this.TARGET_JANJANG);
        dest.writeString(this.BJR);
        dest.writeString(this.GPS_KOORDINAT);
        dest.writeInt(this.STATUS);
        dest.writeLong(this.CREATED_DATE);
        dest.writeString(this.CREATED_BY);
        dest.writeLong(this.MODIFIED_DATE);
        dest.writeString(this.MODIFIED_BY);
    }

    protected RKH_ITEM(Parcel in) {
        this.RKH_ID = in.readString();
        this.LINE = in.readString();
        this.ACTIVITY = in.readString();
        this.BLOCK = in.readString();
        this.TARGET_OUTPUT = in.readString();
        this.SKU = in.readString();
        this.PHL = in.readString();
        this.TARGET_HASIL = in.readString();
        this.TARGET_JANJANG = in.readString();
        this.BJR = in.readString();
        this.GPS_KOORDINAT = in.readString();
        this.STATUS = in.readInt();
        this.CREATED_DATE = in.readLong();
        this.CREATED_BY = in.readString();
        this.MODIFIED_DATE = in.readLong();
        this.MODIFIED_BY = in.readString();
    }

    public static final Parcelable.Creator<RKH_ITEM> CREATOR = new Parcelable.Creator<RKH_ITEM>() {
        @Override
        public RKH_ITEM createFromParcel(Parcel source) {
            return new RKH_ITEM(source);
        }

        @Override
        public RKH_ITEM[] newArray(int size) {
            return new RKH_ITEM[size];
        }
    };
}
