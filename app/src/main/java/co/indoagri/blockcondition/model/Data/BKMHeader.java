package co.indoagri.blockcondition.model.Data;

import android.os.Parcel;
import android.os.Parcelable;

public class BKMHeader implements Parcelable {
	private long rowId = 0; 
	private String imei = "";			//p
	private String companyCode = "";		//p
	private String estate = "";			//p
	private String bkmDate = "";			//p
	private String division = "";		//p
	private String gang = "";			//p
	private String nikForeman = "";
	private String foreman = "";
	private String nikClerk = "";
	private String clerk = "";
	private String gpsKoordinat = "0.0:0.0";
	private int status = 0;
	private long createdDate = 0;
	private String createdBy = "";
	private long modifiedDate = 0;
	private String modifiedBy = "";
	
	public static final String TABLE_NAME = "BKM_HEADER";
	public final static String XML_FILE = "IT_BKM";
	public static final String XML_DOCUMENT = "BKM_HEADER";
	public static final String XML_IMEI = "IMEI";
	public static final String XML_COMPANY_CODE = "COMPANY_CODE";
	public static final String XML_ESTATE = "ESTATE";
	public static final String XML_BKM_DATE = "BKM_DATE";
	public static final String XML_DIVISION = "DIVISION";
	public static final String XML_GANG = "GANG";
	public static final String XML_NIK_FOREMAN = "NIK_FOREMAN";
	public static final String XML_FOREMAN = "FOREMAN";
	public static final String XML_NIK_CLERK = "NIK_CLERK";
	public static final String XML_CLERK = "CLERK";
	public static final String XML_GPS_KOORDINAT = "GPS_KOORDINAT";
	public static final String XML_STATUS = "STATUS";
	public static final String XML_CREATED_DATE = "CREATED_DATE";
	public static final String XML_CREATED_BY = "CREATED_BY";
	public static final String XML_MODIFIED_DATE = "MODIFIED_DATE";
	public static final String XML_MODIFIED_BY = "MODIFIED_BY";
	
	public static final String XML_DOCUMENT_RESTORE = "ABSENT_HEADER";
	public static final String XML_ITEM_RESTORE = "ITEM";
	public static final String XML_BKM_DATE_RESTORE = "BKMDATE";
	public static final String XML_IMEI_RESTORE = "IMEI";
	public static final String XML_ESTATE_RESTORE = "ESTATE";
	public static final String XML_DIVISION_RESTORE = "DIVISION";
	public static final String XML_GANG_RESTORE = "GANG";
	public static final String XML_NIK_FOREMAN_RESTORE = "FOREMAN";
	public static final String XML_NIK_CLERK_RESTORE = "CLERK";
	public static final String XML_CREATED_DATE_RESTORE = "CREATEDDATE";
	public static final String XML_CREATED_BY_RESTORE = "CREATEDBY";
	
	public BKMHeader(){}

	public BKMHeader(long rowId, String imei, String companyCode, String estate,
                     String bkmDate, String division, String gang, String nikForeman,
                     String foreman, String nikClerk, String clerk, String gpsKoordinat, int status,
                     long createdDate, String createdBy, long modifiedDate,
                     String modifiedBy) {
		super();
		this.rowId = rowId;
		this.imei = imei;
		this.companyCode = companyCode;
		this.estate = estate;
		this.bkmDate = bkmDate;
		this.division = division;
		this.gang = gang;
		this.nikForeman = nikForeman;
		this.foreman = foreman;
		this.nikClerk = nikClerk;
		this.clerk = clerk;
		this.gpsKoordinat = gpsKoordinat;
		this.status = status;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.modifiedDate = modifiedDate;
		this.modifiedBy = modifiedBy;
	}

	public long getRowId(){
		return rowId;
	}
	
	public void setRowId(long rowId){
		this.rowId = rowId;
	}
	
	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getBkmDate() {
		return bkmDate;
	}

	public void setBkmDate(String bkmDate) {
		this.bkmDate = bkmDate;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getGang() {
		return gang;
	}

	public void setGang(String gang) {
		this.gang = gang;
	}

	public String getNikForeman() {
		return nikForeman;
	}

	public void setNikForeman(String nikForeman) {
		this.nikForeman = nikForeman;
	}

	public String getForeman() {
		return foreman;
	}

	public void setForeman(String foreman) {
		this.foreman = foreman;
	}

	public String getNikClerk() {
		return nikClerk;
	}

	public void setNikClerk(String nikClerk) {
		this.nikClerk = nikClerk;
	}

	public String getClerk() {
		return clerk;
	}

	public void setClerk(String clerk) {
		this.clerk = clerk;
	}

	public String getGpsKoordinat() {
		return gpsKoordinat;
	}

	public void setGpsKoordinat(String gpsKoordinat) {
		this.gpsKoordinat = gpsKoordinat;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	public static final Parcelable.Creator<BKMHeader> CREATOR = new Creator<BKMHeader>() {

		@Override
		public BKMHeader createFromParcel(Parcel parcel) {
			BKMHeader bpnHeader = new BKMHeader(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), 
					parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), 
					parcel.readString(), parcel.readInt(), parcel.readLong(), parcel.readString(), parcel.readLong(), parcel.readString());
			
			return bpnHeader;
		}

		@Override
		public BKMHeader[] newArray(int size) {
			return new BKMHeader[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeLong(rowId);
		parcel.writeString(imei);
		parcel.writeString(companyCode);
		parcel.writeString(estate);
		parcel.writeString(bkmDate);
		parcel.writeString(division);
		parcel.writeString(gang);
		parcel.writeString(nikForeman);
		parcel.writeString(foreman);
		parcel.writeString(nikClerk);
		parcel.writeString(clerk);
		parcel.writeString(gpsKoordinat);
		parcel.writeInt(status);
		parcel.writeLong(createdDate);
		parcel.writeString(createdBy);
		parcel.writeLong(modifiedDate);
		parcel.writeString(modifiedBy);
	}
}
