package co.indoagri.blockcondition.model.Data;


public class MasterDownload {
    private long rowId;
    private String name; 		// p
    private String fileName; 	// p
    private long syncDate; 		// p
    private int status; 		// 0 = not found, 1 = success, -1 = failed

    public static final String TABLE_NAME = "MASTER_DOWNLOAD";
    public static final String XML_ID = "ID";
    public static final String XML_NAME = "NAME";
    public static final String XML_FILENAME = "FILENAME";
    public static final String XML_SYNC_DATE = "SYNC_DATE";
    public static final String XML_STATUS = "STATUS";

    public MasterDownload() {}

    public MasterDownload(long rowId, String name, String fileName,
                          long syncDate, int status) {
        super();
        this.rowId = rowId;
        this.name = name;
        this.fileName = fileName;
        this.syncDate = syncDate;
        this.status = status;
    }

    public long getRowId() {
        return rowId;
    }

    public void setId(long rowId) {
        this.rowId = rowId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getSyncDate() {
        return syncDate;
    }

    public void setSyncDate(long syncDate) {
        this.syncDate = syncDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
