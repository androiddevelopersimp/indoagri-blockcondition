package co.indoagri.blockcondition.model.Data;

public class VersionDB {

    private String DBVersion;
    public static final String TABLE_NAME = "DB_Version";
    public static final String ALIAS = "VersiDB";
    public final static String XML_DBVersion = "DBVersion";
    public VersionDB(String DBVersion) {
        this.DBVersion = DBVersion;
    }

    public String getDBVersion() {
        return DBVersion;
    }

    public void setDBVersion(String DBVersion) {
        this.DBVersion = DBVersion;
    }
}
