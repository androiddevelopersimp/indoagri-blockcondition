package co.indoagri.blockcondition.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.RKHMaingGangAdapter;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.handler.GpsHandler;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.RKH.RKH_HEADER;
import co.indoagri.blockcondition.model.Users.Employee;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.services.GPSService;
import co.indoagri.blockcondition.services.GPSTriggerService;

public class RKHMainGangActivity extends AppCompatActivity implements
        RKHMaingGangAdapter.ItemListener, RKHMaingGangAdapter.CardTugaskan, RKHMaingGangAdapter.CardEdit, RKHMaingGangAdapter.QrCode{

    Toolbar toolbar;
    TextView mTextToolbar;
    TextView txtLastModified;
    List<Employee> employeeList = new ArrayList<Employee>();
    RecyclerView recyclerView;
    RKHMaingGangAdapter  rkhMaingGangAdapter;
    public static int CHANGE_DATE_RKH = 01;
    public static int TUGASKAN_RKH = 02;
    public static int EDIT_RKH = 03;
    public static int QRCODE_VIEW = 04;
    DatabaseHandler database = new DatabaseHandler(RKHMainGangActivity.this);
    String companyCode;
    String estate;
    String division;
    String gang;
    String nik;
    private GetDataAsyncTask getDataAsync;
    TextView txtDate;
    String RKHDATE;
    RelativeLayout chooseDate;
    String RkhID = "";
    String Imei = null;
    RKH_HEADER newRKH = null;
    GPSService gps;
    GpsHandler gpsHandler;
    String gpsKoordinat = "0.0:0.0";
    double latitude = 0.0;
    double longitude = 0.0;
    @Override
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.rkhmaingang_layout);
        SetGPS();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        setUpToolbar();
        SetDataGang();
        Imei = getUniqID(this);
    }
    private void setUpToolbar() {

        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        txtDate = (TextView)findViewById(R.id.txtDate);
        txtLastModified = (TextView)findViewById(R.id.txtLastModified);
        chooseDate = (RelativeLayout)findViewById(R.id.chooseDate);
        if(getLastModifiedRKH()!=null){
            String Date = new DateLocal(new Date(getLastModifiedRKH().getMODIFIED_DATE())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT);
            LastModified(Date);
        }
        if(new PreferenceManager(getApplicationContext(), Constants.shared_name).getRkhInputrkhdate()!=null){
            RKHDATE = new PreferenceManager(getApplicationContext(), Constants.shared_name).getRkhInputrkhdate();
        }else{
            RKHDATE = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
        }
        //txtDate.setText(new DateLocal(RKHDATE, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
        CheckSetDate(RKHDATE);
        chooseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTanggal();

            }
        });
        mTextToolbar.setText(getResources().getString(R.string.mainmenu_rkh));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    void SetDataGang(){

        if(getLastModifiedRKH()!=null){
            String Date = new DateLocal(new Date(getLastModifiedRKH().getMODIFIED_DATE())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT);
            LastModified(Date);
        }
        companyCode = ViewProfile.getCompanyCode();
        estate = ViewProfile.getEstate();
        division = ViewProfile.getDivisions();
        gang = ViewProfile.getGang();
        nik = ViewProfile.getUserNIK();
        setAdapterGang();
        getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null,
                Employee.XML_DIVISION+ "=? and "+
                        Employee.XML_ROLE_ID+ "=? and "+
                        Employee.XML_ESTATE+ "=? ",
                new String [] {division,"LEADER",estate},
                null, null, null, null);
        getDataAsync.execute();
    }
        void setAdapterGang(){
         rkhMaingGangAdapter = new RKHMaingGangAdapter(this, employeeList, this,this,this,this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(rkhMaingGangAdapter);
    }



    private class GetDataAsyncTask extends AsyncTask<Void, List<Employee>, List<Employee>> {
        boolean distinct;
        String tableName;
        String [] columns;
        String whereClause;
        String [] whereArgs;
        String groupBy;
        String having;
        String orderBy;
        String limit;

        public GetDataAsyncTask(boolean distinct, String tableName, String [] columns, String whereClause, String [] whereParams,
                                String groupBy, String having, String orderBy, String limit){
            this.distinct = distinct;
            this.tableName = tableName;
            this.columns = columns;
            this.whereClause = whereClause;
            this.whereArgs = whereParams;
            this.groupBy = groupBy;
            this.having = having;
            this.orderBy = orderBy;
            this.limit = limit;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<Employee> doInBackground(Void... voids) {
            DatabaseHandler database = new DatabaseHandler(RKHMainGangActivity.this);
            List<Employee> listTemp = new ArrayList<Employee>();
            List<Object> listObject;
            database.openTransaction();
            listObject = database.getListData(distinct, tableName, columns, whereClause, whereArgs, groupBy, having, orderBy, limit);
            database.closeTransaction();
            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    Employee employee = (Employee) listObject.get(i);

                    listTemp.add(employee);
                }
            }
            return listTemp;
        }

        @Override
        protected void onPostExecute(List<Employee> listTemp) {
            super.onPostExecute(listTemp);
            for(int i = 0; i < listTemp.size(); i++){
                Employee employee = (Employee) listTemp.get(i);
                rkhMaingGangAdapter.addData(employee);

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (gps != null) {
            gps.stopUsingGPS();
        }
        new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputrkhdate(null);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    @Override
    public void onItemClick(Employee item, int position) {
        Employee sets =  employeeList.get(position);
        sets.setExpendable(!sets.isExpendable());
        rkhMaingGangAdapter.notifyItemChanged(position);
    }
    @Override
    public void onCardTugaskan(Employee item) {
        Class newclass = null;
        final Activity thisActivity = this;
        String activityString="co.indoagri.blockcondition.activity.RKHJOBActivity";
        try {
            newclass = Class.forName(activityString);
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
        }

        if(newclass != null) {
            newRKH = saveDataRKH(item);
            Log.i("RKHHEADER",String.valueOf(newRKH));
            if(newRKH!=null){
                Toast.makeText(thisActivity, "Membuat Tugas", Toast.LENGTH_SHORT).show();
                Intent nextIntent = new Intent(thisActivity, newclass);
                nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                nextIntent.putExtra("Data",newRKH);
                nextIntent.putExtra("Method","new");
                startActivityForResult(nextIntent, TUGASKAN_RKH);
            }else{
                Toast.makeText(thisActivity, "Ada Kasalah Sistem", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(getApplicationContext(),"new class null",Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onCardEdit(Employee item) {
        Class newclass = null;
        final Activity thisActivity = this;
        String activityString="co.indoagri.blockcondition.activity.RKHJOBActivity";
        try {
            newclass = Class.forName(activityString);
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
        }

        if(newclass != null) {
            RKH_HEADER rkh = getDataRKH(item);
            Intent nextIntent = new Intent(RKHMainGangActivity.this, newclass);
            nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            nextIntent.putExtra("Data",rkh);
            nextIntent.putExtra("Method","edit");
            startActivityForResult(nextIntent, EDIT_RKH);
        } else {
            Toast.makeText(getApplicationContext(),"new class null",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onQrCode(Employee item) {
        Class newclass = null;
        final Activity thisActivity = this;
        String activityString="co.indoagri.blockcondition.activity.RKHQRCodeActivity";
        try {
            newclass = Class.forName(activityString);
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
        }

        if(newclass != null) {
            RKH_HEADER rkh = getDataRKH(item);
            Intent nextIntent = new Intent(RKHMainGangActivity.this, newclass);
            nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            nextIntent.putExtra("Data",rkh);
            nextIntent.putExtra("Method","qrcode");
            startActivityForResult(nextIntent, QRCODE_VIEW);
        } else {
            Toast.makeText(getApplicationContext(),"new class null",Toast.LENGTH_SHORT).show();
        }
    }

    void setTanggal(){
                Intent newIntent = new Intent(RKHMainGangActivity.this,RKHArchieveActivity.class);
                newIntent.putExtra("TOOLBARTITLE","Pilih TANGGAL RKH");
                startActivityForResult(newIntent, CHANGE_DATE_RKH);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHANGE_DATE_RKH) {
            if(resultCode == RESULT_OK) {
                if (data.hasExtra("rkhdate")) {
                    String Dates =  data.getExtras().getString("rkhdate");
                    RKHDATE = new DateLocal(Dates, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_INPUT);
                    CheckSetDate(RKHDATE);
                }else{
                    RKHDATE = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
                    //txtDate.setText(new DateLocal(RKHDATE, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
                    CheckSetDate(RKHDATE);
                }

                SetDataGang();
            }
        }
        if (requestCode == TUGASKAN_RKH) {
            if(resultCode == RESULT_OK) {
                newRKH = null;
                SetDataGang();
            }
        }
        if (requestCode == QRCODE_VIEW) {
            if(resultCode == RESULT_OK) {
                Toast.makeText(this, "FROM QR CODE DISPLAY", Toast.LENGTH_SHORT).show();
            }
        }
    }

    void CheckSetDate(String Dates){
        try{
            Date parsedDate = DateLocal.FORMAT_INPUT.parse(Dates);
            Calendar cal = Calendar.getInstance();
            cal.setTime(parsedDate);
            int days = cal.get(Calendar.DAY_OF_WEEK);
            int dates = cal.get(Calendar.DATE);
            int months = cal.get(Calendar.MONTH);
            int years = cal.get(Calendar.YEAR);

            int hours = cal.get(Calendar.HOUR);
            int minutes = cal.get(Calendar.MINUTE);
            int seconds = cal.get(Calendar.SECOND);
            int amPm = cal.get(Calendar.AM_PM);

            switch (amPm) {
                case Calendar.AM:

                    break;
                case Calendar.PM:
                    hours = hours + 12;
                default:
                    break;
            }

           // txtDate.setText(digitFormat(hours, 2) + ":" + digitFormat(minutes,2) + ":" + digitFormat(seconds,2));

            String hari = "";

            switch (days) {
                case Calendar.SUNDAY:
                    hari = getResources().getString(R.string.minggu);
                    break;
                case Calendar.MONDAY:
                    hari = getResources().getString(R.string.senin);
                    break;
                case Calendar.TUESDAY:
                    hari = getResources().getString(R.string.selasa);
                    break;
                case Calendar.WEDNESDAY:
                    hari = getResources().getString(R.string.rabu);
                    break;
                case Calendar.THURSDAY:
                    hari = getResources().getString(R.string.kamis);
                    break;
                case Calendar.FRIDAY:
                    hari = getResources().getString(R.string.jumat);
                    break;
                case Calendar.SATURDAY:
                    hari = getResources().getString(R.string.sabtu);
                    break;
                default:
                    break;
            }


            String bulan = "";

            switch (months) {
                case Calendar.JANUARY:
                    bulan = getResources().getString(R.string.januari);
                    break;
                case Calendar.FEBRUARY:
                    bulan = getResources().getString(R.string.februari);
                    break;
                case Calendar.MARCH:
                    bulan = getResources().getString(R.string.maret);
                    break;
                case Calendar.APRIL:
                    bulan = getResources().getString(R.string.april);
                    break;
                case Calendar.MAY:
                    bulan = getResources().getString(R.string.mei);
                    break;
                case Calendar.JUNE:
                    bulan = getResources().getString(R.string.juni);
                    break;
                case Calendar.JULY:
                    bulan = getResources().getString(R.string.juli);
                    break;
                case Calendar.AUGUST:
                    bulan = getResources().getString(R.string.agustus);
                    break;
                case Calendar.SEPTEMBER:
                    bulan = getResources().getString(R.string.september);
                    break;
                case Calendar.OCTOBER:
                    bulan = getResources().getString(R.string.oktober);
                    break;
                case Calendar.NOVEMBER:
                    bulan = getResources().getString(R.string.november);
                    break;
                case Calendar.DECEMBER:
                    bulan = getResources().getString(R.string.desember);
                    break;
                default:
                    break;
            }

            txtDate.setText(hari + " , " + dates + " " + bulan + " " + years);

        }catch (Exception e) {}

    }
    void LastModified(String Dates){
        try{
            Date parsedDate = DateLocal.FORMAT_DATE_TIME_INPUT.parse(Dates);
            Calendar cal = Calendar.getInstance();
            cal.setTime(parsedDate);
            int days = cal.get(Calendar.DAY_OF_WEEK);
            int dates = cal.get(Calendar.DATE);
            int months = cal.get(Calendar.MONTH);
            int years = cal.get(Calendar.YEAR);

            int hours = cal.get(Calendar.HOUR);
            int minutes = cal.get(Calendar.MINUTE);
            int seconds = cal.get(Calendar.SECOND);
            int amPm = cal.get(Calendar.AM_PM);

            switch (amPm) {
                case Calendar.AM:

                    break;
                case Calendar.PM:
                    hours = hours + 12;
                default:
                    break;
            }
            String Time = digitFormat(hours, 2) + ":" + digitFormat(minutes,2) + ":" + digitFormat(seconds,2);
            // txtDate.setText(digitFormat(hours, 2) + ":" + digitFormat(minutes,2) + ":" + digitFormat(seconds,2));

            String hari = "";

            switch (days) {
                case Calendar.SUNDAY:
                    hari = getResources().getString(R.string.minggu);
                    break;
                case Calendar.MONDAY:
                    hari = getResources().getString(R.string.senin);
                    break;
                case Calendar.TUESDAY:
                    hari = getResources().getString(R.string.selasa);
                    break;
                case Calendar.WEDNESDAY:
                    hari = getResources().getString(R.string.rabu);
                    break;
                case Calendar.THURSDAY:
                    hari = getResources().getString(R.string.kamis);
                    break;
                case Calendar.FRIDAY:
                    hari = getResources().getString(R.string.jumat);
                    break;
                case Calendar.SATURDAY:
                    hari = getResources().getString(R.string.sabtu);
                    break;
                default:
                    break;
            }


            String bulan = "";

            switch (months) {
                case Calendar.JANUARY:
                    bulan = getResources().getString(R.string.januari);
                    break;
                case Calendar.FEBRUARY:
                    bulan = getResources().getString(R.string.februari);
                    break;
                case Calendar.MARCH:
                    bulan = getResources().getString(R.string.maret);
                    break;
                case Calendar.APRIL:
                    bulan = getResources().getString(R.string.april);
                    break;
                case Calendar.MAY:
                    bulan = getResources().getString(R.string.mei);
                    break;
                case Calendar.JUNE:
                    bulan = getResources().getString(R.string.juni);
                    break;
                case Calendar.JULY:
                    bulan = getResources().getString(R.string.juli);
                    break;
                case Calendar.AUGUST:
                    bulan = getResources().getString(R.string.agustus);
                    break;
                case Calendar.SEPTEMBER:
                    bulan = getResources().getString(R.string.september);
                    break;
                case Calendar.OCTOBER:
                    bulan = getResources().getString(R.string.oktober);
                    break;
                case Calendar.NOVEMBER:
                    bulan = getResources().getString(R.string.november);
                    break;
                case Calendar.DECEMBER:
                    bulan = getResources().getString(R.string.desember);
                    break;
                default:
                    break;
            }

            txtLastModified.setText("Last Update : ( "+Time + " "+hari+" , " + dates + " " + bulan + " " + years+" )");

        }catch (Exception e) {}

    }

    private String digitFormat(int value, int digit){
        String str = String.valueOf(value);

        int l = str.length();

        if(str.length() < digit){
            for(int i = l; i < digit; i++){
                str = "0" + str;
            }
        }
        return str;
    }
    private RKH_HEADER getLastModifiedRKH(){
        String LastUpdate;
        database.openTransaction();
        RKH_HEADER rkhHeader = (RKH_HEADER)database.getDataFirst(false, RKH_HEADER.TABLE_NAME, null,
                null,
                null,
                null, null, RKH_HEADER.XML_MODIFIED_DATE+" DESC ", "1");
        database.closeTransaction();
        return rkhHeader;
    }
    @Override
    public void onRestart()
    {
        super.onRestart();
        employeeList.clear();
        SetDataGang();
    }

    private RKH_HEADER saveDataRKH(Employee employee){
        RKH_HEADER rkh_header = null;
        long RowId = 0;
        RkhID = (TextUtils.isEmpty(RkhID)) ? estate+division+new DateLocal(new Date()).getDateString(DateLocal.FORMAT_ID) + Imei.substring(8, Imei.length() - 1) : RkhID;
        RKHDATE = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
        String filterDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);

        Employee Krani = getClerkFromGang(employee.getGang());
        String nikKrani = (Krani==null)? "" :Krani.getNik();
        String nameKrani = (Krani==null)? "" :Krani.getName();
        try{
            long TodayDate = new Date().getTime();
            database.openTransaction();
            RKH_HEADER setDAta = new RKH_HEADER(Imei,
                    employee.getCompanyCode(),
                    employee.getEstate(),
                    RKHDATE,
                    employee.getDivision(),
                    employee.getGang(),
                    employee.getNik(),
                    employee.getName(),
                    nikKrani,
                    nameKrani,
                    gpsKoordinat,
                    0,
                    TodayDate,
                    nik,
                    TodayDate,
                    nik,
                    RkhID);
            RowId = database.setData(setDAta);
            database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            if(RowId>0){
                rkh_header = (RKH_HEADER) database.getDataFirst(false, RKH_HEADER.TABLE_NAME, null,
                                RKH_HEADER.XML_RKH_ID+ "=? ",
                        new String [] {RkhID},
                        null, null, null, null);
            }else{
                rkh_header = null;
            }
            database.closeTransaction();
        }
        return rkh_header;
    }
    private RKH_HEADER getDataRKH(Employee employee){
        RKH_HEADER rkhHeader = null;
        database.openTransaction();
        String RKHDATE = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
        rkhHeader = (RKH_HEADER) database.getDataFirst(false, RKH_HEADER.TABLE_NAME, null,
                RKH_HEADER.XML_NIK_FOREMAN + "=? AND "+
                        RKH_HEADER.XML_RKH_DATE + "=?",
                new String [] {employee.getNik(),RKHDATE},
                null, null, null, null);
        database.closeTransaction();
        return rkhHeader;
    }
    private Employee getClerkFromGang(String Gang){
        Employee gang = null;
        database.openTransaction();
        gang = (Employee) database.getDataFirst(false, Employee.TABLE_NAME, null,
                Employee.XML_GANG + "=? AND "+
                        Employee.XML_ROLE_ID + "=?",
                new String [] {Gang,"KERANI"},
                null, null, null, null);
        database.closeTransaction();
        return gang;
    }

    private String getUniqID(Context context){
        String number = null;
        if(Constants.const_DeviceID != null || !Constants.const_DeviceID.equals("")){
            number =Constants.const_DeviceID;
        }else{

            number = Constants.const_IMEI;
        }
        return number;
    }

    void SetGPS(){
        gps = new GPSService(RKHMainGangActivity.this);
        gpsHandler = new GpsHandler(RKHMainGangActivity.this);

        GPSTriggerService.rkhMainGangActivity = this;

        gpsHandler.startGPS();
        gpsKoordinat = getLocation();
    }
    private String getLocation(){
        String gpsKoordinat = "0.0:0.0";

        if(gps != null){
            Location location = gps.getLocation();
            if(location != null){
                latitude = location.getLatitude();
                longitude = location.getLongitude();

                gpsKoordinat = latitude + ":" + longitude;
            }
        }

        return gpsKoordinat;
    }

    public void updateGpsKoordinat(Location location){
        if(location != null){
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            gpsKoordinat = latitude + ":" + longitude;
        }
    }
}
