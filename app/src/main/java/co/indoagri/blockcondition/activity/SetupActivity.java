package co.indoagri.blockcondition.activity;

import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.dialog.DialogNotification;
import co.indoagri.blockcondition.handler.BroadcastConnectorHandler;
import co.indoagri.blockcondition.listener.DialogConfirmationListener;
import co.indoagri.blockcondition.listener.DialogNotificationListener;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.widget.ToastMessage;

public class SetupActivity  extends BaseActivity implements View.OnClickListener,
        BroadcastConnectorHandler.ConnectivityReceiverListener,CompoundButton.OnCheckedChangeListener,BaseActivity.LogOutListener,
        AdapterView.OnItemClickListener, android.widget.RadioGroup.OnCheckedChangeListener, DialogNotificationListener, DialogConfirmationListener {
    DatabaseHandler database = new DatabaseHandler(SetupActivity.this);
    Button set_import,set_export,set_backup,set_UpdatePages,btnBack,btnNext;
    ToastMessage toastMessage;
    ConstraintLayout constraintLayout;
    View rootView;
    public static TextView txtTitle;
    public static int SET_EXPORT = 32;
    public static int SET_SETTING = 99;
    DatabaseHandler databaseHandler;
    ArrayList<String> setUPPages = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        animOnStart();

        setContentView(R.layout.activity_setup);

        registerBaseActivityReceiver();
        toastMessage = new ToastMessage(getApplicationContext());
        constraintLayout = (ConstraintLayout)findViewById(R.id.constraint);
        txtTitle = (TextView)findViewById(R.id.txtTitleHeader);
        set_import = (Button)findViewById(R.id.set_import);
        set_import.setOnClickListener(this);
        set_export = (Button)findViewById(R.id.set_export);
        set_export.setOnClickListener(this);
        set_backup = (Button)findViewById(R.id.set_backup);
        set_backup.setOnClickListener(this);
        set_UpdatePages = (Button)findViewById(R.id.set_updatePages);
        set_UpdatePages.setOnClickListener(this);
        btnBack = (Button)findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
        btnNext = (Button)findViewById(R.id.btnNext);
        btnNext.setVisibility(View.GONE);
        setDefault();
        setUPPages.add("Jalan");
        setUPPages.add("Jembatan");
        setUPPages.add("Parit");
        setUPPages.add("TitiPanen");
        setUPPages.add("TitiRintis");
        setUPPages.add("TIkus");
        setUPPages.add("Pencurian");
        setUPPages.add("BW");
        setUPPages.add("TPH");
        setUPPages.add("TPH2");
        setUPPages.add("TPHBersih");
        setUPPages.add("TPHBersih2");
        setUPPages.add("Piringan");
        setUPPages.add("PasarPanen");
        setUPPages.add("PasarRintis");
        setUPPages.add("TunasPokok");
        setUPPages.add("Gawangan");
        setUPPages.add("Drainase");
        setUPPages.add("Ganoderma");
        setUPPages.add("Rayap");
        setUPPages.add("Orcytes");
        setUPPages.add("Sanitasi");
        setUPPages.add("Kacangan");
    }

    void setDefault(){
        btnNext.setVisibility(View.GONE);
        set_import.setVisibility(View.GONE);
        txtTitle.setText(getResources().getString(R.string.setup_condition));
        if(ViewToolbar.CheckLogin()){
            TextView txtDivision = (TextView)findViewById(R.id.txtUserDivision);
            txtDivision.setText(ViewToolbar.getDivisions());
            TextView txtBA = (TextView)findViewById(R.id.txtBA);
            txtBA.setText( ViewToolbar.getEstate());
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        animOnFinish();
                        finish();
                break;

            case R.id.set_export:
                Intent intentsetting = new Intent(SetupActivity.this, ExportActivity.class);
                startActivityForResult(intentsetting, SET_EXPORT);
                break;
            case R.id.set_backup:
                Intent intentsetting2 = new Intent(SetupActivity.this, SettingsActivity.class);
                startActivityForResult(intentsetting2, SET_SETTING);
            case R.id.set_updatePages:
                int i = 0;
                for(i = 0; i < setUPPages.size(); i++){
                    UpdatePages(setUPPages.get(i));
                }
                if(i==setUPPages.size()){
                    new DialogNotification(SetupActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.reset_successed), false).show();
                }

                break;
            default:
                break;
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.e("ONDESTROY", "OnDestory()");
        unRegisterBaseActivityReceiver();
    }

    private void UpdatePages(String field) {
        database.openTransaction();
        try {
            database.ResetData(
                    "UPDATE tblT_BlockCondition" +
                            "     SET "+field+" = REPLACE ( "+field+" , '00', '')");
        } catch (
                SQLiteException e) {
            e.printStackTrace();
            database.closeTransaction();
        } finally {
            database.commitTransaction();
            database.closeTransaction();
        }

    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.e("ONSTOP", "OnStop()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("ONPAUSE", "onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        setDefault();
        Log.e("ONRESUME", "onResume()");
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

    }

    @Override
    public void doLogout() {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onConfirmOK(Object object, int id) {

    }

    @Override
    public void onConfirmCancel(Object object, int id) {

    }

    @Override
    public void onOK(boolean is_finish) {

    }
}
