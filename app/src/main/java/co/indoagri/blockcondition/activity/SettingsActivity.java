package co.indoagri.blockcondition.activity;

import android.Manifest;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteException;
import android.media.MediaScannerConnection;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;
import java.util.Vector;

import co.indoagri.blockcondition.MyApps;
import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.AdapterBluetooth;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.DatabaseHelper;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.dialog.DialogConfirmation;
import co.indoagri.blockcondition.dialog.DialogNotification;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.fragment.AuthMasterDownloadFragment;
import co.indoagri.blockcondition.handler.BroadcastConnectorHandler;
import co.indoagri.blockcondition.handler.DownloadAppsAsyncTask;
import co.indoagri.blockcondition.handler.FolderHandler;
import co.indoagri.blockcondition.handler.GpsHandler;
import co.indoagri.blockcondition.handler.JsonHandler;
import co.indoagri.blockcondition.handler.LanguageHandler;
import co.indoagri.blockcondition.handler.UpdateHandler;
import co.indoagri.blockcondition.listener.DialogConfirmationListener;
import co.indoagri.blockcondition.listener.DialogNotificationListener;
import co.indoagri.blockcondition.model.Apps;
import co.indoagri.blockcondition.model.Bluetooth;
import co.indoagri.blockcondition.model.Data.DeviceAlias;
import co.indoagri.blockcondition.model.Data.SPBSRunningNumber;
import co.indoagri.blockcondition.model.Data.SPTARunningNumber;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.model.Users.UserLogin;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.services.GPSTriggerService;
import co.indoagri.blockcondition.widget.CustomSnackBar;
import co.indoagri.blockcondition.widget.ToastMessage;

public class SettingsActivity extends BaseActivity implements View.OnClickListener,
        BroadcastConnectorHandler.ConnectivityReceiverListener,CompoundButton.OnCheckedChangeListener,BaseActivity.LogOutListener,
        AdapterView.OnItemClickListener, android.widget.RadioGroup.OnCheckedChangeListener, DialogNotificationListener, DialogConfirmationListener {
    boolean doubleBackToExitPressedOnce = false;
    ToastMessage toastMessage;
    ConstraintLayout constraintLayout;
    View rootView;
    public static TextView txtTitle,txtTanggalBlock,txtSettingsAppVersion;
    public static View relContainerTitle,relContainerSubTitle;
    boolean onUserInteraction = false;
    Button btnNext,btnBack;
    ArrayList<tblT_BlockCondition>blockConditions = new ArrayList<>();
    AdapterBluetooth adapter;
    File file;
    ListView lvInternalStorage,lvInternalStorage2;
    int count = 0;
    private String[] FilePathStrings;
    private String[] FileNameStrings;
    private File[] listFile;
    private static final String TAG = "BluetoothConnectMenu";
    private static final int REQUEST_ENABLE_BT = 2;
    ArrayList<String> pathHistory;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothSocket bluetoothSocket;
    private Vector<BluetoothDevice> remoteDevices;
    private BroadcastReceiver searchFinish;
    private BroadcastReceiver searchStart;
    private BroadcastReceiver discoveryResult;

    private GpsHandler gpsHandler;

    int idSelected = 0;

    DialogProgress dialogProgress;
    private CheckBox cbxSettingsWifi;
    private CheckBox cbxSettingsBluetooth;
    private TextView txtSettingsLocationServices;
    private TextView txtSettingsGpsServices;
    private CheckBox cbxSettingsGpsServices;
    private Button btnSettingsBackupDatabase;
    private Button btnSettingsRestoreDatabase;
    private Button btnSettingImportCondition;
    private TextView txtSettingsDeviceAlias;
    private Button btnSettingsDeviceAliasReset;
    private TextView txtSettingsSyncMaster;
    private TextView txtSettingsUpdateApp;
    private TextView txtSettingsLanguage;
    private TextView txtSettingsBluetoothMessage;
    private ListView lsvSettingsBluetooth;
    private RadioGroup rgpSettingsLanguage;
    private RadioButton rbnSettingsLanguageEn;
    private RadioButton rbnSettingsLanguageIn;
    private RadioGroup rgpSettingsQR;
    private RadioButton rbnSettingsQRL;
    private RadioButton rbnSettingsQRS;
    DatabaseHandler database;
    private static final String dir = Environment.getExternalStorageDirectory().getAbsolutePath() + "//temp";
    private static final String fileName = dir + "//BTPrinter";
    private String lastConnAddr;
    Apps apps;
    CheckUpdateAsyncTask checkAysncTask;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        animOnStart();

        setContentView(R.layout.activity_setting);

        registerBaseActivityReceiver();
        database = new DatabaseHandler(getApplicationContext());
        toastMessage = new ToastMessage(getApplicationContext());
        constraintLayout = (ConstraintLayout)findViewById(R.id.constraint);
        txtTitle = (TextView)findViewById(R.id.txtTitleHeader);
        txtTanggalBlock = (TextView)findViewById(R.id.txtTanggalBlock);
        relContainerTitle = (View)findViewById(R.id.titleActivityHeader);
        relContainerSubTitle = (View)findViewById(R.id.titleSubActivityHeader);
        txtSettingsAppVersion = (TextView)findViewById(R.id.txtSettingsAppVersion);
        txtSettingsSyncMaster = (TextView) findViewById(R.id.txtSettingsSyncMaster);
        txtSettingsUpdateApp = (TextView) findViewById(R.id.txtSettingsUpdateApp);
        txtSettingsLanguage = (TextView) findViewById(R.id.txtSettingsLanguage);
        rgpSettingsLanguage = (RadioGroup) findViewById(R.id.rgpSettingsLanguage);
        rbnSettingsLanguageEn = (RadioButton) findViewById(R.id.rbnSettingsLanguageEn);
        rbnSettingsLanguageIn = (RadioButton) findViewById(R.id.rbnSettingsLanguageIn);
        rgpSettingsQR = (RadioGroup) findViewById(R.id.rgpSettingsQR);
        rbnSettingsQRL = (RadioButton) findViewById(R.id.rbnSettingsQRL);
        rbnSettingsQRS = (RadioButton) findViewById(R.id.rbnSettingsQRS);

        cbxSettingsWifi = (CheckBox) findViewById(R.id.cbxSettingsWifi);
        cbxSettingsBluetooth = (CheckBox) findViewById(R.id.cbxSettingsBluetooth);
        txtSettingsBluetoothMessage = (TextView) findViewById(R.id.txtSettingsBluetoothMessage);
        lsvSettingsBluetooth = (ListView) findViewById(R.id.lsvSettingsBluetooth);
        txtSettingsLocationServices = (TextView) findViewById(R.id.txtSettingsLocationServices);
        txtSettingsGpsServices = (TextView) findViewById(R.id.txtSettingsGpsServices);
        cbxSettingsGpsServices = (CheckBox) findViewById(R.id.cbxSettingsGpsServices);
        txtSettingsAppVersion = (TextView) findViewById(R.id.txtSettingsAppVersion);
        btnSettingsBackupDatabase = (Button) findViewById(R.id.btnSettingsDownloadDatabase);
        btnSettingsRestoreDatabase = (Button) findViewById(R.id.btnSettingsRestoreDatabase);
        txtSettingsDeviceAlias = (TextView) findViewById(R.id.txtSettingsDeviceAlias);
        btnSettingsDeviceAliasReset = (Button) findViewById(R.id.btnSettingsDeviceAliasReset);
        btnSettingImportCondition = (Button)findViewById(R.id.btnSettingImportCondition);
        btnSettingImportCondition.setOnClickListener(this);
        txtSettingsSyncMaster.setOnClickListener(this);
        txtSettingsUpdateApp.setOnClickListener(this);
        rgpSettingsLanguage.setOnCheckedChangeListener(this);
        rgpSettingsQR.setOnCheckedChangeListener(this);
        cbxSettingsWifi.setOnCheckedChangeListener(this);
        cbxSettingsBluetooth.setOnCheckedChangeListener(this);
        lsvSettingsBluetooth.setOnItemClickListener(this);
        txtSettingsLocationServices.setOnClickListener(this);
        cbxSettingsGpsServices.setOnCheckedChangeListener(this);
        btnSettingsBackupDatabase.setOnClickListener(this);
        btnSettingsRestoreDatabase.setOnClickListener(this);
        btnSettingsDeviceAliasReset.setOnClickListener(this);
        btnNext = (Button)findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        btnBack = (Button)findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(broadcastReceiver, filter);

        gpsHandler = new GpsHandler(SettingsActivity.this);

        getLanguage();
        getQR();
        initMenu();
        loadSettingFile();
        bluetoothSetup();
        getDeviceAlias();

        cbxSettingsGpsServices.setChecked(isMyServiceRunning(GPSTriggerService.class));
        adapter = new AdapterBluetooth(SettingsActivity.this, new ArrayList<Bluetooth>(), R.layout.item_bluetooth);
        lsvSettingsBluetooth.setAdapter(adapter);

        addPairedDevices();

        discoveryResult = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String key;
                BluetoothDevice remoteDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if(remoteDevice != null){
                    Bluetooth device = new Bluetooth();

                    if(remoteDevice.getBondState() != BluetoothDevice.BOND_BONDED){
                        key = remoteDevice.getName() + "\n[" + remoteDevice.getAddress() + "]";

                        device.setName(remoteDevice.getName());
                        device.setAddress(remoteDevice.getAddress());
                        device.setPaired(false);
                    }
                    else{
                        key = remoteDevice.getName() +"\n["+remoteDevice.getAddress()+"] [Paired]";

                        device.setName(remoteDevice.getName());
                        device.setAddress(remoteDevice.getAddress());
                        device.setPaired(true);

                        database.openTransaction();
                        Bluetooth bluetooth = (Bluetooth) database.getDataFirst(false, Bluetooth.TABLE_NAME, null,
                                Bluetooth.XML_IS_PAIRED + "=?" + " and " +
                                        Bluetooth.XML_IS_SELECTED + "=?",
                                new String [] {"1", "1"},
                                null, null, null, null);
                        database.closeTransaction();

                        if(bluetooth != null && device.getAddress().equalsIgnoreCase(bluetooth.getAddress())){
                            device.setSelected(true);
                        }
                    }

                    remoteDevices.add(remoteDevice);
                    add(device);
                }
            }
        };

        registerReceiver(discoveryResult, new IntentFilter(BluetoothDevice.ACTION_FOUND));

        searchStart = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
            }
        };
        registerReceiver(searchStart, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED));

        searchFinish = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
            }
        };
        registerReceiver(searchFinish, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));

        WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        cbxSettingsWifi.setChecked(wifiManager.isWifiEnabled());
        cbxSettingsBluetooth.setChecked(isBluetoothAvailable());
        setDefault();
    }
    void setDefault(){
        btnNext.setVisibility(View.GONE);
        relContainerSubTitle.setVisibility(View.GONE);
        txtTitle.setText(getResources().getString(R.string.settings));
        txtSettingsAppVersion.setText(String.format(getResources().getString(R.string.settings_version),
                Constants.const_VersionName));
    }
    private void initMenu(){
        txtSettingsLanguage.setText(getResources().getString(R.string.settings_language));
        txtSettingsLocationServices.setText(getResources().getString(R.string.settings_location_services));
        txtSettingsGpsServices.setText(getResources().getString(R.string.settings_gps_services));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSettingImportCondition:


                break;
            case R.id.txtSettingsSyncMaster:
                startActivity(new Intent(SettingsActivity.this, SettingsActivity.class));
                break;
            case R.id.txtSettingsUpdateApp:
                boolean isConnected = BroadcastConnectorHandler.isConnect(MyApps.getAppContext());
                if(isConnected){
                    checkAysncTask = new CheckUpdateAsyncTask();
                    checkAysncTask.execute();
                }else{
                    new DialogNotification(SettingsActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.internet_connection_error), false).show();
                }
                break;
            case R.id.txtSettingsLocationServices:
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                break;
            case R.id.btnSettingsDownloadDatabase:
                try {
                    copyAppDbToDownloadFolder();
                } catch (IOException e) {
                    e.printStackTrace();
                    new DialogNotification(SettingsActivity.this, getResources().getString(R.string.informasi),
                            e.getMessage(), false).show();
                }
                break;
            case R.id.btnSettingsRestoreDatabase:
                startActivity(new Intent(SettingsActivity.this, SettingsActivity.class));
                break;
            case R.id.btnSettingsDeviceAliasReset:
                new DialogConfirmation(SettingsActivity.this, getResources().getString(R.string.informasi),
                        getResources().getString(R.string.settings_device_alias_dialod), null, R.id.btnSettingsDeviceAliasReset).show();
                break;
            case R.id.btnBack:
                if (getFragmentManagerAccount() > 1) {
                    // Log.i("Total",String.valueOf(fragmentManager.getBackStackEntryCount()));
                    Log.i("MainActivity", "popping backstack");
                    baseFragmentManager.popBackStack();
                } else {
                    if(checkAysncTask != null && checkAysncTask.getStatus() != AsyncTask.Status.FINISHED){
                        checkAysncTask.cancel(true);
                        checkAysncTask = null;
                    }
                    else{
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        animOnFinish();
                        finish();
                    }


                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()) {
            case R.id.cbxSettingsWifi:
                WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                wifiManager.setWifiEnabled(isChecked);
                break;
            case R.id.cbxSettingsBluetooth:
                if(bluetoothAdapter == null) bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if(bluetoothAdapter != null){
                    if(isChecked){
                        txtSettingsBluetoothMessage.setVisibility(View.GONE);
                        lsvSettingsBluetooth.setVisibility(View.VISIBLE);
                        bluetoothAdapter.enable();

                        if(bluetoothAdapter.isDiscovering()) bluetoothAdapter.cancelDiscovery();

                        clearBtDevData();
                        adapter.clear();
                        bluetoothAdapter.startDiscovery();
                        addPairedDevices();
                    }else{
                        txtSettingsBluetoothMessage.setVisibility(View.VISIBLE);
                        lsvSettingsBluetooth.setVisibility(View.GONE);
                    }
                }else{
                    new DialogNotification(SettingsActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.bluetooth_not_support), false).show();
                }
                break;
            case R.id.cbxSettingsGpsServices:
                if(isChecked){
                    gpsHandler.startGPS();;
                }else{
                    gpsHandler.stopGPS();
                }
                break;
            default:
                break;
        }
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        cbxSettingsBluetooth.setChecked(false);
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        break;
                    case BluetoothAdapter.STATE_ON:
                        cbxSettingsBluetooth.setChecked(true);
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        break;
                }
            }
        }
    };

    private void bluetoothSetup()
    {
        // Initialize
        clearBtDevData();

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (bluetoothAdapter == null) {
            // Device does not support Bluetooth
            return;
        }

        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }
    private void loadSettingFile(){
        int rin = 0;
        char [] buf = new char[128];
        try{
            FileReader fReader = new FileReader(fileName);
            rin = fReader.read(buf);
            if(rin > 0){
                lastConnAddr = new String(buf,0,rin);
//				txtDeviceSelected.setText(lastConnAddr);
            }
            fReader.close();
        }
        catch (FileNotFoundException e){
            Log.i(TAG, "Connection history not exists.");
        }
        catch (IOException e){
            Log.e(TAG, e.getMessage(), e);
        }
    }

    private void saveSettingFile(){
        try{
            File tempDir = new File(dir);
            if(!tempDir.exists()){
                tempDir.mkdir();
            }

            FileWriter fWriter = new FileWriter(fileName);
            if(lastConnAddr != null)
                fWriter.write(lastConnAddr);
            fWriter.close();
        }
        catch (FileNotFoundException e){
            Log.e(TAG, e.getMessage(), e);
        }
        catch (IOException e){
            Log.e(TAG, e.getMessage(), e);
        }
    }

    // clear device data used list.
    private void clearBtDevData(){
        remoteDevices = new Vector<BluetoothDevice>();
    }

    // add paired device to list
    private void addPairedDevices(){
        BluetoothDevice pairedDevice;

        database.openTransaction();
        Bluetooth bluetooth = (Bluetooth) database.getDataFirst(false, Bluetooth.TABLE_NAME, null,
                Bluetooth.XML_IS_PAIRED + "=?" + " and " +
                        Bluetooth.XML_IS_SELECTED + "=?",
                new String [] {"1", "1"},
                null, null, null, null);
        database.closeTransaction();

        Iterator<BluetoothDevice> iter = (bluetoothAdapter.getBondedDevices()).iterator();

        while(iter.hasNext()){
            pairedDevice = iter.next();

            boolean isSelected = false;

            if(bluetooth != null){
                if(pairedDevice.getAddress().equalsIgnoreCase(bluetooth.getAddress())){
                    isSelected = true;
                }
            }

            remoteDevices.add(pairedDevice);
            add(new Bluetooth(pairedDevice.getName(), pairedDevice.getAddress(), true, isSelected));

            lsvSettingsBluetooth.setVisibility(View.VISIBLE);
            txtSettingsBluetoothMessage.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy(){

        saveSettingFile();

        unregisterReceiver(searchFinish);
        unregisterReceiver(searchStart);
        unregisterReceiver(discoveryResult);
        unregisterReceiver(broadcastReceiver);
        unRegisterBaseActivityReceiver();

        try{
            if(bluetoothSocket != null && bluetoothSocket.isConnected()) bluetoothSocket.close();
        }catch(IOException e){
            e.printStackTrace();
        }

        super.onDestroy();
    }

    @Override
    public void doLogout() {

        runOnUiThread(new Runnable() {
            public void run() {
                toastMessage.shortMessage("Interaction Logout");
                onUserInteraction = false;
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(!onUserInteraction){
            Log.e("INTERACTION", "User interacting with screen");
            onUserInteraction = true;
            startLogoutTimer(this, this);
        }
        else{

        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if(!onUserInteraction){
            Log.e("INTERACTION", "User interacting with screen");
            onUserInteraction = true;
            startLogoutTimer(this, this);
        }
        else{
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("ONSTOP", "OnStop()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("ONPAUSE", "onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApps.getInstance().setConnectivityListener(this);
        Log.e("ONRESUME", "onResume()");
    }

    @Override
    public void onBackPressed() {

        if (getFragmentManagerAccount() > 1) {
            // Log.i("Total",String.valueOf(fragmentManager.getBackStackEntryCount()));
            Log.i("MainActivity", "popping backstack");
            baseFragmentManager.popBackStack();
        } else {
            if(checkAysncTask != null && checkAysncTask.getStatus() != AsyncTask.Status.FINISHED){
                checkAysncTask.cancel(true);
                checkAysncTask = null;
            }
            else{
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                animOnFinish();
                finish();
            }


        }
    }


    private void add(Bluetooth device){
        adapter.add(device);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
        BluetoothDevice bluetoothDevice = remoteDevices.elementAt(position);
        Bluetooth bluetooth = (Bluetooth) adapter.getItem(position);

        try{
            if(bluetoothSocket != null && bluetoothSocket.isConnected())
                bluetoothSocket.close();
        }catch(IOException e){
            e.printStackTrace();
        }

        if(bluetooth.isSelected()){
            adapter.updateCheckbox(-1);

            try{
                database.openTransaction();
                database.deleteData(Bluetooth.TABLE_NAME, null, null);
                database.commitTransaction();
            }catch(SQLiteException e){
                e.printStackTrace();
                database.closeTransaction();
            }finally{
                database.closeTransaction();
            }
        }else{
            idSelected = position;

            if(bluetoothAdapter.isDiscovering()) bluetoothAdapter.cancelDiscovery();

            try {
                openBT(bluetoothDevice);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isBluetoothAvailable() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled());
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        Log.d("tag", serviceClass.getName());

        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            Log.d("tag", service.service.getClassName());

            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void getLanguage(){
        String countryCode = new PreferenceManager(SettingsActivity.this,Constants.shared_name).getCountryCode();

        try{
            if(countryCode.equalsIgnoreCase("in")){
                rbnSettingsLanguageIn.setChecked(true);
            }else{
                rbnSettingsLanguageEn.setChecked(true);
            }
        }catch(NullPointerException e){
            rbnSettingsLanguageEn.setChecked(true);
        }
    }

    private void getQR(){
        String qrCode = new PreferenceManager(SettingsActivity.this,Constants.shared_name).getQR();

        try{
            if(qrCode.equalsIgnoreCase("full")){
                rbnSettingsQRL.setChecked(true);
            }else{
                rbnSettingsQRS.setChecked(true);
            }
        }catch(NullPointerException e){
            rbnSettingsQRS.setChecked(true);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if(checkedId == R.id.rbnSettingsLanguageEn){
            new PreferenceManager(SettingsActivity.this,Constants.shared_name).setCountryCode("us");
            new LanguageHandler(SettingsActivity.this).updateLanguage("us");
        }else if(checkedId == R.id.rbnSettingsLanguageIn){
            new PreferenceManager(SettingsActivity.this,Constants.shared_name).setCountryCode("in");
            new LanguageHandler(SettingsActivity.this).updateLanguage("in");
        }else if(checkedId == R.id.rbnSettingsQRL){
            new PreferenceManager(SettingsActivity.this,Constants.shared_name).setQR("full");
        } else if(checkedId == R.id.rbnSettingsQRS){
            new PreferenceManager(SettingsActivity.this,Constants.shared_name).setQR("simple");
        }
        initMenu();
    }

    public void copyAppDbToDownloadFolder() throws IOException {
        try {
            FolderHandler folderHandler = new FolderHandler(SettingsActivity.this);

            if(folderHandler.isSDCardWritable() && folderHandler.init()){
                File backupDB = new File(folderHandler.getFileDatabaseExport(), DatabaseHelper.dbName);
                File currentDB = getApplicationContext().getDatabasePath(DatabaseHelper.dbName);

                if (currentDB.exists()) {
                    FileInputStream fis = new FileInputStream(currentDB);
                    FileOutputStream fos = new FileOutputStream(backupDB);
                    fos.getChannel().transferFrom(fis.getChannel(), 0, fis.getChannel().size());
                    // or fis.getChannel().transferTo(0, fis.getChannel().size(), fos.getChannel());
                    fis.close();
                    fos.close();

                    MediaScannerConnection.scanFile(SettingsActivity.this, new String[] { backupDB.getAbsolutePath() }, null, null);

                    new DialogNotification(SettingsActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.export_successed), false).show();

                } else{
                    new DialogNotification(SettingsActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.export_failed), false).show();
                }
            }else{
                new DialogNotification(SettingsActivity.this, getResources().getString(R.string.informasi),
                        getResources().getString(R.string.error_sd_card), false).show();
            }
        } catch (IOException e) {
            new DialogNotification(SettingsActivity.this, getResources().getString(R.string.informasi),
                    e.getMessage(), false).show();
        }
    }

    private class CheckUpdateAsyncTask extends AsyncTask<Void, Void, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!isFinishing()){
                dialogProgress = new DialogProgress(SettingsActivity.this, getResources().getString(R.string.wait));
                dialogProgress.show();
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            String json = new UpdateHandler().checkVersion();

            return json;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            if(!TextUtils.isEmpty(json)){
                Log.d("tag", json);

                apps = new JsonHandler(json).getApps();

                if(apps != null){
                    String curVersion = Constants.const_APIVERSION;

                    if(!apps.getVersion().equals(curVersion)){
                        new DialogConfirmation(SettingsActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.apps_update), null, R.id.txtSettingsUpdateApp).show();
                    }else{
                        new DialogNotification(SettingsActivity.this,
                                getResources().getString(R.string.informasi),
                                getResources().getString(R.string.apps_update_not_availabe),
                                false).show();
                    }
                }else{
                    new DialogNotification(SettingsActivity.this,
                            getResources().getString(R.string.informasi),
                            getResources().getString(R.string.internet_connection_error),
                            false).show();
                }

            }else{
                new DialogNotification(SettingsActivity.this,
                        getResources().getString(R.string.informasi),
                        getResources().getString(R.string.internet_connection_error),
                        false).show();
            }
        }
    }

    @Override
    public void onOK(boolean is_finish) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onConfirmOK(Object object, int id) {
        switch (id) {
            case R.id.txtSettingsUpdateApp:
                boolean isConnected = BroadcastConnectorHandler.isConnect(MyApps.getAppContext());
                if(isConnected){
                    new DownloadAppsAsyncTask(SettingsActivity.this, Constants.SERVER + apps.getFilename()).execute();
                }
                break;
            case R.id.btnSettingsDeviceAliasReset:
                 boolean success = false;
                try{
                    database.openTransaction();
                    database.deleteData(DeviceAlias.TABLE_NAME, null, null);
                    database.commitTransaction();
                    txtSettingsDeviceAlias.setText("");
                    success = true;
                }catch(SQLiteException e){
                    e.printStackTrace();
                }finally{
                    database.closeTransaction();
                }

                if(success){
                    getDeviceAlias();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onConfirmCancel(Object object, int id) {

    }

    private void getDeviceAlias(){
        String alias = "";

        database.openTransaction();
        DeviceAlias deviceAlias = (DeviceAlias) database.getDataFirst(false, DeviceAlias.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();

        if(deviceAlias == null){
            database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            String role = "";

            if(userLogin != null){
                role = userLogin.getRoleId();
            }

            String imei = Constants.const_IMEI;

            if(role.equalsIgnoreCase("Clerk")){
                database.openTransaction();
                SPBSRunningNumber spbsNumberMax = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null,
                        SPBSRunningNumber.XML_IMEI + "=?",
                        new String [] {imei},
                        null, null, SPBSRunningNumber.XML_ID + " desc", null);

                database.closeTransaction();

                if(spbsNumberMax != null){
                    alias = spbsNumberMax.getDeviceAlias();
                }
            }else if(role.equalsIgnoreCase("Field Assistant")){
                database.openTransaction();
                SPTARunningNumber sptaNumberMax = (SPTARunningNumber) database.getDataFirst(false, SPTARunningNumber.TABLE_NAME, null,
                        SPTARunningNumber.XML_IMEI + "=?",
                        new String [] {imei},
                        null, null, SPTARunningNumber.XML_ID + " desc", null);

                database.closeTransaction();

                if(sptaNumberMax != null){
                    alias = sptaNumberMax.getDeviceAlias();
                }
            }

            if(!TextUtils.isEmpty(alias)){
                database.openTransaction();
                database.deleteData(DeviceAlias.TABLE_NAME, null, null);
                database.setData(new DeviceAlias(alias));
                database.commitTransaction();
                database.closeTransaction();
            }

        }else{
            alias = deviceAlias.getDeviceAlias();
        }

        txtSettingsDeviceAlias.setText(alias);
    }

    private void openBT(BluetoothDevice bluetoothDevice) throws IOException {
        try {
            // Standard SerialPortService ID
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

            if(bluetoothSocket != null && bluetoothSocket.isConnected()) bluetoothSocket.close();

            bluetoothSocket = bluetoothDevice.createRfcommSocketToServiceRecord(uuid);
            bluetoothSocket.connect();

            adapter.updateCheckbox(idSelected);

            try{
                toastMessage.shortMessage(bluetoothDevice.getName()+" "+bluetoothDevice.getAddress());
                database.openTransaction();
                database.deleteData(Bluetooth.TABLE_NAME, null, null);
                database.setData(new Bluetooth(bluetoothDevice.getName(), bluetoothDevice.getAddress(), true, true));
                database.commitTransaction();
            }catch(SQLiteException e){
                e.printStackTrace();
                database.closeTransaction();
            }finally{
                database.closeTransaction();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void checkConnection() {
        boolean isConnected = BroadcastConnectorHandler.isConnect(getApplicationContext());
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        if (isConnected) {
            CustomSnackBar customSnackbar = CustomSnackBar.make((ViewGroup)rootView.getParent(),8000);
            customSnackbar.setText("Connected");
            customSnackbar.setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onStart();
                }
            });
            customSnackbar.show();
        } else {
            CustomSnackBar customSnackbar = CustomSnackBar.make((ViewGroup)rootView.getParent(),8000);
            customSnackbar.setText("No network connection!");
            customSnackbar.setAction("Retry", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(),"Not Connected",Toast.LENGTH_SHORT).show();
                }
            });
            customSnackbar.show();
        }
    }

    private void checkInternalStorage() {
        Log.d(TAG, "checkInternalStorage: Started.");
        try{
            if (!Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                toastMessage.shortMessage("No SD card found.");
            }
            else{
                // Locate the image folder in your SD Car;d
                file = new File(pathHistory.get(count));
                Log.d(TAG, "checkInternalStorage: directory path: " + pathHistory.get(count));
            }

            listFile = file.listFiles();

            // Create a String array for FilePathStrings
            FilePathStrings = new String[listFile.length];

            // Create a String array for FileNameStrings
            FileNameStrings = new String[listFile.length];

            for (int i = 0; i < listFile.length; i++) {
                // Get the path of the image file
                FilePathStrings[i] = listFile[i].getAbsolutePath();
                // Get the name image file
                FileNameStrings[i] = listFile[i].getName();
            }

            for (int i = 0; i < listFile.length; i++)
            {
                Log.d("Files", "FileName:" + listFile[i].getName());
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, FilePathStrings);
            lvInternalStorage.setAdapter(adapter);

        }catch(NullPointerException e){
            Log.e(TAG, "checkInternalStorage: NULLPOINTEREXCEPTION " + e.getMessage() );
        }
    }

    private void checkFilePermissions() {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){
            int permissionCheck = 0;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                permissionCheck = this.checkSelfPermission("Manifest.permission.READ_EXTERNAL_STORAGE");
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                permissionCheck += this.checkSelfPermission("Manifest.permission.WRITE_EXTERNAL_STORAGE");
            }
            if (permissionCheck != 0) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1001); //Any number
                }
            }
        }else{
            Log.d(TAG, "checkBTPermissions: No need to check permissions. SDK version < LOLLIPOP.");
        }
    }
}
