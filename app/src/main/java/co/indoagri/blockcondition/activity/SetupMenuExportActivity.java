package co.indoagri.blockcondition.activity;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.dialog.NewDialogDate;
import co.indoagri.blockcondition.dialog.DialogNotification;
import co.indoagri.blockcondition.handler.FolderHandler;
import co.indoagri.blockcondition.handler.XmlHandler;
import co.indoagri.blockcondition.listener.DialogDateListener;
import co.indoagri.blockcondition.listener.DialogNotificationListener;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.MessageStatus;
import co.indoagri.blockcondition.model.Users.UserLogin;

public class SetupMenuExportActivity extends BaseActivity implements OnClickListener, DialogNotificationListener, DialogDateListener {
    private Toolbar toolbar;
    private TextView txtExportDate;
    private CheckBox cb_exportRKH;
    private Button btnExportSubmit;
    TextView mTextToolbar;
    private boolean isUsbOtgAvailable = false;
    private boolean isUsbOtgPermit = false;
    String exportDate = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        animOnStart();

        setContentView(R.layout.activity_setupexport);
        setUpToolbar();
        txtExportDate = (TextView) findViewById(R.id.txtExportDate);
        cb_exportRKH = (CheckBox) findViewById(R.id.cb_rkh);
        btnExportSubmit = (Button) findViewById(R.id.btnExportSubmit);

        cb_exportRKH.setEnabled(true);

        txtExportDate.setOnClickListener(this);
        btnExportSubmit.setOnClickListener(this);

        exportDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
        txtExportDate.setText(new DateLocal(exportDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
    }

    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText(getResources().getString(R.string.main_export));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.txtExportDate:
                new NewDialogDate(SetupMenuExportActivity.this, getResources().getString(R.string.tanggal), new Date(), R.id.txtExportDate).show();
                break;
            case R.id.btnExportSubmit:
                FolderHandler folderHandler = new FolderHandler(SetupMenuExportActivity.this);
                List<MessageStatus> lstMsgStatus = new ArrayList<MessageStatus>();
                boolean isUsbOtgSave = isUsbOtgAvailable && isUsbOtgPermit && false;
//			Toast.makeText(getApplicationContext(), isUsbOtgAvailable + ":" + isUsbOtgPermit + ":" + cbxExportUsbOtg.isChecked(), Toast.LENGTH_SHORT).show();

                if(folderHandler.isSDCardWritable() && folderHandler.init()){
                    String status = null;

                    if(cb_exportRKH.isChecked()){
                        MessageStatus messageStatus = new XmlHandler(SetupMenuExportActivity.this).createXML(R.id.cb_rkh, status, isUsbOtgSave, exportDate,null);

                        if(messageStatus != null){
                            lstMsgStatus.add(messageStatus);
                        }
                    }



                    if(lstMsgStatus.size() > 0){
                        String message = "";

                        for(int i = 0; i < lstMsgStatus.size(); i++){
                            MessageStatus msgStatus = (MessageStatus) lstMsgStatus.get(i);

                            message = message + "\n" + msgStatus.getMenu() + ": " + msgStatus.getMessage();
                        }

                        new DialogNotification(SetupMenuExportActivity.this, getResources().getString(R.string.informasi),
                                message, false).show();
                    }
                }else{
                    new DialogNotification(SetupMenuExportActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.error_sd_card), false).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onOK(boolean is_finish) {
        if(is_finish){
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDateOK(Date date, int id) {
        exportDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);

        txtExportDate.setText(new DateLocal(exportDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
    }

    // Add By Adit 20161221
    private String GetLogonUserRole(){
        DatabaseHandler database = new DatabaseHandler(SetupMenuExportActivity.this);

        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();

        String role = "";
        if(userLogin != null) {

            role=userLogin.getRoleId();
        }

        return  role;
    }

}
