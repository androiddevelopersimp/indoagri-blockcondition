package co.indoagri.blockcondition.activity.ui.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.model.Data.ResultDetailModel;
import co.indoagri.blockcondition.model.Data.ResultModel;
import co.indoagri.blockcondition.model.Users.UserLogin;
import co.indoagri.blockcondition.routines.Constants;
/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SummaryPokokTab.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SummaryPokokTab#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SummaryPokokTab extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    DatabaseHandler database;
    GetDataAsyncTask getDataAsync;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private String mParam4;
    Spinner spinnerCondition;
    String SpinnerValue;

    AsyncDetail asyncDetail;

    //

    //
    int lval_color1_val = 0;
    int lval_color2_val = 0;
    int lval_color3_val= 0;
    int lval_color4_val= 0;
    int lval_color5_val= 0;
    int lval_color6_val= 0;
    int lvar_maxcensuspoint = 0;
    int lvar_totalskb = 0;
    String lvar_prevskb = null;
    String lvar_prevbaris = "";
    int baris_index = 0;
    int pokok_index = 0;
    int myProgressCount;
    String[][] baris;
    String[] skb;
    String[][][] pokok;
    String[][][] itemcondition;
    int i;
    int kk;
    String NilaiSKB = null;
    int NilaiCensusPoint = 0;
    String NilaiBlockRow = "";
    String skbString = null;
    String blockrowString = null;
    String KondisiPokok = null;
    String NilaiKondisi = null;

    TextView txtTitle,txtCondition;

    ResultDetailModel resultModel;
    TextView txtnormalmerah,txtnormalkuning,txtnormalhijau,txtmati,txtkosong,txthcv;

    private ProgressBar mProgressBar;
    LinearLayout PanelLoading;
    ProgressBar mprogressBar;
    TextView txtLoadingCount;
    int TotalData;
    private OnFragmentInteractionListener mListener;
    View view;
    String ConditionColor;
    android.widget.TableRow.LayoutParams lay;
    TableLayout tableLayout;
    public SummaryPokokTab() {

    }

    public static SummaryPokokTab newInstance() {
        SummaryPokokTab fragment = new SummaryPokokTab();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = new DatabaseHandler(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Bundle bundle = getArguments();
        if (bundle!= null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);
        }
        mParam1 = new PreferenceManager(getActivity(), Constants.shared_name).getSSYear();
        mParam2 = new PreferenceManager(getActivity(), Constants.shared_name).getSSPeriode();
        mParam3 = new PreferenceManager(getActivity(), Constants.shared_name).getSSBlock();
        mParam4 = new PreferenceManager(getActivity(), Constants.shared_name).getSSPhase();
        if(mParam1!=null){
                view = inflater.inflate(R.layout.fragment_summary_pokok_detail, container, false);
            tableLayout= (TableLayout)view.findViewById(R.id.tableMain);
        }else{
            view = inflater.inflate(R.layout.fragment_summary_null, container, false);
        }
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(mParam1!=null) {
            spinnerCondition = (Spinner) view.findViewById(R.id.spinnerCondition);
            mprogressBar  = (ProgressBar)view.findViewById(R.id.progress_bar);
            txtLoadingCount = (TextView)view.findViewById(R.id.txtCount);
            PanelLoading = (LinearLayout)view.findViewById(R.id.loadingPanel);
            txtTitle = (TextView)view.findViewById(R.id.txtTitleHeader);
            txtCondition = (TextView)view.findViewById(R.id.txtCondition);
            mProgressBar = (ProgressBar) view.findViewById(R.id.pb);
            txtnormalmerah = (TextView)view.findViewById(R.id.normalmerah);
            txtnormalkuning = (TextView)view.findViewById(R.id.normalkuning);
            txtnormalhijau = (TextView)view.findViewById(R.id.normalhijau);
            txtmati = (TextView)view.findViewById(R.id.mati);
            txtkosong = (TextView)view.findViewById(R.id.kosong);
            txthcv= (TextView)view.findViewById(R.id.hcv);
            if(mParam4.equalsIgnoreCase("Mature")){
                setSpinnerMature();
            }if(mParam4.equalsIgnoreCase("Immature")){
              setSpinnerIMMature();
            }

        }else{
            TextView txtID = view.findViewById(R.id.txtID);
            txtID.setText("Data Tidak Ada");
        }
    }


    private void setSpinnerMature(){
        List<String> kondisiR = new ArrayList<String>();
        kondisiR.add("Piringan");
        kondisiR.add("PasarPanen");
        kondisiR.add("TunasPokok");
        kondisiR.add("Gawangan");
        kondisiR.add("Drainase");
        ArrayAdapter<String> dataAdapterR = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kondisiR);

        // Drop down layout style - list view with radio button
        dataAdapterR.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinnerCondition.setAdapter(dataAdapterR);
        spinnerCondition.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                tableLayout.removeAllViews();
                SpinnerValue = String.valueOf(parentView1.getItemAtPosition(position));
                getDataAsync = new GetDataAsyncTask();
                getDataAsync.execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

    }
    private void setSpinnerIMMature(){
        List<String> kondisiR = new ArrayList<String>();
        kondisiR.add("Piringan");
        kondisiR.add("PasarRintis");
        kondisiR.add("Sanitasi");
        kondisiR.add("Gawangan");
        kondisiR.add("Drainase");
        kondisiR.add("Kacangan");
        ArrayAdapter<String> dataAdapterR = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kondisiR);

        // Drop down layout style - list view with radio button
        dataAdapterR.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinnerCondition.setAdapter(dataAdapterR);
        spinnerCondition.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                tableLayout.removeAllViews();
                SpinnerValue = String.valueOf(parentView1.getItemAtPosition(position));
                getDataAsync = new GetDataAsyncTask();
                getDataAsync.execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

    }




    private class BackgroundTask1 extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog;
        public BackgroundTask1(SummaryPokokTab activity) {
            dialog = new ProgressDialog(getActivity());
        }
        @Override
        protected void onPreExecute() {
            dialog.setMessage("Load Data Background 1, please wait.");
            dialog.show();
        }
        @Override
        protected void onPostExecute(Void result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            double Condition = Double.parseDouble(ConditionColor);
            if(Condition <= 3){
                txtCondition.setBackgroundColor(getActivity().getResources().getColor(R.color.colorGreen));
            }
            if(Condition <= 2){
                txtCondition.setBackgroundColor(getActivity().getResources().getColor(R.color.colorYellow));
            }
            if(Condition <= 1){
                txtCondition.setBackgroundColor(getActivity().getResources().getColor(R.color.colorRed));
            }
            txtCondition.setBackgroundColor(getActivity().getResources().getColor(R.color.colorGreen));
            txtTitle.setText(mParam4+" - "+SpinnerValue+" - "+ mParam3+" - "+ resultModel.getPROD_TREES()+" Pkk");
            HitungObject();
        }
        @Override
        protected Void doInBackground(Void... params) {
            try {
                DataSource1();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void HitungObject() {
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        String[] a = new String[6];
        a[0] = mParam1;
        a[1] = mParam2;
        a[2] = mParam3;
        a[3] = mParam1;
        a[4] = mParam2;
        a[5] = mParam3;
        List<Object> listObject;
        String query = "SELECT a.Block, a.Skb, a.BlockRow, a.CensusPoint, " +
                "a.PokokLabel, a.PokokSide, a." + SpinnerValue + " as Condition, a.PokokCondition, a.Flag " +
                "FROM tblT_BlockCondition a " +
                "INNER JOIN " +
                "(SELECT b.block, b.skb, b.blockrow, b.censuspoint,  max(zdate) as zdate from tblT_BlockCondition b " +
                "WHERE translevel = '3' and zyear=? AND period=? AND block=? " +
                "GROUP BY b.block, b.skb, b.blockrow, b.censuspoint ) b " +
                "ON a.block=b.block and a.skb=b.skb AND a.blockrow=b.blockrow AND a.censuspoint=b.censuspoint  AND a.zdate=b.zdate " +
                "WHERE a.zyear=? AND  a.period=? AND a.TransLevel='3' AND a.block=? " +
                //                "ORDER BY  cast(b.SKB as TEXT) ASC, cast(b.BlockRow as TEXT) ASC, cast(b.CensusPoint as int) ASC";
                "ORDER BY  cast(b.SKB as TEXT) ASC, cast(a.PokokSide as TEXT) ASC";
        listObject = database.getListDataRawQuery(query, "RESULTDETAIL", a);
        database.closeTransaction();
        asyncDetail = new AsyncDetail(listObject);
        asyncDetail.execute();
    }


    private class AsyncDetail extends AsyncTask <Void, Integer, Void> {
        List<Object> objectList;

        ResultDetailModel detail;

        public AsyncDetail(List<Object> objects) {
            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            myProgressCount = 0;
            //dialog.setMessage("Get TITI RINTIS, please wait.");
            //dialog.show();
            PanelLoading.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mprogressBar.setProgress(values[0]);
            txtLoadingCount.setText(SpinnerValue +" "+String.valueOf(values[0]+" / "+String.valueOf(objectList.size())));
        }
        @Override
        protected void onPostExecute(Void result) {
            PanelLoading.setVisibility(View.GONE);
            /*initCencusPoint(lvar_maxcensuspoint,lvar_totalskb);*/
            init(detail,TotalData,lvar_maxcensuspoint,lvar_totalskb,lvar_prevbaris,lvar_prevskb,baris,pokok,itemcondition,skb,baris_index,pokok_index);
            //Toast.makeText(getApplicationContext(), "Data is Completed", Toast.LENGTH_LONG).show();
            txtnormalmerah.setText(String.valueOf(lval_color1_val));
            txtnormalkuning.setText(String.valueOf(lval_color2_val));
            txtnormalhijau.setText(String.valueOf(lval_color3_val));
            txtmati.setText(String.valueOf(lval_color4_val));
            txtkosong.setText(String.valueOf(lval_color5_val));
            txthcv.setText(String.valueOf(lval_color6_val));
            //   Toast.makeText(getApplicationContext(), testingnilai, Toast.LENGTH_SHORT).show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            lval_color1_val = 0;
            lval_color1_val = 0;
            lval_color2_val = 0;
            lval_color3_val= 0;
            lval_color4_val= 0;
            lval_color5_val= 0;
            lval_color6_val= 0;
            lvar_maxcensuspoint = 0;
            lvar_totalskb = 0;
            lvar_prevskb = null;
            lvar_prevbaris = "";
            baris_index = 0;
            pokok_index = 0;
            myProgressCount = 0;
            baris = null;
            skb = null;
            pokok = null;
            itemcondition = null;
            i = 0;
            kk = 0;
            NilaiSKB = null;
            NilaiCensusPoint = 0;
            NilaiBlockRow = "";
            skbString = null;
            blockrowString = null;
            KondisiPokok = null;
            NilaiKondisi = null;
            if(objectList.size()<=2){
                baris = new String[objectList.size()+1][objectList.size()+1];
                skb = new String[objectList.size()+1];
                pokok = new String[objectList.size()+1][objectList.size()+1][objectList.size()+1];
                itemcondition = new String[objectList.size()+1][objectList.size()+1][objectList.size()+1];
            }
            if(objectList.size()>2){
                baris = new String[objectList.size()][objectList.size()];
                skb = new String[objectList.size()];
                pokok = new String[objectList.size()][objectList.size()][objectList.size()];
                itemcondition = new String[objectList.size()][objectList.size()][objectList.size()];
            }
            /*    for (i = -1; i < objectList.size(); i++) {*/
            for (int i = 0; i<objectList.size(); i++) {
                if(isCancelled()){
                    break;
                }else{
                    //Log.i("In Background","current value;"+ i);
                    publishProgress(i);
                    detail = (ResultDetailModel) objectList.get(i);
                    skbString = detail.getSKB();
                    blockrowString = detail.getBlockRow();
                    KondisiPokok = detail.getPokokCondition();
                    NilaiKondisi = detail.getCondition();
                    // INT //
                    NilaiSKB = detail.getSKB();
                    NilaiCensusPoint = Integer.parseInt(detail.getCensusPoint());
                    NilaiBlockRow = blockrowString;
                    if (detail.getCondition().equalsIgnoreCase("1")) {
                        lval_color1_val = lval_color1_val + 1;
                    }
                    if (detail.getCondition().equalsIgnoreCase("2")) {
                        lval_color2_val = lval_color2_val + 1;
                    }
                    if (detail.getCondition().equalsIgnoreCase("3")) {
                        lval_color3_val = lval_color3_val + 1;
                    }
                    if (detail.getCondition().equals(null) || detail.getCondition().equalsIgnoreCase("0")) {
                        if (detail.getPokokCondition().equalsIgnoreCase("Mati")) {
                            lval_color4_val = lval_color4_val + 1;
                        }
                        if (detail.getPokokCondition().equalsIgnoreCase("Kosong")) {
                            lval_color5_val = lval_color5_val + 1;
                        }
                        if (detail.getPokokCondition().equalsIgnoreCase("HCV")) {
                            lval_color6_val = lval_color6_val + 1;
                        }
                    }
                    if (NilaiCensusPoint > lvar_maxcensuspoint) {
                        lvar_maxcensuspoint = NilaiCensusPoint;
                    }
                    if (!NilaiSKB.equals(lvar_prevskb)) {
                        lvar_totalskb = lvar_totalskb + 1;
                        lvar_prevskb = NilaiSKB;
                        lvar_prevbaris = NilaiBlockRow;
                        baris_index = 1;
                        pokok_index = 1;
                        skb[lvar_totalskb] = skbString;
                        if (!NilaiBlockRow.equals(lvar_prevbaris)) {
                            lvar_prevbaris = NilaiBlockRow;
                            baris_index = baris_index + 1;
                            pokok_index = 1;
                        }
                        baris[lvar_totalskb][baris_index] = NilaiBlockRow;
                        pokok[lvar_totalskb][baris_index][pokok_index] = KondisiPokok;
                        itemcondition[lvar_totalskb][baris_index][pokok_index] = NilaiKondisi;
                        pokok_index = pokok_index + 1;
                    }else{
                        skb[lvar_totalskb] = skbString;
                        if (!NilaiBlockRow.equals(lvar_prevbaris)) {
                            lvar_prevbaris = NilaiBlockRow;
                            baris_index = baris_index + 1;
                            pokok_index = 1;
                        }
                        baris[lvar_totalskb][baris_index] = NilaiBlockRow;
                        pokok[lvar_totalskb][baris_index][pokok_index] = KondisiPokok;
                        itemcondition[lvar_totalskb][baris_index][pokok_index] = NilaiKondisi;
                        pokok_index = pokok_index + 1;
                    }


                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }

            }

            return null;
        }
    }


    private void DataSource1(){
        database.openTransaction();
        String[] a = new String[4];
        a[0] = "9999-12-31";
        a[1] = mParam1;
        a[2] = mParam2;
        a[3] = mParam3;
        List<Object> listObject;
        List<ResultDetailModel> listTemp = new ArrayList<ResultDetailModel>();
        listTemp.clear();
        String query = "SELECT a.Block, a.Skb, a.BlockRow, a.CensusPoint, " +
                "a.PokokLabel, a.PokokSide, a."+SpinnerValue+" as Condition, a.Flag, b.PROD_TREES FROM tblT_BlockCondition a " +
                "INNER JOIN BLKPLT b ON a.Block = b.Block AND b.VALIDTO = ? " +
                "WHERE zyear=? AND  period=? AND TransLevel='0' AND a.block= ? "+
                "AND a."+SpinnerValue+" > 0 AND a."+SpinnerValue+ " != 0.0";
        listObject = database.getListDataRawQuery(query,"RESULTDETAIL",a);
        database.closeTransaction();
        if(listObject.size() > 0){
            for(int i = 0; i < listObject.size(); i++){
                resultModel = (ResultDetailModel) listObject.get(i);
            }
        }
    }

    public void init(ResultDetailModel detail,int TotalData,int lvar_maxcensuspoint2,int lvar_totalskb,String lvar_prevbaris,
                     String lvar_prevskb,String[][]baris,String[][][]pokok,String[][][]itemcondition,
                     String[]skb,int barisIndex, int pokokIndex) {
        TableLayout tableLayout = (TableLayout)getActivity().findViewById(R.id.tableMain);
        Date date = new Date();
        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getActivity());

        int rows = 0;
        int colums = 0;
        rows = lvar_maxcensuspoint2+2;
        colums = lvar_totalskb+1;

        int JCol;
        int ICol;
        int KCol;
        final GradientDrawable gd = new GradientDrawable();
        gd.setCornerRadius(5);
        gd.setStroke(1, 0xFF000000);
        gd.setColor(getResources().getColor(R.color.Light_Slate));
        final GradientDrawable gd2 = new GradientDrawable();
        gd2.setCornerRadius(5);
        gd2.setStroke(1, 0xFF000000);
        gd2.setColor(getResources().getColor(R.color.Aquamarine));
        final GradientDrawable gdnull = new GradientDrawable();
        gdnull.setCornerRadius(5);
        gdnull.setStroke(1, 0xFF000000);
        gdnull.setColor(getResources().getColor(R.color.White));

        // color //

        final GradientDrawable green = new GradientDrawable();
        green.setCornerRadius(5);
        green.setStroke(1, 0xFF000000);
        green.setColor(getResources().getColor(R.color.colorGreen));

        final GradientDrawable yellow = new GradientDrawable();
        yellow.setCornerRadius(5);
        yellow.setStroke(1, 0xFF000000);
        yellow.setColor(getResources().getColor(R.color.colorYellow));


        final GradientDrawable red = new GradientDrawable();
        red.setCornerRadius(5);
        red.setStroke(1, 0xFF000000);
        red.setColor(getResources().getColor(R.color.colorRed));

        final GradientDrawable white = new GradientDrawable();
        white.setCornerRadius(5);
        white.setStroke(1, 0xFF000000);
        white.setColor(getResources().getColor(R.color.colorWhite));

        final GradientDrawable polos = new GradientDrawable();
        polos.setCornerRadius(5);
        polos.setStroke(1, getResources().getColor(R.color.transparent_black_hex_1));
        polos.setColor(getResources().getColor(R.color.transparent_black_hex_1));

        // COLOR //
        tableLayout.setStretchAllColumns(true);
        tableLayout.bringToFront();
        for (ICol = 0; ICol < rows; ICol++) {
            android.widget.TableRow.LayoutParams lay = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,TableRow.LayoutParams.WRAP_CONTENT);
            TableRow tr = new TableRow(getActivity());
            TextView txtGeneric1 = new TextView(getActivity());
            txtGeneric1.setTextSize(18);
            txtGeneric1.setPadding(15,10,15,10);
            txtGeneric1.setTextColor(getResources().getColor(R.color.Carbon_Gray));
            txtGeneric1.setTypeface(null,Typeface.BOLD);
            if(ICol<2){
                if(ICol==0){
                    lay.setMargins(30, 16, 30, 5);
                    txtGeneric1.setText(" SKB ");
                }else{
                    lay.setMargins(30, 5, 30, 5);
                    txtGeneric1.setText(" BARIS ");
                }
            }else{
                lay.setMargins(30, 1, 30, 1);
                txtGeneric1.setGravity(Gravity.CENTER_HORIZONTAL);
                txtGeneric1.setText(String.valueOf(ICol-1));
            }
            tr.addView(txtGeneric1,lay);
            for (JCol = 1; JCol < colums; JCol++) {
                if(ICol==0){
                    TextView txtSKB = new TextView(getActivity());
                    txtSKB.setBackgroundDrawable(gd);
                    txtSKB.setTextColor(getResources().getColor(R.color.Carbon_Gray));
                    txtSKB.setPadding(15,10,15,10);
                    txtSKB.setText(skb[JCol]);
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(10);
                    txtSKB.setFilters(fArray);
                    txtSKB.setTextSize(18);
                    txtSKB.setGravity(Gravity.CENTER_HORIZONTAL);
                    txtSKB.setTypeface(null, Typeface.BOLD);
                    txtSKB.setVisibility(View.VISIBLE);
                    if(txtSKB.getParent() != null) {
                        ((ViewGroup)txtSKB.getParent()).removeView(txtSKB); // <- fix
                    }
                    tr.addView(txtSKB);
                    TableRow.LayoutParams the_paramSKB;
                    the_paramSKB = (TableRow.LayoutParams)txtSKB.getLayoutParams();
                    the_paramSKB.setMargins(1, 16, 0, 16);
                    the_paramSKB.span = 2;
                    txtSKB.setLayoutParams(the_paramSKB);
                }
                if(ICol==1){
                    String kiri = baris[JCol][1];
                    String kanan = baris[JCol][2];
                    if(kiri==null || kiri.equalsIgnoreCase("")){
                        kiri = "--";
                        // LEFT //
                        TextView txtLeft = new TextView(getActivity());
                        txtLeft.setBackgroundDrawable(polos);
                        txtLeft.setTextColor(getResources().getColor(R.color.Carbon_Gray));
                        txtLeft.setPadding(15,10,15,10);
                        InputFilter[] fArray = new InputFilter[1];
                        fArray[0] = new InputFilter.LengthFilter(10);
                        txtLeft.setFilters(fArray);
                        txtLeft.setText(kiri);
                        txtLeft.setTextSize(18);
                        txtLeft.setTypeface(null, Typeface.BOLD);
                        txtLeft.setVisibility(View.VISIBLE);
                        if(txtLeft.getParent() != null) {
                            ((ViewGroup)txtLeft.getParent()).removeView(txtLeft); // <- fix
                        }
                        tr.addView(txtLeft);
                        TableRow.LayoutParams the_paramLeft;
                        the_paramLeft = (TableRow.LayoutParams)txtLeft.getLayoutParams();
                        the_paramLeft.setMargins(1, 5, 0, 5);
                        the_paramLeft.span = 1;
                        txtLeft.setLayoutParams(the_paramLeft);
                    }else{
                        // LEFT //
                        TextView txtLeft = new TextView(getActivity());
                        txtLeft.setBackgroundDrawable(gd2);
                        txtLeft.setTextColor(getResources().getColor(R.color.Carbon_Gray));
                        txtLeft.setPadding(15,10,15,10);
                        InputFilter[] fArray = new InputFilter[1];
                        fArray[0] = new InputFilter.LengthFilter(10);
                        txtLeft.setFilters(fArray);
                        txtLeft.setText(kiri);
                        txtLeft.setTextSize(18);
                        txtLeft.setTypeface(null, Typeface.BOLD);
                        txtLeft.setVisibility(View.VISIBLE);
                        if(txtLeft.getParent() != null) {
                            ((ViewGroup)txtLeft.getParent()).removeView(txtLeft); // <- fix
                        }
                        tr.addView(txtLeft);
                        TableRow.LayoutParams the_paramLeft;
                        the_paramLeft = (TableRow.LayoutParams)txtLeft.getLayoutParams();
                        the_paramLeft.setMargins(1, 5, 0, 5);
                        the_paramLeft.span = 1;
                        txtLeft.setLayoutParams(the_paramLeft);
                    }
                    if(kanan==null || kanan.equalsIgnoreCase("")){
                        kanan = "--";
                        // RIGHT //
                        TextView txtRight = new TextView(getActivity());
                        txtRight.setBackgroundDrawable(polos);
                        txtRight.setTextColor(getResources().getColor(R.color.Carbon_Gray));
                        txtRight.setPadding(15,10,15,10);
                        InputFilter[] fArray2 = new InputFilter[1];
                        fArray2[0] = new InputFilter.LengthFilter(10);
                        txtRight.setFilters(fArray2);
                        txtRight.setText(kanan);
                        txtRight.setTextSize(18);
                        txtRight.setTypeface(null, Typeface.BOLD);
                        txtRight.setVisibility(View.VISIBLE);
                        if(txtRight.getParent() != null) {
                            ((ViewGroup)txtRight.getParent()).removeView(txtRight); // <- fix
                        }
                        tr.addView(txtRight);
                        TableRow.LayoutParams the_paramRight;
                        the_paramRight = (TableRow.LayoutParams)txtRight.getLayoutParams();
                        the_paramRight.setMargins(1, 5, 0, 5);
                        the_paramRight.span = 1;
                        txtRight.setLayoutParams(the_paramRight);
                    }else{
                        // RIGHT //
                        TextView txtRight = new TextView(getActivity());
                        txtRight.setBackgroundDrawable(gd2);
                        txtRight.setTextColor(getResources().getColor(R.color.Carbon_Gray));
                        txtRight.setPadding(15,10,15,10);
                        InputFilter[] fArray2 = new InputFilter[1];
                        fArray2[0] = new InputFilter.LengthFilter(10);
                        txtRight.setFilters(fArray2);
                        txtRight.setText(kanan);
                        txtRight.setTextSize(18);
                        txtRight.setTypeface(null, Typeface.BOLD);
                        txtRight.setVisibility(View.VISIBLE);
                        if(txtRight.getParent() != null) {
                            ((ViewGroup)txtRight.getParent()).removeView(txtRight); // <- fix
                        }
                        tr.addView(txtRight);
                        TableRow.LayoutParams the_paramRight;
                        the_paramRight = (TableRow.LayoutParams)txtRight.getLayoutParams();
                        the_paramRight.setMargins(1, 5, 0, 5);
                        the_paramRight.span = 1;
                        txtRight.setLayoutParams(the_paramRight);
                    }
                }
                if(ICol>1) {
                    TextView txtLeft = new TextView(getActivity());
                    TextView txtRight = new TextView(getActivity());
                    String nilaiLeft = null;
                    String nilaiRight = null;
                    String KondisiPokokLeft = null;
                    String KondisiPokokRight = null;
                    int JumlahBaris = ICol - 1;
                    if (ICol == 2) {
                        JumlahBaris = ICol - 1;
                    }
                    if (itemcondition[JCol][1][ICol - 1] == null || itemcondition[JCol][1][ICol - 1].length() == 0) {
                        nilaiLeft = "N";
                    } else {
                        nilaiLeft = itemcondition[JCol][1][ICol - 1];
                    }
                    if (itemcondition[JCol][2][ICol - 1] == null || itemcondition[JCol][2][ICol - 1].length() == 0) {
                        nilaiRight = "N";
                    } else {

                        nilaiRight = itemcondition[JCol][2][ICol - 1];
                    }
                    if (pokok[JCol][1][ICol - 1] == null || pokok[JCol][1][ICol - 1].length() == 0) {
                        KondisiPokokLeft = "N";
                    } else {
                        KondisiPokokLeft = pokok[JCol][1][ICol - 1];
                    }
                    if (pokok[JCol][1][ICol - 1] == null || pokok[JCol][1][ICol - 1].length() == 0) {
                        KondisiPokokRight = "N";
                    } else {
                        KondisiPokokRight = pokok[JCol][2][ICol - 1];
                    }
                    // LEFT //
                    if (nilaiLeft.equals("1")) {
                        txtLeft.setBackgroundDrawable(red);
                        txtLeft.setTextColor(getResources().getColor(R.color.Carbon_Gray));
                        txtLeft.setPadding(15, 10, 15, 10);
                        txtLeft.setText("1");
                        txtLeft.setTextColor(getResources().getColor(R.color.Milk_White));
                        txtLeft.setTextSize(18);
                        txtLeft.setGravity(Gravity.CENTER_HORIZONTAL);
                        txtLeft.setTypeface(null, Typeface.BOLD);
                        txtLeft.setVisibility(View.VISIBLE);
                        if (txtLeft.getParent() != null) {
                            ((ViewGroup) txtLeft.getParent()).removeView(txtLeft); // <- fix
                        }
                        tr.addView(txtLeft);
                        TableRow.LayoutParams the_param;
                        the_param = (TableRow.LayoutParams) txtLeft.getLayoutParams();
                        the_param.setMargins(1, 1, 0, 1);
                        the_param.span = 1;
                        txtLeft.setLayoutParams(the_param);
                    }
                    if (nilaiLeft.equals("2")) {
                        txtLeft.setBackgroundDrawable(yellow);
                        txtLeft.setTextColor(getResources().getColor(R.color.Carbon_Gray));
                        txtLeft.setPadding(15, 10, 15, 10);
                        txtLeft.setText("2");
                        txtLeft.setTextColor(getResources().getColor(R.color.Milk_White));
                        txtLeft.setTextSize(18);
                        txtLeft.setGravity(Gravity.CENTER_HORIZONTAL);
                        txtLeft.setTypeface(null, Typeface.BOLD);
                        txtLeft.setVisibility(View.VISIBLE);
                        if (txtLeft.getParent() != null) {
                            ((ViewGroup) txtLeft.getParent()).removeView(txtLeft); // <- fix
                        }
                        tr.addView(txtLeft);
                        TableRow.LayoutParams the_param;
                        the_param = (TableRow.LayoutParams) txtLeft.getLayoutParams();
                        the_param.setMargins(1, 1, 0, 1);
                        the_param.span = 1;
                        txtLeft.setLayoutParams(the_param);
                    }
                    if (nilaiLeft.equals("3")) {
                        txtLeft.setBackgroundDrawable(green);
                        txtLeft.setTextColor(getResources().getColor(R.color.Carbon_Gray));
                        txtLeft.setPadding(15, 10, 15, 10);
                        txtLeft.setText("3");
                        txtLeft.setTextColor(getResources().getColor(R.color.Milk_White));
                        txtLeft.setTextSize(18);
                        txtLeft.setGravity(Gravity.CENTER_HORIZONTAL);
                        txtLeft.setTypeface(null, Typeface.BOLD);
                        txtLeft.setVisibility(View.VISIBLE);
                        if (txtLeft.getParent() != null) {
                            ((ViewGroup) txtLeft.getParent()).removeView(txtLeft); // <- fix
                        }
                        tr.addView(txtLeft);
                        TableRow.LayoutParams the_param;
                        the_param = (TableRow.LayoutParams) txtLeft.getLayoutParams();
                        the_param.setMargins(1, 1, 0, 1);
                        the_param.span = 1;
                        txtLeft.setLayoutParams(the_param);
                    }
                    if (nilaiLeft.equals("0") || nilaiLeft.equals(null) || nilaiLeft.equals("N")) {
                        String BarisLeft = baris[JCol][1];
                        txtLeft.setTextColor(getResources().getColor(R.color.Carbon_Gray));
                        txtLeft.setGravity(Gravity.CENTER_HORIZONTAL);
                        txtLeft.setPadding(15, 10, 15, 10);
                        if (BarisLeft.equals("") && nilaiLeft.equals("0")) {
                            txtLeft.setBackgroundDrawable(polos);
                            txtLeft.setText("  ");
                            txtLeft.setTextSize(18);
                            txtLeft.setTypeface(null, Typeface.BOLD);
                            txtLeft.setVisibility(View.VISIBLE);
                            if (txtLeft.getParent() != null) {
                                ((ViewGroup) txtLeft.getParent()).removeView(txtLeft); // <- fix
                            }
                            tr.addView(txtLeft);
                            TableRow.LayoutParams the_param;
                            the_param = (TableRow.LayoutParams) txtLeft.getLayoutParams();
                            the_param.setMargins(1, 1, 0, 1);
                            the_param.span = 1;
                            txtLeft.setLayoutParams(the_param);
                        }
                        else {
                            if (KondisiPokokLeft == null) {
                                txtLeft.setBackgroundDrawable(polos);
                                txtLeft.setText("  ");
                            } else {
                                if (KondisiPokokLeft.equals("Mati")) {
                                    txtLeft.setBackgroundDrawable(white);
                                    txtLeft.setText("M");
                                } else if (KondisiPokokLeft.equals("Kosong")) {
                                    txtLeft.setBackgroundDrawable(white);
                                    txtLeft.setText("K");
                                } else if (KondisiPokokLeft.equals("HCV")) {
                                    txtLeft.setBackgroundDrawable(white);
                                    txtLeft.setText("H");
                                } else {
                                    txtLeft.setBackgroundDrawable(polos);
                                    txtLeft.setText("  ");
                                }
                            }

                            txtLeft.setTextSize(18);
                            txtLeft.setTypeface(null, Typeface.BOLD);
                            txtLeft.setVisibility(View.VISIBLE);
                            if (txtLeft.getParent() != null) {
                                ((ViewGroup) txtLeft.getParent()).removeView(txtLeft); // <- fix
                            }
                            tr.addView(txtLeft);
                            TableRow.LayoutParams the_param;
                            the_param = (TableRow.LayoutParams) txtLeft.getLayoutParams();
                            the_param.setMargins(1, 1, 0, 1);
                            the_param.span = 1;
                            txtLeft.setLayoutParams(the_param);
                        }
                    }
// RIGHT //
                    if (nilaiRight.equals("1")) {
                        txtRight.setBackgroundDrawable(red);
                        txtRight.setTextColor(getResources().getColor(R.color.Carbon_Gray));
                        txtRight.setPadding(15, 10, 15, 10);
                        txtRight.setText("1");
                        txtRight.setGravity(Gravity.CENTER_HORIZONTAL);
                        txtRight.setTextColor(getResources().getColor(R.color.Milk_White));
                        txtRight.setTextSize(18);
                        txtRight.setTypeface(null, Typeface.BOLD);
                        txtRight.setVisibility(View.VISIBLE);
                        if (txtRight.getParent() != null) {
                            ((ViewGroup) txtRight.getParent()).removeView(txtRight); // <- fix
                        }
                        tr.addView(txtRight);
                        TableRow.LayoutParams the_param;
                        the_param = (TableRow.LayoutParams) txtRight.getLayoutParams();
                        the_param.setMargins(1, 1, 0, 1);
                        the_param.span = 1;
                        txtRight.setLayoutParams(the_param);
                    }
                    if (nilaiRight.equals("2")) {
                        txtRight.setBackgroundDrawable(yellow);
                        txtRight.setTextColor(getResources().getColor(R.color.Carbon_Gray));
                        txtRight.setPadding(15, 10, 15, 10);
                        txtRight.setText("2");
                        txtRight.setGravity(Gravity.CENTER_HORIZONTAL);
                        txtRight.setTextColor(getResources().getColor(R.color.Milk_White));
                        txtRight.setTextSize(18);
                        txtRight.setTypeface(null, Typeface.BOLD);
                        txtRight.setVisibility(View.VISIBLE);
                        if (txtRight.getParent() != null) {
                            ((ViewGroup) txtRight.getParent()).removeView(txtRight); // <- fix
                        }
                        tr.addView(txtRight);
                        TableRow.LayoutParams the_param;
                        the_param = (TableRow.LayoutParams) txtRight.getLayoutParams();
                        the_param.setMargins(1, 1, 0, 1);
                        the_param.span = 1;
                        txtRight.setLayoutParams(the_param);
                    }
                    if (nilaiRight.equals("3")) {
                        txtRight.setBackgroundDrawable(green);
                        txtRight.setTextColor(getResources().getColor(R.color.Carbon_Gray));
                        txtRight.setPadding(15, 10, 15, 10);
                        txtRight.setText("3");
                        txtRight.setGravity(Gravity.CENTER_HORIZONTAL);
                        txtRight.setTextColor(getResources().getColor(R.color.Milk_White));
                        txtRight.setTextSize(18);
                        txtRight.setTypeface(null, Typeface.BOLD);
                        txtRight.setVisibility(View.VISIBLE);
                        if (txtRight.getParent() != null) {
                            ((ViewGroup) txtRight.getParent()).removeView(txtRight); // <- fix
                        }
                        tr.addView(txtRight);
                        TableRow.LayoutParams the_param;
                        the_param = (TableRow.LayoutParams) txtRight.getLayoutParams();
                        the_param.setMargins(1, 1, 0, 1);
                        the_param.span = 1;
                        txtRight.setLayoutParams(the_param);
                    }
                    if (nilaiRight.equals("0") || nilaiRight.equals(null) || nilaiRight.equals("N")) {
                        String BarisRight = baris[JCol][2];
                        txtRight.setGravity(Gravity.CENTER_HORIZONTAL);
                        txtRight.setTextColor(getResources().getColor(R.color.Carbon_Gray));
                        txtRight.setPadding(15, 10, 15, 10);
                        if (BarisRight.equals("") && nilaiRight.equals("0")) {
                            txtRight.setBackgroundDrawable(polos);
                            txtRight.setText("  ");
                            txtRight.setTextSize(18);
                            txtRight.setTypeface(null, Typeface.BOLD);
                            txtRight.setVisibility(View.VISIBLE);
                            if (txtRight.getParent() != null) {
                                ((ViewGroup) txtRight.getParent()).removeView(txtRight); // <- fix
                            }
                            tr.addView(txtRight);
                            TableRow.LayoutParams the_param;
                            the_param = (TableRow.LayoutParams) txtRight.getLayoutParams();
                            the_param.setMargins(1, 1, 0, 1);
                            the_param.span = 1;
                            txtRight.setLayoutParams(the_param);
                        }else{
                            if (KondisiPokokRight == null) {
                                txtRight.setBackgroundDrawable(polos);
                                txtRight.setText("  ");
                            } else {
                                if (KondisiPokokRight.equals("Mati")) {
                                    txtRight.setBackgroundDrawable(white);
                                    txtRight.setText("M");
                                } else if (KondisiPokokRight.equals("Kosong")) {
                                    txtRight.setBackgroundDrawable(white);
                                    txtRight.setText("K");
                                } else if (KondisiPokokRight.equals("HCV")) {
                                    txtRight.setBackgroundDrawable(white);
                                    txtRight.setText("H");
                                } else {
                                    txtRight.setBackgroundDrawable(polos);
                                    txtRight.setText("  ");
                                }
                            }
                            txtRight.setTextSize(18);
                            txtRight.setTypeface(null, Typeface.BOLD);
                            txtRight.setVisibility(View.VISIBLE);
                            if (txtRight.getParent() != null) {
                                ((ViewGroup) txtRight.getParent()).removeView(txtRight); // <- fix
                            }
                            tr.addView(txtRight);
                            TableRow.LayoutParams the_param;
                            the_param = (TableRow.LayoutParams) txtRight.getLayoutParams();
                            the_param.setMargins(1, 1, 0, 1);
                            the_param.span = 1;
                            txtRight.setLayoutParams(the_param);
                        }
                    }
                }
            }
            tableLayout.addView(tr);
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private class GetDataAsyncTask extends AsyncTask<Void, List<ResultModel>, List<ResultModel>> {
        private DialogProgress dialogProgress;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(!getActivity().isFinishing()){
                dialogProgress = new DialogProgress(getActivity(),getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }
        @Override
        protected List<ResultModel> doInBackground(Void... voids) {
            database.openTransaction();
            List<ResultModel> listTemp = new ArrayList<ResultModel>();
            List<Object> listObject;
            try{
                UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
                String[] a = new String[6];
                String query = null;
                a[0] = "9999-12-31";
                a[1] = userLogin.getDivision();
                a[2] = mParam1;
                a[3] = mParam2;
                a[4] = mParam3;
                a[5] = "0";

                if(mParam4.equalsIgnoreCase("MATURE")){
                    query = "SELECT b.Block,"+SpinnerValue + " as Condition FROM tblT_BlockCondition a " +
                            "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi=? AND b.project LIKE 'M%'" +
                            "WHERE a.zyear=? AND  a.period=? AND a.block =? AND a.TransLevel=? AND a."+SpinnerValue+" > 0 AND a."+SpinnerValue+ " != 0.0";
                }
                if(mParam4.equalsIgnoreCase("IMMATURE")){
                    query = "SELECT b.Block,"+SpinnerValue + " as Condition FROM tblT_BlockCondition a " +
                            "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi=? AND b.project LIKE 'T%'" +
                            "WHERE a.zyear=? AND  a.period=? AND a.block =? AND a.TransLevel=? AND a."+SpinnerValue+" > 0 AND a."+SpinnerValue+ " != 0.0";
                }
                listObject = database.getListDataRawQuery(query,"RESULT",a);
                if(listObject.size() > 0){
                    for(int i = 0; i < listObject.size(); i++){
                        ResultModel resultModel = (ResultModel) listObject.get(i);
                        ConditionColor = resultModel.getCondition();
                        listTemp.add(resultModel);
                    }
                }
            }catch(SQLiteException e){
                e.printStackTrace();
                database.closeTransaction();
            }finally{
                database.closeTransaction();
            }
            return listTemp;

        }

        @Override
        protected void onPostExecute(List<ResultModel> listTemp) {
            super.onPostExecute(listTemp);
            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }
            for(int i = 0; i < listTemp.size(); i++){
                ResultModel resultModel = (ResultModel) listTemp.get(i);
                ConditionColor = resultModel.getCondition();
            }
            BackgroundTask1 task = new BackgroundTask1(SummaryPokokTab.this);
            task.execute();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        asyncDetail.cancel(true);
    }
}
