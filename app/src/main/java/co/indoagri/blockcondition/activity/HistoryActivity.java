package co.indoagri.blockcondition.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import co.indoagri.blockcondition.MyApps;
import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.handler.BroadcastConnectorHandler;
import co.indoagri.blockcondition.widget.CustomSnackBar;
import co.indoagri.blockcondition.widget.ToastMessage;

public class HistoryActivity extends BaseActivity implements
        BaseActivity.LogOutListener,
        BroadcastConnectorHandler.ConnectivityReceiverListener,
        View.OnClickListener{

    boolean doubleBackToExitPressedOnce = false;
    ToastMessage toastMessage;
    ConstraintLayout constraintLayout;
    View rootView;
    public static TextView txtTitle,txtTanggalBlock;
    public static View relContainerTitle,relContainerSubTitle;
    boolean onUserInteraction = false;
    Button btnNext,btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        animOnStart();
        registerBaseActivityReceiver();
        setContentView(R.layout.activity_history);

        constraintLayout = (ConstraintLayout)findViewById(R.id.constraint);
        txtTitle = (TextView)findViewById(R.id.txtTitleHeader);
        txtTanggalBlock = (TextView)findViewById(R.id.txtTanggalBlock);
        relContainerTitle = (View)findViewById(R.id.titleActivityHeader);
        relContainerSubTitle = (View)findViewById(R.id.titleSubActivityHeader);
        toastMessage = new ToastMessage(getApplicationContext());
        rootView = constraintLayout;
        btnNext = (Button)findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        btnBack = (Button)findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
        setDefault();
    }

    private void setDefault(){
        btnNext.setVisibility(View.GONE);
        relContainerSubTitle.setVisibility(View.GONE);
        txtTitle.setText(getResources().getString(R.string.history_condition));
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.e("ONDESTROY", "OnDestory()");
        stopLogoutTimer();
        unRegisterBaseActivityReceiver();
    }
    @Override
    public void doLogout() {

        runOnUiThread(new Runnable() {
            public void run() {
                toastMessage.shortMessage("Interaction Logout");
                onUserInteraction = false;
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(!onUserInteraction){
            Log.e("INTERACTION", "User interacting with screen");
            onUserInteraction = true;
            startLogoutTimer(this, this);
        }
        else{

        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if(!onUserInteraction){
            Log.e("INTERACTION", "User interacting with screen");
            onUserInteraction = true;
            startLogoutTimer(this, this);
        }
        else{
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("ONSTOP", "OnStop()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("ONPAUSE", "onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApps.getInstance().setConnectivityListener(this);
        Log.e("ONRESUME", "onResume()");
    }

    @Override
    public void onBackPressed() {

        if (getFragmentManagerAccount() > 1) {
            // Log.i("Total",String.valueOf(fragmentManager.getBackStackEntryCount()));
            Log.i("MainActivity", "popping backstack");
            baseFragmentManager.popBackStack();
        } else {
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            animOnFinish();
            finish();

        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void checkConnection() {
        boolean isConnected = BroadcastConnectorHandler.isConnect(getApplicationContext());
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        if (isConnected) {
            CustomSnackBar customSnackbar = CustomSnackBar.make((ViewGroup)rootView.getParent(),8000);
            customSnackbar.setText("Connected");
            customSnackbar.setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onStart();
                }
            });
            customSnackbar.show();
        } else {
            CustomSnackBar customSnackbar = CustomSnackBar.make((ViewGroup)rootView.getParent(),8000);
            customSnackbar.setText("No network connection!");
            customSnackbar.setAction("Retry", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(),"Not Connected",Toast.LENGTH_SHORT).show();
                }
            });
            customSnackbar.show();
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                if (getFragmentManagerAccount() > 1) {
                    // Log.i("Total",String.valueOf(fragmentManager.getBackStackEntryCount()));
                    Log.i("MainActivity", "popping backstack");
                    baseFragmentManager.popBackStack();
                } else {
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    animOnFinish();
                    finish();

                }
                break;
            case R.id.btnNext:

                break;
            default:
                break;
        }
    }

}
