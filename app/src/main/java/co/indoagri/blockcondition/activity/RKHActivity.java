package co.indoagri.blockcondition.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.RKHAdapter;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.handler.GpsHandler;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.RKH.RKH_HEADER;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.services.GPSService;
import co.indoagri.blockcondition.services.GPSTriggerService;

public class RKHActivity extends AppCompatActivity implements
        RKHAdapter.ItemListener, RKHAdapter.CardTugaskan, RKHAdapter.CardEdit {

    Toolbar toolbar;
    TextView mTextToolbar;
    TextView txtLastModified;
    List<RKH_HEADER> rkh_headers = new ArrayList<RKH_HEADER>();
    RecyclerView recyclerView;
    RKHAdapter adapter;
    public static int PEKERJA_RKH_TAMBAH = 01;
    public static int CHANGE_DATE_RKH = 99;
    public static int CHANGE_PEKERJA = 03;
    DatabaseHandler database = new DatabaseHandler(RKHActivity.this);
    String companyCode;
    String estate;
    String division;
    String gang;
    String nik;
    private GetDataAsyncTask getDataAsync;
    TextView txtDate;
    String RKHDATE;
    RelativeLayout chooseDate;
    private GpsHandler gpsHandler;
    GPSService gps;
    String gpsKoordinat = "0.0:0.0";
    double latitude = 0;
    double longitude = 0;
    @Override
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.rkh_layout);
        gps = new GPSService(RKHActivity.this);
        gpsHandler = new GpsHandler(RKHActivity.this);
        GPSTriggerService.rkhActivity = this;
        gpsHandler.startGPS();
        //isMyServiceRunning(GPSTriggerService.class);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        setUpToolbar();
        SetData();
        setButton();
        setLocation();
    }
    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        txtDate = (TextView)findViewById(R.id.txtDate);
        txtLastModified = (TextView)findViewById(R.id.txtLastModified);
        chooseDate = (RelativeLayout)findViewById(R.id.chooseDate);
        if(getLastModifiedRKH()!=null){
            String Date = new DateLocal(new Date(getLastModifiedRKH().getMODIFIED_DATE())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT);
            LastModified(Date);
        }
        if(new PreferenceManager(getApplicationContext(), Constants.shared_name).getRkhInputrkhdate()!=null){
            RKHDATE = new PreferenceManager(getApplicationContext(), Constants.shared_name).getRkhInputrkhdate();
        }else{
            RKHDATE = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
        }
        //txtDate.setText(new DateLocal(RKHDATE, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
        CheckSetDate(RKHDATE);
        chooseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  setTanggal();
                Toast.makeText(RKHActivity.this, "test Tanggal", Toast.LENGTH_SHORT).show();
            }
        });
        mTextToolbar.setText(getResources().getString(R.string.mainmenu_rkh));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    void setAdapter(){
         adapter = new RKHAdapter(this, rkh_headers, this,this,this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }
    void SetData(){
        companyCode = ViewProfile.getCompanyCode();
        estate = ViewProfile.getEstate();
        division = ViewProfile.getDivisions();
        gang = ViewProfile.getGang();
        nik = ViewProfile.getUserNIK();
        setAdapter();
        getDataAsync = new GetDataAsyncTask(false, RKH_HEADER.TABLE_NAME, null,
                RKH_HEADER.XML_DIVISION+ "=? and "+
                        RKH_HEADER.XML_GANG+ "=? and "+
                        RKH_HEADER.XML_RKH_DATE+ "=? ",
                new String [] {division,gang,RKHDATE},
                null, null, null, null);
        getDataAsync.execute();
    }

    private class GetDataAsyncTask extends AsyncTask<Void, List<RKH_HEADER>, List<RKH_HEADER>> {
        boolean distinct;
        String tableName;
        String [] columns;
        String whereClause;
        String [] whereArgs;
        String groupBy;
        String having;
        String orderBy;
        String limit;

        public GetDataAsyncTask(boolean distinct, String tableName, String [] columns, String whereClause, String [] whereParams,
                                String groupBy, String having, String orderBy, String limit){
            this.distinct = distinct;
            this.tableName = tableName;
            this.columns = columns;
            this.whereClause = whereClause;
            this.whereArgs = whereParams;
            this.groupBy = groupBy;
            this.having = having;
            this.orderBy = orderBy;
            this.limit = limit;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected List<RKH_HEADER> doInBackground(Void... voids) {
            DatabaseHandler database = new DatabaseHandler(RKHActivity.this);
            List<RKH_HEADER> listTemp = new ArrayList<RKH_HEADER>();
            List<Object> listObject;

            database.openTransaction();
            listObject = database.getListData(distinct, tableName, columns, whereClause, whereArgs, groupBy, having, orderBy, limit);
            database.closeTransaction();

            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    RKH_HEADER blockHdrc = (RKH_HEADER) listObject.get(i);

                    listTemp.add(blockHdrc);
                }
            }

            return listTemp;
        }

        @Override
        protected void onPostExecute(List<RKH_HEADER> listTemp) {
            super.onPostExecute(listTemp);
            for(int i = 0; i < listTemp.size(); i++){
                RKH_HEADER rkh_headerList = (RKH_HEADER) listTemp.get(i);
                adapter.addData(rkh_headerList);

            }

        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (gps != null) {
            gps.stopUsingGPS();
        }
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    @Override
    public void onItemClick(RKH_HEADER item, int position) {
            RKH_HEADER sets =  rkh_headers.get(position);
            sets.setExpendable(!sets.isExpendable());
            adapter.notifyItemChanged(position);
    }
    @Override
    public void onCardTugaskan(RKH_HEADER item) {
        Class newclass = null;
        final Activity thisActivity = this;
        String activityString="co.indoagri.blockcondition.activity.RKHJOBActivity";
        try {
            newclass = Class.forName(activityString);
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
        }

        if(newclass != null) {
            Intent nextIntent = new Intent(thisActivity, newclass);
            nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            nextIntent.putExtra("Data",item);
            nextIntent.putExtra("Method","new");
            startActivityForResult(nextIntent, CHANGE_PEKERJA);
        } else {
            Toast.makeText(getApplicationContext(),"new class null",Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onCardEdit(RKH_HEADER item) {
        Class newclass = null;
        final Activity thisActivity = this;
        String activityString="co.indoagri.blockcondition.activity.RKHJOBActivity";
        try {
            newclass = Class.forName(activityString);
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
        }

        if(newclass != null) {
            Intent nextIntent = new Intent(RKHActivity.this, newclass);
            nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            nextIntent.putExtra("Data",item);
            nextIntent.putExtra("Method","edit");
            startActivityForResult(nextIntent, CHANGE_PEKERJA);
        } else {
            Toast.makeText(getApplicationContext(),"new class null",Toast.LENGTH_SHORT).show();
        }
    }

    void setButton(){
        LinearLayout btnPekerjaTambah = (LinearLayout)findViewById(R.id.cardJob);
        btnPekerjaTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(RKHActivity.this,RKHPekerjaActivity.class);
                newIntent.putExtra("TOOLBARTITLE","Pilih Pekerja");
                startActivityForResult(newIntent, PEKERJA_RKH_TAMBAH);
            }
        });
    }
    void setTanggal(){
                Intent newIntent = new Intent(RKHActivity.this,RKHArchieveActivity.class);
                newIntent.putExtra("TOOLBARTITLE","Pilih TANGGAL RKH");
                startActivityForResult(newIntent, CHANGE_DATE_RKH);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == PEKERJA_RKH_TAMBAH) {
            if(resultCode == RESULT_OK) {
                rkh_headers.clear();
                SetData();
            }
        }
        if (requestCode == CHANGE_DATE_RKH) {
            Log.i("Ganti Tanggal",String.valueOf(CHANGE_DATE_RKH));
            Toast.makeText(RKHActivity.this, String.valueOf(CHANGE_DATE_RKH), Toast.LENGTH_SHORT).show();
            String Dates = "";
            if(resultCode == RESULT_OK) {
                if (data!=null) {
                    Dates =  data.getExtras().getString("rkhdate");
                    RKHDATE = new DateLocal(Dates, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_INPUT);
                    CheckSetDate(RKHDATE);

                }else{
                    RKHDATE = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
                    //txtDate.setText(new DateLocal(RKHDATE, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
                    CheckSetDate(RKHDATE);
                }

                SetData();
            }
        }
        if (requestCode == CHANGE_PEKERJA) {
            if(resultCode == RESULT_OK) {
                rkh_headers.clear();
                SetData();
            }
        }
    }

    void CheckSetDate(String Dates){
        try{
            Date parsedDate = DateLocal.FORMAT_INPUT.parse(Dates);
            Calendar cal = Calendar.getInstance();
            cal.setTime(parsedDate);
            int days = cal.get(Calendar.DAY_OF_WEEK);
            int dates = cal.get(Calendar.DATE);
            int months = cal.get(Calendar.MONTH);
            int years = cal.get(Calendar.YEAR);

            int hours = cal.get(Calendar.HOUR);
            int minutes = cal.get(Calendar.MINUTE);
            int seconds = cal.get(Calendar.SECOND);
            int amPm = cal.get(Calendar.AM_PM);

            switch (amPm) {
                case Calendar.AM:

                    break;
                case Calendar.PM:
                    hours = hours + 12;
                default:
                    break;
            }

           // txtDate.setText(digitFormat(hours, 2) + ":" + digitFormat(minutes,2) + ":" + digitFormat(seconds,2));

            String hari = "";

            switch (days) {
                case Calendar.SUNDAY:
                    hari = getResources().getString(R.string.minggu);
                    break;
                case Calendar.MONDAY:
                    hari = getResources().getString(R.string.senin);
                    break;
                case Calendar.TUESDAY:
                    hari = getResources().getString(R.string.selasa);
                    break;
                case Calendar.WEDNESDAY:
                    hari = getResources().getString(R.string.rabu);
                    break;
                case Calendar.THURSDAY:
                    hari = getResources().getString(R.string.kamis);
                    break;
                case Calendar.FRIDAY:
                    hari = getResources().getString(R.string.jumat);
                    break;
                case Calendar.SATURDAY:
                    hari = getResources().getString(R.string.sabtu);
                    break;
                default:
                    break;
            }


            String bulan = "";

            switch (months) {
                case Calendar.JANUARY:
                    bulan = getResources().getString(R.string.januari);
                    break;
                case Calendar.FEBRUARY:
                    bulan = getResources().getString(R.string.februari);
                    break;
                case Calendar.MARCH:
                    bulan = getResources().getString(R.string.maret);
                    break;
                case Calendar.APRIL:
                    bulan = getResources().getString(R.string.april);
                    break;
                case Calendar.MAY:
                    bulan = getResources().getString(R.string.mei);
                    break;
                case Calendar.JUNE:
                    bulan = getResources().getString(R.string.juni);
                    break;
                case Calendar.JULY:
                    bulan = getResources().getString(R.string.juli);
                    break;
                case Calendar.AUGUST:
                    bulan = getResources().getString(R.string.agustus);
                    break;
                case Calendar.SEPTEMBER:
                    bulan = getResources().getString(R.string.september);
                    break;
                case Calendar.OCTOBER:
                    bulan = getResources().getString(R.string.oktober);
                    break;
                case Calendar.NOVEMBER:
                    bulan = getResources().getString(R.string.november);
                    break;
                case Calendar.DECEMBER:
                    bulan = getResources().getString(R.string.desember);
                    break;
                default:
                    break;
            }

            txtDate.setText(hari + " , " + dates + " " + bulan + " " + years);

        }catch (Exception e) {}

    }
    void LastModified(String Dates){
        try{

            Date parsedDate = DateLocal.FORMAT_DATE_TIME_INPUT.parse(Dates);
            Calendar cal = Calendar.getInstance();
            cal.setTime(parsedDate);
            int days = cal.get(Calendar.DAY_OF_WEEK);
            int dates = cal.get(Calendar.DATE);
            int months = cal.get(Calendar.MONTH);
            int years = cal.get(Calendar.YEAR);

            int hours = cal.get(Calendar.HOUR);
            int minutes = cal.get(Calendar.MINUTE);
            int seconds = cal.get(Calendar.SECOND);
            int amPm = cal.get(Calendar.AM_PM);

            switch (amPm) {
                case Calendar.AM:

                    break;
                case Calendar.PM:
                    hours = hours + 12;
                default:
                    break;
            }
            String Time = digitFormat(hours, 2) + ":" + digitFormat(minutes,2) + ":" + digitFormat(seconds,2);
            // txtDate.setText(digitFormat(hours, 2) + ":" + digitFormat(minutes,2) + ":" + digitFormat(seconds,2));

            String hari = "";

            switch (days) {
                case Calendar.SUNDAY:
                    hari = getResources().getString(R.string.minggu);
                    break;
                case Calendar.MONDAY:
                    hari = getResources().getString(R.string.senin);
                    break;
                case Calendar.TUESDAY:
                    hari = getResources().getString(R.string.selasa);
                    break;
                case Calendar.WEDNESDAY:
                    hari = getResources().getString(R.string.rabu);
                    break;
                case Calendar.THURSDAY:
                    hari = getResources().getString(R.string.kamis);
                    break;
                case Calendar.FRIDAY:
                    hari = getResources().getString(R.string.jumat);
                    break;
                case Calendar.SATURDAY:
                    hari = getResources().getString(R.string.sabtu);
                    break;
                default:
                    break;
            }


            String bulan = "";

            switch (months) {
                case Calendar.JANUARY:
                    bulan = getResources().getString(R.string.januari);
                    break;
                case Calendar.FEBRUARY:
                    bulan = getResources().getString(R.string.februari);
                    break;
                case Calendar.MARCH:
                    bulan = getResources().getString(R.string.maret);
                    break;
                case Calendar.APRIL:
                    bulan = getResources().getString(R.string.april);
                    break;
                case Calendar.MAY:
                    bulan = getResources().getString(R.string.mei);
                    break;
                case Calendar.JUNE:
                    bulan = getResources().getString(R.string.juni);
                    break;
                case Calendar.JULY:
                    bulan = getResources().getString(R.string.juli);
                    break;
                case Calendar.AUGUST:
                    bulan = getResources().getString(R.string.agustus);
                    break;
                case Calendar.SEPTEMBER:
                    bulan = getResources().getString(R.string.september);
                    break;
                case Calendar.OCTOBER:
                    bulan = getResources().getString(R.string.oktober);
                    break;
                case Calendar.NOVEMBER:
                    bulan = getResources().getString(R.string.november);
                    break;
                case Calendar.DECEMBER:
                    bulan = getResources().getString(R.string.desember);
                    break;
                default:
                    break;
            }

            txtLastModified.setText("Last Update : ( "+Time + " "+hari+" , " + dates + " " + bulan + " " + years+" )");

        }catch (Exception e) {}

    }

    private String digitFormat(int value, int digit){
        String str = String.valueOf(value);

        int l = str.length();

        if(str.length() < digit){
            for(int i = l; i < digit; i++){
                str = "0" + str;
            }
        }
        return str;
    }
    private RKH_HEADER getLastModifiedRKH(){
        String LastUpdate;
        database.openTransaction();
        RKH_HEADER rkhHeader = (RKH_HEADER)database.getDataFirst(false, RKH_HEADER.TABLE_NAME, null,
                null,
                null,
                null, null, RKH_HEADER.XML_MODIFIED_DATE+" DESC ", "1");
        database.closeTransaction();
        return rkhHeader;
    }
    @Override
    public void onRestart()
    {
        super.onRestart();
        rkh_headers.clear();
        SetData();
    }
    void setLocation(){
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        }
        gpsKoordinat = String.valueOf(latitude) + ":" + String.valueOf(longitude);
    }
    public void updateGpsKoordinat(Location location){
        if(location != null){
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            gpsKoordinat = latitude + ":" + longitude;
        }
    }


}
