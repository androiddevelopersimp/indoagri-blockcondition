package co.indoagri.blockcondition.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.SettingsRecyclerViewAdapter;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.DatabaseHelper;
import co.indoagri.blockcondition.dialog.DialogNotification;
import co.indoagri.blockcondition.handler.FolderHandler;
import co.indoagri.blockcondition.listener.DialogNotificationListener;
import co.indoagri.blockcondition.model.MenuDashboard;
import co.indoagri.blockcondition.model.MenuSettings;
public class SetupUniversalActivity extends BaseActivity implements SettingsRecyclerViewAdapter.ItemListener, SettingsRecyclerViewAdapter.LayoutListener, DialogNotificationListener {
    DatabaseHandler database = new DatabaseHandler(SetupUniversalActivity.this);
    Toolbar toolbar;
    TextView mTextToolbar;
    RecyclerView recyclerView;
    ArrayList<MenuSettings> arrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        animOnStart();
        setContentView(R.layout.activity_universalsetup);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        setUpToolbar();
        registerBaseActivityReceiver();
        MenuInit();
    }

    void MenuInit(){
        arrayList = new ArrayList<>();
        arrayList.add(new MenuSettings("Backup", R.drawable.ic_savebackup, "#20678E",true,"BackupDatabase"));
        arrayList.add(new MenuSettings("Export", R.drawable.ic_export_svg, "#20678E",true,"ExportData"));
        SettingsRecyclerViewAdapter adapter = new SettingsRecyclerViewAdapter(this, arrayList, this,this);
        recyclerView.setAdapter(adapter);
        final GridLayoutManager layoutManager = new GridLayoutManager(this,3);
        recyclerView.setLayoutManager(layoutManager);
    }
    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText(getResources().getString(R.string.pengaturan));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.e("ONDESTROY", "OnDestory()");
        unRegisterBaseActivityReceiver();
    }

    @Override
    public void onItemClick(MenuSettings item) {

    }

    @Override
    public void onLayoutClick(MenuSettings item) {
        ToAction(item);
    }

    void ToAction(MenuSettings item){
        if(item.ActivityClass.equalsIgnoreCase("BackupDatabase")){
            try {
                copyAppDbToDownloadFolder();
            } catch (IOException e) {
                e.printStackTrace();
                new DialogNotification(SetupUniversalActivity.this, getResources().getString(R.string.informasi),
                        e.getMessage(), false).show();
            }
        }if(item.ActivityClass.equalsIgnoreCase("ExportData")){
            Class newclass = null;
            final Activity thisActivity = this;
            String activityString="co.indoagri.blockcondition.activity.SetupMenuExportActivity";
            try {
                newclass = Class.forName(activityString);
            } catch (ClassNotFoundException c) {
                c.printStackTrace();
            }

            if(newclass != null) {
                    Toast.makeText(thisActivity, "Export Data", Toast.LENGTH_SHORT).show();
                    Intent nextIntent = new Intent(thisActivity, newclass);
                    startActivity(nextIntent);

            } else {
                Toast.makeText(getApplicationContext(),"new class null",Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Class newclass = null;
            final Activity thisActivity = this;
            String activityString=item.ActivityClass;
            try {
                newclass = Class.forName(activityString);
            } catch (ClassNotFoundException c) {
                c.printStackTrace();
            }

            if(newclass != null) {
                Intent nextIntent = new Intent(thisActivity, newclass);
                nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(nextIntent);
            } else {
                  Toast.makeText(getApplicationContext(),"new class null",Toast.LENGTH_SHORT).show();
            }

        }

    }

    public void copyAppDbToDownloadFolder() throws IOException {
        try {
            FolderHandler folderHandler = new FolderHandler(SetupUniversalActivity.this);

            if(folderHandler.isSDCardWritable() && folderHandler.init()){
                File backupDB = new File(folderHandler.getFileDatabaseExport(), DatabaseHelper.dbName);
                File currentDB = getApplicationContext().getDatabasePath(DatabaseHelper.dbName);

                if (currentDB.exists()) {
                    FileInputStream fis = new FileInputStream(currentDB);
                    FileOutputStream fos = new FileOutputStream(backupDB);
                    fos.getChannel().transferFrom(fis.getChannel(), 0, fis.getChannel().size());
                    // or fis.getChannel().transferTo(0, fis.getChannel().size(), fos.getChannel());
                    fis.close();
                    fos.close();

                    MediaScannerConnection.scanFile(SetupUniversalActivity.this, new String[] { backupDB.getAbsolutePath() }, null, null);

                    new DialogNotification(SetupUniversalActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.export_successed), false).show();

                } else{
                    new DialogNotification(SetupUniversalActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.export_failed), false).show();
                }
            }else{
                new DialogNotification(SetupUniversalActivity.this, getResources().getString(R.string.informasi),
                        getResources().getString(R.string.error_sd_card), false).show();
            }
        } catch (IOException e) {
            new DialogNotification(SetupUniversalActivity.this, getResources().getString(R.string.informasi),
                    e.getMessage(), false).show();
        }
    }



    @Override
    public void onOK(boolean is_finish) {
        animOnFinish();
        finish();
    }
}
