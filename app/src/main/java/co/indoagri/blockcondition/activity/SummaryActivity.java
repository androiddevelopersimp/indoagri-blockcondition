package co.indoagri.blockcondition.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.activity.ui.main.SectionsPagerAdapter;
import co.indoagri.blockcondition.activity.ui.main.SummaryBlockTab;
import co.indoagri.blockcondition.activity.ui.main.SummaryPokokTab;
import co.indoagri.blockcondition.activity.ui.main.SummarySKBTab;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.fragment.LevelBlockFragment;
import co.indoagri.blockcondition.fragment.MasterBlockHdrcFragment;
import co.indoagri.blockcondition.fragment.YearDialog;
import co.indoagri.blockcondition.model.Data.BlockHdrc;
import co.indoagri.blockcondition.routines.Constants;

public class SummaryActivity extends AppCompatActivity implements SummaryBlockTab.OnFragmentInteractionListener,
        SummaryPokokTab.OnFragmentInteractionListener, SummarySKBTab.OnFragmentInteractionListener, MasterBlockHdrcFragment.FragmentListBlockListener{
    public static String MASTERBLOCKHDRC= "MASTERBLOCKHDRC";
    Toolbar mToolbar;
    FrameLayout frameLayout;
    public static FrameLayout frameForm;
    static Dialog d ;
    int year = Calendar.getInstance().get(Calendar.YEAR);
    int month = Calendar.getInstance().get(Calendar.MONTH);
    ViewPager viewPager;
    TabLayout tabs;
    EditText et_block;
    EditText et_periode;
    EditText et_year;
    String phase;
    DatabaseHandler database;
    BlockHdrc blkDetail;
    SectionsPagerAdapter sectionsPagerAdapter;

    String companyCode;
    String estate;
    String division;
    String nik;
    private static Fragment fragment;
    private static FragmentTransaction fragmentTransaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        database = new DatabaseHandler(getApplicationContext());
        frameForm = (findViewById(R.id.frameForm));
        FloatingActionButton fab = findViewById(R.id.fab);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPager = findViewById(R.id.view_pager);
        tabs = findViewById(R.id.tab_layout);
        frameLayout = findViewById(R.id.fragment_container);
        setSupportActionBar(mToolbar);
        initToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SummaryActivity.this.finish();
                new PreferenceManager(SummaryActivity.this, Constants.shared_name).setSSYear(null);
                new PreferenceManager(SummaryActivity.this,Constants.shared_name).setSSPeriode(null);
                new PreferenceManager(SummaryActivity.this,Constants.shared_name).setSSBlock(null);
                new PreferenceManager(SummaryActivity.this,Constants.shared_name).setSSPhase(null);
                onBackPressed();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        et_block = (findViewById(R.id.et_block));
        et_periode= (findViewById(R.id.et_periode));
        et_year = (findViewById(R.id.et_year));
        et_year.setClickable(true);
        et_block.setClickable(true);
        et_periode.setClickable(true);
        et_periode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMonthDialog();
            }
        });
        et_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            showYearDialog();
            }
        });
        et_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toMasterBlockHDRC();
            }
        });
        Bundle bundle = new Bundle();
        bundle.putString("param1", null);
        bundle.putString("param2",null);
        bundle.putString("param3",  null);
        bundle.putString("param4",  null);
        sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager(),bundle);
        viewPager.setAdapter(sectionsPagerAdapter);
        tabs.setupWithViewPager(viewPager);
        tabs.setTabTextColors(getResources().getColor(R.color.Milk_White),getResources().getColor(R.color.Blue_Hosta));

        Button btnCOnfirmOK = (findViewById(R.id.btn_confirm_ok));
        btnCOnfirmOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String paramsBlock = et_block.getText().toString();
                String paramsPeriode = et_periode.getText().toString();
                String paramsYear = et_year.getText().toString();
                new PreferenceManager(SummaryActivity.this, Constants.shared_name).setSSYear(paramsYear);
                new PreferenceManager(SummaryActivity.this,Constants.shared_name).setSSPeriode(paramsPeriode);
                new PreferenceManager(SummaryActivity.this,Constants.shared_name).setSSBlock(paramsBlock);

                if(new PreferenceManager(SummaryActivity.this, Constants.shared_name).getSSBlock()!=null){
                    database.openTransaction();
                    blkDetail = (BlockHdrc) database.getDataFirst(false, BlockHdrc.TABLE_NAME, null,
                            BlockHdrc.XML_BLOCK + "=? ",
                            new String [] {paramsBlock},
                            BlockHdrc.XML_BLOCK, null, BlockHdrc.XML_BLOCK, null);
                    database.closeTransaction();
                    if(blkDetail!=null){

                        if(blkDetail.getProjectDefinition().substring(0,1).equalsIgnoreCase("M")){
                            phase = "Mature";
                            new PreferenceManager(SummaryActivity.this,Constants.shared_name).setSSPhase(phase);
                            Bundle bundle = new Bundle();
                            bundle.putString("param1",paramsBlock);
                            bundle.putString("param2",paramsPeriode);
                            bundle.putString("param3",paramsYear);
                            bundle.putString("param4",phase);
                            sectionsPagerAdapter = new SectionsPagerAdapter(getApplicationContext(), getSupportFragmentManager(),bundle);
                            viewPager.setAdapter(sectionsPagerAdapter);
                            tabs.setupWithViewPager(viewPager);
                            tabs.setTabTextColors(getResources().getColor(R.color.Milk_White),getResources().getColor(R.color.Blue_Hosta));

                        }else{
                            phase = "Immature";
                            new PreferenceManager(SummaryActivity.this,Constants.shared_name).setSSPhase(phase);
                            Bundle bundle = new Bundle();
                            bundle.putString("param1",paramsBlock);
                            bundle.putString("param2",paramsPeriode);
                            bundle.putString("param3",paramsYear);
                            bundle.putString("param4",phase);
                            sectionsPagerAdapter = new SectionsPagerAdapter(getApplicationContext(), getSupportFragmentManager(),bundle);
                            viewPager.setAdapter(sectionsPagerAdapter);
                            tabs.setupWithViewPager(viewPager);
                            tabs.setTabTextColors(getResources().getColor(R.color.Milk_White),getResources().getColor(R.color.Blue_Hosta));
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"Data Tidak Ada",Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }

    private void initToolbar() {
        if(ViewToolbar.CheckLogin()){
            companyCode = ViewToolbar.getCompanyCode();
            estate = ViewToolbar.getEstate();
            division = ViewToolbar.getDivision();
            nik = ViewToolbar.getNik();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public void showYearDialog()
    {

        final Dialog d = new Dialog(SummaryActivity.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.year_dialog);
        Button set = (Button) d.findViewById(R.id.btnOk);
        Button cancel = (Button) d.findViewById(R.id.btnCancel);
        TextView year_text=(TextView)d.findViewById(R.id.year_text);
        year_text.setText("Year Picker");
        final NumberPicker nopicker = (NumberPicker) d.findViewById(R.id.picker_year);

        nopicker.setMaxValue(year+50);
        nopicker.setMinValue(year-50);
        nopicker.setWrapSelectorWheel(false);
        nopicker.setValue(year);
        nopicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        set.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                et_year.setText(String.valueOf(nopicker.getValue()));
                d.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    public void showMonthDialog()
    {

        final Dialog d = new Dialog(SummaryActivity.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.month_dialog);
        Button btnOK = (Button) d.findViewById(R.id.btnOk);
        Button btnCancel = (Button) d.findViewById(R.id.btnCancel);
        TextView year_text=(TextView)d.findViewById(R.id.year_text);
        year_text.setText("Periode Picker");
        final NumberPicker nopicker = (NumberPicker) d.findViewById(R.id.month_picker);
        nopicker.setMinValue(1);
        nopicker.setMaxValue(12);
        nopicker.setValue(month);

        nopicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        btnOK.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                String MonthString;
                int bulan = nopicker.getValue();
                if(bulan<10){
                    MonthString = "0"+String.valueOf(nopicker.getValue());
                    et_periode.setText(MonthString);
                }
                if(bulan>=10){
                    MonthString = String.valueOf(nopicker.getValue());

                    et_periode.setText(MonthString);
                }
                d.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    public void toMasterBlockHDRC(){
        viewPager.setVisibility(View.GONE);
        tabs.setVisibility(View.GONE);
        frameLayout.setVisibility(View.VISIBLE);
        FragmentTransaction fragmentTransaction = null;
        Bundle args = new Bundle();
        args.putString(BlockHdrc.XML_COMPANY_CODE, companyCode);
        args.putString(BlockHdrc.XML_ESTATE, estate);
        args.putString(BlockHdrc.XML_DIVISION, division);
        args.putBoolean(Constants.SEARCH, true);
        nextFragment(MasterBlockHdrcFragment.class,
                args,
                R.id.fragment_container,MASTERBLOCKHDRC);
    }
    public void nextFragment(      Class classFragment,
                                   Bundle args,
                                   Integer layout,
                                   String Tag){
        try {
            fragment = (Fragment) classFragment.newInstance();
            fragment.setArguments(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
        fragmentTransaction=getSupportFragmentManager().beginTransaction();
        //fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        fragmentTransaction.replace(layout, fragment, Tag);
        fragmentTransaction.addToBackStack(Tag);
        fragmentTransaction.commit();
    }
    @Override
    public void onInputBlockList(String hdrc) {
        et_block.setText(hdrc);
        frameLayout.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
        tabs.setVisibility(View.VISIBLE);
    }
}