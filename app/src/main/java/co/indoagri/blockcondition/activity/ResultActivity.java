package co.indoagri.blockcondition.activity;

import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.AdapterResult;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.handler.BroadcastConnectorHandler;
import co.indoagri.blockcondition.listener.DialogConfirmationListener;
import co.indoagri.blockcondition.listener.DialogNotificationListener;
import co.indoagri.blockcondition.model.Data.ResultModel;
import co.indoagri.blockcondition.model.Data.tblM_AccountingPeriod;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.Users.UserLogin;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.widget.ToastMessage;

public class ResultActivity extends BaseActivity implements View.OnClickListener,
        BroadcastConnectorHandler.ConnectivityReceiverListener,BaseActivity.LogOutListener,
         DialogNotificationListener, DialogConfirmationListener, AdapterView.OnItemClickListener  {
    String PHASE = null;
    String kondisi = null;
    int LEVEL;
    ToastMessage toastMessage;
    ConstraintLayout constraintLayout;
    TextView txtTitle;
    Button btnBack,btnNext;
    DatabaseHandler database;
    private GridView lsvResult;
    private List<ResultModel> list = new ArrayList<ResultModel>();
    private AdapterResult adapterResult;
    private GetDataAsyncTask getDataAsync;
    public static String SESS_HZDATE = null;
    public static String SESS_HZYEAR = null;
    public static String SESS_HPERIOD = null;
    public static String SESS_PERIOD_TODATE = null;
    public static String SESS_PERIOD_FROMDATE = null;
    public static int M_RESULTDETAIL = 123;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        animOnStart();
        registerBaseActivityReceiver();
        database = new DatabaseHandler(getApplicationContext());
        setContentView(R.layout.activity_result_item);
        toastMessage = new ToastMessage(getApplicationContext());
        constraintLayout = (ConstraintLayout)findViewById(R.id.constraint);
        txtTitle = (TextView)findViewById(R.id.txtTitleHeader);
        btnBack = (Button)findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
        btnNext = (Button)findViewById(R.id.btnNext);
        btnNext.setVisibility(View.GONE);
        lsvResult = (GridView) findViewById(R.id.lsvResult);
        setDefault();
        setData();
        SESS_HPERIOD =  new PreferenceManager(getApplicationContext(),Constants.shared_name).getPeriod();
        SESS_HZYEAR=  new PreferenceManager(getApplicationContext(),Constants.shared_name).getYear();
        initToolbar();
    }

    private void initToolbar() {
        if(ViewToolbar.CheckLogin()){
            TextView txtBA = (TextView)findViewById(R.id.txtBA);
            txtBA.setText( ViewToolbar.getEstate());
            TextView txtDivision = (TextView)findViewById(R.id.txtUserDivision);
            txtDivision.setText(ViewToolbar.getDivisions());
        }
    }
    private void setDefault(){
        btnNext.setVisibility(View.GONE);
        txtTitle.setText("RESULT");
    }

    private void setData(){
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            PHASE = bundle.getString("PHASE");
            LEVEL = bundle.getInt("LEVEL");
            kondisi = bundle.getString("KONDISI");
            init();
        }
    }
    void init(){
        lsvResult.setOnItemClickListener(this);
        adapterResult = new AdapterResult(getApplicationContext(), list, R.layout.item_result);
        lsvResult.setAdapter(adapterResult);
        lsvResult.setScrollingCacheEnabled(false);
        getData();
        setPokokResult();

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                animOnFinish();
                finish();
                break;

            default:
                break;
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.e("ONSTOP", "OnStop()");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.e("ONDESTROY", "OnDestory()");
        unRegisterBaseActivityReceiver();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        ResultModel resultModel = (ResultModel) adapterResult.getItem(pos);
        if(LEVEL==Constants.MainResult_POKOK){
            Intent intentresult = new Intent(ResultActivity.this, ResultDetailActivity.class);
            intentresult.putExtra("PHASE",PHASE);
            intentresult.putExtra("LEVEL",LEVEL);
            intentresult.putExtra("KONDISI",kondisi);
            intentresult.putExtra("BLOCK",resultModel.getBlock());
            intentresult.putExtra("CONDITION",resultModel.getCondition());
            startActivityForResult(intentresult, M_RESULTDETAIL);
        }
        if(LEVEL==Constants.MainResult_SKB){
        //    toastMessage.shortMessage("CLICK POKOK" +resultModel.getBlock());
        }
        if(LEVEL==Constants.MainResult_Block){

        }
    }
    private void getData(){

        getDataAsync = new GetDataAsyncTask();
        getDataAsync.execute();
    }
    private class GetDataAsyncTask extends AsyncTask<Void, List<ResultModel>, List<ResultModel>> {
        private DialogProgress dialogProgress;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(!isFinishing()){
                dialogProgress = new DialogProgress(ResultActivity.this,getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }
        @Override
        protected List<ResultModel> doInBackground(Void... voids) {
            database.openTransaction();
            List<ResultModel> listTemp = new ArrayList<ResultModel>();
            List<Object> listObject;
            try{
                UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
                String[] a = new String[5];
                String query = null;
                a[0] = "9999-12-31";
                a[1] = userLogin.getDivision();
                a[2] = SESS_HZYEAR;
                a[3] = SESS_HPERIOD;
                a[4] = "0";

                if(PHASE.equalsIgnoreCase("MATURE")){
                    query = "SELECT b.Block,"+kondisi + " as Condition FROM tblT_BlockCondition a " +
                            "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi=? AND b.project LIKE 'M%'" +
                            "WHERE a.zyear=? AND  a.period=? AND a.TransLevel=? AND a."+kondisi+" > 0 AND a."+kondisi+ " != 0.0";
                }
                if(PHASE.equalsIgnoreCase("IMMATURE")){
                    query = "SELECT b.Block,"+kondisi + " as Condition FROM tblT_BlockCondition a " +
                            "INNER JOIN BLOCK_HDRC b ON a.block=b.block AND b.validto=? AND b.divisi=? AND b.project LIKE 'T%'" +
                            "WHERE a.zyear=? AND  a.period=? AND a.TransLevel=? AND a."+kondisi+" > 0 AND a."+kondisi+ " != 0.0";
                }
                    listObject = database.getListDataRawQuery(query,"RESULT",a);
                if(listObject.size() > 0){
                    for(int i = 0; i < listObject.size(); i++){
                        ResultModel resultModel = (ResultModel) listObject.get(i);

                        listTemp.add(resultModel);
                    }
                }
            }catch(SQLiteException e){
                e.printStackTrace();
                database.closeTransaction();
            }finally{
                database.closeTransaction();
            }
            return listTemp;

        }

        @Override
        protected void onPostExecute(List<ResultModel> listTemp) {
            super.onPostExecute(listTemp);
            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }
            for(int i = 0; i < listTemp.size(); i++){
                ResultModel resultModel = (ResultModel) listTemp.get(i);
                adapterResult.addData(resultModel);

            }
        }

    }

    void setPokokResult(){
        txtTitle.setText(PHASE+" - "+kondisi);
    }

    @Override
    public void doLogout() {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onConfirmOK(Object object, int id) {

    }

    @Override
    public void onConfirmCancel(Object object, int id) {

    }

    @Override
    public void onOK(boolean is_finish) {

    }
}
