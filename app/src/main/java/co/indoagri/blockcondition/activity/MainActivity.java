package co.indoagri.blockcondition.activity;

import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.dialog.DialogConfirmation;
import co.indoagri.blockcondition.fragment.MainFragment;
import co.indoagri.blockcondition.fragment.MonthYearPickerDialog;
import co.indoagri.blockcondition.fragment.NotificationFragment;
import co.indoagri.blockcondition.fragment.ProfileFragment;
import co.indoagri.blockcondition.handler.LogOutHandler;
import co.indoagri.blockcondition.listener.DialogConfirmationListener;
import co.indoagri.blockcondition.model.Data.tblM_AccountingPeriod;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.model.Data.tblT_BlockConditionScore;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.LogModel;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.utils.MarshMallowPermission;
import co.indoagri.blockcondition.widget.CustomSnackBar;
import co.indoagri.blockcondition.widget.ToastMessage;

import static co.indoagri.blockcondition.routines.Constants.PARAM1;

public class MainActivity extends BaseActivity implements
        View.OnClickListener,
        MainFragment.FragmentHomeListener,
        ProfileFragment.FragmentProfileListener,
        NotificationFragment.FragmentNotificationListener{
    boolean doubleBackToExitPressedOnce = false;
    ImageView shutDown;
    boolean onUserInteraction = false;
    ToastMessage toastMessage;
    View rootView;
    public static int M_SETTING = 99;
    public static int M_EXPORT = 88;
    public static int M_ENTRY = 77;
    public static int M_BLOCK = 50;
    public static int M_HISTORY = 66;
    public static int M_SUMMARY = 55;
    public static int M_LOG = 44;
    public static int M_RESULT = 43;
    public static String MAINFRAGMENT= "MAINFRAG";
    public static String N_PROFILE = "PROFILE";
    public static String N_HOME= "HOME";
    public static String N_NOTIFICATION= "NOTIFICATION";
    DatabaseHandler database = new DatabaseHandler(MainActivity.this);
    Bundle bundle;
    int ValueBundle;
    BottomNavigationView navigation;
    public static String SESS_HZDATE = null;
    public static String SESS_HZYEAR = null;
    public static String SESS_HPERIOD = null;
    public static String SESS_PERIOD_TODATE = null;
    public static String SESS_PERIOD_FROMDATE = null;

    public static String LEVELHOME= "LEVELHOME";
    public static String LEVELNOTIF= "LEVELNOTIF";
    public static String LEVELPROFILE= "LEVELPROFILE";
    MarshMallowPermission marshMallowPermission;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle= getIntent().getExtras();
        animOnStart();
        marshMallowPermission = new MarshMallowPermission(MainActivity.this);
        registerBaseActivityReceiver();
        setContentView(R.layout.activity_main);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.MainLayout);
        rootView = relativeLayout;
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        toastMessage = new ToastMessage(getApplicationContext());
        shutDown = (ImageView) findViewById(R.id.imgActionBarLogout);
        shutDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDialog();
            }
        });
       // checkConnection();
        SESS_HZYEAR = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_YEAR_ONLY);
        SESS_HZDATE = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
        SESS_HPERIOD = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_MONTH_ONLY);
        Delete40DaysData();
    }


    private void initToolbar() {
        if(ViewToolbar.CheckLogin()){
            TextView txtDivision = (TextView)findViewById(R.id.txtUserDivision);
            txtDivision.setText(ViewToolbar.getDivisions());
            TextView txtBA = (TextView)findViewById(R.id.txtBA);
            txtBA.setText( ViewToolbar.getEstate());
        }
    }

//    void ResetDBCondition(){
//        int deleted;
//
//        database.openTransaction();
//        deleted = database.deleteData(tblT_BlockCondition.TABLE_NAME,null, null);
//        deleted = database.deleteData(tblT_BlockConditionScore.TABLE_NAME,null,null);
//        database.commitTransaction();
//        database.closeTransaction();
//        if(deleted>0){
//            toastMessage.shortMessage("Success");
//        }
//    }

    void Delete40DaysData(){
        String sql = "DELETE FROM "+ tblT_BlockCondition.TABLE_NAME+" WHERE "+ tblT_BlockCondition.XML_ZDate+" <= date('now','-60 day')";
        database.openTransaction();
        try {
            database.ResetData(sql);
        } catch (
                SQLiteException e) {
            e.printStackTrace();
            database.closeTransaction();
        } finally {
            database.commitTransaction();
            database.closeTransaction();
        }
        GetData();
    }
    void GetData() {
        database.openTransaction();
        String[] a = new String[2];
        a[0] = SESS_HZYEAR;
        a[1] = SESS_HZDATE;
        String query = "SELECT Periode, ClosingDate, Zyear FROM tblM_AccountingPeriod " +
                "WHERE  ZYear = ? AND  ClosingDate  >= ? " +
                "ORDER BY ClosingDate ASC LIMIT 1";
        tblM_AccountingPeriod dtsource1 = (tblM_AccountingPeriod) database.getDataFirstRaw(query,tblM_AccountingPeriod.TABLE_NAME,a);
        database.closeTransaction();
        if(dtsource1!= null){
          //  SESS_PERIOD = dtsource1.getAcc_Period();
            new PreferenceManager(getApplicationContext(),Constants.shared_name).setPeriod(dtsource1.getAcc_Period());
            new PreferenceManager(getApplicationContext(),Constants.shared_name).setYear(dtsource1.getAcc_ZYear());
        }
        else{
            new PreferenceManager(getApplicationContext(),Constants.shared_name).setPeriod("0");
            new PreferenceManager(getApplicationContext(),Constants.shared_name).setYear("0");
            ToastMessage toastMessage = new ToastMessage(getApplicationContext());
            toastMessage.shortMessage("Period Tidak Ditemukan");
        }
        setDefault();
        initToolbar();
    }

    private void setDefault() {
        if(bundle!=null){
            ValueBundle = bundle.getInt(Constants.CONSTBUNDLE);
            Fragment fragment = null;
            Class fragmentClass;
            FragmentManager fragmentManager;
            switch(ValueBundle) {
                case 0:
                    toHome(null);
                    break;
                case 1:
                    toRootMenu(null);
                    Toast.makeText(this, "Menu", Toast.LENGTH_SHORT).show();
                    break;
                case 2:
                    toNotification(null);
                    break;
                case 3:
                    toProfile(null);
                    break;
                default:
                    toHome(null);
            }
        }
        else{
            Class fragmentClass;
            Bundle args = new Bundle();
            args.putInt(PARAM1, 0);
            fragmentClass = MainFragment.class;
            setDefaultFragment(fragmentClass,
                    args,
                    R.id.fragment_container,
                    LEVELHOME);
        }
    }



    public void toNotification(String newText){
        Fragment fragment = null;
        Class fragmentClass;
        FragmentManager fragmentManager;
        navigation.getMenu().findItem(R.id.navigation_notifications).setChecked(true);
        fragmentClass = NotificationFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment)
                .commit();
    }

    public void toHome(String newText){
        navigation.getMenu().findItem(R.id.navigation_home).setChecked(true);
        ExistFragment(MainFragment.class,
                null,
                R.id.fragment_container,LEVELHOME);
    }
    public void toRootMenu(String newText){
        animOnFinish();
        finish();
    }
    public void toProfile(String newText){
        Fragment fragment = null;
        Class fragmentClass;
        FragmentManager fragmentManager;
        navigation.getMenu().findItem(R.id.navigation_profile).setChecked(true);
        fragmentClass = ProfileFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment)
                .commit();
    }
//    private void TestingInputLog() {
//        String setUser = "UserTest";
//        String createdDate = "Date";
//        String Description = "Desc";
//        String Reference = "Ref";
//        int rowid = 1;
//        try{
//            database.openTransaction();
//            database.setData(new LogModel(1,setUser,createdDate,Description,Reference));
//            database.commitTransaction();
//        }catch(SQLiteException e){
//            e.printStackTrace();
//        }finally{
//            database.closeTransaction();
//        }
//
//}


    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.e("ONDESTROY", "OnDestory()");
        unRegisterBaseActivityReceiver();
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.e("ONSTOP", "OnStop()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("ONPAUSE", "onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        //setDefault();
        Log.e("ONRESUME", "onResume()");
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Class fragmentClass;
                    Bundle args = new Bundle();
                    args.putInt(PARAM1, 1);
                    fragmentClass = MainFragment.class;
                    setDefaultFragment(fragmentClass,
                            args,
                            R.id.fragment_container,
                            LEVELHOME);
                    break;
                case R.id.navigation_MenuRoot:
                    animOnFinish();
                    finish();
                    break;
                case R.id.navigation_notifications:
                    fragment = new NotificationFragment();
                    break;
                case R.id.navigation_profile:
                    fragment = new ProfileFragment();
                    break;
            }

            return loadFragment(fragment);
        }
    };


    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        Fragment fragment = null;
        Class fragmentClass;
        FragmentManager fragmentManager;
        if (!(currentFragment instanceof MainFragment)) {
            navigation.getMenu().findItem(R.id.navigation_home).setChecked(true);
            fragmentClass = MainFragment.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment)
                    .commit();
        } else {

            if (getFragmentManagerAccount() > 1) {
                // Log.i("Total",String.valueOf(fragmentManager.getBackStackEntryCount()));
                Log.i("MainActivity", "popping backstack");
                baseFragmentManager.popBackStack();
            } else {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                animOnFinish();
                finish();

            }

//        if (doubleBackToExitPressedOnce) {
//            if (getFragmentManagerAccount() > 1) {
//                // Log.i("Total",String.valueOf(fragmentManager.getBackStackEntryCount()));
//                Log.i("MainActivity", "popping backstack");
//                baseFragmentManager.popBackStack();
//            } else {
//                Intent intent = new Intent();
//                setResult(RESULT_OK, intent);
//                animOnFinish();
//                finish();
//
//            }
//
//        }
//        this.doubleBackToExitPressedOnce = true;
//        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                doubleBackToExitPressedOnce = false;
//            }
//        }, 2000);
    }
    }

    void DisplayDialog(){
        new DialogConfirmation(MainActivity.this, getResources().getString(R.string.logout),
                getResources().getString(R.string.logut_message), null, new DialogConfirmationListener() {
            @Override
            public void onConfirmOK(Object object, int id) {
                LogOutHandler logOutHandler = new LogOutHandler(getApplicationContext(),MainActivity.this);
                logOutHandler.setOnEventListener(new LogOutHandler.LogOutListener() {
                    @Override
                    public void onLogoutInterface(String string, boolean status) {
                        if(status==true){
                            closeAllActivities();
                            startActivity(new Intent(MainActivity.this, AuthActivity.class));
                            animOnFinish();
                        }
                        else{
                            ToastMessage toastMessage = new ToastMessage(getApplicationContext());
                            toastMessage.shortMessage("ADA MASALAH DENGAN FUNGSI");
                        }
                    }
                });
                logOutHandler.Keluar();
            }

            @Override
            public void onConfirmCancel(Object object, int id) {
               /* ToastMessage toastMessage = new ToastMessage(getApplicationContext());
                toastMessage.shortMessage("ADA MASALAH DENGAN FUNGSI");*/
            }
        }).show();
    }

    private void showSnack(boolean isConnected) {
        if (isConnected) {
            CustomSnackBar customSnackbar = CustomSnackBar.make((ViewGroup)rootView.getParent(),8000);
            customSnackbar.setText("Connected");
            customSnackbar.setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onStart();
                }
            });
            customSnackbar.show();
        } else {
            CustomSnackBar customSnackbar = CustomSnackBar.make((ViewGroup)rootView.getParent(),8000);
            customSnackbar.setText("No network connection!");
            customSnackbar.setAction("Retry", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(),"Not Connected",Toast.LENGTH_SHORT).show();
                }
            });
            customSnackbar.show();
        }
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onSendClickHomeFragment(String input, int intent, String kondisi) {
        if(input.equalsIgnoreCase(getString(R.string.entry_condition))){
            Intent intentsetting = new Intent(MainActivity.this, BlockActivity.class);
            startActivityForResult(intentsetting, M_ENTRY);
        }
        if(input.equalsIgnoreCase("header")){
            Intent intentblock = new Intent(MainActivity.this, BlockActivity.class);
            intentblock.putExtra("SEND",intent);
            startActivityForResult(intentblock, M_BLOCK);
        }
        if(input.equalsIgnoreCase("DATE")){
         /*   ToastMessage toastMessage = new ToastMessage(getApplicationContext());
            toastMessage.shortMessage(input);*/


        }
        if(input.equalsIgnoreCase(getString(R.string.setting_condition))){
          /*  ToastMessage toastMessage = new ToastMessage(getApplicationContext());
            toastMessage.shortMessage(input);*/
            Intent intentsetting = new Intent(MainActivity.this, SettingsActivity.class);
            startActivityForResult(intentsetting, M_SETTING);
        }

        if(input.equalsIgnoreCase(getString(R.string.setup_condition))){
          /*  ToastMessage toastMessage = new ToastMessage(getApplicationContext());
            toastMessage.shortMessage(input);*/
            Intent intentsetting = new Intent(MainActivity.this, SetupActivity.class);
            startActivityForResult(intentsetting, M_SETTING);
        }

        if(input.equalsIgnoreCase(getString(R.string.export_condition))){
            if (!marshMallowPermission.checkPermissionForExternalStorageRead()) {
                marshMallowPermission.requestPermissionForExternalStorageRead(MarshMallowPermission.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_BY_LOAD_PROFILE_READ);
            }else {
                Intent intentsetting = new Intent(MainActivity.this, ExportImportActivity.class);
                startActivityForResult(intentsetting, M_EXPORT);
            }
            }
        if(input.equalsIgnoreCase(getString(R.string.history_condition))){
            Intent intentsetting = new Intent(MainActivity.this, HistoryActivity.class);
            startActivityForResult(intentsetting, M_HISTORY);
        }
        if(input.equalsIgnoreCase(getString(R.string.summary_condition))){
            Intent intentsetting = new Intent(MainActivity.this, SummaryActivity.class);
            startActivityForResult(intentsetting, M_SUMMARY);
        }
        if(input.equalsIgnoreCase("MATURE")){
            Intent intentresult = new Intent(MainActivity.this, ResultActivity.class);
            intentresult.putExtra("PHASE",input);
            intentresult.putExtra("LEVEL",intent);
            intentresult.putExtra("KONDISI",kondisi);
            startActivityForResult(intentresult, M_RESULT);
        }
        if(input.equalsIgnoreCase("IMMATURE")){
            Intent intentresult = new Intent(MainActivity.this, ResultActivity.class);
            intentresult.putExtra("PHASE",input);
            intentresult.putExtra("LEVEL",intent);
            intentresult.putExtra("KONDISI",kondisi);
            startActivityForResult(intentresult, M_RESULT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check that it is the SecondActivity with an OK result
        if (requestCode == M_SETTING) {
            if(resultCode == RESULT_OK){
     /*           ToastMessage toastMessage = new ToastMessage(getApplicationContext());
                toastMessage.shortMessage("TESTING FROM SETTING");*/
            }
        }
        if (requestCode == M_ENTRY) {
            if(resultCode == RESULT_OK){
               /* ToastMessage toastMessage = new ToastMessage(getApplicationContext());
                toastMessage.shortMessage("TESTING FROM ENTRY");*/
            }
        }
        if (requestCode == M_EXPORT) {
            if(resultCode == RESULT_OK){/*
                ToastMessage toastMessage = new ToastMessage(getApplicationContext());
                toastMessage.shortMessage("TESTING FROM EXPORT IMPORT");*/
            }
        }
        if (requestCode == M_SUMMARY) {
            if(resultCode == RESULT_OK){/*
                ToastMessage toastMessage = new ToastMessage(getApplicationContext());
                toastMessage.shortMessage("TESTING FROM SUMMARY");*/
            }
        }
        if (requestCode == M_HISTORY) {
            if(resultCode == RESULT_OK){/*
                ToastMessage toastMessage = new ToastMessage(getApplicationContext());
                toastMessage.shortMessage("TESTING FROM HISTORY");*/
            }
        }
        if (requestCode == M_LOG) {
            if(resultCode == RESULT_OK){/*
                ToastMessage toastMessage = new ToastMessage(getApplicationContext());
                toastMessage.shortMessage("TESTING FROM LOG");*/
            }
        }
    }

    @Override
    public void onInterfaceProfile(String input) {
        if(input.equalsIgnoreCase(getResources().getString(R.string.logout))){
            DisplayDialog();
        }
    }

    @Override
    public void onInterfaceNotification(String input) {

    }


}
