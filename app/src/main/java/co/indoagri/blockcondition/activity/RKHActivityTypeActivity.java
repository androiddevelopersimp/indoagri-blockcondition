package co.indoagri.blockcondition.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.RKHActivityTypeAdapter;
import co.indoagri.blockcondition.adapter.RKHGangAdapter;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.model.RKH.RKH_ACTIVITY_TYPE;
import co.indoagri.blockcondition.routines.Constants;

public class RKHActivityTypeActivity extends AppCompatActivity implements RKHActivityTypeAdapter.ItemListener, RKHActivityTypeAdapter.ItemChooseListener, SearchView.OnQueryTextListener {
    private SearchView searchView;
    Toolbar toolbar;
    TextView mTextToolbar,TxtsaveToolbar;
    Bundle bundle;
    ArrayList<RKH_ACTIVITY_TYPE> rkh_activity_types = new ArrayList<RKH_ACTIVITY_TYPE>();
    RecyclerView recyclerView;
    RKHActivityTypeAdapter adapter;
    String companyCode;
    String division;
    String gang;
    String estate;
    GetDataAsyncTask getDataAsync;
    String InputType;
    String InputTypeName;
    public static boolean BtnSaveVisible = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.rkhactivitytype_layout);
        setUpToolbar();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        searchView = findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(this);
        initType();
    }

    void setUpToolbar() {
        bundle= getIntent().getExtras();
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        TxtsaveToolbar = (findViewById(R.id.txt_SaveToolbar));
        TxtsaveToolbar.setVisibility(View.GONE);
        TxtsaveToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveDataAndBack();
            }
        });
        mTextToolbar.setText(getResources().getString(R.string.alihkan_mandor));
        if(bundle!=null) {
            String Title  = bundle.getString("TOOLBARTITLE");
            mTextToolbar.setText(Title);
        }else{
            mTextToolbar.setText(getResources().getString(R.string.alihkan_mandor));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    void initType(){
        new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputtype(null);
        companyCode = ViewProfile.getCompanyCode();
        estate = ViewProfile.getEstate();
        division = ViewProfile.getDivisions();
        gang = ViewProfile.getGang();
        adapter = new RKHActivityTypeAdapter(this, rkh_activity_types,this,this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        getData();
    }
    private void getData(){
        getDataAsync = new GetDataAsyncTask(false, RKH_ACTIVITY_TYPE.TABLE_NAME, null,
                null,
                null,
                RKH_ACTIVITY_TYPE.XML_ACTTYPE, null, RKH_ACTIVITY_TYPE.XML_ACTTYPE, null);
        getDataAsync.execute();
    }

    @Override
    public void onItemClick(RKH_ACTIVITY_TYPE item, int position) {

    }

    @Override
    public void onItemChoose(RKH_ACTIVITY_TYPE item, int position) {
        BtnSaveVisible = true;
        InitialbtnSave();
        InputType = item.getACTTYPE();
        InputTypeName =item.getNAME();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        BtnSaveVisible = false;
        InitialbtnSave();
        adapter.getFilter().filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        BtnSaveVisible = false;
        InitialbtnSave();
        adapter.getFilter().filter(newText);
        return false;
    }


    private class GetDataAsyncTask extends AsyncTask<Void, ArrayList<RKH_ACTIVITY_TYPE>, ArrayList<RKH_ACTIVITY_TYPE>> implements SearchView.OnQueryTextListener {
        boolean distinct;
        String tableName;
        String [] columns;
        String whereClause;
        String [] whereArgs;
        String groupBy;
        String having;
        String orderBy;
        String limit;
        public GetDataAsyncTask(boolean distinct, String tableName, String [] columns, String whereClause, String [] whereParams,
                                String groupBy, String having, String orderBy, String limit){
            this.distinct = distinct;
            this.tableName = tableName;
            this.columns = columns;
            this.whereClause = whereClause;
            this.whereArgs = whereParams;
            this.groupBy = groupBy;
            this.having = having;
            this.orderBy = orderBy;
            this.limit = limit;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected ArrayList<RKH_ACTIVITY_TYPE> doInBackground(Void... voids) {
            DatabaseHandler database = new DatabaseHandler(RKHActivityTypeActivity.this);
            ArrayList<RKH_ACTIVITY_TYPE> listTemp = new ArrayList<RKH_ACTIVITY_TYPE>();
            List<Object> listObject;
            database.openTransaction();
            listObject = database.getListData(distinct, tableName, columns, whereClause, whereArgs, groupBy, having, orderBy, limit);
            database.closeTransaction();
            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    RKH_ACTIVITY_TYPE empNew = (RKH_ACTIVITY_TYPE) listObject.get(i);
                    boolean foundFilter = false;
                    boolean foundCurrent = false;
                    if(rkh_activity_types.size() > 0){
                        for(int x = 0; x < rkh_activity_types.size(); x++){
                            RKH_ACTIVITY_TYPE empCur = (RKH_ACTIVITY_TYPE) rkh_activity_types.get(x);

                            if(empNew.getACTTYPE().equals(empCur.getACTTYPE())){
                                foundCurrent = true;
                                break;
                            }
                        }
                    }

                    if(!foundFilter && !foundCurrent){
                        listTemp.add(empNew);
                    }
                }
            }

            return listTemp;
        }

        @Override
        protected void onPostExecute(ArrayList<RKH_ACTIVITY_TYPE> lstTemp) {
            super.onPostExecute(lstTemp);
            for(int i = 0; i < lstTemp.size(); i++){
                RKH_ACTIVITY_TYPE rkh_activity_type = (RKH_ACTIVITY_TYPE) lstTemp.get(i);
                adapter.addData(rkh_activity_type);
            }

        }

        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        DisplayDialogSave();
    }

    void DisplayDialogSave(){
        if(InputType==null){
            InputType = null;
            Intent intent=new Intent();
            setResult(RESULT_OK, intent);
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }else if(InputType!=null){
            AlertDialog.Builder builder = new AlertDialog.Builder(RKHActivityTypeActivity.this);
            builder.setTitle("Apakah Akan menyimpan Data");
            builder.setMessage("Simpan Data ? ");
            // Set the alert dialog yes button click listener
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SaveDataAndBack();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    NotSaveDataAndBack();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    void NotSaveDataAndBack(){
        new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputtype(null);
        InputType = null;
        Intent intent=new Intent();
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    void SaveDataAndBack(){
        String insertType = InputType+";"+InputTypeName;
        new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputtype(insertType);
        Intent intent=new Intent();
        setResult(RESULT_OK, intent);
        InputType = null;
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    void InitialbtnSave(){
        if(BtnSaveVisible){
            TxtsaveToolbar.setVisibility(View.VISIBLE);
        }else{
            TxtsaveToolbar.setVisibility(View.GONE);
        }
    }


}
