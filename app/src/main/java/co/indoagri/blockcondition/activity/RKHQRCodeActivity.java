package co.indoagri.blockcondition.activity;

import android.content.ContentValues;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.model.RKH.RKH_HEADER;
import co.indoagri.blockcondition.model.RKH.RKH_ITEM;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;

public class RKHQRCodeActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView mTextToolbar;
    Bundle bundle;
    RKH_HEADER rkhHeader;
    String RKHDATE = "";
    String RkhID = "";
    String Method = null;
    private String QR_CODE_DELIMITER = ";";
    private String QR_CODE_DATA_DELIMITER = "|";
    private String QR_CODE_DATA_DELIMITER_CLERK = ",";
    String contentQr = "";
    String lastRKHNumber ="";
    ImageView imQrCode;
    DatabaseHandler database = new DatabaseHandler(RKHQRCodeActivity.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rkhqrcode_layout);
        imQrCode = (findViewById(R.id.imageQr));
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.95);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.80);
        bundle= getIntent().getExtras();
        if(bundle!=null) {
            rkhHeader = (RKH_HEADER) getIntent().getExtras().getParcelable("Data");
            database.openTransaction();
            List<Object> listObject2;
            listObject2 = database.getListData(false, RKH_ITEM.TABLE_NAME, null,
                    RKH_ITEM.XML_RKH_ID + "=? ",
                    new String[]{rkhHeader.getRKH_ID()},
                    null, null, null, null);
            database.closeTransaction();
            for (Object object : listObject2) {
                RKH_ITEM rkh_item = (RKH_ITEM) object;
                createContentQR(rkhHeader,rkh_item);
            }
            Method = bundle.getString("Method");
            RKHDATE = rkhHeader.getRKH_DATE();
            RkhID = rkhHeader.getRKH_ID();
        }
        getWindow().setLayout(width, height);
        setUpToolbar();
        setUI();
    }
    void setUI(){
        try {
            Bitmap bitmapQr = encodeAsBitmap(contentQr, BarcodeFormat.QR_CODE, 600, 600);
            imQrCode.setImageBitmap(bitmapQr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText("Show QRCode");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void createContentQR(RKH_HEADER rkh_header, RKH_ITEM rkh_item) {
        String currentString = rkh_item.getACTIVITY();
        String[] act = currentString.split(";");
        if (!lastRKHNumber.equals("")) {
            contentQr += QR_CODE_DELIMITER;
        }
        contentQr +=rkh_header.getRKH_DATE()+ QR_CODE_DELIMITER +rkh_header.getNIK_FOREMAN()+QR_CODE_DATA_DELIMITER_CLERK+rkh_header.getFOREMAN()+QR_CODE_DATA_DELIMITER_CLERK+rkh_header.getNIK_CLERK()+QR_CODE_DATA_DELIMITER_CLERK+rkh_header.getCLERK()+QR_CODE_DATA_DELIMITER_CLERK+rkh_item.getLINE()+QR_CODE_DELIMITER+act[0] + QR_CODE_DELIMITER + rkh_item.getBLOCK() + QR_CODE_DELIMITER
                + rkh_item.getTARGET_OUTPUT();
        lastRKHNumber = rkh_header.getRKH_ID();
    }
    Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        Log.e(ContentValues.TAG, "encodeAsBitmap: " + bitmap.toString());
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }


}