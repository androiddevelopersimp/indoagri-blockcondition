package co.indoagri.blockcondition.activity;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import java.io.IOException;

import co.indoagri.blockcondition.MyApps;
import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.handler.BroadcastConnectorHandler;
import co.indoagri.blockcondition.handler.FileEncryptionHandler;
import co.indoagri.blockcondition.model.Users.UserLogin;
import co.indoagri.blockcondition.widget.ToastMessage;

public class ExitActivity extends BaseActivity implements BaseActivity.LogOutListener,
        View.OnClickListener,
        BroadcastConnectorHandler.ConnectivityReceiverListener{
    Context context;
    boolean onUserInteraction = false;
    ToastMessage toastMessage;
    DatabaseHandler database = new DatabaseHandler(ExitActivity.this);
    public boolean statusLogout= false;
    BackupTask backupTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        animOnStart();
        registerBaseActivityReceiver();
        context = getApplicationContext();
        toastMessage = new ToastMessage(context);
        backupTask = new BackupTask();
        backupTask.execute();
        Keluar();
    }

    void Keluar() {
        if (isFinishing()) {
            if (statusLogout) {
    /*finishAndRemoveTask();
                closeAllActivities();
                startActivity(new Intent(ExitActivity.this, AuthActivity.class));
                animOnFinish();*/
                closeAllActivities();
                animOnFinish();
                toastMessage.shortMessage(">= 21");
            } else {
                /*finish();
                closeAllActivities();
                startActivity(new Intent(ExitActivity.this, AuthActivity.class));
                animOnFinish();*/
                closeAllActivities();
                animOnFinish();
                toastMessage.shortMessage("<=21");
            }
        }
        else{
            toastMessage.shortMessage(getResources().getString(R.string.error_device));
            }

    }

    private class BackupTask extends AsyncTask<Void, Void, Boolean>{

        DialogProgress dialogProgress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialogProgress = new DialogProgress(context, getResources().getString(R.string.wait));
            dialogProgress.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                return copyAppDbToDownloadFolder();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
            }

            if(result){
                toastMessage.shortMessage(getResources().getString(R.string.backup_successed));
            }else{
                toastMessage.shortMessage(getResources().getString(R.string.backup_failed));
            }

            try{
                database.openTransaction();
                database.deleteData(UserLogin.TABLE_NAME, null, null);
                database.commitTransaction();
                database.closeTransaction();
                statusLogout = true;
            }catch(SQLiteException e){
                e.printStackTrace();
                database.closeTransaction();
            }
        }
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.e("ONDESTROY", "OnDestory()");
        stopLogoutTimer();
        unRegisterBaseActivityReceiver();
    }
    @Override
    public void doLogout() {

        runOnUiThread(new Runnable() {
            public void run() {
                toastMessage.shortMessage("Interaction Logout");
                onUserInteraction = false;
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(!onUserInteraction){
            Log.e("INTERACTION", "User interacting with screen");
            onUserInteraction = true;
            startLogoutTimer(this, this);
        }
        else{

        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if(!onUserInteraction){
            Log.e("INTERACTION", "User interacting with screen");
            onUserInteraction = true;
            startLogoutTimer(this, this);
        }
        else{
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("ONSTOP", "OnStop()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("ONPAUSE", "onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApps.getInstance().setConnectivityListener(this);
        Log.e("ONRESUME", "onResume()");
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private boolean copyAppDbToDownloadFolder() throws IOException {
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
//
        if(mExternalStorageAvailable && mExternalStorageWriteable){
            boolean success = false;

            success = new FileEncryptionHandler(context).encrypt(FileEncryptionHandler.STORAGE_INTERNAL);
            success &= new FileEncryptionHandler(context).encrypt(FileEncryptionHandler.STORAGE_EXTERNAL);

            return success;
        }

        return false;
    }
}