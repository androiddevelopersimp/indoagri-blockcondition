package co.indoagri.blockcondition.activity.ui.main;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.model.Data.BlockHdrc;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.routines.Constants;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SummaryBlockTab.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SummaryBlockTab#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SummaryBlockTab extends Fragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    DatabaseHandler database;
    BlockHdrc blkDetail;
    String LastDate;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private String mParam4;

    // VIew//
    private RadioGroup rgpBlock1,rgpBlock2,rgpBlock3,rgpBlock4,rgpBlock5,rgpBlock6,rgpBlock7,rgpBlock8,rgpBlock9,rgpBlock10;
    private RadioButton rbnblockred1,rbnblockgreen1,rbnblockyellow1;
    private RadioButton rbnblockred2,rbnblockgreen2,rbnblockyellow2;
    private RadioButton rbnblockred3,rbnblockgreen3,rbnblockyellow3;
    private RadioButton rbnblockred4,rbnblockgreen4,rbnblockyellow4;
    private RadioButton rbnblockred5,rbnblockgreen5,rbnblockyellow5;
    private RadioButton rbnblockred6,rbnblockgreen6,rbnblockyellow6;
    private RadioButton rbnblockred7,rbnblockgreen7,rbnblockyellow7;
    private RadioButton rbnblockred8,rbnblockgreen8,rbnblockyellow8;
    private RadioButton rbnblockred9,rbnblockgreen9,rbnblockyellow9;
    private RadioButton rbnblockred10,rbnblockgreen10,rbnblockyellow10;
    private OnFragmentInteractionListener mListener;
    TextView txtCodeBlock,txtCodePhase;
    private EditText editRemark;
    View view;
    public SummaryBlockTab() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static SummaryBlockTab newInstance() {
        SummaryBlockTab fragment = new SummaryBlockTab();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = new DatabaseHandler(SummaryBlockTab.this.getContext());
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle!= null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4= getArguments().getString(ARG_PARAM4);
        }
        mParam1 = new PreferenceManager(getActivity(), Constants.shared_name).getSSYear();
        mParam2 = new PreferenceManager(getActivity(), Constants.shared_name).getSSPeriode();
        mParam3 = new PreferenceManager(getActivity(), Constants.shared_name).getSSBlock();
        mParam4 = new PreferenceManager(getActivity(), Constants.shared_name).getSSPhase();
        if(mParam1!=null){
            if(mParam4.equalsIgnoreCase("Mature")){
                view = inflater.inflate(R.layout.fragment_summary_block_mature, container, false);
            }else{
                view = inflater.inflate(R.layout.fragment_summary_block_immature, container, false);
            }
        }else{
            view = inflater.inflate(R.layout.fragment_summary_null, container, false);
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(mParam1!=null){
            txtCodeBlock = (TextView)view.findViewById(R.id.txtCodeBlock);
            txtCodePhase = (TextView)view.findViewById(R.id.txtCodePhase);
            rgpBlock1 = (RadioGroup) view.findViewById(R.id.rb_group1);
            rgpBlock2 = (RadioGroup) view.findViewById(R.id.rb_group2);
            rgpBlock3 = (RadioGroup) view.findViewById(R.id.rb_group3);
            rgpBlock4 = (RadioGroup) view.findViewById(R.id.rb_group4);
            rgpBlock5 = (RadioGroup) view.findViewById(R.id.rb_group5);
            rgpBlock6 = (RadioGroup) view.findViewById(R.id.rb_group6);
            rgpBlock7 = (RadioGroup) view.findViewById(R.id.rb_group7);
            rgpBlock8 = (RadioGroup) view.findViewById(R.id.rb_group8);
            rgpBlock9 = (RadioGroup) view.findViewById(R.id.rb_group9);
            rgpBlock10 = (RadioGroup) view.findViewById(R.id.rb_group10);
            rgpBlock1.setClickable(false);
            rgpBlock2.setClickable(false);
            rgpBlock3.setClickable(false);
            rgpBlock4.setClickable(false);
            rgpBlock5.setClickable(false);
            rgpBlock6.setClickable(false);
            rgpBlock7.setClickable(false);
            rgpBlock8.setClickable(false);
            rgpBlock9.setClickable(false);
            rgpBlock10.setClickable(false);
            rbnblockgreen1 = (RadioButton) view.findViewById(R.id.rb_green1);
            rbnblockgreen2 = (RadioButton) view.findViewById(R.id.rb_green2);
            rbnblockgreen3 = (RadioButton) view.findViewById(R.id.rb_green3);
            rbnblockgreen4 = (RadioButton) view.findViewById(R.id.rb_green4);
            rbnblockgreen5 = (RadioButton) view.findViewById(R.id.rb_green5);
            rbnblockgreen6 = (RadioButton) view.findViewById(R.id.rb_green6);
            rbnblockgreen7 = (RadioButton) view.findViewById(R.id.rb_green7);
            rbnblockgreen8 = (RadioButton) view.findViewById(R.id.rb_green8);
            rbnblockgreen9 = (RadioButton) view.findViewById(R.id.rb_green9);
            rbnblockgreen10 = (RadioButton) view.findViewById(R.id.rb_green10);
            rbnblockgreen1.setClickable(false);
            rbnblockgreen2.setClickable(false);
            rbnblockgreen3.setClickable(false);
            rbnblockgreen4.setClickable(false);
            rbnblockgreen5.setClickable(false);
            rbnblockgreen6.setClickable(false);
            rbnblockgreen7.setClickable(false);
            rbnblockgreen8.setClickable(false);
            rbnblockgreen9.setClickable(false);
            rbnblockgreen10.setClickable(false);
            rbnblockred1 = (RadioButton) view.findViewById(R.id.rb_red1);
            rbnblockred2 = (RadioButton) view.findViewById(R.id.rb_red2);
            rbnblockred3 = (RadioButton) view.findViewById(R.id.rb_red3);
            rbnblockred4 = (RadioButton) view.findViewById(R.id.rb_red4);
            rbnblockred5 = (RadioButton) view.findViewById(R.id.rb_red5);
            rbnblockred6 = (RadioButton) view.findViewById(R.id.rb_red6);
            rbnblockred7 = (RadioButton) view.findViewById(R.id.rb_red7);
            rbnblockred8 = (RadioButton) view.findViewById(R.id.rb_red8);
            rbnblockred9 = (RadioButton) view.findViewById(R.id.rb_red9);
            rbnblockred10 = (RadioButton) view.findViewById(R.id.rb_red10);
            rbnblockred1.setClickable(false);
            rbnblockred2.setClickable(false);
            rbnblockred3.setClickable(false);
            rbnblockred4.setClickable(false);
            rbnblockred5.setClickable(false);
            rbnblockred6.setClickable(false);
            rbnblockred7.setClickable(false);
            rbnblockred8.setClickable(false);
            rbnblockred9.setClickable(false);
            rbnblockred10.setClickable(false);

            rbnblockyellow1 = (RadioButton) view.findViewById(R.id.rb_yellow1);
            rbnblockyellow2 = (RadioButton) view.findViewById(R.id.rb_yellow2);
            rbnblockyellow3 = (RadioButton) view.findViewById(R.id.rb_yellow3);
            rbnblockyellow4 = (RadioButton) view.findViewById(R.id.rb_yellow4);
            rbnblockyellow5 = (RadioButton) view.findViewById(R.id.rb_yellow5);
            rbnblockyellow6 = (RadioButton) view.findViewById(R.id.rb_yellow6);
            rbnblockyellow7 = (RadioButton) view.findViewById(R.id.rb_yellow7);
            rbnblockyellow8 = (RadioButton) view.findViewById(R.id.rb_yellow8);
            rbnblockyellow9 = (RadioButton) view.findViewById(R.id.rb_yellow9);
            rbnblockyellow10 = (RadioButton) view.findViewById(R.id.rb_yellow10);

            rbnblockyellow1.setClickable(false);
            rbnblockyellow2.setClickable(false);
            rbnblockyellow3.setClickable(false);
            rbnblockyellow4.setClickable(false);
            rbnblockyellow5.setClickable(false);
            rbnblockyellow6.setClickable(false);
            rbnblockyellow7.setClickable(false);
            rbnblockyellow8.setClickable(false);
            rbnblockyellow9.setClickable(false);
            rbnblockyellow10.setClickable(false);
            editRemark = (EditText)view.findViewById(R.id.editRemark);
            txtCodeBlock.setText(mParam3);
            txtCodePhase.setText(mParam4);
            GetData();
        }else{
            TextView txtID = view.findViewById(R.id.txtID);
            txtID.setText("Data Tidak Ada");
        }
    }
    private void GetData(){
        database.openTransaction();
        String[] a = new String[4];
        a[0] = mParam1;
        a[1] = mParam2;
        a[2] = mParam3;
        a[3] = "1";
        String query = "SELECT MAX(ZDate) as ZDate FROM tblT_BlockCondition WHERE ZYear=?" +
                "                             AND Period=? AND Block = ? AND TRANSLEVEL = ?";
        tblT_BlockCondition lastDate = (tblT_BlockCondition) database.getDataFirstRaw(query,tblT_BlockCondition.TABLE_NAME,a);
        database.closeTransaction();
        if(lastDate.getAcc_ZDate()!=null){
            setData(lastDate);
            LastDate = lastDate.getAcc_ZDate();
        }
    }


    void setData(tblT_BlockCondition lastDate){
        database.openTransaction();
        List<Object> listObject =  database.getListData(true, tblT_BlockCondition.TABLE_NAME, null,
                tblT_BlockCondition.XML_ZYear+ "=? and "+
                        tblT_BlockCondition.XML_Period+ "=? and "+
                        tblT_BlockCondition.XML_Block+ "=? and "+
                        tblT_BlockCondition.XML_TransLevel+ "=? and "+
                        tblT_BlockCondition.XML_ZDate+ "=? ",
                new String [] {mParam1,mParam2,mParam3,"1",lastDate.getAcc_ZDate()},
                null, null, null, "1");
        database.closeTransaction();
        if(listObject.size() > 0){
            for(int i = 0; i < listObject.size(); i++){
                tblT_BlockCondition dt = (tblT_BlockCondition) listObject.get(i);
                setDataScore(dt);
            }
        }
    }

    void  setDataScore(tblT_BlockCondition dataScore){
        if(mParam4.equalsIgnoreCase("MATURE")){
            setScoreTitiPanen(dataScore);
            setScoreParit(dataScore);
            setScoreJalan(dataScore);
            setScoreJembatan(dataScore);
            setScoreTikus(dataScore);
            setScoreBW(dataScore);
            setScoreCuri(dataScore);
            setScoreGanoderma(dataScore);
            setScoreRayap(dataScore);
            setScoreOrcytes(dataScore);
            setRemark(dataScore);
        }
        else{
            setScoreRintis(dataScore);
            setScoreParit(dataScore);
            setScoreJalan(dataScore);
            setScoreJembatan(dataScore);
            setScoreTikus(dataScore);
            setScoreBW(dataScore);
            setScoreRayap(dataScore);
            setScoreOrcytes(dataScore);
            setRemark(dataScore);
        }


        rbnblockgreen1.setClickable(false);
        rbnblockgreen2.setClickable(false);
        rbnblockgreen3.setClickable(false);
        rbnblockgreen4.setClickable(false);
        rbnblockgreen5.setClickable(false);
        rbnblockgreen6.setClickable(false);
        rbnblockgreen7.setClickable(false);
        rbnblockgreen8.setClickable(false);
        rbnblockgreen9.setClickable(false);
        rbnblockgreen10.setClickable(false);
        rbnblockred1.setClickable(false);
        rbnblockred2.setClickable(false);
        rbnblockred3.setClickable(false);
        rbnblockred4.setClickable(false);
        rbnblockred5.setClickable(false);
        rbnblockred6.setClickable(false);
        rbnblockred7.setClickable(false);
        rbnblockred8.setClickable(false);
        rbnblockred9.setClickable(false);
        rbnblockred10.setClickable(false);
        rbnblockyellow1.setClickable(false);
        rbnblockyellow2.setClickable(false);
        rbnblockyellow3.setClickable(false);
        rbnblockyellow4.setClickable(false);
        rbnblockyellow5.setClickable(false);
        rbnblockyellow6.setClickable(false);
        rbnblockyellow7.setClickable(false);
        rbnblockyellow8.setClickable(false);
        rbnblockyellow9.setClickable(false);
        rbnblockyellow10.setClickable(false);
    }
    void setScoreTitiPanen(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_TitiPanen().equalsIgnoreCase("1")) {
            rbnblockred1.setChecked(true);
        }
        else if (dataScore.getAcc_TitiPanen().equalsIgnoreCase("2")) {
            rbnblockyellow1.setChecked(true);
        }
        else if (dataScore.getAcc_TitiPanen().equalsIgnoreCase("3")) {
            rbnblockgreen1.setChecked(true);
        }else if (dataScore.getAcc_TitiPanen()==null){
            rbnblockred1.setChecked(false);
            rbnblockgreen1.setChecked(false);
            rbnblockyellow1.setChecked(false);
        }
    }
    void setScoreRintis(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_TitiRintis().equalsIgnoreCase("1")) {
            rbnblockred1.setChecked(true);
        }
        else if (dataScore.getAcc_TitiRintis().equalsIgnoreCase("2")) {
            rbnblockyellow1.setChecked(true);
        }
        else if (dataScore.getAcc_TitiRintis().equalsIgnoreCase("3")) {
            rbnblockgreen1.setChecked(true);
        }else if (dataScore.getAcc_TitiRintis()==null){
            rbnblockred1.setChecked(false);
            rbnblockgreen1.setChecked(false);
            rbnblockyellow1.setChecked(false);
        }
    }
    void setScoreParit(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_Parit().equalsIgnoreCase("1")) {
            rbnblockred2.setChecked(true);
        }
        else if (dataScore.getAcc_Parit().equalsIgnoreCase("2")) {
            rbnblockyellow2.setChecked(true);
        }
        else if (dataScore.getAcc_Parit().equalsIgnoreCase("3")) {
            rbnblockgreen2.setChecked(true);
        }else if (dataScore.getAcc_Parit()==null){
            rbnblockred2.setChecked(false);
            rbnblockgreen2.setChecked(false);
            rbnblockyellow2.setChecked(false);
        }
    }
    void setScoreJalan(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_Jalan().equalsIgnoreCase("1")) {
            rbnblockred3.setChecked(true);
        }
        else if (dataScore.getAcc_Jalan().equalsIgnoreCase("2")) {
            rbnblockyellow3.setChecked(true);
        }
        else if (dataScore.getAcc_Jalan().equalsIgnoreCase("3")) {
            rbnblockgreen3.setChecked(true);
        }else if (dataScore.getAcc_Jalan()==null){
            rbnblockred3.setChecked(false);
            rbnblockgreen3.setChecked(false);
            rbnblockyellow3.setChecked(false);
        }
    }
    void setScoreJembatan(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_Jembatan().equalsIgnoreCase("1")) {
            rbnblockred4.setChecked(true);
        }
        else if (dataScore.getAcc_Jembatan().equalsIgnoreCase("2")) {
            rbnblockyellow4.setChecked(true);
        }
        else if (dataScore.getAcc_Jembatan().equalsIgnoreCase("3")) {
            rbnblockgreen4.setChecked(true);
        }else if (dataScore.getAcc_Jembatan()==null){
            rbnblockred4.setChecked(false);
            rbnblockgreen4.setChecked(false);
            rbnblockyellow4.setChecked(false);
        }
    }
    void setScoreTikus(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_Tikus().equalsIgnoreCase("1")) {
            rbnblockred5.setChecked(true);
        }
        else if (dataScore.getAcc_Tikus().equalsIgnoreCase("2")) {
            rbnblockyellow5.setChecked(true);
        }
        else if (dataScore.getAcc_Tikus().equalsIgnoreCase("3")) {
            rbnblockgreen5.setChecked(true);
        }else if (dataScore.getAcc_Tikus()==null){
            rbnblockred5.setChecked(false);
            rbnblockgreen5.setChecked(false);
            rbnblockyellow5.setChecked(false);
        }
    }
    void setScoreBW(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_BW().equalsIgnoreCase("1")) {
            rbnblockred6.setChecked(true);
        }
        else if (dataScore.getAcc_BW().equalsIgnoreCase("2")) {
            rbnblockyellow6.setChecked(true);
        }
        else if (dataScore.getAcc_BW().equalsIgnoreCase("3")) {
            rbnblockgreen6.setChecked(true);
        }else if (dataScore.getAcc_BW()==null){
            rbnblockred6.setChecked(false);
            rbnblockgreen6.setChecked(false);
            rbnblockyellow6.setChecked(false);
        }
    }
    void setScoreCuri(tblT_BlockCondition dataScore){
        if (dataScore.getAcc_Pencurian().equalsIgnoreCase("1")) {
            rbnblockred7.setChecked(true);
        }
        else if (dataScore.getAcc_Pencurian().equalsIgnoreCase("2")) {
            rbnblockyellow7.setChecked(true);
        }
        else if (dataScore.getAcc_Pencurian().equalsIgnoreCase("3")) {
            rbnblockgreen7.setChecked(true);
        }else if (dataScore.getAcc_Pencurian()==null){
            rbnblockred7.setChecked(false);
            rbnblockgreen7.setChecked(false);
            rbnblockyellow7.setChecked(false);
        }
    }
    void setScoreGanoderma(tblT_BlockCondition dataScore){
        if(dataScore.getAcc_Ganoderma()==null){
            rbnblockred8.setChecked(false);
            rbnblockyellow8.setChecked(false);
            rbnblockgreen8.setChecked(false);
        }else {
            if (dataScore.getAcc_Ganoderma().equalsIgnoreCase("1")) {
                rbnblockred8.setChecked(true);
            }
            else if (dataScore.getAcc_Ganoderma().equalsIgnoreCase("2")) {
                rbnblockyellow8.setChecked(true);
            }
            else if (dataScore.getAcc_Ganoderma().equalsIgnoreCase("3")) {
                rbnblockgreen8.setChecked(true);
            }else{
                rbnblockred8.setChecked(false);
                rbnblockgreen8.setChecked(false);
                rbnblockyellow8.setChecked(false);
            }
        }
    }
    void setScoreRayap(tblT_BlockCondition dataScore){
        if(dataScore.getAcc_Rayap()==null){
            rbnblockred9.setChecked(false);
            rbnblockyellow9.setChecked(false);
            rbnblockgreen9.setChecked(false);
        }else {
            if (dataScore.getAcc_Rayap().equalsIgnoreCase("1")) {
                rbnblockred9.setChecked(true);
            }
            else if (dataScore.getAcc_Rayap().equalsIgnoreCase("2")) {
                rbnblockyellow9.setChecked(true);
            }
            else if (dataScore.getAcc_Rayap().equalsIgnoreCase("3")) {
                rbnblockgreen9.setChecked(true);
            }else{
                rbnblockred9.setChecked(false);
                rbnblockgreen9.setChecked(false);
                rbnblockyellow9.setChecked(false);
            }
        }
    }
    void setScoreOrcytes(tblT_BlockCondition dataScore){
        if(dataScore.getAcc_Orcytes()==null){
            rbnblockred10.setChecked(false);
            rbnblockyellow10.setChecked(false);
            rbnblockgreen10.setChecked(false);
        }else {
            if (dataScore.getAcc_Orcytes().equalsIgnoreCase("1")) {
                rbnblockred10.setChecked(true);
            }
            else if (dataScore.getAcc_Orcytes().equalsIgnoreCase("2")) {
                rbnblockyellow10.setChecked(true);
            }
            else if (dataScore.getAcc_Orcytes().equalsIgnoreCase("3")) {
                rbnblockgreen10.setChecked(true);
            }else{
                rbnblockred10.setChecked(false);
                rbnblockgreen10.setChecked(false);
                rbnblockyellow10.setChecked(false);
            }
        }
    }

    void setRemark(tblT_BlockCondition dataScore){
        if(dataScore.getAcc_Remark()==null){
            editRemark.setText("");
            editRemark.setHint("Remark");
        }else {
            editRemark.setText(dataScore.getAcc_Remark());
            editRemark.setEnabled(false);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
