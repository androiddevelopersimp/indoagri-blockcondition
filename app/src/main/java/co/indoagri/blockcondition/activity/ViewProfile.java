package co.indoagri.blockcondition.activity;

import android.content.Context;
import android.support.annotation.NonNull;

import co.indoagri.blockcondition.MyApps;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.model.Users.UserLogin;

public class ViewProfile{
    static Context context = MyApps.getAppContext();
    @NonNull
    public static boolean CheckLogin() {
        boolean result = false;
        DatabaseHandler database = new DatabaseHandler(context);
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        if(userLogin != null){
            result = true;
        }
        else{
            result = false;
        }
        return result;
    }

    @NonNull
    public static String getUserName() {
        String str;
        DatabaseHandler database = new DatabaseHandler(context);
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        str = userLogin.getName();
        return str;
    }
    @NonNull
    public static String getUserNIK() {
        String str;
        DatabaseHandler database = new DatabaseHandler(context);
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        str = userLogin.getNik();
        return str;
    }
    @NonNull
    public static String getRoleID() {
        String str;
        DatabaseHandler database = new DatabaseHandler(context);
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        str = userLogin.getRoleId();
        return str;
    }
    @NonNull
    public static String getDivisions() {
        String str;
        DatabaseHandler database = new DatabaseHandler(context);
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        str = userLogin.getDivision();
        return str;
    }
    @NonNull
    public static String getCostCenter() {
        String str;
        DatabaseHandler database = new DatabaseHandler(context);
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        str = userLogin.getCostCenter();
        return str;
    }
    @NonNull
    public static String getGang() {
        String str;
        DatabaseHandler database = new DatabaseHandler(context);
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        str = userLogin.getGang();
        return str;
    }
    @NonNull
    public static String getCompanyCode() {
        String str;
        DatabaseHandler database = new DatabaseHandler(context);
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        str = userLogin.getCompanyCode();
        return str;
    }

    @NonNull
    public static String getHarvestCode() {
        String str;
        DatabaseHandler database = new DatabaseHandler(context);
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        str = userLogin.getHarvesterCode();
        return str;
    }
    @NonNull
    public static String getEstate() {
        String str;
        DatabaseHandler database = new DatabaseHandler(context);
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        str = userLogin.getEstate();
        return str;
    }

    @NonNull
    public static String getJobPos() {
        String str;
        DatabaseHandler database = new DatabaseHandler(context);
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        str = userLogin.getJobPos();
        return str;
    }

    @NonNull
    public static String getValidFrom() {
        String str;
        DatabaseHandler database = new DatabaseHandler(context);
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        str = userLogin.getValidFrom();
        return str;
    }
    @NonNull
    public static String getTermDate() {
        String str;
        DatabaseHandler database = new DatabaseHandler(context);
        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        str = userLogin.getTermDate();
        return str;
    }

}


