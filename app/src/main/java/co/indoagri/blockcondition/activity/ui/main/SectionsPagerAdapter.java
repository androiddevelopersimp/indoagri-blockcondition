package co.indoagri.blockcondition.activity.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;
import android.widget.FrameLayout;

import co.indoagri.blockcondition.R;


/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2,R.string.tab_text_3};
    private final Context mContext;
    Bundle bundle;
    public SectionsPagerAdapter(Context context, FragmentManager fm,Bundle bundle) {
        super(fm);
        this.bundle = bundle;
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;
        switch (position){
            case 0:
                fragment=new SummaryBlockTab();
                fragment.setArguments(bundle);
                break;
            case 1:
                fragment=new SummarySKBTab();
                fragment.setArguments(bundle);
                break;
            case 2:
                fragment=new SummaryPokokTab();
                fragment.setArguments(bundle);
                break;
            default:
                fragment=null;
                break;
        }
        return  fragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 3;
    }
}
