package co.indoagri.blockcondition.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.RecyclerViewAdapter;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.dialog.DialogConfirmation;
import co.indoagri.blockcondition.handler.GpsHandler;
import co.indoagri.blockcondition.handler.LogOutHandler;
import co.indoagri.blockcondition.listener.DialogConfirmationListener;
import co.indoagri.blockcondition.model.MenuDashboard;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.services.GPSService;
import co.indoagri.blockcondition.services.GPSTriggerService;
import co.indoagri.blockcondition.widget.AutoFitGridLayoutManager;
import co.indoagri.blockcondition.widget.ToastMessage;

public class MenuActivity extends BaseActivity implements RecyclerViewAdapter.ItemListener, RecyclerViewAdapter.LayoutListener{
    RecyclerView recyclerView;
    ArrayList<MenuDashboard> arrayList;
    TextView txtDate;
    ImageView ivLogout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        MenuInit();
        InitProfile();
        InitTime();
    }
    void InitProfile(){
        if(ViewProfile.CheckLogin()){
            TextView txtUsername = (TextView)findViewById(R.id.txtUsername);
            txtUsername.setText(ViewProfile.getUserName());
            TextView txtTitle = (TextView)findViewById(R.id.txtTitle);
            txtTitle.setText(ViewProfile.getRoleID());
            TextView txtEstate = (TextView)findViewById(R.id.txtEstate);
            txtEstate.setText(ViewProfile.getEstate());
        }
     //   Toast.makeText(this, Screens(), Toast.LENGTH_SHORT).show();
    }
    void MenuInit(){
        ivLogout = (ImageView)findViewById(R.id.iv_Logout);
        ivLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDialog();
            }
        });
        arrayList = new ArrayList<>();
        if(isTablet()){
            if(Screens().equalsIgnoreCase("800x1280")){
                arrayList.add(new MenuDashboard("FADB", R.drawable.ic_fadb, "#20678E",true,"co.indoagri.blockcondition.activity.MainActivity"));
                arrayList.add(new MenuDashboard("Rencana Kerja Harian", R.drawable.ic_rkh, "#20678E",true,"co.indoagri.blockcondition.activity.RKHMainGangActivity"));
                arrayList.add(new MenuDashboard("Pengaturan", R.drawable.ic_setup_60, "#20678E",true,"co.indoagri.blockcondition.activity.SetupUniversalActivity"));
            }else{
                arrayList.add(new MenuDashboard("FADB", R.drawable.ic_fadb, "#20678E",true,"co.indoagri.blockcondition.activity.MainActivity"));
                arrayList.add(new MenuDashboard("Rencana Kerja Harian", R.drawable.ic_rkh, "#20678E",true,"co.indoagri.blockcondition.activity.RKHMainGangActivity"));
                arrayList.add(new MenuDashboard("Pengaturan", R.drawable.ic_setup_60, "#20678E",true,"co.indoagri.blockcondition.activity.SetupUniversalActivity"));
            }
        }else{

            arrayList.add(new MenuDashboard("FADB", R.drawable.ic_fadb, "#09A9FF",false,"co.indoagri.blockcondition.activity.MainActivity"));
            arrayList.add(new MenuDashboard("Rencana Kerja Harian", R.drawable.ic_rkh, "#3E51B1",true,"co.indoagri.blockcondition.activity.RKHMainGangActivity"));
            arrayList.add(new MenuDashboard("Pengaturan", R.drawable.ic_setup_60, "#3E51B1",true,"co.indoagri.blockcondition.activity.SetupUniversalActivity"));
        }

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, arrayList, this,this);
        recyclerView.setAdapter(adapter);
        final GridLayoutManager layoutManager = new GridLayoutManager(this,3);
        recyclerView.setLayoutManager(layoutManager);
    }
    @Override
    public void onItemClick(MenuDashboard item) {
        ToRKH(item);
    }
    @Override
    public void onLayoutClick(MenuDashboard item) {
        ToRKH(item);
    }
    void InitTime(){
        txtDate = (TextView)findViewById(R.id.txtDate);
        Thread myThread = null;
        Runnable runnable = new CountDownRunner();
        myThread= new Thread(runnable);
        myThread.start();
    }
    public void doWork() {
        runOnUiThread(new Runnable() {
            public void run() {
                try{
                    Calendar cal = Calendar.getInstance(Locale.getDefault());

                    int days = cal.get(Calendar.DAY_OF_WEEK);
                    int dates = cal.get(Calendar.DATE);
                    int months = cal.get(Calendar.MONTH);
                    int years = cal.get(Calendar.YEAR);

                    int hours = cal.get(Calendar.HOUR);
                    int minutes = cal.get(Calendar.MINUTE);
                    int seconds = cal.get(Calendar.SECOND);
                    int amPm = cal.get(Calendar.AM_PM);

                    switch (amPm) {
                        case Calendar.AM:

                            break;
                        case Calendar.PM:
                            hours = hours + 12;
                        default:
                            break;
                    }

                    txtDate.setText(digitFormat(hours, 2) + ":" + digitFormat(minutes,2) + ":" + digitFormat(seconds,2));

                    String hari = "";

                    switch (days) {
                        case Calendar.SUNDAY:
                            hari = getResources().getString(R.string.minggu);
                            break;
                        case Calendar.MONDAY:
                            hari = getResources().getString(R.string.senin);
                            break;
                        case Calendar.TUESDAY:
                            hari = getResources().getString(R.string.selasa);
                            break;
                        case Calendar.WEDNESDAY:
                            hari = getResources().getString(R.string.rabu);
                            break;
                        case Calendar.THURSDAY:
                            hari = getResources().getString(R.string.kamis);
                            break;
                        case Calendar.FRIDAY:
                            hari = getResources().getString(R.string.jumat);
                            break;
                        case Calendar.SATURDAY:
                            hari = getResources().getString(R.string.sabtu);
                            break;
                        default:
                            break;
                    }


                    String bulan = "";

                    switch (months) {
                        case Calendar.JANUARY:
                            bulan = getResources().getString(R.string.januari);
                            break;
                        case Calendar.FEBRUARY:
                            bulan = getResources().getString(R.string.februari);
                            break;
                        case Calendar.MARCH:
                            bulan = getResources().getString(R.string.maret);
                            break;
                        case Calendar.APRIL:
                            bulan = getResources().getString(R.string.april);
                            break;
                        case Calendar.MAY:
                            bulan = getResources().getString(R.string.mei);
                            break;
                        case Calendar.JUNE:
                            bulan = getResources().getString(R.string.juni);
                            break;
                        case Calendar.JULY:
                            bulan = getResources().getString(R.string.juli);
                            break;
                        case Calendar.AUGUST:
                            bulan = getResources().getString(R.string.agustus);
                            break;
                        case Calendar.SEPTEMBER:
                            bulan = getResources().getString(R.string.september);
                            break;
                        case Calendar.OCTOBER:
                            bulan = getResources().getString(R.string.oktober);
                            break;
                        case Calendar.NOVEMBER:
                            bulan = getResources().getString(R.string.november);
                            break;
                        case Calendar.DECEMBER:
                            bulan = getResources().getString(R.string.desember);
                            break;
                        default:
                            break;
                    }

                    txtDate.setText(hari + " , " + dates + " " + bulan + " " + years);

                }catch (Exception e) {}
            }
        });
    }



    class CountDownRunner implements Runnable{
        // @Override
        public void run() {
            while(!Thread.currentThread().isInterrupted()){
                try {
                    doWork();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }catch(Exception e){
                }
            }
        }
    }
    private String digitFormat(int value, int digit){
        String str = String.valueOf(value);

        int l = str.length();

        if(str.length() < digit){
            for(int i = l; i < digit; i++){
                str = "0" + str;
            }
        }

        return str;
    }
    void DisplayDialog(){
        new DialogConfirmation(MenuActivity.this, getResources().getString(R.string.logout),
                getResources().getString(R.string.logut_message), null, new DialogConfirmationListener() {
            @Override
            public void onConfirmOK(Object object, int id) {
                LogOutHandler logOutHandler = new LogOutHandler(getApplicationContext(),MenuActivity.this);
                logOutHandler.setOnEventListener(new LogOutHandler.LogOutListener() {
                    @Override
                    public void onLogoutInterface(String string, boolean status) {
                        if(status==true){
                            closeAllActivities();
                            finish();
                            animOnFinish();
                            startActivity(new Intent(MenuActivity.this, AuthActivity.class));

                        }
                        else{
                            ToastMessage toastMessage = new ToastMessage(getApplicationContext());
                            toastMessage.shortMessage("ADA MASALAH DENGAN FUNGSI");
                        }
                    }
                });
                logOutHandler.Keluar();
            }

            @Override
            public void onConfirmCancel(Object object, int id) {
               /* ToastMessage toastMessage = new ToastMessage(getApplicationContext());
                toastMessage.shortMessage("ADA MASALAH DENGAN FUNGSI");*/
            }
        }).show();
    }

    void ToRKH(MenuDashboard item){
        Class newclass = null;
        final Activity thisActivity = this;
        String activityString=item.ActivityClass;
        try {
            newclass = Class.forName(activityString);
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
        }

        if(newclass != null) {
            Intent nextIntent = new Intent(thisActivity, newclass);
            nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(nextIntent);
        } else {
            Toast.makeText(getApplicationContext(),"new class null",Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputrkhdate(null);
    }
    private String Screens(){
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        wm.getDefaultDisplay().getRealSize(size);
        String resolution = size.x + "x" + size.y;
        return resolution;
    }
    public boolean isTablet() {
        try {
            // Compute screen size
            Context context = MenuActivity.this;
            DisplayMetrics dm = context.getResources().getDisplayMetrics();
            float screenWidth  = dm.widthPixels / dm.xdpi;
            float screenHeight = dm.heightPixels / dm.ydpi;
            double size = Math.sqrt(Math.pow(screenWidth, 2) +
                    Math.pow(screenHeight, 2));
            // Tablet devices have a screen size greater than 6 inches
            return size >= 6;
        } catch(Throwable t) {
            Log.e("Compute screen size", t.toString());
            return false;
        }
    }
}
