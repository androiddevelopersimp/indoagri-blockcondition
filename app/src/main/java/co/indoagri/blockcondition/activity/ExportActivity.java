package co.indoagri.blockcondition.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.dialog.DialogNotification;
import co.indoagri.blockcondition.fragment.ExportFragment;
import co.indoagri.blockcondition.fragment.ImportFragment;
import co.indoagri.blockcondition.handler.BroadcastConnectorHandler;
import co.indoagri.blockcondition.handler.FolderHandler;
import co.indoagri.blockcondition.handler.XmlHandler;
import co.indoagri.blockcondition.listener.DialogDateListener;
import co.indoagri.blockcondition.listener.DialogNotificationListener;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.MessageStatus;
import co.indoagri.blockcondition.widget.ToastMessage;

import static co.indoagri.blockcondition.activity.AuthActivity.isUsbOtgAvailable;
import static co.indoagri.blockcondition.activity.AuthActivity.isUsbOtgPermit;

public class ExportActivity extends BaseActivity implements ExportFragment.FragmentExportListener,
        ImportFragment.FragmentImportListener,
        BaseActivity.LogOutListener,
        BroadcastConnectorHandler.ConnectivityReceiverListener, View.OnClickListener,DialogNotificationListener, DialogDateListener {
    Button btnImport,btnExport,btnBackup,btnBack,btnNext;
    ToastMessage toastMessage;
    ConstraintLayout constraintLayout;
    View rootView;
    public static TextView txtTitle;
    private CheckBox cbxTransaksiBC;
    String exportDate = null;
    String periodInput;
    EditText editPeriode;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        animOnStart();
        setContentView(R.layout.activity_export);
        registerBaseActivityReceiver();
        toastMessage = new ToastMessage(getApplicationContext());
        constraintLayout = (ConstraintLayout)findViewById(R.id.constraint);
        txtTitle = (TextView)findViewById(R.id.txtTitleHeader);
        btnBack = (Button)findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
        btnExport = (Button)findViewById(R.id.btnExport);
        btnExport.setOnClickListener(this);
        btnNext = (Button)findViewById(R.id.btnNext);
        btnNext.setVisibility(View.GONE);
        cbxTransaksiBC = (CheckBox)findViewById(R.id.cbxExportTransaksiBC);
        editPeriode = (EditText)findViewById(R.id.editPeriodeXML);
        setDefault();
        exportDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
    }
    void setDefault(){
        btnNext.setVisibility(View.GONE);
        txtTitle.setText(getResources().getString(R.string.export_condition));
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                animOnFinish();
                finish();
                break;
            case R.id.btnExport:
                FolderHandler folderHandler = new FolderHandler(ExportActivity.this);
                List<MessageStatus> lstMsgStatus = new ArrayList<MessageStatus>();
                boolean isUsbOtgSave = isUsbOtgAvailable && isUsbOtgPermit ;

//			Toast.makeText(getApplicationContext(), isUsbOtgAvailable + ":" + isUsbOtgPermit + ":" + cbxExportUsbOtg.isChecked(), Toast.LENGTH_SHORT).show();
                periodInput = editPeriode.getText().toString();
                if(folderHandler.isSDCardWritable() && folderHandler.init()){
                    String status = null;

                    if(cbxTransaksiBC.isChecked()){
                        MessageStatus messageStatus = new XmlHandler(ExportActivity.this).createXML(R.id.cbxExportTransaksiBC, status, isUsbOtgSave, exportDate,periodInput);

                        if(messageStatus != null){
                            lstMsgStatus.add(messageStatus);
                        }
                    }

                    if(lstMsgStatus.size() > 0){
                        String message = "";

                        for(int i = 0; i < lstMsgStatus.size(); i++){
                            MessageStatus msgStatus = (MessageStatus) lstMsgStatus.get(i);

                            message = message + "\n" + msgStatus.getMenu() + ": " + msgStatus.getMessage();
                        }

                        new DialogNotification(ExportActivity.this, getResources().getString(R.string.informasi),
                                message, false).show();
                    }
                }else{
                    new DialogNotification(ExportActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.error_sd_card), false).show();
                }
                break;
            default:
                break;
        }
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.e("ONDESTROY", "OnDestory()");
        unRegisterBaseActivityReceiver();
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.e("ONSTOP", "OnStop()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("ONPAUSE", "onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        setDefault();
        Log.e("ONRESUME", "onResume()");
    }

    @Override
    public void doLogout() {

    }

    @Override
    public void onInterfaceExport(String input) {

    }

    @Override
    public void onInterfaceImport(String input) {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onDateOK(Date date, int id) {

    }

    @Override
    public void onOK(boolean is_finish) {
        if(is_finish){
            finish();
        }
    }
}
