package co.indoagri.blockcondition.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.RKHPekerjaAdapter;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.handler.GpsHandler;
import co.indoagri.blockcondition.model.Data.BKMOutput;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.RKH.RKH_HEADER;
import co.indoagri.blockcondition.model.Users.Employee;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.services.GPSService;
import co.indoagri.blockcondition.services.GPSTriggerService;

public class RKHPekerjaActivity extends AppCompatActivity
        implements RKHPekerjaAdapter.ItemListener, RKHPekerjaAdapter.ItemChooseListener, SearchView.OnQueryTextListener {
    RKHPekerjaAdapter adapter;
    ArrayList<Employee> employeeArrayList = new ArrayList<Employee>();
    RecyclerView recyclerView;
    GetDataAsyncTask getDataAsync;
    String companyCode;
    String estate;
    String division;
    String gang;
    String nik;
    Toolbar toolbar;
    TextView mTextToolbar;
    TextView mTextToolbarSave;
    EditText edtTxtDivisi;
    Bundle bundle;
    RelativeLayout ChooseGang;
    EditText edtTxtGang;
    public static int CHOOSE_GANG = 01;
    String InputPekerja = null;
    Button btnSavePekerja;
    public static boolean BtnSaveVisible = false;
    SearchView searchView;
    DatabaseHandler database = new DatabaseHandler(RKHPekerjaActivity.this);
    String Imei = null;
    Employee saveDataEmployee = null;
    String RkhID = null;
    Bundle bundleRKH;
    RKH_HEADER rkh_header;
    String Method = null;
    String RKHDATE = "";
    long TodayDate = 0;
    String TitleStatus;
    String Pekerja =null;
    String gpsKoordinat = "0.0:0.0";
    double latitude = 0;
    double longitude = 0;
    GPSService gps;
    GpsHandler gpsHandler;
    String Clerk_NIK ="";
    String Clerk_Name="";
    String EmpType="";
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.rkhpekerja_layout);
        SetGPS();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        btnSavePekerja = (Button)findViewById(R.id.btnSaveEdit);
        btnSavePekerja.setVisibility(View.GONE);
        searchView = findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(this);
        setUpToolbar();
        initEmployee();
        initHeaderInput();
        Imei = getUniqID(this);

    }
     void setUpToolbar() {
            bundle= getIntent().getExtras();
            toolbar = (findViewById(R.id.toolbar));
            mTextToolbar = (findViewById(R.id.txt_Toolbar));
             if(bundle!=null) {
                 TitleStatus  = bundle.getString("TOOLBARTITLE");
                 if(TitleStatus.equalsIgnoreCase("Ubah Pekerja")){
                     bundleRKH= getIntent().getExtras();
                     if(bundleRKH!=null) {
                         rkh_header = getIntent().getExtras().getParcelable("Data");
                         Method = bundle.getString("Method");
                         Pekerja = bundle.getString("Pekerja");
                         RKHDATE = rkh_header.getRKH_DATE();
                     }
                 }
                 mTextToolbar.setText(TitleStatus+" ( "+Pekerja+" )");

             }else{
                 mTextToolbar.setText(getResources().getString(R.string.alihkan_mandor));
             }
            mTextToolbarSave = (findViewById(R.id.txt_SaveToolbar));
            mTextToolbarSave.setVisibility(View.GONE);
            mTextToolbarSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(TitleStatus.equals("Pilih Pekerja")){
                        SaveDataNextInputJob();
                    }else if(TitleStatus.equals("Ubah Pekerja")){
                        SaveDataChangeJob();
                    }

                }
            });

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    void initEmployee(){
        new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputPekerja1(null);
        companyCode = ViewProfile.getCompanyCode();
        estate = ViewProfile.getEstate();
        division = ViewProfile.getDivisions();
        gang = ViewProfile.getGang();
        nik = ViewProfile.getUserNIK();
        SetList();
        getData();
    }

    void initHeaderInput(){
        edtTxtDivisi = findViewById(R.id.edtTxt_Divisi);
        edtTxtDivisi.setText(ViewProfile.getDivisions());
        edtTxtDivisi.setEnabled(false);
        edtTxtGang = findViewById(R.id.edtTxt_Gang);
        edtTxtGang.setEnabled(false);
        ChooseGang = findViewById(R.id.ChooseGang);
        ChooseGang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(RKHPekerjaActivity.this,RKHGangActivity.class);
                newIntent.putExtra("TOOLBARTITLE","Pilih Gang");
                startActivityForResult(newIntent, CHOOSE_GANG);
            }
        });

    }

    @Override
    public void onItemClick(Employee item, int position) {
    //    Toast.makeText(this, item.getName(), Toast.LENGTH_SHORT).show();
    }

    private void getData(){
        if(TitleStatus.equalsIgnoreCase("Pilih Pekerja")){
            EmpType = "LEADER";
            getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null,
                    Employee.XML_COMPANY_CODE + "=?" + " and " +
                            Employee.XML_GANG + "=?" + " and " +
                            Employee.XML_ROLE_ID + "=?" + " and " +
                            Employee.XML_ESTATE + "=?",
                    new String [] {companyCode,gang,EmpType, estate},
                    null, null, Employee.XML_NAME, null);
            getDataAsync.execute();
        }else{
            if(Pekerja.equals("mandor")){
                EmpType = "LEADER";
                getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null,
                        Employee.XML_COMPANY_CODE + "=?" + " and " +
                                Employee.XML_GANG + "=?" + " and " +
                                Employee.XML_ROLE_ID + "=?" + " and " +
                                Employee.XML_ESTATE + "=?",
                        new String [] {companyCode,gang,EmpType, estate},
                        null, null, Employee.XML_NAME, null);
                getDataAsync.execute();

            }if(Pekerja.equals("kerani")){
                EmpType = "KERANI";
                getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null,
                        Employee.XML_COMPANY_CODE + "=?" + " and " +
                                Employee.XML_GANG + "=?" + " and " +
                                Employee.XML_ROLE_ID + "=?" + " and " +
                                Employee.XML_ESTATE + "=?",
                        new String [] {companyCode,gang,EmpType, estate},
                        null, null, Employee.XML_NAME, null);
                getDataAsync.execute();
            }
        }
    }
    @Override
    public void onBackPressed() {
        DisplayDialogSave();
        if (gps != null) {
            gps.stopUsingGPS();
        }
    }
    @Override
    public void onItemChoose(Employee item, int position) {
        BtnSaveVisible = true;
        InitialbtnSave();
        saveDataEmployee = item;
        InputPekerja = item.getName();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        adapter.getFilter().filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return false;
    }

    private class GetDataAsyncTask extends AsyncTask<Void, ArrayList<Employee>, ArrayList<Employee>> {
        boolean distinct;
        String tableName;
        String [] columns;
        String whereClause;
        String [] whereArgs;
        String groupBy;
        String having;
        String orderBy;
        String limit;
        public GetDataAsyncTask(boolean distinct, String tableName, String [] columns, String whereClause, String [] whereParams,
                                String groupBy, String having, String orderBy, String limit){
            this.distinct = distinct;
            this.tableName = tableName;
            this.columns = columns;
            this.whereClause = whereClause;
            this.whereArgs = whereParams;
            this.groupBy = groupBy;
            this.having = having;
            this.orderBy = orderBy;
            this.limit = limit;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected ArrayList<Employee> doInBackground(Void... voids) {
            DatabaseHandler database = new DatabaseHandler(RKHPekerjaActivity.this);
            ArrayList<Employee> listTemp = new ArrayList<Employee>();
            List<Object> listObject;

            database.openTransaction();
            listObject = database.getListData(distinct, tableName, columns, whereClause, whereArgs, groupBy, having, orderBy, limit);
            database.closeTransaction();

            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    Employee empNew = (Employee) listObject.get(i);
                    boolean foundFilter = false;
                    boolean foundCurrent = false;

                    if(employeeArrayList.size() > 0){
                        for(int x = 0; x < employeeArrayList.size(); x++){
                            Employee empCur = (Employee) employeeArrayList.get(x);

                            if(empNew.getNik().equals(empCur.getNik())){
                                foundCurrent = true;
                                break;
                            }
                        }
                    }

                    if(!foundFilter && !foundCurrent){
                        listTemp.add(empNew);
                    }
                }
            }else{
                listTemp.clear();
            }

            return listTemp;
        }

        @Override
        protected void onPostExecute(ArrayList<Employee> lstTemp) {
            super.onPostExecute(lstTemp);
            if(lstTemp.size()>0){
                for(int i = 0; i < lstTemp.size(); i++){
                    Employee employee = (Employee) lstTemp.get(i);
                    adapter.addData(employee);
                }
            }else{
              SetList();
            }

        }
    }

    void SetList(){
        adapter = new RKHPekerjaAdapter(this, employeeArrayList,this,this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == CHOOSE_GANG) {
            if(resultCode == RESULT_OK) {
                String ItemGang = null;
                ItemGang = new PreferenceManager(getApplicationContext(), Constants.shared_name).getRkhInputgang();
                if(ItemGang!=null){
                    BtnSaveVisible = false;
                    InitialbtnSave();
                    gang = ItemGang;
                    edtTxtGang.setText(ItemGang);
                    employeeArrayList.clear();
                    getData();
                }else{
                    BtnSaveVisible = false;
                    InitialbtnSave();
                    edtTxtGang.setText(gang);
                    employeeArrayList.clear();
                    getData();
                }
            }
        }
    }

    void InitialbtnSave(){
        if(BtnSaveVisible){
            mTextToolbarSave.setVisibility(View.VISIBLE);
        }else{
            mTextToolbarSave.setVisibility(View.GONE);
        }
    }

    void DisplayDialogSave(){
        if(InputPekerja==null){
            new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputPekerja1(null);
            InputPekerja = null;
            Intent intent=new Intent();
            setResult(RESULT_OK, intent);
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }else if(InputPekerja != null){
            // Build an AlertDialog
            AlertDialog.Builder builder = new AlertDialog.Builder(RKHPekerjaActivity.this);
            builder.setTitle("Akan Menghapus Data Pekerja");
            builder.setMessage("Menghapus Data ? ");
            // Set the alert dialog yes button click listener
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    NotSaveDataAndBack();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    void SaveDataNextInputJob(){
        new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputPekerja1(InputPekerja);
        if(saveData(saveDataEmployee)){
                inputJobNew();
        }else{
            Toast.makeText(this, "Ada Kesalahan Data", Toast.LENGTH_SHORT).show();
        }
    }
    void SaveDataChangeJob(){
        if(Pekerja.equalsIgnoreCase("mandor")){
            new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputPekerja1(InputPekerja);
            if(changeDataMandor(saveDataEmployee)){
                inputJobExist();
            }else{
                Toast.makeText(this, "Ada Kesalahan Data", Toast.LENGTH_SHORT).show();
            }
        }else if(Pekerja.equalsIgnoreCase("kerani")){
             new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputPekerja1(InputPekerja);
            if(changeDataClerk(saveDataEmployee)){
                inputJobExist();
            }else{
                Toast.makeText(this, "Ada Kesalahan Data", Toast.LENGTH_SHORT).show();
            }
        }
    }
    void NotSaveDataAndBack(){
        new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputPekerja1(null);
        InputPekerja = null;
        Intent intent=new Intent();
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private String getUniqID(Context context){
        String number = null;
        if(Constants.const_DeviceID != null || !Constants.const_DeviceID.equals("")){
            number =Constants.const_DeviceID;
        }else{

            number = Constants.const_IMEI;
        }
        return number;
    }

    private boolean saveData(Employee employee){
        boolean hasil = false;
        long RowId = 0;
        RkhID = (TextUtils.isEmpty(RkhID)) ? new DateLocal(new Date()).getDateString(DateLocal.FORMAT_ID) + Imei.substring(8, Imei.length() - 1) : RkhID;
        RKHDATE = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        TodayDate = cal.getTimeInMillis();
        try{
        database.openTransaction();
         RKH_HEADER setDAta = new RKH_HEADER(Imei,
                 employee.getCompanyCode(),
                 employee.getEstate(),
                 RKHDATE,
                 employee.getDivision(),
                 gang,
                 employee.getNik(),
                 employee.getName(),
                 Clerk_NIK,
                 Clerk_Name,
                    gpsKoordinat,
                 0,
                 TodayDate,
                 nik,
                 TodayDate,
                 nik,
                 RkhID);
            RowId = database.setData(setDAta);
        database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
            if(RowId>0){
                hasil = true;
            }
        }
        return hasil;
    }
    private boolean changeDataMandor(Employee employee){
        boolean hasil = false;
        long RowId = 0;
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        TodayDate = cal.getTimeInMillis();
        try{
            database.openTransaction();
            RKH_HEADER setDAta = new RKH_HEADER(rkh_header.getIMEI(),
                    rkh_header.getCOMPANY_CODE(),
                    rkh_header.getESTATE(),
                    rkh_header.getRKH_DATE(),
                    rkh_header.getDIVISION(),
                    gang,
                    employee.getNik(),
                    employee.getName(),
                    rkh_header.getNIK_CLERK(),
                    rkh_header.getCLERK(),
                    gpsKoordinat,
                    0,
                    rkh_header.getCREATED_DATE(),
                    rkh_header.getCREATED_BY(),
                    TodayDate,
                    employee.getNik(),
                    rkh_header.getRKH_ID());
            RowId = database.updateData(setDAta,
                    RKH_HEADER.XML_RKH_ID + "=?",
                    new String [] {rkh_header.getRKH_ID()});
            database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
            if(RowId>0){
                hasil = true;
            }
        }
        return hasil;
    }
    private boolean changeDataClerk(Employee employee){
        boolean hasil = false;
        long RowId = 0;
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        TodayDate = cal.getTimeInMillis();
        try{
            database.openTransaction();
            RKH_HEADER setDAta = new RKH_HEADER(rkh_header.getIMEI(),
                    rkh_header.getCOMPANY_CODE(),
                    rkh_header.getESTATE(),
                    rkh_header.getRKH_DATE(),
                    rkh_header.getDIVISION(),
                    gang,
                    rkh_header.getNIK_FOREMAN(),
                    rkh_header.getFOREMAN(),
                    employee.getNik(),
                    employee.getName(),
                    gpsKoordinat,
                    0,
                    rkh_header.getCREATED_DATE(),
                    rkh_header.getCREATED_BY(),
                    TodayDate,
                    employee.getNik(),
                    rkh_header.getRKH_ID());
            RowId = database.updateData(setDAta,
                    RKH_HEADER.XML_RKH_ID + "=?",
                    new String [] {rkh_header.getRKH_ID()});
            database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
            if(RowId>0){
                hasil = true;
            }
        }
        return hasil;
    }
    private int DeleteData(Employee employee){
        int Delete;
        database.openTransaction();
        Delete = database.deleteData(RKH_HEADER.TABLE_NAME, null, null);
        database.commitTransaction();
        database.closeTransaction();
        return Delete;
    }

    void inputJobNew(){
        Class newclass = null;
        final Activity thisActivity = this;
        String activityString="co.indoagri.blockcondition.activity.RKHJOBActivity";
        try {
            newclass = Class.forName(activityString);
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
        }
        database.openTransaction();
            RKH_HEADER rkh_header_input = (RKH_HEADER)database.getDataFirst(false, RKH_HEADER.TABLE_NAME, null,
                RKH_HEADER.XML_RKH_ID + "=?",
                new String [] {RkhID},
                null, null, null, null);
            database.closeTransaction();
        if(newclass != null) {
            Intent nextIntent = new Intent(thisActivity, newclass);
            nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            nextIntent.putExtra("Data",rkh_header_input);
            nextIntent.putExtra("Method","new");
            startActivity(nextIntent);
            finish();
        } else {
            Toast.makeText(getApplicationContext(),"new class null",Toast.LENGTH_SHORT).show();
        }
    }
    void inputJobExist(){
        new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputPekerja1(null);
        database.openTransaction();
        RKH_HEADER rkh_header_input = (RKH_HEADER)database.getDataFirst(false, RKH_HEADER.TABLE_NAME, null,
            RKH_HEADER.XML_RKH_ID + "=?",
            new String [] {rkh_header.getRKH_ID()},
            null, null, null, null);
            database.closeTransaction();
        InputPekerja = null;
        Intent intent=new Intent();
        intent.putExtra("Data",rkh_header_input);
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    void SetGPS(){
        gps = new GPSService(RKHPekerjaActivity.this);
        gpsHandler = new GpsHandler(RKHPekerjaActivity.this);

        GPSTriggerService.rkhPekerjaActivity = this;

        gpsHandler.startGPS();
        gpsKoordinat = getLocation();
    }
    private String getLocation(){
        String gpsKoordinat = "0.0:0.0";

        if(gps != null){
            Location location = gps.getLocation();
            if(location != null){
                latitude = location.getLatitude();
                longitude = location.getLongitude();

                gpsKoordinat = latitude + ":" + longitude;
            }
        }

        return gpsKoordinat;
    }

    public void updateGpsKoordinat(Location location){
        if(location != null){
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            gpsKoordinat = latitude + ":" + longitude;
        }
    }
}
