package co.indoagri.blockcondition.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.fragment.LevelCodeFragment;
import co.indoagri.blockcondition.fragment.LevelBlockFragment;
import co.indoagri.blockcondition.fragment.LevelPokokFragment;
import co.indoagri.blockcondition.fragment.LevelSKBFragment;
import co.indoagri.blockcondition.fragment.MasterBlockHdrcFragment;
import co.indoagri.blockcondition.model.Data.BlockHdrc;
import co.indoagri.blockcondition.model.Data.tblM_AccountingPeriod;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.widget.ToastMessage;

public class BlockActivity extends BaseActivity implements
        LevelCodeFragment.FragmentLevelCodeListener,
        LevelBlockFragment.FragmentLevelBlockListener,
        LevelSKBFragment.FragmentLevelSKBListener,
        LevelPokokFragment.FragmentLevelPokokListener,
        MasterBlockHdrcFragment.FragmentListBlockListener{
    public static DialogProgress dialogProgress;
    boolean doubleBackToExitPressedOnce = false;
    DatabaseHandler database;
    ToastMessage toastMessage;
    ConstraintLayout constraintLayout;
    View rootView;
    public static TextView txtTitle,txtTanggalBlock,titleFragment;
    public static View relContainerTitle,relContainerSubTitle;
    boolean onUserInteraction = false;
    public static String LEVELBLOCK = "LEVELBLOCK";
    public static String LEVELSKB = "LEVELSKB";
    public static String LEVELCODE= "LEVELCODE";
    public static String LEVELPOKOK= "LEVELPOKOK";
    public static String MASTERBLOCKHDRC= "MASTERBLOCKHDRC";
    ImageView shutDown;
    String companyCode = null;
    String estate = null;
    String division = null;
    String nik = null;
    String block = null;
    String croptype = null;
    public String SESS_ZDATE = null;
    public static String SESS_ZYEAR = null;
    public static String SESS_PERIOD = null;
    public static String SESS_PERIOD_TODATE = null;
    public static String SESS_PERIOD_FROMDATE = null;
    TextView textView;
    int Status = 0;
    @Override
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        animOnStart();
        registerBaseActivityReceiver();
        setContentView(R.layout.activity_block);
        dialogProgress = new DialogProgress(BlockActivity.this);
        constraintLayout = (ConstraintLayout)findViewById(R.id.constraint);
        txtTitle = (TextView)findViewById(R.id.txtTitleHeader);
        txtTanggalBlock = (TextView)findViewById(R.id.txtTanggalBlock);
        relContainerTitle = (View)findViewById(R.id.titleActivityHeader);
        relContainerSubTitle = (View)findViewById(R.id.titleSubActivityHeader);
        txtTanggalBlock = (TextView)findViewById(R.id.txtTanggalBlock);
        titleFragment = (TextView)findViewById(R.id.titleFragment);
        toastMessage = new ToastMessage(getApplicationContext());
        rootView = constraintLayout;
        shutDown = (ImageView) findViewById(R.id.imgActionBarLogout);
        shutDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*  dialogboxLogout();*/
                Intent intent = new Intent(BlockActivity.this, MainActivity.class);
                Bundle b = new Bundle();
                b.putInt(Constants.CONSTBUNDLE, 3); //Your id
                intent.putExtras(b); //Put your id to your next Intent
                startActivity(intent);
                finish();
            }
        });
        setDefault();
        initToolbar();
        new PreferenceManager(getApplicationContext(),Constants.shared_name).setDatein(new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT));
        SESS_ZYEAR = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_YEAR_ONLY);
        SESS_ZDATE = new PreferenceManager(this,Constants.shared_name).getDatein();
        SESS_PERIOD = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_MONTH_ONLY);
        GetData();
        GetData2();

    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.e("ONSTOP", "OnStop()");
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.e("ONDESTROY", "OnDestory()");
        unRegisterBaseActivityReceiver();
    }
    private void initToolbar() {
        if(ViewToolbar.CheckLogin()){
            TextView txtDivision = (TextView)findViewById(R.id.txtUserDivision);
            TextView txtBA = (TextView)findViewById(R.id.txtBA);
            txtBA.setText( ViewToolbar.getEstate());
            txtDivision.setText(ViewToolbar.getDivisions());
            companyCode = ViewToolbar.getCompanyCode();
            estate = ViewToolbar.getEstate();
            division = ViewToolbar.getDivision();
            nik = ViewToolbar.getNik();
        }
    }
    private void setDefault(){
        txtTitle.setText(getResources().getString(R.string.app_name));
        Class fragmentClass;
        fragmentClass = LevelCodeFragment.class;
        Bundle args = new Bundle();
        args.putString(BlockHdrc.XML_COMPANY_CODE, companyCode);
        args.putString(BlockHdrc.XML_ESTATE, estate);
        args.putString(BlockHdrc.XML_DIVISION, division);
        args.putString(BlockHdrc.XML_BLOCK, block);
        setDefaultFragment(fragmentClass,
                args,
                R.id.fragment_container,
                LEVELCODE);
    }

    @Override
    public void onInputLevelCode(String input, String block, String phase) {

        if(input.toString().equalsIgnoreCase(getResources().getString(R.string.blocks))){
                toMasterBlockHDRC(input,1);
        }
        if(input.toString().equalsIgnoreCase(getResources().getString(R.string.m_date))){
           // toMasterBlockHDRC(input,1);
        }
        if(input.toString().equalsIgnoreCase(getResources().getString(R.string.next))){
            toBLOCK(input,block,phase);
        }
        if(input.toString().equalsIgnoreCase(getResources().getString(R.string.nav_home))){
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            animOnFinish();
            finish();
        }
    }

    public void toBLOCK(String input, String block, String phase){
        Bundle args = new Bundle();
        args.putString(BlockHdrc.XML_BLOCK, block);
        args.putString("Phase", phase);
        nextFragment(LevelBlockFragment.class,
                args,
                R.id.fragment_container,LEVELBLOCK);
    }
    public void toMasterBlockHDRC(String newText, int statusSend){
        relContainerSubTitle.setVisibility(View.GONE);
        Bundle args = new Bundle();
        args.putString(BlockHdrc.XML_COMPANY_CODE, companyCode);
        args.putString(BlockHdrc.XML_ESTATE, estate);
        args.putString(BlockHdrc.XML_DIVISION, division);
        args.putBoolean(Constants.SEARCH, true);
        args.putInt(Constants.inStatus,statusSend);
        Status = statusSend;
        nextFragment(MasterBlockHdrcFragment.class,
                args,
                R.id.fragment_container,MASTERBLOCKHDRC);
    }
    public void toSKB(String input,String block,String phase){
        Bundle args = new Bundle();
        args.putString(BlockHdrc.XML_BLOCK, block);
        args.putString("Phase", phase);
        nextFragment(LevelSKBFragment.class,
                args,
                R.id.fragment_container,LEVELSKB);
    }
    public void toPOKOK(String input,String block,String phase,String SKB){
        new PreferenceManager(BlockActivity.this,Constants.shared_name).setPokokR(0);
        new PreferenceManager(BlockActivity.this,Constants.shared_name).setPokokL(0);
        Bundle args = new Bundle();
        args.putString(BlockHdrc.XML_BLOCK, block);
        args.putString("Phase", phase);
        args.putString(tblT_BlockCondition.XML_SKB, SKB);
        nextFragment(LevelPokokFragment.class,
                args,
                R.id.fragment_container,LEVELPOKOK);
    }

    @Override
    public void onInputLevelBlock(String input,String block, String phase) {
        relContainerTitle.setVisibility(View.GONE);
        relContainerSubTitle.setVisibility(View.VISIBLE);
        if(input.toString().equalsIgnoreCase(getResources().getString(R.string.next))){
            fragment= baseFragmentManager.findFragmentByTag(LEVELSKB);
            if(fragment == null){
                toSKB(input,block,phase);
            }
            else{
                toSKB(input,block,phase);
            }
        }
        if(input.toString().equalsIgnoreCase(getResources().getString(R.string.back))){

            fragment= baseFragmentManager.findFragmentByTag(LEVELCODE);
            if(fragment == null){
                FragmentManager manager = getSupportFragmentManager();
                Bundle args = new Bundle();
                args.putString(BlockHdrc.XML_COMPANY_CODE, companyCode);
                args.putString(BlockHdrc.XML_ESTATE, estate);
                args.putString(BlockHdrc.XML_DIVISION, division);
                args.putString(BlockHdrc.XML_BLOCK, block);
                fragment.setArguments(args);
                manager.popBackStack();
            }
            else{
                FragmentManager manager = getSupportFragmentManager();
                Bundle args = new Bundle();
                args.putString(BlockHdrc.XML_COMPANY_CODE, companyCode);
                args.putString(BlockHdrc.XML_ESTATE, estate);
                args.putString(BlockHdrc.XML_DIVISION, division);
                args.putString(BlockHdrc.XML_BLOCK, block);
                fragment.setArguments(args);
                manager.popBackStack();
            }
        }
    }

    @Override
    public void onInputLevelSKB(String input,String block,String phase,String SKB) {
        relContainerTitle.setVisibility(View.GONE);
        relContainerSubTitle.setVisibility(View.VISIBLE);
        if(input.toString().equalsIgnoreCase(getResources().getString(R.string.back))){
            fragment= baseFragmentManager.findFragmentByTag(LEVELBLOCK);
            if(fragment == null){
                FragmentManager manager = getSupportFragmentManager();
                Bundle args = new Bundle();
                args.putString(BlockHdrc.XML_BLOCK, block);
                args.putString("Phase", phase);
                fragment.setArguments(args);
                manager.popBackStack();
            }
            else{
                FragmentManager manager = getSupportFragmentManager();
                Bundle args = new Bundle();
                args.putString(BlockHdrc.XML_BLOCK, block);
                args.putString("Phase", phase);
                fragment.setArguments(args);
                manager.popBackStack();
            }
        }
        if(input.toString().equalsIgnoreCase(getResources().getString(R.string.next))){
            relContainerTitle.setVisibility(View.GONE);
            relContainerSubTitle.setVisibility(View.VISIBLE);
            fragment= baseFragmentManager.findFragmentByTag(LEVELPOKOK);
            if(fragment == null){
                toPOKOK(input,block,phase,SKB);
            }
            else{
                toPOKOK(input,block,phase,SKB);
            }
           Log.i("Informasi",input);
        }
    }




 /*   @Override
    public void onBackPressed() {
        if(getFragmentManagerAccount()==2){
            if(relContainerTitle.getVisibility()==View.GONE){
                relContainerTitle.setVisibility(View.VISIBLE);
                relContainerSubTitle.setVisibility(View.GONE);
            }
        }
        if (getFragmentManagerAccount() > 1) {
           // Log.i("Total",String.valueOf(fragmentManager.getBackStackEntryCount()));
            Log.i("MainActivity", "popping backstack");
            baseFragmentManager.popBackStack();
        } else {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                animOnFinish();
                finish();
                return;
        }
    }*/
 @Override
 public boolean onKeyDown(int keyCode, KeyEvent event)
 {
     return (keyCode == KeyEvent.KEYCODE_BACK ? true : super.onKeyDown(keyCode, event));
 }

    @Override
    public void onInputLevelPokok(String input,String Block, String Phase, String skb) {
        relContainerTitle.setVisibility(View.GONE);
        relContainerSubTitle.setVisibility(View.VISIBLE);
        if(input.toString().equalsIgnoreCase(getResources().getString(R.string.skb))){
            fragment= baseFragmentManager.findFragmentByTag(LEVELSKB);
            if(fragment == null){
                /*FragmentManager manager = getSupportFragmentManager();
                Bundle args = new Bundle();
                args.putString(BlockHdrc.XML_BLOCK, block);
                args.putString("Phase", Phase);
                args.putString(tblT_BlockCondition.XML_SKB, skb);
                fragment.setArguments(args);
                manager.popBackStack();*/
                getSupportFragmentManager().popBackStack();
            }
            else{
                /*FragmentManager manager = getSupportFragmentManager();
                Bundle args = new Bundle();
                args.putString(BlockHdrc.XML_BLOCK, block);
                args.putString("Phase", Phase);
                args.putString(tblT_BlockCondition.XML_SKB, skb);
                fragment.setArguments(args);
                manager.popBackStack();*/
                getSupportFragmentManager().popBackStack();
            }
            Log.i("Informasi",input);
        }
        if(input.toString().equalsIgnoreCase(getResources().getString(R.string.next))){
            Bundle args = new Bundle();
            args.putString(BlockHdrc.XML_BLOCK, block);
            args.putString("Phase", Phase);
            args.putString(tblT_BlockCondition.XML_SKB, skb);
            ExistFragment(LevelPokokFragment.class,
                    args,
                    R.id.fragment_container,LEVELPOKOK);
        }
    }

    @Override
    public void onInputBlockList(String input) {
        block = input;

        if(input==null){
            if(Status==Constants.MatureNotSurveyedBlock || Status==Constants.IMMatureNotSurveyedBlock){
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                animOnFinish();
                finish();
            }
            FragmentManager manager = getSupportFragmentManager();
            Bundle args = new Bundle();
            args.putString(BlockHdrc.XML_COMPANY_CODE, companyCode);
            args.putString(BlockHdrc.XML_ESTATE, estate);
            args.putString(BlockHdrc.XML_DIVISION, division);
            args.putString(BlockHdrc.XML_BLOCK, block);
            fragment.setArguments(args);
            manager.popBackStack();
        }
        else{
            if(Status==Constants.MatureNotSurveyedBlock || Status==Constants.IMMatureNotSurveyedBlock){
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                animOnFinish();
                finish();
            }
            Bundle args = new Bundle();
            args.putString(BlockHdrc.XML_COMPANY_CODE, companyCode);
            args.putString(BlockHdrc.XML_ESTATE, estate);
            args.putString(BlockHdrc.XML_DIVISION, division);
            args.putString(BlockHdrc.XML_BLOCK, block);
            ExistFragment(LevelCodeFragment.class,
                    args,
                    R.id.fragment_container,LEVELCODE);
        }
    }

    void GetData() {
        int value;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            value = bundle.getInt("SEND");
            toMasterBlockHDRC("",value);
        }
        Date date;
        date = new Date(new Date().getTime());
        txtTanggalBlock.setText(new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT));
       database = new DatabaseHandler(getApplicationContext());
        database.openTransaction();
        String[] a = new String[2];
        a[0] = SESS_ZYEAR;
        a[1] = SESS_ZDATE;
        String query = "SELECT Periode, ClosingDate FROM tblM_AccountingPeriod " +
                "WHERE  ZYear = ? AND  ClosingDate  >= ? " +
                "ORDER BY ClosingDate ASC LIMIT 1";
        tblM_AccountingPeriod dtsource1 = (tblM_AccountingPeriod) database.getDataFirstRaw(query,tblM_AccountingPeriod.TABLE_NAME,a);
        database.closeTransaction();
        if(dtsource1!= null){
           SESS_PERIOD = dtsource1.getAcc_Period();
            String hasil = FormatDate(dtsource1.getAcc_ClosingDate());
            SESS_PERIOD_TODATE = hasil;
        }
        else{
            SESS_PERIOD = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_MONTH_ONLY);
            SESS_PERIOD_TODATE = "";
        }
    }
    void GetData2() {
        database = new DatabaseHandler(getApplicationContext());
        database.openTransaction();
        String[] a = new String[2];
        a[0] = SESS_ZYEAR;
        a[1] = SESS_ZDATE;
        String query = "SELECT ClosingDate FROM tblM_AccountingPeriod " +
                " WHERE  ZYear = ? AND  ClosingDate  < ? "+
                " ORDER BY ClosingDate DESC LIMIT 1";
        tblM_AccountingPeriod dtsource2 = (tblM_AccountingPeriod) database.getDataFirstRaw(query,tblM_AccountingPeriod.TABLE_NAME,a);
        database.closeTransaction();
        if(dtsource2!= null){
            String hasil = FormatDate(dtsource2.getAcc_ClosingDate());
            String Add = AddDay(hasil);
            SESS_PERIOD_FROMDATE = Add;
        }
        else{
            SESS_PERIOD_FROMDATE="";
        }
    }

    private String AddDay(String Value){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(Value));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, 1);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        Value= sdf1.format(c.getTime());
        return Value;
 }
    private String FormatDate(String value){
//            String inputPattern = "dd/MM/yyyy HH:mm:ss";
            String inputPattern = "yyyy-MM-dd";
            String outputPattern = "yyyy-MM-dd";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            Date date = null;
            String dateString= null;

            try {
                date = inputFormat.parse(value);
                dateString = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

     return dateString;
    }

}
