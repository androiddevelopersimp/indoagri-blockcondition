package co.indoagri.blockcondition.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.RKHITEMAdapter;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.handler.GpsHandler;
import co.indoagri.blockcondition.model.Data.BJR;
import co.indoagri.blockcondition.model.Data.BLKPLT;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.RKH.RKH_HEADER;
import co.indoagri.blockcondition.model.RKH.RKH_ITEM;
import co.indoagri.blockcondition.model.Users.Employee;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.services.GPSService;
import co.indoagri.blockcondition.services.GPSTriggerService;

import static co.indoagri.blockcondition.activity.BlockActivity.SESS_PERIOD;
import static co.indoagri.blockcondition.activity.BlockActivity.SESS_ZYEAR;

public class RKHJOBActivity extends BaseActivity implements
RKHITEMAdapter.ItemListener, RKHITEMAdapter.CardDelete, RKHITEMAdapter.CardEdit{

    Toolbar toolbar;
    TextView mTextToolbar;
    Bundle bundle;
    String Method = null;
    List<RKH_ITEM> rkh_items = new ArrayList<RKH_ITEM>();
    RecyclerView recyclerView;
    RKHITEMAdapter adapter;
    RKH_HEADER rkhHeader;
    Activity activity;
    TextView txtDate;
    String RKHDATE = "";
    LinearLayout changePekerja1;
    LinearLayout changePekerja2;
    public static int UBAH_MANDOR = 01;
    public static int UBAH_CLERK = 02;
    public static int UBAH_TIPE_AKTIFITAS = 03;
    public static int UBAH_BLOCK = 04;
    String ActivityType = "";
    String ActivityTypeName = "";
    String ItemBlock ="";
    String ItemHATarget = "";
    String ItemSKU = "";
    String ItemBJR = "";
    double ItemBJRdoubel = 0;
    String ItemTonase = "";
    String ItemJenjang="";
    String ItemDay="";
    String ItemPHL ="";
    EditText edtTxtActivityTYpe ;
    EditText edtTxtBlock ;
    EditText edtTxtTargetHA;
    EditText edtTxtSKU;
    EditText edtTxtBJR;
    EditText edtTxtTonase;
    EditText edtTxtJenjang;
    EditText edtTxtPHL;
    EditText edtTxtDay;
    RelativeLayout chooseType;
    RelativeLayout chooseBlock;
    Button btnSaveEdit;
    DatabaseHandler database = new DatabaseHandler(RKHJOBActivity.this);
    String RkhID = "";
    String Imei = null;
    String companyCode;
    String estate;
    String division;
    String gang;
    String nik;
    private GetDataAsyncTask getDataAsync;
    LinearLayout addAktifitas;
    LinearLayout layoutGender;
    RKH_ITEM rkh_itemBundle;
    String gpsKoordinat = "0.0:0.0";
    double latitude = 0;
    double longitude = 0;
    GPSService gps;
    GpsHandler gpsHandler;
    ImageView iv_delete;
    String LastPeriod = "";
    String LastYear ="";
    @Override
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.rkhjob_layout);
        SetGPS();
        bundle= getIntent().getExtras();
        if(bundle!=null) {
            rkhHeader = (RKH_HEADER) getIntent().getExtras().getParcelable("Data");
            RKH_ITEM dataItem = getIntent().getExtras().getParcelable("DataItem");
            if(dataItem!=null){
                rkh_itemBundle = dataItem;
            }
            Method = bundle.getString("Method");
            RKHDATE = rkhHeader.getRKH_DATE();
            RkhID = rkhHeader.getRKH_ID();
        }
        Log.i("RKHID",String.valueOf(rkhHeader));
        SetPageFromMethod();
        activity = this;
        Imei = getUniqID(this);
    }
    void SetPageFromMethod(){
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        }

        gpsKoordinat = String.valueOf(latitude) + ":" + String.valueOf(longitude);
        LastYear = String.valueOf(getPeriode().getFiscalYear());
        LastPeriod = String.valueOf(getPeriode().getFiscalPeriod());
        companyCode = ViewProfile.getCompanyCode();
        estate = ViewProfile.getEstate();
        division = ViewProfile.getDivisions();
        gang = ViewProfile.getGang();
        nik = ViewProfile.getUserNIK();
        RelativeLayout create = (RelativeLayout) findViewById(R.id.Create);
        RelativeLayout edit = (RelativeLayout)findViewById(R.id.Edit);
        setUpToolbar();

        if(Method.equals("new")){
            create.setVisibility(View.VISIBLE);
            edit.setVisibility(View.GONE);
            setProfileNew();
        }if(Method.equals("edit")){
            setProfileEdit();
            create.setVisibility(View.GONE);
            edit.setVisibility(View.VISIBLE);
        }if(Method.equals("edit-aktifitas")){
            create.setVisibility(View.VISIBLE);
            edit.setVisibility(View.GONE);
            setProfileNew();
        }if(Method.equals("new-aktifitas")){
            create.setVisibility(View.VISIBLE);
            edit.setVisibility(View.GONE);
            setProfileNew();
        }


    }
    void setProfileNew(){
        ItemSKU = getSKU(rkhHeader.getESTATE(),LastYear,LastPeriod,rkhHeader.getGANG());
        ItemPHL= getPHL(rkhHeader.getESTATE(),LastYear,LastPeriod,rkhHeader.getGANG());
        ItemDay = String.valueOf(Integer.parseInt(ItemSKU)+Integer.parseInt(ItemPHL));
        layoutGender = (LinearLayout)findViewById(R.id.layoutGender);
        layoutGender.setVisibility(View.GONE);
        TextView txtMandor = (TextView)findViewById(R.id.txtMandor);
        TextView txtGang = (TextView)findViewById(R.id.txtMandorGang);
        TextView txtNik = (TextView)findViewById(R.id.txtNikMandor);
        TextView txtKrani = (TextView)findViewById(R.id.txtKrani);
        TextView txtKraniGang = (TextView)findViewById(R.id.txtKraniGang);
        TextView txtNikKrani = (TextView)findViewById(R.id.txtNikKrani);
        txtMandor.setText(rkhHeader.getFOREMAN());
        txtGang.setText(rkhHeader.getGANG());
        txtNik.setText(rkhHeader.getNIK_FOREMAN());
        txtKrani.setText(rkhHeader.getCLERK());
        txtKraniGang.setText(rkhHeader.getGANG());
        txtNikKrani.setText(rkhHeader.getNIK_CLERK());
        edtTxtActivityTYpe = (EditText)findViewById(R.id.edtTxtActivityType);
        edtTxtBlock = (EditText)findViewById(R.id.edtTxtBlock);
        edtTxtTargetHA = (EditText)findViewById(R.id.edtTxtTargetHA);
        edtTxtSKU = (EditText)findViewById(R.id.edtTxtSKU);
        edtTxtBJR = (EditText)findViewById(R.id.edtTxtBJR);
        edtTxtPHL = (EditText)findViewById(R.id.edtTxtPHL);
        edtTxtJenjang = (EditText)findViewById(R.id.edtTxtJenjang);
        edtTxtTonase = (EditText)findViewById(R.id.edtTxtTonase);
        edtTxtDay = (EditText)findViewById(R.id.edtTxtDay);
        edtTxtSKU.setText(ItemSKU);
        edtTxtPHL.setText(ItemPHL);
        edtTxtDay.setText(ItemDay);
        iv_delete = (ImageView)findViewById(R.id.iv_delete);
        if(rkhHeader.getNIK_CLERK().isEmpty()){
            iv_delete.setVisibility(View.GONE);
            txtKrani.setText(null);
            txtKraniGang.setText(null);
            txtNikKrani.setText(null);
        }else{
            iv_delete.setVisibility(View.VISIBLE);
        }
        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HapusKrani();
            }
        });
        if(Method.equalsIgnoreCase("edit-aktifitas")){
            TextView txtAktifitasCount = (TextView)findViewById(R.id.txtAktifitasCount);
            txtAktifitasCount.setText("Aktifitas "+rkh_itemBundle.getLINE());
                    ItemBlock = rkh_itemBundle.getBLOCK();
                    ItemHATarget = rkh_itemBundle.getTARGET_OUTPUT();
                    ItemSKU = rkh_itemBundle.getSKU();
                    ItemPHL = rkh_itemBundle.getPHL();
                    ItemTonase = rkh_itemBundle.getTARGET_HASIL();
                    ItemJenjang = rkh_itemBundle.getTARGET_JANJANG();
                    ItemBJR = rkh_itemBundle.getBJR();
            mTextToolbar.setText("Edit Aktifitas "+rkh_itemBundle.getLINE());
            String[] separated = rkh_itemBundle.getACTIVITY().split(";");
            ActivityType = separated[0];
            ActivityTypeName = separated[1];
            SetForm();
            setTonase();
            iv_delete = (ImageView)findViewById(R.id.iv_delete);
            if(rkhHeader.getNIK_CLERK().isEmpty()){
                iv_delete.setVisibility(View.GONE);
                txtKrani.setText(null);
                txtKraniGang.setText(null);
                txtNikKrani.setText(null);
            }else{
                iv_delete.setVisibility(View.VISIBLE);
            }
            iv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   HapusKrani();
                }
            });

        }
        if(Method.equalsIgnoreCase("new-aktifitas")){
            TextView txtAktifitasCount = (TextView)findViewById(R.id.txtAktifitasCount);
            txtAktifitasCount.setText("Aktifitas "+String.valueOf(TotalItem()+1));
            mTextToolbar.setText("Aktifitas Baru ("+String.valueOf(TotalItem()+1)+")");
        }
        btnSaveEdit = (Button)findViewById(R.id.btnSave);
        btnSaveEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SaveVAlidation()){
                    if(Method.equalsIgnoreCase("new")||Method.equalsIgnoreCase("new-aktifitas")){
                        String Lines = TotalItem()> 0 ? String.valueOf(LastLine()+1) : String.valueOf(TotalItem()+1);
                        if(saveData(Lines)){
                            UpdateHeader();
                            onBackPressed();
                        }
                    }
                    if(Method.equalsIgnoreCase("edit-aktifitas")){
                        if(editData(rkh_itemBundle)){
                            UpdateHeader();
                            onBackPressed();
                        }
                    }

                }
            }
        });
        setTonase();
        chooseType = (RelativeLayout)findViewById(R.id.chooseType);
        chooseBlock = (RelativeLayout)findViewById(R.id.chooseBlock);
        chooseBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Class newclass = null;
                String activityString="co.indoagri.blockcondition.activity.RKHBlockActivity";
                try {
                    newclass = Class.forName(activityString);
                } catch (ClassNotFoundException c) {
                    c.printStackTrace();
                }

                if(newclass != null) {
                    Intent nextIntent = new Intent(activity, newclass);
                    nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    nextIntent.putExtra("Data",rkhHeader);
                    nextIntent.putExtra("TOOLBARTITLE","Pilih Block");
                    startActivityForResult(nextIntent, UBAH_BLOCK);
                } else {
                    Toast.makeText(getApplicationContext(),"new class null",Toast.LENGTH_SHORT).show();
                }
            }
        });
        chooseType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Class newclass = null;
                String activityString="co.indoagri.blockcondition.activity.RKHActivityTypeActivity";
                try {
                    newclass = Class.forName(activityString);
                } catch (ClassNotFoundException c) {
                    c.printStackTrace();
                }
                if(newclass != null) {
                    Intent nextIntent = new Intent(activity, newclass);
                    nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    nextIntent.putExtra("Data",rkhHeader);
                    nextIntent.putExtra("TOOLBARTITLE","Pilih Tipe Aktifitas");
                    startActivityForResult(nextIntent, UBAH_TIPE_AKTIFITAS);
                } else {
                    Toast.makeText(getApplicationContext(),"new class null",Toast.LENGTH_SHORT).show();
                }
            }
        });
        changePekerja1 = (LinearLayout)findViewById(R.id.cardChangePekerja1);
        changePekerja2 = (LinearLayout)findViewById(R.id.cardChangePekerja2);
        changePekerja1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Class newclass = null;
                String activityString="co.indoagri.blockcondition.activity.RKHPekerjaActivity";
                try {
                    newclass = Class.forName(activityString);
                } catch (ClassNotFoundException c) {
                    c.printStackTrace();
                }
                if(newclass != null) {
                    Intent nextIntent = new Intent(activity, newclass);
                    nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    nextIntent.putExtra("Data",rkhHeader);
                    nextIntent.putExtra("TOOLBARTITLE","Ubah Pekerja");
                    nextIntent.putExtra("Pekerja","mandor");
                    nextIntent.putExtra("Method","new");
                    startActivityForResult(nextIntent, UBAH_MANDOR);
                } else {
                    Toast.makeText(getApplicationContext(),"new class null",Toast.LENGTH_SHORT).show();
                }
            }
        });
        changePekerja2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Class newclass = null;
                String activityString="co.indoagri.blockcondition.activity.RKHPekerjaActivity";
                try {
                    newclass = Class.forName(activityString);
                } catch (ClassNotFoundException c) {
                    c.printStackTrace();
                }

                if(newclass != null) {
                    Intent nextIntent = new Intent(activity, newclass);
                    nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    nextIntent.putExtra("Data",rkhHeader);
                    nextIntent.putExtra("TOOLBARTITLE","Ubah Pekerja");
                    nextIntent.putExtra("Pekerja","kerani");
                    nextIntent.putExtra("Method","new");
                    startActivityForResult(nextIntent, UBAH_CLERK);
                } else {
                    Toast.makeText(getApplicationContext(),"new class null",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    void setTonase(){
        edtTxtTonase.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String text = s.toString();
                try
                {
                    Double parsedFloat = Double.valueOf(text);
                    setJenjangEdtTxt(parsedFloat);
                } catch (NumberFormatException e)
                {
//                    Log.w(getPackageName(), "Non si puo' parsare '" + text + "' col numero", e);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    void setProfileEdit(){
        layoutGender = (LinearLayout)findViewById(R.id.layoutGender);
        layoutGender.setVisibility(View.GONE);
        mTextToolbar.setText("Edit Aktifitas");
        TextView txtMandor = (TextView)findViewById(R.id.txtMandorEdit);
        TextView txtGang = (TextView)findViewById(R.id.txtMandorGangEdit);
        TextView txtNik = (TextView)findViewById(R.id.txtNikMandorEdit);
        TextView txtKrani = (TextView)findViewById(R.id.txtKraniEdit);
        TextView txtKraniGang = (TextView)findViewById(R.id.txtKraniGangEdit);
        TextView txtNikKrani = (TextView)findViewById(R.id.txtNikKraniEdit);
        txtMandor.setText(rkhHeader.getFOREMAN());
        txtGang.setText(rkhHeader.getGANG());
        txtNik.setText(rkhHeader.getNIK_FOREMAN());
        txtKrani.setText(rkhHeader.getCLERK());
        txtKraniGang.setText(rkhHeader.getGANG());
        txtNikKrani.setText(rkhHeader.getNIK_CLERK());
        btnSaveEdit = (Button)findViewById(R.id.btnSave);
        btnSaveEdit.setVisibility(View.GONE);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        addAktifitas = (LinearLayout)findViewById(R.id.addAktifitas);
        TextView TextButtonsalin = (TextView)findViewById(R.id.txtBtn);
        if(TotalItem()>0){
            TextButtonsalin.setText("Salin Aktifitas");
            addAktifitas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String Lines = TotalItem()> 0 ? String.valueOf(LastLine()+1) : String.valueOf(TotalItem()+1);
                    if(salinData(Lines)){
                        UpdateHeader();
                        overridePendingTransition(R.anim.push_down_in, R.anim.push_up_out);
                        SetData();
                    }
                }
            });
        }if(TotalItem()==0){
            TextButtonsalin.setText("Buat Aktifitas");
            addAktifitas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent reOpen = new Intent (RKHJOBActivity.this, RKHJOBActivity.class);
                    reOpen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    reOpen.putExtra("Data", rkhHeader);
                    reOpen.putExtra("Method","new-aktifitas");
                    startActivity(reOpen);
                    finish();
                }
            });
        }

        if(rkhHeader.getNIK_CLERK().isEmpty()){
            txtKrani.setText(null);
            txtKraniGang.setText(null);
            txtNikKrani.setText(null);
        }else{
        }
        SetData();
    }
    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        txtDate = (TextView)findViewById(R.id.txtDate);
        CheckSetDate(RKHDATE);
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText(getResources().getString(R.string.mainmenu_rkh_job));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Method.equalsIgnoreCase("new")){
                    KonfirmasiSaveToBack();
                }else{
                    onBackPressed();
                }
            }
        });
    }
    @Override
    public void onBackPressed() {
        if(Method.equalsIgnoreCase("edit-aktifitas")){
            Intent reOpen = new Intent (RKHJOBActivity.this, RKHJOBActivity.class);
            reOpen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            reOpen.putExtra("Data", rkhHeader);
            reOpen.putExtra("DataItem", rkh_itemBundle);
            reOpen.putExtra("Method","edit");
            startActivity(reOpen);
            finish();
        }  if(Method.equalsIgnoreCase("new")) {
            KonfirmasiSaveToBack();
        }else{
            Intent intent=new Intent();
            setResult(RESULT_OK, intent);
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
        if (gps != null) {
            gps.stopUsingGPS();
        }
    }
    @Override
    public void onItemClick(RKH_ITEM item, int position) {
    }
    @Override
    public void onCardDelete(final RKH_ITEM item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(RKHJOBActivity.this);
        builder.setTitle("Apakah Akan menyimpan data gang");
        builder.setMessage("Simpan Data ? ");
        // Set the alert dialog yes button click listener
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(deleteDataItem(item)){
                    if(TotalItem()==0){
                        UpdateHeader();
                        onBackPressed();
                    }else{
                        SetData();
                    }
                }
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    @Override
    public void onCardEdit(RKH_ITEM item) {
        Intent reOpen = new Intent (RKHJOBActivity.this, RKHJOBActivity.class);
        reOpen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        reOpen.putExtra("Data", rkhHeader);
        reOpen.putExtra("DataItem", item);
        reOpen.putExtra("Method","edit-aktifitas");
        startActivity(reOpen);
        finish();
    }
    void CheckSetDate(String Dates){
        try{
            Date parsedDate = DateLocal.FORMAT_INPUT.parse(Dates);
            Calendar cal = Calendar.getInstance();
            cal.setTime(parsedDate);
            int days = cal.get(Calendar.DAY_OF_WEEK);
            int dates = cal.get(Calendar.DATE);
            int months = cal.get(Calendar.MONTH);
            int years = cal.get(Calendar.YEAR);

            int hours = cal.get(Calendar.HOUR);
            int minutes = cal.get(Calendar.MINUTE);
            int seconds = cal.get(Calendar.SECOND);
            int amPm = cal.get(Calendar.AM_PM);
            switch (amPm) {
                case Calendar.AM:

                    break;
                case Calendar.PM:
                    hours = hours + 12;
                default:
                    break;
            }
            // txtDate.setText(digitFormat(hours, 2) + ":" + digitFormat(minutes,2) + ":" + digitFormat(seconds,2));
            String hari = "";
            switch (days) {
                case Calendar.SUNDAY:
                    hari = getResources().getString(R.string.minggu);
                    break;
                case Calendar.MONDAY:
                    hari = getResources().getString(R.string.senin);
                    break;
                case Calendar.TUESDAY:
                    hari = getResources().getString(R.string.selasa);
                    break;
                case Calendar.WEDNESDAY:
                    hari = getResources().getString(R.string.rabu);
                    break;
                case Calendar.THURSDAY:
                    hari = getResources().getString(R.string.kamis);
                    break;
                case Calendar.FRIDAY:
                    hari = getResources().getString(R.string.jumat);
                    break;
                case Calendar.SATURDAY:
                    hari = getResources().getString(R.string.sabtu);
                    break;
                default:
                    break;
            }
            String bulan = "";
            switch (months) {
                case Calendar.JANUARY:
                    bulan = getResources().getString(R.string.januari);
                    break;
                case Calendar.FEBRUARY:
                    bulan = getResources().getString(R.string.februari);
                    break;
                case Calendar.MARCH:
                    bulan = getResources().getString(R.string.maret);
                    break;
                case Calendar.APRIL:
                    bulan = getResources().getString(R.string.april);
                    break;
                case Calendar.MAY:
                    bulan = getResources().getString(R.string.mei);
                    break;
                case Calendar.JUNE:
                    bulan = getResources().getString(R.string.juni);
                    break;
                case Calendar.JULY:
                    bulan = getResources().getString(R.string.juli);
                    break;
                case Calendar.AUGUST:
                    bulan = getResources().getString(R.string.agustus);
                    break;
                case Calendar.SEPTEMBER:
                    bulan = getResources().getString(R.string.september);
                    break;
                case Calendar.OCTOBER:
                    bulan = getResources().getString(R.string.oktober);
                    break;
                case Calendar.NOVEMBER:
                    bulan = getResources().getString(R.string.november);
                    break;
                case Calendar.DECEMBER:
                    bulan = getResources().getString(R.string.desember);
                    break;
                default:
                    break;
            }

            txtDate.setText(hari + " , " + dates + " " + bulan + " " + years);

        }catch (Exception e) {}
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == UBAH_MANDOR) {
            if(resultCode == RESULT_OK) {
                if(data.getExtras()!=null){
                    rkhHeader = data.getExtras().getParcelable("Data");
                    SetPageFromMethod();
                }else{
                    SetPageFromMethod();
                }

            }
        }
        if (requestCode == UBAH_CLERK) {
            if(resultCode == RESULT_OK) {
                if(data.getExtras()!=null){
                    rkhHeader = data.getExtras().getParcelable("Data");
                    SetPageFromMethod();
                }else{
                    SetPageFromMethod();
                }

            }
        }
        if (requestCode == UBAH_TIPE_AKTIFITAS) {
            if(resultCode == RESULT_OK) {
                String getAktifitas = new PreferenceManager(getApplicationContext(), Constants.shared_name).getRkhInputtype();
                if(getAktifitas!=null){
                    String[] separated = getAktifitas.split(";");
                    ActivityType = separated[0];
                    ActivityTypeName = separated[1];
                    SetForm();
                }else{
                    SetForm();
                }

            }
        }
        if (requestCode == UBAH_BLOCK) {
            if(resultCode == RESULT_OK) {
                String getBlock = new PreferenceManager(getApplicationContext(), Constants.shared_name).getRkhInputblock();
            if(getBlock!=null){
                ItemHATarget = getTargetHA(getBlock);
                ItemBlock=getBlock;
                SetForm();
            }else{
                SetForm();
            }

            }
        }
    }
    private String getTargetHA(String BlockID){
        String result = "0";
        double HA = 0;
        database.openTransaction();
        BLKPLT datas = (BLKPLT)database.getDataFirst(false, BLKPLT.TABLE_NAME, null,
                BLKPLT.XML_BLOCK + "=?",
                new String [] {BlockID},
                BLKPLT.XML_BLOCK, null, null, "1");
        database.closeTransaction();
        if(datas!=null){
            HA = datas.getHarvested();
            result = String.valueOf(HA);
        }else{
            HA = 0;
            result = String.valueOf(HA);
        }
        return result;
    }
    private Employee getPeriode(){
        database.openTransaction();
        Employee datas = (Employee)database.getDataFirst(false, Employee.TABLE_NAME, null,
                null,
               null,
                null, null, Employee.XML_FISCAL_YEAR +" DESC ,"+Employee.XML_FISCAL_PERIOD+" DESC ", "1");
        database.closeTransaction();
        return datas;
    }
    private String getBJR(String BlockID){
        String result = "";
        database.openTransaction();
        BJR datas = (BJR)database.getDataFirst(false, BJR.TABLE_NAME, null,
                BJR.XML_COMPANY_CODE + "=?" + " and " +
                        BJR.XML_ESTATE + "=?" + " and " +
                        BJR.XML_BLOCK + "=?" ,
                new String [] {rkhHeader.getCOMPANY_CODE(),rkhHeader.getESTATE(),BlockID},
                BJR.XML_BLOCK, null, null, "1");
        database.closeTransaction();
        if(datas!=null){
            ItemBJRdoubel = datas.getBjr();
            result = String.valueOf(ItemBJRdoubel);
        }else{
            ItemBJRdoubel = 0;
            result = String.valueOf(ItemBJRdoubel);
        }
        return result;
    }
    private RKH_HEADER getRefreshDataHeader(){
        String result = "";
        database.openTransaction();
        RKH_HEADER datas = (RKH_HEADER)database.getDataFirst(false, RKH_HEADER.TABLE_NAME, null,
                RKH_HEADER.XML_RKH_ID + "=?",
                new String [] {RkhID},
                null, null, null, null);
        database.closeTransaction();
        return datas;
    }
    private String getSKU(String Estate,String Year,String Period, String gang){
        String result = "";
        int SKU = 0;
        database.openTransaction();
        List<Object> listObject = database.getListData(false, Employee.TABLE_NAME, null,
                Employee.XML_COMPANY_CODE + "=?" + " and " +
                        Employee.XML_ESTATE + "=?" + " and " +
                        Employee.XML_FISCAL_PERIOD + "=?" + " and " +
                        Employee.XML_FISCAL_YEAR + "=?" + " and " +
                        Employee.XML_GANG + "=?" + " and " +
                        Employee.XML_EMP_TYPE + " like '%SKU%'" + " and " +
                        Employee.XML_DIVISION + "=?",
                new String [] {rkhHeader.getCOMPANY_CODE(),rkhHeader.getESTATE(),Period,Year,rkhHeader.getGANG(),rkhHeader.getDIVISION()},
                null, null, null, null);
        database.closeTransaction();
        SKU = listObject.size();
        result = String.valueOf(SKU);
        return result;
    }
    private String getPHL(String Estate,String Year, String Period, String gang){
        String result = "";
        int PHL = 0;
        database.openTransaction();
        List<Object> listObject = database.getListData(false, Employee.TABLE_NAME, null,
                Employee.XML_COMPANY_CODE + "=?" + " and " +
                        Employee.XML_ESTATE + "=?" + " and " +
                        Employee.XML_FISCAL_PERIOD + "=?" + " and " +
                        Employee.XML_FISCAL_YEAR + "=?" + " and " +
                        Employee.XML_GANG + "=?" + " and " +
                        Employee.XML_EMP_TYPE + " like '%PHL%'" + " and " +
                        Employee.XML_DIVISION + "=?",
                new String [] {rkhHeader.getCOMPANY_CODE(),rkhHeader.getESTATE(),Period,Year,rkhHeader.getGANG(),rkhHeader.getDIVISION()},
                null, null, null, null);
        database.closeTransaction();
        PHL = listObject.size();
        result = String.valueOf(PHL);
        return result;
    }
    void SetForm(){
        if(TextUtils.isEmpty(ActivityType)){
            edtTxtActivityTYpe.setText("");
        }else{
            edtTxtActivityTYpe.setText(ActivityType+" - "+ActivityTypeName);
        }
        if(TextUtils.isEmpty(ItemBlock)){
            edtTxtBlock.setText("");
        }else{
            edtTxtBlock.setText(ItemBlock);
        }
        edtTxtSKU.setText(ItemSKU);
        edtTxtBJR.setText(getBJR(ItemBlock));
        edtTxtTargetHA.setText(ItemHATarget);
        edtTxtTonase.setText(ItemTonase);
        edtTxtJenjang.setText(ItemJenjang);
        edtTxtPHL.setText(ItemPHL);
        edtTxtDay.setText(ItemDay);
        edtTxtTonase.setEnabled(true);
    }
    void setJenjangEdtTxt(Double Tonase){
        if(Tonase > ItemBJRdoubel){
            double TonaseDouble = Tonase;
            double JenjangValue = TonaseDouble/ItemBJRdoubel;
            DecimalFormat df=new DecimalFormat("0.00");
            String formate = df.format(JenjangValue);
            ItemBJR = edtTxtBJR.getText().toString();
            edtTxtJenjang.setText(formate.replace(',', '.'));
            ItemJenjang=formate;
            ItemTonase=String.valueOf(Tonase);
        }else{
            double TonaseDouble = Tonase;
            double JenjangValue = TonaseDouble/ItemBJRdoubel;
            DecimalFormat df=new DecimalFormat("0.00");
            String formate = df.format(JenjangValue);
            edtTxtJenjang.setText(formate.replace(',', '.'));
            ItemBJR = edtTxtBJR.getText().toString();
            ItemJenjang=formate;
            ItemTonase=String.valueOf(Tonase);
        }
    }
    private boolean SaveVAlidation(){
        boolean value = false;
        if (edtTxtActivityTYpe.getText().toString().isEmpty()){
            edtTxtActivityTYpe.setError("Activity Type tidak boleh kosong");
        }else{
            edtTxtActivityTYpe.setError(null);
            if (edtTxtBlock.getText().toString().isEmpty()){
                edtTxtBlock.setError("Block tidak boleh kosong.");
            }else{
                edtTxtBlock.setError(null);
                if (edtTxtTargetHA.getText().toString().isEmpty()){
                    edtTxtTargetHA.setError("HA tidak boleh kosong.");
                }else{
                    edtTxtTargetHA.setError(null);
                    if (edtTxtSKU.getText().toString().isEmpty()){
                        edtTxtSKU.setError("HA tidak boleh kosong.");
                    }else{
                        edtTxtSKU.setError(null);
                        if (edtTxtPHL.getText().toString().isEmpty()){
                            edtTxtPHL.setError("HA tidak boleh kosong.");
                        }else{
                            edtTxtPHL.setError(null);
                            if (edtTxtTonase.getText().toString().isEmpty()){
                                edtTxtTonase.setError("Tonase tidak boleh kosong.");
                            }else{
                                edtTxtTonase.setError(null);
                                if (edtTxtBJR.getText().toString().isEmpty()){
                                    edtTxtBJR.setError("HA tidak boleh kosong.");
                                }else{
                                    edtTxtBJR.setError(null);
                                    if (edtTxtJenjang.getText().toString().isEmpty()){
                                        edtTxtJenjang.setError("HA tidak boleh kosong.");
                                    }else{
                                        edtTxtJenjang.setError(null);
                                        if (edtTxtDay.getText().toString().isEmpty()){
                                            edtTxtDay.setError("HK tidak boleh kosong.");
                                        }else{
                                            edtTxtDay.setError(null);
                                            if (Double.parseDouble(edtTxtTargetHA.getText().toString())>Double.parseDouble(ItemHATarget)){
                                                edtTxtTargetHA.setError("HA tidak boleh lebih dari yang ditentukan .");
                                                setDialog();
                                            }else{
                                                edtTxtTargetHA.setError(null);
                                                value=true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return value;
    }
    void setDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(RKHJOBActivity.this);
        builder.setTitle("Nilai HA tidak boleh lebih dari "+ItemHATarget);
        builder.setMessage("Silahkan diulang ");
        // Set the alert dialog yes button click listener
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private boolean saveData(String line){
        boolean hasil = false;
        long RowId = 0;
        RkhID = (TextUtils.isEmpty(RkhID)) ? new DateLocal(new Date()).getDateString(DateLocal.FORMAT_ID) + Imei.substring(8, Imei.length() - 1) : RkhID;
//        Calendar cal = Calendar.getInstance(Locale.getDefault());
//        long TodayDate = cal.getTimeInMillis();
        String filterDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
     //   long TodayDate = new Date().getTime();
        try{
            long TodayDate = new Date().getTime();
            database.openTransaction();
            RKH_ITEM setData = new RKH_ITEM(RkhID,
                    line,
                    ActivityType+";"+ActivityTypeName,
                    ItemBlock,
                    ItemHATarget,
                    ItemSKU,
                    ItemPHL,
                    ItemTonase,
                    ItemJenjang,
                    ItemBJR,
                    gpsKoordinat,
                    0,
                    TodayDate,
                    nik,
                    TodayDate,
                    nik);
            RowId = database.setData(setData);
            database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
            if(RowId>0){
                hasil = true;
            }
        }
        return hasil;
    }
    private boolean salinData(String line){

        boolean hasil = false;
        long RowId = 0;
        RKH_ITEM itemRKH = LastItem();
        RkhID = (TextUtils.isEmpty(RkhID)) ? new DateLocal(new Date()).getDateString(DateLocal.FORMAT_ID) + Imei.substring(8, Imei.length() - 1) : RkhID;
        String filterDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
        //long TodayDate = new Date().getTime();
        try{
            long TodayDate = new Date().getTime();
            database.openTransaction();
            RKH_ITEM setData = new RKH_ITEM(RkhID,
                    line,
                    itemRKH.getACTIVITY(),
                    itemRKH.getBLOCK(),
                    itemRKH.getTARGET_OUTPUT(),
                    itemRKH.getSKU(),
                    itemRKH.getPHL(),
                    itemRKH.getTARGET_HASIL(),
                    itemRKH.getTARGET_JANJANG(),
                    itemRKH.getBJR(),
                    gpsKoordinat,
                    0,
                    TodayDate,
                    nik,
                    TodayDate,
                    nik);
            RowId = database.setData(setData);
            database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
            if(RowId>0){
                hasil = true;
            }
        }
        return hasil;
    }
    private boolean editData(RKH_ITEM rkh_item){
        boolean hasil = false;
        long RowId = 0;
        String filterDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
     //   long TodayDate = new Date().getTime();
        try{
            long TodayDate = new Date().getTime();
            database.openTransaction();
            RKH_ITEM setDAta = new RKH_ITEM(rkh_item.getRKH_ID(),
                    rkh_item.getLINE(),
                    ActivityType+";"+ActivityTypeName,
                    ItemBlock,
                    ItemHATarget,
                    ItemSKU,
                    ItemPHL,
                    ItemTonase,
                    ItemJenjang,
                    ItemBJR,
                    gpsKoordinat,
                    0,
                    rkh_item.getCREATED_DATE(),
                    nik,
                    TodayDate,
                    nik);
            RowId = database.updateData(setDAta,
                    RKH_ITEM.XML_RKH_ID + "=? and "+
                    RKH_ITEM.XML_LINE + "=?",
                    new String [] {rkh_item.getRKH_ID(),rkh_item.getLINE()});
            database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
            if(RowId>0){
                hasil = true;
            }
        }
        return hasil;
    }
    private boolean deleteDataItem(RKH_ITEM rkh_item){
        boolean hasil = false;
        long RowId = 0;
        String CreatedDateRKH = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT);
        try{
            database.openTransaction();
            RowId = database.deleteData(RKH_ITEM.TABLE_NAME,
                            RKH_ITEM.XML_RKH_ID+ "=? AND "+
                                    RKH_ITEM.XML_LINE+ "=? ",
                new String [] {rkh_item.getRKH_ID(),rkh_item.getLINE()});
            database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
            if(RowId>0){
                hasil = true;
            }
        }
        return hasil;
    }
    private boolean deleteDataHeader(){
        boolean hasil = false;
        long RowId = 0;
        try{
            database.openTransaction();
            RowId = database.deleteData(RKH_HEADER.TABLE_NAME,
                            RKH_HEADER.XML_RKH_ID+ "=? ",
                    new String [] {rkhHeader.getRKH_ID()});
            database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
            if(RowId>0){
                hasil = true;
            }
        }
        return hasil;
    }
    private boolean deleteKrani(RKH_HEADER rkh_header){
        boolean hasil = false;
        long RowId = 0;
        try{
            database.openTransaction();
            RowId = database.updateDataSQL(RKH_HEADER.TABLE_NAME,"UPDATE "+ RKH_HEADER.TABLE_NAME+
                            " SET "+RKH_HEADER.XML_NIK_CLERK +" = ?," +
                            RKH_HEADER.XML_CLERK+" =? "+
                            "WHERE "+RKH_HEADER.XML_RKH_ID+" =? ",
                    new Object [] {"","",rkh_header.getRKH_ID()});
            database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
            if(RowId>0){
                hasil = true;
            }
        }
        return hasil;
    }
    private String getUniqID(Context context){
        String number = null;
        if(Constants.const_DeviceID != null || !Constants.const_DeviceID.equals("")){
            number =Constants.const_DeviceID;
        }else{

            number = Constants.const_IMEI;
        }
        return number;
    }
    private int TotalItem(){
        int Total = 0;
        database.openTransaction();
        String[] a = new String[1];
        a[0] = RkhID;
        String query = "Select * from "+RKH_ITEM.TABLE_NAME+" where "+RKH_ITEM.XML_RKH_ID+" = ? ";
        Total = database.getTaskCount(query,a);
        Log.i("Total Item ",String.valueOf(Total));
        database.commitTransaction();
        database.closeTransaction();
        return Total;
    }
    private RKH_ITEM LastItem(){
        database.openTransaction();
        RKH_ITEM rkh_item = (RKH_ITEM)database.getDataFirst(true, RKH_ITEM.TABLE_NAME, null,
                RKH_ITEM.XML_RKH_ID + "=?",
                new String [] {RkhID},
                null, null, " cast( "+RKH_ITEM.XML_LINE+" as  int ) DESC", "1");
        database.commitTransaction();
        database.closeTransaction();
        return rkh_item;
    }
    private int LastLine(){
         int Line = 0;
        database.openTransaction();
        RKH_ITEM rkh_item = (RKH_ITEM)database.getDataFirst(true, RKH_ITEM.TABLE_NAME, null,
                RKH_ITEM.XML_RKH_ID + "=?",
                new String [] {RkhID},
                null, null, " cast( "+RKH_ITEM.XML_LINE+" as  int ) DESC", "1");
        database.commitTransaction();
        database.closeTransaction();
        if(rkh_item!=null){
            Line = Integer.parseInt(rkh_item.getLINE());
        }else{
            Line =0;
        }

        return Line;
    }
    void SetData(){

        companyCode = ViewProfile.getCompanyCode();
        estate = ViewProfile.getEstate();
        division = ViewProfile.getDivisions();
        gang = ViewProfile.getGang();
        nik = ViewProfile.getUserNIK();
        rkh_items.clear();
        setAdapter();
        getDataAsync = new GetDataAsyncTask(false, RKH_ITEM.TABLE_NAME, null,
                        RKH_ITEM.XML_RKH_ID+ "=? ",
                new String [] {RkhID},
                null, null, null, null);
        getDataAsync.execute();
    }
    private class GetDataAsyncTask extends AsyncTask<Void, List<RKH_ITEM>, List<RKH_ITEM>> {
        boolean distinct;
        String tableName;
        String [] columns;
        String whereClause;
        String [] whereArgs;
        String groupBy;
        String having;
        String orderBy;
        String limit;

        public GetDataAsyncTask(boolean distinct, String tableName, String [] columns, String whereClause, String [] whereParams,
                                String groupBy, String having, String orderBy, String limit){
            this.distinct = distinct;
            this.tableName = tableName;
            this.columns = columns;
            this.whereClause = whereClause;
            this.whereArgs = whereParams;
            this.groupBy = groupBy;
            this.having = having;
            this.orderBy = orderBy;
            this.limit = limit;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected List<RKH_ITEM> doInBackground(Void... voids) {
            DatabaseHandler database = new DatabaseHandler(RKHJOBActivity.this);
            List<RKH_ITEM> listTemp = new ArrayList<RKH_ITEM>();
            List<Object> listObject;

            database.openTransaction();
            listObject = database.getListData(distinct, tableName, columns, whereClause, whereArgs, groupBy, having, orderBy, limit);
            database.closeTransaction();

            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    RKH_ITEM rkh_item = (RKH_ITEM) listObject.get(i);

                    listTemp.add(rkh_item);
                }
            }

            return listTemp;
        }

        @Override
        protected void onPostExecute(List<RKH_ITEM> listTemp) {
            super.onPostExecute(listTemp);
            for(int i = 0; i < listTemp.size(); i++){
                RKH_ITEM rkh_item = (RKH_ITEM) listTemp.get(i);
                adapter.addData(rkh_item);
            }

        }
    }
    void setAdapter(){
        adapter = new RKHITEMAdapter(this, rkh_items, this,this,this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }
    void HapusKrani(){
        AlertDialog.Builder builder = new AlertDialog.Builder(RKHJOBActivity.this);
        builder.setTitle("Apakah Akan menyimpan data gang");
        builder.setMessage("Simpan Data ? ");
        // Set the alert dialog yes button click listener
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(deleteKrani(rkhHeader)){
                    rkhHeader = getRefreshDataHeader();
                    SetPageFromMethod();
                }
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void KonfirmasiSaveToBack(){
        if(TotalItem()==0){
            AlertDialog.Builder builder = new AlertDialog.Builder(RKHJOBActivity.this);
            builder.setTitle("Anda Belum melakukan Simpan Aktifitas");
            builder.setMessage("Melanjutkan tanpa Menyimpan ? ");
            // Set the alert dialog yes button click listener
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(deleteDataHeader()){
                        Intent intent=new Intent();
                        setResult(RESULT_OK, intent);
                        finish();
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    }
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }else{
            Intent intent=new Intent();
            setResult(RESULT_OK, intent);
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    void SetGPS(){
        gps = new GPSService(RKHJOBActivity.this);
        gpsHandler = new GpsHandler(RKHJOBActivity.this);
        GPSTriggerService.rkhjobActivity = this;
        gpsHandler.startGPS();
    }
//    private String getLocation(){
//        if(gps != null){
//            Location location = gps.getLocation();
//            if(location != null){
//                latitude = location.getLatitude();
//                longitude = location.getLongitude();
//
//                gpsKoordinat = latitude + ":" + longitude;
//            }else{
//                if (gps.canGetLocation()) {
//                    latitude = gps.getLatitude();
//                    longitude = gps.getLongitude();
//                }
//                gpsKoordinat = latitude + ":" + longitude;
//            }
//        }else{
//            if (gps.canGetLocation()) {
//                latitude = gps.getLatitude();
//                longitude = gps.getLongitude();
//            }
//            gpsKoordinat = latitude + ":" + longitude;
//        }
//
//        return gpsKoordinat;
//    }

    public void updateGpsKoordinat(Location location){
        if(location != null){
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            gpsKoordinat = latitude + ":" + longitude;
        }
    }

    private boolean UpdateHeader(){
        boolean hasil = false;
        long RowId = 0;
     //  String filterDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT);

        try{
            long TodayDate = new Date().getTime();
            database.openTransaction();
            RKH_HEADER setHeader = new RKH_HEADER(Imei,
                    rkhHeader.getCOMPANY_CODE(),
                    rkhHeader.getESTATE(),
                    rkhHeader.getRKH_DATE(),
                    rkhHeader.getDIVISION(),
                    rkhHeader.getGANG(),
                    rkhHeader.getNIK_FOREMAN(),
                    rkhHeader.getFOREMAN(),
                    rkhHeader.getNIK_CLERK(),
                    rkhHeader.getCLERK(),
                    gpsKoordinat,
                    0,
                    TodayDate,
                    nik,
                    TodayDate,
                    nik,
                    rkhHeader.RKH_ID);
            database.updateData(setHeader,
                    RKH_HEADER.XML_RKH_ID + "=? and "+
                            RKH_HEADER.XML_RKH_DATE + "=?",
                    new String [] {rkhHeader.getRKH_ID(),RKHDATE});
            database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
            if(RowId>0){
                hasil = true;
            }
        }
        return hasil;
    }
}
