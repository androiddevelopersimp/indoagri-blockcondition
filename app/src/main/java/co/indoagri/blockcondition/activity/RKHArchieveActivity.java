package co.indoagri.blockcondition.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.RKHArchieveAdapter;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.model.DateLocal;
import co.indoagri.blockcondition.model.RKH.RKH_HEADER;
import co.indoagri.blockcondition.routines.Constants;

public class RKHArchieveActivity extends AppCompatActivity implements RKHArchieveAdapter.ItemListener, RKHArchieveAdapter.ItemChooseListener, SearchView.OnQueryTextListener {
    private SearchView searchView;
    Toolbar toolbar;
    TextView mTextToolbar,TxtsaveToolbar,TxtToday;
    Bundle bundle;
    ArrayList<RKH_HEADER> rkh_headers = new ArrayList<RKH_HEADER>();
    RecyclerView recyclerView;
    RKHArchieveAdapter adapter;
    String companyCode;
    String division;
    String gang;
    String estate;
    GetDataAsyncTask getDataAsync;
    String InputTanggal;
    public static boolean BtnSaveVisible = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.rkharchieve_layout);
        setUpToolbar();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        searchView = findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(this);
        initEmployee();
    }

    void setUpToolbar() {
        bundle= getIntent().getExtras();
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        TxtsaveToolbar = (findViewById(R.id.txt_SaveToolbar));
        TxtsaveToolbar.setVisibility(View.GONE);
        TxtToday = (findViewById(R.id.txt_Today));
        TxtsaveToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveDataAndBack();
            }
        });
        TxtToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputTanggal = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
                SaveDataAndBack();
            }
        });
        mTextToolbar.setText(getResources().getString(R.string.alihkan_mandor));
        if(bundle!=null) {
            String Title  = bundle.getString("TOOLBARTITLE");
            mTextToolbar.setText(Title);
        }else{
            mTextToolbar.setText(getResources().getString(R.string.alihkan_mandor));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    void initEmployee(){
        new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputrkhdate(null);
        companyCode = ViewProfile.getCompanyCode();
        estate = ViewProfile.getEstate();
        division = ViewProfile.getDivisions();
        gang = ViewProfile.getGang();
        adapter = new RKHArchieveAdapter(this, rkh_headers,this,this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        getData();
    }
    private void getData(){
        getDataAsync = new GetDataAsyncTask(false, RKH_HEADER.TABLE_NAME, null,
                RKH_HEADER.XML_DIVISION+ "=? and "+
                RKH_HEADER.XML_ESTATE+ "=? ",
                new String [] {division,estate},
                RKH_HEADER.XML_RKH_DATE, null, RKH_HEADER.XML_RKH_DATE+" DESC ", null);
        getDataAsync.execute();
    }

    @Override
    public void onItemClick(RKH_HEADER item, int position) {

    }

    @Override
    public void onItemChoose(RKH_HEADER item, int position) {
        BtnSaveVisible = true;
        InitialbtnSave();
        TxtToday.setVisibility(View.GONE);
        InputTanggal = item.getRKH_DATE();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        BtnSaveVisible = false;
        InitialbtnSave();
        adapter.getFilter().filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        BtnSaveVisible = false;
        InitialbtnSave();
        adapter.getFilter().filter(newText);
        return false;
    }


    private class GetDataAsyncTask extends AsyncTask<Void, ArrayList<RKH_HEADER>, ArrayList<RKH_HEADER>> implements SearchView.OnQueryTextListener {
        boolean distinct;
        String tableName;
        String [] columns;
        String whereClause;
        String [] whereArgs;
        String groupBy;
        String having;
        String orderBy;
        String limit;
        public GetDataAsyncTask(boolean distinct, String tableName, String [] columns, String whereClause, String [] whereParams,
                                String groupBy, String having, String orderBy, String limit){
            this.distinct = distinct;
            this.tableName = tableName;
            this.columns = columns;
            this.whereClause = whereClause;
            this.whereArgs = whereParams;
            this.groupBy = groupBy;
            this.having = having;
            this.orderBy = orderBy;
            this.limit = limit;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected ArrayList<RKH_HEADER> doInBackground(Void... voids) {
            DatabaseHandler database = new DatabaseHandler(RKHArchieveActivity.this);
            ArrayList<RKH_HEADER> listTemp = new ArrayList<RKH_HEADER>();
            List<Object> listObject;
            database.openTransaction();
            listObject = database.getListData(distinct, tableName, columns, whereClause, whereArgs, groupBy, having, orderBy, limit);
            database.closeTransaction();
            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    RKH_HEADER empNew = (RKH_HEADER) listObject.get(i);
                    boolean foundFilter = false;
                    boolean foundCurrent = false;
                    if(rkh_headers.size() > 0){
                        for(int x = 0; x < rkh_headers.size(); x++){
                            RKH_HEADER empCur = (RKH_HEADER) rkh_headers.get(x);

                            if(empNew.getRKH_ID().equals(empCur.getRKH_ID())){
                                foundCurrent = true;
                                break;
                            }
                        }
                    }

                    if(!foundFilter && !foundCurrent){
                        listTemp.add(empNew);
                    }
                }
            }

            return listTemp;
        }

        @Override
        protected void onPostExecute(ArrayList<RKH_HEADER> lstTemp) {
            super.onPostExecute(lstTemp);
            for(int i = 0; i < lstTemp.size(); i++){
                RKH_HEADER rkh_header = (RKH_HEADER) lstTemp.get(i);
                adapter.addData(rkh_header);
            }

        }

        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        DisplayDialogSave();
    }

    void DisplayDialogSave(){
        if(InputTanggal==null){
            InputTanggal = null;
            Intent intent=new Intent();
            setResult(RESULT_OK, intent);
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }else if(InputTanggal!=null){
            AlertDialog.Builder builder = new AlertDialog.Builder(RKHArchieveActivity.this);
            builder.setTitle("Apakah Akan menyimpan data gang");
            builder.setMessage("Simpan Data ? ");
            // Set the alert dialog yes button click listener
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SaveDataAndBack();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    NotSaveDataAndBack();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    void NotSaveDataAndBack(){
        new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputrkhdate(null);
        InputTanggal = null;
        Intent intent=new Intent();
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    void SaveDataAndBack(){
        new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputrkhdate(InputTanggal);
        Intent intent=new Intent();
        intent.putExtra("rkhdate",InputTanggal);
        InputTanggal = null;
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    void InitialbtnSave(){
        if(BtnSaveVisible){
            TxtsaveToolbar.setVisibility(View.VISIBLE);
        }else{
            TxtsaveToolbar.setVisibility(View.GONE);
        }
    }


}
