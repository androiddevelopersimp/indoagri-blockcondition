package co.indoagri.blockcondition.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.dialog.DialogNotification;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.handler.FileXMLHandler;
import co.indoagri.blockcondition.handler.FolderHandler;
import co.indoagri.blockcondition.handler.GpsHandler;
import co.indoagri.blockcondition.handler.ParsingHandler;
import co.indoagri.blockcondition.listener.DialogNotificationListener;
import co.indoagri.blockcondition.model.Data.BJR;
import co.indoagri.blockcondition.model.Data.BLKSBC;
import co.indoagri.blockcondition.model.Data.BLKSBCDetail;
import co.indoagri.blockcondition.model.Data.BLKSUGC;
import co.indoagri.blockcondition.model.Data.BlockHdrc;
import co.indoagri.blockcondition.model.Data.DayOff;
import co.indoagri.blockcondition.model.Data.MasterDownload;
import co.indoagri.blockcondition.model.Data.Penalty;
import co.indoagri.blockcondition.model.Data.RunningAccount;
import co.indoagri.blockcondition.model.Data.SKB;
import co.indoagri.blockcondition.model.Data.SPBSDestination;
import co.indoagri.blockcondition.model.Data.SPBSRunningNumber;
import co.indoagri.blockcondition.model.Data.SPTARunningNumber;
import co.indoagri.blockcondition.model.Data.Vendor;
import co.indoagri.blockcondition.model.Data.VersionDB;
import co.indoagri.blockcondition.model.Data.tblM_AccountingPeriod;
import co.indoagri.blockcondition.model.Data.tblM_BlockConditionScore;
import co.indoagri.blockcondition.model.Data.tblT_BlockConditionScore;
import co.indoagri.blockcondition.model.FileXML;
import co.indoagri.blockcondition.model.MessageStatus;
import co.indoagri.blockcondition.model.RKH.RKH_ACTIVITY_TYPE;
import co.indoagri.blockcondition.model.Users.AbsentType;
import co.indoagri.blockcondition.model.Users.DivisionAssistant;
import co.indoagri.blockcondition.model.Users.Employee;
import co.indoagri.blockcondition.model.Users.GroupHead;
import co.indoagri.blockcondition.model.Users.UserApp;
import co.indoagri.blockcondition.model.Users.UserLogin;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.utils.MarshMallowPermission;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class SplashScreenActivity extends BaseActivity implements DialogNotificationListener {
    private static final int REQUEST_WRITE_STORAGE_REQUEST_CODE = 112;
    private DialogProgress dialogProgress;
    private MarshMallowPermission marshMallowPermission;
    DatabaseHandler database = new DatabaseHandler(SplashScreenActivity.this);
    ReadDataAsynctask readDataAsync;
    Constants constants;
    int REQUEST_CODE_PERMISSION = 99;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getConfig();
        marshMallowPermission = new MarshMallowPermission(SplashScreenActivity.this);
        constants = new Constants();
        constants.initHandler();
        setTheme(R.style.AppTheme_NoActionBar);
        setContentView(R.layout.activity_splashscreen);
        new PreferenceManager(SplashScreenActivity.this, Constants.shared_name).setSSYear(null);
        new PreferenceManager(SplashScreenActivity.this,Constants.shared_name).setSSPeriode(null);
        new PreferenceManager(SplashScreenActivity.this,Constants.shared_name).setSSBlock(null);
        new PreferenceManager(SplashScreenActivity.this,Constants.shared_name).setSSPhase(null);
        if (!marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForExternalStorage(MarshMallowPermission.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_BY_LOAD_PROFILE);
        }else{
//            if (Build.VERSION.SDK_INT >= 23)
//                ensurePermissions(
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE
//                );
            RunningCreate();
        }

}

    @Override
    public void onOK(boolean is_finish) {
        if(is_finish){
            closeAllActivities();
        }
    }
    public void onBackPressed() {
        super.onBackPressed();

        if(readDataAsync != null && readDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            readDataAsync.cancel(true);
        }
    }
    private class ReadDataAsynctask extends AsyncTask<Void, Void, List<MessageStatus>> {
        FolderHandler folderHandler;

        public ReadDataAsynctask(FolderHandler folderHandler) {
            this.folderHandler = folderHandler;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(!isFinishing()){
                dialogProgress = new DialogProgress(SplashScreenActivity.this, getResources().getString(R.string.wait));
                dialogProgress.show();
            }
        }

        @Override
        protected List<MessageStatus> doInBackground(Void... voids) {
            FileXMLHandler fileMasterHandler = new FileXMLHandler(SplashScreenActivity.this);
            List<FileXML> lstMaster;
            List<MessageStatus> lstMsg = new ArrayList<MessageStatus>();

            lstMaster = fileMasterHandler.getFiles(folderHandler.getFileMasterNew(), folderHandler.getFileMasterBackup(), ".xml");

            if(lstMaster.size() > 0){
                lstMsg = new ParsingHandler(SplashScreenActivity.this).ParsingXML(lstMaster);
            }

            return lstMsg;
        }

        @Override
        protected void onPostExecute(List<MessageStatus> lstMsg) {
            super.onPostExecute(lstMsg);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            if(lstMsg.size() > 0){
                String msg = "";

                for(int i = 0; i < lstMsg.size(); i++){
                    MessageStatus msgStatus = (MessageStatus) lstMsg.get(i);

                    if(msgStatus != null){
                        msg = msg + "\n" + msgStatus.getMenu() + ": " + msgStatus.getMessage();
                    }
                }

                Toast.makeText(SplashScreenActivity.this, msg, Toast.LENGTH_LONG).show();
            }

            database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            if(userLogin != null){
                long today = new Date().getTime();

                database.openTransaction();
                UserApp userApp = (UserApp) database.getDataFirst(false, UserApp.TABLE_NAME, null,
                        UserApp.XML_NIK + "=?" + " and " +
                                UserApp.XML_VALID_TO + ">=?",
                        new String [] {userLogin.getNik(), String.valueOf(today)},
                        null, null, null, null);
                database.closeTransaction();

                if(userApp != null){
                    Intent intent = new Intent(SplashScreenActivity.this, MenuActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }else{
                    Intent intent = new Intent(SplashScreenActivity.this, MenuActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }else{
                Intent intent = new Intent(SplashScreenActivity.this, AuthActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }

            animOnFinish();
        }
    }


    private void initMaster(){
        List<MasterDownload> lstMaster = new ArrayList<MasterDownload>();
        lstMaster.add(new MasterDownload(0, AbsentType.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, BJR.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, BLKSBC.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, BLKSBCDetail.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, BlockHdrc.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, DayOff.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, DivisionAssistant.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, Employee.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, SKB.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, SPBSRunningNumber.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, UserApp.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, RunningAccount.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, Penalty.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, SPBSDestination.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, SPTARunningNumber.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, Vendor.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, BLKSUGC.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, GroupHead.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, tblM_AccountingPeriod.ALIAS, "", 0, 0));
        lstMaster.add(new MasterDownload(0, RKH_ACTIVITY_TYPE.ALIAS, "", 0, 0));

        for(int i = 0; i < lstMaster.size(); i++){
            try{
                MasterDownload mdNew = (MasterDownload) lstMaster.get(i);
                String name = mdNew.getName();

                database.openTransaction();

                MasterDownload mdCurr = (MasterDownload) database.getDataFirst(false, MasterDownload.TABLE_NAME, null,
                        MasterDownload.XML_NAME + "=?",
                        new String [] {name},
                        null, null, null, null);

                if(mdCurr == null){
                    database.setData(mdNew);
                }

                database.commitTransaction();
            }catch(SQLiteException e){
                e.printStackTrace();
                database.closeTransaction();
            }finally{
                database.closeTransaction();
            }
        }
    }

    private void InsertMasterScore(){
        int deleted =0;
        database.openTransaction();
        deleted = database.deleteData(tblM_BlockConditionScore.TABLE_NAME,null,null);
        tblM_BlockConditionScore scoreAdd1;
        tblM_BlockConditionScore scoreAdd2;
        tblM_BlockConditionScore scoreAdd3;
        tblM_BlockConditionScore scoreAdd4;
        tblM_BlockConditionScore scoreAdd5;
        tblM_BlockConditionScore scoreAdd6;
        tblM_BlockConditionScore scoreAdd7;
        tblM_BlockConditionScore scoreAdd8;
        tblM_BlockConditionScore scoreAdd9;
        tblM_BlockConditionScore scoreAdd10;
        tblM_BlockConditionScore scoreAdd11;
        tblM_BlockConditionScore scoreAdd12;

        if(deleted>0){
            scoreAdd1 = new tblM_BlockConditionScore("1",
                    "2018-01-01","9999-12-31","1",0,0,1,0,0);
            scoreAdd2 = new tblM_BlockConditionScore("1",
                    "2018-01-01","9999-12-31","2",0,0,2,0,0);
            scoreAdd3 = new tblM_BlockConditionScore("1",
                    "2018-01-01","9999-12-31","3",0,0,3,0,0);
            scoreAdd4 = new tblM_BlockConditionScore("2",
                    "2018-01-01","9999-12-31","1",0,1.67,1,0,0);
            scoreAdd5 = new tblM_BlockConditionScore("2",
                    "2018-01-01","9999-12-31","2",1.68,2.34,2,0,0);
            scoreAdd6 = new tblM_BlockConditionScore("2",
                    "2018-01-01","9999-12-31","3",2.35,9,3,0,0);
            scoreAdd7 = new tblM_BlockConditionScore("3",
                    "2018-01-01","9999-12-31","1",0,0,1,0,0);
            scoreAdd8 = new tblM_BlockConditionScore("3",
                    "2018-01-01","9999-12-31","2",0,0,2,0,0);
            scoreAdd9 = new tblM_BlockConditionScore("3",
                    "2018-01-01","9999-12-31","3",0,0,3,0,0);
            scoreAdd10 = new tblM_BlockConditionScore("4",
                    "2018-01-01","9999-12-31","1",0,7000,1,0,0);
            scoreAdd11 = new tblM_BlockConditionScore("4",
                    "2018-01-01","9999-12-31","2",7001,8500,2,0,0);
            scoreAdd12 = new tblM_BlockConditionScore("4",
                    "2018-01-01","9999-12-31","3",8501,10000,3,0,0);
            database.setData(scoreAdd1);
            database.setData(scoreAdd2);
            database.setData(scoreAdd3);
            database.setData(scoreAdd4);
            database.setData(scoreAdd5);
            database.setData(scoreAdd6);
            database.setData(scoreAdd7);
            database.setData(scoreAdd8);
            database.setData(scoreAdd9);
            database.setData(scoreAdd10);
            database.setData(scoreAdd11);
            database.setData(scoreAdd12);
            database.commitTransaction();
        }
        if(deleted==0){
            scoreAdd1 = new tblM_BlockConditionScore("1",
                    "2018-01-01","9999-12-31","1",0,0,100,0,0);
            scoreAdd2 = new tblM_BlockConditionScore("1",
                    "2018-01-01","9999-12-31","2",0,0,200,0,0);
            scoreAdd3 = new tblM_BlockConditionScore("1",
                    "2018-01-01","9999-12-31","3",0,0,300,0,0);
            scoreAdd4 = new tblM_BlockConditionScore("2",
                    "2018-01-01","9999-12-31","1",0,167,100,0,0);
            scoreAdd5 = new tblM_BlockConditionScore("2",
                    "2018-01-01","9999-12-31","2",168,234,200,0,0);
            scoreAdd6 = new tblM_BlockConditionScore("2",
                    "2018-01-01","9999-12-31","3",235,900,300,0,0);
            scoreAdd7 = new tblM_BlockConditionScore("3",
                    "2018-01-01","9999-12-31","1",0,0,100,0,0);
            scoreAdd8 = new tblM_BlockConditionScore("3",
                    "2018-01-01","9999-12-31","2",0,0,200,0,0);
            scoreAdd9 = new tblM_BlockConditionScore("3",
                    "2018-01-01","9999-12-31","3",0,0,300,0,0);
            scoreAdd10 = new tblM_BlockConditionScore("4",
                    "2018-01-01","9999-12-31","1",0,7000,100,0,0);
            scoreAdd11 = new tblM_BlockConditionScore("4",
                    "2018-01-01","9999-12-31","2",7001,8500,200,0,0);
            scoreAdd12 = new tblM_BlockConditionScore("4",
                    "2018-01-01","9999-12-31","3",8501,10000,300,0,0);
            database.setData(scoreAdd1);
            database.setData(scoreAdd2);
            database.setData(scoreAdd3);
            database.setData(scoreAdd4);
            database.setData(scoreAdd5);
            database.setData(scoreAdd6);
            database.setData(scoreAdd7);
            database.setData(scoreAdd8);
            database.setData(scoreAdd9);
            database.setData(scoreAdd10);
            database.setData(scoreAdd11);
            database.setData(scoreAdd12);
            database.commitTransaction();
        }
        database.closeTransaction();
    }

    private void InsertVersion(){
        database.openTransaction();
        int deleted =0;
        deleted = database.deleteData(VersionDB.TABLE_NAME,null,null);

        if(deleted>0){
            ContentValues dbvalues = new ContentValues();
            dbvalues.put(VersionDB.XML_DBVersion,String.valueOf(Constants.db_version));
            database.insertDataSQL(VersionDB.TABLE_NAME,dbvalues);
            database.commitTransaction();
            database.closeTransaction();
        }else{
            ContentValues dbvalues = new ContentValues();
            dbvalues.put(VersionDB.XML_DBVersion,String.valueOf(Constants.db_version));
            database.insertDataSQL(VersionDB.TABLE_NAME,dbvalues);
            database.commitTransaction();
            database.closeTransaction();
        }
        database.closeTransaction();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MarshMallowPermission.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_BY_LOAD_PROFILE :
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                   RunningCreate();

                } else {

                    //permission denied

                }
                break;
        }
    }

    private void RunningCreate(){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                InsertVersion();
                initMaster();
                InsertMasterScore();
                if(!isFinishing()){
                    FolderHandler folderHandler = new FolderHandler(SplashScreenActivity.this);
                    if(folderHandler.init()){
                        readDataAsync = new ReadDataAsynctask(folderHandler);
                        readDataAsync.execute();

                    }else{
                        new DialogNotification(SplashScreenActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.error_sd_card), true).show();
                    }
                }
            }
        }, 3000);
    }

    private final boolean isPermissionGranted() {
        int permissionCheck = ActivityCompat.checkSelfPermission((Context)this, "android.permission.WRITE_EXTERNAL_STORAGE");
        return permissionCheck ==  PackageManager.PERMISSION_GRANTED;
    }

    private final void requestPermission(String permission, int requestCode) {
        ActivityCompat.requestPermissions((Activity)this, new String[]{permission}, requestCode);
    }
    @TargetApi(23)
    private void ensurePermissions(String... permissions) {
        boolean request = false;
        for (String permission : permissions)
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                request = true;
                break;
            }

        if (request) {
            requestPermissions(permissions, REQUEST_CODE_PERMISSION);
        }
    }
}
