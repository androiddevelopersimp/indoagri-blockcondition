package co.indoagri.blockcondition.activity;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.DatabaseHelper;
import co.indoagri.blockcondition.dialog.DialogConfirmation;
import co.indoagri.blockcondition.dialog.DialogNotification;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.handler.FileEncryptionHandler;
import co.indoagri.blockcondition.handler.FolderHandler;
import co.indoagri.blockcondition.listener.DialogConfirmationListener;
import co.indoagri.blockcondition.listener.DialogNotificationListener;
import de.hdodenhof.circleimageview.CircleImageView;

public class RestoreDatabaseActivity extends BaseActivity implements OnClickListener, DialogNotificationListener, DialogConfirmationListener {
    private Toolbar tbrMain;
    private EditText edtRestoreDatabase2Password;
    private Button btnRestoreDatabase2Submit;

    private RestoreAsyncTask RestoreAsync;
    private DialogProgress dialogProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animOnStart();

        setContentView(R.layout.activity_restore);

        registerBaseActivityReceiver();
        TextView txtUserDivision = (TextView)findViewById(R.id.txtUserDivision);
        txtUserDivision.setVisibility(View.GONE);
        CircleImageView imgActionBarLogout = (CircleImageView) findViewById(R.id.imgActionBarLogout);
        imgActionBarLogout.setVisibility(View.GONE);
        edtRestoreDatabase2Password = (EditText) findViewById(R.id.edtRestoreDatabase2Password);
        btnRestoreDatabase2Submit = (Button) findViewById(R.id.btnRestoreDatabase2Submit);


        btnRestoreDatabase2Submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnRestoreDatabase2Submit:
//			String password = edtRestoreDatabase2Password.getText().toString().trim();
                String password = "12345";

                if(!TextUtils.isEmpty(password)){
//				String today = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_RESTORE);
//				List<String> lstToken = new MD5Handler(new DeviceHandler(RestoreDatabase2Activity.this).getImei(), today).generateToken();
//
                    boolean found = true;
//				for(int i = 0; i < lstToken.size(); i++){
//					Log.d("tag", lstToken.get(i));
//
//					if(lstToken.get(i).equals(password)){
//						found = true;
//						break;
//					}
//				}

                    if(found){
                        new DialogConfirmation(RestoreDatabaseActivity.this,
                                getResources().getString(R.string.informasi),
                                getResources().getString(R.string.databsae_replaced),
                                null, R.id.btnRestoreDatabase2Submit).show();
                    }else{
                        new DialogNotification(RestoreDatabaseActivity.this,
                                getResources().getString(R.string.informasi),
                                getResources().getString(R.string.restore_invalid), false).show();
                    }
                }else{
                    new DialogNotification(RestoreDatabaseActivity.this,
                            getResources().getString(R.string.informasi),
                            getResources().getString(R.string.restore_invalid), false).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private class RestoreAsyncTask extends AsyncTask<Void, Void, Boolean>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!isFinishing()){
                dialogProgress = new DialogProgress(RestoreDatabaseActivity.this, getResources().getString(R.string.wait));
                dialogProgress.show();
            }
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            return Restore();
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            if(success){
                new DialogNotification(RestoreDatabaseActivity.this,
                        getResources().getString(R.string.informasi),
                        getResources().getString(R.string.restore_successed) + "\n" +
                                getResources().getString(R.string.apps_restart), true).show();
            }else{
                new DialogNotification(RestoreDatabaseActivity.this,
                        getResources().getString(R.string.informasi),
                        getResources().getString(R.string.restore_failed), false).show();
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        animOnFinish();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unRegisterBaseActivityReceiver();

        if(RestoreAsync != null && RestoreAsync.getStatus() != AsyncTask.Status.FINISHED){
            RestoreAsync.cancel(true);
        }
    }

    @Override
    public void onOK(boolean is_finish) {
        if(!is_finish){
            edtRestoreDatabase2Password.requestFocus();
        }else{
            closeAllActivities();
        }
    }

    @Override
    public void onConfirmOK(Object object, int id) {
        new RestoreAsyncTask().execute();
    }

    @Override
    public void onConfirmCancel(Object object, int id) {

    }

    private boolean isFoundRestoreDatabase(){
        boolean found = false;
        FolderHandler folderHandler = new FolderHandler(RestoreDatabaseActivity.this);

        if(folderHandler.isSDCardWritable()){
            File[] files = new File[0];
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                files = getExternalFilesDirs(Environment.DIRECTORY_DOCUMENTS);
            }

            if(files.length > 1){
                File folder = new File(files[files.length-1], FolderHandler.DATABASE);

                if(!folder.exists()){
                    folder.mkdirs();
                }

                if(folder.exists()){
                    File file = new File(folder, DatabaseHelper.dbName);
                    found = file.exists();
                }
            }
        }

        return found;
    }

    private boolean restoreDatabase(){
        try{
            FolderHandler folderHandler = new FolderHandler(RestoreDatabaseActivity.this);

            if(folderHandler.isSDCardWritable() && folderHandler.init()){
                File[] files = getExternalFilesDirs(Environment.DIRECTORY_DOCUMENTS);

                if(files.length > 1){
                    File folder = new File(files[files.length-1], FolderHandler.DATABASE);

                    if(folder.exists()){
                        File file = new File(folder, DatabaseHelper.dbName);

                        if(file.exists()){
                            if(new FileEncryptionHandler(RestoreDatabaseActivity.this).decrypt(file.getAbsolutePath())){

                                File database = getApplicationContext().getDatabasePath(DatabaseHelper.dbName);
                                File databaseImport = new File(folderHandler.getFileDatabaseImport(), DatabaseHelper.dbName);

                                if(databaseImport.exists()){
                                    String outFileName = database.getAbsolutePath();
                                    OutputStream myOutput = new FileOutputStream(outFileName);
                                    InputStream myInput = new FileInputStream(databaseImport);

                                    byte[] buffer = new byte[1024];
                                    int length;
                                    while ((length = myInput.read(buffer)) > 0){
                                        myOutput.write(buffer, 0, length);
                                    }

                                    myInput.close();
                                    myOutput.flush();
                                    myOutput.close();

//	        			            databaseImport.delete();

                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }catch(IOException e){
            e.printStackTrace();
        }

        return false;
    }

    private boolean Restore(){
        boolean result = false;
    try {

        FolderHandler folderHandler = new FolderHandler(RestoreDatabaseActivity.this);
        File sd = null;
        if(folderHandler.init()){
            sd = new File(folderHandler.getFileDatabaseImport());
        }

        if (sd.canWrite()) {
            File backupDB = new File(folderHandler.getFileDatabaseImport(), DatabaseHelper.dbName);
            File currentDB = getApplicationContext().getDatabasePath(DatabaseHelper.dbName);
            if (currentDB.exists()) {
                FileChannel src = new FileInputStream(backupDB).getChannel();
                FileChannel dst = new FileOutputStream(currentDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
              /*  Toast.makeText(getApplicationContext(), "Database Restored successfully", Toast.LENGTH_SHORT).show();*/
                result = true;
            }
            else{
                result = false;
            }
        }
    } catch (Exception e) {
        result = false;
    }
    return result;
}

}
