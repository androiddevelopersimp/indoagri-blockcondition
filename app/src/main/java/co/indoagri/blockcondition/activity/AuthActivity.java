package co.indoagri.blockcondition.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.Date;
import co.indoagri.blockcondition.MyApps;
import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.fragment.AuthLoginFragment;
import co.indoagri.blockcondition.fragment.AuthMasterDownloadFragment;
import co.indoagri.blockcondition.fragment.AuthRestoreDatabaseFragment;
import co.indoagri.blockcondition.listener.DialogConfirmationListener;
import co.indoagri.blockcondition.listener.DialogDateListener;
import co.indoagri.blockcondition.listener.DialogNotificationListener;
import co.indoagri.blockcondition.listener.DialogTimeListener;
import co.indoagri.blockcondition.utils.MarshMallowPermission;
import co.indoagri.blockcondition.utils.device.DeviceUtils;
import co.indoagri.blockcondition.widget.ToastMessage;

import static co.indoagri.blockcondition.activity.ViewToolbar.context;

public class AuthActivity extends BaseActivity implements AuthMasterDownloadFragment.FragmentMasterListener,
        AuthLoginFragment.FragmentLoginListener,
        AuthRestoreDatabaseFragment.FragmentRestoreListener,
        DialogNotificationListener,DialogConfirmationListener,
        DialogDateListener,DialogTimeListener {
    boolean doubleBackToExitPressedOnce = false;
    private MarshMallowPermission marshMallowPermission;
    boolean onUserInteraction = false;
    View rootView;
    ToastMessage toastMessage;
    public static boolean isUsbOtgAvailable = false;
    public static boolean isUsbOtgPermit = true;
    private static final String ACTION_USB_PERMISSION =  DeviceUtils.getPackageName(MyApps.getAppContext())+".USB_PERMISSION";
    public static String AUTHLOGIN = "AUTHLOGIN";
    public static String AUTHMASTER= "AUTHMASTER";
    public static String AUTHRESTORE= "AUTHRESTORE";
    public static int M_EXPORTIMPORT= 101;
    public static int M_SETTING= 102;
    public static int M_MAIN = 103;
    public static int M_RESTORE = 104;
    @Override

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        registerBaseActivityReceiver();
        animOnStart();
        marshMallowPermission = new MarshMallowPermission(AuthActivity.this);
        setContentView(R.layout.activity_auth);
        toastMessage = new ToastMessage(getApplicationContext());
        ConstraintLayout constraintLayout = (ConstraintLayout)findViewById(R.id.constraint);
        rootView =constraintLayout;

        setDefault();
        RunUSB();
    }

    private void setDefault(){
        Class fragmentClass;
        fragmentClass = AuthLoginFragment.class;
        Bundle args = new Bundle();
        args.putString("param1", "PARAMS1");
        setDefaultFragment(fragmentClass,
                args,
                R.id.fragment_container,AUTHLOGIN);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("ONPAUSE", "onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("ONRESUME", "onResume()");
    }

    @Override
    public void onBackPressed() {
        int Value = getFragmentManagerAccount();
        if (Value > 1) {
            Log.i("MainActivity", "popping backstack");
            baseFragmentManager.popBackStack();
        } else {
            if (doubleBackToExitPressedOnce) {
                closeAllActivities();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);

        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.e("ONSTOP", "OnStop()");
        try {
            if (mUsbReceiver!=null)
               unregisterReceiver(mUsbReceiver);

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.e("ONDESTROY", "OnDestory()");
        stopLogoutTimer();
        unRegisterBaseActivityReceiver();
    }



    @Override
    public void onOK(boolean is_finish) {
        toastMessage.shortMessage("");
    }

    @Override
    public void onConfirmOK(Object object, int id) {
        toastMessage.shortMessage("Confirmation OK Listener");
    }

    @Override
    public void onConfirmCancel(Object object, int id) {

    }

    @Override
    public void onDateOK(Date date, int id) {
        toastMessage.shortMessage("Ok Dialog Date Listener");
    }


    @Override
    public void onTimeOK(int hour, int minute, int second, int id) {
        toastMessage.shortMessage("OK Dialog Time Listener");
    }


    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if(action.equalsIgnoreCase(UsbManager.ACTION_USB_DEVICE_ATTACHED)){
              //  cbxSettingsMasterDownUsbOtg.setChecked(true);
                isUsbOtgAvailable = true;
            }else if(action.equalsIgnoreCase(UsbManager.ACTION_USB_DEVICE_DETACHED)){
               // cbxSettingsMasterDownUsbOtg.setChecked(false);
                isUsbOtgAvailable = false;
            }

            if(action.equalsIgnoreCase(ACTION_USB_PERMISSION) && isUsbOtgAvailable){
                synchronized (this) {
                    UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    //cbxSettingsMasterDownUsbOtg.setChecked(false);
                    isUsbOtgPermit = false;

                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                      //      cbxSettingsMasterDownUsbOtg.setChecked(true);
                            isUsbOtgPermit = true;
                        }
                    }
                }
            }

        }
    };

    public void ToRestoreActivity(){
        if (!marshMallowPermission.checkPermissionForExternalStorageRead()) {
            marshMallowPermission.requestPermissionForExternalStorageRead(MarshMallowPermission.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_BY_LOAD_PROFILE_READ);
        }else{
            Intent intentLogin= new Intent(AuthActivity.this, RestoreDatabaseActivity.class);
            startActivityForResult(intentLogin, M_RESTORE);
            finish();
            animOnFinish();
        }

    }

    public void toMaster(){
        Bundle args = new Bundle();
        args.putString("param1", "PARAMS1");
        nextFragmentCustomTop(AuthMasterDownloadFragment.class,
                args,
                R.id.fragment_container,AUTHMASTER);
    }
    public void toRestore(){
        Bundle args = new Bundle();
        args.putString("param1", "PARAMS1");
        nextFragmentCustomTop(AuthRestoreDatabaseFragment.class,
                args,
                R.id.fragment_container,AUTHRESTORE);
    }

    @Override
    public void onInterfaceLogin(String input) {
        if(input.toString().equalsIgnoreCase(getResources().getString(R.string.login)))
        {
            Intent intentLogin= new Intent(AuthActivity.this, MenuActivity.class);
            intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivityForResult(intentLogin, M_MAIN);
            finish();
            animOnFinish();
        }
        if(input.toString().equalsIgnoreCase(getResources().getString(R.string.settings_sync_master)))
        {
                fragment= baseFragmentManager.findFragmentByTag(AUTHMASTER);
                if(fragment == null){
                    if (!marshMallowPermission.checkPermissionForExternalStorageRead()) {
                        marshMallowPermission.requestPermissionForExternalStorageRead(MarshMallowPermission.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_BY_LOAD_PROFILE_READ);
                    }else {
                        toMaster();
                    }
                    }
                else{
                    getSupportFragmentManager().popBackStack();
                }

        }
            if(input.toString().equalsIgnoreCase(getResources().getString(R.string.restore_database))){
                ToRestoreActivity();
        }
    }

    @Override
    public void onInputMaster(CharSequence input) {
  /*    ToastMessage toastMessage = new ToastMessage(getApplicationContext());
      toastMessage.shortMessage(String.valueOf(input));*/
    }

    void RunUSB(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        filter.addAction(Environment.MEDIA_MOUNTED);
        filter.addAction(Environment.MEDIA_UNMOUNTED);
        registerReceiver(mUsbReceiver, filter);
    }

    @Override
    public void onInterfaceRestore(String input) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check that it is the SecondActivity with an OK result
        if (requestCode == M_EXPORTIMPORT) {
            if(resultCode == RESULT_OK){
                /*ToastMessage toastMessage = new ToastMessage(getApplicationContext());
                toastMessage.shortMessage("TESTING FROM EXPORT IMPORT");*/
            }
        }
    }
    private String Screens(){
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        wm.getDefaultDisplay().getRealSize(size);
        String resolution = size.x + "x" + size.y;
        return resolution;
    }
}
