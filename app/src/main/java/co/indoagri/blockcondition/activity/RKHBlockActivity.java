package co.indoagri.blockcondition.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.RKHBlockAdapter;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.model.Data.BlockHdrc;
import co.indoagri.blockcondition.routines.Constants;

public class RKHBlockActivity extends AppCompatActivity implements RKHBlockAdapter.ItemListener, RKHBlockAdapter.ItemChooseListener, SearchView.OnQueryTextListener {
    private SearchView searchView;
    Toolbar toolbar;
    TextView mTextToolbar,TxtsaveToolbar;
    Bundle bundle;
    ArrayList<BlockHdrc> blockHdrcs = new ArrayList<BlockHdrc>();
    RecyclerView recyclerView;
    RKHBlockAdapter adapter;
    String companyCode;
    String division;
    String gang;
    String estate;
    GetDataAsyncTask getDataAsync;
    String InputBlock;
    public static boolean BtnSaveVisible = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.rkhblock_layout);
        setUpToolbar();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        searchView = findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(this);
        initEmployee();
    }

    void setUpToolbar() {
        bundle= getIntent().getExtras();
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        TxtsaveToolbar = (findViewById(R.id.txt_SaveToolbar));
        TxtsaveToolbar.setVisibility(View.GONE);
        TxtsaveToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveDataAndBack();
            }
        });
        mTextToolbar.setText(getResources().getString(R.string.alihkan_mandor));
        if(bundle!=null) {
            String Title  = bundle.getString("TOOLBARTITLE");
            mTextToolbar.setText(Title);
        }else{
            mTextToolbar.setText(getResources().getString(R.string.alihkan_mandor));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    void initEmployee(){
        companyCode = ViewProfile.getCompanyCode();
        estate = ViewProfile.getEstate();
        division = ViewProfile.getDivisions();
        gang = ViewProfile.getGang();
        adapter = new RKHBlockAdapter(this, blockHdrcs,this,this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        getData();
    }
    private void getData(){
        getDataAsync = new GetDataAsyncTask(false, BlockHdrc.TABLE_NAME, null,
                BlockHdrc.XML_COMPANY_CODE + "=?" + " and " +
                        BlockHdrc.XML_DIVISION + "=?" + " and " +
                        BlockHdrc.XML_ESTATE + "=?",
                new String [] {companyCode,division, estate},
                BlockHdrc.XML_BLOCK, null, BlockHdrc.XML_BLOCK, null);
        getDataAsync.execute();
    }

    @Override
    public void onItemClick(BlockHdrc item, int position) {

    }

    @Override
    public void onItemChoose(BlockHdrc item, int position) {
        BtnSaveVisible = true;
        InitialbtnSave();
        InputBlock = item.getBlock();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        BtnSaveVisible = false;
        InitialbtnSave();
        adapter.getFilter().filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        BtnSaveVisible = false;
        InitialbtnSave();
        adapter.getFilter().filter(newText);
        return false;
    }


    private class GetDataAsyncTask extends AsyncTask<Void, ArrayList<BlockHdrc>, ArrayList<BlockHdrc>> implements SearchView.OnQueryTextListener {
        boolean distinct;
        String tableName;
        String [] columns;
        String whereClause;
        String [] whereArgs;
        String groupBy;
        String having;
        String orderBy;
        String limit;
        public GetDataAsyncTask(boolean distinct, String tableName, String [] columns, String whereClause, String [] whereParams,
                                String groupBy, String having, String orderBy, String limit){
            this.distinct = distinct;
            this.tableName = tableName;
            this.columns = columns;
            this.whereClause = whereClause;
            this.whereArgs = whereParams;
            this.groupBy = groupBy;
            this.having = having;
            this.orderBy = orderBy;
            this.limit = limit;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected ArrayList<BlockHdrc> doInBackground(Void... voids) {
            DatabaseHandler database = new DatabaseHandler(RKHBlockActivity.this);
            ArrayList<BlockHdrc> listTemp = new ArrayList<BlockHdrc>();
            List<Object> listObject;
            database.openTransaction();
            listObject = database.getListData(distinct, tableName, columns, whereClause, whereArgs, groupBy, having, orderBy, limit);
            database.closeTransaction();
            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    BlockHdrc empNew = (BlockHdrc) listObject.get(i);
                    boolean foundFilter = false;
                    boolean foundCurrent = false;
                    if(blockHdrcs.size() > 0){
                        for(int x = 0; x < blockHdrcs.size(); x++){
                            BlockHdrc empCur = (BlockHdrc) blockHdrcs.get(x);

                            if(empNew.getBlock().equals(empCur.getBlock())){
                                foundCurrent = true;
                                break;
                            }
                        }
                    }

                    if(!foundFilter && !foundCurrent){
                        listTemp.add(empNew);
                    }
                }
            }

            return listTemp;
        }

        @Override
        protected void onPostExecute(ArrayList<BlockHdrc> lstTemp) {
            super.onPostExecute(lstTemp);
            for(int i = 0; i < lstTemp.size(); i++){
                BlockHdrc blockHdrc = (BlockHdrc) lstTemp.get(i);
                adapter.addData(blockHdrc);
            }

        }

        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        DisplayDialogSave();
    }

    void DisplayDialogSave(){
        if(InputBlock==null){
            InputBlock = null;
            Intent intent=new Intent();
            setResult(RESULT_OK, intent);
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }else if(InputBlock!=null){
            AlertDialog.Builder builder = new AlertDialog.Builder(RKHBlockActivity.this);
            builder.setTitle("Apakah Akan menyimpan data");
            builder.setMessage("Simpan Data ? ");
            // Set the alert dialog yes button click listener
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SaveDataAndBack();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    NotSaveDataAndBack();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    void NotSaveDataAndBack(){
        new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputgang(null);
        InputBlock = null;
        Intent intent=new Intent();
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    void SaveDataAndBack(){
        new PreferenceManager(getApplicationContext(), Constants.shared_name).setRkhInputblock(InputBlock);
        InputBlock = null;
        Intent intent=new Intent();
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    void InitialbtnSave(){
        if(BtnSaveVisible){
            TxtsaveToolbar.setVisibility(View.VISIBLE);
        }else{
            TxtsaveToolbar.setVisibility(View.GONE);
        }
    }


}
