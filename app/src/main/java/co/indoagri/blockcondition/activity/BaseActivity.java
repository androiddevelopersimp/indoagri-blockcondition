package co.indoagri.blockcondition.activity;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import co.indoagri.blockcondition.MyApps;
import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.handler.LanguageHandler;
import co.indoagri.blockcondition.routines.Constants;
import co.indoagri.blockcondition.utils.device.DeviceUtils;

public abstract class BaseActivity  extends AppCompatActivity {
    public static final String FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION = DeviceUtils.getPackageName(MyApps.getAppContext())+".FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION";
    private BaseActivityReceiver baseActivityReceiver = new BaseActivityReceiver();
    public static final IntentFilter INTENT_FILTER = createIntentFilter();
    public static FragmentManager baseFragmentManager;
    public static Fragment fragment;
    public static FragmentTransaction fragmentTransaction;
    public interface LogOutListener {
            void doLogout();
        }

        static Timer longTimer;
        static final int LOGOUT_TIME = 50000; // delay in milliseconds i.e. 5 min = 300000 ms or use timeout argument

        public static synchronized void startLogoutTimer(final Context context, final LogOutListener logOutListener) {
            if (longTimer != null) {
                longTimer.cancel();
                longTimer = null;
            }
            if (longTimer == null) {

                longTimer = new Timer();

                longTimer.schedule(new TimerTask() {

                    public void run() {

                        cancel();

                        longTimer = null;

                        try {
                            boolean foreGround = new ForegroundCheckTask().execute(context).get();

                            if (foreGround) {
                                logOutListener.doLogout();
                            }

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }

                    }
                }, LOGOUT_TIME);
            }
        }

    public static synchronized void stopLogoutTimer() {
        if (longTimer != null) {
            longTimer.cancel();
            longTimer = null;
        }
    }

    static class ForegroundCheckTask extends AsyncTask<Context, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Context... params) {
            final Context context = params[0].getApplicationContext();
            return isAppOnForeground(context);
        }

        private boolean isAppOnForeground(Context context) {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
            if (appProcesses == null) {
                return false;
            }
            final String packageName = context.getPackageName();
            for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
                if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                    return true;
                }
            }
            return false;
        }
    }
    private static IntentFilter createIntentFilter(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION);
        return filter;
    }

    protected void registerBaseActivityReceiver() {
        registerReceiver(baseActivityReceiver, INTENT_FILTER);
    }

    protected void unRegisterBaseActivityReceiver() {
        unregisterReceiver(baseActivityReceiver);
    }

    public class BaseActivityReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION)){
                finish();
            }
        }
    }

    protected void closeAllActivities(){
        sendBroadcast(new Intent(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION));
    }

    protected void animOnStart(){
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    protected void animOnFinish(){
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    protected void animOnStartUp(){
        overridePendingTransition(R.anim.push_up_in, R.anim.push_down_in);
    }

    protected void animOnFinishDown(){
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void setDefaultFragment(Class classFragment,
                              Bundle args,
                              Integer layout,
                                   String Tag){
        try {
            fragment = (Fragment) classFragment.newInstance();
            fragment.setArguments(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
        baseFragmentManager = getSupportFragmentManager();
        baseFragmentManager.beginTransaction().add(layout, fragment)
                .addToBackStack(Tag)
                .commit();
    }

    public int getFragmentManagerAccount(){
            baseFragmentManager = getSupportFragmentManager();
            int Value = 0;
            Value = baseFragmentManager.getBackStackEntryCount();
            return Value;
    }

    public void nextFragment(      Class classFragment,
                                   Bundle args,
                                   Integer layout,
                                    String Tag){
        try {
            fragment = (Fragment) classFragment.newInstance();
            fragment.setArguments(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
        fragmentTransaction=getSupportFragmentManager().beginTransaction();
        //fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        fragmentTransaction.replace(layout, fragment, Tag);
        fragmentTransaction.addToBackStack(Tag);
        fragmentTransaction.commit();
    }
    public void ExistFragment(      Class classFragment,
                                   Bundle args,
                                   Integer layout,
                                   String Tag){
        try {
            fragment = (Fragment) classFragment.newInstance();
            fragment.setArguments(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
        fragmentTransaction=getSupportFragmentManager().beginTransaction();
        //fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        fragmentTransaction.replace(layout, fragment, Tag);
        fragmentTransaction.commit();
    }
    public void nextFragmentCustomTop(      Class classFragment,
                                   Bundle args,
                                   Integer layout,
                                   String Tag){
        try {
            fragment = (Fragment) classFragment.newInstance();
            fragment.setArguments(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
        fragmentTransaction=getSupportFragmentManager().beginTransaction();
        //fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        fragmentTransaction.replace(layout, fragment, Tag);
        fragmentTransaction.addToBackStack(Tag);
        fragmentTransaction.commit();
    }
    public void nextFragmentCustomRight(      Class classFragment,
                                            Bundle args,
                                            Integer layout,
                                            String Tag){
        try {
            fragment = (Fragment) classFragment.newInstance();
            fragment.setArguments(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
        fragmentTransaction=getSupportFragmentManager().beginTransaction();
        //fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        fragmentTransaction.replace(layout, fragment, Tag);
        fragmentTransaction.addToBackStack(Tag);
        fragmentTransaction.commit();
    }

    protected void getConfig(){
        String countryCode = new PreferenceManager(BaseActivity.this,Constants.shared_name).getCountryCode();
        new LanguageHandler(BaseActivity.this).updateLanguage(countryCode);
    }
    public void setViewAndChildrenEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }
}
