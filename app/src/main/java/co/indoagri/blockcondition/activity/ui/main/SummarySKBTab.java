package co.indoagri.blockcondition.activity.ui.main;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import co.indoagri.blockcondition.R;
import co.indoagri.blockcondition.adapter.AdapterBlockHdrc;
import co.indoagri.blockcondition.adapter.AdapterSummarySKB;
import co.indoagri.blockcondition.data.DatabaseHandler;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.dialog.DialogProgress;
import co.indoagri.blockcondition.fragment.MasterBlockHdrcFragment;
import co.indoagri.blockcondition.model.Data.BlockHdrc;
import co.indoagri.blockcondition.model.Data.tblT_BlockCondition;
import co.indoagri.blockcondition.routines.Constants;

import static co.indoagri.blockcondition.activity.BlockActivity.SESS_PERIOD;
import static co.indoagri.blockcondition.activity.BlockActivity.SESS_ZYEAR;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SummarySKBTab.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SummarySKBTab#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SummarySKBTab extends Fragment implements AdapterView.OnItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private List<tblT_BlockCondition> tblTBlockConditionList= new ArrayList<tblT_BlockCondition>();
    private AdapterSummarySKB adapter;
    private ListView listSummary;
    private GetSKBSummaryData getDataAsync;
    DatabaseHandler database;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private String mParam4;

    private OnFragmentInteractionListener mListener;
    View view;
    public SummarySKBTab() {
        // Required empty public constructor
    }

    public static SummarySKBTab newInstance() {
        SummarySKBTab fragment = new SummarySKBTab();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        database = new DatabaseHandler(getActivity());
        Bundle bundle = getArguments();
        if (bundle!= null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);
        }
        mParam1 = new PreferenceManager(getActivity(), Constants.shared_name).getSSYear();
        mParam2 = new PreferenceManager(getActivity(), Constants.shared_name).getSSPeriode();
        mParam3 = new PreferenceManager(getActivity(), Constants.shared_name).getSSBlock();
        mParam4 = new PreferenceManager(getActivity(), Constants.shared_name).getSSPhase();
        if(mParam1!=null){
            if(mParam4.equalsIgnoreCase("Mature")){
                view = inflater.inflate(R.layout.fragment_summary_skb, container, false);
            }else{
                view = inflater.inflate(R.layout.fragment_summary_skb, container, false);
            }
        }else{
            view = inflater.inflate(R.layout.fragment_summary_null, container, false);
        }
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(mParam1!=null) {
            listSummary= (ListView) view.findViewById(R.id.lsvSummarySKB);
            TextView textView = view.findViewById(R.id.TextID);
            textView.setVisibility(View.GONE);
            init();
        }else{
            TextView txtID = view.findViewById(R.id.txtID);
            txtID.setText("Data Tidak Ada");
        }
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        tblT_BlockCondition tblT_blockCondition = (tblT_BlockCondition) adapter.getItem(position);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void init(){
            listSummary.setOnItemClickListener(this);
            adapter = new AdapterSummarySKB(getActivity(), tblTBlockConditionList, R.layout.item_summary_skb_mature);
            listSummary.setAdapter(adapter);
            listSummary.setScrollingCacheEnabled(false);
            getData();
    }
    private void getData(){

        getDataAsync = new GetSKBSummaryData();
        getDataAsync.execute();
    }
    private class GetSKBSummaryData extends AsyncTask<Void, List<tblT_BlockCondition>, List<tblT_BlockCondition>> {
        private DialogProgress dialogProgress;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(!getActivity().isFinishing()){
//                dialogProgress = new DialogProgress(getActivity(),getContext().getResources().getString(R.string.loading));
//                dialogProgress.show();
            }
        }
        @Override
        protected List<tblT_BlockCondition> doInBackground(Void... voids) {
            database.openTransaction();
            List<tblT_BlockCondition> listTemp = new ArrayList<tblT_BlockCondition>();
            List<Object> listObject;
            String[] a = null;
            String query = null;
            try{
                        a = new String[4];
                        a[0] = mParam1;
                        a[1] = mParam2;
                        a[2] = "2";
                        a[3] = mParam3;
                        query = "SELECT * FROM tblT_BlockCondition a INNER JOIN BLOCK_HDRC b " +
                                " on b.Block = a.Block " +
                                " WHERE a.zyear= ?" +
                                " AND a.period=?" +
                                " AND a.translevel= ? " +
                                " AND a.Block IN (SELECT Block FROM BLOCK_HDRC " +
                                " where block =?)";
                    listObject = database.getListDataRawQuery(query,tblT_BlockCondition.TABLE_NAME,a);
                database.closeTransaction();
                if(listObject.size() > 0){
                    for(int i = 0; i < listObject.size(); i++){
                        tblT_BlockCondition tblT_blockCondition = (tblT_BlockCondition) listObject.get(i);

                        listTemp.add(tblT_blockCondition);
                    }
                }
            }catch(SQLiteException e){
                e.printStackTrace();
                database.closeTransaction();
            }finally{
                database.closeTransaction();
            }
            return listTemp;

        }

        @Override
        protected void onPostExecute(List<tblT_BlockCondition> listTemp) {
            super.onPostExecute(listTemp);
            for(int i = 0; i < listTemp.size(); i++){
                tblT_BlockCondition blockHdrc = (tblT_BlockCondition) listTemp.get(i);
                adapter.addData(blockHdrc);

            }
        }

    }
}
