package co.indoagri.blockcondition.animation;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;

import co.indoagri.blockcondition.MyApps;
import co.indoagri.blockcondition.R;

public class SlideUpandDown {

    private final Context context;

    public SlideUpandDown (Context context)
    {
        this.context = context;
    }
    public void SlideUP(Activity activity,View viewSlide,View btnUp,View btnDown){
        Animation bottomUp = AnimationUtils.loadAnimation(activity,
                R.anim.push_up_in);

        viewSlide.startAnimation(bottomUp);
        viewSlide.setVisibility(View.VISIBLE);
        btnUp.setVisibility(View.GONE);
        btnDown.setVisibility(View.VISIBLE);
        AnimationDown(btnDown);
    }
    public void SlideDown(Activity activity,View viewSlide,View btnUp,View btnDown){
        btnUp.setVisibility(View.VISIBLE);
        btnDown.setVisibility(View.GONE);

        // Hide the Panel
        Animation bottomDown = AnimationUtils.loadAnimation(activity,
                R.anim.push_down_out);

        viewSlide.startAnimation(bottomDown);
        viewSlide.setVisibility(View.GONE);
        AnimationUP(btnUp);
    }

    public void AnimationUP(View btnUP){
        TranslateAnimation anim = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, -1.0f); // this is distance of top and bottom form current positiong

        anim.setDuration(1000);
        anim.setRepeatCount(1);
        anim.setRepeatMode(Animation.REVERSE);
        btnUP.startAnimation(anim);
    }

    public void AnimationSide(View btnUP){
        TranslateAnimation anim = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, -2f,
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0f); // this is distance of top and bottom form current positiong

        anim.setDuration(1000);
        anim.setRepeatCount(1);
        anim.setRepeatMode(Animation.REVERSE);
        btnUP.startAnimation(anim);
    }
    public void AnimationSideRight(View btnUP){
        TranslateAnimation anim = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, 2f,
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0f); // this is distance of top and bottom form current positiong

        anim.setDuration(1000);
        anim.setRepeatCount(1);
        anim.setRepeatMode(Animation.REVERSE);
        btnUP.startAnimation(anim);
    }

    public void AnimationDown(View btnDown){
        TranslateAnimation anim = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, -1.0f); // this is distance of top and bottom form current positiong

        anim.setDuration(1000);
        anim.setRepeatCount(1);
        anim.setRepeatMode(Animation.REVERSE);
        btnDown.startAnimation(anim);
    }

}
