package co.indoagri.blockcondition;


import android.app.Application;
import android.content.Context;

import co.indoagri.blockcondition.handler.BroadcastConnectorHandler;


public class MyApps extends Application {

    private static MyApps mInstance;

    private static Context context;

    public void onCreate() {
        super.onCreate();
        mInstance = this;
        MyApps.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MyApps.context;
    }

    public static synchronized MyApps getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(BroadcastConnectorHandler.ConnectivityReceiverListener listener) {
        BroadcastConnectorHandler.connectivityReceiverListener = listener;
    }
}