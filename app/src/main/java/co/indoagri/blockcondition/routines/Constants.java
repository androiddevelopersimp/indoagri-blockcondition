package co.indoagri.blockcondition.routines;
import android.content.Context;

import java.io.File;

import co.indoagri.blockcondition.MyApps;
import co.indoagri.blockcondition.data.PreferenceManager;
import co.indoagri.blockcondition.utils.device.DeviceUtils;

public class Constants {
    static Context context = MyApps.getAppContext();
    PreferenceManager preferenceManager;

    public boolean initHandler(){
        preferenceManager = new PreferenceManager(context,shared_name);
        preferenceManager.setValue("APPSVERSION",DeviceUtils.APIVERSION(context));
        File f1 = new File("/data/data/"+DeviceUtils.getPackageName(context)+"/shared_prefs/"+shared_name+".xml");
        if(f1.exists()){
            return true;
        }
        else{
            return false;
        }
    }

    public final static int TotalBlock = 32;
    public final static int TotalSurveyedBlock = 33;
    public final static int txtHome_Date = 34;
    public final static int MatureNumberofBlock = 21;
    public final static int MatureSurveyedBlock = 22;
    public final static int MatureNotSurveyedBlock = 23;

    public final static int IMMatureNumberofBlock = 11;
    public final static int IMMatureSurveyedBlock = 12;
    public final static int IMMatureNotSurveyedBlock = 13;
    public final static int MainResult_Block = 14;
    public final static int MainResult_SKB = 15;
    public final static int MainResult_POKOK = 16;


    public final static String ROWID = "ROWID";
    public final static String SEARCH = "search";
    public final static String inStatus = "INSTATUS";
    public final static String TYPE = "type";
    public final static String DISTINCT = "distinct";
    public static final String CONSTBUNDLE = "CONSTBUNDLE";
    public static final String PARAM1 = "param1";
    public static final String PARAM2 = "param2";
    public static final String PARAM3 = "param3";
    public static final String Versi = "versi.json";
    public static final String shared_name = DeviceUtils.getAppName(context)+"Pref";
    public static final String db_name = DeviceUtils.getAppName(context)+".db";
    public static final String apk_name = DeviceUtils.getAppName(context)+".apk";
    public static final int db_version = 162;
    public final static String SERVER = "http://10.126.20.217/hms/";
    public static final String url_login = "http://dev.indoagri.co.id/fajar/eppbr/api/key";
    public static final String url_consumer = "http://dev.indoagri.co.id/fajar/eppbr/api/";
    public static final String dir_package = "/data/"+ DeviceUtils.getPackageName(context) +"/databases/"+context+".db";
    public static final String folder_root = DeviceUtils.getAppName(context);
    public static final String folder_image = "image";
    public static final String folder_backup = "backup";
    public static final String folder_report = "report";
    public static final String folder_general = "general";
    public static final String folder_export= "export";
    public static final String folder_import= "import";
    public static final String folder_exported= "exported";
    public static final String folder_imported= "imported";
    public static final String const_DeviceID = DeviceUtils.getAndroidId(context);
    public static final String const_VersionName = DeviceUtils.getAppVersion(context);
    public static final String const_AppsName = DeviceUtils.getAppName(context);
    public static final String const_IMEI = DeviceUtils.getUniqueIMEIId(context);
    public static final String const_APIVERSION = String.valueOf(DeviceUtils.APIVERSION(context));
}
